import os
import sys
import glob
import math
import subprocess
import tkMessageBox
from Tkinter import *
from hotpot.configuring import read_new_ebs_config
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'

def main(configs):
    options = configs['options']
    exes = configs['exes']
    ebs_params = configs['ebs_params']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    raw_end  =  data_root.split('\\')[-2]
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    modification = options['modification'].strip().lower()
    cluster_name = str(options['cluster_name']).strip()
    volume_choice = ebs_params['volume_choice']
    mount_path = get_volume_mount_path(volume_choice)
    cloud_data_path = mount_path + 'data/'
    cloud_mzid_path = cloud_data_path + raw_end + '/search_results/'
    gpquest_log_filename = 'GPQuest3.log'
    param_json_filename = 'param.json'
    python2_path = os.path.dirname(exes['python']) + '\\'
    starcluster_file = python2_path + 'Scripts\\starcluster.exe'
    
    if modification != 'intact':
        download_title = 'mzid'
    elif modification == 'intact':
        download_title = 'csv/sptxt'
    
    mzml_files_all = glob.glob(mzml_path + '*.mzML')
                
    # only specifies mzML files for database searching on the cloud that have not already been searched locally
    mzml_files = []
    # decides which files to download based on which search engine was used (MS-GF+ vs GPQuest)
    if modification != 'intact':
        for mzml_file in mzml_files_all:
            mzid_name = os.path.basename(mzml_file).replace('.mzML', '.mzid')
            
            if not os.path.exists(mzid_path + mzid_name):
                mzml_files.append(mzml_file)
    elif modification == 'intact':
        for mzml_file in mzml_files_all:
            mzml_basename = os.path.basename(mzml_file)
            csv_name = mzml_basename.replace('.mzML', '.csv')
            sptxt_name = mzml_basename.replace('.mzML', '.sptxt')
            iso_name = mzml_basename.replace('.mzML', '_iso.txt')
            
            if not os.path.exists(mzid_path + csv_name) or not os.path.exists(mzid_path + sptxt_name) or not os.path.exists(mzid_path + iso_name):
                mzml_files.append(mzml_file)
    
    if len(mzml_files) > 0:
        start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name + ' --cluster-size=1 --master-instance-type=t2.micro'
        print start_command
        sys.stdout.flush()
        subprocess.call(start_command)
        
        print '\nRetrieving ' + str(len(mzml_files)) + ' ' + download_title + ' files from the cloud...'
        print '-' * 150
        sys.stdout.flush()
        
        files_len = len(mzml_files)
        
        percent_interval = math.floor(100.0 / files_len)
        if percent_interval < 5.0:
            percent_interval = 5.0
        
        percent_checked = 0.0
        num_files_checked = 0
        
        print 'Cloud ' + download_title + ' download progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
        sys.stdout.flush()
        if modification != 'intact':
            for mzml_file in mzml_files:
                mzid_filename = os.path.basename(mzml_file).replace('.mzML', '.mzid')
                
                # get mzid results from the cluster
                get_mzid_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + mzid_filename + ' ' + mzid_path
                #print get_mzid_command
                #sys.stdout.flush()
                subprocess.call(get_mzid_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = math.floor(current_percent)
                
                    print 'Cloud ' + download_title + ' download progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
        elif modification == 'intact':
            if not os.path.exists(mzid_path + gpquest_log_filename):
                get_gpquest_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + gpquest_log_filename + ' ' + mzid_path
                #print get_gpquest_command
                #sys.stdout.flush()
                subprocess.call(get_gpquest_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
            print ''
            sys.stdout.flush()
            
            if not os.path.exists(mzid_path + param_json_filename):
                get_param_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + param_json_filename + ' ' + mzid_path
                #print get_param_command
                #sys.stdout.flush()
                subprocess.call(get_param_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
            
            print ''
            sys.stdout.flush()
            
            for mzml_file in mzml_files:
                mzml_basename= os.path.basename(mzml_file)
                csv_filename = mzml_basename.replace('.mzML', '.csv')
                sptxt_filename = mzml_basename.replace('.mzML', '.sptxt')
                iso_filename = mzml_basename.replace('.mzML', '_iso.txt')
                
                # get csv results from the cluster
                get_csv_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + csv_filename + ' ' + mzid_path
                #print get_csv_command
                #sys.stdout.flush()
                subprocess.call(get_csv_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                # get sptxt results from the cluster
                get_sptxt_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + sptxt_filename + ' ' + mzid_path
                #print get_sptxt_command
                #sys.stdout.flush()
                subprocess.call(get_sptxt_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                # get iso results from the cluster
                get_iso_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + iso_filename + ' ' + mzid_path
                #print get_iso_command
                #sys.stdout.flush()
                subprocess.call(get_iso_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = math.floor(current_percent)
                
                    print 'Cloud ' + download_title + ' download progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
        if percent_checked < 100.0:
            print 'Cloud ' + download_title + ' download progress... 100 %'
            sys.stdout.flush()
            
        print ''
        sys.stdout.flush()
        
        # terminates cluster that was used to download the mzid files    
        terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
        print terminate_command
        sys.stdout.flush()
        proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
        proc.wait()
        proc.terminate()
    
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Done!', 'The cloud database search result file(s) have been downloaded!')
        root.destroy()
    else:
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Done!', 'All of the .mzML file(s) in the "mzml" folder have corresponding search result files in the "search_results" folder, ' + 
                                        'and so there is no need to download any search result files from the cloud.')
        root.destroy()
    
def get_volume_mount_path(volume_choice):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    volume_section_flag = False
    
    for line in lines:
        line = line.strip()
        
        # specified volume must NOT be commented out
        if line == '[volume ' + volume_choice + ']':
            volume_section_flag = True
            
        if volume_section_flag == True:
            if line[:10] == 'MOUNT_PATH':
                line = line.split('=')[-1].strip()
                mount_path = line
                
                return mount_path
    
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['ebs_params'] = read_new_ebs_config(config_path + 'ebs_params.txt')
    
    main(configs)