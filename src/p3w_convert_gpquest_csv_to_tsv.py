import os
import csv
import sys
import glob
import time
import shutil
from multiprocessing import Pool
from multiprocessing import cpu_count
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'

def main(configs):
    print '\nStep 3 continued: Converting GPQuest database search results to tsv format...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    mzid_path = data_root + 'step3-search_results\\'
    #num_thread = int(cpu_count()) - 1
    
    # limited due to memory constraints on computers and this step is quick regardless
    num_thread = 2
    
    if num_thread == 0:
        num_thread = 1
    
    mzid_files = glob.glob(mzid_path + '*.csv')
    mzid_paths = [mzid_path] * len(mzid_files)
    
    pool = Pool(processes = num_thread)
    pool.map(csv_to_tsv_in_parallel, zip(mzid_files, mzid_paths))
            
def csv_to_tsv_in_parallel((mzid_file, mzid_path)):
    mzid_split = str(mzid_file).strip().split('\\')
    filename = mzid_split[-1].replace('.csv', '.gpquest.tsv')
    
    if not os.path.exists(mzid_path + filename):
        print 'Converting... ' + str(os.path.basename(mzid_file))
        sys.stdout.flush()
        
        with open(mzid_file,'r') as csvin, open(mzid_path + filename, 'w') as tsvout:
            csvin = csv.reader(csvin)
            tsvout = csv.writer(tsvout, delimiter = '\t', lineterminator = '\n')
        
            for row in csvin:
                tsvout.writerow(row)  
    
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['backup'] = True
    
    main(configs)