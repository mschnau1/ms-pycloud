import re
import fasta
import pickle
import peptide_digestion

def build_peptide_to_protein_map(peptide_to_protein_dict, 
                                 peptide_seq_set, 
                                 log_message = {}):
    peptide_to_protein_map = {}
    log_message["peptides_not_in_database"] = []
    for peptide in peptide_seq_set:
        if peptide in peptide_to_protein_dict:
            peptide_to_protein_map[peptide] = peptide_to_protein_dict[peptide]
        else:
            log_message["peptides_not_in_database"].append(peptide)

    print("Number of peptides that did not exist in the database", 
          len(log_message["peptides_not_in_database"]))

    return peptide_to_protein_map

def build_protein_to_peptide_map(peptide_to_protein_map, 
                                 min_num_peptides = 1, 
                                 log_message = {}):
    protein_to_peptide_map = {}
    for peptide in peptide_to_protein_map:
        for protein in peptide_to_protein_map[peptide]:
            if protein in protein_to_peptide_map:
                protein_to_peptide_map[protein].add(peptide)
            else:
                protein_to_peptide_map[protein] = set([peptide])

    proteins_to_remove = []
    for protein in protein_to_peptide_map:
        if len(protein_to_peptide_map[protein]) < min_num_peptides:
            proteins_to_remove.append(protein)

    print("len(proteins_to_remove)", len(proteins_to_remove))
    print("len(protein_to_peptide_map)", len(protein_to_peptide_map))
    import sys
    sys.stdout.flush()

    for protein in proteins_to_remove:
        del protein_to_peptide_map[protein]

    log_message["num_proteins_removed_due_to_min_num_peptides"] = \
        len(proteins_to_remove)

    print("len(protein_to_peptide_map)", len(protein_to_peptide_map))

    return protein_to_peptide_map

def build_metaprotein_to_peptide_map(protein_to_peptide_map):
    metaprotein_to_peptide_map = {}
    str_key_to_metaprotein_map = {}
    for protein in protein_to_peptide_map:
        k = ";".join(sorted(protein_to_peptide_map[protein]))
        if k in str_key_to_metaprotein_map:
            str_key_to_metaprotein_map[k].append(protein)
        else:
            str_key_to_metaprotein_map[k] = [protein]

    for k, v in str_key_to_metaprotein_map.items():
        metaprotein_to_peptide_map[";".join(sorted(v))] = protein_to_peptide_map[v[0]]

    return metaprotein_to_peptide_map




def build_peptide_to_metaprotein_map(metaprotein_to_peptide_map):
    peptide_to_metaprotein_map = {}

    for protein in metaprotein_to_peptide_map:
        for peptide in metaprotein_to_peptide_map[protein]:
            if peptide in peptide_to_metaprotein_map:
                peptide_to_metaprotein_map[peptide].add(protein)
            else:
                peptide_to_metaprotein_map[peptide] = set([protein])

    return peptide_to_metaprotein_map


def get_connected_components_in_bipartite_graph(map1, map2):
    node_list = map1.keys()
    node_type = [1] * len(node_list)
    node_list2 = map2.keys()
    node_type2 = [2] * len(node_list2)
    node_list.extend(node_list2)
    node_type.extend(node_type2)

    nodes = set(zip(node_list, node_type))

    queue = []
    nodes_cc = []
    connected_components = []
    i = 0
    while nodes:
        node, t = nodes.pop()
        queue.append((node, t))
        while queue:
            node, t = queue.pop(0)
            nodes_cc.append((node, t))
            if t == 1:
                for n in map1[node]:
                    if (n, 2) in nodes:
                        queue.append((n, 2))
                        nodes.remove((n, 2))
            else:
                for n in map2[node]:
                    if (n, 1) in nodes:
                        queue.append((n, 1))
                        nodes.remove((n, 1))

        i += 1
        print("Connected component in the bipartite graph: i = ", i)
        connected_components.append(nodes_cc)
        nodes_cc = []
    return connected_components
                
def get_node_lists_in_bipartite_cc(cc):
    node_list1 = []
    node_list2 = []
    for n, t in cc:
        if t == 1:
            node_list1.append(n)
        else:
            node_list2.append(n)

    return (node_list1, node_list2)

def assign_peptide_to_protein(proteins, peptides, 
                              metaprotein_to_peptide_map, 
                              peptide_to_metaprotein_map):

    # proteins and peptides should be in one connected component
    # of the bipartite graph

    peptide_assignment = []

    candidate_proteins = set(proteins)
    selected_proteins = set()

    uncovered_peptides = set(peptides)
    covered_peptides = set()

    # First, pick the unique peptides -- peptides that can only be 
    # covered by one metaprotein
    for peptide in peptides:
        if len(peptide_to_metaprotein_map[peptide]) == 1:
            uncovered_peptides.remove(peptide)
            covered_peptides.add(peptide)
            selected_proteins |= peptide_to_metaprotein_map[peptide]
            peptide_assignment.append((peptide, 
                                       list(peptide_to_metaprotein_map[peptide])[0], 
                                       "Unique"))
    candidate_proteins -= selected_proteins

    # Second, examine currently selected proteins (proteins selected because
    # of having unique peptides) to see what additional peptides can be
    # explained by them. Update covered_peptides and uncovered_peptides; and
    # update peptide_assignment.
    for protein in selected_proteins:
        for peptide in metaprotein_to_peptide_map[protein].intersection(uncovered_peptides):
            peptide_assignment.append((peptide, protein, "UniqueCovered"))
        covered_peptides |= metaprotein_to_peptide_map[protein].intersection(uncovered_peptides)
        uncovered_peptides -= covered_peptides

    # Third, for the remaining uncovered peptides, use a greedy algorithm
    # to pick the protein that cover the most uncovered peptides. Iterate
    # this process until all peptides are covered.
    while uncovered_peptides:
        protein_edge_count = []
        for protein in candidate_proteins:
            protein_edge_count.append( 
                (protein, 
                 len(metaprotein_to_peptide_map[protein].intersection(uncovered_peptides))
                 ))
                
        protein_edge_count = sorted(protein_edge_count, 
                                    key = lambda count: count[1], 
                                    reverse = True)
        p = protein_edge_count[0][0]
        candidate_proteins.remove(p)
        selected_proteins.add(p)
        for peptide in metaprotein_to_peptide_map[p].intersection(uncovered_peptides):
            peptide_assignment.append((peptide, p, "Greedy"))
        covered_peptides |= metaprotein_to_peptide_map[p].intersection(uncovered_peptides)
        uncovered_peptides -= covered_peptides

    peptide_assignment_refined = []
    for peptide, protein, t in peptide_assignment:
        if t == "UniqueCovered" and len(selected_proteins.intersection(peptide_to_metaprotein_map[peptide])) >= 2:
            peptide_assignment_refined.append((peptide, protein, "Multiple"))
        elif t == "Greedy" and len(selected_proteins.intersection(peptide_to_metaprotein_map[peptide])) >= 2:
            peptide_assignment_refined.append((peptide, protein, "MultipleGreedy"))
        else:
            peptide_assignment_refined.append((peptide, protein, t))

    return peptide_assignment_refined

def protein_grouping(
    peptide_set,
    protein_fasta_file, 
    min_peptide_per_protein = 2,
    cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 6, max_length = 50,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True,
    log_message = {}):
    
    peptide_to_protein_dict = \
        peptide_digestion.build_peptide_to_protein_dict_from_fasta( \
        protein_fasta_file, 
        cleavage_rule = cleavage_rule,
        max_miscleavage = max_miscleavage,
        min_length = min_length, 
        max_length = max_length,
        min_cleavage_terminus = min_cleavage_terminus,
        optional_n_term_met_cleavage = optional_n_term_met_cleavage)

    peptide_to_protein_map = \
        build_peptide_to_protein_map(peptide_to_protein_dict, peptide_set, 
                                     log_message)
    
    protein_to_peptide_map = build_protein_to_peptide_map(peptide_to_protein_map, 
                                                          min_peptide_per_protein,
                                                          log_message)
    
    log_message["len_peptide_to_protein_map"] = len(peptide_to_protein_map)
    log_message["len_protein_to_peptide_map"] = len(protein_to_peptide_map)

    print(len(peptide_to_protein_map))
    print(len(protein_to_peptide_map))

    metaprotein_to_peptide_map = build_metaprotein_to_peptide_map(protein_to_peptide_map)
    peptide_to_metaprotein_map = build_peptide_to_metaprotein_map(metaprotein_to_peptide_map)

    connected_components = \
        get_connected_components_in_bipartite_graph(metaprotein_to_peptide_map, 
                                                    peptide_to_metaprotein_map)
    peptide_assignment = []

    j = 0
    for cc in connected_components:
        j += 1
        proteins_in_cc, peptides_in_cc = get_node_lists_in_bipartite_cc(cc)
        peptide_assignment.extend(
            assign_peptide_to_protein(proteins_in_cc, peptides_in_cc, 
                                      metaprotein_to_peptide_map, 
                                      peptide_to_metaprotein_map))
        
    peptides = set()
    proteins = set()
    for s, p, t in peptide_assignment:
        peptides.add(s)
        proteins.add(p)

    log_message["num_unique_peptides"] = len(peptides)
    log_message["num_unique_protein_groups"] = len(proteins)
    
    print("Total number of peptides: ", len(peptides))
    print("Total number of proteins: ", len(proteins))

    return peptide_assignment

def filter_by_psms_per_peptide(peptide_psm_count_map,
                               min_psm_per_peptide = 1):
    peptide_psm_count_map_filtered = {}
    for p in peptide_psm_count_map:
        if peptide_psm_count_map[p] >= min_psm_per_peptide:
            peptide_psm_count_map_filtered[p] = peptide_psm_count_map[p]
    return peptide_psm_count_map_filtered