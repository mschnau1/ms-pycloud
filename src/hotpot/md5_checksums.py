import os
import sys
import glob
import hashlib
from math import floor

def step3_local_mzid_md5_checksums(files, output_path):
    # only checks the mzid files that have been downloaded from the cloud
    print '\nGenerating md5 checksums for the mzid files retrieved from the cloud...'
    print '-' * 150
    sys.stdout.flush()
    
    md5_data = {}
    
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
        
        num_files_checked += 1
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = floor(current_percent)
        
            print 'Checksumming progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()
        
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step3_local_mzid_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()

def step3_local_mzml_md5_checksums(files, output_path):
    # only checks the mzML files that have been selected to be uploaded and searched on the cloud
    print '\nGenerating md5 checksums for the locally stored mzML files...'
    print '-' * 150
    sys.stdout.flush()
    
    md5_data = {}
    
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
        
        num_files_checked += 1
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = floor(current_percent)
        
            print 'Checksumming progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()
        
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step3_local_mzml_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()
        
def step4_search_results_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for search results (mzid/pepxml/xml) files...'
    print '-' * 150
    sys.stdout.flush()

    valid_extensions = ['mzid', 'pepxml', 'pep.xml']

    md5_data = {}
    
    # check for search result files that are not in subdirectories
    path_files = glob.glob(filepath + '*.*')
    files = []
    
    for f in path_files:
        file_ext = str('.'.join(f.split('.')[1:]))
        
        if file_ext in valid_extensions:
            files.append(f)
    
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
               
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step4_search_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()
    
def step4_search_results_gpquest_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for search results (mzid/pepxml/xml) files...'
    print '-' * 150
    sys.stdout.flush()

    valid_extensions = ['gpquest.tsv']

    md5_data = {}
    
    # check for search result files that are not in subdirectories
    path_files = glob.glob(filepath + '*.*')
    files = []
    
    for f in path_files:
        file_ext = str('.'.join(f.split('.')[1:]))
        
        if file_ext in valid_extensions:
            files.append(f)
    
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
               
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step4_search_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()

def step4_psm_quant_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for psm quantitation files...'
    print '-' * 150
    sys.stdout.flush()

    md5_data = {}
    
    # check for psm quantitation files that are not in subdirectories
    files = glob.glob(filepath + '*.txt')
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step4_psm_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()
    
def inf_file_md5_checksum(filename, output_path):
    basename = os.path.basename(filename)
    
    print '\nGenerating md5 checksum for the ' + str(basename) + ' file...'
    print '-' * 150
    sys.stdout.flush()
    
    file_ext = str(basename.split('.')[-1])
    name = basename.replace('.' + file_ext, '')
    
    print 'Checksumming... ' + filename
    sys.stdout.flush()
    
    md5_data = hash_bytestr_iter(file_as_blockiter(open(filename, 'rb')), hashlib.md5())
    
    output_data = ['Filename\tMD5 checksum']
    output_data.append(str(basename) + '\t' + str(md5_data))
        
    f = open(output_path + str(name) + '_md5_checksum.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()

def step5_search_results_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for search results (mzid/pepxml/xml) files...'
    print '-' * 150
    sys.stdout.flush()

    valid_extensions = ['mzid', 'pepxml', 'pep.xml', 'gpquest.tsv']

    md5_data = {}
    
    # check for search result files that are not in subdirectories
    path_files = glob.glob(filepath + '*.*')
    files = []
    
    for f in path_files:
        file_ext = str('.'.join(f.split('.')[1:]))
        
        if file_ext in valid_extensions:
            files.append(f)
    
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step5_search_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()

def step6_psm_quant_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for psm quantitation files...'
    print '-' * 150
    sys.stdout.flush()

    md5_data = {}
    
    # check for psm quantitation files that are not in subdirectories
    files = glob.glob(filepath + '*.txt')
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step6_psm_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()

def step7_protein_quant_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for peptide_protein quantitation files...'
    print '-' * 150
    sys.stdout.flush()

    md5_data = {}
    
    # check for psm quantitation files that are not in subdirectories
    files = glob.glob(filepath + '*.txt')
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step7_protein_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()
    
def step7b_protein_quant_md5_checksums(filepath, output_path, sample_filenames):
    print '\nGenerating md5 checksums for peptide_protein quantitation files...'
    print '-' * 150
    sys.stdout.flush()

    md5_data = {}
    
    # check for psm quantitation files that are not in subdirectories
    files = glob.glob(filepath + '*.txt')
    files_len = len(files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Checksumming progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for f in files:
        filename = os.path.basename(f)
        
        for sample_filename in sample_filenames:
            if sample_filename in filename:
                md5_data[f] = hash_bytestr_iter(file_as_blockiter(open(f, 'rb')), hashlib.md5())
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Checksumming progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                
                break
    
    if percent_checked < 100.0:
        print 'Checksumming progress... 100 %'
        sys.stdout.flush()
    
    output_data = ['Filename\tMD5 checksum']
    for key in sorted(md5_data):
        output_data.append(str(os.path.basename(key)) + '\t' + str(md5_data[key]))
        
    f = open(output_path + 'step7b_protein_md5_checksums.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()
        
def config_file_md5_checksum(filename, output_path):
    basename = os.path.basename(filename)
    
    print '\nGenerating md5 checksum for the ' + str(basename) + ' config file...'
    print '-' * 150
    sys.stdout.flush()
    
    file_ext = str(basename.split('.')[-1])
    name = basename.replace('.' + file_ext, '')
    
    print 'Checksumming... ' + filename
    sys.stdout.flush()
    
    md5_data = hash_bytestr_iter(file_as_blockiter(open(filename, 'rb')), hashlib.md5())
    
    output_data = ['Filename\tMD5 checksum']
    output_data.append(str(basename) + '\t' + str(md5_data))
        
    f = open(output_path + str(name) + '_md5_checksum.tsv', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()
    
    print '-'
    sys.stdout.flush()
    
def raw_file_md5_checksum(filename, output_path):
    basename = os.path.basename(filename)
    
    file_ext = str(basename.split('.')[-1])
    name = basename.replace('.' + file_ext, '')
    
    print 'Checksumming... ' + filename
    sys.stdout.flush()
    
    md5_data = hash_bytestr_iter(file_as_blockiter(open(filename, 'rb')), hashlib.md5())
    
    output_data = ['Filename\tMD5 checksum']
    output_data.append(str(basename) + '\t' + str(md5_data))
        
    f = open(output_path + str(name) + '.md5.cksum', 'w')
    for row in output_data[:-1]:
        f.write(str(row) + '\n')
        
    f.write(str(output_data[-1]))
    
    f.close()

def hash_bytestr_iter(bytesiter, hasher, ashexstr = True):
    for block in bytesiter:
        hasher.update(block)
    return (hasher.hexdigest() if ashexstr else hasher.digest())

def file_as_blockiter(afile, blocksize=65536):
    with afile:
        block = afile.read(blocksize)
        while len(block) > 0:
            yield block
            block = afile.read(blocksize)