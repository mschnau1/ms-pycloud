import os
import sys
import mzid
import fasta
import pepfdr
import protein_identification
from math import floor
from read_write import read_config_param as rcp

# sets the configuration filepath
config_list= os.path.dirname(os.path.abspath(__file__)).split('\\')
del config_list[-1]
config_path = ''
for s in config_list:
    config_path += s +'\\'
config_path += 'configs\\'

def create_reversed_protein_sequence_fasta(protein_fasta_file,
                                           reversed_decoy_fasta_file):
    """Writes a reversed protein sequence fasta file.

    Args:
        protein_fasta_file: protein fasta file.
        reversed_decoy_fasta_file: reversed decoy fasta file.
    """

    data = fasta.read_fasta(protein_fasta_file)

    with open(protein_fasta_file) as f, open(reversed_decoy_fasta_file, "w") as g:
        for info, sequence in data:
            g.write(">rev_" + info + "\n")
            g.write(sequence[::-1] + "\n")

def get_decoy_psms(mzid_file, psm_fdr = 0.01, mod = 'global'):
    """Gets a list of decoy PSMs below given PSM FDR (q-value).

    Args:
        mzid_file: mzid file such as MS-GF+ output.
        psm_fdr: PSM FDR (QValue) threshold.

    Returns:
        A list of decoy PSMs.
    """
    threshold_dict = {}
    mzident = mzid.Mzid(mzid_file)
    psms_results = mzident.psms_at_fdr(fdr = psm_fdr, mod = mod, is_decoy_included = True)
    psms = psms_results[0]
    threshold_dict[os.path.basename(mzid_file)] = psms_results[1]
    
    decoy_psms = [psm for psm in psms if psm[5]]
    return decoy_psms


def get_decoy_peptides(mzid_file, psm_fdr = 0.01, mod = 'global'):
    """Gets a set of decoy peptides below given PSM FDR (q-value).

    Args:
        mzid_file: mzid file such as MS-GF+ output.
        psm_fdr: PSM FDR (QValue) threshold.

    Returns:
        A set of decoy peptides.
    """
    threshold_dict = {}
    mzident = mzid.Mzid(mzid_file)
    psms_results = mzident.psms_at_fdr(fdr = psm_fdr, mod = mod, is_decoy_included = True)
    psms = psms_results[0]
    threshold_dict[os.path.basename(mzid_file)] = psms_results[1]
    
    decoy_peptides = set([psm[1] for psm in psms if psm[5]])
    return decoy_peptides

def get_decoy_peptide_psm_count_map(search_files, search_engine, psm_fdr = 0.01, mod = 'global'):
    """Gets a map of decoy peptides below given PSM FDR (q-value) and their
    PSM counts.

    Args:
        search_file: mzid or pepxml file such as MS-GF+ output.
        psm_fdr: PSM FDR (QValue) threshold.

    Returns:
        A set of decoy peptides.
    """
    psms = []
    threshold_dict = {}
    
    if mod != 'intact':
        if search_engine == 'myrimatch':
            # if myrimatch was the search engine, sets default score used for ranking to 'xcorr'
            score_used_for_ranking = 'XCorr'
            decoy_prefix = rcp(filepath = config_path + 'myrimatch.cfg', param_name = 'DecoyPrefix')
            psms_results = pepfdr.psms_at_fdr(search_files, fdr = psm_fdr, decoy_prefix = decoy_prefix, mod = mod, is_decoy_included = True)   
            
            psms = psms_results[0]
            threshold_dict['all pepxml files'] = psms_results[1]                      
        elif search_engine == 'comet':
            # if comet was the search engine, sets default score used for ranking to 'xcorr'
            score_used_for_ranking = 'XCorr'
            decoy_prefix = rcp(filepath = config_path + 'comet.params', param_name = 'decoy_prefix')
            psms_results = pepfdr.psms_at_fdr(search_files, fdr = psm_fdr, score_used_for_ranking = score_used_for_ranking, decoy_prefix = decoy_prefix, mod = mod, is_decoy_included = True)
            
            psms = psms_results[0]
            threshold_dict['all xml files'] = psms_results[1]
        elif search_engine == 'msgf':
            files_len = len(search_files)
        
            percent_interval = floor(100.0 / files_len)
            if percent_interval < 5.0:
                percent_interval = 5.0
        
            percent_checked = 0.0
            num_files_checked = 0
        
            print 'Reading search_results files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
            sys.stdout.flush()
            for search_file in search_files: 
                num_files_checked += 1
                        
                mzident = mzid.Mzid(search_file)
                psms_results = mzident.psms_at_fdr(fdr = psm_fdr, mod = mod, is_decoy_included = True)
                psms.extend(psms_results[0])
                threshold_dict[os.path.basename(search_file)] = psms_results[1]
                
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Reading search_results files progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                    
            if percent_checked < 100.0:
                print 'Reading search_results files progress... 100 %'
                sys.stdout.flush()
                
            print ''
            sys.stdout.flush()
        else:
            raise Exception('search engine should be msgf, comet, or myrimatch')
    else:
        (psms, n_glyco_psms, o_glyco_psms) = get_gpquest_decoy_psms(search_files, fdr = psm_fdr)
        threshold_dict['all gpquest files'] = {}
        # PSM-level FDR estimation is handled differently between GPQuest and MS-GF+
        threshold_dict['all gpquest files']['all charges'] = 'NA'
        
    peptide_psm_count_map = {}
    for psm in psms:
        if psm[5]:
            peptide = psm[1]
            
            if peptide in peptide_psm_count_map:
                peptide_psm_count_map[peptide] += 1
            else:
                peptide_psm_count_map[peptide] = 1
                
    if mod == 'intact':
        n_glyco_peptide_psm_count_map = {}
        for psm in n_glyco_psms:
            if psm[5]:
                peptide = psm[1]
                
                if peptide in n_glyco_peptide_psm_count_map:
                    n_glyco_peptide_psm_count_map[peptide] += 1
                else:
                    n_glyco_peptide_psm_count_map[peptide] = 1
                    
        o_glyco_peptide_psm_count_map = {}
        for psm in o_glyco_psms:
            if psm[5]:
                peptide = psm[1]
                
                if peptide in o_glyco_peptide_psm_count_map:
                    o_glyco_peptide_psm_count_map[peptide] += 1
                else:
                    o_glyco_peptide_psm_count_map[peptide] = 1
    
    if mod != 'intact':
        return peptide_psm_count_map
    else:
        return (peptide_psm_count_map, n_glyco_peptide_psm_count_map, o_glyco_peptide_psm_count_map)
    
def get_gpquest_decoy_psms(search_files, fdr = 0.01):
    # GPQuest search result data columns to be extracted
    ms2_col = 1
    charge_col = 3
    protein_col = 19
    pep_col = 21
    mod_pep_col = 22
    n_glycan_col = 39
    o_glycan_col = 41
    score_col = 47
    fdr_col = 49
    # number of header rows in GPQuest search result files
    gpquest_header = 1
    
    # missing glycan value in the GPQuest results files
    missing_glycan_flag = 'NA'
    
    files_len = len(search_files)
        
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0

    percent_checked = 0.0
    num_files_checked = 0

    print 'Reading search_results files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    
    data = []
    n_glyco_data = []
    o_glyco_data = []
    for gpquest_f in search_files:
        num_files_checked += 1
        
        f = open(gpquest_f, 'r')
        lines = f.readlines()
        f.close()
        
        basename = os.path.basename(gpquest_f).replace('.gpquest.tsv', '').strip()
        
        for line in lines[gpquest_header:]:
            line = line.strip().split('\t')
            ms2 = int(line[ms2_col].strip())
            charge = int(line[charge_col].strip()) 
            protein = line[protein_col].strip()
            pep = line[pep_col].strip()
            mod_pep = line[mod_pep_col].strip()
            n_glycan = line[n_glycan_col].strip()
            o_glycan = line[o_glycan_col].strip()
            # Morpheus score
            score = float(line[score_col].strip())
            gpquest_fdr = float(line[fdr_col].strip())
            
            spectrum = basename + '.' + str(ms2) + '.' + str(ms2) + '.' + str(charge)
            
            n_glycopeptide = ''
            n_mod_glycopeptide = ''
            if n_glycan != '' and n_glycan != missing_glycan_flag:
                n_glycan = n_glycan.split(':')[0].strip()
                
                n_glycopeptide = pep + '-' + n_glycan
                n_mod_glycopeptide = mod_pep + '-' + n_glycan
             
            o_glycopeptide = ''
            o_mod_glycopeptide = ''   
            if o_glycan != '' and o_glycan != missing_glycan_flag:
                o_glycan = o_glycan.split(':')[0].strip()
                
                o_glycopeptide = pep + '-' + o_glycan
                o_mod_glycopeptide = mod_pep + '-' + o_glycan
            
            # keep the same formatting as MS-GF+ psm quantitation files
            if protein[-1] == ';':
                protein = protein[:-1]
            
            if protein.lower() == 'decoy' and gpquest_fdr <= fdr:
                is_decoy = True
                output_row = (ms2, pep, mod_pep, score, protein, is_decoy, spectrum)
                
                data.append(output_row)
                
                if n_glycopeptide != '':
                    n_output_row = (ms2, n_glycopeptide, n_mod_glycopeptide, score, protein, is_decoy, spectrum)
                    
                    n_glyco_data.append(n_output_row)
                    
                if o_glycopeptide != '':
                    o_output_row = (ms2, o_glycopeptide, o_mod_glycopeptide, score, protein, is_decoy, spectrum)
                    
                    o_glyco_data.append(o_output_row)
                
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = floor(current_percent)
        
            print 'Reading search_results files progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()
    
    if percent_checked < 100.0:
        print 'Reading search_results files progress... 100 %'
        sys.stdout.flush()
        
    print ''
    sys.stdout.flush()
            
    return (data, n_glyco_data, o_glyco_data)
    