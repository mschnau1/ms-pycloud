from __future__ import division

import os
import sys
import n_glycopeptide as glycopeptide
import phosphopeptide
from math import floor
from pyteomics import pepxml

def get_top_rank_psms_from_pepxml(pepxml_file, 
                                  score_used_for_ranking,
                                  decoy_prefix = "rev_",
                                  decoy_suffix = ":reversed"):
    reader = pepxml.read(pepxml_file)
    top_rank_psms = []
    for psm in reader:
        end_scan = psm["end_scan"]
        spectrum = psm["spectrum"]
        charge = psm["assumed_charge"]
        
        if "search_hit" in psm:
            for hit in psm["search_hit"]:
                if hit["hit_rank"] == 1:
                    peptide = hit["peptide"]
                    score = hit["search_score"][score_used_for_ranking]
                    protein_list = hit["proteins"]
                    modified_peptide = hit["modified_peptide"]
                    proteins = []
                    is_decoy = False
                    for protein in protein_list:
                        protein_name = protein["protein"]
                        proteins.append(protein_name)
                        if decoy_prefix and (decoy_prefix in protein_name):
                            is_decoy = True
                        if decoy_suffix and (decoy_suffix in protein_name):
                            is_decoy = True
                        if ("protein_descr" in protein) and \
                                protein["protein_descr"] and decoy_prefix and \
                                (decoy_prefix in protein["protein_descr"]):
                            is_decoy = True
                        if ("protein_descr" in protein) and \
                                protein["protein_descr"] and decoy_suffix and \
                                (decoy_suffix in protein["protein_descr"]):
                            is_decoy = True
                            
                    top_rank_psms.append((end_scan, peptide, modified_peptide,
                                        score, proteins, is_decoy, spectrum, charge))
    return top_rank_psms
    

def sort_peptides_by_score(top_rank_psms, descending = True):
    peptide_score_map = {}
    for end_scan, peptide, modified_peptide, score, \
            proteins, is_decoy, spectrum in top_rank_psms:
        if peptide in peptide_score_map:
            if descending and score > peptide_score_map[peptide][0]:
                peptide_score_map[peptide] = (score, is_decoy)
            if (not descending) and score < peptide_score_map[peptide][0]:
                peptide_score_map[peptide] = (score, is_decoy)
        else:
            peptide_score_map[peptide] = (score, is_decoy)
    peptide_score_list = []
    for k, v in peptide_score_map.items():
        peptide_score_list.append((k, v[0], v[1]))
    return sorted(peptide_score_list, key = lambda x: x[1], reverse=descending)

def sort_psms_by_score(top_rank_psms, descending = True):
    psm_score_list = []
    for end_scan, peptide, modified_peptide, score, \
            proteins, is_decoy, spectrum in top_rank_psms:
        psm_score_list.append((peptide, score, is_decoy))
    return sorted(psm_score_list, key = lambda x: x[1], reverse=descending)


def calculate_score_fdr(sorted_peptides_by_score):
    num_true_hit = 0
    num_decoy_hit = 0
    score_fdr = []
    for peptide, score, is_decoy in sorted_peptides_by_score:
        if is_decoy:
            num_decoy_hit += 1
        else:
            num_true_hit += 1
        
        try:
            score_fdr.append((score, float(num_decoy_hit) / float(num_true_hit)))
        except ZeroDivisionError:
            pass
                    
    return score_fdr

def calculate_threshold_at_fdr(score_fdr, fdr = 0.01):
    threshold = None
    num_to_skip = int(min(2 / fdr, len(score_fdr) / 10))
    # FDR estimation at the beginning of list 
    # may not be accurate if there is at least 
    # one decoy near the top of the list.
    # One workaround is to skip a fraction of the list.
    i = 0
    for s, f in score_fdr:
        if f > fdr and i >= num_to_skip:
            break
        else:
            threshold = s
        i += 1
    if i == num_to_skip:
        for s, f in score_fdr:
            if f > fdr:
                break
            else:
                threshold = s

    return threshold

def psms_at_fdr(pepxml_files, fdr = 0.01, 
                score_used_for_ranking = "mvh",
                decoy_prefix = "rev_", 
                decoy_suffix = ":reversed", 
                is_score_larger_better = True,
                mod = 'global',
                is_decoy_included = False):
    
    if mod == 'phospho':
        step3_config = os.path.dirname(pepxml_files[0]) + '\\step3.config'
        mod_peps = phosphopeptide.get_phospho_mass(step3_config)
    
    files_len = len(pepxml_files)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0

    percent_checked = 0.0
    num_files_checked = 0

    print 'Reading search_results files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    
    top_rank_psms = []
    for f in pepxml_files:
        num_files_checked += 1
        
        a = get_top_rank_psms_from_pepxml(pepxml_file = f, 
                                          score_used_for_ranking = score_used_for_ranking,
                                          decoy_prefix = decoy_prefix,
                                          decoy_suffix = decoy_suffix)
        top_rank_psms.extend(a)
        
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = floor(current_percent)
        
            print 'Reading search_results files progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()

    if percent_checked < 100.0:
        print 'Reading search_results files progress... 100 %'
        sys.stdout.flush()
        
    print ''
    sys.stdout.flush()
    
    # filter the psm's into groups based on their charge
    charge_psm_map = {}
    for end_scan, peptide, modified_peptide, score, proteins, is_decoy, spectrum, charge in top_rank_psms:
        if charge in charge_psm_map:
            charge_psm_map[charge].append((end_scan, peptide, modified_peptide, score, proteins, is_decoy, spectrum))
        else:
            charge_psm_map[charge] = []
            charge_psm_map[charge].append((end_scan, peptide, modified_peptide, score, proteins, is_decoy, spectrum))
    
    # calculate minimum peptide level score for each of the psm charge groups such that each group's fdr is below the specified fdr value    
    psms = []
    threshold_dict = {}
    for charge in charge_psm_map:
        charge_psms = charge_psm_map[charge]
        #b = sort_peptides_by_score(top_rank_psms = charge_psms, descending = is_score_larger_better)
        b = sort_psms_by_score(top_rank_psms = charge_psms, descending = is_score_larger_better)

        score_fdr = calculate_score_fdr(b)
        threshold = calculate_threshold_at_fdr(score_fdr, fdr)
        threshold_dict[charge] = threshold
        
        for end_scan, peptide, modified_peptide, \
                score, proteins, is_decoy, spectrum in charge_psms:
            if mod == 'glyco':
                # filter by motif later
                if not is_decoy_included:
                    #is_glyco = glycopeptide.has_motif(peptide)
                    # includes all psms for purposes of identification numbers
                    is_glyco = True
                else:
                    #is_glyco = glycopeptide.has_motif(peptide)
                    # includes all decoy psms as decoys, not just decoy glyco psms when doing decoy estimation
                    is_glyco = True
                
                if is_glyco:
                    if is_score_larger_better:
                        if score >= threshold and (not is_decoy):
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
                        if score >= threshold and is_decoy_included and is_decoy:
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
                    else:
                        if score <= threshold and (not is_decoy):
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
                        if score <= threshold and is_decoy_included and is_decoy:
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))        
            elif mod == 'phospho':
                # filter by motif later
                if not is_decoy_included:
                    #is_phospho = phosphopeptide.has_motif(modified_peptide, mod_peps)
                    # includes all psms for purposes of identification numbers
                    is_phospho = True
                else:
                    #is_phospho = phosphopeptide.has_motif(modified_peptide, mod_peps)
                    # includes all decoy psms as decoys, not just decoy phospho psms when doing decoy estimation
                    is_phospho = True
                                
                if is_phospho:
                    if is_score_larger_better:
                        if score >= threshold and (not is_decoy):
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
                        if score >= threshold and is_decoy_included and is_decoy:
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
                    else:
                        if score <= threshold and (not is_decoy):
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
                        if score <= threshold and is_decoy_included and is_decoy:
                            psms.append((end_scan, peptide, modified_peptide, \
                                            score, proteins, is_decoy, spectrum))
            else:
                if is_score_larger_better:
                    if score >= threshold and (not is_decoy):
                        psms.append((end_scan, peptide, modified_peptide, \
                                        score, proteins, is_decoy, spectrum))
                    if score >= threshold and is_decoy_included and is_decoy:
                        psms.append((end_scan, peptide, modified_peptide, \
                                        score, proteins, is_decoy, spectrum))
                else:
                    if score <= threshold and (not is_decoy):
                        psms.append((end_scan, peptide, modified_peptide, \
                                        score, proteins, is_decoy, spectrum))
                    if score <= threshold and is_decoy_included and is_decoy:
                        psms.append((end_scan, peptide, modified_peptide, \
                                        score, proteins, is_decoy, spectrum))
    
    # set this value whenever the scoring parameter is changed
    # NOTE: Excel will interpret leading '-' or '=' characters as a formula, so in those cases make the leading character a blank space
    score_eq = str(score_used_for_ranking)
    
    return (psms, threshold_dict, score_eq)