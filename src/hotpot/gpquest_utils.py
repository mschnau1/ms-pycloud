from pyteomics import mzml
from tqdm import tqdm
import pandas as pd
import numpy as np
import re, os, sys
from joblib import Parallel, delayed
from sklearn.linear_model import LogisticRegression
import json
import pyopenms

import itertools as it
from collections import Counter
from glypy import monosaccharides
from pyteomics import mass

mass_d = {'aa_comp': dict(mass.std_aa_comp),
          'mod_comp': {},
          'gly_comp': {},
          'mic': {}}

try:
    mass_d['mod_comp']['cam'] = mass.Composition({'H': 3, 'C': 2, 'N': 1, 'O': 1})
    mass_d['mod_comp']['ox'] = mass.Composition({'O': 1})
    mass_d['mod_comp']['tmt10'] = mass.Composition({'H': 20, 'C': 8, 'C[13]': 4, 'N': 1, 'N[15]': 1, 'O': 2})
    mass_d['mod_comp']['p'] = mass.Composition('HPO3')
    mass_d['mod_comp']['tmt11'] = mass.Composition({'H': 20, 'C': 8, 'C[13]': 4, 'N': 1, 'N[15]': 1, 'O': 2})
    mass_d['mod_comp']['tmt16'] = mass.Composition({'H': 25, 'C': 8, 'C[13]': 7, 'N': 1, 'N[15]': 2, 'O': 3})
    mass_d['mod_comp']['itraq4'] = mass.Composition({'H': 12, 'C': 4, 'C[13]': 3, 'N': 1, 'N[15]': 1, 'O': 1})
    mass_d['mod_comp']['itraq8'] = mass.Composition({'H': 24, 'C': 7, 'C[13]': 7, 'N': 3, 'N[15]': 1, 'O': 3})

    mass_d['gly_comp']['N'] = mass.Composition({'H': 13, 'C': 8, 'N': 1, 'O': 5})
    mass_d['gly_comp']['H'] = mass.Composition({'H': 10, 'C': 6, 'O': 5})
    mass_d['gly_comp']['F'] = mass.Composition({'H': 10, 'C': 6, 'O': 4})
    mass_d['gly_comp']['S'] = mass.Composition({'H': 17, 'C': 11, 'N': 1, 'O': 8})
    mass_d['gly_comp']['G'] = mass.Composition({'H': 17, 'C': 11, 'N': 1, 'O': 9})

except:
    db = mass.Unimod()
    mass_d['mod_comp']['cam'] = db.by_title('Carbamidomethyl')['composition']
    mass_d['mod_comp']['ox'] = db.by_title('Oxidation')['composition']
    mass_d['mod_comp']['tmt10'] = db.by_title('TMT6plex')['composition']
    mass_d['mod_comp']['tmt11'] = db.by_title('TMT6plex')['composition']
    mass_d['mod_comp']['tmt16'] = db.by_title('TMTpro')['composition']
    mass_d['mod_comp']['itraq4'] = db.by_title('iTRAQ4plex')['composition']
    mass_d['mod_comp']['itraq8'] = db.by_title('iTRAQ8plex')['composition']

mass_d['mic']['H2O'] = mass.calculate_mass(formula='H2O')
mass_d['mic']['GN'] = monosaccharides["GlcNAc"].mass() - mass_d['mic']['H2O']
mass_d['mic']['GNF'] = monosaccharides["GlcNAc"].mass() + monosaccharides["Fucose"].mass() - mass_d['mic']['H2O'] * 2
mass_d['mic']['GN2'] = (monosaccharides["GlcNAc"].mass() - mass_d['mic']['H2O']) * 2

mod_d = {
    'TMT10plex': 'tmt10',
    'Carbamidomethyl': 'cam',
    'Oxidation': 'ox',
    'TMT11plex': 'tmt11',
    'TMT16plex': 'tmt16',
    'iTRAQ4plex': 'itraq4',
    'iTRAQ8plex': 'itraq8'
}

def hello():
    print('Hi')

def ppm2diff(v, ppm):
    diff = v * ppm * 1e-6
    return diff


def get_isotope_peaks(scan, pmz, pch, mz_max, mz_min, ms1_d, ms1_scans,
                      n=1, isotope_peaks_num=5, bin_width=0.01, ppm=20):
    mz_arr_list = []
    bin_num = int((mz_max - mz_min) / bin_width) + 1
    i_arr_list = np.zeros(bin_num)
    mz_arr_list = list(range(bin_num + 1))

    scan_index = ms1_scans.index(scan)

    pmz_list = [pmz + i * 1.00728 / pch for i in range(-isotope_peaks_num, isotope_peaks_num + 1)]

    scan_index_list = [i for i in range(scan_index - n, scan_index + 1 + n) if 0 <= i < len(ms1_scans)]
    for si in scan_index_list:
        scan = ms1_scans[si]
        mz_arr = ms1_d.get(scan)['mz_array']
        i_arr = ms1_d.get(scan)['intensity_array']
        x = np.where((mz_arr > mz_min) & (mz_arr < mz_max))
        mz_arr2 = list(mz_arr[x])
        i_arr2 = list(i_arr[x])

        for i, j in zip(mz_arr2, i_arr2):
            bin_index = int((i - mz_min) / bin_width)
            i_arr_list[bin_index] += j
    mz_arr_list = np.array([mz_min + i * bin_width for i in mz_arr_list])
    i_arr_list = np.array(i_arr_list)

    i_list = []
    for i in pmz_list:
        diff = ppm2diff(i, ppm)
        x = np.where((mz_arr_list < i + diff) & (mz_arr_list > i - diff))
        y = np.sum(i_arr_list[x])
        i_list.append(y)

    iso_peaks = dict(mz_array=pmz_list, intensity_array=i_list)
    return iso_peaks


def read_param(param_path, **kwargs):
    # check param path
    if not param_path:
        param_path = "template_param.json"
        # param_path = "I:\\param\\glycositesatlas_iTRAQ_GPQuest3Test_WangQiong.json"
    print("Preparation: Read Parameters from {0}".format(param_path))

    # load param into memoyr
    param = json.load(open(param_path, "r"))
    from shutil import copyfile
    output_dir = kwargs.get("output")
    copyfile(param_path, output_dir + os.sep + "param.json")

    # setup input files
    input = kwargs.get('input', [])
    param['INPUT'] = {'FILENAMES': input}

    # setup output files
    output = kwargs.get("output", "")
    param['OUTPUT'] = {'PATH': output}

    # setup  predictor of glycan type
    param["_predictor_ions"] = ["HexNAc", "HexNAc_Marker_C7H7O2N1", "UNKNOWN1",
                                "Hex", "HexNAc-2H2O", "HexNAc-H2O", "NeuAc-H2O",
                                "NeuAc", "UNKNOWN2", "Hex+HexNAc"]
    predictor = LogisticRegression(C=1e5)
    predictor.coef_ = np.array([[9.924611769338409,
                                 2.009884121447966,
                                 -12.735237731400312,
                                 7.120259224711610,
                                 -0.104179855754817,
                                 -28.145177207738801,
                                 0.173158320996686,
                                 -5.207913771035063,
                                 6.176866571930246,
                                 1.284869889886792]])
    predictor.intercept_ = 0
    param["GLYCAN_TYPE_PREDICTOR"] = predictor

    # read mass list
    # mass = param["MASS"]
    # mass["AA"]["D"] += 59.0609
    # mass["AA"]["E"] += 59.0609
    # mass["GLYCAN"]["S"] += 59.0609
    # mass["GLYCAN"]["G"] += 59.0609

    # make list of oxonium ions
    ess = param["PREPROCESS"]["OXONIUM_IONS"]["ESS_IONS"]
    ess_mz_list = np.array([param["MASS"]["OXONIUM_IONS"][i] for i in ess], dtype=float)
    param["_ess_mz_order"] = np.argsort(ess_mz_list)
    param["_sorted_ess_mz_list"] = ess_mz_list[param["_ess_mz_order"]]

    nss = param["PREPROCESS"]["OXONIUM_IONS"]["NON_ESS_IONS"]
    nss_mz_list = np.array([param["MASS"]["OXONIUM_IONS"][i] for i in nss], dtype=float)
    param["_nss_mz_order"] = np.argsort(nss_mz_list)
    param["_sorted_nss_mz_list"] = nss_mz_list[param["_nss_mz_order"]]

    param["_oxoall"] = ess + nss

    # tolerance
    ms1_ppm = param["SEARCH"]["MASS_TOLERANCE"]["MS1(PPM)"]
    param["_ms1_tol_factor"] = float(ms1_ppm) / 1000000
    ms2_ppm = param["SEARCH"]["MASS_TOLERANCE"]["MS2(PPM)"]
    param["_ms2_tol_factor"] = float(ms2_ppm) / 1000000
    param["_ms1_tol_factor2"] = 3 * param["_ms1_tol_factor"]
    param["_ms2_tol_min"] = float(param["SEARCH"]["MASS_TOLERANCE"]["MIN"])
    param["_mH"] = float(param["MASS"]["MIC"]["H+"])
    param["_mH2O"] = float(param["MASS"]["MIC"]["H2O"])
    param["_mNH2"] = float(param["MASS"]["MIC"]["NH2"])
    param["_b_mz"] = param["_mH"]
    param["_y_mz"] = param["_mH2O"] + param["_mH"]
    param["_c_mz"] = param["_mNH2"] + param["MASS"]["MIC"]["1H"] + param["_mH"]
    # z_mz = 0 - mNH2 + (mH2O - mass["MIC"]["1H"]) + mH
    param["_z_mz"] = param["_y_mz"] - 17.02546

    # mod map
    param["_mods_map"] = {}
    for mod in param["SEARCH"]["MODIFICATIONS"]["mods"]:
        x = re.split(",", mod)
        param["_mods_map"][x[5]] = (x[1], x[4])

    param["SEARCH"]["MODIFICATIONS"]["ADDCUT"] = {
        4: {
            "mass": [1.00728 * 4],
            "name": ["4H"]
        },
        3: {
            "mass": [1.00723 * 3, 8.33459 * 3, 15.66191 * 3, 22.98922 * 3],
            "name": ["3H", "2H+Na", "H+2Na", "3Na"]
        },
        2: {
            "mass": [1.00728 * 2, 11.99825 * 2, 22.98922 * 2],
            "name": ["2H", "H+Na", "2Na"]
        }
    }

    for mod in param["SEARCH"]["MODIFICATIONS"]["glycan_mods"]:
        x = re.split(",", mod)
        tg = x[1]
        ng = x[5]
        param["MASS"]["GLYCAN"][ng] = param["MASS"]["GLYCAN"][tg] + float(param["MASS"]["MOD"][x[4]])


    return param


# mzML parsers
def get_mzml_scan(spec):
    scan_id = spec['id']
    m = re.search(r'scan=(\d+)', scan_id)
    if m:
        scan = int(m.group(1))
        return scan
    else:
        return None


def get_mzml_rt_time(spec):
    try:
        return float(spec['scanList']['scan'][0]['scan start time'])
    except KeyError:
        return None


def get_mzml_parent_scan(spec):
    m = re.search(r'scan=(\d+)', spec['precursorList']['precursor'][0]['spectrumRef'])
    if m:
        return int(m.group(1))
    else:
        return None


def get_mzml_precursor(spec):
    # precursor = GPQuestPrecursor()
    precursor = {}
    p = spec['precursorList']['precursor'][0]['selectedIonList']['selectedIon'][0]
    precursor['rt_time'] = get_mzml_rt_time(spec)
    precursor['mz'] = float(p['selected ion m/z']) if 'selected ion m/z' in p else None
    precursor['intensity'] = float(p['peak intensity']) if 'peak intensity' in p else None
    precursor['charge'] = int(p['charge state']) if 'charge state' in p else None
    # precursor['scan'] = get_mzML_scan(spec)
    precursor['scan'] = get_mzml_parent_scan(spec)
    return precursor


def mzml2spectra(mzml_path, **kwargs):
    spectra = mzml.read(mzml_path)
    rows = []
    for spec in tqdm(spectra):
        scan = get_mzml_scan(spec)
        ms_level = int(spec['ms level'])
        rt_time = get_mzml_rt_time(spec)
        # mz_array = ','.join(['{0:.4f}'.format(i) for i in spec['m/z array']])
        # intensity_array = ','.join(['{0:.4f}'.format(i) for i in spec['intensity array']])
        mz_array = spec['m/z array']
        intensity_array = spec['intensity array']
        precursor = {}

        if ms_level == 2:
            # precursor = spec['precursorList']['precursor'][0]['selectedIonList']['selectedIon'][0]
            precursor = get_mzml_precursor(spec)
        row = [
            scan, ms_level, rt_time, mz_array, intensity_array,
            precursor.get('scan'), precursor.get('rt_time'), precursor.get('charge'),
            precursor.get('mz'), precursor.get('intensity')
        ]
        rows.append(row)
    df = pd.DataFrame(rows, columns=[
        'scan', 'ms_level', 'rt_time', 'mz_array', 'intensity_array',
        'precursor_scan', 'precursor_rt_time', 'precursor_charge',
        'precursor_mz', 'precursor_intensity'
    ])

    to_str = kwargs.get('to_str', True)
    if to_str:
        data_grouped = df.groupby(df.index)

        def key_func(subset):
            subset['mz_array'] = subset['mz_array'].apply(lambda i: ','.join(['{0:.4f}'.format(k) for k in i]))
            subset['intensity_array'] = subset['intensity_array'].apply(
                lambda i: ','.join(['{0:.4f}'.format(k) for k in i]))
            return subset

        n_cores = kwargs.get('n_cores', 2)
        results = Parallel(n_jobs=n_cores, max_nbytes=None)(
            delayed(key_func)(group) for name, group in tqdm(data_grouped))
        df = pd.concat(results)
    return df


def calc_isotope_peaks(df, **kwargs):
    ms2toms1_map = {}
    ms1toms2_map = {}
    ms1_scans = []
    ms2_scans = []
    cur_ms1 = 0
    print('Build scan mapping...')
    for index, row in tqdm(df.iterrows()):
        scan = row['scan']
        if row['ms_level'] == 1:
            cur_ms1 = scan
            ms1toms2_map[scan] = []
            ms1_scans.append(scan)
        elif row['ms_level'] == 2:
            ms1toms2_map[cur_ms1].append(scan)
            ms2toms1_map[scan] = cur_ms1
            ms2_scans.append(scan)

    print('Build MS1 array mapping...')
    ms1_d = {}
    for index, row in tqdm(df.iterrows()):
        scan = row['scan']
        if row['ms_level'] == 1:
            ms1_d[scan] = dict(mz_array=row['mz_array'],
                               intensity_array=row['intensity_array'])
    df2 = df[df['ms_level'] == 2].copy(deep=True)
    df2['isotope_peaks'] = df2.apply(lambda i: get_isotope_peaks(ms2toms1_map.get(i['scan']),
                                                                 pmz=i['precursor_mz'],
                                                                 pch=i['precursor_charge'],
                                                                 mz_max=i['precursor_mz'] + 5,
                                                                 mz_min=i['precursor_mz'] - 5,
                                                                 ms1_d=ms1_d, ms1_scans=ms1_scans,
                                                                 n=10, isotope_peaks_num=5, bin_width=0.01,
                                                                 ppm=20), axis=1)

    df2['iso_mz_array'] = [i['mz_array'] for i in df2['isotope_peaks']]
    df2['iso_i_array'] = [i['intensity_array'] for i in df2['isotope_peaks']]
    df2.drop(['isotope_peaks'], axis=1, inplace=True)
    return df2

    # rows = []
    # for index, row in tqdm(df.iterrows()):
    #     if row['ms_level'] == 2:
    #         scan = row['scan']
    #         pch = row['precursor_charge']
    #         pmz = row['precursor_mz']
    #
    #         mz_min = pmz - 5
    #         mz_max = pmz + 5
    #         ms1_scan = ms2toms1_map.get(scan)
    #         row['iso_mz_arr'], row['iso_i_arr'] = get_isotope_peaks(ms1_scan, pmz, pch, mz_max, mz_min,
    #                                                                 ms1_d=ms1_d, ms1_scans=ms1_scans,
    #                                                                 n=10, isotope_peaks_num=5, bin_width=0.01, ppm=20)
    #         rows.append(row)


def choose_glycan(glycans, oxo_ions, seq, mods, scan, conn, **kwargs):
    # print(glycans)
    glycans = [re.split(':', i) for i in re.split('/', glycans)]
    oxo_d = parse_oxo(oxo_ions)
    glycans = [i + [oxo_fit_glycan(oxo_d, parse_glycan(i[0]))] for i in glycans]

    iso_scores = [calc_iso_score(seq, mods, glycan[0], scan, conn, **kwargs) for glycan in glycans]
    # NOTE:: 0: name, 1:mass,2:charge,3:shift,4: fit_sia
    # 5:iso_score, 6:iso_hits, 7:th_pmz, 8:pmz, 9:pmz_offset, 10:i, 11:i/np.max(spec_peaks['i_arr'])
    glycans = [list(i) + list(j) for i, j in zip(glycans, iso_scores)]
    # print(glycans)
    # print(scan)
    if len(glycans) == 1:
        res_str = '/'.join([','.join([str(k) for k in i]) for i in glycans])
        return res_str, glycans[0]
    # sort by i[4] (glycan priority) i[5] (iso_score)
    glycans = sorted(glycans, key=lambda i: (i[4], i[5]), reverse=True)
    fit_glycans = [i for i in glycans if i[11] > 0.05]
    # fit_glycans = glycans
    if len(fit_glycans) == 0:
        res_str = '/'.join([','.join([str(k) for k in i]) for i in glycans])
        return res_str, glycans[0]
    else:
        res_str = '/'.join([','.join([str(k) for k in i]) for i in glycans])
        return res_str, fit_glycans[0]


def parse_oxo(oxo_ions):
    oxo_ions = [re.split('/', i) for i in re.split(';', oxo_ions)]
    oxo_d = dict([(i[0], float(i[-1])) for i in oxo_ions])
    return oxo_d


def parse_modx(a):
    m = re.search('([A-Znc])\[(.+?)\]', a)
    if m:
        return m.group(1), m.group(2)
    else:
        return a, None


def parse_glycan(glycan):
    m = re.finditer('([A-Z])(\d+)|([A-Z]\[.+?\])(\d+)', glycan)
    if m:
        d = dict([(i.group(1), int(i.group(2))) for i in m])
        return d
    return None


def parse_peptide(pep):
    m = re.finditer("[A-Znc]\[.+?\]|[A-Z]", pep)
    m = [parse_modx(pep[i.start():i.end()]) for i in m]
    if m[0][0] != 'n':
        m = [('n', None)] + m
    if m[-1][0] != 'c':
        m += [('c', None)]
    seq = ''.join([i[0] for i in m if i[0] not in ['n', 'c']])
    mods = '/'.join(['{0},{1},{2}'.format(i, m[i][0], mod_d[m[i][1]]) for i in range(len(m)) if m[i][1]])
    return seq, mods


# def oxo_fit_sia(oxo_d, glycan_d, min_i=10000):
#     oxo_has_sia1 = has_exact_monosaccharide(oxo_d, glycan_d, name='NeuAc', min_i=min_i)
#     oxo_has_sia2 = has_monosaccharide(oxo_d, glycan_d, name='NeuAc', min_i=1000)
#     s = glycan_d['S'] > 0
#
#     return (s and oxo_has_sia2) | (not s and not oxo_has_sia1)


def oxo_fit_sia(oxo_d, glycan_d, upper=10000, lower=1000):
    priority = 0
    oxo_sia_lower = has_monosaccharide(oxo_d, glycan_d, name='NeuAc', min_i=lower)
    oxo_sia_upper = has_monosaccharide(oxo_d, glycan_d, name='NeuAc', min_i=upper)
    s = glycan_d['S'] > 0
    if s:
        if oxo_sia_upper:
            priority = 2
        else:
            if oxo_sia_lower:
                priority = 1
            else:
                priority = 0
    else:
        if oxo_sia_upper:
            priority = 0
        else:
            if oxo_sia_lower:
                priority = 1
            else:
                priority = 2
    return priority


def oxo_fit_glycan(oxo_d, glycan_d, upper=10000, lower=1000):
    return oxo_fit_sia(oxo_d, glycan_d, upper=upper, lower=lower)


def has_monosaccharide(oxo_d, glycan_d, name, min_i=10000):
    for i in oxo_d:
        if re.search(name, i):
            if oxo_d[i] > min_i:
                return True
    return False


def has_exact_monosaccharide(oxo_d, glycan_d, name, min_i=10000):
    if oxo_d.get(name) > min_i:
        return True
    else:
        return False


def calc_counter_comp(counter, comp_map):
    comp = mass.Composition()
    for i in counter:
        comp += comp_map[i] * int(counter[i])
    return comp


def calc_glycan_comp(glycan, comp_map, residue=True):
    if residue:
        return calc_counter_comp(glycan_counter(glycan), comp_map=comp_map)
    else:
        return calc_counter_comp(glycan_counter(glycan), comp_map=comp_map) + mass.Composition({'H': 2, 'O': 1})


def glycan_counter(glycan):
    return dict([(i.group(1), int(i.group(2))) for i in re.finditer('([A-Z]|[A-Z]\[.+?\])(\d+)', glycan)])


def iso_comp_counter(comp):
    return glycan_counter(comp)


def mods_counter(mods):
    return Counter([re.split(',', i)[-1] for i in re.split('/', mods)])


def calc_seq_comp(seq, comp_map, residue=True):
    if residue:
        return calc_counter_comp(Counter(list(seq)), comp_map)
    else:
        return calc_counter_comp(Counter(list(seq)), comp_map) + mass.Composition({'H': 2, 'O': 1})


def calc_mod_comp(mods, comp_map):
    return calc_counter_comp(mods_counter(mods), comp_map)


def calc_pep_comp(seq, mods, aa_comp_map, mod_comp_map, residue=True):
    x = calc_seq_comp(seq, aa_comp_map, residue=residue)
    if mods == "" or pd.isnull(mods):
        return x
    else:
        return x + calc_mod_comp(mods, mod_comp_map)


def comp_to_str(comp):
    return ''.join(['{}{}'.format(i, comp[i]) for i in comp])


def calc_iso_dist(comp, **kwargs):
    filter_iso = kwargs.get('filter_iso', True)
    formula = comp
    if filter_iso:
        comp_d = iso_comp_counter(comp)
        formula = ''.join(['{}{}'.format(i, comp_d[i]) for i in comp_d if not re.search('\[', i)])
    N = kwargs.get('N', 5)
    wm = pyopenms.EmpiricalFormula(formula)
    isotopes = wm.getIsotopeDistribution(pyopenms.CoarseIsotopePatternGenerator(N))
    return np.array([float(i.getIntensity()) for i in isotopes.getContainer()], dtype=float)


def calc_iso_score(seq, mods, glycan, scan, conn=None, **kwargs):
    gly_comp = mass_d['gly_comp']
    aa_comp = mass_d['aa_comp']
    mod_comp = mass_d['mod_comp']
    pep_res_comp = calc_pep_comp(seq, mods, aa_comp_map=aa_comp, mod_comp_map=mod_comp)

    g_res_comp = calc_glycan_comp(glycan, comp_map=gly_comp)
    gp_comp = pep_res_comp + g_res_comp + mass.Composition({'H': 2, 'O': 1})

    n = kwargs.get('n', 5)
    gp_iso_dist = calc_iso_dist(comp_to_str(gp_comp), N=n)
    gp_mass = mass.calculate_mass(gp_comp)

    if conn is None:
        pmz = kwargs.get('pmz')
        pch = kwargs.get('pch')
        spec_peaks = kwargs.get('spec_peaks')
    else:
        table_name = kwargs.get('table_name', 'spectra_iso')
        key_col = kwargs.get("key_col", 'scan')
        pmz_col = kwargs.get("pmz_col", 'precursor_mz')
        pch_col = kwargs.get("pch_col", 'precursor_charge')
        mz_col = kwargs.get("mz_col", 'isotope_mz_array')
        i_col = kwargs.get("i_col", 'isotope_i_array')
        spec_peaks, pmz, pch = get_spec_iso_peaks(scan, conn, table_name, key_col,
                                                  pmz_col, pch_col, mz_col, i_col)
    
    # make sure pmz is a floating point number
    pmz = float(pmz)
    
    gp_peaks = dict(mz_arr=[i * 1.0033 / pch + gp_mass / pch + 1.0073 for i in range(n)], i_arr=gp_iso_dist)
    iso_score, iso_hits, x, y = cos_dot(spec_peaks, gp_peaks)
    th_pmz = gp_mass / pch + 1.00728
    pmz_offset = pmz - th_pmz
    pos = x[0] if 0 in y else 0
    return [iso_score, iso_hits, th_pmz, pmz, pmz_offset * pch, pos,
            spec_peaks['i_arr'][pos] / np.max(spec_peaks['i_arr'])]
    # TODO
    # return {
    #     'iso_score': iso_score
    # }


def build_sqlite_from_df(df, table_name, key_col, conn, if_exists='replace', sep="\t"):
    df.to_sql(table_name, con=conn, if_exists=if_exists)
    createSecondaryIndex = "CREATE INDEX index_{1}_{0} ON {1}({0})".format(key_col, table_name)
    conn.cursor().execute(createSecondaryIndex)


def get_query_df(conn, query, table_name, key_col):
    return pd.read_sql_query("select * from {0} where {1}='{2}';".format(table_name, key_col, query), conn)


def get_spec_iso_peaks(scan, conn, table_name='spectra_iso', key_col='scan',
                       pmz_col='precursor_mz', pch_col='precursor_charge',
                       mz_col='isotope_mz_array', i_col='isotope_i_array'):
    spec_iso_df = get_query_df(conn, scan, table_name, key_col)
    spec_iso_d = dict(spec_iso_df.iloc[0])
    spec_iso_mz_arr = np.array(re.split(',', spec_iso_d[mz_col]), dtype=float)
    spec_iso_i_arr = np.array(re.split(',', spec_iso_d[i_col]), dtype=float)
    pmz = spec_iso_d[pmz_col]
    pch = spec_iso_d[pch_col]
    spec_peaks = dict(mz_arr=spec_iso_mz_arr, i_arr=spec_iso_i_arr)
    return spec_peaks, pmz, pch


def get_close_pairs(a, b, **kwargs):
    # print(a, b)
    rtol = kwargs.get('rtol', 2e-05)
    atol = kwargs.get('atol', 1e-08)
    a = np.array(a)
    a = np.reshape(a, (a.size, 1))
    b = np.array(b)
    x, y = np.nonzero(np.isclose(a, b, rtol=rtol, atol=atol))
    return x.tolist(), y.tolist()


def cos_dot(a, b, **kwargs):
    x, y = get_close_pairs(a['mz_arr'], b['mz_arr'], **kwargs)
    ia = a['i_arr']
    ib = b['i_arr']
    dot = np.dot(ia[x], ib[y])
    norma = np.linalg.norm(ia[x])
    normb = np.linalg.norm(ib[y])
    cos = dot / (norma * normb)
    return cos, len(x), x, y


def refine_result_standalone(result_path, spectra_iso_path, output_path, **kwargs):
    df = pd.read_csv(result_path, sep=",")
    spectra_iso_df = pd.read_csv(spectra_iso_path, sep="\t")
    rdf2 = refine_result(df, spectra_iso_df, **kwargs)
    rdf2.to_csv(output_path, sep="\t", index=False)
    if kwargs.get('verbose', True):
        print('Save refined result to {}'.format(output_path))


def refine_result(df, spectra_iso_df, **kwargs):
    spectra_iso_df = spectra_iso_df.loc[:, ['scan', 'iso_mz_array', 'iso_i_array']]
    rows = []
    df2 = df.merge(spectra_iso_df, left_on='MS2', right_on='scan')
    # print(df2.shape)
    for index, row in tqdm(df2.iterrows()):
        # print(row)
        glycans = row['NGLYCANS']
        if pd.isna(glycans):
            rows.append(row)
            continue
        oxo_ions = row['Oxonium Ions']
        seq, mods = parse_peptide(row['Peptide'])
        scan = row['MS2']
        spec_iso_mz_arr = np.array(re.split(',', row['iso_mz_array']), dtype=float)
        spec_iso_i_arr = np.array(re.split(',', row['iso_i_array']), dtype=float)
        spec_peaks = dict(mz_arr=spec_iso_mz_arr, i_arr=spec_iso_i_arr)
        pmz = row['Precursor MZ']
        pch = row['Precursor Charge']
        x = choose_glycan(glycans, oxo_ions, seq, mods, scan, conn=None, spec_peaks=spec_peaks, pmz=pmz, pch=pch)
        result_str = x[0]
        glycan = x[1]
        row['iso_result'] = result_str
        row['glycan'] = None if not glycan else glycan[0]
        row['fit_oxo'] = None if not glycan else glycan[4]
        row['isotope_score'] = None if not glycan else glycan[5]
        row['precursor_mass_offset'] = None if not glycan else glycan[9]
        row['mono/max'] = None if not glycan else glycan[11]
        rows.append(row)
    rdf = pd.DataFrame(rows)
    print('Refine {0} PSMs...'.format(rdf.shape[0]))
    # filter isotope score
    min_isotope_score = kwargs.get('min_isotope_score', 0.9)
    rdf2 = rdf[rdf['isotope_score'] >= min_isotope_score]
    print('Remove isotope score < {0}, remain {1} PSMs'.format(min_isotope_score, rdf2.shape[0]))
    # rdf2 = rdf
    # filter glycan composition (NeuAc)
    min_glycan_priority = kwargs.get('min_glycan_priority', 1)
    rdf2 = rdf2[rdf2['fit_oxo'] >= min_glycan_priority]
    print('Remove glycan priority < {0}, remain {1} PSMs'.format(min_glycan_priority, rdf2.shape[0]))

    min_monoisotope_ratio = kwargs.get('min_monoisotope_ratio', 0.05)
    rdf2 = rdf2[rdf2['mono/max'] >= min_monoisotope_ratio]
    print('Remove monoisotope/base ratio < {0}, remain {1} PSMs'.format(min_monoisotope_ratio, rdf2.shape[0]))
    # refine fdr
    fdrs = []
    total = 0
    decoy = 0
    for index, row in rdf2.iterrows():
        total += 1
        if re.search('DECOY', row['Protein Accession']):
            decoy += 1
        fdr = decoy / (total - decoy)
        fdrs.append(fdr)
    for fi in range(len(fdrs))[::-1]:
        fj = fi + 1
        if fj < len(fdrs):
            if fdrs[fj] < fdrs[fi]:
                fdrs[fi] = fdrs[fj]
    rdf2['refined_fdr'] = fdrs
    return rdf2


def plot_peaks(peaks, **kwargs):
    from matplotlib import collections
    import matplotlib.pyplot as plt
    color = kwargs.get('color', 'k')
    x_min = kwargs.get('x_min', min(peaks['mz_arr']) - 1)
    x_max = kwargs.get('x_max', max(peaks['mz_arr']) + 1)
    y_min = kwargs.get('y_min', 0)
    y_max = kwargs.get('y_max', max(peaks['i_arr']) + 1)
    fig, ax = plt.subplots(dpi=300)
    lines = [[[mz, 0], [mz, i]] for mz, i in zip(peaks['mz_arr'], peaks['i_arr'])]
    col = collections.LineCollection(lines, color=color)
    ax.add_collection(col)
    # print(x_min,x_max)
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    if kwargs.get('title', None):
        plt.title(kwargs.get('title'))
    if kwargs.get('out_path', None):
        plt.savefig(kwargs.get('out_path'), bbox_inches='tight')
    if not kwargs.get('verbose', None):
        plt.close()
    return fig, ax

