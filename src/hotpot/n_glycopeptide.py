import re
import sys
import fasta
import itertools
import traceback
import peptide_digestion
from pyteomics import mass

pattern_site = re.compile(r"([Nn]{1}[^Pp]{1}[TtSs]{1})")

def has_motif(peptide):
    result = pattern_site.search(peptide)
    if result:
        return True
    else:
        return False
        
def has_modification(peptide, mod_peps):
    result = False
    
    # determine the index of the last amino acid in the modified peptide sequence
    last_aa = 0
    for index, aa in enumerate(peptide):
        if aa.isalpha() and index >= last_aa:
            last_aa = index
    
    # fill peptide_mods dictionary with the modifications on each amino acid and n-terminus of the current modified peptide sequence
    current_aa = ''
    peptide_mods = {}
    c_term_flag = False
    for index, aa in enumerate(peptide):
        if index == last_aa:
            c_term_flag = True
                
        if aa.isalpha():
            current_aa = aa.upper()
        elif aa == '[':
            current_mod = ''
        elif aa == ']':
            if current_aa == '':
                if 'N-term' not in peptide_mods:
                    peptide_mods['N-term'] = set([float(current_mod)])
                    peptide_mods['Prot-N-term'] = set([float(current_mod)])
                else:
                    peptide_mods['N-term'].add(float(current_mod))
                    peptide_mods['Prot-N-term'].add(float(current_mod))
            else:
                if current_aa not in peptide_mods:
                    peptide_mods[current_aa] = set([float(current_mod)])
                else:
                    peptide_mods[current_aa].add(float(current_mod))
                    
                if c_term_flag:
                    if 'C-term' not in peptide_mods:
                        peptide_mods['C-term'] = set([float(current_mod)])
                        peptide_mods['Prot-C-term'] = set([float(current_mod)])
                    else:
                        peptide_mods['C-term'].add(float(current_mod))
                        peptide_mods['Prot-C-term'].add(float(current_mod))
        
        elif aa.isdigit() or aa == '.':
            current_mod += aa 
    
    for peptide_mod in peptide_mods:
        peptide_mods[peptide_mod] = list(peptide_mods[peptide_mod])
    
    # determine if the peptide has the glyco motif
    for label_position in sorted(mod_peps):     
        if label_position in peptide_mods:
            for label_mass in mod_peps[label_position]:
                for peptide_mods_mass in peptide_mods[label_position]:
                    min_mass = float(min(label_mass, peptide_mods_mass))
                    max_mass = float(max(label_mass, peptide_mods_mass))
                
                    # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                    if ((min_mass / max_mass) * 100.0) >= 99.999:
                        result = True
                        return result
    
    return result

def get_motif(peptide):
    result = pattern_site.findall(peptide)
    if result:
        return result
    else:
        return None

def set_n_to_motif(peptide):
    result = peptide.upper()
    for match in pattern_site.finditer(peptide):
        result = result[:match.start()] + "n" + \
            result[(match.start() + 1):]
    return result

def select_glycopeptide(peptide_list):
    glycopeptide_list = []
    for peptide in peptide_list:
        if has_motif(peptide):
            glycopeptide_list.append(peptide)
            
    return glycopeptide_list
        
def get_glycopeptide(
    protein_seq, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 2, max_length = 1e15,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True):
    """Returns a set of digested glycopeptides from the given protein sequence.

    Args:
        protein_seq: protein sequence.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.
        min_cleavage_terminus: minimum number of cleavage termini allowd.
            Value: 1: semi-digestion, at least one cleavage terminus.
                   2: full digestion, peptides with two cleavage termini.
        optional_n_term_met_cleavage: whether to consider the case of 
            N-terminal methionine cleavage.

    Returns:
        A set of digested glycopeptide sequences ('n' is in lowercase).
    """
    
    pattern = re.compile(r"n")
    protein_seq = set_n_to_motif(protein_seq[:])
    digested_peptides = peptide_digestion.protein_digestion(
        seq = protein_seq, cleavage_rule = cleavage_rule,
        max_miscleavage = max_miscleavage,
        min_length = min_length, max_length = max_length,
        min_cleavage_terminus = min_cleavage_terminus,
        optional_n_term_met_cleavage = optional_n_term_met_cleavage)
    result = set()
    for p in digested_peptides:
        if "n" in p:
            result.add(p)
    return result

def get_glycopeptide_set(fasta_file, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 2, max_length = 1e15,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True):
    """Returns a set of all digested glycopeptides from fasta file.

    Args:
        fasta_file: a fasta file name (string) of protein sequences.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.
        min_cleavage_terminus: minimum number of cleavage termini allowd.
            Value: 1: semi-digestion, at least one cleavage terminus.
                   2: full digestion, peptides with two cleavage termini.
        optional_n_term_met_cleavage: whether to consider the case of 
            N-terminal methionine cleavage.

    Returns:
        A set of digested glycopeptide sequences ('N' is in uppercase).
    """

    fasta_data = fasta.read_fasta(fasta_file)
    glycopeptide_set = set()
    for info, seq in fasta_data:
        gps = get_glycopeptide(
            protein_seq = seq, 
            cleavage_rule = cleavage_rule,
            max_miscleavage = max_miscleavage,
            min_length = min_length, max_length = max_length,
            min_cleavage_terminus = min_cleavage_terminus,
            optional_n_term_met_cleavage = optional_n_term_met_cleavage)
        for p in gps:
            glycopeptide_set.add(p.upper())
    return glycopeptide_set

def get_glycopeptide_to_glycosite_map(
    fasta_file, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 2, max_length = 1e15,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True):
    """Returns a dictionary of glycopeptide to glycosite built from fasta file.

    Args:
        fasta_file: a fasta file name (string) of protein sequences.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.
        min_cleavage_terminus: minimum number of cleavage termini allowd.
            Value: 1: semi-digestion, at least one cleavage terminus.
                   2: full digestion, peptides with two cleavage termini.
        optional_n_term_met_cleavage: whether to consider the case of 
            N-terminal methionine cleavage.

    Returns:
        A dictionary of glycopeptide to glycosite built from fasta file.
    """
    fasta_data = fasta.read_fasta(fasta_file)
    n_pattern = re.compile(r"n")
    glycopeptide_to_glycosite_map = {}
    for info, seq in fasta_data:
        processed_semi_seqs = set()
        gps_no_miscleavage = get_glycopeptide(
            protein_seq = seq, 
            cleavage_rule = cleavage_rule,
            max_miscleavage = 0,
            min_length = min_length, max_length = len(seq),
            min_cleavage_terminus = 2,
            optional_n_term_met_cleavage = optional_n_term_met_cleavage)
        for p in gps_no_miscleavage:
            if p.upper() not in glycopeptide_to_glycosite_map and \
                    len(p) >= min_length and len(p) <= max_length:
                glycopeptide_to_glycosite_map[p.upper()] = p 
            if min_cleavage_terminus == 1:
                processed_semi_seqs.add(p)
                semi_digested = peptide_digestion.generate_semi_digested_peptides(
                    p, min_length = min_length)
                for q in semi_digested:
                    if "n" in q and \
                            q.upper() not in glycopeptide_to_glycosite_map and \
                            len(q) <= max_length:
                        glycopeptide_to_glycosite_map[q.upper()] = p 

        if max_miscleavage == 0:
            continue

        gps = get_glycopeptide(
            protein_seq = seq, 
            cleavage_rule = cleavage_rule,
            max_miscleavage = max_miscleavage,
            min_length = min_length, max_length = len(seq),
            min_cleavage_terminus = 2,
            optional_n_term_met_cleavage = optional_n_term_met_cleavage)
        gps = gps.difference(gps_no_miscleavage)



        for p in sorted(list(gps), key = lambda x: len(x)):
            further_digested_peptides = \
                peptide_digestion.get_list_of_miscleavage_peptides_tuples(
                p, cleavage_rule = cleavage_rule,
                max_miscleavage = 0,
                min_length = 1)
            g_sites = []
            for m, short_peptides in further_digested_peptides:
                for q in short_peptides:
                    if "n" in q:
                        g_sites.extend([q] * q.count("n"))
            glycopeptide_to_glycosite_map[p.upper()] = g_sites[0]
    
            if min_cleavage_terminus != 1:
                continue

            processed_semi_seqs.add(p)
            site_idx = 0 if p[0] != "n" else 1
            for i in range(1, len(p) - min_length + 1):
                if p[i:] in processed_semi_seqs or "n" not in p[i:]:
                    break
                glycopeptide_to_glycosite_map[p[i:].upper()] = g_sites[site_idx]
                if p[i] == "n":
                    site_idx += 1

            site_idx = 0
            for i in range(len(p) - 1, min_length, -1):
                if p[:i] in processed_semi_seqs or "n" not in p[:i]:
                    break
                glycopeptide_to_glycosite_map[p[:i].upper()] = g_sites[site_idx]

    for k in list(glycopeptide_to_glycosite_map):
        if len(k) < min_length or len(k) > max_length:
            del glycopeptide_to_glycosite_map[k]

    return glycopeptide_to_glycosite_map

def map_glycopeptide_to_glycosite(glycopeptide_to_glycosite_map, glycopeptide):
    if glycopeptide in glycopeptide_to_glycosite_map:
        return glycopeptide_to_glycosite_map[glycopeptide]
    else:
        return ""
        
def get_glyco_mass(step3_config):
    non_amino_positions = ['N-term', 'C-term', 'Prot-N-term', 'Prot-C-term']
    
    modification_name = 'deamidated'
    
    glyco_mass = {}
    
    f = open(step3_config, 'r')
    msgf_lines = f.readlines()
    f.close()
    
    mod_section = False
    for line in msgf_lines:
        line = line.strip()
        
        if mod_section:
            if line == '#-# search parameters':
                break
                
        if not mod_section:
            if line == '#-# modifications':
                mod_section = True
                continue
                    
        if mod_section:
            if line != '':
                if modification_name in line.lower() and line[0] != '#':
                    line = line.split(',')[:4]
                    
                    mod_mass_str = line[0]
                    mod_mass = ''
                    try:
                        mod_mass_val = float(mod_mass_str)
                        mod_mass = mod_mass_val
                    except:
                        try:
                            mod_mass_val = float(mass.calculate_mass(formula = mod_mass_str))
                            mod_mass = mod_mass_val
                        except:
                            try:
                                mod_mass_val = float(mass.calculate_mass(composition = mod_mass_str))
                                mod_mass = mod_mass_val
                            except:
                                print '-'
                                print 'Error with n_glycopeptide.get_glyco_mass()...'
                                print traceback.format_exc()
                                print "\n\nCould not determine the modification's mass..."
                                print '-'
                                sys.stdout.flush()
                                
                                mod_mass = ''
                    
                    mod_position = ''
                    if line[1] == '*':
                        mod_position = line[3]
                    else:
                        mod_position = line[1]
                        
                    if mod_position not in non_amino_positions:
                        mod_positions = list(mod_position)
                    else:
                        mod_positions = [mod_position]
                    
                    if mod_mass != '':
                        for mod_p in mod_positions:
                            if mod_p not in glyco_mass:    
                                glyco_mass[mod_p] = []
                                glyco_mass[mod_p].append(mod_mass)
                            else:
                                glyco_mass[mod_p].append(mod_mass)
                        
    return glyco_mass