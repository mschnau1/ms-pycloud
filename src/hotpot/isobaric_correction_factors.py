from sympy import *

def tmt10_correction_matrix(correction_matrix_lines, quantitation_path):
    header_section = 'TMT10plex'
    tmt_channels =  ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
    matrix_dict = get_matrix_values(correction_matrix_lines, header_section)
    
    var('original_126')
    var('original_127N')
    var('original_127C')
    var('original_128N')
    var('original_128C')
    var('original_129N')
    var('original_129C')
    var('original_130N')
    var('original_130C')
    var('original_131')
    
    var('measured_126')
    var('measured_127N')
    var('measured_127C')
    var('measured_128N')
    var('measured_128C')
    var('measured_129N')
    var('measured_129C')
    var('measured_130N')
    var('measured_130C')
    var('measured_131')
        
    channel_126 = ((matrix_dict['126'][2] - matrix_dict['126'][0] - matrix_dict['126'][1] - matrix_dict['126'][3] - matrix_dict['126'][4]) * original_126 + matrix_dict['127C'][1] * original_127C + matrix_dict['128C'][0] * original_128C) / 100.0 - measured_126
    channel_127N = ((matrix_dict['127N'][2] - matrix_dict['127N'][0] - matrix_dict['127N'][1] - matrix_dict['127N'][3] - matrix_dict['127N'][4]) * original_127N + matrix_dict['128N'][1] * original_128N + matrix_dict['129N'][0] * original_129N) / 100.0 - measured_127N
    channel_127C = ((matrix_dict['127C'][2] - matrix_dict['127C'][0] - matrix_dict['127C'][1] - matrix_dict['127C'][3] - matrix_dict['127C'][4]) * original_127C + matrix_dict['128C'][1] * original_128C + matrix_dict['129C'][0] * original_129C + matrix_dict['126'][3] * original_126) / 100.0 - measured_127C
    channel_128N = ((matrix_dict['128N'][2] - matrix_dict['128N'][0] - matrix_dict['128N'][1] - matrix_dict['128N'][3] - matrix_dict['128N'][4]) * original_128N + matrix_dict['129N'][1] * original_129N + matrix_dict['130N'][0] * original_130N + matrix_dict['127N'][3] * original_127N) / 100.0 - measured_128N
    channel_128C = ((matrix_dict['128C'][2] - matrix_dict['128C'][0] - matrix_dict['128C'][1] - matrix_dict['128C'][3] - matrix_dict['128C'][4]) * original_128C + matrix_dict['129C'][1] * original_129C + matrix_dict['130C'][0] * original_130C + matrix_dict['127C'][3] * original_127C + matrix_dict['126'][4] * original_126) / 100.0 - measured_128C
    channel_129N = ((matrix_dict['129N'][2] - matrix_dict['129N'][0] - matrix_dict['129N'][1] - matrix_dict['129N'][3] - matrix_dict['129N'][4]) * original_129N + matrix_dict['130N'][1] * original_130N + matrix_dict['131'][0] * original_131 + matrix_dict['128N'][3] * original_128N + matrix_dict['127N'][4] * original_127N) / 100.0 - measured_129N
    channel_129C = ((matrix_dict['129C'][2] - matrix_dict['129C'][0] - matrix_dict['129C'][1] - matrix_dict['129C'][3] - matrix_dict['129C'][4]) * original_129C + matrix_dict['130C'][1] * original_130C + matrix_dict['128C'][3] * original_128C + matrix_dict['127C'][4] * original_127C) / 100.0 - measured_129C
    channel_130N = ((matrix_dict['130N'][2] - matrix_dict['130N'][0] - matrix_dict['130N'][1] - matrix_dict['130N'][3] - matrix_dict['130N'][4]) * original_130N + matrix_dict['131'][1] * original_131 + matrix_dict['129N'][3] * original_129N + matrix_dict['128N'][4] * original_128N) / 100.0 - measured_130N
    channel_130C = ((matrix_dict['130C'][2] - matrix_dict['130C'][0] - matrix_dict['130C'][1] - matrix_dict['130C'][3] - matrix_dict['130C'][4]) * original_130C +  matrix_dict['129C'][3] * original_129C + matrix_dict['128C'][4] * original_128C) / 100.0 - measured_130C
    channel_131 = ((matrix_dict['131'][2] - matrix_dict['131'][0] - matrix_dict['131'][1] - matrix_dict['131'][3] - matrix_dict['131'][4]) * original_131 + matrix_dict['130N'][3] * original_130N + matrix_dict['129N'][4] * original_129N) / 100.0 - measured_131
        
    sol = solve([channel_126,
                channel_127N,
                channel_127C,
                channel_128N,
                channel_128C,
                channel_129N,
                channel_129C,
                channel_130N,
                channel_130C,
                channel_131
                ],
                [original_126, original_127N, original_127C, original_128N, original_128C, original_129N, original_129C, original_130N, original_130C, original_131]
                )
    
    tmt_matrix = []
    keys = [original_126, original_127N, original_127C, original_128N, original_128C, original_129N, original_129C, original_130N, original_130C, original_131]        
    for key in keys:
        vals = str(sol[key]).split(' ')
        
        solution = {}
        for i, val in enumerate(vals):
            val = val.strip()
            if val != '-' and val != '+':
                val = val.split('*')
                
                coefficient = float(val[0].strip())
                if vals[i - 1].strip() == '-':
                    coefficient = -1 * coefficient 
                
                variable = val[-1].strip()
        
                solution[variable] = coefficient
        
        matrix_row = ['measured_126', 'measured_127N', 'measured_127C', 'measured_128N', 'measured_128C', 'measured_129N', 'measured_129C', 'measured_130N', 'measured_130C', 'measured_131'] 
        
        for sol_key in sorted(solution.keys()):
            if sol_key in matrix_row:
                index = matrix_row.index(sol_key)
                matrix_row[index] = solution[sol_key]
        
        for i, element in enumerate(matrix_row):
            if not isinstance(element, float):
                matrix_row[i] = 0
                
        tmt_matrix.append(matrix_row)
        
    f = open(quantitation_path + 'correction_factor_matrix.tsv', 'w')
    f.write('Channel\n')
    for i, row in enumerate(tmt_matrix):
        output_row = str(tmt_channels[i]) + '\t'
        for col in row:
            output_row += (str(col) + '\t')
        
        f.write(output_row.strip() + '\n')
    f.close()
        
    return tmt_matrix 

def tmt10_identity_matrix():    
    tmt_matrix = []

    tmt126_row = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt127N_row = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt127C_row = [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    tmt128N_row = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    tmt128C_row = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
    tmt129N_row = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    tmt129C_row = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    tmt130N_row = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
    tmt130C_row = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
    tmt131_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]

    tmt_matrix.append(tmt126_row)
    tmt_matrix.append(tmt127N_row)
    tmt_matrix.append(tmt127C_row)
    tmt_matrix.append(tmt128N_row)
    tmt_matrix.append(tmt128C_row)
    tmt_matrix.append(tmt129N_row)
    tmt_matrix.append(tmt129C_row)
    tmt_matrix.append(tmt130N_row)
    tmt_matrix.append(tmt130C_row)
    tmt_matrix.append(tmt131_row)
    
    return tmt_matrix
    
def tmt11_correction_matrix(correction_matrix_lines, quantitation_path):
    header_section = 'TMT11plex'
    tmt_channels =  ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
    matrix_dict = get_matrix_values(correction_matrix_lines, header_section)
    
    var('original_126')
    var('original_127N')
    var('original_127C')
    var('original_128N')
    var('original_128C')
    var('original_129N')
    var('original_129C')
    var('original_130N')
    var('original_130C')
    var('original_131')
    var('original_131C')
    
    var('measured_126')
    var('measured_127N')
    var('measured_127C')
    var('measured_128N')
    var('measured_128C')
    var('measured_129N')
    var('measured_129C')
    var('measured_130N')
    var('measured_130C')
    var('measured_131')
    var('measured_131C')
        
    channel_126 = ((matrix_dict['126'][2] - matrix_dict['126'][0] - matrix_dict['126'][1] - matrix_dict['126'][3] - matrix_dict['126'][4]) * original_126 + matrix_dict['127C'][1] * original_127C + matrix_dict['128C'][0] * original_128C) / 100.0 - measured_126
    channel_127N = ((matrix_dict['127N'][2] - matrix_dict['127N'][0] - matrix_dict['127N'][1] - matrix_dict['127N'][3] - matrix_dict['127N'][4]) * original_127N + matrix_dict['128N'][1] * original_128N + matrix_dict['129N'][0] * original_129N) / 100.0 - measured_127N
    channel_127C = ((matrix_dict['127C'][2] - matrix_dict['127C'][0] - matrix_dict['127C'][1] - matrix_dict['127C'][3] - matrix_dict['127C'][4]) * original_127C + matrix_dict['128C'][1] * original_128C + matrix_dict['129C'][0] * original_129C + matrix_dict['126'][3] * original_126) / 100.0 - measured_127C
    channel_128N = ((matrix_dict['128N'][2] - matrix_dict['128N'][0] - matrix_dict['128N'][1] - matrix_dict['128N'][3] - matrix_dict['128N'][4]) * original_128N + matrix_dict['129N'][1] * original_129N + matrix_dict['130N'][0] * original_130N + matrix_dict['127N'][3] * original_127N) / 100.0 - measured_128N
    channel_128C = ((matrix_dict['128C'][2] - matrix_dict['128C'][0] - matrix_dict['128C'][1] - matrix_dict['128C'][3] - matrix_dict['128C'][4]) * original_128C + matrix_dict['129C'][1] * original_129C + matrix_dict['130C'][0] * original_130C + matrix_dict['127C'][3] * original_127C + matrix_dict['126'][4] * original_126) / 100.0 - measured_128C
    channel_129N = ((matrix_dict['129N'][2] - matrix_dict['129N'][0] - matrix_dict['129N'][1] - matrix_dict['129N'][3] - matrix_dict['129N'][4]) * original_129N + matrix_dict['130N'][1] * original_130N + matrix_dict['131'][0] * original_131 + matrix_dict['128N'][3] * original_128N + matrix_dict['127N'][4] * original_127N) / 100.0 - measured_129N
    channel_129C = ((matrix_dict['129C'][2] - matrix_dict['129C'][0] - matrix_dict['129C'][1] - matrix_dict['129C'][3] - matrix_dict['129C'][4]) * original_129C + matrix_dict['130C'][1] * original_130C + matrix_dict['131C'][0] * original_131C + matrix_dict['128C'][3] * original_128C + matrix_dict['127C'][4] * original_127C) / 100.0 - measured_129C
    channel_130N = ((matrix_dict['130N'][2] - matrix_dict['130N'][0] - matrix_dict['130N'][1] - matrix_dict['130N'][3] - matrix_dict['130N'][4]) * original_130N + matrix_dict['131'][1] * original_131 + matrix_dict['129N'][3] * original_129N + matrix_dict['128N'][4] * original_128N) / 100.0 - measured_130N
    channel_130C = ((matrix_dict['130C'][2] - matrix_dict['130C'][0] - matrix_dict['130C'][1] - matrix_dict['130C'][3] - matrix_dict['130C'][4]) * original_130C + matrix_dict['131C'][1] * original_131C + matrix_dict['129C'][3] * original_129C + matrix_dict['128C'][4] * original_128C) / 100.0 - measured_130C
    channel_131 = ((matrix_dict['131'][2] - matrix_dict['131'][0] - matrix_dict['131'][1] - matrix_dict['131'][3] - matrix_dict['131'][4]) * original_131 + matrix_dict['130N'][3] * original_130N + matrix_dict['129N'][4] * original_129N) / 100.0 - measured_131
    channel_131C = ((matrix_dict['131C'][2] - matrix_dict['131C'][0] - matrix_dict['131C'][1] - matrix_dict['131C'][3] - matrix_dict['131C'][4]) * original_131C + matrix_dict['130C'][3] * original_130C + matrix_dict['129C'][4] * original_129C) / 100.0 - measured_131C
            
    sol = solve([channel_126,
                channel_127N,
                channel_127C,
                channel_128N,
                channel_128C,
                channel_129N,
                channel_129C,
                channel_130N,
                channel_130C,
                channel_131,
                channel_131C
                ],
                [original_126, original_127N, original_127C, original_128N, original_128C, original_129N, original_129C, original_130N, original_130C, original_131, original_131C]
                )
    
    tmt_matrix = []
    keys = [original_126, original_127N, original_127C, original_128N, original_128C, original_129N, original_129C, original_130N, original_130C, original_131, original_131C]        
    for key in keys:
        vals = str(sol[key]).split(' ')
        
        solution = {}
        for i, val in enumerate(vals):
            val = val.strip()
            if val != '-' and val != '+':
                val = val.split('*')
                
                coefficient = float(val[0].strip())
                if vals[i - 1].strip() == '-':
                    coefficient = -1 * coefficient 
                
                variable = val[-1].strip()
        
                solution[variable] = coefficient
        
        matrix_row = ['measured_126', 'measured_127N', 'measured_127C', 'measured_128N', 'measured_128C', 'measured_129N', 'measured_129C', 'measured_130N', 'measured_130C', 'measured_131', 'measured_131C'] 
        
        for sol_key in sorted(solution.keys()):
            if sol_key in matrix_row:
                index = matrix_row.index(sol_key)
                matrix_row[index] = solution[sol_key]
        
        for i, element in enumerate(matrix_row):
            if not isinstance(element, float):
                matrix_row[i] = 0
                
        tmt_matrix.append(matrix_row)
        
    f = open(quantitation_path + 'correction_factor_matrix.tsv', 'w')
    f.write('Channel\n')
    for i, row in enumerate(tmt_matrix):
        output_row = str(tmt_channels[i]) + '\t'
        for col in row:
            output_row += (str(col) + '\t')
        
        f.write(output_row.strip() + '\n')
    f.close()
         
    return tmt_matrix 

def tmt11_identity_matrix():    
    tmt_matrix = []

    tmt126_row = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt127N_row = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt127C_row = [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt128N_row = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    tmt128C_row = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    tmt129N_row = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
    tmt129C_row = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    tmt130N_row = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    tmt130C_row = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
    tmt131_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
    tmt131C_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]

    tmt_matrix.append(tmt126_row)
    tmt_matrix.append(tmt127N_row)
    tmt_matrix.append(tmt127C_row)
    tmt_matrix.append(tmt128N_row)
    tmt_matrix.append(tmt128C_row)
    tmt_matrix.append(tmt129N_row)
    tmt_matrix.append(tmt129C_row)
    tmt_matrix.append(tmt130N_row)
    tmt_matrix.append(tmt130C_row)
    tmt_matrix.append(tmt131_row)
    tmt_matrix.append(tmt131C_row)
    
    return tmt_matrix
    
def tmt16_correction_matrix(correction_matrix_lines, quantitation_path):
    header_section = 'TMT16plex'
    tmt_channels =  ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
    matrix_dict = get_matrix_values(correction_matrix_lines, header_section)
    
    var('original_126')
    var('original_127N')
    var('original_127C')
    var('original_128N')
    var('original_128C')
    var('original_129N')
    var('original_129C')
    var('original_130N')
    var('original_130C')
    var('original_131')
    var('original_131C')
    var('original_132N')
    var('original_132C')
    var('original_133N')
    var('original_133C')
    var('original_134N')
    
    var('measured_126')
    var('measured_127N')
    var('measured_127C')
    var('measured_128N')
    var('measured_128C')
    var('measured_129N')
    var('measured_129C')
    var('measured_130N')
    var('measured_130C')
    var('measured_131')
    var('measured_131C')
    var('measured_132N')
    var('measured_132C')
    var('measured_133N')
    var('measured_133C')
    var('measured_134N')
        
    channel_126 = ((matrix_dict['126'][4] - matrix_dict['126'][0] - matrix_dict['126'][1] - matrix_dict['126'][2] - matrix_dict['126'][3] - matrix_dict['126'][5] - matrix_dict['126'][6] - matrix_dict['126'][7] - matrix_dict['126'][8]) * original_126 + matrix_dict['127N'][3] * original_127N + matrix_dict['127C'][2] * original_127C) / 100.0 - measured_126
    channel_127N = ((matrix_dict['127N'][4] - matrix_dict['127N'][0] - matrix_dict['127N'][1] - matrix_dict['127N'][2] - matrix_dict['127N'][3] - matrix_dict['127N'][5] - matrix_dict['127N'][6] - matrix_dict['127N'][7] - matrix_dict['127N'][8]) * original_127N + matrix_dict['128N'][2] * original_128N) / 100.0 - measured_127N
    channel_127C = ((matrix_dict['127C'][4] - matrix_dict['127C'][0] - matrix_dict['127C'][1] - matrix_dict['127C'][2] - matrix_dict['127C'][3] - matrix_dict['127C'][5] - matrix_dict['127C'][6] - matrix_dict['127C'][7] - matrix_dict['127C'][8]) * original_127C + matrix_dict['128C'][2] * original_128C + matrix_dict['126'][6] * original_126) / 100.0 - measured_127C
    channel_128N = ((matrix_dict['128N'][4] - matrix_dict['128N'][0] - matrix_dict['128N'][1] - matrix_dict['128N'][2] - matrix_dict['128N'][3] - matrix_dict['128N'][5] - matrix_dict['128N'][6] - matrix_dict['128N'][7] - matrix_dict['128N'][8]) * original_128N + matrix_dict['129N'][2] * original_129N + matrix_dict['127N'][6] * original_127N) / 100.0 - measured_128N
    channel_128C = ((matrix_dict['128C'][4] - matrix_dict['128C'][0] - matrix_dict['128C'][1] - matrix_dict['128C'][2] - matrix_dict['128C'][3] - matrix_dict['128C'][5] - matrix_dict['128C'][6] - matrix_dict['128C'][7] - matrix_dict['128C'][8]) * original_128C + matrix_dict['129C'][2] * original_129C + matrix_dict['127C'][6] * original_127C) / 100.0 - measured_128C
    channel_129N = ((matrix_dict['129N'][4] - matrix_dict['129N'][0] - matrix_dict['129N'][1] - matrix_dict['129N'][2] - matrix_dict['129N'][3] - matrix_dict['129N'][5] - matrix_dict['129N'][6] - matrix_dict['129N'][7] - matrix_dict['129N'][8]) * original_129N + matrix_dict['130N'][2] * original_130N + matrix_dict['128N'][6] * original_128N) / 100.0 - measured_129N
    channel_129C = ((matrix_dict['129C'][4] - matrix_dict['129C'][0] - matrix_dict['129C'][1] - matrix_dict['129C'][2] - matrix_dict['129C'][3] - matrix_dict['129C'][5] - matrix_dict['129C'][6] - matrix_dict['129C'][7] - matrix_dict['129C'][8]) * original_129C + matrix_dict['130C'][2] * original_130C + matrix_dict['128C'][6] * original_128C) / 100.0 - measured_129C
    channel_130N = ((matrix_dict['130N'][4] - matrix_dict['130N'][0] - matrix_dict['130N'][1] - matrix_dict['130N'][2] - matrix_dict['130N'][3] - matrix_dict['130N'][5] - matrix_dict['130N'][6] - matrix_dict['130N'][7] - matrix_dict['130N'][8]) * original_130N + matrix_dict['131'][2] * original_131 + matrix_dict['129N'][6] * original_129N) / 100.0 - measured_130N
    channel_130C = ((matrix_dict['130C'][4] - matrix_dict['130C'][0] - matrix_dict['130C'][1] - matrix_dict['130C'][2] - matrix_dict['130C'][3] - matrix_dict['130C'][5] - matrix_dict['130C'][6] - matrix_dict['130C'][7] - matrix_dict['130C'][8]) * original_130C + matrix_dict['131C'][2] * original_131C + matrix_dict['129C'][6] * original_129C) / 100.0 - measured_130C
    channel_131 = ((matrix_dict['131'][4] - matrix_dict['131'][0] - matrix_dict['131'][1] - matrix_dict['131'][2] - matrix_dict['131'][3] - matrix_dict['131'][5] - matrix_dict['131'][6] - matrix_dict['131'][7] - matrix_dict['131'][8]) * original_131 + matrix_dict['132N'][2] * original_132N + matrix_dict['130N'][6] * original_130N) / 100.0 - measured_131
    channel_131C = ((matrix_dict['131C'][4] - matrix_dict['131C'][0] - matrix_dict['131C'][1] - matrix_dict['131C'][2] - matrix_dict['131C'][3] - matrix_dict['131C'][5] - matrix_dict['131C'][6] - matrix_dict['131C'][7] - matrix_dict['131C'][8]) * original_131C + matrix_dict['132C'][2] * original_132C + matrix_dict['130C'][6] * original_130C) / 100.0 - measured_131C
    channel_132N = ((matrix_dict['132N'][4] - matrix_dict['132N'][0] - matrix_dict['132N'][1] - matrix_dict['132N'][2] - matrix_dict['132N'][3] - matrix_dict['132N'][5] - matrix_dict['132N'][6] - matrix_dict['132N'][7] - matrix_dict['132N'][8]) * original_132N + matrix_dict['133N'][2] * original_133N + matrix_dict['131'][6] * original_131) / 100.0 - measured_132N
    channel_132C = ((matrix_dict['132C'][4] - matrix_dict['132C'][0] - matrix_dict['132C'][1] - matrix_dict['132C'][2] - matrix_dict['132C'][3] - matrix_dict['132C'][5] - matrix_dict['132C'][6] - matrix_dict['132C'][7] - matrix_dict['132C'][8]) * original_132C + matrix_dict['133C'][2] * original_133C + matrix_dict['131C'][6] * original_131C) / 100.0 - measured_132C
    channel_133N = ((matrix_dict['133N'][4] - matrix_dict['133N'][0] - matrix_dict['133N'][1] - matrix_dict['133N'][2] - matrix_dict['133N'][3] - matrix_dict['133N'][5] - matrix_dict['133N'][6] - matrix_dict['133N'][7] - matrix_dict['133N'][8]) * original_133N + matrix_dict['134N'][2] * original_134N + matrix_dict['132N'][6] * original_132N) / 100.0 - measured_133N
    channel_133C = ((matrix_dict['133C'][4] - matrix_dict['133C'][0] - matrix_dict['133C'][1] - matrix_dict['133C'][2] - matrix_dict['133C'][3] - matrix_dict['133C'][5] - matrix_dict['133C'][6] - matrix_dict['133C'][7] - matrix_dict['133C'][8]) * original_133C + matrix_dict['132C'][6] * original_132C) / 100.0 - measured_133C
    channel_134N = ((matrix_dict['134N'][4] - matrix_dict['134N'][0] - matrix_dict['134N'][1] - matrix_dict['134N'][2] - matrix_dict['134N'][3] - matrix_dict['134N'][5] - matrix_dict['134N'][6] - matrix_dict['134N'][7] - matrix_dict['134N'][8]) * original_134N + matrix_dict['133N'][6] * original_133N) / 100.0 - measured_134N

    sol = solve([channel_126,
                channel_127N,
                channel_127C,
                channel_128N,
                channel_128C,
                channel_129N,
                channel_129C,
                channel_130N,
                channel_130C,
                channel_131,
                channel_131C,
                channel_132N,
                channel_132C,
                channel_133N,
                channel_133C,
                channel_134N
                ],
                [original_126, original_127N, original_127C, original_128N, original_128C, original_129N, original_129C, original_130N, original_130C, original_131, original_131C, original_132N, original_132C, original_133N, original_133C, original_134N]
                )
    
    tmt_matrix = []
    keys = [original_126, original_127N, original_127C, original_128N, original_128C, original_129N, original_129C, original_130N, original_130C, original_131, original_131C, original_132N, original_132C, original_133N, original_133C, original_134N]        
    for key in keys:
        vals = str(sol[key]).split(' ')
        
        solution = {}
        for i, val in enumerate(vals):
            val = val.strip()
            if val != '-' and val != '+':
                val = val.split('*')
                
                coefficient = float(val[0].strip())
                if vals[i - 1].strip() == '-':
                    coefficient = -1 * coefficient 
                
                variable = val[-1].strip()
        
                solution[variable] = coefficient
        
        matrix_row = ['measured_126', 'measured_127N', 'measured_127C', 'measured_128N', 'measured_128C', 'measured_129N', 'measured_129C', 'measured_130N', 'measured_130C', 'measured_131', 'measured_131C', 'measured_132N', 'measured_132C', 'measured_133N', 'measured_133C', 'measured_134N'] 
        
        for sol_key in sorted(solution.keys()):
            if sol_key in matrix_row:
                index = matrix_row.index(sol_key)
                matrix_row[index] = solution[sol_key]
        
        for i, element in enumerate(matrix_row):
            if not isinstance(element, float):
                matrix_row[i] = 0
                
        tmt_matrix.append(matrix_row)
        
    f = open(quantitation_path + 'correction_factor_matrix.tsv', 'w')
    f.write('Channel\n')
    for i, row in enumerate(tmt_matrix):
        output_row = str(tmt_channels[i]) + '\t'
        for col in row:
            output_row += (str(col) + '\t')
        
        f.write(output_row.strip() + '\n')
    f.close()
         
    return tmt_matrix 

def tmt16_identity_matrix():    
    tmt_matrix = []

    tmt126_row = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt127N_row = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt127C_row = [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt128N_row = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt128C_row = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt129N_row = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt129C_row = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt130N_row = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    tmt130C_row = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    tmt131_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    tmt131C_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
    tmt132N_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    tmt132C_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    tmt133N_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
    tmt133C_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
    tmt134N_row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]

    tmt_matrix.append(tmt126_row)
    tmt_matrix.append(tmt127N_row)
    tmt_matrix.append(tmt127C_row)
    tmt_matrix.append(tmt128N_row)
    tmt_matrix.append(tmt128C_row)
    tmt_matrix.append(tmt129N_row)
    tmt_matrix.append(tmt129C_row)
    tmt_matrix.append(tmt130N_row)
    tmt_matrix.append(tmt130C_row)
    tmt_matrix.append(tmt131_row)
    tmt_matrix.append(tmt131C_row)
    tmt_matrix.append(tmt132N_row)
    tmt_matrix.append(tmt132C_row)
    tmt_matrix.append(tmt133N_row)
    tmt_matrix.append(tmt133C_row)
    tmt_matrix.append(tmt134N_row)
    
    return tmt_matrix
        
def itraq4plex_correction_matrix(correction_matrix_lines, quantitation_path):
    # currently no correction factors
    matrix = []

    itraq114_row = [1, 0, 0, 0]
    itraq115_row = [0, 1, 0, 0]
    itraq116_row = [0, 0, 1, 0]
    itraq117_row = [0, 0, 0, 1]
    
    matrix.append(itraq114_row)
    matrix.append(itraq115_row)
    matrix.append(itraq116_row)
    matrix.append(itraq117_row)
    
    return matrix
    
def itraq4plex_identity_matrix():
    matrix = []

    itraq114_row = [1, 0, 0, 0]
    itraq115_row = [0, 1, 0, 0]
    itraq116_row = [0, 0, 1, 0]
    itraq117_row = [0, 0, 0, 1]
    
    matrix.append(itraq114_row)
    matrix.append(itraq115_row)
    matrix.append(itraq116_row)
    matrix.append(itraq117_row)
    
    return matrix
    
def itraq8plex_correction_matrix(correction_matrix_lines, quantitation_path):
    # currently no correction factors
    matrix = []

    itraq113_row = [1, 0, 0, 0, 0, 0, 0, 0]
    itraq114_row = [0, 1, 0, 0, 0, 0, 0, 0]
    itraq115_row = [0, 0, 1, 0, 0, 0, 0, 0]
    itraq116_row = [0, 0, 0, 1, 0, 0, 0, 0]
    itraq117_row = [0, 0, 0, 0, 1, 0, 0, 0]
    itraq118_row = [0, 0, 0, 0, 0, 1, 0, 0]
    itraq119_row = [0, 0, 0, 0, 0, 0, 1, 0]
    itraq121_row = [0, 0, 0, 0, 0, 0, 0, 1]
    
    matrix.append(itraq113_row)
    matrix.append(itraq114_row)
    matrix.append(itraq115_row)
    matrix.append(itraq116_row)
    matrix.append(itraq117_row)
    matrix.append(itraq118_row)
    matrix.append(itraq119_row)
    matrix.append(itraq121_row)
    
    return matrix
    
def itraq8plex_identity_matrix():
    matrix = []

    itraq113_row = [1, 0, 0, 0, 0, 0, 0, 0]
    itraq114_row = [0, 1, 0, 0, 0, 0, 0, 0]
    itraq115_row = [0, 0, 1, 0, 0, 0, 0, 0]
    itraq116_row = [0, 0, 0, 1, 0, 0, 0, 0]
    itraq117_row = [0, 0, 0, 0, 1, 0, 0, 0]
    itraq118_row = [0, 0, 0, 0, 0, 1, 0, 0]
    itraq119_row = [0, 0, 0, 0, 0, 0, 1, 0]
    itraq121_row = [0, 0, 0, 0, 0, 0, 0, 1]
    
    matrix.append(itraq113_row)
    matrix.append(itraq114_row)
    matrix.append(itraq115_row)
    matrix.append(itraq116_row)
    matrix.append(itraq117_row)
    matrix.append(itraq118_row)
    matrix.append(itraq119_row)
    matrix.append(itraq121_row)
    
    return matrix
    
def get_matrix_values(correction_matrix_lines, header_section):
    correction_dict = {}
    header_flag = False
    header_sections = ['iTRAQ4plex', 'iTRAQ8plex', 'TMT10plex', 'TMT11plex', 'TMT16plex']
    
    for line in correction_matrix_lines:
        line = line.strip()
        split_line = line.split('\t')
        
        if line == header_section:
            header_flag = True
            continue
            
        if header_flag == True and (line == '' or line in header_sections):
            header_flag = False
            break
        
        if header_flag == True:
            # skip header row
            if split_line[0].strip() == 'Mass tag':
                continue
            else:
                channel_name = str(split_line[0].strip())
                
                correction_dict[channel_name] = []
                for val in split_line[1:]:
                    correction_dict[channel_name].append(float(val)) 
    
    return correction_dict