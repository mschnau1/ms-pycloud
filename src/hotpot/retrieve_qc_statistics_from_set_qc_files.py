"""
    - quality control statistics for a single set of data
"""

def retrieve_qc_stats(configs):
    import sys
    import glob
    import numpy
    from decimal import Decimal
    
    options = configs['options']
    
    # data parameters
    data_root = options['raw_data'] 
    data_path = data_root + 'quality_control\\'
    mzml_path = data_root + 'step1-mzml\\'
    num_mzml_files = len(glob.glob(mzml_path + '*.mzML'))
    data_type = options['modification']
    label_type = options['label_type']
    flr = float(options['false_localization_rate'])
    setname = data_path.split('\\')[-3].strip()
    data_file = data_path + 'qc-complete-' + setname + '.tsv'
    tmp_output_file = data_path + 'qc-compiled-' + setname + '.tsv'
    # only needs to be specified for phospho data
    phospho_statistics_file = data_path + 'stats-site_localization-' + setname + '.log'
    glycopeptide_file = data_path + 'stats-glycopeptide_identifications-' + setname + '.tsv'
    
    num_channels = -1
    if label_type == '4plex':
        num_channels = 4
    elif label_type == '8plex':
        num_channels = 8
    elif label_type == '10plex':
        num_channels = 10
    elif label_type == '11plex':
        num_channels = 11
    elif label_type == '16plex':
        num_channels = 16
    else:
        raise Exception('number of labeled channels could not be figured out when compiling the qc statistics')
    
    # data_file columns
    ms2_column = -6
    first_intensity_col = 6
    final_psm_col = 1
    final_pep_col = 2
    final_protein_col = 3
    psm_before_filter_col = 4
    pep_before_filter_col = 5
    
    header = 1
    
    # generating values for retrieving information from the data_file
    num_fractions = -1
    if data_type == 'global':
        num_fractions = 25
        flr = '-'
    elif data_type == 'phospho':
        num_fractions = 12
    elif data_type == 'intact':
        num_fractions = 12
        flr = '-'
    elif data_type == 'glyco':
        num_fractions = 12
        flr = '-'
    else:
        raise Exception("data_type must be either 'global' or 'phospho' or 'intact' or 'glyco'")
        
    last_intensity_col = first_intensity_col + num_channels
    
    # only runs this function if one set of data is being processed
    if num_fractions == num_mzml_files:
        print '\nCompiling quality control statistics together...'
        print '-'
        sys.stdout.flush()
        
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        num_nglycopeptides = '-'
        num_oglycopeptides = '-'
        if data_type == 'intact':
            f = open(glycopeptide_file, 'r')
            glyco_lines = f.readlines()
            f.close()
            
            # since this is the quality control for a single set, there is only 1 row of identifications besides the header row
            glyco_identifications_line = glyco_lines[1].strip().split('\t')
            
            num_nglycopeptides = glyco_identifications_line[1].strip()
            num_oglycopeptides = glyco_identifications_line[2].strip()
        
        num_phosphosites = '-'
        num_localized_phosphosites = '-'
        if data_type == 'phospho':
            f = open(phospho_statistics_file, 'r')
            phospho_lines = f.readlines()
            f.close()
            
            for line in phospho_lines:
                line = line.strip()
                
                if line[:len('Total sites =')] == 'Total sites =':
                    num_phosphosites = line.split('=')[-1].strip()
                elif line[:len('Localized sites =')] == 'Localized sites =':
                    num_localized_phosphosites = line.split('=')[-1].strip()
        
        ms2_counts = []
        min_summed_channel_intensity = -1
        for i, line in enumerate(lines[header:]):
            line = line.strip().split('\t')
            
            if i < num_fractions:
                ms2_count = int(line[ms2_column].strip())
                summed_channel_intensities = line[first_intensity_col : last_intensity_col]
                
                for q, intensity in enumerate(summed_channel_intensities):
                    summed_channel_intensities[q] = float(intensity)
                    
                min_intensity = numpy.min(summed_channel_intensities)
                if min_summed_channel_intensity == -1:
                    min_summed_channel_intensity = min_intensity
                    
                if min_intensity < min_summed_channel_intensity:
                    min_summed_channel_intensity = min_intensity
                
                ms2_counts.append(ms2_count)
            elif i == num_fractions + 1:
                # get identification numbers and min/max deviations from the median channel intensity
                final_psm = int(line[final_psm_col].strip())
                final_pep = int(line[final_pep_col].strip())
                final_protein = int(line[final_protein_col].strip())
                psm_before_filter = int(line[psm_before_filter_col].strip())
                pep_before_filter = int(line[pep_before_filter_col].strip())
                intensities = line[first_intensity_col : first_intensity_col + num_channels]
                
                for j, intensity in enumerate(intensities):
                    intensities[j] = float(intensity)
                    
                median_intensity = numpy.median(intensities)
                min_intensity = numpy.min(intensities)
                max_intensity = numpy.max(intensities)
                
                min_intensity_deviation = round((min_intensity / median_intensity) * 100.0, 2)
                max_intensity_deviation = int(round((max_intensity / median_intensity) * 100.0, 0))
            elif i == num_fractions + 5:
                pep_1_fraction = round((float(line[final_pep_col].strip()) / float(final_pep)) * 100.0, 2)
            elif i == num_fractions + 6:
                pep_2_fractions = round((float(line[final_pep_col].strip()) / float(final_pep)) * 100.0, 2)
            elif i == num_fractions + 7:
                pep_3_fractions = round((float(line[final_pep_col].strip()) / float(final_pep)) * 100.0, 2)
            elif i == num_fractions + 8:
                pep_4_fractions = round((float(line[final_pep_col].strip()) / float(final_pep)) * 100.0, 2)
            
        total_ms2 = numpy.sum(ms2_counts)
        median_ms2 = round(numpy.median(ms2_counts), 1)
        std_ms2 = round(numpy.std(ms2_counts), 4)
        
        #min_summed_channel_intensity = '%.2E' % Decimal(min_summed_channel_intensity)
        
        #print setname
        #print '-'
        #print 'N-glycopeptides = ' + str(num_nglycopeptides)
        #print 'O-glycopeptides = ' + str(num_oglycopeptides)
        #print 'FLR = ' + str(flr)
        #print 'Phosphosites = ' + str(num_phosphosites)
        #print 'Localized phosphosites = ' + str(num_localized_phosphosites)
        #print 'Final PSMs = ' + str(final_psm)
        #print 'Final peptide = ' + str(final_pep)
        #print 'Final protein = ' + str(final_protein)
        #print 'PSMs before filtering = ' + str(psm_before_filter)
        #print 'Peptides before filtering = ' + str(pep_before_filter)
        #print 'Peptide percent in 1 fraction = ' + str(pep_1_fraction)
        #print 'Peptide percent in 2 fractions = ' + str(pep_2_fractions)
        #print 'Peptide percent in 3 fractions = ' + str(pep_3_fractions)
        #print 'Peptide percent in 4 fractions = ' + str(pep_4_fractions)
        #print 'Minimum summed channel intensity = ' + str(min_summed_channel_intensity)
        #print 'Minimum intensity deviation from median = ' + str(min_intensity_deviation)
        #print 'Maximum intensity deviation from median = ' + str(max_intensity_deviation)
        #print 'Total MS2 = ' + str(total_ms2)
        #print 'Median MS2 = ' + str(median_ms2)
        #print 'Std. Dev. MS2 = ' + str(std_ms2)
        
        output_data = []
        output_data.append(str(setname))
        output_data.append(str(num_nglycopeptides))
        output_data.append(str(num_oglycopeptides))
        output_data.append(str(flr))
        output_data.append(str(num_phosphosites))
        output_data.append(str(num_localized_phosphosites))
        output_data.append(str(final_psm))
        output_data.append(str(final_pep))
        output_data.append(str(final_protein))
        output_data.append(str(psm_before_filter))
        output_data.append(str(pep_before_filter))
        output_data.append(str(pep_1_fraction))
        output_data.append(str(pep_2_fractions))
        output_data.append(str(pep_3_fractions))
        output_data.append(str(pep_4_fractions))
        output_data.append(str(min_summed_channel_intensity))
        output_data.append(str(min_intensity_deviation))
        output_data.append(str(max_intensity_deviation))
        output_data.append(str(total_ms2))
        output_data.append(str(median_ms2))
        output_data.append(str(std_ms2))
        
        f = open(tmp_output_file, 'w')
        output_line = '\t'.join(output_data)
        f.write(str(output_line).strip() + '\n')
        
        f.close()
        
        print 'Success...'
        sys.stdout.flush()