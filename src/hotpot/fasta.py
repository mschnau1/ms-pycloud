import re
import sys

def fasta_gi_to_protein_name_dict(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
    name_dict = {}
    for line in lines:
        line = line.strip()
        if line.startswith('>'):
            # splits at the gap between the gi accession name and protein name
            split_line = line.split(' ')
            
            gi = str(split_line[0]).strip()
            gi = gi.replace('>', '')
            protein_name = str(' '.join(split_line[1:])).strip()
            
            if gi != '':
                name_dict[gi] = protein_name
            else:
                print str(line) + ', did not have proper formatting of " " separating the gi accession name and the protein name.'
                sys.stdout.flush()
        else:
            continue
        
    return name_dict

def read_fasta(filename):
    with open(filename) as f:
        lines = f.readlines()

    fasta_data_current_file = []
    info = None
    for l in lines:
        l = l.rstrip()
        if l.startswith(">"):
            if info:
                fasta_data_current_file.append((info, "".join(seq)))
            info, seq = l[1:], []
        else:
            seq.append(l)
    
    if info:
        fasta_data_current_file.append((info, "".join(seq)))

    return fasta_data_current_file


def write_fasta(fasta_data, filename, multi_line = True,
                seq_line_width = 80):
    with open(filename, "w") as f:
        for info, seq in fasta_data:
            f.write(">" + info + "\n")
            if multi_line:
                i = 0
                while (i + 1) * seq_line_width <= len(seq):
                    f.write(seq[(i * seq_line_width):((i + 1) * seq_line_width)] + "\n")
                    i += 1
                if i * seq_line_width < len(seq):
                    f.write(seq[(i * seq_line_width):] + "\n")