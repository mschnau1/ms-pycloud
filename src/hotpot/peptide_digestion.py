import re
import fasta

cleavage_patterns = {"Trypsin": r"[KkRr]{1}[^Pp]{1}",
                     "Trypsin/P": r"[KkRr]{1}",
                     "Arg-C": r"[Rr]{1}",
                     "alphaLP": r"[TtAaSsVv]{1}"}

def get_list_of_miscleavage_peptides_tuples(
    seq, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 1, max_length = 1e15):
    """Returns a list of miscleavage and peptides pairs.

    The protein / peptide sequence is fully digested based on the cleavage
    rule, with up to the max miscleavage allowed.

    Args:
        seq: protein / peptide sequence.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        max_miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.

    Returns:
        A list of miscleavage and peptides pairs. For exmaple:
        
        [(0, ["NNNNK", "NNNNNNNR", "SSSSS"]),
         (1, ["NNNNKNNNNNNNR", "NNNNNNNRSSSSS"]),
         (2, ["NNNNKNNNNNNNRSSSSS"]]
    """
    cleavage_site_pattern = re.compile(cleavage_patterns[cleavage_rule])

    cleavage_site_idx = [0]
    
    for site_match in cleavage_site_pattern.finditer(seq):
        cleavage_site_idx.append(site_match.start() + 1)
        if len(seq[(site_match.start() + 1):]) >= 2 and \
                cleavage_site_pattern.match(seq[(site_match.start() + 1):(site_match.start() + 3)]):
            cleavage_site_idx.append(site_match.start() + 2)

    if cleavage_site_idx[-1] != len(seq):
        cleavage_site_idx.append(len(seq))

    complete_digested_peptides = []
    for i, site in enumerate(cleavage_site_idx):
        if i < len(cleavage_site_idx) - 1:
            complete_digested_peptides.append(seq[site:cleavage_site_idx[i + 1]])

    digested_peptides = [(0, [s for s in complete_digested_peptides \
                                  if len(s) >= min_length and \
                                  len(s) <= max_length])]
    if max_miscleavage > 0:
        for i in range(1, max_miscleavage + 1):
            peptides = []
            for j in range(0, len(complete_digested_peptides) - i):
                peptides.append("".join(complete_digested_peptides[j:(j+i+1)]))
            digested_peptides.append((i, [s for s in peptides \
                                              if len(s) >= min_length and \
                                              len(s) <= max_length]))

    return digested_peptides

def generate_semi_digested_peptides(seq,
                                    min_length = 2):
    """Returns a set of semi-digested peptides (with one cleavage terminus)
       from a fully digested peptide sequence (with two cleavage termini). 
       The returned result does not include the fully digested input sequence.

    Args:
        seq: a fully digested peptide sequence.
        min_length: min length allowed of the digested peptides.

    Returns:
        A set of peptides (not including the input sequence).
    """
    semi_digested_peptides = set()
    for i in range(min_length, len(seq)):
        semi_digested_peptides.add(seq[:i])
        semi_digested_peptides.add(seq[(len(seq) - i):])

    return semi_digested_peptides

def protein_digestion(
    seq, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 6, max_length = 50,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True):
    """Returns a set of digested peptides from the given protein sequence.

    Args:
        seq: protein sequence.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.
        min_cleavage_terminus: minimum number of cleavage termini allowd.
            Value: 1: semi-digestion, at least one cleavage terminus.
                   2: full digestion, peptides with two cleavage termini.
        optional_n_term_met_cleavage: whether to consider the case of 
            N-terminal methionine cleavage.

    Returns:
        A set of digested peptide sequences.
    """
    miscleavage_peptides = get_list_of_miscleavage_peptides_tuples(
        seq = seq, cleavage_rule = cleavage_rule,
        max_miscleavage = max_miscleavage,
        min_length = 1, max_length = len(seq))
    fully_digested_peptides = set()
    for m, peptides in miscleavage_peptides:
        for p in peptides:
            fully_digested_peptides.add(p)

    if seq.startswith("M") and optional_n_term_met_cleavage:
        miscleavage_peptides = get_list_of_miscleavage_peptides_tuples(
            seq = seq[1:], cleavage_rule = cleavage_rule,
            max_miscleavage = max_miscleavage,
            min_length = 1, max_length = len(seq))
        for m, peptides in miscleavage_peptides:
            for p in peptides:
                fully_digested_peptides.add(p)

    protein_digested = set([p for p in fully_digested_peptides if \
                                len(p) >= min_length and \
                                len(p) <= max_length])

    if min_cleavage_terminus == 1:
        semi_digested = set()
        for p in sorted(list(fully_digested_peptides), key=lambda x: len(x)):
            if p in semi_digested or len(p) <= min_length:
                continue
            semi_digested.add(p)
            semi_peptides = generate_semi_digested_peptides(p, min_length=min_length)
            semi_digested |= semi_peptides
        protein_digested |= semi_digested
        
    protein_digested = set([p for p in protein_digested if \
                                len(p) >= min_length and \
                                len(p) <= max_length])

    return protein_digested

def build_peptide_to_protein_dict_from_ncbi(
    protein_fasta_file, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 6, max_length = 50,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True):
    """Returns a dictionary of peptide-to-proteins from the protein fasta file.

    Args:
        seq: protein sequence.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.
        min_cleavage_terminus: minimum number of cleavage termini allowd.
            Value: 1: semi-digestion, at least one cleavage terminus.
                   2: full digestion, peptides with two cleavage termini.
        optional_n_term_met_cleavage: whether to consider the case of 
            N-terminal methionine cleavage.

    Returns:
        A dictionary of peptide-to-proteins from the protein fasta file.
    """
    re_pattern1 = re.compile(r"gi\|(\d*)[| ]{1}")
    re_pattern2 = re.compile(r"ref\|(.*?)[| ]{1}")

    data = fasta.read_fasta(protein_fasta_file)

    protein_gi_set = set()
    protein_accession_set = set()
    sequence_set = set()

    peptide_to_protein = {}

    counter = 0
    for info, sequence in data:
        counter += 1
        protein_gi = re_pattern1.search(info).group(1)
        protein_accession = re_pattern2.search(info).group(1)

        protein_gi_set.add(protein_gi)
        protein_accession_set.add(protein_accession)
        digestion_data = protein_digestion(sequence, 
                                           cleavage_rule = cleavage_rule,
                                           max_miscleavage = max_miscleavage,
                                           min_length = min_length, 
                                           max_length = max_length,
                                           min_cleavage_terminus = min_cleavage_terminus,
                                           optional_n_term_met_cleavage = optional_n_term_met_cleavage)
        for s in digestion_data:
            sequence_set.add(s)
            if s in peptide_to_protein:
                peptide_to_protein[s].add(protein_gi)
            else:
                peptide_to_protein[s] = set([protein_gi])
        if counter % 1000 == 0:
            print(counter)
    
    # for k in peptide_to_protein:
    #     peptide_to_protein[k] = ";".join(sorted(list(peptide_to_protein[k])))

    print("len(protein_gi_set) ", len(protein_gi_set))
    print("len(protein_accession_set) ", len(protein_accession_set))
    print("len(sequence_set) ", len(sequence_set))
    print("len(peptide_to_protein)", len(peptide_to_protein))
    print(counter)

    return peptide_to_protein

def build_peptide_to_protein_dict_from_fasta(
    protein_fasta_file, cleavage_rule = "Trypsin",
    max_miscleavage = 0,
    min_length = 6, max_length = 50,
    min_cleavage_terminus = 2,
    optional_n_term_met_cleavage = True):
    """Returns a dictionary of peptide-to-proteins from the protein fasta file.

    Args:
        seq: protein sequence.
        cleavage_rule: digestion cleavage rule. 
                       Value: 'Trypsin'
                              'Trypsin/P'
                              'Arg-C'
        miscleavage: maximum no. of miscleavage allowed.
        min_length: min length allowed of the digested peptides.
        max_length: max length allowed of the digested peptides.
        min_cleavage_terminus: minimum number of cleavage termini allowd.
            Value: 1: semi-digestion, at least one cleavage terminus.
                   2: full digestion, peptides with two cleavage termini.
        optional_n_term_met_cleavage: whether to consider the case of 
            N-terminal methionine cleavage.

    Returns:
        A dictionary of peptide-to-proteins from the protein fasta file.
    """
    data = fasta.read_fasta(protein_fasta_file)

    protein_id_set = set()
    sequence_set = set()

    peptide_to_protein = {}

    counter = 0
    for info, sequence in data:
        counter += 1
        protein_id = info.split(" ")[0]

        protein_id_set.add(protein_id)
        digestion_data = protein_digestion(sequence, 
                                           cleavage_rule = cleavage_rule,
                                           max_miscleavage = max_miscleavage,
                                           min_length = min_length, 
                                           max_length = max_length,
                                           min_cleavage_terminus = min_cleavage_terminus,
                                           optional_n_term_met_cleavage = optional_n_term_met_cleavage)
        for s in digestion_data:
            sequence_set.add(s)
            if s in peptide_to_protein:
                peptide_to_protein[s].add(protein_id)
            else:
                peptide_to_protein[s] = set([protein_id])
        if counter % 1000 == 0:
            print(counter)
    
    print("len(protein_id_set) ", len(protein_id_set))
    print("len(sequence_set) ", len(sequence_set))
    print("len(peptide_to_protein)", len(peptide_to_protein))
    print(counter)

    return peptide_to_protein