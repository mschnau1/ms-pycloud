import os
import sys
import mzid
import glob
import pepfdr
from spectrum import MS2
from read_write import read_config_param as rcp
from configuring import read_steps_config as steps

# sets the configuration filepath
config_list= os.path.dirname(os.path.abspath(__file__)).split('\\')
del config_list[-1]
config_path = ''
for s in config_list:
    config_path += s +'\\'
config_path += 'configs\\'

def get_mutant_psms(pepxml_files,
                    fdr = 0.01, 
                    score_used_for_ranking = "mvh",
                    decoy_prefix = "rev_", 
                    decoy_suffix = ":reversed", 
                    is_score_larger_better = True,
                    mod = 'global'):
    
    pepxml_path = os.path.dirname(pepxml_files) + '\\'
    search_engine = steps(pepxml_path + 'step3.config')['search_engine'].lower()
    
    psms = []
    threshold_dict = {}
    if search_engine == 'myrimatch':
        decoy_prefix = rcp(filepath = config_path + 'myrimatch.cfg', param_name = 'DecoyPrefix')
        psms_results = pepfdr.psms_at_fdr(pepxml_files, fdr, 
                          score_used_for_ranking,
                          decoy_prefix, 
                          decoy_suffix, 
                          is_score_larger_better,
                          mod)
        psms = psms_results[0]
        threshold_dict['all pepxml files'] = psms_results[1]
    elif search_engine == 'comet':
        # if comet was the search engine, sets default score used for ranking to 'xcorr'
        score_used_for_ranking = 'xcorr'
        decoy_prefix = rcp(filepath = config_path + 'comet.params', param_name = 'decoy_prefix')
        psms_results = pepfdr.psms_at_fdr(pepxml_files, fdr, 
                          score_used_for_ranking,
                          decoy_prefix, 
                          decoy_suffix, 
                          is_score_larger_better,
                          mod)
        psms = psms_results[0]
        threshold_dict['all xml files'] = psms_results[1]
        
    mutant_psms = []

    for end_scan, peptide, modified_peptide, \
            score, proteins, is_decoy, spectrum in psms:
        is_to_keep = True
        for p in proteins:
            if p.startswith("gi"):
                is_to_keep = False
        if is_to_keep:
            mutant_psms.append((end_scan, peptide, modified_peptide, \
                                    score, ";".join(proteins), is_decoy, spectrum))
    return mutant_psms


def get_mutant_psms_from_mzid(mzid_file,
                              q_value = 0.01, 
                              mutant_protein_identifier = "",
                              mod = 'global'):
    threshold_dict = {}
    mzident = mzid.Mzid(mzid_file)
    psms_results = mzident.psms_at_fdr(fdr = q_value, mod = mod, is_decoy_included = False)
    psms = psms_results[0]
    threshold_dict[os.path.basename(mzid_file)] = psms_results[1]
      
    mutant_psms = []
    for end_scan, peptide, modified_peptide, \
            score, proteins, is_decoy, spectrum in psms:
        is_to_keep = True
        for p in proteins:
            if mutant_protein_identifier not in p:
                is_to_keep = False
        if is_to_keep:
            mutant_psms.append((end_scan, peptide, modified_peptide, \
                                    score, ";".join(proteins), is_decoy, spectrum))
    return mutant_psms


def write_data_to_file(data, output_file):
    with open(output_file, "w") as f:
        for row in data:
            f.write("\t".join([str(s) for s in row]) + "\n")