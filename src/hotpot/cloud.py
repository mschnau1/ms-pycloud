def get_aws_ip(starcluster_config_path, cluster_name, user_id):
    import sys
    import subprocess 
    
    # retrieves the public dns of the master node
    # this command only works on local computer because the full path to the starcluster executable is not specified
    ip_command = 'starcluster -c ' + starcluster_config_path + ' sshmaster ' + cluster_name + ' GET http://169.254.169.254/latest/meta-data/public-hostname'
    
    print ip_command
    sys.stdout.flush()
    vals = subprocess.Popen(ip_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    ip, errors = vals.communicate()
    
    master_ip = user_id + '@' + str(ip).strip()
    
    return master_ip