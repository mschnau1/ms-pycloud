def read_steps_config(filepath):
    """
    Description: 
        Reads the configuration file for the pipeline steps.
        
    Args:
        - filepath: the full filepath of the steps configuration txt file.
        
    Returns:
        - var_dict: dictionary with all of the step configurations
    """
    
    # dictionary of the path variables
    var_dict = {}
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    for line in lines:
        # strips all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line != '' and line[0] != '#' and '=' in line:
            split_line = line.split('=')  
            # removes leading and trailing whitespace
            split_line[0] = split_line[0].strip()
            split_line[1] = split_line[1].strip()
            
            if '#' in split_line[1]:
                split_line[1] = split_line[1].split('#')[0].strip()
            
            # converts 'True' or 'False' string to True or False boolean respectively
            if split_line[1] == 'True' or split_line[1] == 'False':
                split_line[1] = (split_line[1] == 'True')
            
            # stores the config values in a dictionary
            var_dict[split_line[0]] = split_line[1]
    
    return var_dict
  
def get_starcluster_cluster_size(filepath):
    cluster_section_flag = False
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    for line in lines:
        line = str(line).strip()
        
        if line == '[cluster mspycloud]':
            cluster_section_flag = True
            
        if line == '## Defining Additional Cluster Templates ##':
            cluster_section_flag = False
        
        if cluster_section_flag == True:
            split_line = line.split('=')    
            
            key = split_line[0].strip().lower()
            if key == '':
                continue
            elif key == 'cluster_size':
                cluster_size = str(split_line[1].split('#')[0].strip())
                break
            elif key[0] == '#':
                if key.split('#')[1].strip() == 'cluster_size':
                    cluster_size = str(split_line[1].split('#')[0].strip())
                    break
                
    return cluster_size
      
def get_starcluster_volume_info(filepath):
    cluster_section_flag = False
    volume_section_flag = False
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    for line in lines:
        line = str(line).strip()
        
        if line == '[cluster mspycloud]':
            cluster_section_flag = True
            
        if line == '## Defining Additional Cluster Templates ##':
            cluster_section_flag = False
        
        if cluster_section_flag == True:
            split_line = line.split('=')    
            
            key = split_line[0].strip().lower()
            if key == '':
                continue
            elif key == 'volumes':
                volume_name = str(split_line[1].split('#')[0].strip())
                break
            elif key[0] == '#':
                if key.split('#')[1].strip() == 'volumes':
                    volume_name = str(split_line[1].split('#')[0].strip())
                    break
                     
    volumes = []
    for line in lines:
        line = str(line).strip()
            
        if line == '# Sections starting with "volume" define your EBS volumes':
            volume_section_flag = True
            
        if line == '## Configuring Security Group Permissions ##':
            volume_section_flag = False
            break
        
        if volume_section_flag == True: 
            if line.split(' ')[0].strip() == '[volume':
                volume = line.split('[volume ')[1].split(']')[0].strip()
                volumes.append(volume)
            elif line != '':
                if line[0] == '#':
                    if line.split('#')[1].strip().split(' ')[0].strip() == '[volume':
                        volume = line.split('[volume ')[1].split(']')[0].strip()
                        volumes.append(volume)    
                
    return (volume_name, volumes)           

def get_starcluster_key_info(filepath):
    cluster_section_flag = False
    key_section_flag = False
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    for line in lines:
        line = str(line).strip()
        
        if line == '[cluster mspycloud]':
            cluster_section_flag = True
            
        if line == '## Defining Additional Cluster Templates ##':
            cluster_section_flag = False
        
        if cluster_section_flag == True:
            split_line = line.split('=')    
            
            key = split_line[0].strip().lower()
            if key == '':
                continue
            elif key == 'keyname':
                keyname = str(split_line[1].split('#')[0].strip())
                break
            elif key[0] == '#':
                if key.split('#')[1].strip() == 'keyname':
                    keyname = str(split_line[1].split('#')[0].strip())
                    break 
                    
    keys = []
    for line in lines:
        line = str(line).strip()
            
        if line == '# match your key name e.g.:':
            key_section_flag = True
            
        if line == '## Defining Cluster Templates ##':
            key_section_flag = False
            break
        
        if key_section_flag == True: 
            if line.split(' ')[0].strip() == '[key':
                key = line.split('[key ')[1].split(']')[0].strip()
                keys.append(key)
            elif line != '':
                if line[0] == '#':
                    if line.split('#')[1].strip().split(' ')[0].strip() == '[key':
                        key = line.split('[key ')[1].split(']')[0].strip()
                        keys.append(key)                                   
                
    return (keyname, keys)           

def get_starcluster_key_location(filepath, keyname):
    key_section_flag = False
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    next_line = False
    for line in lines:
        line = str(line).strip()
            
        if line == '# match your key name e.g.:':
            key_section_flag = True
            
        if line == '## Defining Cluster Templates ##':
            key_section_flag = False
        
        if key_section_flag == True:
            if next_line == True:
                if '=' in line:
                    split_line = line.split('=')
                    
                    key = split_line[0].strip().lower()
                    if key == '':
                        continue
                    elif key == 'key_location':
                        key_location = str(split_line[1].split('#')[0].strip())
                        break
                    elif key[0] == '#':
                        if key.split('#')[1].strip() == 'key_location':
                            key_location = str(split_line[1].split('#')[0].strip())
                            break  
                        
            if line == '[key ' + keyname + ']':
                next_line = True
            elif line != '':
                if line[0] == '#':
                    if line.split('#')[1].strip() == '[key ' + keyname + ']':
                        next_line = True
                
    return key_location           

def sc_append_values_with_comma(filepath):
    """
    Description: 
        Reads the configuration file for the pipeline steps.
        
    Args:
        - filepath: the full filepath of the steps configuration txt file.
        
    Returns:
        - var_dict: dictionary with all of the step configurations
    """
    
    # dictionary of the path variables
    var_dict = {}
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    for line in lines:
        # strips all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line != '' and line[0] != '#' and '=' in line:
            split_line = line.split('=')  
            # removes leading and trailing whitespace
            split_line[0] = split_line[0].strip()
            split_line[1] = split_line[1].strip()
            
            # converts 'True' or 'False' string to True or False boolean respectively
            if split_line[1] == 'True' or split_line[1] == 'False':
                split_line[1] = (split_line[1] == 'True')
            
            # stores the config values in a dictionary
            if split_line[0] not in var_dict:
                var_dict[split_line[0]] = split_line[1]
            else:
                var_dict[split_line[0]] += ',' + split_line[1] 
    
    return var_dict
    
def read_msgf_mods_modifications_config(filepath):
    """
    Description: 
        Reads the configuration file for the pipeline steps.
        
    Args:
        - filepath: the full filepath of the steps configuration txt file.
        
    Returns:
        - var_dict: dictionary with all of the step configurations
    """
    
    available_mods = []
    selected_mods = []
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    search_flag = False
    
    for line in lines:
        # strips all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line == '#-# modifications':
            search_flag = True
            continue
            
        if search_flag == True:
            if line[0:3] == '#-#':
                break
            
            if line != '' and line[0] != '#':
                if ' ' in line:
                    mod = line.split()[0].strip()
                    
                    # removes leading and trailing whitespace
                    selected_mods.append(mod)
                    
                else:
                    selected_mods.append(line)
            elif line != '' and line[0] == '#':
                line = line[1:].strip()
                if ' ' in line:
                    mod = line.split()[0].strip()
                    
                    # removes leading and trailing whitespace
                    available_mods.append(mod)
                    
                else:
                    available_mods.append(line)
    
    return (available_mods, selected_mods)

def read_msgf_mods_search_config(filepath):
    """
    Description: 
        Reads the configuration file for the pipeline steps.
        
    Args:
        - filepath: the full filepath of the steps configuration txt file.
        
    Returns:
        - var_dict: dictionary with all of the step configurations
    """
    
    # dictionary of the path variables
    var_dict = {}
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    search_flag = False
    
    for line in lines:
        # strips all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line == '#-# search parameters':
            search_flag = True
            continue
            
        if search_flag == True:
            if line[0:3] == '#-#':
                break
            
            if line != '' and line[0] != '#':
                if ' ' in line:
                    split_line = line.split(' ')
                    
                    # removes leading and trailing whitespace
                    split_line[0] = split_line[0].strip()
                    split_line[1] = split_line[1].strip()
                    
                    # converts 'True' or 'False' string to True or False boolean respectively
                    if split_line[1] == 'True' or split_line[1] == 'False':
                        split_line[1] = (split_line[1] == 'True')
                    
                    # stores the config values in a dictionary
                    var_dict[split_line[0]] = split_line[1]
                    
                elif line[0:4] == '-Xmx':
                    k = line[0:4]
                    v = line[4:]
                    
                    var_dict[k] = v
    
    return var_dict
        
def read_msgf_config(filepath):
    """
    Description: 
        Reads all the commands to be used with MSGFPlus.
        
    Args:
        - filepath: the full filepath of the msgfplus configuration txt file.
        
    Returns:
        - lines_list: The first element is the heap size for Java.  Second element is the
                      destination for the copy command (if applicable, otherwise blank).
                      Third element is the string concatenation of the rest of the configuration
                      parameters.
    """
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    lines_list = [' -Xmx1024M','','']
    
    for i in range(len(lines)):
        # strips all leading and trailing whitespace
        lines[i] = lines[i].strip()
        lines[i] = str(lines[i])
        
        if lines[i][:4] == '-Xmx':
            # sets the first element of lines_list to the user specified heap size
            lines_list[0] = ' ' + lines[i]
        elif lines[i][-8:] == '.BuildSA':
            # sets the second element to the user specified copy command destination
            lines_list[1] = ' ' + lines[i]
        else:
            # sets the third element to the string concatenation of the rest of the commands
            lines_list[2] = lines_list[2] + ' ' + lines[i]
    
    return lines_list
    
def write_to_msconvert_config(filepath, command):
    """
    Description: 
        Writes a command into the msconvert configuration txt file.
        
    Args:
        - filepath: the full filepath of the msconvert configuration txt file.
        - command: the new command to be added to the configuration file
        
    Returns:
        None
    """
    import os
    
    # open file for reading and appending to the end
    infile = open(filepath, 'r')
    lines = infile.readlines()
    infile.close()
    
    outfile_path = filepath[:-4] + '_copy.txt'
    outfile = open(outfile_path, 'w')
    
    for line in lines:
        # strips all leading and trailing whitespace
        line_check = line
        line_check = line_check.strip()
        line_check = str(line_check)
        line_check = line_check.split('=')[0]
        
        # ensures that the new command replaces the old command if applicable
        if (line_check != command.split('=')[0]):
            # ensures no double new line characters
            outfile.write(line.strip("\n") + '\n')
    
    # writes the new command to file after removing the old command
    outfile.write(command + "\n")
    
    outfile.close()
    
    # deletes the old configuration file
    os.remove(filepath)
    # renames the new configuration file to the name of the old configuration file
    os.rename(outfile_path, filepath)
    
def read_itraq_filenames_config(filepath):
    """
    Description: 
        Reads the unique part of the itraq filenames
        
    Args:
        - filepath: the full filepath to itraq_filenames.txt
        
    Returns:
        - names_list: The list of filenames.
    """
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    names_list = []
    
    for i in range(len(lines)):
        # strips the newline character and all leading and trailing whitespace
        lines[i] = lines[i].strip()
        lines[i] = str(lines[i])
        
        if lines[i] != '':
            names_list.append(lines[i])
    
    return names_list
    
def write_itraq_filenames_config(filepath, names):
    """
    Description: 
        Writes the itraq_filenames configuration file.
        
    Args:
        - filepath: the full filepath of the where the itraq_filenames configuration 
        txt file will go.
        
    Returns:
        None
    """ 
    # open file for writing
    outfile = open(filepath, 'w')
    
    for name in names:
        # writes the iTRAQ run names to the itraq_filenames file
        outfile.write(str(name) + '\n\n')
    
    outfile.close()
    
def read_new_ebs_config(filepath):
    from hotpot.configuring import read_steps_config as steps
    
    ebs_params = steps(filepath)
    
    return ebs_params