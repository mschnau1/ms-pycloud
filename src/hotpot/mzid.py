from __future__ import division

import os
import sys
import json
import math
import traceback
import phosphopeptide
import n_glycopeptide as glycopeptide

try:
    from lxml import etree
except ImportError:
    import xml.etree.ElementTree as etree


import pepfdr
    
class Mzid:

    def __init__(self, mzid_file):
        self.filename = mzid_file
        self.tree = etree.parse(self.filename)
        self.root = self.tree.getroot()
        ##print self.root.attrib
        self.xmlns="{http://psidev.info/psi/pi/mzIdentML/1.1}"

    def get_db_sequence_map(self):
        db_seq_map = {}
        db_seq = self.root.findall(self.xmlns + "SequenceCollection/" + self.xmlns + "DBSequence")
        for s in db_seq:
            if s.attrib["id"] not in db_seq_map:
                db_seq_map[s.attrib["id"]] = s.attrib["accession"]
            else:
                print("Something wrong.")
        return db_seq_map

    def get_peptide_map(self):
        peptide_map = {}
        peptide = self.root.findall(self.xmlns + "SequenceCollection/" + self.xmlns + "Peptide")
        for p in peptide:
            mods = {}
            for child in p:
                if child.tag == self.xmlns + "PeptideSequence":
                    seq = child.text
                if child.tag == self.xmlns + "Modification":
                    location = int(child.attrib["location"])
                    delta = "[" + child.attrib["monoisotopicMassDelta"] + "]"
                    if location not in mods:
                        mods[location] = delta
                    else:
                        mods[location] = mods[location] + delta
            mseq = list(seq)
            mseq.insert(0, "")
            for m in mods:
                mseq[m] = mseq[m] + mods[m]
            mod_seq = "".join(mseq)
                    
            if p.attrib["id"] not in peptide_map:
                peptide_map[p.attrib["id"]] = (seq, mod_seq)
            else:
                print("Something wrong.")
        return peptide_map

    def get_peptide_evidence_map(self):
        pep_ev_map = {}
        pep_ev = self.root.findall(self.xmlns + "SequenceCollection/" + self.xmlns + "PeptideEvidence")
        for ev in pep_ev:
            if ev.attrib["id"] not in pep_ev_map:
                pep_ev_map[ev.attrib["id"]] = (ev.attrib["isDecoy"], ev.attrib["peptide_ref"], ev.attrib["dBSequence_ref"]) 
            else:
                print("Something wrong.")
        return pep_ev_map


    def get_spectrum_identification_items(self):
        spetrum_id_results = self.root.findall(self.xmlns + "DataCollection/" + self.xmlns + "AnalysisData/" + self.xmlns + "SpectrumIdentificationList/" + self.xmlns + "SpectrumIdentificationResult")
        psms = []
        for result in spetrum_id_results:
            scan_para = [s for s in result.attrib["spectrumID"].split(" ") if "scan" in s]
            scan = int(scan_para[0].split("=")[1])
            for item in result:
                if "SpectrumIdentificationItem" in item.tag and \
                        int(item.attrib["rank"]) == 1:
                    peptide_ref = item.attrib["peptide_ref"]
                    calculatedMassToCharge = float(item.attrib["calculatedMassToCharge"])
                    passThreshold = item.attrib["passThreshold"]
                    experimentalMassToCharge = item.attrib["experimentalMassToCharge"]
                    chargeState = item.attrib["chargeState"]
                    pep_ev_ref = []
                    scores = {}
                    for x in item:
                        if "PeptideEvidenceRef" in x.tag:
                            pep_ev_ref.append(x.attrib["peptideEvidence_ref"])
                        if "cvParam" in x.tag:
                            if x.attrib["name"] not in scores:
                                scores[x.attrib["name"]] = float(x.attrib["value"])
                    psms.append({"scan": scan,
                                 "peptide_ref": peptide_ref,
                                 "passThreshold": passThreshold,
                                 "calculatedMassToCharge": calculatedMassToCharge,
                                 "experimentalMassToCharge": experimentalMassToCharge,
                                 "peptideEvidence_ref": pep_ev_ref,
                                 "scores": scores,
                                 "chargeState": chargeState})
        return psms

    def get_spectrum_identification_items_filtered_by_q_values(self, q_value):
        psms = self.get_spectrum_identification_items()
        psms_filtered = [psm for psm in psms if psm["scores"]["MS-GF:QValue"] < q_value]
        return psms_filtered
    
    def psms_at_peptide_fdr(self, fdr = 0.01):
        ## peptide level fdr
        db_seq_map = self.get_db_sequence_map()
        peptide_map = self.get_peptide_map()
        pep_ev_map = self.get_peptide_evidence_map()

        psms = self.get_spectrum_identification_items()
        psms_filtered = []

        for psm in psms:
            score = psm["scores"]["MS-GF:QValue"]
            if score < fdr * 10:
                scan = psm["scan"]
                peptide = peptide_map[psm["peptide_ref"]][0]
                modified_peptide = peptide_map[psm["peptide_ref"]][1]
                is_decoy = True
                pro = []
                for pe in psm["peptideEvidence_ref"]:
                    if pep_ev_map[pe][0] == "false":
                        is_decoy = False
                        pro.append(db_seq_map[pep_ev_map[pe][2]])
                proteins = pro
                spectrum = os.path.basename(self.filename).split(".mzid")[0] + "." + str(scan) + "." + str(scan) + "." + psm["chargeState"]
                psms_filtered.append((scan, peptide, modified_peptide, \
                                          score, proteins, is_decoy, spectrum))

        top_rank_psms = psms_filtered

        b = pepfdr.sort_peptides_by_score(top_rank_psms,
                                  descending = False)

        score_fdr = pepfdr.calculate_score_fdr(b)
        threshold = pepfdr.calculate_threshold_at_fdr(score_fdr, fdr)

        print("threshold = " + str(threshold))

        psms_filtered = []
        for end_scan, peptide, modified_peptide, \
                score, proteins, is_decoy, spectrum in top_rank_psms:
            if score <= threshold and (not is_decoy):
                psms.append((end_scan, peptide, modified_peptide, \
                                 score, proteins, is_decoy, spectrum))

        return psms_filtered

    def threshold_at_peptide_fdr(self, fdr = 0.01):
        ## peptide level fdr
        db_seq_map = self.get_db_sequence_map()
        peptide_map = self.get_peptide_map()
        pep_ev_map = self.get_peptide_evidence_map()

        psms = self.get_spectrum_identification_items()
        psms_filtered = []

        for psm in psms:
            score = psm["scores"]["MS-GF:QValue"]
            if score < fdr * 10:
                scan = psm["scan"]
                peptide = peptide_map[psm["peptide_ref"]][0]
                modified_peptide = peptide_map[psm["peptide_ref"]][1]
                is_decoy = True
                pro = []
                for pe in psm["peptideEvidence_ref"]:
                    if pep_ev_map[pe][0] == "false":
                        is_decoy = False
                        pro.append(db_seq_map[pep_ev_map[pe][2]])
                proteins = pro
                spectrum = os.path.basename(self.filename).split(".mzid")[0] + "." + str(scan) + "." + str(scan) + "." + psm["chargeState"]
                psms_filtered.append((scan, peptide, modified_peptide, \
                                          score, proteins, is_decoy, spectrum))

        top_rank_psms = psms_filtered

        b = pepfdr.sort_peptides_by_score(top_rank_psms,
                                  descending = False)

        score_fdr = pepfdr.calculate_score_fdr(b)
        threshold = pepfdr.calculate_threshold_at_fdr(score_fdr, fdr)

        return threshold
     
    def get_psms_values(self, top_rank_psms, db_seq_map, peptide_map, pep_ev_map):
        psm_list = []
        for psm in top_rank_psms:
            scan = psm["scan"]
            peptide = peptide_map[psm["peptide_ref"]][0]
            modified_peptide = peptide_map[psm["peptide_ref"]][1]
            #score = psm["scores"]["MS-GF:RawScore"]
            score = -1.0 * math.log(psm["scores"]["MS-GF:SpecEValue"], 10)
            is_decoy = True
            pro = []
            for pe in psm["peptideEvidence_ref"]:
                    if pep_ev_map[pe][0] == "false":
                        is_decoy = False
                        pro.append(db_seq_map[pep_ev_map[pe][2]])
            
            proteins = pro
            spectrum = os.path.basename(self.filename).split(".mzid")[0] + "." + str(scan) + "." + str(scan) + "." + psm["chargeState"]
            
            psm_list.append((scan, peptide, modified_peptide, score, proteins, is_decoy, spectrum))   
        
        return psm_list

    def psms_at_fdr(self, fdr = 0.01, is_score_larger_better = True, mod = 'global', is_decoy_included = False):
        try:
            if mod == 'phospho':
                step3_config = os.path.dirname(self.filename) + '\\step3.config'
                mod_peps = phosphopeptide.get_phospho_mass(step3_config)
            
            ## psm level fdr
            db_seq_map = self.get_db_sequence_map()
            peptide_map = self.get_peptide_map()
            pep_ev_map = self.get_peptide_evidence_map()
    
            psms = self.get_spectrum_identification_items()
        
            # filter the psm's into groups based on their charge
            charge_psm_map = {}
            for psm in psms:
                charge = int(psm["chargeState"])
    
                if charge in charge_psm_map:
                    charge_psm_map[charge].append(psm)
                else:
                    charge_psm_map[charge] = []
                    charge_psm_map[charge].append(psm)
                    
            # calculate minimum peptide level MS-GF+ score for each of the psm charge groups such that each group's fdr is below the specified fdr value
            psms_filtered = []
            threshold_dict = {}
            for charge in charge_psm_map:
                charge_psms = self.get_psms_values(top_rank_psms = charge_psm_map[charge], db_seq_map = db_seq_map, peptide_map = peptide_map, pep_ev_map = pep_ev_map)
                #b = pepfdr.sort_peptides_by_score(top_rank_psms = charge_psms, descending = is_score_larger_better)
                b = pepfdr.sort_psms_by_score(top_rank_psms = charge_psms, descending = is_score_larger_better)
                
                score_fdr = pepfdr.calculate_score_fdr(b)
                threshold = pepfdr.calculate_threshold_at_fdr(score_fdr, fdr)
                threshold_dict[charge] = threshold
                
                for scan, peptide, modified_peptide, \
                        score, proteins, is_decoy, spectrum in charge_psms:
                    if mod == 'glyco':
                        # filter by motif later
                        if not is_decoy_included:
                            #is_glyco = glycopeptide.has_motif(peptide)
                            # includes all psms for purposes of identification numbers
                            is_glyco = True
                        else:
                            #is_glyco = glycopeptide.has_motif(peptide)
                            # includes all decoy psms as decoys, not just decoy glyco psms when doing decoy estimation
                            is_glyco = True
                        
                        if is_glyco:
                            if is_score_larger_better:
                                if score >= threshold and (not is_decoy):
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                                if score >= threshold and is_decoy_included and is_decoy:
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                            else:
                                if score <= threshold and (not is_decoy):
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                                if score <= threshold and is_decoy_included and is_decoy:
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                    elif mod == 'phospho':
                        # filter by motif later
                        if not is_decoy_included:
                            #is_phospho = phosphopeptide.has_motif(modified_peptide, mod_peps)
                            # includes all psms for purposes of identification numbers
                            is_phospho = True
                        else:
                            #is_phospho = phosphopeptide.has_motif(modified_peptide, mod_peps)
                            # includes all decoy psms as decoys, not just decoy phospho psms when doing decoy estimation
                            is_phospho = True
                        
                        if is_phospho:
                            if is_score_larger_better:
                                if score >= threshold and (not is_decoy):
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                                if score >= threshold and is_decoy_included and is_decoy:
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                            else:
                                if score <= threshold and (not is_decoy):
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                                if score <= threshold and is_decoy_included and is_decoy:
                                    psms_filtered.append((scan, peptide, modified_peptide, \
                                                    score, proteins, is_decoy, spectrum))
                    else:
                        if is_score_larger_better:
                            if score >= threshold and (not is_decoy):
                                psms_filtered.append((scan, peptide, modified_peptide, \
                                                score, proteins, is_decoy, spectrum))
                            if score >= threshold and is_decoy_included and is_decoy:
                                psms_filtered.append((scan, peptide, modified_peptide, \
                                                score, proteins, is_decoy, spectrum))
                        else:
                            if score <= threshold and (not is_decoy):
                                psms_filtered.append((scan, peptide, modified_peptide, \
                                                score, proteins, is_decoy, spectrum))
                            if score <= threshold and is_decoy_included and is_decoy:
                                psms_filtered.append((scan, peptide, modified_peptide, \
                                                score, proteins, is_decoy, spectrum))
            
            # set this value whenever the scoring parameter is changed
            # NOTE: Excel will interpret leading '-' or '=' characters as a formula, so in those cases make the leading character a blank space
            score_eq = " -Log10(MS-GF+ SpectralEValue)"
            
            return (psms_filtered, threshold_dict, score_eq)
        except:
            print '-'
            print 'Unexpected error during mzid.psms_at_fdr()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()