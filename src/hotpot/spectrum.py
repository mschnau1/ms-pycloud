from __future__ import division

from pyteomics import mzml

import sys
import glob
import traceback

class Spectrum(object):

    def __init__(self, spectrum_dict):
        self.count = spectrum_dict['count']
        self.index = spectrum_dict['index']

        self.id = spectrum_dict['id']
        scan_para = [s for s in self.id.split(" ") if "scan" in s]
        self.scan = int(scan_para[0].split("=")[1])

        assert self.index == self.scan - 1 
        # Based on observation, index starts from index 0 and scan starts
        # index 1. They have 1-to-1 correspondence.

        self.spectrum_title = spectrum_dict['spectrum title'] \
            if 'spectrum title' in spectrum_dict else None
            
        self.ms_level = spectrum_dict['ms level']

        self.scan_list = spectrum_dict['scanList']

        self.mz_array = spectrum_dict['m/z array'].tolist()
        self.intensity_array = spectrum_dict['intensity array'].tolist()
        
        if 'defaultArrayLength' in spectrum_dict:
            self.default_array_length = spectrum_dict['defaultArrayLength']
        else:
            self.default_array_length = None
        
        if 'highest observed m/z' in spectrum_dict:
            self.highest_observed_mz = spectrum_dict['highest observed m/z']
        else:
            self.highest_observed_mz = None
        
        if 'lowest observed m/z' in spectrum_dict:
            self.lowest_observed_mz = spectrum_dict['lowest observed m/z']
        else:
            self.lowest_observed_mz = None

        if 'total ion current' in spectrum_dict:
            self.total_ion_current = spectrum_dict['total ion current']
        else:
            self.total_ion_current = None

        if 'positive scan' in spectrum_dict:
            self.positive_scan = spectrum_dict['positive scan']
        else:
            self.positive_scan = None

        if 'base peak intensity' in spectrum_dict:
            self.base_peak_intensity = spectrum_dict['base peak intensity']
        else:
            self.base_peak_intensity = None
            
        if 'base peak m/z' in spectrum_dict:
            self.base_peak_mz = spectrum_dict['base peak m/z']
        else:
            self.base_peak_mz = None
    
        if 'profile spectrum' in spectrum_dict:
            self.profile_spectrum = True
        else:
            self.profile_spectrum = False

        if 'centroid spectrum' in spectrum_dict:
            self.centroid_spectrum = True
        else:
            self.centroid_spectrum = False

        if 'precursorList' in spectrum_dict:
            self.precursor_list = spectrum_dict['precursorList']
        else:
            self.precursor_list = None
        
        if 'MS1 spectrum' in spectrum_dict:
            self.ms1_spectrum = True
        else:
            self.ms1_spectrum = False

        if 'MSn spectrum' in spectrum_dict:
            self.msn_spectrum = True
        else:
            self.msn_spectrum = False
        

    def get_mz_intensity(self):
        return zip(self.mz_array, self.intensity_array)



    @staticmethod
    def get_all_spectra_from_mzml(mzml_file):
        spectra = []
        with mzml.read(mzml_file) as reader:
            for entry in reader:
                spectrum = Spectrum(entry)
                spectra.append(spectrum)
        return spectra

    @staticmethod
    def get_mz_intensity_by_scans(mzml_file, scans):
        scan_list = []
        mz_list = []
        intensity_list = []
        with mzml.read(mzml_file) as reader:
            for entry in reader:
                s = Spectrum(entry)
                if s.scan in scans:
                    scan_list.append(s.scan)
                    mz_list.append(s.mz_array)
                    intensity_list.append(s.intensity_array)
        return zip(scan_list, mz_list, intensity_list)
        
    @staticmethod
    def get_mz_intensity_by_scans_and_precursor_data(mzml_file, scans): 
        scan_list = []
        mz_list = []
        intensity_list = []
        precursor = {}
        
        with mzml.read(mzml_file) as reader:
            for entry in reader:
                s = Spectrum(entry)
                
                if s.scan in scans:
                    scan_list.append(s.scan)
                    mz_list.append(s.mz_array)
                    intensity_list.append(s.intensity_array)
                
                ms2_s = MS2(entry)
                ms2_scan = ms2_s.scan
            
                if ms2_scan in scans:
                    precursor[ms2_scan] = {}
                    precursor[ms2_scan]['mz'] = ms2_s.precursor_list['precursor'][0]['selectedIonList']['selectedIon'][0]['selected ion m/z']
                    
                    if 'peak intensity' in ms2_s.precursor_list['precursor'][0]['selectedIonList']['selectedIon'][0]:
                        precursor[ms2_scan]['peak intensity'] = float(ms2_s.precursor_list['precursor'][0]['selectedIonList']['selectedIon'][0]['peak intensity'])
                    else:
                        # placeholder value
                        precursor[ms2_scan]['peak intensity'] = 'BAD'
                    
                    if 'charge state' in ms2_s.precursor_list['precursor'][0]['selectedIonList']['selectedIon'][0]:
                        precursor[ms2_scan]['charge'] = int(ms2_s.precursor_list['precursor'][0]['selectedIonList']['selectedIon'][0]['charge state'])
                    else:
                        # placeholder value
                        precursor[ms2_scan]['charge'] = 0
                    
                    precursor[ms2_scan]['retention'] = ms2_s.scan_list['scan'][0]['scan start time']
                    precursor[ms2_scan]['base peak mz'] = ms2_s.base_peak_mz
                    precursor[ms2_scan]['base peak intensity'] = ms2_s.base_peak_intensity
            
        return (zip(scan_list, mz_list, intensity_list, precursor),precursor)


class MS1(Spectrum):

    ms1_keys = ['count', 'index', 'highest observed m/z', 'm/z array', 'ms level', 'total ion current', 'profile spectrum', 'lowest observed m/z', 'defaultArrayLength', 'intensity array', 'positive scan', 'MS1 spectrum', 'spectrum title', 'base peak intensity', 'scanList', 'id', 'base peak m/z']


    @staticmethod
    def get_all_ms1_spectra_from_mzml(mzml_file):
        ms1_spectra = []
        with mzml.read(mzml_file) as reader:
            for entry in reader:
                spectrum = MS1(entry)
                if spectrum.ms_level == 1:
                    ms1_spectra.append(spectrum)
        return ms1_spectra

class MS2(Spectrum):

    ms2_keys = ['count', 'index', 'highest observed m/z', 'm/z array', 'precursorList', 'ms level', 'total ion current', 'intensity array', 'lowest observed m/z', 'defaultArrayLength', 'centroid spectrum', 'positive scan', 'MSn spectrum', 'spectrum title', 'base peak intensity', 'scanList', 'id', 'base peak m/z']

    def __init__(self, spectrum_dict):
        super(MS2, self).__init__(spectrum_dict)


    def get_isobaric_quantitation(self, 
                                  tag_mass = [114.1112, 115.1083, 116.1116, 117.1150],
                                  mz_diff_tolerance = 0.06):
        try:
            if self.ms_level != 2:
                return None
    
            if not self.centroid_spectrum:
                # Current version can only handle centroid MS2 data
                return None
    
            quantitation = []
            for mz in tag_mass:
                mz_diff = [abs(x - mz) for x in self.mz_array]
                reporter = None
                for delta, intensity in zip(mz_diff, self.intensity_array):
                    if delta <= mz_diff_tolerance and \
                            (reporter == None or intensity > reporter):
                        reporter = intensity
                quantitation.append(reporter if reporter else 0)
    
            return quantitation
        except:
            print '-'
            print 'Error with spectrum.get_isobaric_quantitation()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()

    @staticmethod
    def get_all_ms2_spectra_from_mzml(mzml_file):
        try:
            ms2_spectra = []
            with mzml.read(mzml_file) as reader:
                i = 0
                spectrum_index = -1
                try:
                    for entry in reader:
                        i += 1
                        spectrum = MS2(entry)
                        spectrum_index = spectrum.index
                        if spectrum.ms_level == 2:
                            ms2_spectra.append(spectrum)
                except (UnicodeEncodeError, TypeError) as e:
                    print("UnicodeEncodeError in " + mzml_file + "   iteration: " + str(i) + "    spectrum index: " + str(spectrum_index))
            return ms2_spectra
        except:
            print '-'
            print 'Error with spectrum.get_all_ms2_spectra_from_mzml()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()       