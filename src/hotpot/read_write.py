def read_lines(filepath):
    """
    Description: 
        Reads the entries on each line of a txt file and returns the lines in a list.
        
    Args:
        - filepath: the full filepath to the txt file
        
    Returns:
        - data: the list of string lines
    """
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    data = []
    
    for line in lines:
        # strips the newline character and all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line != '':
            data.append(line)
    
    return data
    
def read_luciphor2_config_lines(filepath):
    """
    Description: 
        Reads the entries on each line of a txt file and returns the lines in a list.
        
    Args:
        - filepath: the full filepath to the txt file
        
    Returns:
        - data: the list of string lines
    """
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    data = []
    
    for line in lines:
        data.append(line)
    
    return data
    
def read_uncommented_lines(filepath):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    data = []
    
    for line in lines:
        # strips the newline character and all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line != '' and line[0] != '#':
            data.append(line)
    
    return data
    
def string_to_list(string, sep):
    """
    Description: 
        Converts a string list into a list object
        
    Args:
        - string: string list
        - sep: separator character (e.g. ',')
        
    Returns:
        - string_list: The list of the data.
    """
    
    string_list = string.split(sep)
    
    for index, element in enumerate(string_list):
        string_list[index] = element.strip()
        
    return string_list
    
def write_lines(filepath, data):
    """
    Description: 
        Writes the data to a txt file.
        
    Args:
        - filepath: the full filepath to the txt file
        - data: data to be written to the file
    """
    f = open(filepath, 'w')
    
    for line in data:
        f.write(line + '\n')
        
    f.close()
    
def write_luciphor2_config_lines(filepath, data):
    """
    Description: 
        Writes the data to a txt file.
        
    Args:
        - filepath: the full filepath to the txt file
        - data: data to be written to the file
    """
    f = open(filepath, 'w')
    
    for line in data:
        f.write(line)
        
    f.close()
    
def write_msgf_search(filepath, data):
    """
    Description: 
        Writes the msgf_search temporary config file
        
    Args:
        - filepath: the full filepath to the txt file
        - data: data to be written to the file
        - mod: modification (global or glyco)
    """
    
    line_test = '#-# search parameters'
    line_flag = False
    line_num = 0
    
    f = open(filepath, 'w')
    
    for line in data:
        line = line.strip()
        
        if line_flag == True:
            if line[0:3] == '#-#':
                break
            elif line != '':
                if line_num > 0:
                    f.write('\n' + line)
                    line_num += 1
                else:
                    f.write(line)
                    line_num += 1
        
        if line == line_test:
            line_flag = True
            continue
       
    f.close()
    
def write_msgf_build(filepath, data):
    """
    Description: 
        Writes the msgf_build temporary config file
        
    Args:
        - filepath: the full filepath to the txt file
        - data: data to be written to the file
        - mod: modification (global or glyco)
    """
    
    line_test = '#-# build parameters'
    line_flag = False
    line_num = 0
    
    f = open(filepath, 'w')
    
    for line in data:
        line = line.strip()
        
        if line_flag == True:
            if line[0:3] == '#-#':
                break
            elif line != '':
                if line_num > 0:
                    f.write('\n' + line)
                    line_num += 1
                else:
                    f.write(line)
                    line_num += 1
        
        if line == line_test:
            line_flag = True
            continue
       
    f.close()
    
def write_msgf_mods(filepath, data):
    """
    Description: 
        Writes the msgf_mods configuration temporary file to be used by MSGF+
        
    Args:
        - filepath: the full filepath to the txt file
        - data: data to be written to the file
        - mod: modification (global or glyco)
    """
    
    line_test = '#-# modifications'
    line_flag = False
    line_num = 0
    
    f = open(filepath, 'w')
    
    for line in data:
        line = line.strip()
        
        if line_flag == True:
            if line[0:3] == '#-#':
                break
            elif line != '':
                if line_num > 0:
                    f.write('\n' + line)
                    line_num += 1
                else:
                    f.write(line)
                    line_num += 1
        
        if line == line_test:
            line_flag = True
            continue
       
    f.close()
    
def write_tmp_config(outfilepath, infilepath):
    """
    Description: 
        Writes the steps temporary config file
        
    Args:
        - outfilepath: the full filepath to the tmp config file
        - infilepath: the full filepath to the original config file
    """
    infile = open(infilepath, 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    outfile = open(outfilepath, 'w')
    
    for line in infile_lines:
        outfile.write(line)
      
    outfile.close()
    
def read_config_param(filepath, param_name):
    """
    Description: 
        Reads the entries on each line of a txt file and returns the lines in a list.
        
    Args:
        - filepath: the full filepath to the txt file
        
    Returns:
        - data: the list of string lines
    """
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    for line in lines:
        # strips the newline character and all leading and trailing whitespace
        line = line.strip()
        line = str(line)
        
        if line[0:len(param_name)] == param_name:
            value = line.split('=')[1].strip()
            break
    
    return value
    
def write_mspycloud_config_params(params, outfile):
    f = open(outfile, 'w')
    header = 'MS-PyCloud settings'
    f.write('#' * (len(header) + 4) + '\n')
    f.write('# ' + header + ' #\n')
    f.write('#' * (len(header) + 4) + '\n') 
    
    for param in params:
        f.write('\n' + param + '\n')
    
    f.close()

def write_third_party_config_params(filepath, outfile, config_name):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    f = open(outfile, 'a')
    f.write('\n' + '#' * (len(config_name) + 4) + '\n')
    f.write('# ' + config_name + ' #\n')
    f.write('#' * (len(config_name) + 4) + '\n\n')
    
    for line in lines:
        f.write(line)
        
    f.write('\n')    
    
    f.close()
    
def append_step_sh_script(filename, command):
    step_sh = open(filename, 'a')
    
    step_sh.write(command + '\n\n')
    
    step_sh.close()
