def yyyymmdd():
    """
    Description:
        Converts the current date into a string with the format yyyymmdd
    
    Args:
        N/A
        
    Returns:
        date: a string of today's date in format yyyymmdd
    """
    
    import datetime
    
    date_str = str(datetime.date.today()).split('-')
    date = date_str[0] + date_str[1] + date_str[2]
    
    return date
    
def timestamp_to_datetime(timestamp):
    """
    Description:
        Converts a timestamp to a string of the format yyyymmdd_hhmmss 
        (year month day hour minute second).
    
    Args:
        timestamp: timestamp of a file.
        
    Returns:
        simple_datetime: the date and time in the form mentioned above as derived from the
        timestamp.
    """
    import datetime

    full_datetime = str(datetime.datetime.fromtimestamp(timestamp))
    
    date = (full_datetime.split(' ')[0]).split('-')
    year = date[0]
    month = date[1]
    day = date[2]
    
    time = (full_datetime.split(' ')[1]).split(':')
    hour = time[0]
    minute = time[1]
    second = time[2]
    
    simple_datetime = year + month + day + '_' + hour + minute + second
    
    return simple_datetime