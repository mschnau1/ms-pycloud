import sys
import traceback
from pyteomics import mass

def has_motif(peptide, mod_peps):
    result = False
    
    # determine the index of the last amino acid in the modified peptide sequence
    last_aa = 0
    for index, aa in enumerate(peptide):
        if aa.isalpha() and index >= last_aa:
            last_aa = index
    
    # fill peptide_mods dictionary with the modifications on each amino acid and n-terminus of the current modified peptide sequence
    current_aa = ''
    peptide_mods = {}
    c_term_flag = False
    for index, aa in enumerate(peptide):
        if index == last_aa:
            c_term_flag = True
                
        if aa.isalpha():
            current_aa = aa.upper()
        elif aa == '[':
            current_mod = ''
        elif aa == ']':
            if current_aa == '':
                if 'N-term' not in peptide_mods:
                    peptide_mods['N-term'] = set([float(current_mod)])
                    peptide_mods['Prot-N-term'] = set([float(current_mod)])
                else:
                    peptide_mods['N-term'].add(float(current_mod))
                    peptide_mods['Prot-N-term'].add(float(current_mod))
            else:
                if current_aa not in peptide_mods:
                    peptide_mods[current_aa] = set([float(current_mod)])
                else:
                    peptide_mods[current_aa].add(float(current_mod))
                    
                if c_term_flag:
                    if 'C-term' not in peptide_mods:
                        peptide_mods['C-term'] = set([float(current_mod)])
                        peptide_mods['Prot-C-term'] = set([float(current_mod)])
                    else:
                        peptide_mods['C-term'].add(float(current_mod))
                        peptide_mods['Prot-C-term'].add(float(current_mod))
        
        elif aa.isdigit() or aa == '.':
            current_mod += aa 
    
    for peptide_mod in peptide_mods:
        peptide_mods[peptide_mod] = list(peptide_mods[peptide_mod])
    
    # determine if the peptide has the phospho motif
    for label_position in sorted(mod_peps):     
        if label_position in peptide_mods:
            for label_mass in mod_peps[label_position]:
                for peptide_mods_mass in peptide_mods[label_position]:
                    min_mass = float(min(label_mass, peptide_mods_mass))
                    max_mass = float(max(label_mass, peptide_mods_mass))
                
                    # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                    if ((min_mass / max_mass) * 100.0) >= 99.999:
                        result = True
                        return result
    
    return result

def select_phosphopeptide(peptide_list, step3_config):
    phosphopeptide_list = []
    
    for peptide in peptide_list:
        if has_motif(peptide, step3_config):
            phosphopeptide_list.append(peptide)
    
    return phosphopeptide_list
    
def get_phospho_mass(step3_config):
    non_amino_positions = ['N-term', 'C-term', 'Prot-N-term', 'Prot-C-term']
    
    modification_name = 'phospho'
    
    phospho_mass = {}
    
    f = open(step3_config, 'r')
    msgf_lines = f.readlines()
    f.close()
    
    mod_section = False
    for line in msgf_lines:
        line = line.strip()
        
        if mod_section:
            if line == '#-# search parameters':
                break
                
        if not mod_section:
            if line == '#-# modifications':
                mod_section = True
                continue
                    
        if mod_section:
            if line != '':
                if modification_name in line.lower() and line[0] != '#':
                    line = line.split(',')[:4]
                    
                    mod_mass_str = line[0]
                    mod_mass = ''
                    try:
                        mod_mass_val = float(mod_mass_str)
                        mod_mass = mod_mass_val
                    except:
                        try:
                            mod_mass_val = float(mass.calculate_mass(formula = mod_mass_str))
                            mod_mass = mod_mass_val
                        except:
                            try:
                                mod_mass_val = float(mass.calculate_mass(composition = mod_mass_str))
                                mod_mass = mod_mass_val
                            except:
                                print '-'
                                print 'Error with phosphopeptide.get_phospho_mass()...'
                                print traceback.format_exc()
                                print "\n\nCould not determine the modification's mass..."
                                print '-'
                                sys.stdout.flush()
                                
                                mod_mass = ''
                    
                    mod_position = ''
                    if line[1] == '*':
                        mod_position = line[3]
                    else:
                        mod_position = line[1]
                        
                    if mod_position not in non_amino_positions:
                        mod_positions = list(mod_position)
                    else:
                        mod_positions = [mod_position]
                    
                    if mod_mass != '':
                        for mod_p in mod_positions:
                            if mod_p not in phospho_mass:    
                                phospho_mass[mod_p] = []
                                phospho_mass[mod_p].append(mod_mass)
                            else:
                                phospho_mass[mod_p].append(mod_mass)
                        
    return phospho_mass