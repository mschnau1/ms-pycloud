import os
import re
import sys
import glob
import traceback
from pathing import make_path
from pyteomics import mass
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

pep_col = 0
mod_pep_col = 1
proteins_col = -4
header = 0

def get_label_masses(label, step3_config):
    non_amino_positions = ['N-term', 'C-term', 'Prot-N-term', 'Prot-C-term']
    
    modification_name = ''
    if label == '10plex':
        modification_name = 'tmt6plex'
    elif label == '11plex':
        modification_name = 'tmt6plex'
    elif label == '16plex':
        modification_name = 'tmtpro'
    elif label == '4plex':
        modification_name = 'itraq4plex'
    elif label == '8plex':
        modification_name = 'itraq8plex'
    
    label_masses = {}
    
    f = open(step3_config, 'r')
    msgf_lines = f.readlines()
    f.close()
    
    mod_section = False
    for line in msgf_lines:
        line = line.strip()
        
        if mod_section:
            if line == '#-# search parameters':
                break
                
        if not mod_section:
            if line == '#-# modifications':
                mod_section = True
                continue
                    
        if mod_section:
            if line != '':
                if modification_name in line.lower() and line[0] != '#':
                    line = line.split(',')[:4]
                    
                    mod_mass_str = line[0]
                    mod_mass = ''
                    try:
                        mod_mass_val = float(mod_mass_str)
                        mod_mass = mod_mass_val
                    except:
                        try:
                            mod_mass_val = float(mass.calculate_mass(formula = mod_mass_str))
                            mod_mass = mod_mass_val
                        except:
                            try:
                                mod_mass_val = float(mass.calculate_mass(composition = mod_mass_str))
                                mod_mass = mod_mass_val
                            except:
                                print '-'
                                print 'Error with labeling_efficiency.get_label_masses()...'
                                print traceback.format_exc()
                                print "\n\nCould not determine the modification's mass..."
                                print '-'
                                sys.stdout.flush()
                                
                                mod_mass = ''
                    
                    mod_position = ''
                    if line[1] == '*':
                        mod_position = line[3]
                    else:
                        mod_position = line[1]
                        
                    if mod_position not in non_amino_positions:
                        mod_positions = list(mod_position)
                    else:
                        mod_positions = [mod_position]
                    
                    if mod_mass != '':
                        for mod_p in mod_positions:
                            if mod_p not in label_masses:    
                                label_masses[mod_p] = []
                                label_masses[mod_p].append(mod_mass)
                            else:
                                label_masses[mod_p].append(mod_mass)
                        
    return label_masses

def generate_labeling_efficiency_file(output_folder_path, psm_file, fasta_map, label_type, label_masses):
    quantitation_path = output_folder_path + 'step4a-psm_quantitation\\'
    
    psm_basename = os.path.basename(psm_file)
    outfile_name = psm_basename.replace('.txt', '') + '_labeling_efficiency.tsv'
    
    channel_begin = 2
    if label_type == '10plex':
        label_output = 'TMT10plex'
        channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
        channel_end = channel_begin + 10
    elif label_type == '11plex':
        label_output = 'TMT11plex'
        channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
        channel_end = channel_begin + 11
    elif label_type == '16plex':
        label_output = 'TMT16plex'
        channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
        channel_end = channel_begin + 16
    elif label_type == '4plex':
        label_output = 'iTRAQ4plex'
        channels = ['114', '115', '116', '117']
        channel_end = channel_begin + 4
    elif label_type == '8plex':
        label_output = 'iTRAQ8plex'
        channels = ['113', '114', '115', '116', '117', '118', '119', '121']
        channel_end = channel_begin + 8
    
    output_data = {}
    
    if psm_basename not in output_data:
        output_data[psm_basename] = ''
    else:
        print 'Error... ' + str(psm_basename) + ' was already a key in the labeling efficiency output_data dictionary...'
    
    g = open(psm_file, 'r')
    lines = g.readlines()
    g.close()
    
    label_counts = {}
    label_counts['total'] = 0
    label_counts['none'] = 0
    # fully labeled peptides
    label_counts['full'] = 0
    # partially labeled peptides
    label_counts['partial'] = 0
    # possible amino labeling sites
    label_counts['possible sites'] = 0
    # labeling amino labeling sites
    label_counts['labeled sites'] = 0
    
    tmt_channel_zeros = [0 for x in range(channel_end - channel_begin)]
    for line in lines[header: ]:
        label_counts['total'] += 1
        possible_sites = 0
        labeled_sites = 0
        
        line = line.strip().split('\t')
        seq = line[pep_col].strip()
        mod_pep = line[mod_pep_col].strip()
        # these are the proteins returned from MS-GF+, not from PyCloud's protein inference
        proteins = line[proteins_col].strip().split(';')
        intensities = line[channel_begin: channel_end]
        
        # determine if the peptide was at the start of the protein sequence so that we can determine if Prot-N-term was a possible site
        peptide_protein_start = False
        if 'Prot-N-term' in label_masses:
            peptide_protein_start = determine_if_peptide_at_start_of_protein_sequence(seq, proteins, fasta_map)
            
        peptide_protein_end = False
        # determine if the peptide was at the end of the protein sequence so that we can determine if Prot-C-term was a possible site
        if 'Prot-C-term' in label_masses:
            peptide_protein_end = determine_if_peptide_at_end_of_protein_sequence(seq, proteins, fasta_map)
        
        # determine the index of the last amino acid in the modified peptide sequence
        last_aa = 0
        for index, aa in enumerate(mod_pep):
            if aa.isalpha():
                if index >= last_aa:
                    last_aa = index
                
                if aa in label_masses:
                    possible_sites += 1
                    
        if 'N-term' in label_masses:
            possible_sites += 1   
        elif 'Prot-N-term' in label_masses and peptide_protein_start:
            # do not double count N-terminus possible sites
            possible_sites += 1
        
        if 'C-term' in label_masses:
            possible_sites += 1
        elif 'Prot-C-term' in label_masses and peptide_protein_end:
            # do not double count C-terminus possible sites
            possible_sites += 1
        
        # fill peptide_mods dictionary with the modifications on each amino acid and n-terminus and c-terminus of the current modified peptide sequence
        current_aa = ''
        peptide_mods = {}
        c_term_flag = False
        for index, aa in enumerate(mod_pep):
            if index == last_aa:
                c_term_flag = True
            
            if aa.isalpha():
                current_aa = aa.upper()
            elif aa == '[':
                current_mod = ''
            elif aa == ']':
                float_mod = float(current_mod)
                
                if current_aa == '':
                    if 'N-term' not in peptide_mods:
                        peptide_mods['N-term'] = set([float(current_mod)])
                        peptide_mods['Prot-N-term'] = set([float(current_mod)])
                    else:
                        peptide_mods['N-term'].add(float(current_mod))
                        peptide_mods['Prot-N-term'].add(float(current_mod))
                        
                    if 'N-term' in label_masses:
                        for label_mass in label_masses['N-term']:
                            min_mass = float(min(label_mass, float_mod))
                            max_mass = float(max(label_mass, float_mod))
                            
                            # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                            if ((min_mass / max_mass) * 100.0) >= 99.999:
                                labeled_sites += 1
                                break
                    elif 'Prot-N-term' in label_masses:
                        for label_mass in label_masses['Prot-N-term']:
                            min_mass = float(min(label_mass, float_mod))
                            max_mass = float(max(label_mass, float_mod))
                            
                            # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                            if ((min_mass / max_mass) * 100.0) >= 99.999:
                                labeled_sites += 1
                                break
                else:
                    if current_aa not in peptide_mods:
                        peptide_mods[current_aa] = set([float(current_mod)])
                    else:
                        peptide_mods[current_aa].add(float(current_mod))
                        
                    if current_aa in label_masses:
                        for label_mass in label_masses[current_aa]:
                            min_mass = float(min(label_mass, float_mod))
                            max_mass = float(max(label_mass, float_mod))
                            
                            # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                            if ((min_mass / max_mass) * 100.0) >= 99.999:
                                labeled_sites += 1
                                break
                    
                    if c_term_flag:
                        if 'C-term' not in peptide_mods:
                            peptide_mods['C-term'] = set([float(current_mod)])
                            peptide_mods['Prot-C-term'] = set([float(current_mod)])
                        else:
                            peptide_mods['C-term'].add(float(current_mod))
                            peptide_mods['Prot-C-term'].add(float(current_mod))
                            
                        if 'C-term' in label_masses:
                            for label_mass in label_masses['C-term']:
                                min_mass = float(min(label_mass, float_mod))
                                max_mass = float(max(label_mass, float_mod))
                                
                                # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                                if ((min_mass / max_mass) * 100.0) >= 99.999:
                                    labeled_sites += 1
                                    break
                        elif 'Prot-C-term' in label_masses:
                            for label_mass in label_masses['Prot-C-term']:
                                min_mass = float(min(label_mass, float_mod))
                                max_mass = float(max(label_mass, float_mod))
                                
                                # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                                if ((min_mass / max_mass) * 100.0) >= 99.999:
                                    labeled_sites += 1
                                    break
                        
            elif aa.isdigit() or aa == '.':
                current_mod += aa 
        
        if labeled_sites == possible_sites:
            label_counts['full'] += 1
            label_counts['partial'] += 1
        else:
            if labeled_sites >= 1:
                label_counts['partial'] += 1
        
        if labeled_sites > possible_sites:
            raise Exception('labeled_sites was greater than possible_sites during labeling efficiency calculation.')
            
        label_counts['possible sites'] += possible_sites
        label_counts['labeled sites'] += labeled_sites
        
        for peptide_mod in peptide_mods:
            peptide_mods[peptide_mod] = list(peptide_mods[peptide_mod])
        
        # determine what parts of the peptide were modified with the reagent label specified in the MS-GF+ search
        mod_positions = ''
        for label_position in sorted(label_masses):
            mod_flag = False
            
            if label_position in peptide_mods:
                for label_mass in label_masses[label_position]:
                    for peptide_mods_mass in peptide_mods[label_position]:
                        min_mass = float(min(label_mass, peptide_mods_mass))
                        max_mass = float(max(label_mass, peptide_mods_mass))
                    
                        # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                        if ((min_mass / max_mass) * 100.0) >= 99.999:
                            mod_flag = True
                            break
                            
                    if mod_flag:
                        break
            
            if mod_flag:
                if mod_positions == '':
                    mod_positions += str(label_position)
                else:
                    mod_positions += ', ' + str(label_position) 
                
        if mod_positions != '':
            if mod_positions not in label_counts:
                label_counts[mod_positions] = 1
            else:
                label_counts[mod_positions] += 1
        else:
            label_counts['none'] += 1
            
        for index, intensity in enumerate(intensities):
            intensity = float(intensity)
            if intensity == 0:
                tmt_channel_zeros[index] += 1  
    
    output_data[psm_basename] += str(float(label_counts['full']) / float(label_counts['total']) * 100.0) + '\t'
    output_data[psm_basename] += str(float(label_counts['partial']) / float(label_counts['total']) * 100.0) + '\t'
    output_data[psm_basename] += str(float(label_counts['labeled sites']) / float(label_counts['possible sites']) * 100.0) + '\t'
    
    summed_percents = 0
    for label_position in sorted(label_counts):
        if label_position != 'none' and label_position != 'total' and label_position != 'full' and label_position != 'partial' \
           and label_position != 'possible sites' and label_position != 'labeled sites':
            output_data[psm_basename] += str(float(label_counts[label_position]) / float(label_counts['total']) * 100.0) + '\t'
            summed_percents += float(label_counts[label_position]) / float(label_counts['total'])
            
    summed_percents += float(label_counts['none']) / float(label_counts['total'])
    summed_percents = str(summed_percents * 100.0) + '\t'
    
    output_data[psm_basename] += str(float(label_counts['none']) / float(label_counts['total']) * 100.0) + '\t'
    output_data[psm_basename] += summed_percents
    
    for tmt_zero in tmt_channel_zeros:
        output_data[psm_basename] += str(float(tmt_zero) / float(label_counts['total']) * 100.0) + '\t'
    
    output_data[psm_basename] += str(label_counts['none']) + '\n'
    
    f = open(quantitation_path + outfile_name, 'w')
    
    output_header = ''
    output_header += 'Filename' + '\t'
    output_header += 'Fully labeled peptides (%)' + '\t'
    output_header += 'Partially labeled peptides (%)' + '\t'
    output_header += 'Sites labeled (%)' + '\t'
    
    for label_position in sorted(label_counts):
        if label_position != 'none' and label_position != 'total' and label_position != 'full' and label_position != 'partial' \
           and label_position != 'possible sites' and label_position != 'labeled sites':        
            output_header += str(label_position) + ' ' + label_output + ' modification(s) (%)' + '\t'
    
    output_header += 'No ' + label_output + ' modifications (%)' + '\t'
    
    output_header += 'Summed percents to verify unity (%)' + '\t'
    
    for channel in channels:
        output_header += str(channel) + ' null intensities (%)' + '\t'
        
    output_header += 'Number of unmodified PSMs'
    
    f.write(str(output_header).strip() + '\n')
    
    for key in sorted(output_data):
        f.write(str(key).strip() + '\t' + output_data[key].strip() + '\n')
    
    f.close()

def determine_if_peptide_at_start_of_protein_sequence(seq, proteins, fasta_map):
    peptide_start = False
    for protein in proteins:
        if protein in fasta_map:
            protein_seq = fasta_map[protein]
            
            # only checks the first occurrence of seq in protein_seq
            protein_position = protein_seq.find(seq)
            
            # the start codon (Methionine) may or may not be cleaved, but the protein N-terminus could be modified in either case
            if protein_position == 0 or protein_position == 1:
                peptide_start = True
                
        if peptide_start:
            break
    
    return peptide_start
    
def determine_if_peptide_at_end_of_protein_sequence(seq, proteins, fasta_map):
    peptide_end = False
    for protein in proteins:
        if protein in fasta_map:
            protein_seq = fasta_map[protein]
            
            # only checks the last occurrence of seq in protein_seq
            protein_position = [m.start() for m in re.finditer(seq, protein_seq)][-1]
            
            if protein_position + len(seq) == len(protein_seq):
                peptide_end = True
                
        if peptide_end:
            break
    
    return peptide_end

def combine_labeling_efficiency_files(data_root, output_folder_path, sample_filenames, label_type):
    data_set_name = data_root.split('\\')[-2]
    quantitation_path = output_folder_path + 'step4a-psm_quantitation\\'
    qc_path = output_folder_path + 'quality_control\\'
    outfile_name = 'stats-labeling_efficiency-' + data_set_name + '.tsv'
    
    output_path = qc_path
    # creates the output path folders
    make_path(output_path)
    
    xtick_labels = ['Fully labeled peptides', 'Partially labeled peptides', 'Sites labeled']
    y_positions = [q for q in range(0, 105, 5)]
    ytick_labels = [str(q) if q % 10 == 0 else '' for q in range(0, 105, 5)]
    
    psm_files_all = sorted(glob.glob(quantitation_path + '*_labeling_efficiency.tsv'))
    psm_files = []
    
    for psm_file in psm_files_all:
        psm_basename = os.path.basename(psm_file).split('_labeling_efficiency.tsv')[0]
        
        if psm_basename in sample_filenames:
            psm_files.append(psm_file)
    
    if len(psm_files) > 0:
        psm_files.sort(key = natural_keys)
        
        longest_header = []
        for psm_file in psm_files:            
            g = open(psm_file, 'r')
            lines = g.readlines()
            g.close()
            
            header_line = lines[0].strip().split('\t')
            for i, header_col in enumerate(header_line):
                # in case Excel added any quotes to the header column names
                header_col = header_col.replace('"', '')
                
                if header_col not in longest_header:
                    longest_header.insert(i, header_col)
        
        with PdfPages(output_path + 'plots-labeling_efficiency_barcharts-' + data_set_name + '.pdf') as pdf:
            output_data = []
            for psm_file in psm_files:
                psm_basename = os.path.basename(psm_file).replace('_labeling_efficiency.tsv', '.txt')
                sample_filename = psm_basename.replace('.txt', '')
                
                output_row = [0 for x in range(len(longest_header))]
                output_row[longest_header.index('Filename')] = psm_basename
                
                g = open(psm_file, 'r')
                lines = g.readlines()
                g.close()
                
                current_header = lines[0].strip().split('\t')
                current_vals = lines[1].strip().split('\t')
                
                fully_labeled_col = -1
                partially_labeled_col = -1
                sites_labeled_col = -1
                for i, header_col in enumerate(current_header):
                    # in case Excel added any quotes to the header column names
                    header_col = header_col.replace('"', '')
                    
                    if header_col == 'Fully labeled peptides (%)':
                        fully_labeled_col = i
                    elif header_col == 'Partially labeled peptides (%)':
                        partially_labeled_col = i
                    elif header_col == 'Sites labeled (%)':
                        sites_labeled_col = i
                    
                    longest_header_index = longest_header.index(header_col)
                    output_row[longest_header_index] = current_vals[i]
                
                if fully_labeled_col != -1 and partially_labeled_col != -1 and sites_labeled_col != -1:    
                    labeling_efficiency_set_data = []
                    labeling_efficiency_set_data.append(float(output_row[fully_labeled_col]))
                    labeling_efficiency_set_data.append(float(output_row[partially_labeled_col]))
                    labeling_efficiency_set_data.append(float(output_row[sites_labeled_col]))
                    x_positions = [q for q in range(len(labeling_efficiency_set_data))]
                    
                    figure = plt.figure(figsize=(11, 8.5), dpi=72)
                    ax = figure.gca()
                    ax.xaxis.grid(False)
                    ax.yaxis.grid(True, linestyle = '--', alpha = 0.75)      
                    plt.bar(x_positions, labeling_efficiency_set_data, color = 'green', alpha = 0.75)
                    plt.xticks(x_positions, xtick_labels)
                    plt.yticks(y_positions, ytick_labels)
                    plt.title('Labeling efficiency bar chart - ' + sample_filename)
                    plt.xlabel('Category')
                    plt.ylabel('Labeling efficiency (%)')
                    plt.xlim(-0.5 , 2.5)
                    plt.ylim(0, 105)
                    
                    rects = ax.patches
    
                    # Make some labels.
                    chart_labels = [str(round(val, 2)) + ' %' for val in labeling_efficiency_set_data]
                    
                    for rect, chart_label in zip(rects, chart_labels):
                        height = rect.get_height()
                        ax.text(rect.get_x() + rect.get_width() / 2, height + 1, chart_label,
                                ha = 'center', va = 'bottom')
                                
                    pdf.savefig(figure)        
                    plt.close()
                
                output_data.append(output_row)
        
        f = open(output_path + outfile_name, 'w')
        
        output_header = ''
        for header_col in longest_header:
            output_header += str(header_col) + '\t'
        
        f.write(str(output_header).strip() + '\n')
        
        for row in output_data:
            output_row = ''
            for col in row:
                output_row += str(col) + '\t'
                
            f.write(output_row.strip() + '\n')
        
        f.close()

def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]