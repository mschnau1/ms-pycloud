import json
import glob
from multiprocessing import Pool
from pyteomics import mzml
from spectrum import MS2


def validate_mzml((mzml_file, log_message)):
    log_message = {"error": []}
    ms2_spectra = []
    print("Analyzing " + mzml_file + " ...")
    with mzml.read(mzml_file) as reader:
        i = 0
        try:
            for entry in reader:
                i += 1
                spectrum = MS2(entry)
                
        except UnicodeEncodeError as e:
            log_message["error"].append("UnicodeEncodeError in " + mzml_file + "    iteration: " + str(i) + "    Scan=" + str(spectrum.scan)  + "    Exception: " + str(e))
        except TypeError as e:
            log_message["error"].append("TypeError in " + mzml_file + "   iteration: " + str(i) + "    Scan=" + str(spectrum.scan)  + "    Exception: " + str(e))
        except StandardError as e:
            log_message["error"].append("StandardError in " + mzml_file + "   iteration: " + str(i) + "    Scan=" + str(spectrum.scan)  + "    Exception: " + str(e))
        except Exception as e:
            log_message["error"].append("Exception in " + mzml_file + "   iteration: " + str(i) + "    Scan=" + str(spectrum.scan)  + "    Exception: " + str(e))

    return True


def validate_mzml_files_in_parallel(mzml_files, 
                                    num_thread = 1, 
                                    log_message = {}):
    log_messages = [{}] * len(mzml_files)
    pool = Pool(processes = num_thread)
    pool.map(validate_mzml, zip(mzml_files, log_messages))
    for f, m in zip(mzml_files, log_messages):
        log_message[f] = m