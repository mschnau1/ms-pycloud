def make_path(path):
    """
    Description:
        Checks to see if the file path already exists and if it does not it creates the path
    
    Args:
        path: file path
    """
    import os
    
    if not os.path.exists(path):
        os.makedirs(path)

def make_path_cloud(path):
    """
    Description:
        Checks to see if the file path already exists and if it does not it creates the path
    
    Args:
        path: file path
        cluster_name: the name of the cloud cluster
        node_num: number of cloud nodes on the cluster
    """
    
    import os
    import sys
    import subprocess
    
    if not os.path.exists(path):
        mkdir_command = 'sudo mkdir ' + path
        
        print mkdir_command
        sys.stdout.flush()
        subprocess.call(mkdir_command, shell = True)

def scan_dir_for_file(path, file_list):
    """
    Description:
        Scans the directory and all subdirectories for a particular file to see if it
        exists.
    
    Args:
        path: directory to start search
        filename: file to search for
        
    Returns:
        file_exists: boolean variable which returns True if the file was found in
            the path or any of the subdirectories of the path
    """
    import os
    import sys
    import time
    import math
    
    file_paths = {}
    
    for filename in file_list:
        file_paths[filename] = []
    
    elapsed_time = 0
    start_time = time.time()
    
    for root, dirs, files in os.walk(path):
        for f in files:
            if f in file_list:
                file_paths[f].append(os.path.join(root, f))
                
            end_time = time.time()
            time_diff = math.floor(end_time - start_time)
            if time_diff >= 20:
                elapsed_time += time_diff
                start_time = end_time
                
                print 'Elapsed time = ' + str(elapsed_time) + ' seconds. Searching, please wait (everything is normal)...'
                sys.stdout.flush()
    
    print ''
    
    for key in file_paths:
        # prompts the user to select the correct file path if multiple files with same name
        if len(file_paths[key]) > 1:
            name_count = 1
                
            for name in file_paths[key]:
                print str(name_count) + ': ' + name
                name_count += 1
    
            while True:
                try:
                    correct_name_num = int(raw_input('\nEnter the number corresponding to the correct ' + key + ' file: '))
                    if correct_name_num in range(1, name_count):
                        break
                    else:
                        print 'Oops! Number not in range (1-' + str(name_count - 1) + ').  Try again...'
                except ValueError:
                    print 'Oops! That was not a valid number.  Try again...'
                    
            
            print 'Processing...\n'
            sys.stdout.flush()
                
            correct_name = file_paths[key][int(correct_name_num) - 1]
            file_paths[key] = correct_name
        else:
            file_paths[key] = file_paths[key][0]
    
    return file_paths
    
def glob_files_subdir(path, filename):
    """
    Description:
        Grabs all filenames matching filename from the directory and all subdirectories 
    
    Args:
        path: directory to start search
        filename: file to search for
        
    Returns:
        matched_filenames: list of found filenames that match filename
    """
    import os
    import fnmatch
    
    matched_filenames = []
    
    for root, dirs, files in os.walk(path):
        for f in fnmatch.filter(files, filename):
            matched_filenames.append(os.path.join(root, f))
    
    return matched_filenames
    
def check_file_exists(filepath):
    """
    Description:
        Checks to see if the filepath exists
        
    Args:
        filepath: path to the file
        
    Returns:
        True or False
    """
    import os
    
    if os.path.exists(filepath):
        return True
    else:
        return False