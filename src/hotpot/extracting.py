def unzip(zipped, output_path, zip_prog):
    """
    Description:
        Unzips files using 7-zip
    
    Args:
        zipped: the file (including the path) to be unzipped
        output_path: path for the output file   
    """
    import subprocess
    
    subprocess.call(zip_prog + ' e ' + zipped + ' -o' + output_path)