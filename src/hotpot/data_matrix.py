import numpy
from numpy import linalg
import math
import copy


class DataMatrix(object):
    """Data matrix representation of LM-MS/MS data (identification and 
    quantitation) table.

    Attributes: 
        data: a 2-D list (data matrix). The first index is row index, 
            the second index is the column index.
        col_names: a list of column names (string), the same length as the 
            number of columns in data; names in the col_names should be unique.
    """

    def __init__(self):
        self.data = None
        self.col_names = None

    def get_ncol(self):
        """Gets number of columns.
        
        Returns:
            Number of columns
        """
        return len(self.col_names)

    def get_nrow(self):
        """Gets number of rows.
        
        Returns:
            Number of rows
        """
        return len(self.data)


    def set_col_names(self, colnames):
        """Sets column names.
        
        Args:
            colnames: a list of column names.
        """
        self.col_names = copy.copy(colnames)
        

    def col_names_to_col_idx(self, colnames):
        """Converts a list of column names to a list of column indices.
        
        Args:
            colnames: a list of column names.
            
        Returns:
            A list of column indices corresponding to the input colnames.
        
        Raises:
            ValueError: an error occurs when some elements of colnames are not in 
                col_names.
        """
        if isinstance(colnames, str):
            colnames = [colnames]
        col_idx = [self.col_names.index(name) for name in colnames]
        return col_idx
    
    @staticmethod
    def data_matrix_from_tsv(tsv_file, header = True):
        """Reads data from a tab-delimited file and returns a DataMatrix object.
        
        Args:
            tsv_file: a tab-delimited file name.
            header: whether the first row in the file is the header row of column names.
            
        Returns:
            A DataMatrix object.

        Warnings:
            prints a warning when elements in col_names are not unique.
        """
        dm = DataMatrix()
        dm.data = []
        with open(tsv_file) as f:
            i = 0
            for l in f:
                i += 1
                row_values = l.split("\t")
                row_values[-1] = row_values[-1].rstrip()
                if header and i == 1:
                    dm.col_names = row_values
                else:
                    dm.data.append(row_values)
        if not header:
            dm.col_names = ["Column_" + str(i + 1) for i in range(len(dm.data[0]))]

        if len(set(dm.col_names)) != len(dm.col_names):
            print("Warning: column names are not unique.")

        return dm
        
    @staticmethod
    def data_matrix_from_tsv_remove_contams(tsv_file, protein_id_col, contams, header = True):
        """Reads data from a tab-delimited file and returns a DataMatrix object.
        
        Args:
            tsv_file: a tab-delimited file name.
            header: whether the first row in the file is the header row of column names.
            
        Returns:
            A DataMatrix object.

        Warnings:
            prints a warning when elements in col_names are not unique.
        """
        dm = DataMatrix()
        dm.data = []
        with open(tsv_file) as f:
            i = 0
            for l in f:
                i += 1
                row_values = l.split("\t")
                row_values[-1] = row_values[-1].rstrip()
                if header and i == 1:
                    dm.col_names = row_values
                else:
                    proteins = row_values[protein_id_col].strip().split(';')
                    
                    contams_flag = False
                    if contams != set():
                        for protein in proteins:
                            protein = protein.strip()
                            
                            if protein in contams:
                                contams_flag = True
                                break
                    
                    if not contams_flag:
                        dm.data.append(row_values)
        if not header:
            dm.col_names = ["Column_" + str(i + 1) for i in range(len(dm.data[0]))]

        if len(set(dm.col_names)) != len(dm.col_names):
            print("Warning: column names are not unique.")

        return dm

    @staticmethod
    def data_matrix_from_tsv_with_threshold(tsv_file, first_intensity_col, num_intensity_cols, intensity_threshold, header = True):
        """Reads data from a tab-delimited file and returns a DataMatrix object.
        
        Args:
            tsv_file: a tab-delimited file name.
            intensity_threshold: minimum PSM intensity to accept for quantification
            header: whether the first row in the file is the header row of column names.
            
        Returns:
            A DataMatrix object.

        Warnings:
            prints a warning when elements in col_names are not unique.
        """
        dm = DataMatrix()
        dm.data = []
        with open(tsv_file) as f:
            i = 0
            for l in f:
                i += 1
                row_values = l.split("\t")
                row_values[-1] = row_values[-1].rstrip()
                if header and i == 1:
                    dm.col_names = row_values
                else:
                    # filter the intensity values that are below intensity_threshold
                    for index, row_v in enumerate(row_values[first_intensity_col : first_intensity_col + num_intensity_cols]):
                        row_v = float(row_v)
                        
                        if row_v <= intensity_threshold:
                            row_values[index + first_intensity_col] = 0
                        
                    dm.data.append(row_values)
        if not header:
            dm.col_names = ["Column_" + str(i + 1) for i in range(len(dm.data[0]))]

        if len(set(dm.col_names)) != len(dm.col_names):
            print("Warning: column names are not unique.")

        return dm

    @staticmethod
    def data_matrix_from_csv(csv_file, header = True):
        """Reads data from a comma-delimited file and returns a DataMatrix object.
        
        Args:
            csv_file: a comma-delimited file name.
            header: whether the first row in the file is the header row of column names.
            
        Returns:
            A DataMatrix object.

        Warnings:
            prints a warning when elements in col_names are not unique.
        """
        dm = DataMatrix()
        dm.data = []
        with open(csv_file) as f:
            i = 0
            for l in f:
                i += 1
                row_values = l.split(",")
                row_values[-1] = row_values[-1].rstrip()
                if header and i == 1:
                    dm.col_names = row_values
                else:
                    dm.data.append(row_values)
        if not header:
            dm.col_names = ["Column_" + str(i + 1) for i in range(len(dm.data[0]))]

        if len(set(dm.col_names)) != len(dm.col_names):
            print("Warning: column names are not unique.")

        return dm


    def write_tsv(self, tsv_file, header = True):
        """Writes a DataMatrix object to a tab-delimited file.
        
        Args:
            tsv_file: the file name for the tab-delimited output.
            header: a boolean value indicating whether to output table header.
        """
        i = 0
        with open(tsv_file, "w") as f:
            if header:
                f.write("\t".join(self.col_names) + "\n")
            for row in self.data:
                f.write("\t".join([str(x) for x in row]) + "\n")

    def write_nglycoforms_tsv(self, tsv_file, header = True):
        """Writes a DataMatrix object to a tab-delimited file.  Combines the peptide and modified peptide columns with 
            the N-glycan column to create a glycoform.  Filters out missing N-glycans ('NA' or '').
        
        Args:
            tsv_file: the file name for the tab-delimited output.
            header: a boolean value indicating whether to output table header.
        """
        pep_col = 0
        mod_pep_col = 1
        n_glycan_col = -10
        
        i = 0
        with open(tsv_file, "w") as f:
            if header:
                header_column_names = list(self.col_names)
                header_column_names.remove('N-glycan')
                header_column_names.remove('O-glycan')
                
                f.write("\t".join(header_column_names) + "\n")
            for row in self.data:
                n_glycan = row[n_glycan_col].strip()
                if n_glycan != 'NA' and n_glycan != '':
                    glycoform = row[pep_col].strip() + '-' + n_glycan
                    mod_glycoform = row[mod_pep_col].strip() + '-' + n_glycan
                    
                    output_row = [str(glycoform), str(mod_glycoform)]
                    for col in row[2 : -10]:
                        output_row.append(str(col))
                        
                    # output the N-glycans column
                    output_row.append(str(row[-9]))
                        
                    for col in row[-7:]:
                        output_row.append(str(col))
                        
                    output_line = '\t'.join(output_row)
                    f.write(str(output_line.strip()) + '\n')           
    
    def write_oglycoforms_tsv(self, tsv_file, header = True):
        """Writes a DataMatrix object to a tab-delimited file.  Combines the peptide and modified peptide columns with 
            the O-glycan column to create a glycoform.  Filters out missing O-glycans ('NA' or '').
        
        Args:
            tsv_file: the file name for the tab-delimited output.
            header: a boolean value indicating whether to output table header.
        """
        pep_col = 0
        mod_pep_col = 1
        o_glycan_col = -8
        
        i = 0
        with open(tsv_file, "w") as f:
            if header:
                header_column_names = list(self.col_names)
                header_column_names.remove('N-glycan')
                header_column_names.remove('O-glycan')
                
                f.write("\t".join(header_column_names) + "\n")
            for row in self.data:
                o_glycan = row[o_glycan_col].strip()
                if o_glycan != 'NA' and o_glycan != '':
                    glycoform = row[pep_col].strip() + '-' + o_glycan
                    mod_glycoform = row[mod_pep_col].strip() + '-' + o_glycan
                    
                    output_row = [str(glycoform), str(mod_glycoform)]
                    for col in row[2 : -10]:
                        output_row.append(str(col))
                        
                    # output the N-glycans column
                    output_row.append(str(row[-9]))
                        
                    for col in row[-7:]:
                        output_row.append(str(col))
                        
                    output_line = '\t'.join(output_row)
                    f.write(str(output_line.strip()) + '\n')
    
    def write_csv(self, csv_file, header = True):
        """Writes a DataMatrix object to a comma-delimited file.
        
        Args:
            csv_file: the file name for the comma-delimited output.
            header: a boolean value indicating whether to output table header.
        """
        i = 0
        with open(csv_file, "w") as f:
            if header:
                f.write(",".join(self.col_names) + "\n")
            for row in self.data:
                f.write(",".join([str(x) for x in row]) + "\n")

                

    def quantitative_columns_to_float(self, quantitative_columns,
                                      missing_value_imputed_with = 0.0,
                                      by_name = True):
        """Converts quantitative columns from strings to float values.
        
        Args:
            quantitative_columns: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"], if by_name is True. 
                If by_name is False, a list of column indices is expected, 
                such as [1, 2, 3].
            missing_value_imputed_with: a numeric value or None for missing
                value imputation.
            
        Returns:
            A DataMatrix object with quantitative columns as float values.
        """
        if by_name:
            col_idx = self.col_names_to_col_idx(quantitative_columns)
        else:
            col_idx = quantitative_columns
        for row in self.data:
            for j in col_idx:
                row[j] = float(row[j]) if row[j] != "" and \
                    row[j] != "NA" and row[j] != "None" and \
                    row[j] != None else missing_value_imputed_with
        return self

    def correct_isotopic_impurity_for_itraq_4plex(self, 
                                                  itraq_intensity_col_names,
                                                  correction_matrix = None): 
        """Corrects iTRAQ reporter ion intensitites due to isotopic impurity.
        
        Args:
            itraq_intensity_col_names: itraq intensity column names, 
                such as ["X114", "X115", "X116", "X117"]
            correction_matrix: a numpy.array of the correction matrix, the intensity row
                vector will dot multiply this matrix from the left. The missing values
                of the intensity vector will be considered as zero. 
                If correction_matrix = None, then the default value is used.
            
        Returns:
            A DataMatrix object with iTRAQ intensities corrected.
        """   
        # http://sourceforge.net/p/sashimi/code/HEAD/tree/trunk/trans_proteomic_pipeline/src/Quantitation/Libra/docs/libra_info.html
        isotopic_contribution_matrix = numpy.array([[1, 0.063, 0, 0], 
                                                    [0.02, 1, 0.06, 0],
                                                    [0, 0.03, 1, 0.049],
                                                    [0, 0, 0.04, 1]])
        ## Proteome Discoverer -- iTRAQ 4-plex isotopic contribution -- Thermo instruments
        isotopic_contribution_matrix = numpy.array([[0.9452, 0.049, 0, 0], 
                                                    [0.0059, 0.9477, 0.0464, 0],
                                                    [0, 0.0138, 0.9584, 0.0278],
                                                    [0, 0, 0.0213, 0.9518]])
        if correction_matrix == None:
            correction_matrix = linalg.inv(isotopic_contribution_matrix)

        col_idx = self.col_names_to_col_idx(itraq_intensity_col_names)
        for row in self.data:
            missing_value_mask = [(1.0 if row[j] != "" and \
                                       row[j] != "NA" and row[j] != "None" and \
                                       row[j] != None else 0.0) \
                                      for j in col_idx]
            values = [(float(row[j]) if row[j] != "" and \
                           row[j] != "NA" and row[j] != "None" and \
                           row[j] != None else 0.0) \
                          for j in col_idx]
            corrected_values = numpy.dot(numpy.array(values), correction_matrix).tolist()
            k = 0
            for j in col_idx:
                row[j] = corrected_values[k] * missing_value_mask[k] if \
                    corrected_values[k] >= 0 else 0.0
                k += 1
        return self


    def correct_isotopic_impurity_for_itraq(self, 
                                            itraq_intensity_col_names,
                                            correction_matrix):
        """Corrects iTRAQ reporter ion intensitites due to isotopic impurity.
        
        Args:
            itraq_intensity_col_names: itraq intensity column names, 
                such as ["X114", "X115", "X116", "X117"]
            correction_matrix: a numpy.array of the correction matrix, the intensity row
                vector will dot multiply this matrix from the left. The missing values
                of the intensity vector will be considered as zero.
            
        Returns:
            A DataMatrix object with iTRAQ intensities corrected.
        """
        col_idx = self.col_names_to_col_idx(itraq_intensity_col_names)
        for row in self.data:
            missing_value_mask = [(1.0 if row[j] != "" and \
                                       row[j] != "NA" and row[j] != "None" and \
                                       row[j] != None else 0.0) \
                                      for j in col_idx]
            values = [(float(row[j]) if row[j] != "" and \
                           row[j] != "NA" and row[j] != "None" and \
                           row[j] != None else 0.0) \
                          for j in col_idx]
            corrected_values = numpy.dot(numpy.array(values), correction_matrix).tolist()
            k = 0
            for j in col_idx:
                row[j] = corrected_values[k] * missing_value_mask[k] if \
                    corrected_values[k] >= 0 else 0.0
                k += 1
        return self


    def get_statistics(self, quantitative_col_names, func):
        """Computes statistics of quantitative columns using the provided function.
        
        Args:
            quantitative_col_names: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"]
            func: the function used for computing the statistics. For example,
                sum, numpy.median, numpy.mean, numpy.sum.
            
        Returns:
            A list of the statistics of these quantitative columns.
        """
        col_idx = self.col_names_to_col_idx(quantitative_col_names)
        stats = [[] for j in col_idx]
        for row in self.data:
            k = 0
            for j in col_idx:
                stats[k].append(row[j])
                k += 1

        for k in range(len(stats)):
            # imputed_values = [(float(x) if x != "" and x != "NA" and \
            #                        x != None else 0.0) for x in stats[k] if x != None]
            # stats[k] = func(imputed_values) if imputed_values else None
            stats[k] = func(stats[k]) if stats[k] else None

        return stats 
        
    def get_statistics_omit_zeroes(self, quantitative_col_names, func):
        """Computes statistics of quantitative columns using the provided function.
        
        Args:
            quantitative_col_names: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"]
            func: the function used for computing the statistics. For example,
                sum, numpy.median, numpy.mean, numpy.sum.
            
        Returns:
            A list of the statistics of these quantitative columns.
        """
        col_idx = self.col_names_to_col_idx(quantitative_col_names)
        stats = [[] for j in col_idx]
        for row in self.data:
            k = 0
            for j in col_idx:
                if row[j] > 0:
                    stats[k].append(row[j])
                
                k += 1

        for k in range(len(stats)):
            # imputed_values = [(float(x) if x != "" and x != "NA" and \
            #                        x != None else 0.0) for x in stats[k] if x != None]
            # stats[k] = func(imputed_values) if imputed_values else None
            stats[k] = func(stats[k]) if stats[k] else None

        return stats 
    
    def get_medians_omit_none_values(self, quantitative_col_names):
        """Computes numpy.median of quantitative columns.  None values are omitted from the median calculation
        
        Args:
            quantitative_col_names: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"]
            
        Returns:
            A list of the statistics of these quantitative columns.
        """
        col_idx = self.col_names_to_col_idx(quantitative_col_names)
        stats = [[] for j in col_idx]
        for row in self.data:
            k = 0
            for j in col_idx:
                # omits None values
                if row[j] != None:
                    stats[k].append(row[j])
                    
                k += 1

        for k in range(len(stats)):
            # imputed_values = [(float(x) if x != "" and x != "NA" and \
            #                        x != None else 0.0) for x in stats[k] if x != None]
            # stats[k] = func(imputed_values) if imputed_values else None
            stats[k] = numpy.median(stats[k]) if stats[k] else None

        return stats 

    def normalize_quantitative_columns(self, 
                                       quantitative_col_names, 
                                       normalization_factors):
        """Normalizes quantitative columns by multiplying normalization factors 
        respectively.
        
        Args:
            quantitative_col_names: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"]
            normalization_factors: a list of normalization factors. Its length
                is the same as that of quantitative_col_names.
            
        Returns:
            A DataMatrix object with quantitative columns normalized.
        """
        col_idx = self.col_names_to_col_idx(quantitative_col_names)
        for row in self.data:
            k = 0
            for j in col_idx:
                if normalization_factors[k] != None:
                    row[j] = float(row[j]) * normalization_factors[k]
                    
                k += 1
        return self

    def log2_ratio_with_virtual_reference(self, numerator_col_names, denominator_col_name, quant_col_names):
        """Calculates log2 ratios using the virtual reference.
        
        Args:
            numerator_col_names: a list of column names that are used as
                numerators in log ratios, such as ["X115", "X116", "X117"].
            denominator_col_name: a list of the column name of demoninator. 
                For example, ["X114"].
            quant_col_names: a list of all quantitative column names.
                For example, ["X114", "X115", "X116", "X117"]
            
        Returns:
            A DataMatrix object with log2 ratio columns appended to the rightmost 
            of the table.
        """
        n_col_idx = self.col_names_to_col_idx(numerator_col_names)
        d_col_idx = self.col_names_to_col_idx(denominator_col_name)[0]
        q_col_idx = self.col_names_to_col_idx(quant_col_names)
        for row in self.data:
            # calculate the virtual reference as the median of all numerator columns, zero values are omitted
            denominator = [float(row[j]) for j in q_col_idx if row[j] > 0]
            denominator = float(numpy.median(denominator)) if denominator else 0
            if denominator > 1e-10:
                logratio_values = [(math.log(float(row[j]) / denominator, 2) if row[j] and row[j] > 1e-10 else None) for j in n_col_idx]
            else:
                logratio_values = [None for j in n_col_idx]
            row.extend(logratio_values)

        self.col_names.extend(["log2ratio_" + self.col_names[j] + "_" + \
                                   self.col_names[d_col_idx] for j in n_col_idx])
        return self
    
    def log2_ratio(self, numerator_col_names, denominator_col_name):
        """Calculates log2 ratios.
        
        Args:
            numerator_col_names: a list of column names that are used as
                numerators in log ratios, such as ["X115", "X116", "X117"].
            denominator_col_name: a list of the column name of demoninator. 
                For example, ["X114"].
            
        Returns:
            A DataMatrix object with log2 ratio columns appended to the rightmost 
            of the table.
        """
        n_col_idx = self.col_names_to_col_idx(numerator_col_names)
        d_col_idx = self.col_names_to_col_idx(denominator_col_name)[0]
        for row in self.data:
            denominator = float(row[d_col_idx]) if row[d_col_idx] else 0
            if denominator > 1e-10:
                logratio_values = [(math.log(float(row[j]) / denominator, 2) if row[j] and row[j] > 1e-10 else None) for j in n_col_idx]
            else:
                logratio_values = [None for j in n_col_idx]
            row.extend(logratio_values)

        self.col_names.extend(["log2ratio_" + self.col_names[j] + "_" + \
                                   self.col_names[d_col_idx] for j in n_col_idx])
        return self

    def md_normalize_columns(self, 
                            quantitative_col_names, 
                            log2_medians):
        """Normalizes quantitative columns by subtracting itraq_medians 
        respectively.
        
        Args:
            quantitative_col_names: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"]
            log2_medians: a list of medians for each channel in the set.
            
        Returns:
            A DataMatrix object with quantitative columns normalized.
        """
        col_idx = self.col_names_to_col_idx(quantitative_col_names)
        for row in self.data:
            k = 0
            for j in col_idx:
                if row[j] != None and log2_medians[k] != None:
                    row[j] = float(row[j]) - log2_medians[k]
                else:
                    row[j] = None
                    
                k += 1
        return self
    
    def mad_scale_columns(self, 
                            quantitative_col_names, 
                            log2_medians):
        """Normalizes quantitative columns by subtracting itraq_medians 
        respectively.
        
        Args:
            quantitative_col_names: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"]
            log2_medians: a list of medians for each channel in the set.
            
        Returns:
            A DataMatrix object with quantitative columns normalized.
        """
        log2_medians_remove_none = [x for x in log2_medians if x != None]
        median_log2_medians = numpy.median(log2_medians_remove_none) if log2_medians_remove_none else None
        
        col_idx = self.col_names_to_col_idx(quantitative_col_names)
        abs_deviations = [[] for x in range(len(log2_medians))]
        for row in self.data:
            k = 0
            for j in col_idx:
                if row[j] != None:
                    abs_deviations[k].append(abs(row[j]))
                    
                k += 1
                
        abs_deviations = [numpy.median(x) if x!= list() else None for x in abs_deviations]
        abs_deviations_remove_none = [x for x in abs_deviations if x != None]
        median_abs_deviations = numpy.median(abs_deviations_remove_none) if abs_deviations_remove_none else None
        
        for row in self.data:
            k = 0
            for j in col_idx:
                if row[j] != None:
                    if abs_deviations[k] != None and median_abs_deviations != None and median_log2_medians != None:
                        if abs_deviations[k] > 1e-10:
                            row[j] = ((row[j] / abs_deviations[k]) * median_abs_deviations) + median_log2_medians
                        else:
                            # substitutes a very small value (1e-10) for abs_deviations[k], since the median absolute deviation is ~0, in order to apply MAD scaling
                            row[j] = ((row[j] / 1e-10) * median_abs_deviations) + median_log2_medians
                    else:
                        raise Exception('abs_deviations[' + str(k) + '] or median_abs_deviations or median_log2_medians = None')
                
                k += 1
                    
        return self
    
    def select_columns(self, selected_columns, by_name = True):
        """Select columns. This operation also alters the original data matrix.
        
        Args:
            selected_columns: a list of column names to be seleced,
                such as ["X115", "X116", "X117"], if by_name is True. If by_name
                is False, a list of column indices are expected, such as [0, 1, 5].
            by_name: a boolean value, indicating whether to select the columns by
                their column names (default True). If False, a list of column indices
                are used (0-based index).
            
        Returns:
            A DataMatrix object of the selected columns of the original data matrix.
        """
        if by_name == True:
            col_idx = self.col_names_to_col_idx(selected_columns)
        else:
            col_idx = selected_columns
        selected_data = []
        for row in self.data:
            values = [row[i] for i in col_idx]
            selected_data.append(values)
        self.data = selected_data
        if by_name == True:
            self.col_names = copy.copy(selected_columns)
        else:
            self.col_names = [self.col_names[j] for j in col_idx]
        return self

 
    def group_by(self, group_by_col_name, func, quantitative_columns, by_name = True):
        """Aggregate data by the column indicated by group_by_col_name, and take the 
           aggregation function over the grouped values in quantitative columns.
        
        Args:
            group_by_col_name: a list of a single column name that is used for
                aggregation, for example, ["Sequence"], if by_name is True. If by_name
                is False, an integer of column index is expected, such as 3.
            func: a function for aggregating the values in a group in each 
                  quantitative columns, respectively.
            quantitative_columns: a list of quantitative column names, 
                such as ["X114", "X115", "X116", "X117"], if by_name is True. 
                If by_name is False, a list of column indices is expected, 
                such as [1, 2, 3].
            by_name: a boolean value, indicating whether to select the column by
                their column names (default True). If False, a column index
                is used (0-based index).
            
        Returns:
            A DataMatrix object after data aggregation (group-by operation) 
            of the table.
        """
        if by_name:
            group_by_idx = self.col_names_to_col_idx(group_by_col_name)[0]
            col_idx = self.col_names_to_col_idx(quantitative_columns)
        else:
            group_by_idx = group_by_col_name
            col_idx = quantitative_columns
        self.data.sort(key = lambda x: x[group_by_idx])

        aggregated_data = []
        aggregated_row = []
        group = None
        for row in self.data:
            if row[group_by_idx] != group:
                if aggregated_row:
                    # this if-statement skips the first row / first iteration,
                    # because only the first iteration has aggregated_row = [].
                    for j in col_idx:
                        imputed_values = [x for x in aggregated_row[j] if x != None]
                        aggregated_row[j] = func(imputed_values) if imputed_values else None
                    aggregated_row[group_by_idx] = group
                    aggregated_data.append(aggregated_row)
                group = row[group_by_idx]
                aggregated_row = [[] for j in range(len(self.col_names))]
            for j in range(len(self.col_names)):
                aggregated_row[j].append(row[j])

        # After the loop exits, the following block deals with the last group.
        for j in col_idx:
            imputed_values = [x for x in aggregated_row[j] if x != None]
            aggregated_row[j] = func(imputed_values) if imputed_values else None
        aggregated_row[group_by_idx] = group
        aggregated_data.append(aggregated_row)

        self.data = aggregated_data
        return self
            
    def filter_rows(self, func, filter_col_name):
        """Filters the rows of the data matrix by applying the func on 
        filter_col_name column. If the function returns true, then the 
        row is retained.
        
        Args:
            func: a function for used filtering, which returns true if a row 
                is to be retained.
            filter_col_name: a list of a single column name that is used for
                filtering, for example, ["Sequence"].
            
        Returns:
            A DataMatrix object with rows filtered.
        """
        filter_col_idx = self.col_names_to_col_idx(filter_col_name)[0]
        self.data = [row for row in self.data if func(row[filter_col_idx])]
        return self


    def transform_column(self, func, col_name):
        """Transform the elements in the column using func.
        
        Args:
            func: a function for data transformation in the column.
            col_name: a list of a single column name that is used for
                data transformation, for example, ["Sequence"].
            
        Returns:
            A DataMatrix object with data in the column transformed.
        """
        col_idx = self.col_names_to_col_idx(col_name)[0]
        self.data = [[(row[j] if j != col_idx else func(row[j])) \
                          for j in range(len(row))] for row in self.data]
        return self



    def sort_rows_by_key(self, key_col_name, reverse = False):
        """Sorts the rows of the data matrix by the values of the key column.
        
        Args:
            key_col_name: a list of a single column name that is used for
                sorting, for example, ["Sequence"].
            reverse: a boolean value: True if sorted in reverse order.
            
        Returns:
            A DataMatrix object with rows sorted.
        """
        key_col_idx = self.col_names_to_col_idx(key_col_name)[0]
        self.data.sort(key = lambda x: x[key_col_idx], reverse = reverse)
        return self


    def column_to_list(self, column, by_name = True):
        """Returns a list of the elements in a selected column.
        
        Args:
            column: a list of a single column name to be selected, 
                for example, ["Sequence"], if by_name is True. If by_name
                is False, an integer of column index is expected, such as 3.
            by_name: a boolean value, indicating whether to select the column by
                their column names (default True). If False, a column index
                is used (0-based index).
            
        Returns:
            A list of the elements in a selected column.
        """

        if by_name == True:
            col_idx = self.col_names_to_col_idx(column)[0]
        else:
            col_idx = column
        column = [row[col_idx] for row in self.data]
        return column


    def insert_column(self, new_column, new_col_name, index = 0):
        """Inserts a new column to the data matrix.
        
        Args:
            new_column: a list of elements to be inserted as the new column.
            new_col_name: a string of the new column name.
            index: the index of the column before which to insert.
            
        Returns:
            A DataMatrix object with the new column inserted.

        Raises:
            ValueError: if the length of the new column is different from
                the length (number of rows ) of the data matrix.
        """
        if len(new_column) != len(self.data):
            raise ValueError("The length of the new column is different" + \
                                 " from the length of the data matrix.")
        for row, new_element in zip(self.data, new_column):
            row.insert(index, new_element)
        self.col_names.insert(index, new_col_name)
        return self
        

    def column_bind_data_matrix_by_key(self, another_dm, key_col_name):
        """Column-wise binds two data matrices by matching keys.
        
        Args:
            another_dm: another data matrix to be combined to the right of 
                this data matrix.
            key_col_name: a list of a single column name that is used as key for
                merging the two data matrix, for example, ["Sequence"].
            
        Returns:
            A DataMatrix object after combining the two data matrices.

        Raises:
            ValueError: if the values in the key column are not unique.
        """
        key_col_idx1 = self.col_names_to_col_idx(key_col_name)[0]
        key_col_idx2 = another_dm.col_names_to_col_idx(key_col_name)[0]

        key_data_map1 = {row[key_col_idx1]: [row[j] for j in range(len(row)) if j != key_col_idx1] for row in self.data}
        key_data_map2 = {row[key_col_idx2]: [row[j] for j in range(len(row)) if j != key_col_idx2] for row in another_dm.data}
        if len(key_data_map1) != len(self.data):
            raise ValueError("Keys are not unique in the first data matrix.")
        if len(key_data_map2) != len(another_dm.data):
            raise ValueError("Keys are not unique in the second data matrix.")

        keys = sorted(set(key_data_map1) | set(key_data_map2))

        combined_data = []
        for k in keys:
            combined_row = [k]
            if k in key_data_map1:
                combined_row.extend(key_data_map1[k])
            else:
                combined_row.extend([None for j in range(len(self.col_names) - 1)])
            if k in key_data_map2:
                combined_row.extend(key_data_map2[k])
            else:
                combined_row.extend([None for j in range(len(another_dm.col_names) - 1)])
            combined_data.append(combined_row)

        self.data = combined_data
        key_name = self.col_names[key_col_idx1]
        self.col_names = [self.col_names[j] for j in range(len(self.col_names)) if j != key_col_idx1]
        self.col_names.extend([another_dm.col_names[j] for j in range(len(another_dm.col_names)) if j != key_col_idx2])
        self.col_names.insert(0, key_name)
        return self


    def col_bind_data_matrix(self, another_dm):
        """Column-wise binds two data matrices.
        
        Args:
            another_dm: another data matrix to be combined to the right of
                this data matrix.
            
        Returns:
            A DataMatrix object after combining the two data matrices.

        Raises:
            ValueError: if the numbers of row of two data matrices do not match.
        """
        if len(self.data) != len(another_dm.data):
            raise ValueError("The numbers of rows of the two data matrices differ.")
        for row1, row2 in zip(self.data, another_dm.data):
            row1.extend(row2)
        self.col_names.extend(another_dm.col_names)
        return self


    def row_bind_data_matrix(self, another_dm):
        """Row-wise binds two data matrices.
        
        Args:
            another_dm: another data matrix to be combined to the bottom of
                this data matrix.
            
        Returns:
            A DataMatrix object after combining the two data matrices.

        Raises:
            ValueError: if the col_names of two data matrices do not match or 
               the numbers of columns in two data matrices differ.
        """
        if len(self.col_names) != len(another_dm.col_names):
            raise ValueError("The col_names of two data matrices do not match.")
        for x, y in zip(self.col_names, another_dm.col_names):
            if x != y:
                raise ValueError("The col_names of two data matrices do not match.")
        if len(self.data[0]) != len(another_dm.data[0]):
            raise ValueError("The numbers of columns in two data matrices differ.")
        self.data.extend(another_dm.data)
        return self