"""
Description:
    Uses Myrimatch to search the fasta database and convert the mzml files to pepxml
    files.
    
Notes: 
    - Myrimatch configuration file is used.
"""

import os
import sys
import glob
import time
import shutil
import subprocess
import multiprocessing
import tkMessageBox
import p3b_mzml_only_charge_injection_time as p3b
from Tkinter import *
from datetime import datetime
from hotpot.pathing import make_path
from hotpot.read_write import write_tmp_config as wtc
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
myrimatch_path = pipeline_path + 'myrimatch\\'
tmp_path = pipeline_path + 'tmp\\'

def main(configs): 
    print '\nStep 3:\tUsing Myrimatch to search the fasta file and create pepxml files...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    
    # no decoy specific parameter to modify in the myrimatch.cfg file to force a concatenated decoy search (assuming MyriMatch does this automatically)
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_path = options['archived_path']
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    fasta_file = options['fasta_path']
    myrimatch_file = myrimatch_path + 'myrimatch.exe'
    myrimatch_config = config_path + 'myrimatch.cfg'
    num_threads = str(multiprocessing.cpu_count())
    
    search_files = glob.glob(mzid_path + '*.*')
    
    result_type = ''
    archive_results = False
    for search_f in search_files:
        file_ext = str(search_f.split('.')[-1]).lower()
        
        if file_ext == 'mzid' or file_ext == 'xml':
            archive_results = True
            
            if file_ext == 'mzid':
                result_type = 'msgfplus'
            elif file_ext == 'xml':
                result_type = 'comet'
            
            break
            
    if archive_results == True:
        current_time = str(datetime.now().strftime('%Y%m%d_%H%M%S'))
        archived_path = output_path + 'archived\\' + result_type + '\\' + current_time + '\\'
        make_path(archived_path)
        
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Notice!', 'Because you have chosen a different search engine from previously searched data, the previous results will be archived in ' + archived_path)
        root.destroy()
        
        shutil.copytree(mzid_path, archived_path + 'step3-search_results\\')
        shutil.rmtree(mzid_path, ignore_errors=True)
        
        if os.path.exists(output_path + 'configs\\'):
            shutil.copytree(output_path + 'configs\\', archived_path + 'configs\\')
            shutil.rmtree(output_path + 'configs\\', ignore_errors=True)
            shutil.copytree(config_path, output_path + 'configs\\')
            
        if os.path.exists(output_path + 'quality_control\\'):
            shutil.copytree(output_path + 'quality_control\\', archived_path + 'quality_control\\')
            shutil.rmtree(output_path + 'quality_control\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'logs\\'):
            shutil.copytree(output_path + 'logs\\', archived_path + 'logs\\')
            shutil.rmtree(output_path + 'logs\\', ignore_errors=True)
        
        if os.path.exists(output_path + 'step4a-psm_quantitation\\'):
            shutil.copytree(output_path + 'step4a-psm_quantitation\\', archived_path + 'step4a-psm_quantitation\\')
            shutil.rmtree(output_path + 'step4a-psm_quantitation\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'step4b-protein_inference\\'):
            shutil.copytree(output_path + 'step4b-protein_inference\\', archived_path + 'step4b-protein_inference\\')
            shutil.rmtree(output_path + 'step4b-protein_inference\\', ignore_errors=True)
        
        if os.path.exists(output_path + 'step6-peptide_protein_quantitation\\'):
            shutil.copytree(output_path + 'step6-peptide_protein_quantitation\\', archived_path + 'step6-peptide_protein_quantitation\\')
            shutil.rmtree(output_path + 'step6-peptide_protein_quantitation\\', ignore_errors=True)
        
        if os.path.exists(output_path + 'step5-decoy\\'):
            shutil.copytree(output_path + 'step5-decoy\\', archived_path + 'step5-decoy\\')
            shutil.rmtree(output_path + 'step5-decoy\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'step7-expression_matrix\\'):
            shutil.copytree(output_path + 'step7-expression_matrix\\', archived_path + 'step7-expression_matrix\\')
            shutil.rmtree(output_path + 'step7-expression_matrix\\', ignore_errors=True)
    
    # creates the output path folders
    make_path(mzid_path)
    
    # grabs all mzml filenames
    mzml_files = glob.glob(mzml_path + '*.mzML')
    
    edit_myrimatch_config_fasta(fasta_file)
    
    for mzml_file in mzml_files:  
        # the filename of the potential mzml file to be created
        mzid_filename = os.path.basename(mzml_file)[:-5] + '.pepxml'
        
        # if the particular mzid file does not already exist, then the mzml file is converted to mzid via MSGFPlus
        if not os.path.exists(mzid_path + mzid_filename):
            subprocess.call(myrimatch_file + ' -cfg ' + myrimatch_config + ' -workdir ' + mzid_path + ' -cpus ' + num_threads + ' ' + mzml_file)
            
    sys.stdout.flush()
    p3b.main(configs) 
    
    write_config_params = []
    write_config_params.append('mzml_path = ' + mzml_path)
    write_config_params.append('fasta_path = ' + fasta_file)
    write_config_params.append('search_engine = ' + 'Myrimatch')
    write_config_outfile = mzid_path + 'step3.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'myrimatch.cfg', write_config_outfile, 'myrimatch.cfg settings')

# ******************************************************************************      

def edit_myrimatch_config_fasta(fasta_file):
    wtc(tmp_path + 'myrimatch.cfg', config_path + 'myrimatch.cfg')
            
    infile = open(tmp_path + 'myrimatch.cfg', 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    outfile = open(config_path + 'myrimatch.cfg', 'w')
        
    for line in infile_lines:
        if line[0:len('ProteinDatabase')] == 'ProteinDatabase':
            outfile.write('ProteinDatabase = "' + fasta_file + '"\n')
        else:
            outfile.write(line)
                    
    outfile.close()
        
    os.remove(tmp_path + 'myrimatch.cfg')
        
# ******************************************************************************
                              
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['backup'] = True
    
    main(configs)