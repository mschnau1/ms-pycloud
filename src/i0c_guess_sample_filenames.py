import os
import re
import glob
import tkMessageBox
from Tkinter import *
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'

def main(configs):
    options = configs['options']
    
    raw_path = options['raw_data']
    num_samples = int(options['num_raw_samples'])
    
    raw_data = glob.glob(raw_path + '*.raw')
    
    names = []
    new_itraq_names = []
    
    for raw in raw_data:
        split_name = os.path.basename(raw).split('.raw')[0]
        names.append(split_name)
        new_itraq_names.append('')
    
    exit_flag = False
    
    while len(set(new_itraq_names)) <= num_samples and exit_flag == False:
        itraq_names = list(set(new_itraq_names))
        
        exit_flag = True
        
        for index, name in enumerate(names):
            if name != '':
                new_itraq_names[index] += name[0]
                names[index] = name[1:]
                exit_flag = False
    
    name_splits = {}
    itraq_copy = list(itraq_names)
    
    while len(set(name_splits.keys())) < num_samples:
        itraq_names = list(itraq_copy)
        
        name_splits = {}
        break
        
        for index, name in enumerate(itraq_copy):
            name_split = re.split('([^a-zA-Z0-9]+)', name)
            key = name_split[-1]
            
            if key in name_splits.keys():
                name_splits[key].append(name_split)
            else:
                name_splits[key] = []
                name_splits[key].append(name_split)
               
        itraq_copy = []
                
        for end in name_splits.keys():
            entry = name_splits[end]
            if len(entry) > 1:
                for name_part in entry:
                    del(name_part[-2:])
                    itraq_copy.append(''.join(name_part))
            else:
                itraq_copy.append(''.join(entry[0]))
    
    if len(itraq_names) != num_samples:
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Alert!', 'Unable to guess ' + str(num_samples) + ' unique sample filenames!')
        root.destroy()
        return ''
    else:
        print itraq_names

if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    
    main(configs)