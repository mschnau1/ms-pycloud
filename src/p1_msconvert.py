"""
Description:
    Uses ProteoWizard's MSConvert tool to convert raw (.raw) data to .mzML format.

Notes: 
    - MSConvert default settings are used.
    - num_cores is user specified in the steps.txt file.
"""

import os
import sys
import glob
import time
import shutil
import subprocess
from multiprocessing import Pool
from multiprocessing import cpu_count
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.read_write import write_lines as wl
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import write_to_msconvert_config as cmdwrite

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
tmp_path = pipeline_path + 'tmp\\'

def main(configs):         
    print '\nStep 1:\tConverting raw data files to mzML format...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
    wl(tmp_path + 'msconvert.txt', configs['msconvert']) 
    
    # sets variables to configuration values
    data_root = options['raw_data']
    mzml_path = data_root + 'step1-mzml\\'
    msconvert_file = exes['msconvert']
    msconfig = tmp_path + 'msconvert.txt'
    num_thread = int(cpu_count()) - 1
    
    if num_thread == 0:
        num_thread = 1
    
    # adds the command to the configuration file
    cmdwrite(msconfig, 'outdir=' + mzml_path)
    
    # creates the output path folders
    make_path(mzml_path)
    
    # gathers all of the raw data filenames in the data_root folder
    raw_files = glob.glob(data_root + '*.raw')
    
    # creates lists of the msconvert_file, mzml_path and config for multiprocessing
    msconvert_file = [msconvert_file] * len(raw_files)
    mzml_path = [mzml_path] * len(raw_files)
    msconfig = [msconfig] * len(raw_files)
    
    pool = Pool(processes = num_thread)
    pool.map(msconvert_in_parallel, zip(raw_files, msconvert_file, msconfig, mzml_path))
    
    write_config_params = []
    write_config_params.append('raw_data = ' + data_root)
    write_config_outfile = data_root + 'step1-mzml\\step1.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(tmp_path + 'msconvert.txt', write_config_outfile, 'msconvert.txt settings')
    
    os.remove(tmp_path + 'msconvert.txt')

# ******************************************************************************
                         
def msconvert_in_parallel((raw_file, msconvert_file, msconfig, mzml_path)):
    """
    Description: Makes multiple calls to msconvert at once for converting raw files
    
    Args:
        - raw_file: one of the raw data files
        - msconvert_file: the folder path to the msconvert executable
        - msconfig: the msconvert configuration file
        - mzml_path: the folder path for the resulting mzml files
    """
    # the filename of the potential mzml file to be created
    mzml_filename = os.path.basename(raw_file)[:-4] + '.mzml'
    
    # if the particular mzml file does not already exist, then the raw file is converted to mzml
    if not os.path.exists(mzml_path + mzml_filename):
        # runs msconvert from the command prompt
        subprocess.call(msconvert_file + ' ' + raw_file + \
            ' -c ' + msconfig)

# ******************************************************************************
          
if __name__ == '__main__':      
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['msconvert'] = rl(config_path + 'msconvert.txt')
    configs['backup'] = True
    
    main(configs)