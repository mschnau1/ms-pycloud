import os
import re
import sys
import glob
import time
import shutil
import subprocess
from multiprocessing import Pool
from multiprocessing import cpu_count
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.configuring import read_steps_config as steps
from hotpot.md5_checksums import raw_file_md5_checksum

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
quameter_path = pipeline_path + 'quameter\\'
documentation_path = pipeline_path + 'documentation\\'

qc_quameter_columns = ['Filename', 'MS2-Count', 'MS2-PrecZ-1', 'MS2-PrecZ-2', 'MS2-PrecZ-3', 'MS2-PrecZ-4', 'MS2-PrecZ-5' ]

def main(configs):  
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    generate_raw_checksums(configs)
    idfree_quameter(configs)
    #id_quameter(configs)
        
def generate_raw_checksums(configs):
    options = configs['options']
    
    data_root = options['raw_data']
    output_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2]
    qc_path = output_path + 'quality_control\\'
    
    # creates the output path folders
    make_path(qc_path)
    
    run_flag = False
    raw_files = glob.glob(data_root + '*.raw')
    
    for raw_file in raw_files:
        basename = os.path.basename(raw_file).replace('.raw', '')
        
        if not os.path.exists(data_root + basename + '.md5.cksum'):
            run_flag = True
            break
    
    if run_flag:
        print '\nGenerating md5 checksums for each raw file...'
        print '-' * 150
        sys.stdout.flush()
        
        for raw_file in raw_files:
            basename = os.path.basename(raw_file).replace('.raw', '')
            
            if not os.path.exists(data_root + basename + '.md5.cksum'):
                raw_file_md5_checksum(raw_file, data_root)
        
    # combine individual  raw file .md5.cksum files into one file for easy viewing
    raw_cksums = glob.glob(data_root + '*.md5.cksum')
    
    if raw_cksums != []:
        raw_cksums.sort(key = natural_keys)
        
        output_data = []
        for raw_cksum in raw_cksums:
            g = open(raw_cksum, 'r')
            cksum_lines = g.readlines()
            g.close()
            
            # ignore header row
            cksum_info = cksum_lines[1]
            output_data.append(cksum_info.strip())
            
        f = open(qc_path + 'qc-md5-' + data_set_name + '.cksum', 'w')
        cksum_header = 'Filename' + '\t' + 'MD5 checksum' + '\n'
        f.write(cksum_header)
        
        for row in output_data:
            f.write(row.strip() + '\n')
        
        f.close()
        
def idfree_quameter(configs):
    options = configs['options']
    sample_filenames = configs['sample filenames']
    sample_filenames.sort(key = natural_keys)
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_path = options['archived_path']
    qc_path = output_path + 'quality_control\\'
    data_set_name = data_root.split('\\')[-2]
    num_thread = int(cpu_count()) - 1
    
    if num_thread == 0:
        num_thread = 1
        
    # creates the output path folders
    make_path(qc_path)
    
    # process .raw files through idfree quameter
    raw_files = glob.glob(data_root + '*.raw')
    
    run_flag = False
    
    qual_tsv_files = []
    for raw_file in raw_files:
        qual_tsv_file = raw_file.replace('.raw', '.qual.tsv')
        qual_tsv_files.append(qual_tsv_file)
        
        if not os.path.exists(qual_tsv_file) and run_flag == False:
            run_flag = True
    
    if run_flag == True:
        print '\nStep 0:\tGenerating quality control (qc) data for each raw file...'
        print '-' * 150
        sys.stdout.flush()
        
        pool = Pool(processes = num_thread)
        pool.map(idfree_in_parallel, zip(raw_files, qual_tsv_files))
        
    # combine idfree tsv results into one tsv file
    tsv_files = glob.glob(data_root + '*.qual.tsv')
    
    if tsv_files != []:
        tsv_files.sort(key = natural_keys)
        
        f = open(tsv_files[0], 'r')
        lines = f.readlines()
        f.close()
        
        header_line = lines[0]
        
        output_data = [header_line]
        
        qc_header_line = ''
        for header_col in header_line.split('\t'):
            if header_col in qc_quameter_columns:
                qc_header_line += header_col + '\t'
            
        qc_data = [qc_header_line]
        
        for tsv_file in tsv_files:
            current_filename = str(os.path.basename(tsv_file).replace('.qual.tsv', ''))
            
            if sample_filenames != list():
                for sample_filename in sample_filenames:
                    if sample_filename in current_filename:
                        f = open(tsv_file, 'r')
                        lines = f.readlines()
                        f.close()
                        
                        header_line = lines[0]
                        val_line = lines[1]
                        
                        output_data.append(val_line)
                        
                        header_split = header_line.strip().split('\t')
                        val_split = val_line.strip().split('\t')
                        
                        qc_line = ''
                        for qc_col in qc_quameter_columns:
                            data_col = header_split.index(qc_col)
                            qc_line += val_split[data_col] + '\t'
                
                        qc_data.append(qc_line)
            
                        break
            else:
                f = open(tsv_file, 'r')
                lines = f.readlines()
                f.close()
                
                header_line = lines[0]
                val_line = lines[1]
                
                output_data.append(val_line)
                
                header_split = header_line.strip().split('\t')
                val_split = val_line.strip().split('\t')
                
                qc_line = ''
                for qc_col in qc_quameter_columns:
                    data_col = header_split.index(qc_col)
                    qc_line += val_split[data_col] + '\t'
        
                qc_data.append(qc_line)
            
        # generate total idfree quameter file
        f = open(data_root + data_set_name + '_combined_idfree_quameter_results.tsv', 'w')
        for row in output_data:
            f.write(row.strip() + '\n')
        
        f.close()
        
        # generate preliminary qc file
        f = open(qc_path + 'qc-quameter-' + data_set_name + '.tsv', 'w')
        for row in qc_data:
            f.write(row.strip() + '\n')
        
        f.close()
        
# ******************************************************************************
                         
def idfree_in_parallel((raw_file, qual_tsv_file)):
    """
    Description: Makes multiple calls to quameter
    
    Args:
        - raw_file: one of the raw data files
        - qual_tsv_file: qual_tsv_file output path
    """
    # if the particular quameter file does not already exist, then the raw file is run through quameter
    if not os.path.exists(qual_tsv_file):
        # runs quameter from the command prompt
        command = quameter_path + 'quameter ' + str(raw_file) + ' -MetricsType idfree'    
        print command
        sys.stdout.flush()
        subprocess.call(command)

# ******************************************************************************

def id_quameter(configs):
    options = configs['options']
    sample_filenames = configs['sample filenames']
    sample_filenames.sort(key = natural_keys)
    
    # sets variables to configuration values
    data_root = options['raw_data']
    data_set_name = data_root.split('\\')[-2]
    pepxml_path = data_root + 'step3-search_results\\'
    fasta_file = options['fasta_path']
    num_thread = int(cpu_count()) - 1
    
    if num_thread == 0:
        num_thread = 1
    
    # process .pepXML files through id quameter
    pepxml_files = glob.glob(pepxml_path + '*.pepXML')
    
    run_flag = False
    
    idpdb_files = []
    for pepxml_file in pepxml_files:
        idpdb_file = pepxml_file.replace('.pepXML', '.idpDB')
        idpdb_files.append(idpdb_file)
        
        if not os.path.exists(idpdb_file) and run_flag == False:
            run_flag = True
    
    if run_flag == True:
        print '\nStep 0:\tGenerating quality control (qc) data for each pepXML file...'
        print '-' * 150
        sys.stdout.flush()
        
        fasta_files = [fasta_file] * len(pepxml_files)
        data_roots = [data_root] * len(pepxml_files)
        
        pool = Pool(processes = num_thread)
        pool.map(id_in_parallel, zip(pepxml_files, fasta_files, idpdb_files, data_roots))         
            
    # combine id tsv results into one tsv file
    tsv_files = glob.glob(pepxml_path + '*.qual.tsv')
    
    if tsv_files != []:
        tsv_files.sort(key = natural_keys)
        
        f = open(tsv_files[0], 'r')
        lines = f.readlines()
        f.close()
        
        header_line = lines[0]
        output_data = [header_line]
        
        for tsv_file in tsv_files:
            current_filename = str(os.path.basename(tsv_file).replace('.qual.tsv', ''))
            
            if sample_filenames != list():
                for sample_filename in sample_filenames:
                    if sample_filename in current_filename:
                        f = open(tsv_file, 'r')
                        lines = f.readlines()
                        f.close()
                        
                        val_line = lines[1]
                
                        output_data.append(val_line)
                        break
            else:
                f = open(tsv_file, 'r')
                lines = f.readlines()
                f.close()
                
                val_line = lines[1]
        
                output_data.append(val_line)  
            
        f = open(pepxml_path + data_set_name + '_combined_id_quameter_results.tsv', 'w')
        for row in output_data:
            f.write(row.strip() + '\n')
        
        f.close()
        
# ******************************************************************************
                         
def id_in_parallel((pepxml_file, fasta_file, idpdb_file, data_root)):
    # if the particular quameter file does not already exist, then the pepxml file is run through quameter
    if not os.path.exists(idpdb_file):
        command = quameter_path + 'idpqonvert ' + str(pepxml_file) + ' -ProteinDatabase ' + fasta_file
        print command
        sys.stdout.flush()
        subprocess.call(command)
        
        qual_tsv_file = idpdb_file.replace('.idpDB', '.qual.tsv')
        
        # runs quameter from the command prompt
        command = quameter_path + 'quameter ' + str(idpdb_file) + ' -RawDataPath ' + data_root + ' -OutputFilepath ' + qual_tsv_file
        print command
        sys.stdout.flush()
        subprocess.call(command)
    
# ******************************************************************************

def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]

if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)