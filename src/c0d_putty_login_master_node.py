import os
import sys
import subprocess
import tkMessageBox
from Tkinter import *
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_key_location as sc_key_location

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
install_path = pipeline_path + 'installs\\'

def main(configs):
    print '\nStep c0d: Logging into PuTTY...'
    print '-' * 150
    sys.stdout.flush()
    
    options = configs['options']
    exes = configs['exes']
    sc_key, sc_keys = sc_key_info(starcluster_config_path)
    sc_key_path = sc_key_location(starcluster_config_path, sc_key)
    
    # sets variables to configuration values
    cluster_name = 'puttycluster_' + str(options['cluster_name']).strip()
    user_id = options['cluster_user_id']
    pem_key = sc_key_path
    ppk_key = pem_key.replace('.pem', '.ppk')
    python2_path = os.path.dirname(exes['python']) + '\\'
    starcluster_file = python2_path + 'Scripts\\starcluster.exe'
    putty_file = exes['putty']
    
    putty_path = os.path.dirname(putty_file) + '\\'
    
    if not os.path.exists(ppk_key):
        ppk_command = putty_path + 'puttygen ' + pem_key
        
        print ppk_command
        sys.stdout.flush()
        subprocess.call(ppk_command)
    
    # starts the starcluster instance for uploading data - will be terminated once data is uploaded
    start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name + ' --cluster-size=1 --master-instance-type=t2.micro'
    print start_command
    sys.stdout.flush()
    subprocess.call(start_command)
    
    # retrieves the public dns of the master node
    ip_command = starcluster_file + ' -c ' + starcluster_config_path + ' sshmaster ' + cluster_name + ' GET http://169.254.169.254/latest/meta-data/public-hostname' 
    
    print ip_command
    sys.stdout.flush()
    vals = subprocess.Popen(ip_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    ip, errors = vals.communicate()
    print ip
    vals.wait()
    vals.terminate()
    master_ip = user_id + '@' + str(ip).strip()
    
    putty_command = putty_path + 'putty -ssh ' + master_ip + ' 22 -i ' + ppk_key
    print putty_command
    sys.stdout.flush()
    proc = subprocess.Popen(putty_command)
    print proc.communicate("y\n")
    proc.wait()
    proc.terminate()
    
    print ''
    sys.stdout.flush()
    
    terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
    print terminate_command
    sys.stdout.flush()
    proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
    proc.wait()
    proc.terminate()
    
    print '-' * 150
    print cluster_name + ' has been terminated!'
    sys.stdout.flush()
    
    print '\nSee pipeline graphical user interface to continue...'
    sys.stdout.flush()
    
    root = Tk()
    root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
    root.update_idletasks()
    tkMessageBox.showinfo('Done!', cluster_name + ' has been terminated!')
    root.destroy()
    
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    
    main(configs)