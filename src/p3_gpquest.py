"""
Description:
    Uses GPQuest to search and convert the mzml files to csv files.
    
Notes: 
    - gpquest_params.json configuration file is used.
"""

import os
import sys
import glob
import time
import shutil
import subprocess
import p3w_convert_gpquest_csv_to_tsv as p3w
import p3b_mzml_only_charge_injection_time as p3b
#import hotpot.gpquest_create_protein_decoy_database as gpquest_decoy
from Tkinter import *
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
msgf_path = pipeline_path + 'msgfplus\\'
tmp_path = pipeline_path + 'tmp\\'

def main(configs): 
    print '\nStep 3:\tUsing GPQuest to search the mzML files...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
    
        configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    fasta_file = config_path + 'gpquest_params.json'
    java_file = exes['python3']
    #java_path = os.path.dirname(java_file) + '\\'
    msgf_jar_file = pipeline_path + 'GPQuest21.py'
    #msgf_jar_file = java_path + 'Scripts\\gpquest.exe'
            
    # creates the output path folders
    make_path(mzid_path)    
    
    # create the GPQuest decoy Protein database if necessary
    database_file = read_gpquest_params_key(fasta_file, '"PROTEIN":').replace('"', '')
    database_split = database_file.split('.')
    decoy_database_file = database_split[0] + '_decoys.csv'
    target_decoy_database_file = database_split[0] + '_target_decoys.txt'
    
    if not os.path.exists(decoy_database_file) or not os.path.exists(target_decoy_database_file):
        print('\nGPQuest is creating the decoy databases...')
        print('-' * 150)
        sys.stdout.flush()
        
        #gpquest_decoy.main(input_files = None, output_dir = None, param_file = fasta_file)
        subprocess.call(java_file + ' ' + msgf_jar_file + ' ' + 'None' + ' ' + mzid_path + ' ' + fasta_file)
        #subprocess.call(msgf_jar_file + ' -d ' + 'None' + ' -o ' + mzid_path + ' -p ' + fasta_file)
        #subprocess.call(msgf_jar_file + ' -o ' + mzid_path + ' -p ' + fasta_file)
    
    # grabs all mzml filenames
    mzml_files = glob.glob(mzml_path + '*.mzML')
    
    for mzml_file in mzml_files:
        mzml_filename = os.path.basename(mzml_file)
        
        # the filename of the potential mzml file to be created
        mzid_filename = mzml_filename[:-5] + '.csv'
        
        # if the particular csv file does not already exist, then the mzml file is converted to csv via GPQuest
        if not os.path.exists(mzid_path + mzid_filename):
            subprocess.call(java_file + ' ' + msgf_jar_file + ' ' + mzml_file + ' ' + mzid_path + ' ' + fasta_file)
            #subprocess.call(msgf_jar_file + ' -d ' + mzml_path + ' -o ' + mzid_path + ' -p ' + fasta_file)
                
    # convert csv results to tsv
    p3w.main(configs)
    
    sys.stdout.flush()
    p3b.main(configs)
    
    write_config_params = []
    write_config_params.append('mzml_path = ' + mzml_path)
    write_config_params.append('search_engine = ' + 'GPQuest')
    write_config_outfile = mzid_path + 'step3.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'gpquest_params.json', write_config_outfile, 'gpquest_params.json settings')

# ******************************************************************************

def read_gpquest_params_key(filepath, key):
    # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
    infile = open(filepath, 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    val = ''
    for line in infile_lines:
        line = line.strip()
        
        if line[:len(key)] == key:
            val = line.split(key)[1].strip()
            if val[-1] == ',':
                val = val.split(',')[0].strip()
                    
    if val != '':
        return val
    else:
        raise Exception(str(key) + ' was not found in ' + str(filepath) + '.')
  
# ******************************************************************************
               
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)