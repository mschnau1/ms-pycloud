"""
Description:
     
    
Notes:
    - min_psm_per_peptide and min_peptide_per_protein are set to 1 and 1 respectively.
"""

import os
import sys
import glob
import json
import time
import shutil
import traceback
import hotpot.n_glycopeptide as glycopeptide
from copy import deepcopy
from hotpot.reversed_decoy import *
from hotpot import data_matrix
from hotpot import protein_identification
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.configuring import read_steps_config as steps
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.md5_checksums import step5_search_results_md5_checksums as search_md5

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'

def main(configs):
    print '\nStep 5:\tUse the reversed decoy fasta file to estimate decoys...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    inference_options = configs['inference_search']
    sample_filenames = sorted(configs['sample filenames'])
    
    # sets variables to configuration values
    data_root = options['raw_data']  
    output_folder_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2] 
    mzid_path = data_root + 'step3-search_results\\'
    qc_path = output_folder_path + 'quality_control\\'
    protein_fasta_file = options['fasta_path']
    min_psm_per_peptide = int(options['min_psm_per_peptide'])
    min_peptide_per_protein = int(options['min_peptide_per_protein'])
    fdr = float(options['false_discovery_rate']) / 100.0
    optional_n_term_met_cleavage = str(options['optional_n_term_met_cleavage']).lower()
    cleavage_rule = str(inference_options['cleavage_rule'])
    max_miscleavage = int(inference_options['max_miscleavage'])
    min_length = int(inference_options['min_length'])
    max_length = int(inference_options['max_length'])
    min_cleavage_terminus = int(inference_options['min_cleavage_terminus'])
    mod = str(options['modification']).lower()
    
    output_path = output_folder_path + 'step5-decoy\\'
    output_path_exists = os.path.exists(output_path)
    
    valid_extensions = ['mzid', 'pepxml', 'pep.xml', 'gpquest.tsv']
    
    if optional_n_term_met_cleavage == 'true':
        optional_n_term_met_cleavage = True
    else:
        optional_n_term_met_cleavage = False
    
    mzid_file_ext = ''    
    mzid_path_files = glob.glob(mzid_path + '*.*')
    for mzid_f in mzid_path_files:
        file_ext = str('.'.join(mzid_f.split('.')[1:]))
        
        if file_ext.lower() in valid_extensions:    
            if mzid_file_ext == '':
                mzid_file_ext = file_ext
                break
                
    # handle single shot data
    if sample_filenames == list():
        mzid_files = glob.glob(mzid_path + '*.' + mzid_file_ext)
        for mzid_f in mzid_files:
            mzid_f_basename = os.path.basename(mzid_f).replace('.' + mzid_file_ext, '')
            
            if mzid_f_basename not in sample_filenames:
                sample_filenames.append(mzid_f_basename)
                
        # removes non-unique names in case a set file was left over from previous analysis
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)
    
    # creates the output path folders
    make_path(output_path)
    make_path(qc_path)
    
    run_code = False
    move_md5 = False
    # number of header lines in md5 checksum files
    md5_header = 1
    search_md5_filename = 'step5_search_md5_checksums.tsv'
    
    files_should_exist = [output_path + 'decoy_peptide_protein.txt', output_path + 'reversed_decoy_log.log', qc_path + 'stats-number_log_decoy-' + data_set_name + '.tsv', qc_path + 'stats-fdr_log-' + data_set_name + '.log', output_path + 'step5.config']
    
    if not os.path.exists(mzid_path + search_md5_filename) or output_path_exists == False:
        search_md5(mzid_path, mzid_path, sample_filenames)
        
        run_code = True
    else:
        try:
            for filepath in files_should_exist:
                if not os.path.exists(filepath):
                    run_code = True
            
            f = open(mzid_path + search_md5_filename, 'r')
            previous_md5_lines = f.readlines()
            f.close()
            
            search_md5(mzid_path, output_path, sample_filenames)
                
            f = open(output_path + search_md5_filename, 'r')
            current_md5_lines = f.readlines()
            f.close()
            
            previous_md5 = {}
            for line in previous_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                previous_md5[filename] = checksum
                
            current_md5 = {}
            for line in current_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                current_md5[filename] = checksum
            
            for key in current_md5:
                if key not in previous_md5:
                    run_code = True        
                elif previous_md5[key] != current_md5[key]:
                    run_code = True
                        
            for key in previous_md5:
                if key not in current_md5:
                    run_code = True
                elif current_md5[key] != previous_md5[key]:
                    run_code = True
       
            move_md5 = True
        except:
            if os.path.exists(output_path + search_md5_filename):
                os.remove(output_path + search_md5_filename)
                
            search_md5(mzid_path, mzid_path, sample_filenames)
            
            run_code = True
    
    if run_code == True:
        mzid_path_files = glob.glob(mzid_path + '*.' + mzid_file_ext)
        mzid_files = []
            
        for mzid_f in mzid_path_files:
            current_filename = str(os.path.basename(mzid_f).replace('.' + mzid_file_ext, ''))
            
            for sample_filename in sample_filenames: 
                if sample_filename in current_filename:
                    mzid_files.append(mzid_f)
                    break
        
        # begin number_log file creation
        # create log file with the number of mzid files used for generating the decoy_peptide_protein.txt file
        # report how many fractions were used for the generating the decoy_peptide_protein.txt file so that the user can verify pipeline worked correctly
        f = open(qc_path + 'stats-number_log_decoy-' + data_set_name + '.tsv', 'w')
        f.write('Filename\t# fraction files\n')
        f.write('decoy_peptide_protein' + '\t' + str(len(mzid_files)))
        f.close()
        # end number_log file creation
        
        search_engine = steps(mzid_path + 'step3.config')['search_engine'].lower()
        
        decoy_peptide_psm_count_map = {}
        n_glyco_decoy_peptide_psm_count_map = {}
        o_glyco_decoy_peptide_psm_count_map = {}
        
        if mod != 'intact':
            pep_spectra_count_map = get_decoy_peptide_psm_count_map(mzid_files, search_engine = search_engine, psm_fdr = fdr, mod = mod)
        else:
            (pep_spectra_count_map, n_glyco_pep_spectra_count_map, o_glyco_pep_spectra_count_map) = get_decoy_peptide_psm_count_map(mzid_files, search_engine = search_engine, psm_fdr = fdr, mod = mod)
        
        for p in pep_spectra_count_map:
            if p in decoy_peptide_psm_count_map:
                decoy_peptide_psm_count_map[p] += pep_spectra_count_map[p]
            else:
                decoy_peptide_psm_count_map[p] = pep_spectra_count_map[p]
                
        if mod == 'intact':
            for p in n_glyco_pep_spectra_count_map:
                if p in n_glyco_decoy_peptide_psm_count_map:
                    n_glyco_decoy_peptide_psm_count_map[p] += n_glyco_pep_spectra_count_map[p]
                else:
                    n_glyco_decoy_peptide_psm_count_map[p] = n_glyco_pep_spectra_count_map[p]
                    
            for p in o_glyco_pep_spectra_count_map:
                if p in o_glyco_decoy_peptide_psm_count_map:
                    o_glyco_decoy_peptide_psm_count_map[p] += o_glyco_pep_spectra_count_map[p]
                else:
                    o_glyco_decoy_peptide_psm_count_map[p] = o_glyco_pep_spectra_count_map[p]
        
        print(len(decoy_peptide_psm_count_map))
        print(sum(decoy_peptide_psm_count_map.values()))
        
        if mod == 'intact':
            print '-'
            print len(n_glyco_decoy_peptide_psm_count_map)
            print sum(n_glyco_decoy_peptide_psm_count_map.values())
            
            print '-'
            print len(o_glyco_decoy_peptide_psm_count_map)
            print sum(o_glyco_decoy_peptide_psm_count_map.values())
            print ''
        
        #reversed_decoy_fasta_file = os.path.dirname(protein_fasta_file) + '\\' + os.path.basename(protein_fasta_file).replace('.fasta', '.revCat.fasta')
        reversed_decoy_fasta_file = protein_fasta_file + ".mspycloud.reversed"
        
        # create reversed decoy fasta file
        if not os.path.exists(reversed_decoy_fasta_file):
            create_reversed_protein_sequence_fasta(protein_fasta_file, reversed_decoy_fasta_file)
        
        # no suffix unless intact glycopeptide data 
        file_suffix = ''   
        estimate_decoy(decoy_peptide_psm_count_map, output_path, qc_path, data_set_name, 
                    reversed_decoy_fasta_file,
                    min_psm_per_peptide = min_psm_per_peptide,
                    min_peptide_per_protein = min_peptide_per_protein,
                    cleavage_rule = cleavage_rule,
                    max_miscleavage = max_miscleavage,
                    min_length = min_length, 
                    max_length = max_length,
                    min_cleavage_terminus = min_cleavage_terminus,
                    optional_n_term_met_cleavage = optional_n_term_met_cleavage,
                    mod = mod,
                    file_suffix = file_suffix,
                    fdr = fdr)
                    
        if mod == 'intact':
            print '-'
            # suffix for the N-glycopeptide decoy files
            file_suffix = '-nglycoform'   
            estimate_decoy(n_glyco_decoy_peptide_psm_count_map, output_path, qc_path, data_set_name, 
                        reversed_decoy_fasta_file,
                        min_psm_per_peptide = min_psm_per_peptide,
                        min_peptide_per_protein = min_peptide_per_protein,
                        cleavage_rule = cleavage_rule,
                        max_miscleavage = max_miscleavage,
                        min_length = min_length, 
                        max_length = max_length,
                        min_cleavage_terminus = min_cleavage_terminus,
                        optional_n_term_met_cleavage = optional_n_term_met_cleavage,
                        mod = mod,
                        file_suffix = file_suffix,
                        fdr = fdr)
                        
            print '-'
            # suffix for the O-glycopeptide decoy files
            file_suffix = '-oglycoform'   
            estimate_decoy(o_glyco_decoy_peptide_psm_count_map, output_path, qc_path, data_set_name, 
                        reversed_decoy_fasta_file,
                        min_psm_per_peptide = min_psm_per_peptide,
                        min_peptide_per_protein = min_peptide_per_protein,
                        cleavage_rule = cleavage_rule,
                        max_miscleavage = max_miscleavage,
                        min_length = min_length, 
                        max_length = max_length,
                        min_cleavage_terminus = min_cleavage_terminus,
                        optional_n_term_met_cleavage = optional_n_term_met_cleavage,
                        mod = mod,
                        file_suffix = file_suffix,
                        fdr = fdr)
        
        write_config_params = []
        write_config_params.append('search_results_path = ' + mzid_path)
        write_config_params.append('fasta_path = ' + protein_fasta_file)
        write_config_params.append('reversed_fasta_file = ' + reversed_decoy_fasta_file)
        write_config_params.append('min_psm_per_peptide = ' + str(min_psm_per_peptide))
        write_config_params.append('min_peptide_per_protein = ' + str(min_peptide_per_protein))
        write_config_params.append('optional_n_term_met_cleavage = ' + str(optional_n_term_met_cleavage))
        write_config_params.append('cleavage_rule = ' + cleavage_rule)
        write_config_params.append('max_miscleavage = ' + str(max_miscleavage))
        write_config_params.append('min_cleavage_terminus = ' + str(min_cleavage_terminus))
        write_config_params.append('min_length = ' + str(min_length))
        write_config_params.append('max_length = ' + str(max_length))
        write_config_params.append('false_discovery_rate = ' + str(fdr * 100.0) + '%')
        write_config_outfile = output_path + 'step5.config'
        
        write_mspy(write_config_params, write_config_outfile)
        write_tp(config_path + 'sample_filenames.txt', write_config_outfile, 'sample_filenames.txt settings')
        
    if move_md5 == True:
        # move new search_md5_checksums.tsv from psm_quantitation folder to the search_results folder
        os.remove(mzid_path + search_md5_filename)
        shutil.move(output_path + search_md5_filename, mzid_path + search_md5_filename)

# ******************************************************************************

def estimate_decoy(decoy_peptide_psm_count_map, output_path, qc_path, data_set_name,
                   reversed_decoy_fasta_file,
                   min_psm_per_peptide = 2,
                   min_peptide_per_protein = 2,
                   cleavage_rule = 'Trypsin',
                   max_miscleavage = 1,
                   min_length = 6, max_length = 50,
                   min_cleavage_terminus = 2,
                   optional_n_term_met_cleavage = True,
                   mod = 'global',
                   file_suffix = '',
                   fdr = 1.0):

    
    log_message = {}
    
    log_message['num_decoy_peptide_before_filtering'] = len(decoy_peptide_psm_count_map)
    log_message['num_decoy_psm_before_filtering'] = sum(decoy_peptide_psm_count_map.values())

    decoy_peptide_psm_count_map = deepcopy(decoy_peptide_psm_count_map)

    decoy_peptide_psm_count_map = protein_identification.filter_by_psms_per_peptide(
        decoy_peptide_psm_count_map, 
        min_psm_per_peptide = min_psm_per_peptide)
                                                       
    log_message['num_decoy_peptide_after_filtering'] = len(decoy_peptide_psm_count_map)
    log_message['num_decoy_psm_after_filtering'] = sum(decoy_peptide_psm_count_map.values())

    peptide_set = decoy_peptide_psm_count_map.keys()
    #peptide_set_original = decoy_peptide_psm_count_map.keys()
    #
    #if mod == 'glyco':
    #    # filters out NXS/T motif peptides because of glyco modification
    #    peptide_set = glycopeptide.select_glycopeptide(peptide_set_original)
    #elif mod == 'phospho':
    #    # filtered in reversed_decoy.get_decoy_peptide_psm_count_map()
    #    peptide_set = peptide_set_original
    #elif mod == 'global':
    #    peptide_set = peptide_set_original
    #else:
    #    raise Exception("Modification must be either 'global', 'glyco', or 'phospho'.")
        
    print('len(decoy_peptide_set)', len(peptide_set))

    num_decoy_psm = 0
    num_decoy_peptides = 0
    num_decoy_proteins = 0
    if mod != 'intact':
        peptide_assignment = protein_identification.protein_grouping(
            peptide_set,
            reversed_decoy_fasta_file, 
            min_peptide_per_protein = min_peptide_per_protein,
            cleavage_rule = cleavage_rule,
            max_miscleavage = max_miscleavage,
            min_length = min_length, max_length = max_length,
            min_cleavage_terminus = min_cleavage_terminus,
            optional_n_term_met_cleavage = optional_n_term_met_cleavage,
            log_message = log_message)
    
        # post-protein grouping statistics
        assigned_peptides = set()
        protein_groups = set()
        for s, p, t in peptide_assignment:
            assigned_peptides.add(s)
            protein_groups.add(p)
    
        num_decoy_psm = 0
        for p in decoy_peptide_psm_count_map:
            if p in assigned_peptides:
                num_decoy_psm += decoy_peptide_psm_count_map[p]
    
        num_decoy_peptides = len(assigned_peptides)
        num_decoy_proteins = len(protein_groups)
        
        with open(output_path + 'decoy_peptide_protein' + file_suffix + '.txt', 'w') as f:
            for s, p, t in peptide_assignment:
                f.write('\t'.join([s, p, t]))
                f.write('\n')
    else:
        assigned_peptides = set()
        for s in peptide_set:
            assigned_peptides.add(s)
            
        num_decoy_psm = 0
        for p in decoy_peptide_psm_count_map:
            num_decoy_psm += decoy_peptide_psm_count_map[p]
            
        num_decoy_peptides = len(assigned_peptides)
        num_decoy_proteins = 0
        
        with open(output_path + 'decoy_peptide_protein' + file_suffix + '.txt', 'w') as f:
            for s in peptide_set:
                f.write('\t'.join([s, 'NA', 'NA']))
                f.write('\n')

    log_message['final_decoy_peptide_num'] = num_decoy_peptides
    log_message['final_decoy_psm_num'] = num_decoy_psm
    log_message['final_protein_group_num'] = num_decoy_proteins
    
    with open(output_path + 'reversed_decoy_log' + file_suffix + '.log', 'w') as f:
        f.write(json.dumps(log_message, sort_keys=True, indent=4) + '\n')
    
    if mod != 'intact' or (mod == 'intact' and file_suffix == ''):
        # calculate psm, peptide, and protein level fdr for the total protein inference results
        fdr_output = []
        
        psm_fdr = -1
        peptide_fdr = -1
        protein_fdr = -1
        
        try:
            protein_inf_path = output_path.replace('step5-decoy\\', 'step4b-protein_inference\\')    
            
            if os.path.isfile(protein_inf_path + 'peptide_protein_log.log'):
                f_pep = open(protein_inf_path + 'peptide_protein_log.log', 'r')
                inf_lines = f_pep.readlines()
                f_pep.close()
            else:
                inf_lines = []
                fdr_output.append('peptide_protein_log.log file is missing from the protein_inference folder.  Please rerun Step 4. \n\n') 
                        
            for line in inf_lines:
                line = line.strip()
                
                if ':' in line:
                    line = line.split(':')
                    line_key = str(line[0].strip())
                    line_val = str(line[1].strip())
                
                    if line_val[-1] == ',':
                        line_val = float(line_val[:-1])
                    else:
                        line_val = float(line_val)
                    
                    if line_key == '"final_psm_num"':
                        psm_fdr = (float(num_decoy_psm) / (line_val)) * 100.0
                    elif line_key == '"final_peptide_num"':
                        peptide_fdr = (float(num_decoy_peptides) / (line_val)) * 100.0
                    elif line_key == '"final_protein_group_num"':
                        # GPQuest uses a different database for decoys (random vs reverse) and so the decoy proteins for intact glyco data cannot be inferred
                        if mod != 'intact':
                            protein_fdr = (float(num_decoy_proteins) / (line_val)) * 100.0
                        else:
                            protein_fdr = -1
                            
                if psm_fdr != -1 and peptide_fdr != -1:
                    if mod != 'intact':
                        if protein_fdr != -1:
                            break
                    else:
                        break
        except:
            print '-'
            print 'Error with p5_decoy_estimation.estimate_decoy()...'
            e = traceback.format_exc()
            print e
            print '-'
            sys.stdout.flush()
                
            print '\nAn error occurred while calculating the psm, peptide, and protein fdr.'
            sys.stdout.flush()
            
            fdr_output.append('Unexpected error: \n' + str(e) + '\n\n')
        
        fdr_output.append('PSM fdr = ' + str(psm_fdr) + ' %\n')
        fdr_output.append('Peptide fdr = ' + str(peptide_fdr) + ' %\n')
        fdr_output.append('Protein fdr = ' + str(protein_fdr) + ' %\n')           
        
        f = open(qc_path + 'stats-fdr_log-' + data_set_name + '.log', 'w')
        for fdr_line in fdr_output:
            f.write(fdr_line)
        f.close()
            
# ******************************************************************************
               
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)
