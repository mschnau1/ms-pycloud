import os
import sys
import traceback
import subprocess
import tkMessageBox
from Tkinter import *
from hotpot.read_write import write_tmp_config as wtc
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_key_location as sc_key_location

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
msgf_path = pipeline_path + 'msgfplus\\'
gpquest_path = pipeline_path + 'GPQuest21.py'
gpquest_hotpot_path = pipeline_path + 'hotpot\\gpquest_utils.py'
anaconda_path = pipeline_path + 'anaconda\\'
tmp_path = pipeline_path + 'tmp\\'
install_path = pipeline_path + 'installs\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
scripts_path = pipeline_path + 'shell_scripts\\'

def main(configs):
    print '\nStep c0a: Creating the new EBS...'
    print '-' * 150
    sys.stdout.flush()
    
    options = configs['options']
    exes = configs['exes']
    ebs_params = configs['ebs_params']
    sc_key, sc_keys = sc_key_info(starcluster_config_path)
    sc_key_path = sc_key_location(starcluster_config_path, sc_key)
    
    cluster_name = str(options['cluster_name']).strip()
    volume_name = str(ebs_params['volume_name'])
    volume_size = str(ebs_params['volume_size'])
    volume_zone = str(ebs_params['volume_zone'])
    mount_path = str(ebs_params['mount_path'])
    pem_key = sc_key_path
    ppk_key = pem_key.replace('.pem', '.ppk')
    user_id = options['cluster_user_id']
    python2_path = os.path.dirname(exes['python']) + '\\'
    starcluster_file = python2_path + 'Scripts\\starcluster.exe'
    putty_file = exes['putty']
    putty_path = os.path.dirname(putty_file) + '\\'
    
    try:
        # format mount_path
        if mount_path[0] != '/':
            mount_path = '/' + mount_path
        if mount_path[-1] != '/':
            mount_path += '/'
    
        if not os.path.exists(ppk_key):
            ppk_command = putty_path + 'puttygen ' + pem_key
            
            print ppk_command
            sys.stdout.flush()
            subprocess.call(ppk_command)
        
        id_command = starcluster_file + ' -c ' + starcluster_config_path + ' listvolumes --name ' + volume_name   
        print id_command
        sys.stdout.flush()
        vals = subprocess.Popen(id_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
        
        vol_id, errors = vals.communicate()
        vals.wait()
        vals.terminate()
        vol_id = vol_id.strip()
        
        if int(str(vol_id).count('volume_id:')) >= 1: 
            print ''
            sys.stdout.flush()
                   
            print '-' * 150
            print 'A volume already exists with the specified name (' + str(volume_name) + ').  Please specify ' + \
                   'a different "Volume name" and then retry creating a new EBS volume.'
            sys.stdout.flush()
            
            print '\nSee pipeline graphical user interface to continue...\n'
            sys.stdout.flush()
            
            root = Tk()
            root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
            root.update_idletasks()
            tkMessageBox.showinfo('Error!', 'A volume already exists with the specified name (' + str(volume_name) + ').  Please specify ' + \
                                  'a different "Volume name" and then retry creating a new EBS volume.')
            root.destroy()
            
            return 
        
        print ''
        sys.stdout.flush()
        
        create_new_ebs_command = starcluster_file + ' -c ' + starcluster_config_path + ' createvolume -n ' + volume_name + ' -k ' + sc_key + ' ' + volume_size + ' ' + volume_zone
        print create_new_ebs_command
        sys.stdout.flush()
        subprocess.call(create_new_ebs_command)
        
        print ''
        sys.stdout.flush()
        
        id_command = starcluster_file + ' -c ' + starcluster_config_path + ' listvolumes --name ' + volume_name   
        print id_command
        sys.stdout.flush()
        vals = subprocess.Popen(id_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
        
        vol_id, errors = vals.communicate()
        vals.wait()
        vals.terminate()
        vol_id = vol_id.strip()
        volume_id = vol_id.split('volume_id:')[1].split('\n')[0].strip()
        
        current_params = {}
        current_params['volume_name'] = volume_name
        current_params['volume_size'] = volume_size
        current_params['volume_zone'] = volume_zone
        current_params['volume_id'] = volume_id
        current_params['mount_path'] = mount_path
        
        # update starcluster config parameters
        write_params_to_starcluster(current_params)
        update_config('ebs_params.txt', 'volume_choice', volume_name)
        
        print ''
        sys.stdout.flush()
        
        ebs_terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force volumecreator'
        print ebs_terminate_command
        sys.stdout.flush()
        proc = subprocess.Popen(ebs_terminate_command, stdin = subprocess.PIPE, shell = True)
        proc.wait()
        proc.terminate()
        
        write_mspycloud_dir(scripts_path = scripts_path, mount_path = mount_path)
        
        print ''
        sys.stdout.flush()
        
        # starts the starcluster instance for uploading mspycloud code
        start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name + ' --cluster-size=1 --master-instance-type=t2.micro'
        print start_command
        sys.stdout.flush()
        subprocess.call(start_command)
        
        print ''
        sys.stdout.flush()
        
        # retrieves the public dns of the master node
        ip_command = starcluster_file + ' -c ' + starcluster_config_path + ' sshmaster ' + cluster_name + ' GET http://169.254.169.254/latest/meta-data/public-hostname'
        print ip_command
        sys.stdout.flush()
        vals = subprocess.Popen(ip_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
        ip, errors = vals.communicate()
        vals.wait()
        vals.terminate()
        
        master_ip = user_id + '@' + str(ip).strip()
        
        # commands to put items on master node 
        plink_dirs_command = putty_path + 'plink ' + master_ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'create_mspycloud_dir.sh'
        print plink_dirs_command
        sys.stdout.flush()
        proc = subprocess.Popen(plink_dirs_command, stdin = subprocess.PIPE, shell = False)
        print proc.communicate("y\n")
        proc.wait()
        proc.terminate()
        
        print ''
        sys.stdout.flush()
        
        # upload necessary files for MS-GF+/Myrimatch/Comet cloud processing
        cloud_pipeline_path = mount_path + 'mspycloud/'
        cloud_msgf_path  = cloud_pipeline_path + 'msgfplus/'
        
        # copy MS-GF+/Myrimatch/Comet pipeline code to the ebs
        put_msgf_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + msgf_path + ' ' + cloud_msgf_path
        print put_msgf_command
        sys.stdout.flush()
        subprocess.call(put_msgf_command, shell = True)
        
        print ''
        sys.stdout.flush()
        
        # upload necessary files for GPQuest cloud processing
        cloud_pipeline_path = mount_path + 'gpquest/'
        cloud_gpquest_path  = cloud_pipeline_path + 'software/'
        cloud_gpquest_hotpot_path = cloud_gpquest_path + 'hotpot/'
        cloud_anaconda_path = cloud_pipeline_path + 'anaconda/'
        
        # copy GPQuest pipeline code to the ebs
        put_msgf_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + gpquest_path + ' ' + cloud_gpquest_path
        print put_msgf_command
        sys.stdout.flush()
        subprocess.call(put_msgf_command, shell = True)
        
        print ''
        sys.stdout.flush()
        
        # copy GPQuest hotpot code to the ebs
        put_msgf_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + gpquest_hotpot_path + ' ' + cloud_gpquest_hotpot_path
        print put_msgf_command
        sys.stdout.flush()
        subprocess.call(put_msgf_command, shell = True)
        
        print ''
        sys.stdout.flush()
        
        # copy anaconda to the ebs
        anaconda_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + anaconda_path + ' ' + cloud_anaconda_path
        print anaconda_command
        sys.stdout.flush()
        subprocess.call(anaconda_command, shell = True)
        
        print ''
        sys.stdout.flush()
        
        terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
        print terminate_command
        sys.stdout.flush()
        proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
        proc.wait()
        proc.terminate()
        
        print '-' * 150
        print 'New EBS created!'
        sys.stdout.flush()
        
        print '\nSee pipeline graphical user interface to continue...\n'
        sys.stdout.flush()
        
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Done!', 'New EBS created!')
        root.destroy()
    except:
        # terminate clusters
        ebs_terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force volumecreator'
        print ebs_terminate_command
        sys.stdout.flush()
        proc = subprocess.Popen(ebs_terminate_command, stdin = subprocess.PIPE, shell = True)
        proc.wait()
        proc.terminate()
        
        print ''
        sys.stdout.flush()
        
        terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
        print terminate_command
        sys.stdout.flush()
        proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
        proc.wait()
        proc.terminate()
        
        print '-'
        print 'Error with c0a.create_new_ebs.main()...'
        print traceback.format_exc()
        print '-'
        sys.stdout.flush()  
    
def update_config(config_file, param_name, param):
    wtc(tmp_path + config_file, config_path + config_file)
    
    infile = open(tmp_path + config_file, 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    outfile = open(config_path + config_file, 'w')
    
    for line in infile_lines:
        if line[0:len(param_name)] == param_name:
            comments = line.split('#')[1:]
            
            comment = '\t\t'
            for section in comments:
                comment += '#' + section
                
            comment = comment.rstrip()
            
            outfile.write(param_name + ' = ' + str(param) + comment + '\n')
        else:
            outfile.write(line)
            
    outfile.close()
    
    os.remove(tmp_path + config_file)

def read_new_ebs_config(filepath):
    ebs_params = steps(filepath)
    
    return ebs_params
    
def write_params_to_starcluster(params):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    name_exists = False
    
    for line in lines:
        line = str(line).strip()
        if line == '[volume ' + params['volume_name'] + ']':
            name_exists = True
        elif line != '':
            if line[0] == '#':
                if line.split('#')[1].strip() == '[volume ' + params['volume_name'] + ']':
                    name_exists = True
    
    cluster_section_flag = False
    volume_section_flag = False
    correct_volume_flag = False
    added_volume = False
    
    starcluster_output_data = []
    for line in lines:
        line = str(line).strip()
        
        if line == '[cluster mspycloud]':
            cluster_section_flag = True
            
        if line == '## Defining Additional Cluster Templates ##':
            cluster_section_flag = False
            
        if line == '# Sections starting with "volume" define your EBS volumes':
            volume_section_flag = True
            
        if line == '## Configuring Security Group Permissions ##':
            volume_section_flag = False
        
        if cluster_section_flag == True:
            split_line = line.split('=')    
            
            key = split_line[0].strip().lower()
            if key == '':
                new_line = line
            elif key == 'volumes':
                new_line = 'VOLUMES = ' + params['volume_name']  
            elif key[0] == '#':
                if key.split('#')[1].strip() == 'volumes':
                    new_line = 'VOLUMES = ' + params['volume_name']
                else:
                    new_line = line
            else:
                new_line = line
        elif volume_section_flag == True:
            if name_exists == False:
                if added_volume == False:
                    new_line = line + '\n'
                    new_line += '[volume ' + params['volume_name'] + ']\n'
                    new_line += 'VOLUME_ID = ' + params['volume_id'] + '\n'
                    new_line += 'MOUNT_PATH = ' + params['mount_path'] + '\n'
                    added_volume = True
                else:
                    new_line = line
            elif name_exists == True:  
                if correct_volume_flag == True: 
                    if line.split(' ')[0].strip() == '[volume':
                        correct_volume_flag = False
                    elif line != '':
                        if line[0] == '#':
                            if line.split('#')[1].strip().split(' ')[0].strip() == '[volume':
                                correct_volume_flag = False
                    
                if line == '[volume ' + params['volume_name'] + ']':
                    correct_volume_flag = True
                elif line != '':
                    if line[0] == '#':
                        if line.split('#')[1].strip() == '[volume ' + params['volume_name'] + ']':
                            correct_volume_flag = True
                
                if correct_volume_flag == True:
                    if line == '############################################':
                        correct_volume_flag = False
                        new_line = line
                    elif line == '':
                        new_line = line
                    elif line.split('=')[0].strip().lower() == 'volume_id':
                        new_line = 'VOLUME_ID = ' + params['volume_id']
                    elif line.split('=')[0].strip().lower() == 'mount_path':
                        new_line = 'MOUNT_PATH = ' + params['mount_path']
                    elif line[0] == '#':
                        if line.split('#')[1].strip().split('=')[0].strip().lower() == 'volume_id':
                            new_line = 'VOLUME_ID = ' + params['volume_id']
                        elif line.split('#')[1].strip().split('=')[0].strip().lower() == 'mount_path':
                            new_line = 'MOUNT_PATH = ' + params['mount_path']
                        elif line.split('#')[1].strip() == '[volume ' + params['volume_name'] + ']':
                            new_line = line[1:].strip()
                        else:
                            new_line = line
                    else:
                        new_line = line
                else:
                    new_line = line
                         
        else:
            new_line = line
                
        starcluster_output_data.append(new_line + '\n')
    
    f = open(starcluster_config_path, 'w')
    for starcluster_output_line in starcluster_output_data:
        f.write(starcluster_output_line)               
    f.close()
    
def write_mspycloud_dir(scripts_path, mount_path):
    root_sh = open(scripts_path + 'create_mspycloud_dir.sh', 'w')
    
    # make directories for MS-GF+/Myrimatch/Comet processing
    root_sh.write('#/bin/bash\n')
    root_sh.write('cd ' + mount_path + '\n')
    root_sh.write('sudo mkdir -p mspycloud\n')
    root_sh.write('cd mspycloud\n')
    root_sh.write('sudo mkdir -p msgfplus\n')
    root_sh.write('sudo mkdir -p myrimatch\n')
    root_sh.write('sudo mkdir -p comet\n')
    root_sh.write('sudo mkdir -p tmp\n')
    
    # make directories for GPQuest processing
    root_sh.write('cd ' + mount_path + '\n')
    root_sh.write('sudo mkdir -p gpquest\n')
    root_sh.write('cd gpquest\n')
    #root_sh.write('sudo mkdir -p gpquest-env\n')
    root_sh.write('sudo mkdir -p anaconda\n')
    root_sh.write('sudo mkdir -p software\n')
    root_sh.write('sudo mkdir -p tmp\n')
    root_sh.write('cd software\n')
    root_sh.write('sudo mkdir -p hotpot\n')
    
    root_sh.close()

if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['ebs_params'] = read_new_ebs_config(config_path + 'ebs_params.txt')
    
    main(configs)