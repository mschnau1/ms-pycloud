import os
import sys
import glob
import shutil
import traceback
import numpy as np
import time as pytime
#from pylab import frange
from pyteomics import mzml
from hotpot.spectrum import Spectrum
from multiprocessing import Pool
from multiprocessing import cpu_count
from hotpot.read_write import read_lines as rl
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'
# 0 based index
mzid_scan_col = 2
mzid_charge_col = 7
mzid_pep_col = 8
mzid_header_lines = 1

def main(configs):
    print '\nStep 3 continued: Reading charge states and injection times from mzML files and comparing to mzID files...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(pytime.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    inference_options = configs['inference_search']
    sample_filenames = sorted(configs['sample filenames'])
    
    data_root = options['raw_data']
    data_set_name = data_root.split('\\')[-2]
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    num_thread = int(cpu_count()) - 1
    digestion_type = str(inference_options['cleavage_rule']).strip().lower()
    
    # sets the step for the injection time grouping
    injection_time_step = 10
    
    if num_thread == 0:
        num_thread = 1
    
    # handle single shot data
    mzml_files = glob.glob(mzml_path + '*.mzML')
    if sample_filenames == list():
        for mzml_f in mzml_files:
            mzml_f_basename = os.path.basename(mzml_f).replace('.mzML', '')
            
            if mzml_f_basename not in sample_filenames:
                sample_filenames.append(mzml_f_basename)
    
    data_roots = [data_root] * len(mzml_files)
    digestion_types = [digestion_type] * len(mzml_files)
    
    pool = Pool(processes = num_thread)
    pool.map(charge_injection_time_in_parallel, zip(mzml_files, data_roots, digestion_types))    
    
    charge_files = sorted(glob.glob(mzid_path + '*.charge.counts.tsv'))
    combine_charge_count_files(charge_files = charge_files, mzid_path = mzid_path, data_set_name = data_set_name, sample_filenames = sample_filenames)
            
    # combine search quameter tsv results into one tsv file
    tsv_files = sorted(glob.glob(mzid_path + '*.qual.mspycloud.tsv'))
    
    if tsv_files != []:
        longest_charge_list = []
        longest_charge_ratio_list = []
        longest_injection_list = []
        longest_injection_ratio_list = []
        max_injection_time = -1
        longest_header = ''
        for tsv_file in tsv_files:
            current_filename = str(os.path.basename(tsv_file).replace('.qual.mspycloud.tsv', ''))
            
            for sample_filename in sample_filenames:
                if sample_filename in current_filename:
                    f = open(tsv_file, 'r')
                    lines = f.readlines()
                    f.close()
                    
                    header = lines[0]
                    header_list = header.strip().split('\t')
                    
                    charge_list = []
                    charge_ratio_list = []
                    injection_list = []
                    #injection_ratio_list = []
                    for col in header_list:
                        if 'charge #' in col:
                            charge_list.append(col)
                        elif 'ratio charge' in col:
                            charge_ratio_list.append(col)
                        elif 'injection time #' in col:
                            injection_list.append(col)
                        #elif 'ratio injection time' in col:
                        #    injection_ratio_list.append(col) 
                    
                    for charge_col in charge_list:
                        if charge_col not in longest_charge_list:
                            longest_charge_list.append(charge_col)
                        
                    for charge_col in charge_ratio_list:
                        if charge_col not in longest_charge_ratio_list:
                            longest_charge_ratio_list.append(charge_col)
                        
                    for injection_col in injection_list:
                        # checks to see if the injection time header name is the final injection time value
                        if '-' not in injection_col:
                            injection_time = float(injection_col.split('injection time #')[0].strip())
                        
                            if injection_time > max_injection_time:
                                max_injection_time = injection_time
                    break
                
        for time in np.arange(0, max_injection_time + injection_time_step, injection_time_step):
            if time < (max_injection_time - injection_time_step):
                time_key = str(time) + '-' + str(time + injection_time_step) + ' injection time # (mzML)'
                if time_key not in longest_injection_list:
                    longest_injection_list.append(time_key)
                
                time_ratio_key = str(time) + '-' + str(time + injection_time_step) + ' ratio injection time (mzid/mzML)'
                if time_ratio_key not in longest_injection_ratio_list:
                    longest_injection_ratio_list.append(time_ratio_key)
            elif time < max_injection_time:
                time_key = str(time) + '-' + str(max_injection_time) + ' injection time # (mzML)'
                if time_key not in longest_injection_list:
                    longest_injection_list.append(time_key)
                
                time_ratio_key = str(time) + '-' + str(max_injection_time) + ' ratio injection time (mzid/mzML)'
                if time_ratio_key not in longest_injection_ratio_list:
                    longest_injection_ratio_list.append(time_ratio_key)
                    
                time_key = str(max_injection_time) + ' injection time # (mzML)'
                if time_key not in longest_injection_list:
                    longest_injection_list.append(time_key)
                    
                time_ratio_key = str(max_injection_time) + ' ratio injection time (mzid/mzML)'
                if time_ratio_key not in longest_injection_ratio_list:
                    longest_injection_ratio_list.append(time_ratio_key)
        
        for i, charge_col in enumerate(longest_charge_list):
            longest_charge_list[i] = str(charge_col).strip()
            
        for i, charge_col in enumerate(longest_charge_ratio_list):
            longest_charge_ratio_list[i] = str(charge_col).strip()
            
        for i, injection_col in enumerate(longest_injection_list):
            longest_injection_list[i] = str(injection_col).strip()
            
        for i, injection_col in enumerate(longest_injection_ratio_list):
            longest_injection_ratio_list[i] = str(injection_col).strip()
        
        longest_header_part = sorted(sorted(longest_charge_list), key = len) + sorted(sorted(longest_charge_ratio_list), key = len) + longest_injection_list + longest_injection_ratio_list
        
        f = open(tsv_files[0], 'r')
        lines = f.readlines()
        f.close()
        
        longest_header_list = []
        longest_header_part_flag = False
        
        header = lines[0]
        header_list = header.strip().split('\t')
        
        for col in header_list:
            col = str(col).strip()
            
            if col not in longest_header_part:
                longest_header_list.append(col)
            elif not longest_header_part_flag:
                longest_header_list += longest_header_part  
                longest_header_part_flag = True
                
        if not longest_header_part_flag:
            longest_header_list += longest_header_part  
            longest_header_part_flag = True
        
        longest_header = '\t'.join(longest_header_list).strip()
        
        output_data = [longest_header]
        for tsv_file in tsv_files:
            current_filename = str(os.path.basename(tsv_file).replace('.qual.mspycloud.tsv', ''))
            
            for sample_filename in sample_filenames:
                if sample_filename in current_filename:                   
                    f = open(tsv_file, 'r')
                    lines = f.readlines()
                    f.close()
                    
                    header_line = lines[0]
                    header_list = header_line.strip().split('\t')
                    current_val_line = lines[1]
                    val_list_initial = current_val_line.strip().split('\t')
                    
                    vals_dict = {}
                    for i, col in enumerate(header_list):
                        col = str(col).strip()
                        
                        vals_dict[col] = val_list_initial[i]
                    
                    val_ratio_dict = {}
                    val_list = [0] * len(longest_header_list)   
                    for val_header in vals_dict:
                        # necessary check because injection time bins are not necessarily the same for each file
                        if val_header in longest_header_list:
                            header_index = longest_header_list.index(val_header)
                            val_list[header_index] = vals_dict[val_header]
                        else:
                            if 'injection time #' in val_header:
                                header_key = 'injection time #'
                                
                                if '-' not in val_header:
                                    val_time = float(val_header.split(header_key)[0].strip())
                                else:
                                    val_time = float(val_header.split(header_key)[0].strip().split('-')[-1].strip())
                                
                                for y, header_name in enumerate(longest_header_list):
                                    if header_key in header_name and '-' in header_name:
                                        header_name = header_name.split(header_key)[0].strip()
                                        times = header_name.split('-')
                                        
                                        beg_time = float(times[0].strip())
                                        end_time = float(times[-1].strip())
                                        
                                        if val_time >= beg_time and val_time < end_time:
                                            val_list[y] = int(val_list[y]) + int(vals_dict[val_header])
                                            val_list[y] = str(val_list[y])
                                            break
                            elif 'ratio injection time' in val_header:
                                val_header_non_ratio = val_header.replace('ratio injection time (mzid/mzML)', 'injection time # (mzML)')
                                
                                header_key = 'ratio injection time'
                                
                                if '-' not in val_header:
                                    val_time = float(val_header.split(header_key)[0].strip())
                                else:
                                    val_time = float(val_header.split(header_key)[0].strip().split('-')[-1].strip())
                                
                                for y, header_name in enumerate(longest_header_list):
                                    if header_key in header_name and '-' in header_name:
                                        header_name = header_name.split(header_key)[0].strip()
                                        times = header_name.split('-')
                                        
                                        beg_time = float(times[0].strip())
                                        end_time = float(times[-1].strip())
                                        
                                        if val_time >= beg_time and val_time < end_time:
                                            mzml_num = int(vals_dict[val_header_non_ratio])
                                            mzid_num = int(np.round(mzml_num * float(vals_dict[val_header])))
                                            
                                            if y not in val_ratio_dict:
                                                val_ratio_dict[y] = {}
                                                val_ratio_dict[y]['mzml'] = mzml_num
                                                val_ratio_dict[y]['mzid'] = mzid_num
                                            else:
                                                val_ratio_dict[y]['mzml'] += mzml_num
                                                val_ratio_dict[y]['mzid'] += mzid_num
                                            break
                            else:
                                print 'Unexpected issue: Column missing from header was not an injection time bin column!'
                                    
                    for ratio_dict_index in val_ratio_dict:
                        try:
                            val_list[ratio_dict_index] = float(val_ratio_dict[ratio_dict_index]['mzid']) / float(val_ratio_dict[ratio_dict_index]['mzml'])
                            val_list[ratio_dict_index] = str(val_list[ratio_dict_index])
                        except ZeroDivisionError:
                            val_list[ratio_dict_index] = '0'
                    
                    val_line = ''
                    for val in val_list:
                        val = str(val).strip()
                        val_line += val + '\t'
                    val_line += '\n'
                    
                    output_data.append(val_line)
                    break
            
        f = open(mzid_path + data_set_name + '_search_idfree_quameter_results.tsv', 'w')
        for row in output_data:
            f.write(row.strip() + '\n')
        
        f.close()    

# ******************************************************************************

def combine_charge_count_files(charge_files, mzid_path, data_set_name, sample_filenames):
    if charge_files != []:
        longest_charge_list = []
        longest_header = ''
        for charge_file in charge_files:
            current_filename = str(os.path.basename(charge_file).replace('.charge.counts.tsv', ''))
            
            for sample_filename in sample_filenames:
                if sample_filename in current_filename:
                    f = open(charge_file, 'r')
                    lines = f.readlines()
                    f.close()
                    
                    header = lines[0]
                    header_list = header.strip().split('\t')
                    
                    charge_list = []
                    for col in header_list:
                        if 'charge #' in col:
                            charge_list.append(col)
                    
                    for charge_col in charge_list:
                        if charge_col not in longest_charge_list:
                            longest_charge_list.append(charge_col)
                            
                    break
        
        for i, charge_col in enumerate(longest_charge_list):
            longest_charge_list[i] = str(charge_col).strip()
            
        longest_header_part = sorted(sorted(longest_charge_list), key = len)
        
        f = open(charge_files[0], 'r')
        lines = f.readlines()
        f.close()
        
        longest_header_list = []
        longest_header_part_flag = False
        
        header = lines[0]
        header_list = header.strip().split('\t')
        
        for col in header_list:
            col = str(col).strip()
            
            if col not in longest_header_part:
                longest_header_list.append(col)
            elif not longest_header_part_flag:
                longest_header_list += longest_header_part  
                longest_header_part_flag = True
        
        if not longest_header_part_flag:
            longest_header_list += longest_header_part  
            longest_header_part_flag = True
        
        longest_header = '\t'.join(longest_header_list).strip()
        
        output_data = [longest_header]
        for charge_file in charge_files:
            current_filename = str(os.path.basename(charge_file).replace('.charge.counts.tsv', ''))
            
            for sample_filename in sample_filenames:
                if sample_filename in current_filename:
                    f = open(charge_file, 'r')
                    lines = f.readlines()
                    f.close()
                    
                    header_line = lines[0]
                    header_list = header_line.strip().split('\t')
                    current_val_line = lines[1]
                    val_list_initial = current_val_line.strip().split('\t')
                    
                    charge_vals = {}
                    for i, col in enumerate(header_list):
                        col = str(col).strip()
                        
                        charge_vals[col] = val_list_initial[i]
                    
                    val_list = [0] * len(longest_header_list)   
                    for charge_val_header in charge_vals:
                        header_index = longest_header_list.index(charge_val_header)
                        val_list[header_index] = charge_vals[charge_val_header]
                                    
                    val_line = ''
                    for val in val_list:
                        val = str(val).strip()
                        val_line += val + '\t'
                    val_line += '\n'
                    
                    output_data.append(val_line)
                    break
            
        f = open(mzid_path + data_set_name + '_charge_counts.tsv', 'w')
        for row in output_data:
            f.write(row.strip() + '\n')
        
        f.close()    
                                                  
def charge_injection_time_in_parallel((mzml_file, data_root, digestion_type)):
    """
    Description: Makes multiple calls to quameter
    
    Args:
        - raw_file: one of the raw data files
        - qual_tsv_file: qual_tsv_file output path
    """
    try:
        mzid_path = data_root + 'step3-search_results\\'
        
        # charge and injection time data that will be added to the quameter results file
        data = {}
        # sets the step for the injection time grouping
        injection_time_step = 10
        # number of decimal places to round injection time values
        injection_time_precision = 9
        
        raw_filename = os.path.basename(mzml_file).replace('.mzML', '')
        
        # if the particular charge and injection time quameter result file does not already exist, then it is created
        if not os.path.exists(mzid_path + raw_filename + '.qual.mspycloud.tsv') or not os.path.exists(mzid_path + raw_filename + '.charge.counts.tsv') or not os.path.exists(mzid_path + raw_filename + '.injection.times.tsv'):
            with mzml.read(mzml_file) as reader:
                print 'Reading ' + mzml_file
                sys.stdout.flush()
                
                data[raw_filename] = {}
                data[raw_filename]['charges'] = {}
                data[raw_filename]['charges']['unassigned'] = {}
                data[raw_filename]['charges']['unassigned']['count'] = 0
                data[raw_filename]['charges']['unassigned']['id count'] = 0
                data[raw_filename]['charges']['unassigned']['scans'] = []
                data[raw_filename]['charges']['assigned'] = {}
                data[raw_filename]['charges']['assigned']['count'] = 0
                data[raw_filename]['charges']['assigned']['id count'] = 0
                data[raw_filename]['charges']['assigned']['scans'] = []
                data[raw_filename]['injection times'] = {}
                data[raw_filename]['injection times']['scans'] = {}
                data[raw_filename]['injection times']['unassigned'] = {}
                data[raw_filename]['injection times']['unassigned']['count'] = 0
                data[raw_filename]['injection times']['unassigned']['id count'] = 0
                data[raw_filename]['injection times']['unassigned']['scans'] = []
                data[raw_filename]['injection times']['assigned'] = {}
                data[raw_filename]['injection times']['assigned']['all'] = []
                data[raw_filename]['injection times']['assigned']['count'] = 0
                data[raw_filename]['injection times']['assigned']['id count'] = 0
                data[raw_filename]['injection times']['assigned']['scans'] = []  
                
                for entry in reader:
                    spec = Spectrum(entry)
                    ms_level = int(spec.ms_level)
                    
                    if ms_level == 2:
                        scan = int(spec.scan)
                        
                        try:
                            injection_time = np.round(float(spec.scan_list['scan'][0]['ion injection time']), injection_time_precision)  
                            
                            data[raw_filename]['injection times']['assigned']['all'].append(injection_time)
                            data[raw_filename]['injection times']['assigned']['count'] += 1
                            data[raw_filename]['injection times']['assigned']['scans'].append(scan)
                            data[raw_filename]['injection times']['scans'][scan] = injection_time
                        except ValueError:
                            data[raw_filename]['injection times']['unassigned']['count'] += 1
                            data[raw_filename]['injection times']['unassigned']['scans'].append(scan)
                            #print 'Invalid injection time, not a floating point number! - scan (' + raw_filename + ') = ' + str(scan)        
                        
                        if spec.precursor_list != None:
                            precursor_list = spec.precursor_list['precursor'][0]
                            selected_ion = precursor_list['selectedIonList']['selectedIon'][0]
                            
                            if 'charge state' in selected_ion:
                                charge = int(selected_ion['charge state'])
                                
                                if charge in data[raw_filename]['charges']:
                                    data[raw_filename]['charges'][charge]['count'] += 1
                                    data[raw_filename]['charges'][charge]['scans'].append(scan)
                                    data[raw_filename]['charges']['assigned']['count'] += 1
                                    data[raw_filename]['charges']['assigned']['scans'].append(scan)
                                else:
                                    data[raw_filename]['charges'][charge] = {}
                                    data[raw_filename]['charges'][charge]['count'] = 1
                                    data[raw_filename]['charges'][charge]['id count'] = 0
                                    data[raw_filename]['charges'][charge]['scans'] = []
                                    data[raw_filename]['charges'][charge]['scans'].append(scan)
                                    data[raw_filename]['charges']['assigned']['count'] += 1
                                    data[raw_filename]['charges']['assigned']['scans'].append(scan)
                            else:
                                data[raw_filename]['charges']['unassigned']['count'] += 1
                                data[raw_filename]['charges']['unassigned']['scans'].append(scan)
                        else:
                            data[raw_filename]['charges']['unassigned']['count'] += 1
                            data[raw_filename]['charges']['unassigned']['scans'].append(scan)
        
            # picks out the maximum injection time for splitting up the injection times into 10 second intervals
            max_injection_time = max(data[raw_filename]['injection times']['assigned']['all'])
                
            all_charges = data[raw_filename]['charges'].keys()
            all_charges.remove('unassigned')
            all_charges.remove('assigned')
            
            min_charge = min(all_charges)
            max_charge = max(all_charges)
            
            time_keys = []
            charge_keys = []
        
            for time in np.arange(0, max_injection_time + injection_time_step, injection_time_step):
                if time < (max_injection_time - injection_time_step):
                    time_key = str(time) + '-' + str(time + injection_time_step)
                    time_keys.append(time_key)
                    
                    if time_key not in data[raw_filename]['injection times']:
                        data[raw_filename]['injection times'][time_key] = {}  
                    
                    data[raw_filename]['injection times'][time_key]['count'] = 0
                    data[raw_filename]['injection times'][time_key]['id count'] = 0
                    data[raw_filename]['injection times'][time_key]['scans'] = []
                elif time < max_injection_time:
                    time_key = str(time) + '-' + str(max_injection_time)
                    time_keys.append(time_key)
                    time_keys.append(str(max_injection_time))
                    
                    if time_key not in data[raw_filename]['injection times']:
                        data[raw_filename]['injection times'][time_key] = {}     
                    
                    data[raw_filename]['injection times'][time_key]['count'] = 0
                    data[raw_filename]['injection times'][time_key]['id count'] = 0
                    data[raw_filename]['injection times'][time_key]['scans'] = []
                    
                    if str(max_injection_time) not in data[raw_filename]['injection times']:
                        data[raw_filename]['injection times'][str(max_injection_time)] = {} 
                    
                    data[raw_filename]['injection times'][str(max_injection_time)]['count'] = 0
                    data[raw_filename]['injection times'][str(max_injection_time)]['id count'] = 0 
                    data[raw_filename]['injection times'][str(max_injection_time)]['scans'] = []          
            
            for injection_scan in data[raw_filename]['injection times']['scans']:
                injection_time = data[raw_filename]['injection times']['scans'][injection_scan]
                
                for time_key in time_keys:
                    if '-' in time_key:
                        time_key_selection = time_key.strip().split('-')
                        beg_time = float(time_key_selection[0].strip())
                        end_time = float(time_key_selection[-1].strip())
                        
                        if injection_time >= beg_time and injection_time < end_time:
                            data[raw_filename]['injection times'][time_key]['count'] += 1
                            data[raw_filename]['injection times'][time_key]['scans'].append(injection_scan)
                            break
                    else:
                        time_key_selection = float(time_key.strip())
                        if injection_time == time_key_selection:
                            data[raw_filename]['injection times'][time_key]['count'] += 1
                            data[raw_filename]['injection times'][time_key]['scans'].append(injection_scan)
                            break
                                    
            for key in range(min_charge, max_charge + 1):
                if key in data[raw_filename]['charges']:
                    #print 'Charge ' + str(key) + ' = ' + str(data[raw_filename]['charges'][key]['count']) 
                    charge_keys.append(key)
                    continue
                #else:
                #    data[raw_filename]['charges'][key] = {}
                #    data[raw_filename]['charges'][key]['count'] = 0
                #    data[raw_filename]['charges'][key]['id count'] = 0
                #    data[raw_filename]['charges'][key]['scans'] = []
                #    #print 'Charge ' + str(key) + ' = ' + str(data[raw_filename]['charges'][key]['count'])
            
            #print 'Assigned charges = ' + str(data[raw_filename]['charges']['assigned']['count'])
            #print 'Unassigned charges = ' + str(data[raw_filename]['charges']['unassigned']['count'])
            
            # prints injection time results
            for key in time_keys:
                #print 'Injection time ' + str(key) + ' = ' + str(data[raw_filename]['injection times'][key])
                continue  
            
            mzid_f = open(mzid_path + raw_filename + '.tsv', 'r')
            mzid_lines = mzid_f.readlines()
            mzid_f.close()
            
            num_mzid_miscleavages = 0
            mzid_scans = set()
            
            # dictionary for keeping track of what the mzid assigned charge states were for scans that had unassigned charges in mzml
            mzid_charges = {}
            mzid_unassigned_charge_count = 0
            
            for mzid_line in mzid_lines[mzid_header_lines: ]:
                mzid_line = mzid_line.strip().split('\t')
                mzid_scan = int(mzid_line[mzid_scan_col])
                mzid_charge = int(mzid_line[mzid_charge_col])
                pep = str(mzid_line[mzid_pep_col]).upper()
                
                # update mzid_charges if necessary
                if mzid_scan in data[raw_filename]['charges']['unassigned']['scans']:
                    mzid_unassigned_charge_count += 1
                    
                    if mzid_charge in mzid_charges:
                        mzid_charges[mzid_charge] += 1
                    else:
                        mzid_charges[mzid_charge] = 1
                
                mzid_scans.add(mzid_scan)
                
                # determine if the peptide was miscleaved
                num_k_r = 0
                
                amino_position = -1
                for amino in pep[:-1]:
                    amino_position += 1
                    
                    succeeding_amino = amino_position + 1
                    if (amino == 'K' or amino == 'R') and pep[succeeding_amino] != 'P':
                        num_k_r += 1
                
                if pep[-1] == 'K' or pep[-1] == 'R':
                    num_k_r += 1
                
                if num_k_r > 1 and digestion_type == 'trypsin':
                    num_mzid_miscleavages += 1
                    
            # output file with info regarding what the mzid assigned charge states were for scans that had unassigned charges in mzml
            charge_header = ['Filename', 'invalid charges # (mzML)', 'validated charges # (mzid)']
            for charge_key in sorted(mzid_charges):
                charge_header.append('+' + str(charge_key) + ' charge # (mzid)')
            
            charge_data = [charge_header]
            charge_row = [raw_filename + '.raw', str(len(data[raw_filename]['charges']['unassigned']['scans'])), str(mzid_unassigned_charge_count)]
            for charge_key in sorted(mzid_charges):
                charge_row.append(str(mzid_charges[charge_key]))       
                    
            charge_data.append(charge_row)
            
            charge_f = open(mzid_path + raw_filename + '.charge.counts.tsv' , 'w')
            
            #charge_f.write('Number of mzML file scans with unassigned charge states that were then assigned by MS-GF+ in the resultant mzid file.\n\n')
            for row in charge_data:
                charge_output_row = ''
                for col in row:
                    charge_output_row += str(col) + '\t'
                    
                charge_f.write(charge_output_row.strip() + '\n')
            
            charge_f.close()
            
            # determine identified/unidentified ratios for the scans in each charge state
            for charge_key in charge_keys:
                mzml_scans = data[raw_filename]['charges'][charge_key]['scans']
                for mzml_scan in mzml_scans:
                    if mzml_scan in mzid_scans:
                        data[raw_filename]['charges'][charge_key]['id count'] += 1
                
            # checking all charge assigned scans               
            mzml_scans = data[raw_filename]['charges']['assigned']['scans']
            for mzml_scan in mzml_scans:
                if mzml_scan in mzid_scans:
                    data[raw_filename]['charges']['assigned']['id count'] += 1
            
            # checking all charge unassigned scans               
            mzml_scans = data[raw_filename]['charges']['unassigned']['scans']
            for mzml_scan in mzml_scans:
                if mzml_scan in mzid_scans:
                    data[raw_filename]['charges']['unassigned']['id count'] += 1
            
            # determine identified/unidentified ratios for the scans in each injection time bin
            for time_key in time_keys:
                mzml_scans = data[raw_filename]['injection times'][time_key]['scans']
                for mzml_scan in mzml_scans: 
                    if mzml_scan in mzid_scans:
                        data[raw_filename]['injection times'][time_key]['id count'] += 1
                
            # checking all injection time assigned scans
            mzml_scans = data[raw_filename]['injection times']['assigned']['scans']
            for mzml_scan in mzml_scans: 
                if mzml_scan in mzid_scans:
                    data[raw_filename]['injection times']['assigned']['id count'] += 1   
            
            # checking all injection time unassigned scans
            mzml_scans = data[raw_filename]['injection times']['unassigned']['scans']
            for mzml_scan in mzml_scans: 
                if mzml_scan in mzid_scans:
                    data[raw_filename]['injection times']['unassigned']['id count'] += 1      
                
            reverse_time_keys = list(time_keys)
            reverse_time_keys.reverse()
            reverse_charge_keys = list(charge_keys)
            reverse_charge_keys.reverse()
            
            idfree_file = data_root + raw_filename + '.qual.tsv'
            
            if os.path.isfile(idfree_file):
                f = open(idfree_file, 'r')
                idfree_lines = f.readlines()
                f.close()
            else:
                # handles missing quameter results file
                idfree_lines = ['Filename', raw_filename + '.raw']
            
            outfile_data = []
            
            for line in idfree_lines:
                if line == idfree_lines[0]: 
                    line = line.strip().split('\t')   
                    
                    for time in reverse_time_keys:
                        line.insert(1, str(time) + ' ratio injection time (mzid/mzML)')
                    
                    for time in reverse_time_keys:
                        line.insert(1, str(time) + ' injection time # (mzML)')
                    
                    for charge in reverse_charge_keys:
                        line.insert(1, '+' + str(charge) + ' ratio charge (mzid/mzML)')
                    
                    for charge in reverse_charge_keys:
                        line.insert(1, '+' + str(charge) + ' charge # (mzML)')
                        
                    line.insert(1, 'invalid injection times ratio (mzid/mzML)')
                    line.insert(1, 'valid injection times ratio (mzid/mzML)')
                    line.insert(1, 'invalid injection times # (mzML)')
                    line.insert(1, 'valid injection times # (mzML)')
                    
                    line.insert(1, 'invalid charges ratio (mzid/mzML)')
                    line.insert(1, 'valid charges ratio (mzid/mzML)')
                    line.insert(1, 'invalid charges # (mzML)')
                    line.insert(1, 'valid charges # (mzML)')
                     
                    line.insert(1, 'Std. dev. injection time')
                    line.insert(1, 'Mean injection time')
                    line.insert(1, 'Median injection time')
                           
                    line.insert(1, 'miscleavages # (mzid)')
                else:
                    line = line.strip().split('\t')
                    current_raw_filename = line[0].strip().replace('.raw', '').replace('"', '')
                    
                    current_data = data[current_raw_filename]
                    
                    # add in specific injection times data
                    for time in reverse_time_keys:
                        try:
                            line.insert(1, float(current_data['injection times'][time]['id count']) / float(current_data['injection times'][time]['count']))
                        except ZeroDivisionError:
                            line.insert(1, 0.0)
                    
                    for time in reverse_time_keys:    
                        line.insert(1, current_data['injection times'][time]['count'])      
                    
                    # add in specific charges data
                    for charge in reverse_charge_keys:
                        try:
                            line.insert(1, float(current_data['charges'][charge]['id count']) / float(current_data['charges'][charge]['count']))
                        except ZeroDivisionError:
                            line.insert(1, 0.0)
                    
                    for charge in reverse_charge_keys:
                        line.insert(1, current_data['charges'][charge]['count'])
                    
                    # add in injection times assigned/unassigned data
                    try:
                        line.insert(1, float(current_data['injection times']['unassigned']['id count']) / float(current_data['injection times']['unassigned']['count']))
                    except ZeroDivisionError:
                        line.insert(1, 0.0)
                
                    try:
                        line.insert(1, float(current_data['injection times']['assigned']['id count']) / float(current_data['injection times']['assigned']['count']))
                    except ZeroDivisionError:
                        line.insert(1, 0.0)
                    
                    line.insert(1, current_data['injection times']['unassigned']['count'])
                    line.insert(1, current_data['injection times']['assigned']['count'])
                    
                    # add in charges assigned/unassigned data
                    try:
                        line.insert(1, float(current_data['charges']['unassigned']['id count']) / float(current_data['charges']['unassigned']['count']))
                    except ZeroDivisionError:
                        line.insert(1, 0.0)
                    
                    try:
                        line.insert(1, float(current_data['charges']['assigned']['id count']) / float(current_data['charges']['assigned']['count']))
                    except ZeroDivisionError:
                        line.insert(1, 0.0)
                        
                    line.insert(1, current_data['charges']['unassigned']['count'])
                    line.insert(1, current_data['charges']['assigned']['count']) 
                    
                    line.insert(1, np.std(current_data['injection times']['assigned']['all']))
                    line.insert(1, np.mean(current_data['injection times']['assigned']['all']))
                    line.insert(1, np.median(current_data['injection times']['assigned']['all']))
                    
                    line.insert(1, num_mzid_miscleavages)
                    
                    f = open(mzid_path + raw_filename + '.injection.times.tsv', 'w')
                    for injection_t in current_data['injection times']['assigned']['all']:
                        f.write(str(injection_t).strip() + '\n')
                    f.close()
                    
                output_line = ''
                for value in line:
                    output_line += str(value) + '\t'
                    
                outfile_data.append(output_line.strip() + '\n')
            
            outfile = open(mzid_path + raw_filename + '.qual.mspycloud.tsv', 'w')
            for outfile_line in outfile_data:
                outfile.write(outfile_line)
            outfile.close()
    except:
        print '-'
        print 'Error with p3a_mzml_mzid_charge_injection_time.charge_injection_time_in_parallel()...'
        print traceback.format_exc()
        print '-'
        sys.stdout.flush()
        
        pass
                
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)