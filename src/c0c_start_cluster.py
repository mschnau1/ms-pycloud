import os
import sys
import subprocess
from hotpot.read_write import write_tmp_config as wtc
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_key_location as sc_key_location
from hotpot.configuring import get_starcluster_cluster_size as sc_cluster_size

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
tmp_path = pipeline_path + 'tmp\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
scripts_path = pipeline_path + 'shell_scripts\\'
pwiz_path = pipeline_path + 'installs\\linux\\'

def main(configs):
    print '\nStep c0c: Starting the computational cluster...'
    print '-' * 150
    sys.stdout.flush()
    
    configs['starcluster'] = steps(starcluster_config_path)
    
    options = configs['options']
    exes = configs['exes']
    sc_key, sc_keys = sc_key_info(starcluster_config_path)
    sc_key_path = sc_key_location(starcluster_config_path, sc_key)
    node_num = int(sc_cluster_size(starcluster_config_path))
    
    # sets variables to configuration values
    mod = str(options['modification']).strip().lower()
    gpquest_param_file = config_path + 'gpquest_params.json'
    gpquest_param_basename = os.path.basename(gpquest_param_file)
    cluster_name = str(options['cluster_name']).strip()
    pem_key = sc_key_path
    ppk_key = pem_key.replace('.pem', '.ppk')
    user_id = options['cluster_user_id']
    # fdr not divided by 100.0 here because fdr is not used for calculations in this function
    #fdr = float(options['false_discovery_rate'])
    python2_path = os.path.dirname(exes['python']) + '\\'
    starcluster_file = python2_path + 'Scripts\\starcluster.exe'
    putty_file = exes['putty']
    putty_path = os.path.dirname(putty_file) + '\\'
    
    if not os.path.exists(ppk_key):
        ppk_command = putty_path + 'puttygen ' + pem_key
        
        print ppk_command
        sys.stdout.flush()
        subprocess.call(ppk_command)
    
    cloud_param_path = '/gpquest_files/param/'
    cloud_database_path = '/gpquest_files/databases/'
    
    # deprecated - need to change this function if used in future versions of MS-PyCloud
    #write_installs(scripts_path = scripts_path, mount_path = mount_path)
    write_dummy(scripts_path = scripts_path)
    write_database_dirs(scripts_path = scripts_path)
    # IMPORTANT - must be done when uploading the databases to the cloud since the database paths in this file will be changed to reflect cloud pathing
    wtc(tmp_path + gpquest_param_basename, config_path + gpquest_param_basename)
    update_param_cloud_database_paths(param_file = tmp_path + gpquest_param_basename, cloud_database_path = cloud_database_path)
    ips = []
    
    # starts the starcluster instance
    start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name
    print start_command
    sys.stdout.flush()
    subprocess.call(start_command)
    
    print ''
    sys.stdout.flush()
                                    
    ip_command = starcluster_file + ' -c ' + starcluster_config_path + ' sshmaster ' + cluster_name + ' GET http://169.254.169.254/latest/meta-data/public-hostname'
    print ip_command
    sys.stdout.flush()
    vals = subprocess.Popen(ip_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    ip, errors = vals.communicate()
    vals.wait()
    vals.terminate()
    master_ip = user_id + '@' + str(ip).strip()
    
    ips.append(master_ip)
    
    # dummy plink command to add key to registry
    plink_dummy_command = putty_path + 'plink ' + master_ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'dummy.sh'
    print plink_dummy_command
    sys.stdout.flush()
    proc = subprocess.Popen(plink_dummy_command, stdin = subprocess.PIPE, shell = False)
    print proc.communicate("y\n")
    proc.wait()
    proc.terminate()
    
    print ''
    sys.stdout.flush()
    
    if mod == 'intact':
        # gpquest_databases plink command to create folders for the GPQuest databases and gpquest_params.json config file
        plink_databases_command = putty_path + 'plink ' + master_ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'create_gpquest_database_dirs.sh'
        print plink_databases_command
        sys.stdout.flush()
        proc = subprocess.Popen(plink_databases_command, stdin = subprocess.PIPE, shell = False)
        print proc.communicate("y\n")
        proc.wait()
        proc.terminate()
        
        print ''
        sys.stdout.flush()
        
        # copy GPQuest protein database and the decoy database to the ebs
        database_file = read_gpquest_params_key(gpquest_param_file, '"PROTEIN":').replace('"', '')
        database_split = database_file.split('.')
        decoy_database_file = database_split[0] + '_decoys.csv'
        target_decoy_database_file = database_split[0] + '_target_decoys.txt' 
        
        if os.path.exists(database_file):
            put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node master ' + database_file + ' ' + cloud_database_path
            print put_database_command
            sys.stdout.flush()
            subprocess.call(put_database_command, shell = True)
        
            print ''
            sys.stdout.flush()
        
        if os.path.exists(decoy_database_file):
            put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node master ' + decoy_database_file + ' ' + cloud_database_path
            print put_database_command
            sys.stdout.flush()
            subprocess.call(put_database_command, shell = True)
        
            print ''
            sys.stdout.flush()
            
        if os.path.exists(target_decoy_database_file):
            put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node master ' + target_decoy_database_file + ' ' + cloud_database_path
            print put_database_command
            sys.stdout.flush()
            subprocess.call(put_database_command, shell = True)
        
            print ''
            sys.stdout.flush()
        
        # copy GPQuest n-glycan database to the ebs
        database_file = read_gpquest_params_key(gpquest_param_file, '"NGLYCAN":').replace('"', '')
        
        if os.path.exists(database_file):
            put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node master ' + database_file + ' ' + cloud_database_path
            print put_database_command
            sys.stdout.flush()
            subprocess.call(put_database_command, shell = True)
            
            print ''
            sys.stdout.flush()
        
        # copy GPQuest o-glycan database to the ebs
        database_file = read_gpquest_params_key(gpquest_param_file, '"OGLYCAN":').replace('"', '')
        
        if os.path.exists(database_file):
            put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node master ' + database_file + ' ' + cloud_database_path
            print put_database_command
            sys.stdout.flush()
            subprocess.call(put_database_command, shell = True)
            
            print ''
            sys.stdout.flush()
            
        # copy gpquest_params.json to the ebs
        put_gpquest_param_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node master ' + tmp_path + gpquest_param_basename + ' ' + cloud_param_path
        print put_gpquest_param_command
        sys.stdout.flush()
        subprocess.call(put_gpquest_param_command, shell = True)
        
        print ''
        sys.stdout.flush()
    
    # add files to the nodes other than the 'master' node
    for i in range(1, node_num):
        node_name = '00' + str(i)
        node_name = 'node' + node_name[-3:]
        
        print ''
        sys.stdout.flush()
    
        ip_command = starcluster_file + ' -c ' + starcluster_config_path + ' sshnode ' + cluster_name + ' ' + node_name + ' GET http://169.254.169.254/latest/meta-data/public-hostname'
        print ip_command
        sys.stdout.flush()
        vals = subprocess.Popen(ip_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
        ip, errors = vals.communicate()
        vals.wait()
        vals.terminate()
        aws_ip = user_id + '@' + str(ip).strip()
        
        ips.append(aws_ip)
        
        # dummy plink command to add key to registry
        plink_dummy_command = putty_path + 'plink ' + aws_ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'dummy.sh'
        print plink_dummy_command
        sys.stdout.flush()
        proc = subprocess.Popen(plink_dummy_command, stdin = subprocess.PIPE, shell = False)
        print proc.communicate("y\n")
        proc.wait()
        proc.terminate()
        
        print ''
        sys.stdout.flush()
        
        if mod == 'intact':
            plink_databases_command = putty_path + 'plink ' + aws_ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'create_gpquest_database_dirs.sh'
            print plink_databases_command
            sys.stdout.flush()
            proc = subprocess.Popen(plink_databases_command, stdin = subprocess.PIPE, shell = False)
            print proc.communicate("y\n")
            proc.wait()
            proc.terminate()
            
            print ''
            sys.stdout.flush()
            
            # copy GPQuest protein database and the decoy database to the ebs
            database_file = read_gpquest_params_key(gpquest_param_file, '"PROTEIN":').replace('"', '')
            database_split = database_file.split('.')
            decoy_database_file = database_split[0] + '_decoys.csv'
            target_decoy_database_file = database_split[0] + '_target_decoys.txt' 
            
            if os.path.exists(database_file):
                put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node ' + node_name + ' ' + database_file + ' ' + cloud_database_path
                print put_database_command
                sys.stdout.flush()
                subprocess.call(put_database_command, shell = True)
            
                print ''
                sys.stdout.flush()
            
            if os.path.exists(decoy_database_file):
                put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node ' + node_name + ' ' + decoy_database_file + ' ' + cloud_database_path
                print put_database_command
                sys.stdout.flush()
                subprocess.call(put_database_command, shell = True)
            
                print ''
                sys.stdout.flush()
                
            if os.path.exists(target_decoy_database_file):
                put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node ' + node_name + ' ' + target_decoy_database_file + ' ' + cloud_database_path
                print put_database_command
                sys.stdout.flush()
                subprocess.call(put_database_command, shell = True)
            
                print ''
                sys.stdout.flush()
            
            # copy GPQuest n-glycan database to the ebs
            database_file = read_gpquest_params_key(gpquest_param_file, '"NGLYCAN":').replace('"', '')
            
            if os.path.exists(database_file):
                put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node ' + node_name + ' ' + database_file + ' ' + cloud_database_path
                print put_database_command
                sys.stdout.flush()
                subprocess.call(put_database_command, shell = True)
                
                print ''
                sys.stdout.flush()
            
            # copy GPQuest o-glycan database to the ebs
            database_file = read_gpquest_params_key(gpquest_param_file, '"OGLYCAN":').replace('"', '')
            
            if os.path.exists(database_file):
                put_database_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node ' + node_name + ' ' + database_file + ' ' + cloud_database_path
                print put_database_command
                sys.stdout.flush()
                subprocess.call(put_database_command, shell = True)
                
                print ''
                sys.stdout.flush()
                
            # copy gpquest_params.json to the ebs
            put_gpquest_param_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' --node ' + node_name + ' ' + tmp_path + gpquest_param_basename + ' ' + cloud_param_path
            print put_gpquest_param_command
            sys.stdout.flush()
            subprocess.call(put_gpquest_param_command, shell = True)
            
            print ''
            sys.stdout.flush()

    # delete the temporary gpquest_params.json file
    os.remove(tmp_path + gpquest_param_basename)
    
    f = open(config_path + 'node_ips.txt', 'w')
    f.write('###########################\n')
    f.write('# Cloud node ip addresses #\n')
    f.write('###########################\n\n')
    
    for ip in ips:
        f.write(ip + '\n\n')
    
    f.close()
    
    print '\n'
    print '-' * 150
    print 'Amazon AWS cluster has been started!'
    print '-' * 150
    sys.stdout.flush()

def update_param_cloud_database_paths(param_file, cloud_database_path):
    # beginning of line spacer in gpquest_params.json for the database paths
    spacer = 6
    
    f = open(param_file, 'r')
    lines = f.readlines()
    f.close()  
    
    output_lines = []
    for current_line in lines:
        line = current_line.strip()
        
        if line[:len('"PROTEIN":')] == '"PROTEIN":':
            split_line = line.split('"PROTEIN":')
            file_basename = os.path.basename(split_line[-1].replace('"', '').replace(',', ''))
            
            new_line = ' ' * spacer + '"PROTEIN": "' + cloud_database_path + file_basename + '"' + ',\n'
        elif line[:len('"NGLYCAN":')] == '"NGLYCAN":':
            split_line = line.split('"NGLYCAN":')
            file_basename = os.path.basename(split_line[-1].replace('"', '').replace(',', ''))
            
            new_line = ' ' * spacer + '"NGLYCAN": "' + cloud_database_path + file_basename + '"' + ',\n'
        elif line[:len('"OGLYCAN":')] == '"OGLYCAN":':
            split_line = line.split('"OGLYCAN":')
            file_basename = os.path.basename(split_line[-1].replace('"', '').replace(',', ''))
            
            new_line = ' ' * spacer + '"OGLYCAN": "' + cloud_database_path + file_basename + '"\n'
        else:
            new_line = current_line
            
        output_lines.append(new_line)
            
    f = open(param_file, 'w')
    for output_line in output_lines:
        f.write(output_line)
    
    f.close()

def read_gpquest_params_key(filepath, key):
    # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
    infile = open(filepath, 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    val = ''
    for line in infile_lines:
        line = line.strip()
        
        if line[:len(key)] == key:
            val = line.split(key)[1].strip()
            if val[-1] == ',':
                val = val.split(',')[0].strip()
                    
    if val != '':
        return val
    else:
        raise Exception(str(key) + ' was not found in ' + str(filepath) + '.')

def write_database_dirs(scripts_path):
    gpquest_databases_sh = open(scripts_path + 'create_gpquest_database_dirs.sh', 'w')
    
    gpquest_databases_sh.write('#/bin/bash\n')
    gpquest_databases_sh.write('cd /\n')
    gpquest_databases_sh.write('sudo mkdir -p gpquest_files\n')
    gpquest_databases_sh.write('cd gpquest_files\n')
    gpquest_databases_sh.write('sudo mkdir -p param\n')
    gpquest_databases_sh.write('sudo mkdir -p databases\n')

    gpquest_databases_sh.close()

def write_installs(scripts_path, mount_path):
    installs_sh = open(scripts_path + 'installs.sh', 'w')
    
    installs_sh.write('#/bin/bash\n')
    installs_sh.write('#sudo apt-get install p7zip-full\n')
    installs_sh.write('cd ' + mount_path + 'mspycloud/installs-cloud\n')
    installs_sh.write('python program_installs_cloud.py')
    
    installs_sh.close()
    
def write_dummy(scripts_path):
    dummy_sh = open(scripts_path + 'dummy.sh', 'w')
    
    dummy_sh.write('#/bin/bash\n')
    
    dummy_sh.close()

if __name__ == '__main__':  
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    
    main(configs)