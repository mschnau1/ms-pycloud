import os
import re
import sys
import ttk
import time
import glob
import tkFont
import psutil
import shutil
import traceback
import webbrowser
import subprocess
import tkMessageBox
import hotpot.singleton as singleton
from Tkinter import *
from PIL import Image, ImageTk
from win32api import GetSystemMetrics
from tkFileDialog import askdirectory
from tkFileDialog import askopenfilename
from hotpot.read_write import read_lines as rl
from hotpot.read_write import read_luciphor2_config_lines as rluciphor2
from hotpot.read_write import write_msgf_mods as wmm
from hotpot.read_write import write_msgf_search as wms
from hotpot.read_write import write_msgf_build as wmb
from hotpot.read_write import write_tmp_config as wtc
from hotpot.read_write import read_uncommented_lines as rul
from hotpot.pathing import make_path
from hotpot.pathing import check_file_exists as cfe
from hotpot.configuring import read_new_ebs_config
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import sc_append_values_with_comma
from hotpot.configuring import read_msgf_mods_search_config as msgf_search_read
from hotpot.configuring import read_msgf_mods_modifications_config as msgf_mod_read
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_volume_info as sc_volume_info

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
willows_path = pipeline_path + 'willows\\'
install_path = pipeline_path + 'installs\\'
scripts_path = pipeline_path + 'shell_scripts\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
# valid_msgf_cleavages and valid_gpquest_cleavages must be subsets of valid_pipeline_cleavages
valid_pipeline_cleavages = ['Trypsin', 'Trypsin/P', 'Arg-C', 'alphaLP']
# although MS-GF+ supports additional cleavages, MS-PyCloud does not currently support them in downstream processing
valid_msgf_cleavages = ['Trypsin', 'Trypsin/P', 'Arg-C', 'alphaLP']
# currently GPQuest only supports Trypsin
valid_gpquest_cleavages = ['Trypsin']
valid_mods = ['global', 'glyco', 'intact', 'phospho']
valid_labels = ['free', '4plex', '8plex', '10plex', '11plex', '16plex']
valid_channels = ['113', '114', '115', '116', '117', '118', '119', '121', '126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C',
                  '132N', '132C', '133N', '133C', '134N']

# modifications used for populating the modifications combobox
mod_names = ['Global', 'SPEG', 'Intact glyco', 'Phospho']

# reagent labels used for populating the reagent label combobox
label_names = ['Label free', 'iTRAQ 4plex', 'iTRAQ 8plex', 'TMT 10plex', 'TMT 11plex', 'TMT 16plex']

# channels used for populating the reference channel combobox
itraq_channels = ['113', '114', '115', '116', '117', '118', '119', '121']
tmt_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C',
                  '132N', '132C', '133N', '133C', '134N']
label_channels = ['113 - iTRAQ', '114 - iTRAQ', '115 - iTRAQ', '116 - iTRAQ', '117 - iTRAQ', '118 - iTRAQ', '119 - iTRAQ', '121 - iTRAQ',
                  '126 - TMT', '127N - TMT', '127C - TMT', '128N - TMT', '128C - TMT', '129N - TMT', '129C - TMT', '130N - TMT', '130C - TMT', '131 - TMT', 
                  '131C - TMT', '132N - TMT', '132C - TMT', '133N - TMT', '133C - TMT', '134N - TMT']

class Processing_Pipeline():
    def __init__(self):            
        # will sys.exit(-1) if other instance is running
        me = singleton.SingleInstance() 
        
        self.root = Tk()
        self.root.title('MS-PyCloud v2.10.1')
        
        try:
            self.root.iconbitmap(willows_path + 'water-lily-pond-at-giverny-1919.ico')
        except:
            pass
        
        self.root.wm_state('zoomed')
        
        #self.height = self.root.winfo_screenheight()
        #self.width = self.root.winfo_screenwidth()
        self.width = GetSystemMetrics(0)
        self.height = GetSystemMetrics(1)
        
        # screen resolution setting
        str_width = str(self.width)
        str_height = str(self.height)
          
        self.width_base = 915
          
        if str_width == '1920' and str_height == '1080': 
            self.height_base = 595
        elif str_width == '1680' and str_height == '1050': 
            self.height_base = 540
        elif (str_width == '1600' and str_height == '900') or (str_width == '1536' and str_height == '864'): 
            self.height_base = 470
        else:
            tkMessageBox.showinfo('Error!', 'Screen resolution must be set to one of the following (with 100% text scaling):' + \
                                  '\n\t- 1920 x 1080 (native)' + \
                                  '\n\t- 1680 x 1050' + \
                                  '\n\t- 1600 x 900')
            
            self.root.update_idletasks()
            #self.root.quit()
            self.root.destroy()
            
            raise Exception('Invalid screen resolution')
            
        if str_width != '1920' and str_height != '1080':
            tkMessageBox.showinfo('Notice!', 'The GUI is optimized for 1920 x 1080 resolution.  Consider changing your ' + \
                                  'screen resolution if the display appears wonky.')
        
        self.centered_height = 0.6 * self.height
        self.centered_width = 1.5 * self.centered_height
        
        self.font = tkFont.Font(family = 'arial', size = int((self.height_base / self.centered_height) * self.height / 113) + 1)
        self.step_font = tkFont.Font(family = 'arial', weight = 'bold', slant = 'italic', size = int((self.height_base / self.centered_height) * self.height / 113) + 1)
        self.title_font = tkFont.Font(family = 'arial', weight = 'bold', size = int((self.height_base / self.centered_height) * self.height / 113) + 4)
        
        self.canvas_display()
        
        self.step1Var = BooleanVar()
        self.step2Var = BooleanVar()
        self.step3Var = BooleanVar()
        self.step4Var = BooleanVar()
        self.step5Var = BooleanVar()
        self.step6Var = BooleanVar()
        self.step7Var = BooleanVar()
        self.typeVar = StringVar()
        self.modVar = StringVar()
        self.localizePhosphositesVar = StringVar()
        self.labelVar = StringVar()
        self.databaseVar = StringVar()
        self.fastaVar = StringVar()
        self.channelVar = StringVar()
        self.nMetVar = StringVar()
        self.correctionMatrixVar = StringVar()
        self.guessSetFractionVar = StringVar()
        
        Message(self.canvas, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 2, columnspan = 1)
        
        self.initialize_flags()
        self.set_config()
        self.display_dimensions()
        
        self.pepid_display()
        self.protinf_display()
        #self.logo_display()
        self.quant_display()
        self.pipe_display()
        
        self.root.update_idletasks()
        self.initialize_gui()
        
        #self.root.focus_force()
        self.root.lift()
        self.root.attributes('-topmost', True)
        self.root.attributes('-topmost', False)
    
        self.root.protocol('WM_DELETE_WINDOW', self.exit)
        
        self.root.update_idletasks()
        
        # make the frames autoscale for different monitors
        self.root.columnconfigure(0, weight = 1)
        self.root.rowconfigure(0, weight = 1)
        
        for i in range(4):
            self.canvas.columnconfigure(i, weight = 1)
        
        for i in range(4):
            self.canvas.rowconfigure(i, weight = 1)
    
        self.root.protocol("WM_DELETE_WINDOW", self.shutdown_ttk_repeat)
        self.root.mainloop()  
    
    def canvas_display(self):
        self.canvas = Canvas(self.root, width = self.width, height = self.height, highlightthickness = 0)
        self.canvas.grid_propagate(0)
        self.canvas.grid(row = 0, column = 0, rowspan = 1, columnspan = 1)
        Message(self.canvas, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(self.canvas, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(self.canvas, text = '', width = 1, font = self.font).grid(row = 0, column = 3, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(self.canvas, text = '', width = 1, font = self.font).grid(row = 2, column = 3, rowspan = 1, columnspan = 1, sticky = 'w')
    
    #def logo_display(self):
    #    logoFrame = Frame(self.canvas, height = self.dims['sample h'] + 60, width = self.dims['sample w'], padx = 5, pady = 5)
    #    logoFrame.grid_propagate(0)
    #    
    #    # logoFrame widgets
    #    image = Image.open(self.images_path + 'biomarker_center_logo.bmp')
    #    biomarker_photo = ImageTk.PhotoImage(image)
    #    biomarker_logo = Label(logoFrame, image = biomarker_photo, padx = 5, pady = 5, height = 80, width = self.dims['sample w'] / 2.25, bg = 'black')
    #    biomarker_logo.image = biomarker_photo
    #    
    #    # logoFrame grid items
    #    biomarker_logo.grid(row = 14, column = 19, rowspan = 1, columnspan = 1, sticky = 'w')
    #    
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 3, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 4, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 5, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 6, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 7, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 8, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 9, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 10, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 11, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 12, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 13, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 3, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 6, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 7, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 8, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 9, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 10, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 12, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 13, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 14, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 15, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 16, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 17, rowspan = 1, columnspan = 1, sticky = 'w')
    #    Message(logoFrame, text = '', width = 1, font = self.font).grid(row = 14, column = 18, rowspan = 1, columnspan = 1, sticky = 'w')
    #    
    #    logoFrame.grid(row = 3, column = 2, rowspan = 1, columnspan = 1)
    
    def pipe_display(self):
        pipeFrame = LabelFrame(self.canvas, text = '4 - Run pipeline', height = self.dims['sample h'] + 30, width = self.dims['sample w'], padx = 5, pady = 0, font = self.title_font)
        pipeFrame.grid_propagate(0)
        
        commonFrame = LabelFrame(pipeFrame, text = 'Common step parameters', padx = 5, pady = 0, height = 125, width = self.dims['sample w'] / 2.0, font = self.step_font)
        cloudFrame = LabelFrame(pipeFrame, text = 'StarCluster and Amazon AWS cloud parameters', padx = 5, pady = 0, height = 233, width = self.dims['sample w'] / 2.0, font = self.step_font)
        configFrame = LabelFrame(pipeFrame, text = 'Common configuration files', padx = 5, pady = 0, height = 125, width = self.dims['sample w'] / 2.0 - 15, font = self.step_font)
        runFrame = LabelFrame(pipeFrame, text = 'Run pipeline', padx = 5, pady = 0, height = 233, width = self.dims['sample w'] / 2.0 - 15, font = self.step_font)
        
        commonFrame.grid_propagate(0)
        cloudFrame.grid_propagate(0)
        configFrame.grid_propagate(0)
        runFrame.grid_propagate(0)
        
        # commonFrame widgets
        raw_path_title = Message(commonFrame, text = 'Directory of the raw data files', width = self.dims['title w'], font = self.font)
        self.raw_path_text = Text(commonFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
        raw_path_browse = Button(commonFrame, text = 'Browse', command = self.browse_raw_files, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        archived_path_title = Message(commonFrame, text = 'Directory for saving output files', width = self.dims['title w'], font = self.font)
        self.archived_path_text = Text(commonFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
        archived_path_browse = Button(commonFrame, text = 'Browse', command = self.browse_archived_path, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # cloudFrame widgets
        starcluster_title = Message(cloudFrame, text = 'StarCluster settings', width = self.dims['title w'], font = self.font)
        self.starcluster_config = Button(cloudFrame, text = 'StarCluster', command = self.open_starcluster, width = 12, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        user_id_title = Message(cloudFrame, text = 'Instance user ID', width = self.dims['title w'], font = self.font)
        self.user_id_text = Text(cloudFrame, bd = 2, height = 1, width = self.dims['text w'] / 3 + 1, padx = 5, pady = 4, font = self.font)
        cluster_name_title = Message(cloudFrame, text = 'Cluster name', width = self.dims['title w'], font = self.font)
        self.cluster_name_text = Text(cloudFrame, bd = 2, height = 1, width = self.dims['text w'] / 3 + 1, padx = 5, pady = 4, font = self.font)
        launch_putty_title = Message(cloudFrame, text = 'Launch PuTTY to navigate cluster', width = self.dims['title w'], font = self.font)
        self.launch_putty = Button(cloudFrame, text = 'PuTTY', command = self.create_putty_key, width = 12, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        add_aws_creds_title = Message(cloudFrame, text = 'Update Amazon credentials', width = self.dims['title w'], font = self.font)
        self.add_aws_creds = Button(cloudFrame, text = 'Keys', command = self.add_aws_credentials, width = 12, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        key_choice_title = Message(cloudFrame, text = 'Selected EC2 .pem key', width = self.dims['title w'], font = self.font)
        self.key_choice_text = Text(cloudFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] / 4 + 4, padx = 5, pady = 4, font = self.font)
        self.key_choice_select = Button(cloudFrame, text = 'Select', command = self.select_ec2_key, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        new_ebs_title = Message(cloudFrame, text = 'Create a new elastic book volume (EBS)', width = self.dims['title w'], font = self.font)
        self.new_ebs = Button(cloudFrame, text = 'Volume', command = self.make_ebs_display, width = 12, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        volume_choice_title = Message(cloudFrame, text = 'Selected EBS volume', width = self.dims['title w'], font = self.font)
        self.volume_choice_text = Text(cloudFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] / 4 + 4, padx = 5, pady = 4, font = self.font)
        self.volume_choice_select = Button(cloudFrame, text = 'Select', command = self.select_ebs_volume, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # configFrame widgets
        set_steps = Button(configFrame, text = 'MS-PyCloud settings', command = self.open_steps, width = 20, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        readme_button = Button(configFrame, text = 'User Manual (.pdf)', command = self.view_readme, width = 20, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        version_button = Button(configFrame, text = 'Software version info', command = self.view_ms_pycloud_version, width = 20, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # runFrame widgets
        mod_title = Message(runFrame, text = 'Modification', width = self.dims['title w'], font = self.font)
        self.mod_cb = ttk.Combobox(runFrame, values = mod_names, textvariable = self.modVar, state = 'readonly', height = 5, width = 11, font = self.font)
        #self.globalMod = Radiobutton(runFrame, text = 'Global', variable = self.modVar, value = 'global', activeforeground = 'blue', command = self.write_config_mod, font = self.font)
        #self.glycoMod = Radiobutton(runFrame, text = 'SPEG', variable = self.modVar, value = 'glyco', activeforeground = 'blue', command = self.write_config_mod, font = self.font)
        #self.intactMod = Radiobutton(runFrame, text = 'Intact glyco', variable = self.modVar, value = 'intact', activeforegroun = 'blue', command = self.write_config_mod, font = self.font)
        #self.phosphoMod = Radiobutton(runFrame, text = 'Phospho', variable = self.modVar, value = 'phospho', activeforeground = 'blue', command = self.write_config_mod, font = self.font)
        self.localizePhosphosites = Checkbutton(runFrame, text = 'Localize sites', variable = self.localizePhosphositesVar, onvalue = 'true', offvalue = 'false', activeforeground = 'blue', disabledforeground = 'black', command = self.write_config_localize_phosphosites, font = self.font)
        label_title = Message(runFrame, text = 'Reagent label', width = self.dims['title w'], font = self.font)
        self.label_cb = ttk.Combobox(runFrame, values = label_names, textvariable = self.labelVar, state = 'readonly', height = 5, width = 11, font = self.font)
        self.label_free = Radiobutton(runFrame, text = 'Label free', variable = self.labelVar, value = 'free', activeforeground = 'blue', command = self.write_config_label, font = self.font)
        #self.label_spin = Spinbox(runFrame, values = ('Label free', 'iTRAQ 4plex', 'iTRAQ 8plex', 'TMT 10plex', 'TMT 11plex'), wrap = True)
        #self.label_4plex = Radiobutton(runFrame, text = 'iTRAQ 4plex', variable = self.labelVar, value = '4plex', activeforeground = 'blue', command = self.write_config_label, font = self.font)
        #self.label_8plex = Radiobutton(runFrame, text = 'iTRAQ 8plex', variable = self.labelVar, value = '8plex', activeforeground = 'blue', command = self.write_config_label, font = self.font)
        #self.label_10plex = Radiobutton(runFrame, text = 'TMT 10plex', variable = self.labelVar, value = '10plex', activeforeground = 'blue', command = self.write_config_label, font = self.font)
        #self.label_11plex = Radiobutton(runFrame, text = 'TMT 11 plex', variable = self.labelVar, value = '11plex', activeforeground = 'blue', command = self.write_config_label, font = self.font)
        cloud_title = Message(runFrame, text = 'Pipeline', width = self.dims['title w'], font = self.font)
        localType = Radiobutton(runFrame, text = 'Local', variable = self.typeVar, value = 'local', activeforeground = 'blue', font = self.font)
        cloudType = Radiobutton(runFrame, text = 'Cloud', variable = self.typeVar, value = 'cloud', activeforeground = 'blue', font = self.font) 
        sample_names_title = Message(runFrame, text = 'MS set filenames', width = self.dims['title w'], font =  self.font)
        set_sample_names = Button(runFrame, text = 'Select sets', command = self.sample_display, width = 10, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        guess_sample_names = Button(runFrame, text = 'Guess sets', command = self.guess_display, width = 10, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        self.start_cluster = Button(runFrame, text = 'Start cluster', command = self.start_cloud, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        self.terminate_cluster = Button(runFrame, text = 'Terminate cluster', command = self.terminate_cloud, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        all_steps = Button(runFrame, text = 'Select all steps', command = self.all_steps, width = 15, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
        clear_all_steps = Button(runFrame, text = 'Clear all steps', command = self.clear_steps, width = 15, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
        run = Button(runFrame, text = 'Run pipeline', command = self.execute, width = 15, bg = 'burlywood4', fg = 'white', activeforeground = 'burlywood4', font = self.font)
        quit = Button(runFrame, text = 'Exit', command = self.exit, width = 15, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
        
        # bind the write_config_label function to the combobox
        self.label_cb.bind("<<ComboboxSelected>>", self.write_config_label)
        self.mod_cb.bind("<<ComboboxSelected>>", self.write_config_mod)
        
        # pipeFrame grid items
        commonFrame.grid(row = 0, column = 1, rowspan = 1, columnspan = 1)
        cloudFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 1)
        configFrame.grid(row = 0, column = 2, rowspan = 1, columnspan = 1)
        runFrame.grid(row = 1, column = 2, rowspan = 1, columnspan = 1)
        
        for i in range(3):
            pipeFrame.columnconfigure(i, weight = 1)
        
        for i in range(2):
            pipeFrame.rowconfigure(i, weight = 1)
        
        #commonFrame grid items
        raw_path_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 3, sticky = 'nw')
        self.raw_path_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'new')
        raw_path_browse.grid(row = 1, column = 2, rowspan = 1, columnspan = 1, sticky = 'nw')
        archived_path_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 3, sticky = 'nw')
        self.archived_path_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'new')
        archived_path_browse.grid(row = 5, column = 2, rowspan = 1, columnspan = 1, sticky = 'nw')
        
        Message(commonFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(100):
            commonFrame.columnconfigure(i, weight = 1)
        
        for i in range(50):
            commonFrame.rowconfigure(i, weight = 1)
        
        # cloudFrame grid items
        starcluster_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 30, sticky = 'w')
        self.starcluster_config.grid(row = 1, column = 1, rowspan = 1, columnspan = 30, sticky = 'w')
        user_id_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 30, sticky = 'w')
        self.user_id_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 30, sticky = 'w')
        cluster_name_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 30, sticky = 'w')
        self.cluster_name_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 30, sticky = 'w')
        launch_putty_title.grid(row = 6, column = 0, rowspan = 1, columnspan = 40, sticky = 'w')
        self.launch_putty.grid(row = 7, column = 1, rowspan = 1, columnspan = 30, sticky = 'w')
        
        add_aws_creds_title.grid(row = 0, column = 40, rowspan = 1, columnspan = 10, sticky = 'w')
        self.add_aws_creds.grid(row = 1, column = 41, rowspan = 1, columnspan = 10, sticky = 'w')
        key_choice_title.grid(row = 2, column = 40, rowspan = 1, columnspan = 10, sticky = 'w')
        self.key_choice_text.grid(row = 3, column = 41, rowspan = 1, columnspan = 6, sticky = 'new')
        self.key_choice_select.grid(row = 3, column = 47,  rowspan = 1, columnspan = 4, sticky = 'nw')
        new_ebs_title.grid(row = 4, column = 40, rowspan = 1, columnspan = 11, sticky = 'w')
        self.new_ebs.grid(row = 5, column = 41, rowspan = 1, columnspan = 10, sticky = 'w')
        volume_choice_title.grid(row = 6, column = 40, rowspan = 1, columnspan = 10, sticky = 'w')
        self.volume_choice_text.grid(row = 7, column = 41, rowspan = 1, columnspan = 6, sticky = 'new')
        self.volume_choice_select.grid(row = 7, column = 47,  rowspan = 1, columnspan = 4, sticky = 'nw')
        
        Message(cloudFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        #Message(cloudFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 40, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(100):
            cloudFrame.columnconfigure(i, weight = 1)
            
        for i in range(25):
            cloudFrame.rowconfigure(i, weight = 1)
        
        # configFrame grid items
        set_steps.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'nsw')
        readme_button.grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'nsw')
        version_button.grid(row = 2, column = 0, rowspan = 1, columnspan = 1, sticky = 'nsw')
        
        Message(configFrame, text = '', width = 1, font = self.font).grid(row = 3, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(1):
            configFrame.columnconfigure(i, weight = 1)
            
        for i in range(4):
            configFrame.rowconfigure(i, weight = 1)
        
        # runFrame grid items
        mod_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 3, sticky = 'w')
        self.mod_cb.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.globalMod.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.glycoMod.grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.intactMod.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.phosphoMod.grid(row = 4, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        self.localizePhosphosites.grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'e')
        label_title.grid(row = 0, column = 3, rowspan = 1, columnspan = 3, sticky = 'w')
        #self.label_spin.grid(row = 1, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        self.label_cb.grid(row = 1, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.label_free.grid(row = 1, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.label_4plex.grid(row = 2, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.label_8plex.grid(row = 3, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.label_10plex.grid(row = 4, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.label_11plex.grid(row = 5, column = 5, rowspan = 1, columnspan = 1, sticky = 'w')
        cloud_title.grid(row = 0, column = 8, rowspan = 1, columnspan = 4, sticky = 'w')
        localType.grid(row = 1, column = 9, rowspan = 1, columnspan = 1, sticky = 'w')
        cloudType.grid(row = 2, column = 9, rowspan = 1, columnspan = 1, sticky = 'w')
        sample_names_title.grid(row = 0, column = 11, rowspan = 1, columnspan = 6, sticky = 'w')
        set_sample_names.grid(row = 1, column = 12, rowspan = 1, columnspan = 1, sticky = 'w')
        guess_sample_names.grid(row = 2, column = 12, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.start_cluster.grid(row = 4, column = 12, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.terminate_cluster.grid(row = 6, column = 12, rowspan = 1, columnspan = 1, sticky = 'w')
        all_steps.grid(row = 7, column = 0, rowspan = 1, columnspan = 4, sticky = 'sew')
        clear_all_steps.grid(row = 8, column = 0, rowspan = 1, columnspan = 4, sticky = 'new')
        run.grid(row = 7, column = 4, rowspan = 1, columnspan = 4, sticky = 'sew')
        quit.grid(row = 8, column = 4, rowspan = 1, columnspan = 4, sticky = 'new')
        
        #Message(runFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 3, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 7, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 10, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 8, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 10, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 3, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 4, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(runFrame, text = '', width = 1, font = self.font).grid(row = 5, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        #Message(runFrame, text = '', width = 1, font = self.font).grid(row = 6, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(13):
            runFrame.columnconfigure(i, weight = 1)
            
        for i in range(100):
            runFrame.rowconfigure(i, weight = 1)
        
        pipeFrame.grid(row = 3, column = 2, rowspan = 1, columnspan = 1)
        
    def add_aws_credentials(self):
        if self.add_aws_flag == False:
            self.awsFrame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 310, width = self.dims['sample w'])
            self.awsFrame.grid_propagate(0)
            self.awsFrame.title('Update Amazon user keys in starcluster config file')
            
            self.x_aws = (self.width / 2) - (self.dims['sample w'] / 2.0)
            self.y_aws = (self.height / 2) - ((self.dims['sample h'] * 2 - 250) / 2.0)
            
            self.centered_height_aws = 0.65 * (self.dims['sample h'] * 2 - 75)
            self.centered_width_aws = 1.54 * self.centered_height_aws
            
            self.add_aws_flag = True
            
            credsFrame = LabelFrame(self.awsFrame, text = 'Amazon AWS user credentials', height = self.dims['sample h'] - 130, width = self.dims['sample w'] / 1.5 - 12, font = self.step_font)
            addKeyFrame = LabelFrame(self.awsFrame, text = 'Add .pem key', height = self.dims['sample h'] / 4.0 + 15, width = self.dims['sample w'] / 1.5 - 12, font = self.step_font)
            
            credsFrame.grid_propagate(0)
            addKeyFrame.grid_propagate(0)
            
            # self.awsFrame widgets
            exit_aws_creds_window = Button(self.awsFrame, text = 'Close', command = self.exit_add_aws, width = 13, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
            
            # credsFrame widgets
            access_key_title = Message(credsFrame, text = 'Amazon AWS access key', width = self.dims['title w'], font = self.font)
            self.access_key_text = Text(credsFrame, bd = 2, height = 1, width = self.dims['text w'] + 13, padx = 5, pady = 4, font = self.font)
            secret_key_title = Message(credsFrame, text = 'Amazon AWS secret access key', width = self.dims['title w'], font = self.font)
            self.secret_key_text = Text(credsFrame, bd = 2, height = 1, width = self.dims['text w'] + 13, padx = 5, pady = 4, font = self.font)
            aws_user_title = Message(credsFrame, text = 'Amazon AWS user id', width = self.dims['title w'], font = self.font)
            self.aws_user_text = Text(credsFrame, bd = 2, height = 1, width = self.dims['text w'] + 13, padx = 5, pady = 4, font = self.font)
            update_aws_info = Button(credsFrame, text = 'Update', command = self.update_aws_creds, width = 12, height = 1, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            
            # addKeyFrame widgets
            ec2_key_path_title = Message(addKeyFrame, text = 'Filepath to new EC2 key', width = self.dims['title w'], font = self.font)
            self.ec2_key_path_text = Text(addKeyFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
            ec2_path_browse = Button(addKeyFrame, text = 'Browse', command = self.browse_ec2_path, width = 12, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            add_new_key = Button(addKeyFrame, text = 'Add key', command = self.add_new_ec2_key, width = 12, height = 1, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            
            # self.awsFrame grid items
            credsFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 1)
            addKeyFrame.grid(row = 2, column = 1, rowspan = 1, columnspan = 1)
            exit_aws_creds_window.grid(row = 4, column = 1, rowspan = 1, columnspan = 1, sticky = 'e')
            
            Message(self.awsFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1)
            Message(self.awsFrame, text = '', width = 1, font = self.font).grid(row = 3, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(50):
                self.awsFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.awsFrame.rowconfigure(i, weight = 1)
            
            # credsFrame grid items
            access_key_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 10, sticky = 'w')
            self.access_key_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            secret_key_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 10, sticky = 'w')
            self.secret_key_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            aws_user_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 10, sticky = 'w')
            self.aws_user_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            update_aws_info.grid(row = 7, column = 1, rowspan = 1, columnspan = 1, sticky = 'e')
            
            Message(credsFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
            Message(credsFrame, text = '', width = 1, font = self.font).grid(row = 6, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(50):
                credsFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                credsFrame.rowconfigure(i, weight = 1)
        
            # addKeyFrame grid items
            ec2_key_path_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 10, sticky = 'nw')
            self.ec2_key_path_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'new')
            ec2_path_browse.grid(row = 1, column = 9, rowspan = 1, columnspan = 1, sticky = 'nw')
            add_new_key.grid(row = 2, column = 9, rowspan = 1, columnspan = 1, sticky = 'nw')
            
            Message(addKeyFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(1000):
                addKeyFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                addKeyFrame.rowconfigure(i, weight = 1)
            
            self.ec2_key_path_text.delete('1.0', 'end')
            self.ec2_key_path_text.insert('insert', '')
            self.ec2_key_path_text.config(state = 'disabled')
            
            self.size_window(self.awsFrame, self.centered_width_aws, self.centered_height_aws, self.x_aws, self.y_aws)
            self.awsFrame.focus()  
            
            self.awsFrame.protocol('WM_DELETE_WINDOW', self.exit_add_aws)
        elif self.add_aws_flag == True:    
            self.awsFrame.focus()
    
    def update_aws_creds(self):
        new_key = str(self.access_key_text.get('1.0', 'end')).strip()
        new_secret_key = str(self.secret_key_text.get('1.0', 'end')).strip()
        new_user_id = str(self.aws_user_text.get('1.0', 'end')).strip()
        
        self.update_sc_basic_config('starcluster', 'AWS_ACCESS_KEY_ID', new_key)
        self.update_sc_basic_config('starcluster', 'AWS_SECRET_ACCESS_KEY', new_secret_key)
        self.update_sc_basic_config('starcluster', 'AWS_USER_ID', new_user_id)
        
        self.awsFrame.focus()
    
    def add_new_ec2_key(self):
        ec2_key_path = self.ec2_key_path_text.get('1.0', 'end').strip()
        
        if ec2_key_path != '':
            ec2_key_basename = os.path.basename(ec2_key_path).strip()
            ec2_key_basename_split = ec2_key_basename.split('.')
            ec2_key_name = ec2_key_basename_split[0].strip()
            ec2_key_type = ec2_key_basename_split[1].strip().lower()
        
            if ec2_key_type == 'pem':
                self.key_choice_text.config(state = 'normal')
                self.key_choice_text.delete('1.0', 'end')
                self.key_choice_text.insert('insert', ec2_key_name)
                self.key_choice_text.config(state = 'disabled')
                
                f = open(starcluster_config_path, 'r')
                lines = f.readlines()
                f.close()
                
                name_exists = False
                
                for line in lines:
                    line = str(line).strip()
                    
                    if line == '[key ' + ec2_key_name + ']':
                        name_exists = True
                    elif line != '':
                        if line[0] == '#':
                            if line.split('#')[1].strip() == '[key ' + ec2_key_name + ']':
                                name_exists = True
                
                cluster_section_flag = False
                key_section_flag = False
                correct_key_flag = False
                added_key = False
                
                starcluster_output_data = []
                for line in lines:
                    line = str(line).strip()
                    
                    if line == '[cluster mspycloud]':
                        cluster_section_flag = True
                        
                    if line == '## Defining Additional Cluster Templates ##':
                        cluster_section_flag = False
                        
                    if line == '# match your key name e.g.:':
                        key_section_flag = True
                        
                    if line == '## Defining Cluster Templates ##':
                        key_section_flag = False
                    
                    if cluster_section_flag == True:
                        split_line = line.split('=')    
                        
                        key = split_line[0].strip().lower()
                        if key == '':
                            new_line = line
                        elif key == 'keyname':
                            new_line = 'KEYNAME = ' + ec2_key_name  
                        elif key[0] == '#':
                            if key.split('#')[1].strip() == 'keyname':
                                new_line = 'KEYNAME = ' + ec2_key_name
                            else:
                                new_line = line
                        else:
                            new_line = line
                    elif key_section_flag == True:
                        if name_exists == False:
                            if added_key == False:
                                new_line = line + '\n'
                                new_line += '[key ' + ec2_key_name + ']\n'
                                new_line += 'KEY_LOCATION = ' + ec2_key_path + '\n'
                                added_key = True
                            else:
                                new_line = line
                        elif name_exists == True:  
                            if correct_key_flag == True: 
                                if line.split(' ')[0].strip() == '[key':
                                    correct_key_flag = False
                                elif line != '':
                                    if line[0] == '#':
                                        if line.split('#')[1].strip().split(' ')[0].strip() == '[key':
                                            correct_key_flag = False                                    
                                                                                                    
                            if line == '[key ' + ec2_key_name + ']':
                                correct_key_flag = True
                            elif line != '':
                                if line[0] == '#':
                                    if line.split('#')[1].strip() == '[key ' + ec2_key_name + ']':
                                        correct_key_flag = True
                            
                            if correct_key_flag == True:
                                if line == '################################':
                                    correct_key_flag = False
                                    new_line = line
                                elif line == '':
                                    new_line = line
                                elif line.split('=')[0].strip().lower() == 'key_location':
                                    new_line = 'KEY_LOCATION = ' + ec2_key_path
                                elif line[0] == '#':
                                    if line.split('#')[1].strip().split('=')[0].strip().lower() == 'key_location':
                                        new_line = 'KEY_LOCATION = ' + ec2_key_path
                                    elif line.split('#')[1].strip() == '[key ' + ec2_key_name + ']':
                                        new_line = line[1:].strip()
                                    else:
                                        new_line = line
                                else:
                                    new_line = line
                            else:
                                new_line = line 
                    else:
                        new_line = line
                    
                    starcluster_output_data.append(new_line + '\n')        
                            
                f = open(starcluster_config_path, 'w')
                for starcluster_output_line in starcluster_output_data:
                    f.write(starcluster_output_line)
                f.close()
                
                self.awsFrame.focus()
            else:
                tkMessageBox.showinfo('Error!', 'Invalid key type.  Must be .pem key')
                self.awsFrame.focus()
        else:
            tkMessageBox.showinfo('Error!', 'Invalid key type.  Must be .pem key')
            self.awsFrame.focus()
    
    def browse_ec2_path(self):  
        ec2_key_path = askopenfilename(initialdir = 'C:\\').strip().replace('/', '\\')
        
        if ec2_key_path != '\\' and ec2_key_path != '':
            self.ec2_key_path_text.config(state = 'normal')
            self.ec2_key_path_text.delete('1.0', 'end')
            self.ec2_key_path_text.insert('insert', ec2_key_path)
            self.ec2_key_path_text.config(state = 'disabled')
            
        self.awsFrame.focus()
        
    def select_ec2_key(self):
        if self.select_ec2_flag == False:
            self.selectEC2Frame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 210, width = self.dims['sample w'])
            self.selectEC2Frame.grid_propagate(0)
            self.selectEC2Frame.title('EC2 key selection')
            
            self.x_select_ec2 = (self.width / 2) - (self.dims['sample w'] / 2.0)
            self.y_select_ec2 = (self.height / 2) - ((self.dims['sample h'] * 2 - 250) / 2.0)
            
            self.centered_height_select_ec2 = 0.77 * (self.dims['sample h'] * 2 - 100)
            self.centered_width_select_ec2 = 1.444 * self.centered_height_select_ec2
            
            self.select_ec2_flag = True
            
            # self.selectEC2Frame widgets
            keyFrame = LabelFrame(self.selectEC2Frame, text = 'Highlight .pem key and click "Select" button', height = self.dims['sample h'] + 5, width = self.dims['sample w'] - 285, font = self.step_font)
            make_key_selection = Button(self.selectEC2Frame, text = 'Select', command = self.update_selected_ec2, width = 13, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            exit_selectEC2_window = Button(self.selectEC2Frame, text = 'Close', command = self.exit_select_ec2, width = 13, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)

            keyFrame.grid_propagate(0)

            # keyFrame widgets
            self.key_choice = Listbox(keyFrame, height = 20, width = 86, relief = 'ridge', font = self.font)
            self.key_choice_scrollbar = Scrollbar(keyFrame, command = self.key_choice.yview, takefocus = 0)
            
            self.key_choice['yscrollcommand'] = self.key_choice_scrollbar.set
            
            # self.selectEC2Frame grid items
            keyFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            make_key_selection.grid(row = 3, column = 1, rowspan = 1, columnspan = 2, sticky = 'se')
            exit_selectEC2_window.grid(row = 4, column = 1, rowspan = 1, columnspan = 2, sticky = 'ne')
            
            Message(self.selectEC2Frame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            Message(self.selectEC2Frame, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            
            for i in range(50):
                self.selectEC2Frame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.selectEC2Frame.rowconfigure(i, weight = 1)
            
            # keyFrame grid items
            self.key_choice.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            self.key_choice_scrollbar.grid(row = 0, column = 2, rowspan = 1, columnspan = 1, sticky = 'nse')  
            
            Message(keyFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')   
            
            for i in range(50):
                keyFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                keyFrame.rowconfigure(i, weight = 1)                                                                                              
            
            self.current_keys = list(self.key_choice.get(0, 'end'))  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
            self.size_window(self.selectEC2Frame, self.centered_width_select_ec2, self.centered_height_select_ec2, self.x_select_ec2, self.y_select_ec2)
            self.selectEC2Frame.focus()
            
            f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
            lines = f.readlines()
            f.close()
            
            new_keys = []
            key_section_flag = False
            for line in lines:
                line = line.strip()
                
                if line == '## Defining EC2 Keypairs ##':
                    key_section_flag = True
                    continue
                    
                if line == '## Defining Cluster Templates ##':
                    break
                    
                if key_section_flag == True: 
                    if line.split(' ')[0].strip() == '[key':
                        key_name = line.split('[key')[-1].split(']')[0].strip()
                        new_keys.append(key_name)
                    elif line != '':
                        if line[0] == '#':
                            if line.split('#')[1].strip().split(' ')[0].strip() == '[key':
                                key_name = line.split('[key')[-1].split(']')[0].strip()
                                new_keys.append(key_name) 
                        
            # update listbox contents
            if new_keys != self.current_keys:
                self.current_keys = new_keys
                
                self.key_choice.delete(0, 'end')
                for k in self.current_keys:
                    self.key_choice.insert(-1, k)   
                
                scroll_pos = self.key_choice.yview()
                self.key_choice.yview('moveto', scroll_pos[0])
            
                self.size_window(self.selectEC2Frame, self.centered_width_select_ec2, self.centered_height_select_ec2, self.x_select_ec2, self.y_select_ec2)
            
            self.selectEC2Frame.protocol('WM_DELETE_WINDOW', self.exit_select_ec2)
        elif self.select_ec2_flag == True:
            self.selectEC2Frame.focus()
    
    def update_selected_ec2(self):   
        selected_key = str(self.key_choice.get(self.key_choice.curselection())).strip()
        
        self.key_choice_text.config(state = 'normal')
        self.key_choice_text.delete('1.0', 'end')
        self.key_choice_text.insert('insert', selected_key)
        self.key_choice_text.config(state = 'disabled')
        
        f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
        lines = f.readlines()
        f.close()
        
        out_f_data = []
        
        cluster_section_flag = False
        key_section_flag = False
        correct_key_flag = False
        for line in lines:
            new_line = ''
            line = line.strip()
            
            if line == '## Defining EC2 Keypairs ##':
                key_section_flag = True
                
            if line == '## Defining Cluster Templates ##':
                key_section_flag = False
            
            if line == '[cluster mspycloud]':
                cluster_section_flag = True
                
            if line == '## Defining Additional Cluster Templates ##':
                cluster_section_flag = False
              
            if cluster_section_flag == True:
                if line.split('=')[0].strip().lower() == 'keyname':
                    new_line = 'KEYNAME = ' + selected_key
                elif line == '':
                    new_line = line
                elif line[0] == '#':
                    if line.split('#')[1].strip().split('=')[0].strip().lower() == 'keyname':
                        new_line = 'KEYNAME = ' + selected_key
                    else:
                        new_line = line  
                else:
                    new_line = line
            elif key_section_flag == True:
                if correct_key_flag == True: 
                    if line.split(' ')[0].strip() == '[key':
                        correct_key_flag = False
                    elif line != '':
                        if line[0] == '#':
                            if line.split('#')[1].strip().split(' ')[0].strip() == '[key':
                                correct_key_flag = False
                
                if line == '[key ' + selected_key + ']':
                    correct_key_flag = True
                elif line != '':
                    if line[0] == '#':
                        if line.split('#')[1].strip() == '[key ' + selected_key + ']':
                            correct_key_flag = True
                    
                if correct_key_flag == True:
                    if line == '################################':
                        correct_key_flag = False
                        new_line = line
                    elif line == '':
                        new_line = line
                    elif line[0] == '#':
                        if line.split('#')[1].strip().split('=')[0].strip().lower() == 'key_location':
                            new_line = line[1:].strip()
                        elif line.split('#')[1].strip() == '[key ' + selected_key + ']':
                            new_line = line[1:].strip()
                        else:
                            new_line = line
                    else:
                        new_line = line
                else:
                    new_line = line
            else:
                new_line = line
            
            out_f_data.append(new_line + '\n')    
        
        out_f = open(self.pipeline_path + 'configs-starcluster\\starcluster' , 'w')
        for out_f_line in out_f_data:
            out_f.write(out_f_line)                
        out_f.close()
    
    def select_ebs_volume(self):
        if self.select_ebs_flag == False:
            self.selectEBSFrame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 210, width = self.dims['sample w'])
            self.selectEBSFrame.grid_propagate(0)
            self.selectEBSFrame.title('EBS volume selection')
            
            self.x_select_ebs = (self.width / 2) - (self.dims['sample w'] / 2.0)
            self.y_select_ebs = (self.height / 2) - ((self.dims['sample h'] * 2 - 250) / 2.0)
            
            self.centered_height_select_ebs = 0.77 * (self.dims['sample h'] * 2 - 100)
            self.centered_width_select_ebs = 1.444 * self.centered_height_select_ebs
            
            self.select_ebs_flag = True
            
            # self.selectEBSFrame widgets
            volumeFrame = LabelFrame(self.selectEBSFrame, text = 'Highlight volume and click "Select" button', height = self.dims['sample h'] + 5, width = self.dims['sample w'] - 285, font = self.step_font)
            make_volume_selection = Button(self.selectEBSFrame, text = 'Select', command = self.update_selected_volume, width = 13, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            exit_selectEBS_window = Button(self.selectEBSFrame, text = 'Close', command = self.exit_select_ebs, width = 13, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)

            volumeFrame.grid_propagate(0)

            # volumeFrame widgets
            self.volume_choice = Listbox(volumeFrame, height = 20, width = 86, relief = 'ridge', font = self.font)
            self.volume_choice_scrollbar = Scrollbar(volumeFrame, command = self.volume_choice.yview, takefocus = 0)
            
            self.volume_choice['yscrollcommand'] = self.volume_choice_scrollbar.set
            
            # self.selectEBSFrame grid items
            volumeFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            make_volume_selection.grid(row = 3, column = 1, rowspan = 1, columnspan = 2, sticky = 'se')
            exit_selectEBS_window.grid(row = 4, column = 1, rowspan = 1, columnspan = 2, sticky = 'ne')
            
            Message(self.selectEBSFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            Message(self.selectEBSFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            
            for i in range(50):
                self.selectEBSFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.selectEBSFrame.rowconfigure(i, weight = 1) 
            
            # volumeFrame grid items
            self.volume_choice.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            self.volume_choice_scrollbar.grid(row = 0, column = 2, rowspan = 1, columnspan = 1, sticky = 'nse')  
            
            Message(volumeFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            
            for i in range(50):
                volumeFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                volumeFrame.rowconfigure(i, weight = 1)                                                                                                  
            
            self.current_volumes = list(self.volume_choice.get(0, 'end'))  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
            self.size_window(self.selectEBSFrame, self.centered_width_select_ebs, self.centered_height_select_ebs, self.x_select_ebs, self.y_select_ebs)
            self.selectEBSFrame.focus()
            
            f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
            lines = f.readlines()
            f.close()
            
            new_volumes = []
            volume_section_flag = False
            for line in lines:
                line = line.strip()
                
                if line == '# Sections starting with "volume" define your EBS volumes':
                    volume_section_flag = True
                    continue
                    
                if line == '## Configuring Security Group Permissions ##':
                    break
                    
                if volume_section_flag == True: 
                    if line.split(' ')[0].strip() == '[volume':
                        volume_name = line.split('[volume')[-1].split(']')[0].strip()
                        new_volumes.append(volume_name)
                    elif line != '':
                        if line[0] == '#':
                            if line.split('#')[1].strip().split(' ')[0].strip() == '[volume':
                                volume_name = line.split('[volume')[-1].split(']')[0].strip()
                                new_volumes.append(volume_name)
                        
            # update listbox contents
            if new_volumes != self.current_volumes:
                self.current_volumes = new_volumes
                
                self.volume_choice.delete(0, 'end')
                for vol in self.current_volumes:
                    self.volume_choice.insert(-1, vol)   
                
                scroll_pos = self.volume_choice.yview()
                self.volume_choice.yview('moveto', scroll_pos[0])
            
                self.size_window(self.selectEBSFrame, self.centered_width_select_ebs, self.centered_height_select_ebs, self.x_select_ebs, self.y_select_ebs)
        
            self.selectEBSFrame.protocol('WM_DELETE_WINDOW', self.exit_select_ebs)
        elif self.select_ebs_flag == True:
            self.selectEBSFrame.focus()
    
    def update_selected_volume(self):   
        selected_volume = str(self.volume_choice.get(self.volume_choice.curselection())).strip()
        
        self.update_config('ebs_params.txt', 'volume_choice', selected_volume)
        
        self.volume_choice_text.config(state = 'normal')
        self.volume_choice_text.delete('1.0', 'end')
        self.volume_choice_text.insert('insert', selected_volume)
        self.volume_choice_text.config(state = 'disabled')
        
        f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
        lines = f.readlines()
        f.close()
        
        out_f_data = []
        
        cluster_section_flag = False
        volume_section_flag = False
        correct_volume_flag = False
        for line in lines:
            new_line = ''
            line = line.strip()
            
            if line == '[cluster mspycloud]':
                cluster_section_flag = True
                
            if line == '## Defining Additional Cluster Templates ##':
                cluster_section_flag = False
            
            if line == '# Sections starting with "volume" define your EBS volumes':
                volume_section_flag = True
                
            if line == '## Configuring Security Group Permissions ##':
                volume_section_flag = False
            
            if cluster_section_flag == True:
                if line.split('=')[0].strip().lower() == 'volumes':
                    new_line = 'VOLUMES = ' + selected_volume
                elif line == '':
                    new_line = line
                elif line[0] == '#':
                    if line.split('#')[1].strip().split('=')[0].strip().lower() == 'volumes':
                        new_line = 'VOLUMES = ' + selected_volume 
                    else:
                        new_line = line 
                else:
                    new_line = line 
            elif volume_section_flag == True:
                if correct_volume_flag == True: 
                    if line.split(' ')[0].strip() == '[volume':
                        correct_volume_flag = False
                    elif line != '':
                        if line[0] == '#':
                            if line.split('#')[1].strip().split(' ')[0].strip() == '[volume':
                                correct_volume_flag = False
                
                if line == '[volume ' + selected_volume + ']':
                    correct_volume_flag = True
                elif line != '':
                    if line[0] == '#':
                        if line.split('#')[1].strip() == '[volume ' + selected_volume + ']':
                            correct_volume_flag = True
                                
                if correct_volume_flag == True:
                    if line == '############################################':
                        correct_volume_flag = False
                        new_line = line
                    elif line == '':
                        new_line = line
                    elif line[0] == '#':
                        if line.split('#')[1].strip().split('=')[0].strip().lower() in ['volume_id', 'mount_path', 'partition']:
                            new_line = line[1:].strip()
                        elif line.split('#')[1].strip() == '[volume ' + selected_volume + ']':
                            new_line = line[1:].strip()
                        else:
                            new_line = line
                    else:
                        new_line = line
                else:
                    new_line = line
            else:
                new_line = line
                
            out_f_data.append(new_line + '\n')
                        
        out_f = open(self.pipeline_path + 'configs-starcluster\\starcluster' , 'w')
        for out_f_line in out_f_data:
            out_f.write(out_f_line)
        out_f.close()
    
    def make_ebs_display(self):
        if self.make_ebs_flag == False:
            self.makeEBSFrame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 310, width = self.dims['sample w'])
            self.makeEBSFrame.grid_propagate(0)
            self.makeEBSFrame.title('Set ebs_params.txt configuration file')
            
            self.x_ebs = (self.width / 2) - (self.dims['sample w'] / 2.0)
            self.y_ebs = (self.height / 2) - ((self.dims['sample h'] * 2 - 250) / 2.0)
            
            self.centered_height_ebs = 0.70 * (self.dims['sample h'] * 2 - 200)
            self.centered_width_ebs = 1.165 * self.centered_height_ebs
            
            self.make_ebs_flag = True
            
            volumeFrame = LabelFrame(self.makeEBSFrame, text = 'New EBS volume settings', height = self.dims['sample h'] - 80, width = self.dims['sample w'] - 550, font = self.step_font)
            exit_makeEBS_window = Button(self.makeEBSFrame, text = 'Close', command = self.exit_make_ebs, width = 13, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
            
            volumeFrame.grid_propagate(0)
            
            # volumeFrame widgets
            volume_name_title = Message(volumeFrame, text = 'Volume name', width = self.dims['title w'], font = self.font)
            self.volume_name_text = Text(volumeFrame, bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
            volume_size_title = Message(volumeFrame, text = 'Volume size (gigabytes)', width = self.dims['title w'], font = self.font)
            self.volume_size_text = Text(volumeFrame, bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
            volume_zone_title = Message(volumeFrame, text = 'Volume zone (e.g. us-east-1a)', width = self.dims['title w'], font = self.font)
            self.volume_zone_text = Text(volumeFrame, bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
            volume_mount_path_title = Message(volumeFrame, text = 'Mount path', width = self.dims['title w'], font = self.font)
            self.volume_mount_path_text = Text(volumeFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'], padx = 5, pady = 4, font = self.font)
            ebs_params_button = Button(volumeFrame, text = 'EBS parameters', command = self.view_ebs_params, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            create_ebs_button = Button(volumeFrame, text = 'Create volume', command = self.create_new_ebs, width = 15, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            
            # self.makeEBSFrame grid items
            volumeFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 2, sticky = 'w')
            exit_makeEBS_window.grid(row = 3, column = 2, sticky = 'e')  
            
            Message(self.makeEBSFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            Message(self.makeEBSFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            
            for i in range(50):
                self.makeEBSFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.makeEBSFrame.rowconfigure(i, weight = 1)
            
            # volumeFrame grid items
            volume_name_title.grid(row = 1, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.volume_name_text.grid(row = 2, column = 2, rowspan = 1, columnspan = 9, sticky = 'w')
            volume_size_title.grid(row = 3, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.volume_size_text.grid(row = 4, column = 2, rowspan = 1, columnspan = 9, sticky = 'w')  
            volume_zone_title.grid(row = 5, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.volume_zone_text.grid(row = 6, column = 2, rowspan = 1, columnspan = 9, sticky = 'w')  
            volume_mount_path_title.grid(row = 7, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.volume_mount_path_text.grid(row = 8, column = 2, rowspan = 1, columnspan = 9, sticky = 'w')
            #ebs_params_button.grid(row = 10, column = 2, rowspan = 1, columnspan = 9, sticky = 'w')
            create_ebs_button.grid(row = 11, column = 2, rowspan = 1, columnspan = 9, sticky = 'w')
            
            Message(volumeFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'w') 
            Message(volumeFrame, text = '', width = 1, font = self.font).grid(row = 9, column = 1, rowspan = 1, columnspan = 1, sticky = 'w') 
            
            for i in range(50):
                volumeFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                volumeFrame.rowconfigure(i, weight = 1)               
            
            ebs_params = read_new_ebs_config(self.config_path + 'ebs_params.txt')
            self.volume_name_text.delete('1.0', 'end')
            self.volume_name_text.insert('insert', ebs_params['volume_name'])
            self.volume_size_text.delete('1.0', 'end')
            self.volume_size_text.insert('insert', ebs_params['volume_size'])
            self.volume_zone_text.delete('1.0', 'end')
            self.volume_zone_text.insert('insert', ebs_params['volume_zone'])
            self.volume_mount_path_text.config(state = 'normal')
            self.volume_mount_path_text.delete('1.0', 'end')
            self.volume_mount_path_text.insert('insert', ebs_params['mount_path'])
            self.volume_mount_path_text.config(state = 'disabled')
            
            self.current_volume_name = self.volume_name_text.get('1.0', 'end').strip()
            self.current_volume_size = self.volume_size_text.get('1.0', 'end').strip()
            self.current_volume_zone = self.volume_zone_text.get('1.0', 'end').strip()
            self.current_volume_mount = self.volume_mount_path_text.get('1.0', 'end').strip()
                                                            
            self.size_window(self.makeEBSFrame, self.centered_width_ebs, self.centered_height_ebs, self.x_ebs, self.y_ebs)
            self.makeEBSFrame.focus()
            
            self.makeEBSFrame.protocol('WM_DELETE_WINDOW', self.exit_make_ebs)
        elif self.make_ebs_flag == True:
            self.makeEBSFrame.focus()
    
    def quant_display(self):
        quantFrame = LabelFrame(self.canvas, text = '3 - Quantitation', height = self.dims['sample h'] + 30, width = self.dims['sample w'], padx = 5, pady = 0, font = self.title_font)
        quantFrame.grid_propagate(0)
        
        matrixFrame = LabelFrame(quantFrame, text = 'Step 6 & 7 parameters', padx = 5, pady = 0, height = 362, width = self.dims['sample w'] / 2.0, font = self.step_font)
        stepFrame = LabelFrame(quantFrame, text = 'Select steps', padx = 5, pady = 0, height = 362, width = self.dims['sample w'] / 2.0 - 15, font = self.step_font)
        
        matrixFrame.grid_propagate(0)
        stepFrame.grid_propagate(0)
        
        # matrixFrame widgets
        channelFrame = LabelFrame(matrixFrame, text = 'Reference channel', height = self.dims['sample h'] - 20, width = self.dims['title w'] / 2.0 - 6, padx = 5, pady = 0, font = self.step_font)
        quantConfigFrame = LabelFrame(matrixFrame, text = 'Quantitation settings', height = self.dims['sample h'] - 20, width = self.dims['title w'] / 2.0 - 6, padx = 5, pady = 0, font = self.step_font)
        
        channelFrame.grid_propagate(0)
        quantConfigFrame.grid_propagate(0)
        
        # channelFrame widgets
        label_cb_title = Message(channelFrame, text = 'Reagent label channels', width = self.dims['title w'], font = self.font)
        #itraq_title = Message(channelFrame, text = 'iTRAQ channels', width = self.dims['title w'], font = self.font)
        #tmt_title = Message(channelFrame, text = 'TMT channels', width = self.dims['title w'], font = self.font)
        self.reference_channel_cb = ttk.Combobox(channelFrame, values = label_channels, textvariable = self.channelVar, state = 'readonly', height = 11, width = 20, font = self.font)
        #self.channel113 = Radiobutton(channelFrame, text = '113', variable = self.channelVar, value = '113', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel114 = Radiobutton(channelFrame, text = '114', variable = self.channelVar, value = '114', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel115 = Radiobutton(channelFrame, text = '115', variable = self.channelVar, value = '115', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel116 = Radiobutton(channelFrame, text = '116', variable = self.channelVar, value = '116', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel117 = Radiobutton(channelFrame, text = '117', variable = self.channelVar, value = '117', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel118 = Radiobutton(channelFrame, text = '118', variable = self.channelVar, value = '118', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel119 = Radiobutton(channelFrame, text = '119', variable = self.channelVar, value = '119', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel121 = Radiobutton(channelFrame, text = '121', variable = self.channelVar, value = '121', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel126 = Radiobutton(channelFrame, text = '126', variable = self.channelVar, value = '126', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel127N = Radiobutton(channelFrame, text = '127N', variable = self.channelVar, value = '127N', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel127C = Radiobutton(channelFrame, text = '127C', variable = self.channelVar, value = '127C', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel128N = Radiobutton(channelFrame, text = '128N', variable = self.channelVar, value = '128N', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel128C = Radiobutton(channelFrame, text = '128C', variable = self.channelVar, value = '128C', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel129N = Radiobutton(channelFrame, text = '129N', variable = self.channelVar, value = '129N', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel129C = Radiobutton(channelFrame, text = '129C', variable = self.channelVar, value = '129C', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel130N = Radiobutton(channelFrame, text = '130N', variable = self.channelVar, value = '130N', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel130C = Radiobutton(channelFrame, text = '130C', variable = self.channelVar, value = '130C', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel131 = Radiobutton(channelFrame, text = '131', variable = self.channelVar, value = '131', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        #self.channel131C = Radiobutton(channelFrame, text = '131C', variable = self.channelVar, value = '131C', command = self.write_config_channel, activeforeground = 'blue', font = self.font)
        
        # bind the write_config_channel function to the combobox
        self.reference_channel_cb.bind("<<ComboboxSelected>>", self.write_config_channel)
        
        # quantConfigFrame widgets
        intensity_threshold_title = Message(quantConfigFrame, text = 'PSM intensity threshold', width = self.dims['title w'], font = self.font)
        self.intensity_threshold_text = Text(quantConfigFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
        flr_title = Message(quantConfigFrame, text = 'False localization rate (%)', width = self.dims['title w'], font = self.font)
        self.flr_text = Text(quantConfigFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
        luciphor2_title = Message(quantConfigFrame, text = 'LuciPHOr2 settings', width = self.dims['title w'], font = self.font)
        set_luciphor2 = Button(quantConfigFrame, text = 'LuciPHOr2', command = self.open_luciphor2, width = 18, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        contams_title = Message(quantConfigFrame, text = '.fasta contaminant entries to remove', width = self.dims['title w'], font = self.font)
        set_contams = Button(quantConfigFrame, text = 'Contaminants', command = self.open_contams, width = 18, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)  
        matrix_header_title = Message(quantConfigFrame, text = 'Expression matrix header settings', width = self.dims['title w'], font = self.font)
        set_matrix_header = Button(quantConfigFrame, text = 'Patient sample IDs', command = self.open_matrix_header, width = 18, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # stepFrame widgets
        self.step6 = Checkbutton(stepFrame, text = '( 6 ) Peptide and protein quantitation', variable = self.step6Var, onvalue = 1, offvalue = 0, activeforeground = 'blue', disabledforeground = 'black', font = self.font)
        self.step7 = Checkbutton(stepFrame, text = '( 7 ) Protein expression matrix', variable = self.step7Var, onvalue = 1, offvalue = 0, activeforeground = 'blue', disabledforeground = 'black', font = self.font)
        stepAll = Button(stepFrame, text = 'Select steps 6 - 7', command = self.all_steps_quant, bg = 'blue', fg = 'white', activeforeground = 'blue', width = 15, font = self.font)
        stepClear = Button(stepFrame, text = 'Clear steps 6 - 7', command = self.clear_steps_quant, bg = 'blue', fg = 'white', activeforeground = 'blue', width = 15, font = self.font)
        
        # quantFrame grid items
        matrixFrame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        stepFrame.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(2):
            quantFrame.columnconfigure(i, weight = 1)
            
        for i in range(1):
            quantFrame.rowconfigure(i, weight = 1)
        
        # stepFrame grid items
        self.step6.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'sw')
        self.step7.grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'nw')
        stepAll.grid(row = 4, column = 0, rowspan = 1, columnspan = 1, sticky = 'sw')
        stepClear.grid(row = 5, column = 0, rowspan = 1, columnspan = 1, sticky = 'nw')
        
        for i in range(1):
            stepFrame.columnconfigure(i, weight = 1)
          
        for i in range(100):
            stepFrame.rowconfigure(i, weight = 1)
        
        # matrixFrame grid items
        channelFrame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        quantConfigFrame.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'w') 
        
        for i in range(2):
            matrixFrame.columnconfigure(i, weight = 1)
            
        for i in range(1):
            matrixFrame.rowconfigure(i, weight = 1)
        
        # channelFrame grid items
        label_cb_title.grid(row = 1, column = 1, rowspan = 1, columnspan = 15, sticky = 'w')
        #itraq_title.grid(row = 1, column = 1, rowspan = 1, columnspan = 9, sticky = 'w')
        #tmt_title.grid(row = 1, column = 10, rowspan = 1, columnspan = 5, sticky = 'w')
        self.reference_channel_cb.grid(row = 2, column = 2, rowspan = 1, columnspan = 13, sticky = 'w')
        #self.channel113.grid(row = 2, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel114.grid(row = 3, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel115.grid(row = 4, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel116.grid(row = 5, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel117.grid(row = 6, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel118.grid(row = 7, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel119.grid(row = 8, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel121.grid(row = 9, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel126.grid(row = 2, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel127N.grid(row = 3, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel127C.grid(row = 4, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel128N.grid(row = 5, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel128C.grid(row = 6, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel129N.grid(row = 7, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel129C.grid(row = 8, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel130N.grid(row = 9, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel130C.grid(row = 10, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel131.grid(row = 11, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.channel131C.grid(row = 12, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
       
        Message(channelFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(13):
            channelFrame.columnconfigure(i, weight = 1)
            
        for i in range(100):
            channelFrame.rowconfigure(i, weight = 1)
        
        # quantConfigFrame grid items
        intensity_threshold_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        self.intensity_threshold_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        flr_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        self.flr_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        luciphor2_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        set_luciphor2.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        contams_title.grid(row = 7, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        set_contams.grid(row = 8, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        matrix_header_title.grid(row = 9, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        set_matrix_header.grid(row = 10, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        
        Message(quantConfigFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(quantConfigFrame, text = '', width = 1, font = self.font).grid(row = 6, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(52):
            quantConfigFrame.columnconfigure(i, weight = 1)
            
        for i in range(100):
            quantConfigFrame.rowconfigure(i, weight = 1)
        
        quantFrame.grid(row = 3, column = 1, rowspan = 1, columnspan = 1)
    
    def protinf_display(self):
        infFrame = LabelFrame(self.canvas, text = '2 - Protein inference', height = self.dims['sample h']  + 210, width = self.dims['sample w'], padx = 5, pady = 0, font = self.title_font)
        infFrame.grid_propagate(0)
        
        protFrame = LabelFrame(infFrame, text = 'Step 4 & 5 common parameters', padx = 5, pady = 0, height = 190, width = self.dims['sample w'] / 2.0, font = self.step_font)
        step4Frame = LabelFrame(infFrame, text = 'Step 4 parameters only', padx = 5, pady = 0, height = 348, width = self.dims['sample w'] / 2.0, font = self.step_font)
        stepFrame = LabelFrame(infFrame, text = 'Select steps', padx = 5, pady = 0, height = 542, width = self.dims['sample w'] / 2.0 - 15, font = self.step_font)
        
        step4Frame.grid_propagate(0)
        protFrame.grid_propagate(0)
        stepFrame.grid_propagate(0)
        
        # step4Frame widgets
        apply_correction_factors_title = Message(step4Frame, text = 'Apply correction factors (TMT only)', width = self.dims['title w'], font = self.font)
        self.apply_correction_true = Radiobutton(step4Frame, text = 'True', variable = self.correctionMatrixVar, command = self.write_config_correction_matrix, value = 'true', activeforeground = 'blue', font = self.font)
        apply_correction_false = Radiobutton(step4Frame, text = 'False', variable = self.correctionMatrixVar, command = self.write_config_correction_matrix, value = 'false', activeforeground = 'blue', font = self.font)
        correction_factors_title = Message(step4Frame, text = 'Correction factor values', width = self.dims['title w'], font = self.font)
        set_correction_factors = Button(step4Frame, text = 'Correction factors', command = self.open_correction_matrix, width = 18, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # protFrame widgets
        n_met_title = Message(protFrame, text = 'N-terminus Methionine cleavage', width = self.dims['title w'], font = self.font)
        n_met_true = Radiobutton(protFrame, text = 'True', variable = self.nMetVar, command = self.write_config_n_met_cleavage, value = 'true', activeforeground = 'blue', font = self.font)
        n_met_false = Radiobutton(protFrame, text = 'False', variable = self.nMetVar, command = self.write_config_n_met_cleavage, value = 'false', activeforeground = 'blue', font = self.font)
        fdr_title = Message(protFrame, text = 'False discovery rate (% at PSM level)', width = self.dims['title w'], font = self.font)
        self.fdr_text = Text(protFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
        # IR score filter for intact glycopeptides
        ir_title = Message(protFrame, text = 'IR score minimum', width = self.dims['title w'], font = self.font)
        self.ir_text = Text(protFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
        morpheus_title = Message(protFrame, text = 'Morpheus score minimum', width = self.dims['title w'], font = self.font)
        self.morpheus_text = Text(protFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
        min_psm_title = Message(protFrame, text = 'Minimum # of psm per peptide', width = self.dims['title w'], font = self.font)
        self.min_psm_text = Text(protFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
        min_pep_title = Message(protFrame, text = 'Minimum # of peptides per protein', width = self.dims['title w'], font = self.font)
        self.min_pep_text = Text(protFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
        
        # stepFrame widgets
        self.step4 = Checkbutton(stepFrame, text = '( 4 ) Protein inference (includes PSM quantitation)', variable = self.step4Var, onvalue = True, offvalue = False, activeforeground = 'blue', disabledforeground = 'black', font = self.font)
        self.step5 = Checkbutton(stepFrame, text = '( 5 ) False discovery rate estimation', variable = self.step5Var, onvalue = True, offvalue = False, activeforeground = 'blue', disabledforeground = 'black', font = self.font)
        stepAll = Button(stepFrame, text = 'Select steps 4 - 5', command = self.all_steps_protinf, bg = 'blue', fg = 'white', activeforeground = 'blue', width = 15, font = self.font)
        stepClear = Button(stepFrame, text = 'Clear steps 4 - 5', command = self.clear_steps_protinf, bg = 'blue', fg = 'white', activeforeground = 'blue', width = 15, font = self.font)
        
        # infFrame grid items
        protFrame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        step4Frame.grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        stepFrame.grid(row = 0, column = 1, rowspan = 2, columnspan = 1, sticky = 'w')
        
        for i in range(2):
            infFrame.columnconfigure(i, weight = 1)
            
        for i in range(2):
            infFrame.rowconfigure(i, weight = 1)
        
        # step4Frame grid items
        apply_correction_factors_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 150, sticky = 'w')
        self.apply_correction_true.grid(row = 1, column = 1, rowspan = 1, columnspan = 25, sticky = 'w')
        apply_correction_false.grid(row = 1, column = 1, rowspan = 1, columnspan = 25, sticky = 'e')
        correction_factors_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        set_correction_factors.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        
        Message(step4Frame, text = '', font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
        
        for i in range(3000):
            step4Frame.columnconfigure(i, weight = 1)
             
        for i in range(150):
            step4Frame.rowconfigure(i, weight = 1)
          
        # protFrame grid items
        n_met_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 150, sticky = 'w')
        n_met_true.grid(row = 1, column = 1, rowspan = 1, columnspan = 25, sticky = 'w')
        n_met_false.grid(row = 1, column = 54, rowspan = 1, columnspan = 50, sticky = 'e')
        fdr_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 150, sticky = 'w')
        self.fdr_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 75, sticky = 'w')
        ir_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 150, sticky = 'w')
        self.ir_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 75, sticky = 'w')
        min_psm_title.grid(row = 0, column = 150, rowspan = 1, columnspan = 50, sticky = 'w')
        self.min_psm_text.grid(row = 1, column = 151, rowspan = 1, columnspan = 1, sticky = 'w')
        min_pep_title.grid(row = 2, column = 150, rowspan = 1, columnspan = 50, sticky = 'w')
        self.min_pep_text.grid(row = 3, column = 151, rowspan = 1, columnspan = 1, sticky = 'w')
        morpheus_title.grid(row = 4, column = 150, rowspan = 1, columnspan = 50, sticky = 'w')
        self.morpheus_text.grid(row = 5, column = 151, rowspan = 1, columnspan = 1, sticky = 'w')
        
        Message(protFrame, text = '', font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
        Message(protFrame, text = '', font = self.font).grid(row = 1, column = 150, rowspan = 1, columnspan = 1)
        
        for i in range(3000):
            protFrame.columnconfigure(i, weight = 1)
        
        for i in range(150):
            protFrame.rowconfigure(i, weight = 1)
        
        # stepFrame grid items
        self.step4.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'sw')
        self.step5.grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'nw')
        stepAll.grid(row = 6, column = 0, rowspan = 1, columnspan = 1, sticky = 'sw')
        stepClear.grid(row = 7, column = 0, rowspan = 1, columnspan = 1, sticky = 'nw')
        
        for i in range(1):
            stepFrame.columnconfigure(i, weight = 1)
         
        for i in range(300):
            stepFrame.rowconfigure(i, weight = 1)
        
        infFrame.grid(row = 1, column = 2, rowspan = 1, columnspan = 1)
    
    def msgf_display(self):
        if self.msgf_flag == False:
            self.msgfFrame = Toplevel(self.canvas, height = self.dims['msgf h'] * 2 - 310, width = self.dims['msgf w'])
            self.msgfFrame.grid_propagate(0)
            self.msgfFrame.title('MS-GF+ parameters')
            
            self.x_msgf = (self.width / 2) - (self.dims['msgf w'] / 2.0)
            self.y_msgf = (self.height / 2) - ((self.dims['msgf h'] * 2 - 250) / 2.0)
            
            self.centered_height_msgf = 0.95 * (self.dims['msgf h'] * 2 - 250)
            self.centered_width_msgf = 2.19 * self.centered_height_msgf
            
            self.msgf_flag = True
            
            Message(self.msgfFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 2, columnspan = 1)
            
            searchFrame = LabelFrame(self.msgfFrame, text = 'Search parameters', padx = 5, pady = 5, height = self.dims['msgf h'] * 2 - 285, width = self.dims['msgf w'] / 3.0 - 25, font = self.step_font)
            modFrame = LabelFrame(self.msgfFrame, text = 'Modifications', padx = 5, pady = 5, height = self.dims['msgf h'] * 2 - 285, width = self.dims['msgf w'] - 288, font = self.step_font)

            searchFrame.grid_propagate(0)
            modFrame.grid_propagate(0)
            
            # searchFrame widgets
            java_heap_search = Message(searchFrame, text = 'java heap size (Xmx)', width = self.dims['title w'], font = self.font)
            self.java_heap_search_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            t = Message(searchFrame, text = 't', width = self.dims['title w'], font = self.font)
            self.t_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            ti = Message(searchFrame, text = 'ti', width = self.dims['title w'], font = self.font)
            self.ti_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            msgf_thread = Message(searchFrame, text = 'thread', width = self.dims['title w'], font = self.font)
            self.msgf_thread_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            tda_search = Message(searchFrame, text = 'tda', width = self.dims['title w'], font = self.font)
            self.tda_search_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            m = Message(searchFrame, text = 'm', width = self.dims['title w'], font = self.font)
            self.m_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            inst = Message(searchFrame, text = 'inst', width = self.dims['title w'], font = self.font)
            self.inst_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            e = Message(searchFrame, text = 'e', width = self.dims['title w'], font = self.font)
            self.e_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            add_features = Message(searchFrame, text = 'addFeatures', width = self.dims['title w'], font = self.font)
            self.add_features_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            protocol = Message(searchFrame, text = 'p (protocol)', width = self.dims['title w'], font = self.font)
            self.protocol_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            ntt = Message(searchFrame, text = 'ntt', width = self.dims['title w'], font = self.font)
            self.ntt_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            min_length = Message(searchFrame, text = 'minLength', width = self.dims['title w'], font = self.font)
            self.min_length_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            max_length = Message(searchFrame, text = 'maxLength', width = self.dims['title w'], font = self.font)
            self.max_length_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            min_charge = Message(searchFrame, text = 'minCharge', width = self.dims['title w'], font = self.font)
            self.min_charge_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            max_charge = Message(searchFrame, text = 'maxCharge', width = self.dims['title w'], font = self.font)
            self.max_charge_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            n = Message(searchFrame, text = 'n', width = self.dims['title w'], font = self.font)
            self.n_text = Text(searchFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
            
            #  modFrame widgets
            msgf_available_mods = Message(modFrame, text = 'Available modifications', width = self.dims['title w'], font = self.font)
            self.msgf_available_mods = Text(modFrame, bd = 2, height = self.dims['sample text h'], width = self.dims['sample text w'] - 70, font = self.font)
            msgf_available_scrollbar = Scrollbar(modFrame, command = self.msgf_available_mods.yview, takefocus = 0)
            add_mod = Button(modFrame, text = '>', command = self.add_msgf_selected_mod, width = 1, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            remove_mod = Button(modFrame, text = '<', command = self.remove_msgf_selected_mod, width = 1, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            msgf_selected_mods = Message(modFrame, text = 'Selected modifications', width = self.dims['title w'], font = self.font)
            self.msgf_selected_mods = Text(modFrame, bd = 2, height = self.dims['sample text h'], width = self.dims['sample text w'] - 70, font = self.font)
            msgf_selected_scrollbar = Scrollbar(modFrame, command = self.msgf_selected_mods.yview, takefocus = 0)
            input_mod = Message(modFrame, text = "Input modification to add to 'Available modifications'", width = self.dims['title w'], font = self.font)
            self.input_mod_text = Text(modFrame, bd = 2, height = 1, width = self.dims['sample text w'] - 70, padx = 5, pady = 4, font = self.font)
            add_available_mod = Button(modFrame, text = 'Add modification', command = self.add_msgf_available_mod, width = 17, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            msgf_website_message = Message(modFrame, text = 'MS-GF+ website', width = self.dims['title w'], font = self.font)
            msgf_website = Button(modFrame, text = 'MS-GF+ website', command = self.open_msgf_website, width = 17, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            remove_available_mod_message = Message(modFrame, text = "Delete selected modification from 'Available modifications'", width = self.dims['title w'], font = self.font)
            remove_available_mod = Button(modFrame, text = 'Remove modification', command = self.remove_msgf_available_mod, width = 17, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)

            self.msgf_available_mods['yscrollcommand'] = msgf_available_scrollbar.set
            self.msgf_selected_mods['yscrollcommand'] = msgf_selected_scrollbar.set
            self.msgf_available_mods.tag_configure('sel')
            self.msgf_selected_mods.tag_configure('sel')
           
            # self.msgfFrame grid items
            modFrame.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            searchFrame.grid(row = 0, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
             
            # searchFrame grid items
            java_heap_search.grid(row = 0, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.java_heap_search_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            t.grid(row = 2, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.t_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            ti.grid(row = 4, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.ti_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            msgf_thread.grid(row = 6, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.msgf_thread_text.grid(row = 7, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            tda_search.grid(row = 8, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.tda_search_text.grid(row = 9, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            m.grid(row = 10, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.m_text.grid(row = 11, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            inst.grid(row = 12, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.inst_text.grid(row = 13, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            e.grid(row = 14, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.e_text.grid(row = 15, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
            protocol.grid(row = 0, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.protocol_text.grid(row = 1, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            ntt.grid(row = 2, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.ntt_text.grid(row = 3, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            min_length.grid(row = 4, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.min_length_text.grid(row = 5, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            max_length.grid(row = 6, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.max_length_text.grid(row = 7, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            min_charge.grid(row = 8, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.min_charge_text.grid(row = 9, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            max_charge.grid(row = 10, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.max_charge_text.grid(row = 11, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            n.grid(row = 12, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.n_text.grid(row = 13, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            add_features.grid(row = 14, column = 3, rowspan = 1, columnspan = 2, sticky = 'w')
            self.add_features_text.grid(row = 15, column = 4, rowspan = 1, columnspan = 1, sticky = 'w')
            
            Message(searchFrame, text = '', font = self.font).grid(row = 0, column = 2, rowspan = 1, columnspan = 1)
            Message(searchFrame, text = '', font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
            Message(searchFrame, text = '', font = self.font).grid(row = 1, column = 3, rowspan = 1, columnspan = 1)
            
            # modFrame widgets
            msgf_available_mods.grid(row = 0, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.msgf_available_mods.grid(row = 1, column = 1, rowspan = 12, columnspan = 1, sticky = 'w')
            msgf_available_scrollbar.grid(row = 1, column = 2, rowspan = 12, columnspan = 1, sticky = 'nse')
            add_mod.grid(row = 5, column = 4, rowspan = 1, columnspan = 1, sticky = 's')
            remove_mod.grid(row = 6, column = 4, rowspan = 1, columnspan = 1, sticky = 'n')
            msgf_selected_mods.grid(row = 0, column = 5, rowspan = 1, columnspan = 2, sticky = 'w')
            self.msgf_selected_mods.grid(row = 1, column = 6, rowspan = 12, columnspan = 1, sticky = 'w')
            msgf_selected_scrollbar.grid(row = 1, column = 7, rowspan = 12, columnspan = 1, sticky = 'nse')
            input_mod.grid(row = 13, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
            self.input_mod_text.grid(row = 14, column = 1, rowspan = 1, columnspan = 3, sticky = 'w')
            add_available_mod.grid(row = 14, column = 2, rowspan = 1, columnspan = 8, sticky = 'w')
            #remove_available_mod_message.grid(row = 15, column = 0, rowspan = 1, columnspan = 4, sticky = 'w')
            #remove_available_mod.grid(row = 16, column = 1, rowspan = 1, columnspan = 3, sticky = 'w') 
            #msgf_website_message.grid(row = 18, column = 0, rowspan = 1, columnspan = 4, sticky = 'w')
            #msgf_website.grid(row = 19, column = 1, rowspan = 1, columnspan = 3, sticky = 'w')
           
            Message(modFrame, text = '', font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
            Message(modFrame, text = '', font = self.font).grid(row = 5, column = 3, rowspan = 1, columnspan = 1)
            Message(modFrame, text = '', font = self.font).grid(row = 5, column = 5, rowspan = 1, columnspan = 1)
            Message(modFrame, text = '', font = self.font).grid(row = 17, column = 0, rowspan = 1, columnspan = 1)
             
            msgf_search = msgf_search_read(self.config_path + 'msgf_mods.txt')
            available_mods, selected_mods = msgf_mod_read(self.config_path + 'msgf_mods.txt')
              
            self.java_heap_search_text.delete('1.0', 'end')
            self.java_heap_search_text.insert('insert', msgf_search['-Xmx'])  
            self.t_text.delete('1.0', 'end')
            self.t_text.insert('insert', msgf_search['-t'])
            self.ti_text.delete('1.0', 'end')
            self.ti_text.insert('insert', msgf_search['-ti'])
            self.msgf_thread_text.delete('1.0', 'end')
            self.msgf_thread_text.insert('insert', msgf_search['-thread'])
            self.tda_search_text.delete('1.0', 'end')
            self.tda_search_text.insert('insert', msgf_search['-tda'])
            self.m_text.delete('1.0', 'end')
            self.m_text.insert('insert', msgf_search['-m'])
            self.inst_text.delete('1.0', 'end')
            self.inst_text.insert('insert', msgf_search['-inst'])
            self.e_text.delete('1.0', 'end')
            self.e_text.insert('insert', msgf_search['-e'])
            self.add_features_text.delete('1.0', 'end')
            self.add_features_text.insert('insert', msgf_search['-addFeatures'])
            self.protocol_text.delete('1.0', 'end')
            self.protocol_text.insert('insert', msgf_search['-protocol'])
            self.ntt_text.delete('1.0', 'end')
            self.ntt_text.insert('insert', msgf_search['-ntt'])
            self.min_length_text.delete('1.0', 'end')
            self.min_length_text.insert('insert', msgf_search['-minLength'])
            self.max_length_text.delete('1.0', 'end')
            self.max_length_text.insert('insert', msgf_search['-maxLength'])
            self.min_charge_text.delete('1.0', 'end')
            self.min_charge_text.insert('insert', msgf_search['-minCharge'])
            self.max_charge_text.delete('1.0', 'end')
            self.max_charge_text.insert('insert', msgf_search['-maxCharge'])
            self.n_text.delete('1.0', 'end')
            self.n_text.insert('insert', msgf_search['-n'])
            self.msgf_available_mods.delete('1.0', 'end')
            self.msgf_selected_mods.delete('1.0', 'end')
            
            for available_mod in available_mods[:-1]:
                self.msgf_available_mods.insert('insert', available_mod + '\n')
                
            self.msgf_available_mods.insert('insert', available_mods[-1])
            
            self.current_msgf_available_text_position = self.msgf_available_mods.index('insert')
            
            for selected_mod in selected_mods[:-1]:
                self.msgf_selected_mods.insert('insert', selected_mod + '\n')
                
            self.msgf_selected_mods.insert('insert', selected_mods[-1])
        
            self.current_msgf_selected_text_position = self.msgf_selected_mods.index('insert')
        
            self.size_window(self.msgfFrame, self.centered_width_msgf, self.centered_height_msgf, self.x_msgf, self.y_msgf)
            self.msgfFrame.focus()
            
            self.msgfFrame.protocol('WM_DELETE_WINDOW', self.exit_msgf)
        elif self.msgf_flag == True:
            self.msgfFrame.focus()
    
    def gpquest_display(self):
        if self.gpquest_gui_flag == False:
            self.gpquestGUIFrame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 310, width = self.dims['sample w'])
            self.gpquestGUIFrame.grid_propagate(0)
            self.gpquestGUIFrame.title('Set gpquest_params.json configuration file')
            
            self.x_gpquest = (self.width / 2) - (self.dims['sample w'] / 2.0)
            self.y_gpquest = (self.height / 2) - ((self.dims['sample h'] * 2 - 400) / 2.0)
            
            self.centered_height_gpquest = 0.70 * (self.dims['sample h'] * 2 - 120)
            self.centered_width_gpquest = 1.165 * self.centered_height_gpquest * 1.045
            
            self.gpquest_gui_flag = True
            
            pathFrame = LabelFrame(self.gpquestGUIFrame, text = 'GPQuest databases', height = self.dims['sample h'] - 160, width = self.dims['sample w'] - 463, font = self.step_font)
            tolerancesFrame = LabelFrame(self.gpquestGUIFrame, text = 'Tolerances', height = self.dims['sample h'] - 225, width = self.dims['sample w'] - 463, font = self.step_font)
            exit_gpquestGUI_window = Button(self.gpquestGUIFrame, text = 'Close', command = self.exit_gpquest_gui, width = 13, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
            
            pathFrame.grid_propagate(0)
            tolerancesFrame.grid_propagate(0)
            
            # pathFrame widgets
            protein_database_title = Message(pathFrame, text = 'Protein/Peptide database', width = self.dims['title w'], font = self.font)
            self.protein_database_text = Text(pathFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] + 3, padx = 5, pady = 4, font = self.font)
            protein_database_browse = Button(pathFrame, text = 'Browse', command = self.browse_gpquest_protein_database, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            nglycan_database_title = Message(pathFrame, text = 'N-linked Glycan database', width = self.dims['title w'], font = self.font)
            self.nglycan_database_text = Text(pathFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] + 3, padx = 5, pady = 4, font = self.font)
            nglycan_database_browse = Button(pathFrame, text = 'Browse', command = self.browse_gpquest_nglycan_database, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            oglycan_database_title = Message(pathFrame, text = 'O-linked Glycan database', width = self.dims['title w'], font = self.font)
            self.oglycan_database_text = Text(pathFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] + 3, padx = 5, pady = 4, font = self.font)
            oglycan_database_browse = Button(pathFrame, text = 'Browse', command = self.browse_gpquest_oglycan_database, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            
            # tolerancesFrame widgets
            ms1_ppm_title = Message(tolerancesFrame, text = 'MS1 (ppm)', width = self.dims['title w'], font = self.font)
            self.ms1_ppm_text = Text(tolerancesFrame, bd = 2, height = 1, width = self.dims['text w'] + 12, padx = 5, pady = 4, font = self.font)
            ms2_ppm_title = Message(tolerancesFrame, text = 'MS2 (ppm)', width = self.dims['title w'], font = self.font)
            self.ms2_ppm_text = Text(tolerancesFrame, bd = 2, height = 1, width = self.dims['text w'] + 12, padx = 5, pady = 4, font = self.font)
            
            # self.gpquestGUIFrame grid items
            pathFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 2, sticky = 'w')
            tolerancesFrame.grid(row = 2, column = 1, rowspan = 1, columnspan = 2, sticky = 'w')
            exit_gpquestGUI_window.grid(row = 4, column = 2, sticky = 'e')  
            
            Message(self.gpquestGUIFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            Message(self.gpquestGUIFrame, text = '', width = 1, font = self.font).grid(row = 3, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
            
            for i in range(50):
                self.gpquestGUIFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.gpquestGUIFrame.rowconfigure(i, weight = 1)
            
            # pathFrame grid items
            protein_database_title.grid(row = 1, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.protein_database_text.grid(row = 2, column = 2, rowspan = 1, columnspan = 9, sticky = 'ew')
            protein_database_browse.grid(row = 2, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
            nglycan_database_title.grid(row = 3, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.nglycan_database_text.grid(row = 4, column = 2, rowspan = 1, columnspan = 9, sticky = 'ew')  
            nglycan_database_browse.grid(row = 4, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')
            oglycan_database_title.grid(row = 5, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.oglycan_database_text.grid(row = 6, column = 2, rowspan = 1, columnspan = 9, sticky = 'ew')
            oglycan_database_browse.grid(row = 6, column = 11, rowspan = 1, columnspan = 1, sticky = 'w')  
            
            Message(pathFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'w') 
            Message(pathFrame, text = '', width = 1, font = self.font).grid(row = 9, column = 1, rowspan = 1, columnspan = 1, sticky = 'w') 
            
            for i in range(50):
                pathFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                pathFrame.rowconfigure(i, weight = 1)    
                
            # tolerancesFrame grid items   
            ms1_ppm_title.grid(row = 1, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.ms1_ppm_text.grid(row = 2, column = 2, rowspan = 1, columnspan = 9, sticky = 'ew')
            ms2_ppm_title.grid(row = 3, column = 1, rowspan = 1, columnspan = 10, sticky = 'w')
            self.ms2_ppm_text.grid(row = 4, column = 2, rowspan = 1, columnspan = 9, sticky = 'ew')  
            
            Message(tolerancesFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'w') 
            
            for i in range(50):
                tolerancesFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                tolerancesFrame.rowconfigure(i, weight = 1)         
            
            gpquest_protein_path = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"PROTEIN":').replace('"', '')
            gpquest_nglycan_path = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"NGLYCAN":').replace('"', '')
            gpquest_oglycan_path = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"OGLYCAN":').replace('"', '')
            gpquest_ms1_ppm = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"MS1(PPM)":')
            gpquest_ms2_ppm = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"MS2(PPM)":')
            
            self.protein_database_text.config(state = 'normal')
            self.protein_database_text.delete('1.0', 'end')
            self.protein_database_text.insert('insert', gpquest_protein_path)
            self.protein_database_text.config(state = 'disabled')
            self.nglycan_database_text.config(state = 'normal')
            self.nglycan_database_text.delete('1.0', 'end')
            self.nglycan_database_text.insert('insert', gpquest_nglycan_path)
            self.nglycan_database_text.config(state = 'disabled')
            self.oglycan_database_text.config(state = 'normal')
            self.oglycan_database_text.delete('1.0', 'end')
            self.oglycan_database_text.insert('insert', gpquest_oglycan_path)
            self.oglycan_database_text.config(state = 'disabled')
            self.ms1_ppm_text.delete('1.0', 'end')
            self.ms1_ppm_text.insert('insert', gpquest_ms1_ppm)
            self.ms2_ppm_text.delete('1.0', 'end')
            self.ms2_ppm_text.insert('insert', gpquest_ms2_ppm)
            
            self.current_protein_database = self.protein_database_text.get('1.0', 'end').strip()
            self.current_nglycan_database = self.nglycan_database_text.get('1.0', 'end').strip()
            self.current_oglycan_database = self.oglycan_database_text.get('1.0', 'end').strip()
            self.current_ms1_ppm = self.ms1_ppm_text.get('1.0', 'end').strip()
            self.current_ms2_ppm = self.ms2_ppm_text.get('1.0', 'end').strip()
                                                                                        
            self.size_window(self.gpquestGUIFrame, self.centered_width_gpquest, self.centered_height_gpquest, self.x_gpquest, self.y_gpquest)
            self.gpquestGUIFrame.focus()
            
            self.gpquestGUIFrame.protocol('WM_DELETE_WINDOW', self.exit_gpquest_gui)
        elif self.gpquest_gui_flag == True:
            self.gpquestGUIFrame.focus()
    
    def remove_msgf_available_mod(self):
        if self.msgf_available_mods.tag_ranges('sel') != ():
            self.msgf_available_mods.delete('sel.first', 'sel.last')
        else:
            tkMessageBox.showinfo('Error!', 'Please highlight one of the available modifications to be deleted.')
            self.msgfFrame.focus()
    
    def add_msgf_available_mod(self):
        new_mod = self.input_mod_text.get('1.0', 'end').strip()
        
        available_mods = self.msgf_available_mods.get('1.0', 'end').strip().split('\n')
        
        if new_mod != '' and new_mod not in available_mods:
            self.msgf_available_mods.insert('insert', '\n' + new_mod)
        else:
            tkMessageBox.showinfo('Alert!', 'Please enter a properly formatted and unique modification.')
            self.msgfFrame.focus()
    
    def add_msgf_selected_mod(self):
        if self.msgf_available_mods.tag_ranges('sel'):
            if self.msgf_selected_mods.get('1.0', 'end').strip() != '' and self.msgf_available_mods.get('sel.first', 'sel.last').strip() != '':
                self.msgf_selected_mods.insert('insert', '\n' + self.msgf_available_mods.get('sel.first', 'sel.last').strip())
            elif self.msgf_available_mods.get('sel.first', 'sel.last').strip() != '':
                self.msgf_selected_mods.insert('insert', self.msgf_available_mods.get('sel.first', 'sel.last').strip())
            self.msgf_available_mods.delete('sel.first', 'sel.last')
            self.msgf_available_mods.tag_remove('sel', '1.0', 'end')
        else:
            tkMessageBox.showinfo('Error!', 'Please highlight one of the available modifications to be added.')
            self.msgfFrame.focus()
        
    def remove_msgf_selected_mod(self):
        if self.msgf_selected_mods.tag_ranges('sel'):
            if self.msgf_available_mods.get('1.0', 'end').strip() != '' and self.msgf_selected_mods.get('sel.first', 'sel.last').strip() != '':
                self.msgf_available_mods.insert('insert', '\n' + self.msgf_selected_mods.get('sel.first', 'sel.last').strip())
            elif self.msgf_selected_mods.get('sel.first', 'sel.last').strip() != '':
                self.msgf_available_mods.insert('insert', self.msgf_selected_mods.get('sel.first', 'sel.last').strip())
            self.msgf_selected_mods.delete('sel.first', 'sel.last')  
        else:
            tkMessageBox.showinfo('Error!', 'Please highlight one of the selected modifications to be removed.')
            self.msgfFrame.focus()
    
    def myrimatch_display(self):
        if self.myrimatch_flag == False:
            self.myrimatchFrame = Toplevel(self.canvas, height = self.dims['myrimatch h'] * 2 - 310, width = self.dims['myrimatch w'])
            self.myrimatchFrame.grid_propagate(0)
            self.myrimatchFrame.title('Myrimatch parameters')
            
            x_myrimatch = (self.width / 2) - (self.dims['myrimatch w'] / 2.0)
            y_myrimatch = (self.height / 2) - ((self.dims['myrimatch h'] * 2 - 250) / 2.0)
            
            centered_height_myrimatch = 0.95 * (self.dims['myrimatch h'] * 2 - 250)
            centered_width_myrimatch = 2.19 * centered_height_myrimatch
            
            self.size_window(self.myrimatchFrame, centered_width_myrimatch, centered_height_myrimatch, x_myrimatch, y_myrimatch)
            
            self.myrimatch_flag = True
        
    def comet_display(self):
        if self.comet_flag == False:
            self.cometFrame = Toplevel(self.canvas, height = self.dims['comet h'] * 2 - 310, width = self.dims['comet w'])
            self.cometFrame.grid_propagate(0)
            self.cometFrame.title('Comet parameters')
            
            x_comet = (self.width / 2) - (self.dims['comet w'] / 2.0)
            y_comet = (self.height / 2) - ((self.dims['comet h'] * 2 - 250) / 2.0)
            
            centered_height_comet = 0.95 * (self.dims['comet h'] * 2 - 250)
            centered_width_comet = 2.19 * centered_height_comet
            
            self.size_window(self.cometFrame, centered_width_comet, centered_height_comet, x_comet, y_comet)
        
            self.comet_flag = True
        
    def xtandem_display(self):
        if self.xtandem_flag == False:
            self.xtandemFrame = Toplevel(self.canvas, height = self.dims['xtandem h'] * 2 - 310, width = self.dims['xtandem w'])
            self.xtandemFrame.grid_propagate(0)
            self.xtandemFrame.title('X! Tandem parameters')
            
            x_xtandem = (self.width / 2) - (self.dims['xtandem w'] / 2.0)
            y_xtandem = (self.height / 2) - ((self.dims['xtandem h'] * 2 - 250) / 2.0)
            
            centered_height_xtandem = 0.95 * (self.dims['xtandem h'] * 2 - 250)
            centered_width_xtandem = 2.19 * centered_height_xtandem
            
            self.size_window(self.xtandemFrame, centered_width_xtandem, centered_height_xtandem, x_xtandem, y_xtandem)
            
            self.xtandem_flag = True
    
    def sample_display(self): 
        if self.sample_flag == False:
            self.sampleFrame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 310, width = self.dims['sample w'])
            self.sampleFrame.grid_propagate(0)
            self.sampleFrame.title('Build sample_filenames.txt configuration file')
            
            self.x_sample = (self.width / 2) - (self.dims['sample w'] / 2.0)
            self.y_sample = (self.height / 2) - ((self.dims['sample h'] * 2 - 250) / 2.0)
            
            self.centered_height_sample = 1.15 * (self.dims['sample h'] * 2 - 200)
            self.centered_width_sample = 1.63 * self.centered_height_sample
            
            self.current_raw_data = ''
            self.current_mzml_data = ''
            self.sample_flag = True
            
            pathFrame = Frame(self.sampleFrame, height = self.dims['sample h'] - 305, width = self.dims['sample w'])
            namesFrame = LabelFrame(self.sampleFrame, text = 'Select filename(s) unique to each set of sample(s)', height = self.dims['sample h'], width = self.dims['sample w'], font = self.step_font)
            export_sample = Button(self.sampleFrame, text = 'Write config', command = self.write_sample, width = 16, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            set_sample = Button(self.sampleFrame, text = 'View config', command = self.open_sample, width = 16, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            exit_sample_window = Button(self.sampleFrame, text = 'Close', command = self.exit_sample, width = 16, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
            
            pathFrame.grid_propagate(0)
            namesFrame.grid_propagate(0)
            
            # pathFrame widgets
            raw_path_sample_title = Message(pathFrame, text = 'Directory of the raw data files', width = self.dims['title w'], font = self.font)
            self.raw_path_sample_text = Text(pathFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] * 3 - 28, padx = 5, pady = 4, font = self.font)
            raw_path_sample_browse = Button(pathFrame, text = 'Browse', command = self.browse_raw_files_sample, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
            
            # namesFrame widgets
            sample_label = Label(namesFrame, text = 'Highlight set name\nand click', font = self.font)
            self.sample_options = Text(namesFrame, bd = 2, height = self.dims['sample text h'], width = self.dims['sample text w'], font = self.font)
            self.sample_choices = Text(namesFrame, bd = 2, height = int(self.dims['sample text h'] / 2) + 1, width = self.dims['sample text w'], font = self.font)
            scrollbar_options = Scrollbar(namesFrame, command = self.sample_options.yview, takefocus = 0)
            scrollbar_choices = Scrollbar(namesFrame, command = self.sample_choices.yview, takefocus = 0)
            add_sample_name = Button(namesFrame, text = 'Add set name', command = self.add_sample, width = 16, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            clear_names = Button(namesFrame, text = 'Clear names', command = self.clear_sample, width = 16, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
            guess_sample = Button(namesFrame, text = 'Guess names', command = self.guess_sample, width = 16, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
        
            self.sample_options['yscrollcommand'] = scrollbar_options.set
            self.sample_choices['yscrollcommand'] = scrollbar_choices.set
            
            # self.sampleFrame grid items
            pathFrame.grid(row = 1, column = 0, rowspan = 1, columnspan = 2)  
            namesFrame.grid(row = 3, column = 1, rowspan = 1, columnspan = 1)
            export_sample.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'se')
            set_sample.grid(row = 6, column = 1, rowspan = 1, columnspan = 1, sticky = 'ne')
            exit_sample_window.grid(row = 8, column = 1, rowspan = 1, columnspan = 1, sticky = 'e')   
            
            Message(self.sampleFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1)
            Message(self.sampleFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1)
            Message(self.sampleFrame, text = '', width = 1, font = self.font).grid(row = 4, column = 0, rowspan = 1, columnspan = 1)
            Message(self.sampleFrame, text = '', width = 1, font = self.font).grid(row = 7, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(50):
                self.sampleFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.sampleFrame.rowconfigure(i, weight = 1)
            
            # pathFrame grid items  
            raw_path_sample_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 5, sticky = 'nw')
            self.raw_path_sample_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 3, sticky = 'new')
            raw_path_sample_browse.grid(row = 1, column = 4, rowspan = 1, columnspan = 2, sticky = 'nw')      
            
            Message(pathFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(50):
                pathFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                pathFrame.rowconfigure(i, weight = 1)
            
            # namesFrame grid items
            sample_label.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 's')
            self.sample_options.grid(row = 0, column = 3, rowspan = 2, columnspan = 2, sticky = 'w')
            scrollbar_options.grid(row = 0, column = 4, rowspan = 2, columnspan = 1, sticky = 'nse')
            add_sample_name.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'n')
            #guess_sample.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 's')
            self.sample_choices.grid(row = 3, column = 3, rowspan = 3, columnspan = 2, sticky = 'w') 
            scrollbar_choices.grid(row = 3, column = 4, rowspan = 3, columnspan = 1, sticky = 'nse')
            clear_names.grid(row = 4, column = 1, rowspan = 1, columnspan = 1)
            
            Message(namesFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 4, columnspan = 1)
            Message(namesFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 2, rowspan = 4, columnspan = 1)
            Message(namesFrame, text = '- ' * int((124.0 / self.width_base) * self.centered_width) + '-', width = self.width, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 5)
        
            for i in range(50):
                namesFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                namesFrame.rowconfigure(i, weight = 1)
        
            # check search results and mzml folders for files if no raw files, but only check once per change in raw data path/raw data files
            self.sample_try_search_results = True
        
            configs = {}
            configs['options'] = steps(self.config_path + 'steps.txt')
        
            self.raw_path_sample_text.config(state = 'normal')
            self.raw_path_sample_text.delete('1.0', 'end')
            self.raw_path_sample_text.insert('insert', configs['options']['raw_data'])
            self.raw_path_sample_text.config(state = 'disabled')
            
            self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample)
            self.sampleFrame.focus()
            
            self.sampleFrame.protocol('WM_DELETE_WINDOW', self.exit_sample)
        elif self.sample_flag == True:
            self.sampleFrame.focus()
    
    def guess_display(self):
        if self.guess_flag == False:
            self.guessTopFrame = Toplevel(self.canvas, height = self.dims['sample h'] * 2 - 310, width = self.dims['sample w'])
            self.guessTopFrame.grid_propagate(0)
            self.guessTopFrame.title('Guess set names')
            
            self.x_guess = (self.width / 2) - (self.dims['sample w'] / 2.0 - 175)
            self.y_guess = (self.height / 2) - ((self.dims['sample h'] * 2 - 250) / 2.0)
            
            self.centered_height_guess = 0.98 * (self.dims['sample h'] * 2 - 400)
            self.centered_width_guess = 1.56 * self.centered_height_guess
            
            self.guess_flag = True
            self.guessSetFractionVar.set('set')
            
            guessFrame = LabelFrame(self.guessTopFrame, text = 'Guess set names to build sample_filenames.txt configuration file', height = self.dims['sample h'] * 2 - 565, width = self.dims['sample w'] / 2 - 20, font = self.step_font)
            
            guessFrame.grid_propagate(0)
            
            # self.guessTopFrame widgets
            guess_set_names_button = Button(self.guessTopFrame, text = 'Guess', command = self.guess_sets, width = 16, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            set_sample = Button(self.guessTopFrame, text = 'View config', command = self.open_sample, width = 16, bg = 'blue', fg = 'white', activeforeground = 'blue', font = self.font)
            exit_guess_window = Button(self.guessTopFrame, text = 'Close', command = self.exit_guess, width = 16, bg = 'red', fg = 'white', activeforeground = 'red', font = self.font)
            
            # guessFrame widgets
            guess_set_fraction_first_title = Message(guessFrame, text = 'Choose whether the set or fraction identifier comes first in the filename(s)', width = self.dims['title w'], font = self.font)
            guess_set_first = Radiobutton(guessFrame, text = 'Set', variable = self.guessSetFractionVar, value = 'set', activeforeground = 'blue', state = 'normal', font = self.font)
            guess_fraction_first = Radiobutton(guessFrame, text = 'Fraction', variable = self.guessSetFractionVar, value = 'fraction', activeforeground = 'blue', state = 'normal', font = self.font)
            guess_num_sets_title = Message(guessFrame, text = 'Number of unique sets', width = self.dims['title w'], font = self.font)
            self.guess_num_sets_text = Text(guessFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
            
            # self.guessTopFrame grid items
            guessFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 20)
            guess_set_names_button.grid(row = 3, column = 20, rowspan = 1, columnspan = 1, sticky = 'se')
            set_sample.grid(row = 4, column = 20, rowspan = 1, columnspan = 1, sticky = 'ne')
            exit_guess_window.grid(row = 6, column = 20, rowspan = 1, columnspan = 1, sticky = 'se')
            
            Message(self.guessTopFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1)
            Message(self.guessTopFrame, text = '', width = 1, font = self.font).grid(row = 2, column = 0, rowspan = 1, columnspan = 1)
            Message(self.guessTopFrame, text = '', width = 1, font = self.font).grid(row = 5, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(50):
                self.guessTopFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                self.guessTopFrame.rowconfigure(i, weight = 1)
            
            # guessFrame grid items
            guess_set_fraction_first_title.grid(row = 1, column = 1, rowspan = 1, columnspan = 20, sticky = 'sw')
            guess_set_first.grid(row = 2, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
            guess_fraction_first.grid(row = 3, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
            guess_num_sets_title.grid(row = 4, column = 1, rowspan = 1, columnspan = 5, sticky = 'sw')
            self.guess_num_sets_text.grid(row = 5, column = 2, rowspan = 1, columnspan = 10, sticky = 'w')
            
            #Message(guessFrame, text = '', width = 1, font = self.font).grid(row = 0, column = 0, rowspan = 1, columnspan = 1)
            
            for i in range(50):
                guessFrame.columnconfigure(i, weight = 1)
            
            for i in range(50):
                guessFrame.rowconfigure(i, weight = 1)
            
            self.guess_num_sets_text.delete('1.0', 'end')
            self.guess_num_sets_text.insert('insert', '1')
            
            self.size_window(self.guessTopFrame, self.centered_width_guess, self.centered_height_guess, self.x_guess, self.y_guess)
            self.guessTopFrame.focus()
            
            self.guessTopFrame.protocol('WM_DELETE_WINDOW', self.exit_guess)
        elif self.guess_flag == True:
            self.guessTopFrame.focus()
    
    def guess_sets(self):
        # read in the steps.txt config file
        configs = {}
        configs['options'] = steps(self.config_path + 'steps.txt')
        raw_data_path = configs['options']['raw_data']
        raw_data_files = glob.glob(raw_data_path + '*.raw')
        
        raw_file_names = set([])
        for raw_data_f in raw_data_files:
            raw_data_filename = os.path.basename(raw_data_f).replace('.raw', '').strip()
            raw_file_names.add(raw_data_filename)
        
        # check for mzML files if the raw files are missing
        if raw_file_names == set([]):
            mzml_path = raw_data_path + 'step1-mzml\\'
            mzml_files = glob.glob(mzml_path + '*.mzML')
            
            for mzml_f in mzml_files:
                mzml_filename = os.path.basename(mzml_f).replace('.mzML', '').strip()
                raw_file_names.add(mzml_filename)
             
            # check for search result (e.g. mzid) files if the raw and mzML files are missing 
            if raw_file_names == set([]):
                mzid_path = raw_data_path + 'step3-search_results\\'
                mzid_path_files = glob.glob(mzid_path + '*.*')
                
                valid_extensions = ['mzid', 'pepxml', 'xml', 'csv']
                mzid_files = []
                
                for f in mzid_path_files:
                    f_extension = f.split('.')[-1].lower()
                    
                    if f_extension in valid_extensions:
                        mzid_files.append(f)
                
                for index, mzid in enumerate(mzid_files):
                    mzid_filename = os.path.basename(mzid).replace('.' + mzid.split('.')[-1], '')
                    raw_file_names.add(mzid_filename)             
        
        # algorithm for guessing set names
        raw_file_names = list(raw_file_names)
        if raw_file_names != []:
            try:
                raw_file_names.sort(key = natural_keys)
                num_sets = int(self.guess_num_sets_text.get('1.0', 'end').strip())
                set_fraction_first = self.guessSetFractionVar.get()
                
                num_raw_files = len(raw_file_names)
            
                self.guess_set_names = []
                if num_sets == num_raw_files:
                    self.guess_set_names = list(raw_file_names)
                else:
                    samples_per_set = float(num_raw_files) / float(num_sets)
                    if samples_per_set == int(samples_per_set):
                        samples_per_set = int(samples_per_set)
                    else:
                        raise Exception('Guessing the unique set names only works if there are an equal number of .raw files from each set.')
                    
                    self.guess_set_names = set([])
                    for raw_file_name in raw_file_names:
                        if set_fraction_first == 'fraction':
                            raw_file_name = raw_file_name[::-1]
                            
                        set_name = ''
                        finalized_set_name = ''
                        for i, char in enumerate(raw_file_name):
                            set_name += char
                            
                            num_samples_covered = 0
                            for raw_file_name in raw_file_names:
                                if set_name in raw_file_name:
                                    num_samples_covered += 1
                                        
                            if num_samples_covered < samples_per_set and num_samples_covered != 0:
                                break
                            else:
                                finalized_set_name = set_name.strip()
                                
                        if finalized_set_name != '':
                            self.guess_set_names.add(finalized_set_name)
                        
                        if len(self.guess_set_names) == num_sets:
                            break
                            
                    self.guess_set_names = list(self.guess_set_names)
                    num_sets_found = len(self.guess_set_names)
                    
                    if num_sets == num_sets_found:
                        ending_characters = set([])
                        while len(ending_characters) <= 1:
                            ending_characters = set([])
                            for set_name in self.guess_set_names:
                                ending_character = set_name[-1]
                                ending_characters.add(ending_character)
                            
                            if len(ending_characters) == 1:
                                ending_character = list(ending_characters)[0]
                                self.guess_set_names = [set_name[:-1] for set_name in self.guess_set_names]
                                
                                if not ending_character.isalpha() and not ending_character.isdigit():
                                    break
                                
                        if set_fraction_first == 'fraction':
                            for i, set_name in enumerate(self.guess_set_names):
                                self.guess_set_names[i] = set_name[::-1]
                
                self.guess_set_names.sort(key = natural_keys)
                
                set_names_str = ''
                # only displays the first 15 and last 15 guessed set names if there are more than 30 set names
                if len(self.guess_set_names) <= 30:
                    for set_name in self.guess_set_names:
                        set_names_str += ' ' + str(set_name).strip() + '\n'
                else:
                    for set_name in self.guess_set_names[:15]:
                        set_names_str += ' ' + str(set_name).strip() + '\n'
                        
                    set_names_str += ' ' + '...' + '\n'
                    
                    for set_name in self.guess_set_names[-15:]:
                        set_names_str += ' ' + str(set_name).strip() + '\n'
                    
                set_names_str = set_names_str.rstrip()
                    
                accept_set_names = tkMessageBox.askokcancel('Question?','Accept the following set name(s)?\n\n' + set_names_str)
                
                if accept_set_names:
                    self.write_sample_guess()
            except:
                tkMessageBox.showinfo('Alert!', 'The set names could not be guessed for the given parameters/filenames.')
    
        self.guessTopFrame.focus()
    
    def pepid_display(self):
        idFrame = LabelFrame(self.canvas, text = '1 - Peptide identification', height = self.dims['sample h'] + 210, width = self.dims['sample w'], padx = 5, pady = 0, font = self.title_font)
        idFrame.grid_propagate(0)
        
        col1Frame = Frame(idFrame, padx = 5, pady = 0, height = 542, width = self.dims['sample w'] / 2.0 - 2)
        col2Frame = Frame(idFrame, padx = 5, pady = 0, height = 542, width = self.dims['sample w'] / 2.0 - 13)
        
        col1Frame.grid_propagate(0)
        col2Frame.grid_propagate(0)
        
        # col1Frame widgets
        msconvertFrame = LabelFrame(col1Frame, text = 'Step 1 parameters', padx = 5, pady = 0, height = 110, width = self.dims['sample w'] / 2.0 - 7, font = self.step_font)
        ftpFrame = LabelFrame(col1Frame, text = 'Step 2 parameters', padx = 5, pady = 0, height = 428, width = self.dims['sample w'] / 2.0 - 7, font = self.step_font)
        
        msconvertFrame.grid_propagate(0)
        ftpFrame.grid_propagate(0)
        
        # col2Frame widgets
        searchFrame = LabelFrame(col2Frame, text = 'Step 3 parameters', padx = 5, pady = 0, height = 307, width = self.dims['sample w'] / 2.0 - 18, font = self.step_font)
        stepFrame = LabelFrame(col2Frame, text = 'Select steps', padx = 5, pady = 0, height = 231, width = self.dims['sample w'] / 2.0 - 18, font = self.step_font)
        
        searchFrame.grid_propagate(0)
        stepFrame.grid_propagate(0)
        
        # searchFrame widgets
        engineParamsFrame = LabelFrame(searchFrame, text = 'Search engine parameters used in 2 - Protein inference', padx = 5, pady = 0, height = 173, width = self.dims['sample w'] / 2.0 - 33, font = self.step_font)
        engineConfigFrame = LabelFrame(searchFrame, text = 'Search engine configuration files', padx = 5, pady = 0, height = 105, width = self.dims['sample w'] / 2.0 - 33, font = self.step_font)
    
        engineParamsFrame.grid_propagate(0)
        engineConfigFrame.grid_propagate(0)
        
        # ftpFrame widgets
        selectFrame = LabelFrame(ftpFrame, text = 'Select', padx = 5, pady = 0, height = 75, width = self.dims['sample w'] / 2.0 - 20, font = self.step_font)
        downloadFrame = LabelFrame(ftpFrame, text = 'Download', padx = 5, pady = 0, height = 325, width = self.dims['sample w'] / 2.0 - 20, font = self.step_font)
        
        selectFrame.grid_propagate(0)
        downloadFrame.grid_propagate(0)
        
        # msconvertFrame widgets
        msconvert_title = Message(msconvertFrame, text = 'MSConvert settings', width = self.dims['title w'], font =  self.font)
        set_msconvert = Button(msconvertFrame, text = 'MSConvert', command = self.open_msconvert, width = 10, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # selectFrame widgets
        fasta_path_title = Message(selectFrame, text = 'Filepath for the .fasta file to be used', width = self.dims['title w'], font = self.font)
        self.fasta_path_text = Text(selectFrame, bg = '#d3d3d3', bd = 2, height = 1, width = self.dims['text w'] - 2, padx = 5, pady = 4, font = self.font)
        fasta_path_browse = Button(selectFrame, text = 'Browse', command = self.browse_fasta_path, width = 10, height = 1, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # downloadFrame widgets
        ftp_address_title = Message(downloadFrame, text = 'ftp address of the .faa source files', width = self.dims['title w'], font = self.font)
        self.ftp_address_text = Text(downloadFrame, bd = 2, height = 1, width = self.dims['text w'] + 9, padx = 5, pady = 4, font = self.font)
        ftp_user_title = Message(downloadFrame, text = 'ftp username', width = self.dims['title w'], font = self.font)
        self.ftp_user_text = Text(downloadFrame, bd = 2, height = 1, width = self.dims['text w'] + 9, padx = 5, pady = 4, font = self.font)
        ftp_password_title = Message(downloadFrame, text = 'ftp password', width = self.dims['title w'], font = self.font)
        self.ftp_password_text = Text(downloadFrame, bd = 2, height = 1, width = self.dims['text w'] + 9, padx = 5, pady = 4, font = self.font)
        ftp_dir_title = Message(downloadFrame, text = 'ftp subdirectory with the .faa files', width = self.dims['title w'], font = self.font)
        self.ftp_dir_text = Text(downloadFrame, bd = 2, height = 1, width = self.dims['text w'] + 9, padx = 5, pady = 4, font = self.font)
        faa_ext_title = Message(downloadFrame, text = 'Common file extension of the .faa files to be downloaded', width = self.dims['title w'], font = self.font)
        self.faa_ext_text = Text(downloadFrame, bd = 2, height = 1, width = self.dims['text w'] + 9, padx = 5, pady = 4, font = self.font)
        fasta_name_title = Message(downloadFrame, text = 'Specify filename for .fasta file that will be created', width = self.dims['title w'], font = self.font)
        self.fasta_name_text = Text(downloadFrame, bd = 2, height = 1, width = self.dims['text w'] + 9, padx = 5, pady = 4, font = self.font)
        
        # engineParamsFrame widgets
        cleavage_rule_title = Message(engineParamsFrame, text = 'Cleavage rule', width = self.dims['title w'], font = self.font)
        self.cleavage_rule_text = Text(engineParamsFrame, bd = 2, height = 1, width = self.dims['text w'] / 4, padx = 5, pady = 4, font = self.font)
        min_term_title = Message(engineParamsFrame, text = 'Minimum # of cleavage termini', width = self.dims['title w'], font = self.font)
        self.min_term_text = Text(engineParamsFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
        max_mis_title = Message(engineParamsFrame, text = 'Maximum miscleavage', width = self.dims['title w'], font = self.font)
        self.max_mis_text = Text(engineParamsFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
        min_len_title = Message(engineParamsFrame, text = 'Minimum peptide sequence length', width = self.dims['title w'], font = self.font)
        self.min_len_text = Text(engineParamsFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
        max_len_title = Message(engineParamsFrame, text = 'Maximum peptide sequence length', width = self.dims['title w'], font = self.font)
        self.max_len_text = Text(engineParamsFrame, bd = 2, height = 1, width = self.dims['text w'] / 10, padx = 5, pady = 4, font = self.font)
        
        # engineConfigFrame widgets
        msgf_title = Message(engineConfigFrame, text = 'MS-GF+ settings', width = self.dims['title w'], font = self.font)
        msgf_params = Button(engineConfigFrame, text = 'Parameters', command = self.msgf_display, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        set_msgf = Button(engineConfigFrame, text = 'MS-GF+', command = self.open_msgf, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        gpquest_title = Message(engineConfigFrame, text = 'GPQuest settings', width = self.dims['title w'], font = self.font)
        set_gpquest = Button(engineConfigFrame, text = 'GPQuest', command = self.open_gpquest, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        gui_gpquest = Button(engineConfigFrame, text = 'GUI', command = self.gpquest_display, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        myrimatch_title = Message(engineConfigFrame, text = 'Myrimatch settings', width = self.dims['title w'], font = self.font)
        myrimatch_params = Button(engineConfigFrame, text = 'Parameters', command = self.myrimatch_display, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        set_myrimatch = Button(engineConfigFrame, text = 'Myrimatch', command = self.open_myrimatch, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        comet_title = Message(engineConfigFrame, text = 'Comet settings', width = self.dims['title w'], font = self.font)
        comet_params = Button(engineConfigFrame, text = 'Parameters', command = self.comet_display, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        set_comet = Button(engineConfigFrame, text = 'Comet', command = self.open_comet, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        xtandem_title = Message(engineConfigFrame, text = 'X! Tandem settings', width = self.dims['title w'], font = self.font)
        xtandem_params = Button(engineConfigFrame, text = 'Parameters', command = self.xtandem_display, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        set_xtandem = Button(engineConfigFrame, text = 'X! Tandem', command = self.open_xtandem, width = 15, bg = 'black', fg = 'white', activeforeground = 'black', font = self.font)
        
        # stepFrame widgets
        step1 = Checkbutton(stepFrame, text = '( 1 ) MSConvert .raw to .mzML', variable = self.step1Var, onvalue = True, offvalue = False, activeforeground = 'blue', font = self.font)
        step2 = Checkbutton(stepFrame, text = '( 2 ) Choose to specify .fasta path or download from ftp', variable = self.step2Var, onvalue = True, offvalue = False, activeforeground = 'blue', state = 'disabled', disabledforeground = 'black', font = self.font)
        self.step2_select = Radiobutton(stepFrame, text = 'Select', variable = self.fastaVar, value = 'select', activeforeground = 'blue', state = 'normal', font = self.font)
        self.step2_download = Radiobutton(stepFrame, text = 'Download', variable = self.fastaVar, value = 'download', activeforeground = 'blue', state = 'normal', font = self.font)
        step3 = Checkbutton(stepFrame, text = '( 3 ) Peptide identification', variable = self.step3Var, onvalue = True, offvalue = False, activeforeground = 'blue', font = self.font)
        self.step3_msgf = Radiobutton(stepFrame, text = 'MS-GF+  ', variable = self.databaseVar, command = self.write_config_search_engine, value = 'msgf', activeforeground = 'blue', state = 'disabled', font = self.font)
        self.step3_comet = Radiobutton(stepFrame, text = 'Comet', variable = self.databaseVar, command = self.write_config_search_engine, value = 'comet', activeforeground = 'blue', state = 'disabled', font = self.font)
        self.step3_myrimatch = Radiobutton(stepFrame, text = 'Myrimatch', variable = self.databaseVar, command = self.write_config_search_engine, value = 'myrimatch', activeforeground = 'blue', state = 'disabled', font = self.font)
        self.step3_xtandem = Radiobutton(stepFrame, text = 'X! Tandem', variable = self.databaseVar, command = self.write_config_search_engine, value = 'xtandem', activeforeground = 'blue', state = 'disabled', font = self.font)
        stepAll = Button(stepFrame, text = 'Select steps 1 - 3', command = self.all_steps_pepid, bg = 'blue', fg = 'white', activeforeground = 'blue', width = 15, font = self.font)
        stepClear = Button(stepFrame, text = 'Clear steps 1 - 3', command = self.clear_steps_pepid, bg = 'blue', fg = 'white', activeforeground = 'blue', width = 15, font = self.font)
        
        # idFrame grid items
        col1Frame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        col2Frame.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(2):
            idFrame.columnconfigure(i, weight = 1)
            
        for i in range(1):
            idFrame.rowconfigure(i, weight = 1)
        
        # col1Frame grid items
        msconvertFrame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        ftpFrame.grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(1):
            col1Frame.columnconfigure(i, weight = 1)
            
        for i in range(2):
            col1Frame.rowconfigure(i, weight = 1)
        
        # col2Frame grid items
        searchFrame.grid(row = 0, column = 1, rowspan = 1, columnspan = 1, sticky = 'nw')
        stepFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'sw')
        
        for i in range(2):
            col2Frame.columnconfigure(i, weight = 1)
            
        for i in range(2):
            col2Frame.rowconfigure(i, weight = 1)
    
        # msconvertFrame grid items
        msconvert_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 3, sticky = 'w')
        set_msconvert.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(100):
            msconvertFrame.columnconfigure(i, weight = 1)
            
        for i in range(100):
            msconvertFrame.rowconfigure(i, weight = 1)
        
        # ftpFrame grid items
        selectFrame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        downloadFrame.grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(1):
            ftpFrame.columnconfigure(i, weight = 1)
            
        for i in range(2):
            ftpFrame.rowconfigure(i, weight = 1)
        
        # selectFrame grid items
        fasta_path_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 3, sticky = 'nw')
        self.fasta_path_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'new')
        fasta_path_browse.grid(row = 1, column = 2, rowspan = 1, columnspan = 1, sticky = 'nw')
        
        Message(selectFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(100):
            selectFrame.columnconfigure(i, weight = 1)
            
        for i in range(25):
            selectFrame.rowconfigure(i, weight = 1)
        
        # downloadFrame grid items
        ftp_address_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
        self.ftp_address_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        ftp_user_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
        self.ftp_user_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        ftp_password_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
        self.ftp_password_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        ftp_dir_title.grid(row = 6, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
        self.ftp_dir_text.grid(row = 7, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        faa_ext_title.grid(row = 8, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
        self.faa_ext_text.grid(row = 9, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        fasta_name_title.grid(row = 10, column = 0, rowspan = 1, columnspan = 2, sticky = 'w')
        self.fasta_name_text.grid(row = 11, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
       
        Message(downloadFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(100):
            downloadFrame.columnconfigure(i, weight = 1)
            
        for i in range(25):
            downloadFrame.rowconfigure(i, weight = 1)
        
        # searchFrame grid items
        engineParamsFrame.grid(row = 0, column = 0, rowspan = 1, columnspan = 1)
        engineConfigFrame.grid(row = 1, column = 0, rowspan = 1, columnspan = 1)
        
        for i in range(1):
            searchFrame.columnconfigure(i, weight = 1)
            
        for i in range(2):
            searchFrame.rowconfigure(i, weight = 1)
            
        # engineParamsFrame grid items
        cleavage_rule_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        self.cleavage_rule_text.grid(row = 1, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        min_term_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        self.min_term_text.grid(row = 3, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        max_mis_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 50, sticky = 'w')
        self.max_mis_text.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        min_len_title.grid(row = 0, column = 50, rowspan = 1, columnspan = 50, sticky = 'w')
        self.min_len_text.grid(row = 1, column = 51, rowspan = 1, columnspan = 1, sticky = 'w')
        max_len_title.grid(row = 2, column = 50, rowspan = 1, columnspan = 50, sticky = 'w')
        self.max_len_text.grid(row = 3, column = 51, rowspan = 1, columnspan = 1, sticky = 'w') 
        
        Message(engineParamsFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')   
        Message(engineParamsFrame, text = '', width = 1, font = self.font).grid(row = 1, column = 50, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(150):
            engineParamsFrame.columnconfigure(i, weight = 1)
            
        for i in range(25):
            engineParamsFrame.rowconfigure(i, weight = 1)     
            
        # engineConfigFrame grid items
        msgf_title.grid(row = 0, column = 0, rowspan = 1, columnspan = 3, sticky = 'sw')
        #msgf_params.grid(row = 0, column = 3, rowspan = 1, columnspan = 1)
        set_msgf.grid(row = 0, column = 4, rowspan = 1, columnspan = 1, sticky = 'sw')
        gpquest_title.grid(row = 1, column = 0, rowspan = 1, columnspan = 3, sticky = 'nw')
        set_gpquest.grid(row = 1, column = 4, rowspan = 1, columnspan = 1, sticky = 'nw')
        gui_gpquest.grid(row = 1, column = 5, rowspan = 1, columnspan = 1, sticky = 'nw')
        #myrimatch_title.grid(row = 2, column = 0, rowspan = 1, columnspan = 3, sticky = 'w')
        #myrimatch_params.grid(row = 2, column = 3, rowspan = 1, columnspan = 1)
        #set_myrimatch.grid(row = 2, column = 4, rowspan = 1, columnspan = 1)
        #comet_title.grid(row = 3, column = 0, rowspan = 1, columnspan = 3, sticky = 'w')
        #comet_params.grid(row = 3, column = 3, rowspan = 1, columnspan = 1)
        #set_comet.grid(row = 3, column = 4, rowspan = 1, columnspan = 1)
        #xtandem_title.grid(row = 4, column = 0, rowspan = 1, columnspan = 3, sticky = 'w')
        #xtandem_params.grid(row = 4, column = 3, rowspan = 1, columnspan = 1)
        #set_xtandem.grid(row = 4, column = 4, rowspan = 1, columnspan = 1) 
        
        for i in range(250):
            engineConfigFrame.columnconfigure(i, weight = 1)
            
        for i in range(5):
            engineConfigFrame.rowconfigure(i, weight = 1)
        
        # stepFrame grid items
        step1.grid(row = 0, column = 0, rowspan = 1, columnspan = 10, sticky = 'w')
        step2.grid(row = 1, column = 0, rowspan = 1, columnspan = 10, sticky = 'w')
        self.step2_select.grid(row = 2, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        self.step2_download.grid(row = 2, column = 3, rowspan = 1, columnspan = 1, sticky = 'w')
        step3.grid(row = 3, column = 0, rowspan = 1, columnspan = 10, sticky = 'w')
        self.step3_msgf.grid(row = 4, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.step3_myrimatch.grid(row = 4, column = 3, rowspan = 1, columnspan = 1, sticky = 'nw')
        #self.step3_comet.grid(row = 5, column = 1, rowspan = 1, columnspan = 1, sticky = 'w')
        #self.step3_xtandem.grid(row = 5, column = 3, rowspan = 1, columnspan = 1, sticky = 'w')
        stepAll.grid(row = 6, column = 0, rowspan = 1, columnspan = 3, sticky = 'sw')
        stepClear.grid(row = 7, column = 0, rowspan = 1, columnspan = 3, sticky = 'nw')
        
        Message(stepFrame, text = '', width = 1, font = self.font).grid(row = 4, column = 0, rowspan = 1, columnspan = 1, sticky = 'w')
        Message(stepFrame, text = '', width = 1, font = self.font).grid(row = 4, column = 2, rowspan = 1, columnspan = 1, sticky = 'w')
        
        for i in range(500):
            stepFrame.columnconfigure(i, weight = 1)
            
        for i in range(100):
            stepFrame.rowconfigure(i, weight = 1)
        
        idFrame.grid(row = 1, column = 1, rowspan = 1, columnspan = 1)
                
    def set_config(self):
        self.pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
        self.config_path = self.pipeline_path + 'configs\\'
        self.documentation_path = self.pipeline_path + 'documentation\\'
        self.install_path = self.pipeline_path + 'installs\\'
        self.starcluster_config_path = self.pipeline_path + 'configs-starcluster\\starcluster'
        self.starcluster_config_folder = os.path.dirname(self.starcluster_config_path) + '\\'
        self.tmp_path = self.pipeline_path + 'tmp\\'
        self.images_path = self.pipeline_path + 'images\\'
    
    def display_dimensions(self):
        self.dims = {}
       
        self.dims['type h'] = (85.0 / self.height_base) * self.centered_height
        self.dims['type w'] = (150.0 /self.width_base) * self.centered_width
        self.dims['steps h'] = (171.0 / self.height_base) * self.centered_height
        self.dims['steps w'] = (740.0 / self.width_base) * self.centered_width
        self.dims['sample h'] = (330.0 / self.height_base) * self.centered_height
        self.dims['sample w'] = (890.0 / self.width_base) * self.centered_width
        self.dims['msgf h'] = (330.0 / self.height_base) * self.centered_height
        self.dims['msgf w'] = (890.0 / self.width_base) * self.centered_width
        self.dims['myrimatch h'] = (330.0 / self.height_base) * self.centered_height
        self.dims['myrimatch w'] = (890.0 / self.width_base) * self.centered_width
        self.dims['comet h'] = (330.0 / self.height_base) * self.centered_height
        self.dims['comet w'] = (890.0 / self.width_base) * self.centered_width
        self.dims['xtandem h'] = (330.0 / self.height_base) * self.centered_height
        self.dims['xtandem w'] = (890.0 / self.width_base) * self.centered_width
        self.dims['sample text h'] = int((12.0 / self.height_base) * self.centered_height)
        self.dims['sample text w'] = int((100.0 / self.width_base) * self.centered_width)
        
        self.dims['title w'] = (445.0 / self.width_base) * self.centered_width
        self.dims['text w'] = int((47.0 / self.width_base) * self.centered_width)
    
    def size_window(self, frame, width, height, x, y):
        frame.geometry('%dx%d+%d+%d' % (width + 1, height, x, y))
        frame.update_idletasks()
        frame.geometry('%dx%d+%d+%d' % (width, height, x, y))
        frame.update_idletasks()
    
    def initialize_flags(self):
        self.select_ec2_flag = False
        self.select_ebs_flag = False
        self.make_ebs_flag = False
        self.add_aws_flag = False
        self.sample_flag = False
        self.guess_flag = False
        self.msgf_flag = False
        self.gpquest_gui_flag = False
        self.myrimatch_flag = False
        self.comet_flag = False
        self.xtandem_flag = False
    
    def initialize_gui(self):
        options = steps(self.config_path + 'steps.txt')
        starcluster = steps(self.starcluster_config_path)
        inference_search = steps(self.config_path + 'inference_search_params.txt')
        ebs_params = read_new_ebs_config(self.config_path + 'ebs_params.txt')
        msgf_search = msgf_search_read(self.config_path + 'msgf_mods.txt')
        
        self.clear_steps()
        #self.current_raw_path = self.raw_path_text.get('1.0', 'end').strip()
        # sets self.current_raw_path to -1 for purposes of initializing the MS-PyCloud GUI
        # prevents the initialization from overwriting the output folder path
        self.current_raw_path = '-1'
        self.current_archived_path = self.archived_path_text.get('1.0', 'end').strip()
        self.current_fasta_path = self.fasta_path_text.get('1.0', 'end').strip()
        self.current_cleavage_rule = self.cleavage_rule_text.get('1.0', 'end').strip()
        self.current_min_term = self.min_term_text.get('1.0', 'end').strip()
        self.current_max_mis = self.max_mis_text.get('1.0', 'end').strip()
        # sets self.current_fdr equal to -1 for purposes of initializing the MS-PyCloud GUI
        self.current_fdr = '-1'
        #self.current_fdr = self.fdr_text.get('1.0', 'end').strip()
        # sets self.current_ir equal to -1 for purpose of initializing the MS-PyCloud GUI
        self.current_ir = -1
        #self.current_ir = self.ir_text.get('1.0', 'end').strip()
        # sets self.current_morpheus equal to -1 for purpose of initializing the MS-PyCloud GUI
        self.current_morpheus = -1
        #self.current_morpheus = self.morpheus_text.get('1.0', 'end').strip()
        # sets self.current_flr equal to -1 for purposes of initializing the MS-PyCloud GUI
        self.current_flr = '-1'
        #self.current_flr = self.flr_text.get('1.0', 'end').strip()
        self.current_intensity_threshold = self.intensity_threshold_text.get('1.0', 'end').strip()
        self.current_min_len = self.min_len_text.get('1.0', 'end').strip()
        self.current_max_len = self.max_len_text.get('1.0', 'end').strip()
        self.current_max_charge = msgf_search['-maxCharge']
        self.current_min_psm = self.min_psm_text.get('1.0', 'end').strip()
        self.current_min_pep = self.min_pep_text.get('1.0', 'end').strip()
        self.current_ftp_address = self.ftp_address_text.get('1.0', 'end').strip()
        self.current_ftp_user = self.ftp_user_text.get('1.0', 'end').strip()
        self.current_ftp_password = self.ftp_password_text.get('1.0', 'end').strip()
        self.current_ftp_dir = self.ftp_dir_text.get('1.0', 'end').strip()
        self.current_faa_ext = self.faa_ext_text.get('1.0', 'end').strip()
        self.current_fasta_name = self.fasta_name_text.get('1.0', 'end').strip()
        self.current_user_id = self.user_id_text.get('1.0', 'end').strip()
        self.current_cluster_name = self.cluster_name_text.get('1.0', 'end').strip()
        self.current_ec2_key = self.key_choice_text.get('1.0', 'end').strip()
        self.current_volume_choice = self.volume_choice_text.get('1.0', 'end').strip()
        self.step2Var.set(True)
        self.current_s3 = False
        self.current_type = ''
        self.current_label = ''
        self.current_mod = ''
        self.typeVar.set('local')
        self.databaseVar.set(options['search_engine'].lower())
        self.fastaVar.set('select')
        
        # initializes the modification combobox
        initial_mod = options['modification'].lower()
        if initial_mod == 'global':
            self.modVar.set('Global')
        elif initial_mod == 'glyco':
            self.modVar.set('SPEG')
        elif initial_mod == 'intact':
            self.modVar.set('Intact glyco')
        elif initial_mod == 'phospho':
            self.modVar.set('Phospho')
        else:
            # handles odd cases
            self.modVar.set('')
        
        self.localizePhosphositesVar.set(str(options['localize_phosphosites']).lower())
        
        # initializes the reagent label combobox
        initial_reagent_label = options['label_type'].lower()
        if initial_reagent_label == 'free':
            self.labelVar.set('Label free')
        elif initial_reagent_label == '4plex' or initial_reagent_label == '8plex':
            self.labelVar.set('iTRAQ ' + initial_reagent_label)    
        elif initial_reagent_label == '10plex' or initial_reagent_label == '11plex' or initial_reagent_label == '16plex':
            self.labelVar.set('TMT ' + initial_reagent_label)
        else:
            # handles odd cases
            self.labelVar.set('')
        
        # initializes the normalization channel combobox
        initial_norm_channel = options['normalization_channel']
        if initial_norm_channel in itraq_channels:
            self.channelVar.set(initial_norm_channel + ' - iTRAQ')
        elif initial_norm_channel in tmt_channels:
            self.channelVar.set(initial_norm_channel + ' - TMT')
        else:
            # handles odd cases
            self.channelVar.set('')
            
        self.nMetVar.set(str(options['optional_n_term_met_cleavage']).title())
        self.correctionMatrixVar.set(str(options['correction_matrix']).title())
        self.guessSetFractionVar.set('set')
        self.config_procs = []
        
        # 1 - Peptide identification
        self.fasta_path_text.delete('1.0', 'end')
        self.fasta_path_text.insert('insert', options['fasta_path']) 
        self.fasta_path_text.config(state = 'disabled')
        self.ftp_address_text.delete('1.0', 'end')
        self.ftp_address_text.insert('insert', options['ftp_address'])
        self.ftp_user_text.delete('1.0', 'end')
        self.ftp_user_text.insert('insert', options['ftp_user'])
        self.ftp_password_text.delete('1.0', 'end')
        self.ftp_password_text.insert('insert', options['ftp_password'])
        self.ftp_dir_text.delete('1.0', 'end')
        self.ftp_dir_text.insert('insert', options['ftp_dir'])
        self.faa_ext_text.delete('1.0', 'end')
        self.faa_ext_text.insert('insert', options['faa_extension'])
        self.fasta_name_text.delete('1.0', 'end')
        self.fasta_name_text.insert('insert', options['fasta_filename'].split('.fasta')[0])
        self.cleavage_rule_text.delete('1.0', 'end')
        self.cleavage_rule_text.insert('insert', inference_search['cleavage_rule'])
        self.min_len_text.delete('1.0', 'end')
        self.min_len_text.insert('insert', inference_search['min_length'])
        self.max_len_text.delete('1.0', 'end')
        self.max_len_text.insert('insert', inference_search['max_length'])
        self.min_term_text.delete('1.0', 'end')
        self.min_term_text.insert('insert', inference_search['min_cleavage_terminus'])
        self.max_mis_text.delete('1.0', 'end')
        self.max_mis_text.insert('insert', inference_search['max_miscleavage'])
        
        # 2 - Protein inference
        self.min_psm_text.delete('1.0', 'end')
        self.min_psm_text.insert('insert', options['min_psm_per_peptide'])
        self.min_pep_text.delete('1.0', 'end')
        self.min_pep_text.insert('insert', options['min_peptide_per_protein'])
        self.fdr_text.delete('1.0', 'end')
        self.fdr_text.insert('insert', '%.2f' %(float(options['false_discovery_rate'])))
        self.ir_text.delete('1.0', 'end')
        self.ir_text.insert('insert', '%.2f' %(float(options['ir_score'])))
        self.morpheus_text.delete('1.0', 'end')
        self.morpheus_text.insert('insert', '%.2f' %(float(options['morpheus_score'])))
        self.intensity_threshold_text.delete('1.0', 'end')
        self.intensity_threshold_text.insert('insert', options['intensity_threshold'])
        
        # 3 - Quantitation
        self.flr_text.delete('1.0', 'end')
        self.flr_text.insert('insert', '%.2f' %(float(options['false_localization_rate'])))
        
        # 4 - Run pipeline
        self.raw_path_text.delete('1.0', 'end')
        self.raw_path_text.insert('insert', options['raw_data'])
        self.raw_path_text.config(state = 'disabled')
        self.archived_path_text.delete('1.0', 'end')
        self.archived_path_text.insert('insert', options['archived_path'])
        self.archived_path_text.config(state = 'disabled')
        self.user_id_text.delete('1.0', 'end')
        self.user_id_text.insert('insert', options['cluster_user_id'])
        self.cluster_name_text.delete('1.0', 'end')
        self.cluster_name_text.insert('insert', options['cluster_name'])
        self.volume_choice_text.delete('1.0', 'end')
        self.volume_choice_text.insert('insert', ebs_params['volume_choice'])
        self.volume_choice_text.config(state = 'disabled')
        
        f = open(self.starcluster_config_path, 'r')
        lines = f.readlines()
        f.close()
        
        cluster_section_flag = False
        keyname = ''
        for line in lines:
            line = line.strip()
            
            if line == '[cluster mspycloud]':
                cluster_section_flag = True
                
            if cluster_section_flag == True:
                if line.split('=')[0].strip().lower() == 'keyname':
                    keyname = str(line.split('=')[-1]).strip()
                    break
                elif line == '':
                    continue
                elif line[0] == '#':
                    if line.split('#')[1].strip().split('=')[0].strip().lower() == 'keyname':
                        keyname = str(line.split('=')[-1]).strip()
                        break
   
        self.key_choice_text.delete('1.0', 'end')
        self.key_choice_text.insert('insert', keyname)
        self.key_choice_text.config(state = 'disabled')
        
        self.update_display()
    
    def browse_archived_path(self):
        options = steps(self.config_path + 'steps.txt')
        
        self.archived_path = askdirectory(initialdir = options['archived_path']).strip().replace('/', '\\') + '\\'
        
        if self.archived_path != '\\' and self.archived_path != '':
            self.archived_path_text.config(state = 'normal')
            self.archived_path_text.delete('1.0', 'end')
            self.archived_path_text.insert('insert', self.archived_path)
            self.archived_path_text.config(state = 'disabled')
            
            wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
            
            infile = open(self.tmp_path + 'steps.txt', 'r')
            infile_lines = infile.readlines()
            infile.close()
            
            outfile = open(self.config_path + 'steps.txt', 'w')
            for line in infile_lines:
                if line[0:13] == 'archived_path':
                    outfile.write('archived_path = ' + self.archived_path + '\n')
                else:
                    outfile.write(line)
                    
            outfile.close()
            
            os.remove(self.tmp_path + 'steps.txt')
    
    def browse_fasta_path(self):
        options = steps(self.config_path + 'steps.txt')
        
        fasta_path = askopenfilename(initialdir = options['fasta_path']).strip().replace('/', '\\')
        
        if fasta_path != '\\' and fasta_path != '':
            self.fasta_path_text.config(state = 'normal')
            self.fasta_path_text.delete('1.0', 'end')
            self.fasta_path_text.insert('insert', fasta_path)
            self.fasta_path_text.config(state = 'disabled')
            
            wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
            
            infile = open(self.tmp_path + 'steps.txt', 'r')
            infile_lines = infile.readlines()
            infile.close()
            
            outfile = open(self.config_path + 'steps.txt', 'w')
            
            for line in infile_lines:
                if line[0:10] == 'fasta_path':
                    outfile.write('fasta_path = ' + fasta_path + '\n')
                else:
                    outfile.write(line)
                    
            outfile.close()
            
            os.remove(self.tmp_path + 'steps.txt')
            
    def browse_gpquest_protein_database(self):
        key = '"PROTEIN":'
        spacer = 6
        line_end = ','
        
        gpquest_protein_path = os.path.dirname(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')) + '\\'
        
        ask_path = askopenfilename(initialdir = gpquest_protein_path).strip().replace('/', '\\').replace('\\', '\\\\')
        
        if ask_path != '\\' and ask_path != '':
            self.protein_database_text.config(state = 'normal')
            self.protein_database_text.delete('1.0', 'end')
            self.protein_database_text.insert('insert', ask_path)
            self.protein_database_text.config(state = 'disabled')
            
            self.update_gpquest_params_key(key = key, spacer = spacer, val = ask_path, line_end = line_end, quotes = True)
            
        self.gpquestGUIFrame.focus()
        
    def browse_gpquest_nglycan_database(self):
        key = '"NGLYCAN":'
        spacer = 6
        line_end = ','
        
        gpquest_protein_path = os.path.dirname(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')) + '\\'
        
        ask_path = askopenfilename(initialdir = gpquest_protein_path).strip().replace('/', '\\').replace('\\', '\\\\')
        
        if ask_path != '\\' and ask_path != '':
            self.nglycan_database_text.config(state = 'normal')
            self.nglycan_database_text.delete('1.0', 'end')
            self.nglycan_database_text.insert('insert', ask_path)
            self.nglycan_database_text.config(state = 'disabled')
            
            self.update_gpquest_params_key(key = key, spacer = spacer, val = ask_path, line_end = line_end, quotes = True)
            
        self.gpquestGUIFrame.focus()
        
    def browse_gpquest_oglycan_database(self):
        key = '"OGLYCAN":'
        spacer = 6
        line_end = ''
        
        gpquest_protein_path = os.path.dirname(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')) + '\\'
        
        ask_path = askopenfilename(initialdir = gpquest_protein_path).strip().replace('/', '\\').replace('\\', '\\\\')
        
        if ask_path != '\\' and ask_path != '':
            self.oglycan_database_text.config(state = 'normal')
            self.oglycan_database_text.delete('1.0', 'end')
            self.oglycan_database_text.insert('insert', ask_path)
            self.oglycan_database_text.config(state = 'disabled')
            
            self.update_gpquest_params_key(key = key, spacer = spacer, val = ask_path, line_end = line_end, quotes = True)
            
        self.gpquestGUIFrame.focus()
    
    def read_gpquest_config_proteases(self):        
        infile = open(self.config_path + 'gpquest_params.json', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        proteases_section_flag = False 
        proteases = []  
        for line in infile_lines:
            strip_line = line.strip()
            
            if strip_line[:len('"PROTEASES":')] == '"PROTEASES":':
                proteases_section_flag = True
                continue
                
            if strip_line[:len('"MIN_PEP_LEN":')] == '"MIN_PEP_LEN":':
                proteases_section_flag = False
                break
                
            if proteases_section_flag:
                if strip_line != '],':
                    if '"' in line:
                        protease = strip_line.replace(',', '').replace('"', '')
                        proteases.append(protease)
                        
        return proteases
    
    def read_luciphor2_params_key(self, filepath, key):
        infile = open(filepath, 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        val = ''
        for line in infile_lines:
            line = line.strip()
            
            if line[:len(key)] == key:
                # remove any trailing comments
                val = line.split('=')[1].strip().split('#')[0].strip()
                
        if val != '':
            return val
        else:
            raise Exception(str(key) + ' was not found in ' + str(filepath) + '.')
        
    def read_gpquest_params_key(self, filepath, key):
        # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
        infile = open(filepath, 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        val = ''
        for line in infile_lines:
            line = line.strip()
            
            if line[:len(key)] == key:
                val = line.split(key)[1].strip()
                if val[-1] == ',':
                    val = val.split(',')[0].strip()
                       
        if val != '':
            return val
        else:
            raise Exception(str(key) + ' was not found in ' + str(filepath) + '.')
            
    def read_gpquest_params_key_list(self, filepath, key, section_end_key):
        # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
        infile = open(filepath, 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        val = None
        gpquest_section_flag = False
        for line in infile_lines:
            line = line.strip()
            
            if gpquest_section_flag:
                if line[:len(section_end_key)] != section_end_key:
                    if line[-1] == ',':
                        line = line.split(',')[0].strip()
                        
                    if val == '':
                        val += line
                    else:
                        val += ';' + line
                else:
                    break
                    
            if line[:len(key)] == key:
                gpquest_section_flag = True
                val = ''
                continue
                       
        if val != None:
            return val
        else:
            raise Exception(str(key) + ' was not found in ' + str(filepath) + '.')
            
    def update_comet_params_key(self, key, val, comment_spacer, val_list_spacer, change_last_val_only):
        # update comet.params 
        wtc(self.tmp_path + 'comet.params', self.config_path + 'comet.params')
        
        infile = open(self.tmp_path + 'comet.params', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'comet.params', 'w')
        for line in infile_lines:
            current_line = line.strip()
            
            if current_line[:len(key)] == key:
                comment = ''
                if '#' in current_line:
                    comment = '# ' + current_line.split('#')[-1].strip()
                
                if not change_last_val_only:    
                    line = key + ' = ' + str(val) + ' ' * comment_spacer + comment + '\n'
                else:
                    # removes only the last entry in the val if there are multiple entries (i.e. min and max precursor charge state
                    original_vals_str = ''
                    vals_section_flag = False
                    for char in current_line:
                        if char == '=':
                            vals_section_flag = True
                            continue
                        
                        if vals_section_flag:    
                            if char == '#':
                                break
                            else:
                                original_vals_str += char
                                
                    original_vals_str = original_vals_str.strip()
                    # remove the last non-whitespace character
                    updated_vals = original_vals_str.split(val_list_spacer)[:-1]
                    updated_vals.append(val)
                    updated_vals_str = val_list_spacer.join(updated_vals).strip()
                    
                    line = key + ' = ' + str(updated_vals_str) + ' ' * comment_spacer + comment + '\n'
                
            outfile.write(line)
            
        outfile.close()
        
        os.remove(self.tmp_path + 'comet.params')
    
    def update_luciphor2_params_key(self, key, val):
        # update luciphor2_input.txt
        wtc(self.tmp_path + 'luciphor2_input.txt', self.config_path + 'luciphor2_input.txt')
        
        infile = open(self.tmp_path + 'luciphor2_input.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'luciphor2_input.txt', 'w')
        for line in infile_lines:
            current_line = line.strip()
            
            if current_line[:len(key)] == key:
                comment = ''
                if '#' in current_line:
                    comment = '## ' + current_line.split('#')[-1].strip()
                    
                line = key + ' = ' + str(val) + ' ' + comment + '\n'
                
            outfile.write(line)
        
        outfile.close()
        
        os.remove(self.tmp_path + 'luciphor2_input.txt')
    
    def update_gpquest_params_key(self, key, spacer, val, line_end = '', quotes = True):
        # update gpquest_params.json
        # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
        wtc(self.tmp_path + 'gpquest_params.json', self.config_path + 'gpquest_params.json')
        
        infile = open(self.tmp_path + 'gpquest_params.json', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'gpquest_params.json', 'w')
        
        for line in infile_lines:
            current_line = line.strip()
            
            if current_line[:len(key)] == key:
                if quotes:
                    line = ' ' * spacer + str(key) + ' "' + str(val) + '"' + line_end + '\n'
                else:
                    line = ' ' * spacer + str(key) + ' ' + str(val) + line_end + '\n'

            outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'gpquest_params.json')
    
    # similar to update_gpquest_params_key except this function will output in list format rather than a key : value pair
    def update_gpquest_params_key_list(self, key, section_end_key, spacer, section_end_spacer, val, line_end = '', quotes = True):
        # update gpquest_params.json
        # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
        wtc(self.tmp_path + 'gpquest_params.json', self.config_path + 'gpquest_params.json')
        
        infile = open(self.tmp_path + 'gpquest_params.json', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'gpquest_params.json', 'w')
        
        gpquest_section_flag = False
        for line in infile_lines:
            current_line = line.strip()
                
            if gpquest_section_flag:
                if current_line[:len(section_end_key)] == section_end_key:
                    if quotes:
                        line = ' ' * spacer + '"' + str(val) + '"' + line_end + '\n'
                        line += ' ' * section_end_spacer + section_end_key + '\n'
                    else:
                        line = ' ' * spacer + str(val) + line_end + '\n'
                        line += ' ' * section_end_spacer + section_end_key + '\n'
                        
                    gpquest_section_flag = False
                else:
                    continue
                
            if current_line[:len(key)] == key:
                gpquest_section_flag = True

            outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'gpquest_params.json')
    
    def browse_raw_files(self):
        options = steps(self.config_path + 'steps.txt')
        
        self.raw_path = askdirectory(initialdir = options['raw_data']).strip().replace('/', '\\') + '\\'
        
        if self.raw_path != '\\' and self.raw_path != '':
            self.raw_path_text.config(state = 'normal')
            self.raw_path_text.delete('1.0', 'end')
            self.raw_path_text.insert('insert', self.raw_path)
            self.raw_path_text.config(state = 'disabled')
            
            if self.sample_flag == True:
                self.raw_path_sample_text.config(state = 'normal')
                self.raw_path_sample_text.delete('1.0', 'end')
                self.raw_path_sample_text.insert('insert', self.raw_path)
                self.raw_path_sample_text.config(state = 'disabled')
                
            # set the output path to the raw path when the raw path is changed
            self.archived_path = str(self.raw_path)
            
            self.archived_path_text.config(state = 'normal')
            self.archived_path_text.delete('1.0', 'end')
            self.archived_path_text.insert('insert', self.archived_path)
            self.archived_path_text.config(state = 'disabled')
            
            wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
            
            infile = open(self.tmp_path + 'steps.txt', 'r')
            infile_lines = infile.readlines()
            infile.close()
            
            outfile = open(self.config_path + 'steps.txt', 'w')
            for line in infile_lines:
                if line[0:8] == 'raw_data':
                    outfile.write('raw_data = ' + self.raw_path + '\n')
                elif line[0:13] == 'archived_path':
                    outfile.write('archived_path = ' + self.archived_path + '\n')
                else:
                    outfile.write(line)
                    
            outfile.close()
            
            os.remove(self.tmp_path + 'steps.txt')
            
    def browse_raw_files_sample(self):
        options = steps(self.config_path + 'steps.txt')
        
        self.raw_path = askdirectory(initialdir = options['raw_data']).strip().replace('/', '\\') + '\\'
        
        if self.raw_path != '\\' and self.raw_path != '':
            self.raw_path_text.config(state = 'normal')
            self.raw_path_text.delete('1.0', 'end')
            self.raw_path_text.insert('insert', self.raw_path)
            self.raw_path_text.config(state = 'disabled')
            
            self.raw_path_sample_text.config(state = 'normal')
            self.raw_path_sample_text.delete('1.0', 'end')
            self.raw_path_sample_text.insert('insert', self.raw_path)
            self.raw_path_sample_text.config(state = 'disabled')
            self.sampleFrame.focus()
            
            wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
            
            infile = open(self.tmp_path + 'steps.txt', 'r')
            infile_lines = infile.readlines()
            infile.close()
            
            outfile = open(self.config_path + 'steps.txt', 'w')
            
            for line in infile_lines:
                if line[0:8] == 'raw_data':
                    outfile.write('raw_data = ' + self.raw_path + '\n')
                else:
                    outfile.write(line)
                    
            outfile.close()
            
            os.remove(self.tmp_path + 'steps.txt')
    
    def add_sample(self):
        if self.sample_options.tag_ranges('sel') != ():
            if self.sample_choices.get('1.0', 'end').strip() == '':
                self.sample_choices.insert('insert', self.sample_options.get('sel.first', 'sel.last').strip())
            else:
                self.sample_choices.insert('insert', '\n\n' + self.sample_options.get('sel.first', 'sel.last').strip())
                
            #self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample)
        else:
            tkMessageBox.showinfo('Alert!', 'Please highlight part of a sample name.')
            self.sampleFrame.focus() 
    
    def clear_sample(self):
        self.sample_choices.delete('1.0', 'end')
        #self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample) 
    
    def update_config(self, config_file, param_name, param):
        wtc(self.tmp_path + config_file, self.config_path + config_file)
        
        infile = open(self.tmp_path + config_file, 'r')
        infile_lines = infile.readlines()
        infile.close()
            
        outfile = open(self.config_path + config_file, 'w')
        
        for line in infile_lines:
            if line[0:len(param_name)] == param_name:
                comments = line.split('#')[1:]
                
                comment = '\t\t'
                for section in comments:
                    comment += '#' + section
                    
                comment = comment.rstrip()
                
                outfile.write(param_name + ' = ' + str(param) + comment + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + config_file)
        
    def update_sc_basic_config(self, config_file, param_name, param):
        wtc(self.tmp_path + config_file, self.starcluster_config_folder + config_file)
        
        infile = open(self.tmp_path + config_file, 'r')
        infile_lines = infile.readlines()
        infile.close()
            
        outfile = open(self.starcluster_config_folder + config_file, 'w')
        
        for line in infile_lines:
            if line[0:len(param_name)] == param_name:
                comments = line.split('#')[1:]
                
                comment = '\t\t'
                for section in comments:
                    comment += '#' + section
                    
                comment = comment.rstrip()
                
                outfile.write(param_name + ' = ' + str(param) + comment + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + config_file)

    def update_sc_key_config(self, keyname_value):
        f = open(self.starcluster_config_path, 'r')
        lines = f.readlines()
        f.close()
        
        starcluster_output_data = []
        
        cluster_section_flag = False
        for line in lines:
            line = str(line).strip()
            
            if line == '[cluster mspycloud]':
                cluster_section_flag = True
                
            if line == '## Defining Additional Cluster Templates ##':
                cluster_section_flag = False
            
            if cluster_section_flag == True:
                split_line = line.split('=')    
                
                key = split_line[0].strip().lower()
                if key == '':
                    new_line = line
                elif key == 'keyname':
                    new_line = 'KEYNAME = ' + keyname_value  
                elif key[0] == '#':
                    if key.split('#')[1].strip() == 'keyname':
                        new_line = 'KEYNAME = ' + keyname_value
                    else:
                        new_line = line
                else:
                    new_line = line
            else:
                new_line = line
            
            starcluster_output_data.append(new_line + '\n')    
                    
        f = open(self.starcluster_config_path, 'w')
        for starcluster_output_line in starcluster_output_data:
            f.write(starcluster_output_line)
        f.close()
        
    def update_sc_volume_config(self, volume_value):
        f = open(self.starcluster_config_path, 'r')
        lines = f.readlines()
        f.close()
        
        starcluster_output_data = []
        
        cluster_section_flag = False
        for line in lines:
            line = str(line).strip()
            
            if line == '[cluster mspycloud]':
                cluster_section_flag = True
                
            if line == '## Defining Additional Cluster Templates ##':
                cluster_section_flag = False
            
            if cluster_section_flag == True:
                split_line = line.split('=')    
                
                key = split_line[0].strip().lower()
                if key == '':
                    new_line = line
                elif key == 'volumes':
                    new_line = 'VOLUMES = ' + volume_value  
                elif key[0] == '#':
                    if key.split('#')[1].strip() == 'volumes':
                        new_line = 'VOLUMES = ' + volume_value
                    else:
                        new_line = line
                else:
                    new_line = line
            else:
                new_line = line
                
            starcluster_output_data.append(new_line + '\n')
                    
        f = open(self.starcluster_config_path, 'w')
        for starcluster_output_line in starcluster_output_data:
            f.write(starcluster_output_line)
        f.close()
    
    def update_msgf_config(self, param_name, param):
        wtc(self.tmp_path + 'msgf_mods.txt', self.config_path + 'msgf_mods.txt')
        
        infile = open(self.tmp_path + 'msgf_mods.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'msgf_mods.txt', 'w')
        
        for line in infile_lines:
            if line[0:len(param_name)] == param_name:
                outfile.write(param_name + ' ' + str(param) + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'msgf_mods.txt')
    
    def write_config_mod(self, event):
        update_mod = self.modVar.get().strip().lower()
        if update_mod == 'global':
            update_mod = 'global'
        elif update_mod == 'speg':
            update_mod = 'glyco'
        elif update_mod == 'intact glyco':
            update_mod = 'intact'
        elif update_mod == 'phospho':
            update_mod = 'phospho'
        #else:
        #    # handles odd cases
        #    update_mod = update_mod.lower()
        
        wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
        
        infile = open(self.tmp_path + 'steps.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'steps.txt', 'w')
        
        for line in infile_lines:
            if line[0:len('modification')] == 'modification':
                outfile.write('modification = ' + str(update_mod) + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'steps.txt')
        
        self.root.focus()
        
    def write_config_localize_phosphosites(self):
        wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
        
        infile = open(self.tmp_path + 'steps.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'steps.txt', 'w')
        
        for line in infile_lines:
            if line[0:len('localize_phosphosites')] == 'localize_phosphosites':
                outfile.write('localize_phosphosites = ' + str(self.localizePhosphositesVar.get()).lower() + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'steps.txt')
        
    def write_config_label(self, event):
        update_label = self.labelVar.get().split(' ')[-1].strip()
        gpquest_map = {}
        gpquest_map['16plex'] = 'TMT16plex'
        gpquest_map['10plex'] = 'TMT10plex'
        gpquest_map['4plex'] = 'iTRAQ4plex'
        gpquest_map['free'] = 'None'
        # GPQuest cannot currently process iTRAQ8plex or TMT11plex, and the GUI provides a warning message in this case
        gpquest_map['11plex'] = 'TMT11plex'
        gpquest_map['8plex'] = 'iTRAQ8plex'
        
        # update steps.txt
        if update_label in valid_labels:
            wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
            
            infile = open(self.tmp_path + 'steps.txt', 'r')
            infile_lines = infile.readlines()
            infile.close()
            
            outfile = open(self.config_path + 'steps.txt', 'w')
            
            for line in infile_lines:
                if line[0:len('label_type')] == 'label_type':
                    outfile.write('label_type = ' + str(update_label).lower() + '\n')
                else:
                    outfile.write(line)
                    
            outfile.close()
            
            os.remove(self.tmp_path + 'steps.txt')
        
        # update gpquest_params.json
        if update_label in gpquest_map:
            wtc(self.tmp_path + 'gpquest_params.json', self.config_path + 'gpquest_params.json')
            
            infile = open(self.tmp_path + 'gpquest_params.json', 'r')
            infile_lines = infile.readlines()
            infile.close()
            
            outfile = open(self.config_path + 'gpquest_params.json', 'w')
            
            for line in infile_lines:
                current_line = line.strip()
                
                # comma is to make sure modifying the right "LABEL" section since "LABEL" appears twice in the config file
                if current_line[:len('"LABEL":')] == '"LABEL":' and current_line[-1] == ',':
                    line = ' ' * 4 + '"LABEL": "' + gpquest_map[update_label] + '"' + ',\n'

                outfile.write(line)
                    
            outfile.close()
            
            os.remove(self.tmp_path + 'gpquest_params.json')
            
        self.root.focus()
    
    def write_config_channel(self, event):
        wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
        
        infile = open(self.tmp_path + 'steps.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'steps.txt', 'w')
        
        for line in infile_lines:
            if line[0:len('normalization_channel')] == 'normalization_channel':
                outfile.write('normalization_channel = ' + str(self.channelVar.get()).split('-')[0].strip() + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'steps.txt')
        
        self.root.focus()
        
    def write_config_search_engine(self):
        wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
        
        infile = open(self.tmp_path + 'steps.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'steps.txt', 'w')
        
        for line in infile_lines:
            if line[0:len('search_engine')] == 'search_engine':
                outfile.write('search_engine = ' + str(self.databaseVar.get()) + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'steps.txt')
    
    def write_config_n_met_cleavage(self):
        wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
        
        infile = open(self.tmp_path + 'steps.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'steps.txt', 'w')
        
        for line in infile_lines:
            if line[0:len('optional_n_term_met_cleavage')] == 'optional_n_term_met_cleavage':
                outfile.write('optional_n_term_met_cleavage = ' + str(self.nMetVar.get()) + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'steps.txt')
        
    def write_config_correction_matrix(self):
        wtc(self.tmp_path + 'steps.txt', self.config_path + 'steps.txt')
        
        infile = open(self.tmp_path + 'steps.txt', 'r')
        infile_lines = infile.readlines()
        infile.close()
        
        outfile = open(self.config_path + 'steps.txt', 'w')
        
        for line in infile_lines:
            if line[0:len('correction_matrix')] == 'correction_matrix':
                outfile.write('correction_matrix = ' + str(self.correctionMatrixVar.get()) + '\n')
            else:
                outfile.write(line)
                
        outfile.close()
        
        os.remove(self.tmp_path + 'steps.txt')
              
    def write_sample(self):
        names = self.sample_choices.get('1.0', 'end').strip()
        
        if names != '':
            names = names.split('\n\n')
            
            # check to make sure all the names are unique
            unique_names = [names[0]]
            unique = True
            for name in names[1:]:
                for unique_name in unique_names:
                    if name in unique_name:
                        unique = False
                        break
                        
                if not unique:
                    break
                else:
                    unique_names.append(name)    
            
            if unique:
                outfile = open(self.config_path + 'sample_filenames.txt', 'w')
                
                for name in names[:-1]:
                    name = name.strip('')
                    outfile.write(name + '\n\n')
                
                outfile.write(names[-1])    
                outfile.close()    
            else:
                tkMessageBox.showinfo('Alert!', 'The set names you selected are not unique.  Please revise your selections to only include set names that are unique to each sample.')
                self.sampleFrame.focus()
        else:
            #tkMessageBox.showinfo('Alert!', 'At least one sample name needs to be added.')
            
            outfile = open(self.config_path + 'sample_filenames.txt', 'w')
            outfile.write('')  
            outfile.close()    
            
            self.sampleFrame.focus()
            
    def write_sample_guess(self):
        names = self.guess_set_names
        
        if names != []:            
            # check to make sure all the names are unique
            unique_names = [names[0]]
            unique = True
            for name in names[1:]:
                for unique_name in unique_names:
                    if name in unique_name:
                        unique = False
                        break
                        
                if not unique:
                    break
                else:
                    unique_names.append(name)    
            
            if unique:
                outfile = open(self.config_path + 'sample_filenames.txt', 'w')
                
                for name in names[:-1]:
                    name = name.strip('')
                    outfile.write(name + '\n\n')
                
                outfile.write(names[-1])    
                outfile.close()    
            else:
                tkMessageBox.showinfo('Alert!', 'The guessed set names are not unique.')
                self.guessTopFrame.focus()
        else:
            #tkMessageBox.showinfo('Alert!', 'At least one sample name needs to be added.')
            
            outfile = open(self.config_path + 'sample_filenames.txt', 'w')
            outfile.write('')  
            outfile.close()    
            
            self.guessTopFrame.focus()
            
    def guess_sample(self):
        import i0c_guess_sample_filenames as i0c
        
        configs = {}
        configs['options'] = steps(self.config_path + 'steps.txt')
        
        self.clear_sample()
        
        names = i0c.main(configs)
        
        if names != '':
            for name in names:
                if self.sample_choices.get('1.0', 'end').strip() == '':
                    self.sample_choices.insert('insert', name)
                else:
                    self.sample_choices.insert('insert', '\n\n' + name)
        
        self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample) 
        self.sampleFrame.focus() 
        
    def open_steps(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'steps.txt')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'steps.txt')
    
    def open_contams(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'contaminants.fasta')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'contaminants.fasta')
    
    def open_matrix_header(self):
        try:
            p = subprocess.Popen('excel.exe ' + self.config_path + 'header_expression_matrix.tsv')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'header_expression_matrix.tsv')
            
    def open_correction_matrix(self):
        try:
            p = subprocess.Popen('excel.exe ' + self.config_path + 'correction_factors.tsv')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'correction_factors.tsv')
                  
    def open_luciphor2(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'luciphor2_input.txt')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'luciphor2_input.txt')
    
    def open_msgf(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'msgf_mods.txt')
            self.config_procs.append(p)
        except:    
            webbrowser.open(self.config_path + 'msgf_mods.txt')
        
    def open_msgf_website(self):
        webbrowser.open('https://bix-lab.ucsd.edu/pages/viewpage.action?pageId=13533355')
        
    def open_gpquest(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'gpquest_params.json')
            self.config_procs.append(p)
        except:    
            webbrowser.open(self.config_path + 'gpquest_params.json')
        
    def open_myrimatch(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'myrimatch.cfg')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'myrimatch.cfg')
        
    def open_comet(self):
        try:
            p = subprocess.Popen('notepad.exe ' + self.config_path + 'comet.params')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'comet.params')
        
    def open_xtandem(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'msgf_mods.txt')
            self.config_procs.append(p)
        except:    
            webbrowser.open(self.config_path + 'msgf_mods.txt')
        
    def open_msconvert(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'msconvert.txt')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'msconvert.txt')
        
    def open_sample(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'sample_filenames.txt')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'sample_filenames.txt')
        
    def open_starcluster(self):
        try:
            p = subprocess.Popen('write.exe ' + self.starcluster_config_path)
            self.config_procs.append(p)
        except:
            webbrowser.open(self.starcluster_config_path)
        
    def view_ebs_params(self):
        try:
            p = subprocess.Popen('write.exe ' + self.config_path + 'ebs_params.txt')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'ebs_params.txt')
    
    def view_readme(self):
        webbrowser.open(self.documentation_path + 'MS-PyCloud_User_Manual.pdf')
        
    def view_ms_pycloud_version(self):
        try:
            p = subprocess.Popen('notepad.exe ' + self.pipeline_path + 'configs-pycloud_version\\ms-pycloud_version.txt')
            self.config_procs.append(p)
        except:
            webbrowser.open(self.config_path + 'ebs_params.txt')
    
    def all_steps_pepid(self):
        self.step1Var.set(True)
        #self.step2Var.set(True)
        self.step3Var.set(True)
                
    def clear_steps_pepid(self):
        self.step1Var.set(False)
        #self.step2Var.set(False)
        self.step3Var.set(False)
        
    def all_steps_protinf(self):
        self.step4Var.set(True)
        self.step5Var.set(True)
        
    def clear_steps_protinf(self):
        self.step4Var.set(False)
        self.step5Var.set(False)
        
    def all_steps_quant(self):
        self.step6Var.set(True)
        
        if self.labelVar.get().split(' ')[-1].strip() != 'free':
            self.step7Var.set(True)
        
    def clear_steps_quant(self):
        self.step6Var.set(False)
        self.step7Var.set(False)
        
    def all_steps(self):
        #current_modVar = self.modVar.get()
        current_labelVar = self.labelVar.get().split(' ')[-1].strip()
        
        self.step1Var.set(True)
        #self.step2Var.set(True)
        self.step3Var.set(True)
        self.step4Var.set(True)
        self.step5Var.set(True)
        self.step6Var.set(True)
        
        if current_labelVar != 'free':
            self.step7Var.set(True)
                
    def clear_steps(self):
        self.step1Var.set(False)
        #self.step2Var.set(False)
        self.step3Var.set(False)
        self.step4Var.set(False)
        self.step5Var.set(False)
        self.step6Var.set(False)
        self.step7Var.set(False)
        
    def create_new_ebs(self):
        self.update_config('ebs_params.txt', 'volume_name', self.volume_name_text.get('1.0', 'end').strip())
        self.update_config('ebs_params.txt', 'volume_size', self.volume_size_text.get('1.0', 'end').strip())
        self.update_config('ebs_params.txt', 'volume_zone', self.volume_zone_text.get('1.0', 'end').strip())
        self.update_config('ebs_params.txt', 'mount_path', self.volume_mount_path_text.get('1.0', 'end').strip())
        
        import c0a_create_new_ebs as c0a

        configs = {}
        configs['options'] = steps(self.config_path + 'steps.txt')
        configs['exes'] = steps(self.config_path + 'executables.txt')
        configs['ebs_params'] = read_new_ebs_config(self.config_path + 'ebs_params.txt')
        
        c0a.main(configs)
        
        # update selected volume text box in gui
        f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
        lines = f.readlines()
        f.close()
        
        cluster_section_flag = False
        selected_volume = ''
        for line in lines:
            line = str(line).strip()
        
            if line == '[cluster mspycloud]':
                cluster_section_flag = True
                
            if line == '## Defining Additional Cluster Templates ##':
                cluster_section_flag = False
            
            if cluster_section_flag == True:
                split_line = line.split('=')    
                
                key = split_line[0].strip().lower()
                if key == '':
                    continue
                elif key == 'volumes':
                    selected_volume = split_line[-1].split('#')[0].strip()  
                    break
                elif key[0] == '#':
                    if key.split('#')[1].strip() == 'volumes':
                        selected_volume = split_line[-1].split('#')[0].strip()  
                        break
                    
        self.volume_choice_text.config(state = 'normal')
        self.volume_choice_text.delete('1.0', 'end')
        self.volume_choice_text.insert('insert', selected_volume)
        self.volume_choice_text.config(state = 'disabled')
        self.update_config('ebs_params.txt', 'volume_choice', selected_volume)
        
        self.makeEBSFrame.focus()
    
    def create_putty_key(self):
        import c0d_putty_login_master_node as c0d
        
        configs = {}
        configs['options'] = steps(self.config_path + 'steps.txt')
        configs['exes'] = steps(self.config_path + 'executables.txt')
        
        c0d.main(configs)  
    
    def start_cloud(self):
        import c0c_start_cluster as c0c
        
        configs = {}
        configs['options'] = steps(self.config_path + 'steps.txt')
        configs['exes'] = steps(self.config_path + 'executables.txt')
        configs['ebs_params'] = read_new_ebs_config(self.config_path + 'ebs_params.txt')
        
        c0c.main(configs)
        
    def terminate_cloud(self):
        import c0e_terminate_cluster as c0e
        
        configs = {}
        configs['options'] = steps(self.config_path + 'steps.txt')
        
        c0e.main(configs)
            
    def execute(self):
        try:
            execute_modVar = self.modVar.get().lower()
            if execute_modVar == 'global':
                execute_modVar = 'global'
            elif execute_modVar == 'speg':
                execute_modVar = 'glyco'
            elif execute_modVar == 'intact glyco':
                execute_modVar = 'intact'
            elif execute_modVar == 'phospho':
                execute_modVar = 'phospho'
            
            execute_labelVar = self.labelVar.get().split(' ')[-1].strip()
            execute_fastaVar = self.fastaVar.get()
            execute_typeVar = self.typeVar.get()
            execute_databaseVar = self.databaseVar.get()
            execute_correctionMatrixVar = self.correctionMatrixVar.get()
            execute_channelVar = str(self.channelVar.get()).split('-')[0].strip()
            
            execute_cleavage_rule = self.cleavage_rule_text.get('1.0', 'end').strip()
            execute_fasta_path = self.fasta_path_text.get('1.0', 'end').strip()
            
            s1 = self.step1Var.get()
            s2 = self.step2Var.get()
            s3 = self.step3Var.get()
            s4 = self.step4Var.get()
            s5 = self.step5Var.get()
            s6 = self.step6Var.get()
            s7 = self.step7Var.get()
            pipe = self.typeVar.get()
            
            all_steps = [s1, s2, s3, s4, s5, s6, s7]
            sAll = self.all_steps_check(step_values = all_steps)
            
            # sets the fasta_path config file setting based on what user chose
            fasta = execute_fastaVar
            if fasta == 'select':
                self.update_config('steps.txt', 'fasta_path', execute_fasta_path)
                self.update_config('comet.params', 'database_name', execute_fasta_path)
                self.update_config('myrimatch.cfg', 'ProteinDatabase', '"' + execute_fasta_path + '"')
            elif fasta == 'download':
                updated_fasta_name = self.fasta_name_text.get('1.0', 'end').strip()
                # will remove anything that happens to be after the .fasta file extension, just in case
                if '.fasta' in updated_fasta_name:
                    updated_fasta_name = updated_fasta_name.split('.fasta')[0]
                
                configs = {}
                configs['options'] = steps(self.config_path + 'steps.txt')
                configs['exes'] = steps(self.config_path + 'executables.txt')
                configs['backup'] = True
                
                import p2_fasta as p2
                updated_fasta_path = p2.main(configs)
                
                if updated_fasta_path != None:
                    updated_fasta_path += updated_fasta_name + '.fasta'
                    
                    self.update_config('steps.txt', 'fasta_path', updated_fasta_path) 
                    self.update_config('comet.params', 'database_name', updated_fasta_path)
                    self.update_config('myrimatch.cfg', 'ProteinDatabase', '"' + updated_fasta_path + '"')
                    self.fasta_path_text.config(state = 'normal')
                    self.fasta_path_text.delete('1.0', 'end')
                    self.fasta_path_text.insert('insert', updated_fasta_path)  
                    self.fasta_path_text.config(state = 'disabled')
            
            # header_expression_matrix data
            f = open(self.config_path + 'header_expression_matrix.tsv', 'r')
            header_expression_matrix_data = f.readlines()
            f.close()
            
            # correction_factors data
            f = open(self.config_path + 'correction_factors.tsv', 'r')
            correction_factors_data = f.readlines()
            f.close()
            
            # contaminants.fasta data
            f = open(self.config_path + 'contaminants.fasta', 'r')
            contams_lines = f.readlines()
            f.close()
            
            self.configs = {}
            self.configs['options'] = steps(self.config_path + 'steps.txt')
            self.configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
            self.configs['exes'] = steps(self.config_path + 'executables.txt')
            self.configs['myrimatch'] = steps(self.config_path + 'myrimatch.cfg')
            self.configs['comet'] = steps(self.config_path + 'comet.params')
            self.configs['sample filenames'] = rl(self.config_path + 'sample_filenames.txt')
            self.configs['msconvert'] = rl(self.config_path + 'msconvert.txt')
            self.configs['msgf mods'] = rl(self.config_path + 'msgf_mods.txt')
            self.configs['ebs_params'] = read_new_ebs_config(self.config_path + 'ebs_params.txt')
            self.configs['luciphor2'] = rluciphor2(self.config_path + 'luciphor2_input.txt')
            self.configs['node ips'] = rul(self.config_path + 'node_ips.txt')
            self.configs['header_expression_matrix'] = header_expression_matrix_data
            self.configs['correction_factors'] = correction_factors_data
            self.configs['contams_lines'] = contams_lines
            self.configs['backup'] = False
            
            # copies the config files over
            data_config_copy_path = self.configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
            if not os.path.exists(data_config_copy_path):
                shutil.copytree(self.config_path, data_config_copy_path + 'configs\\')
                shutil.copytree(self.pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
                #shutil.copytree(self.pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\')
            
            if (s3 or s4 or s5) and execute_modVar != 'intact':
                if execute_cleavage_rule not in valid_msgf_cleavages:
                    cleavage_string = ''
                    for cleavage in valid_msgf_cleavages[:-1]:
                        cleavage_string += str(cleavage) + ', '
                    cleavage_string += 'or ' + str(valid_msgf_cleavages[-1])
                    
                    tkMessageBox.showinfo('Error!', 'Cleavage rule must be ' + cleavage_string)
                    return
                
            if execute_labelVar not in valid_labels:
                label_string = ''
                for valid_label in valid_labels[:-1]:
                    label_string += str(valid_label) + ', '
                label_string += 'or ' + str(valid_labels[-1])
                
                tkMessageBox.showinfo('Error!', 'Reagent label must be ' + label_string)
                return

            if execute_modVar not in valid_mods:
                mod_string = ''
                for valid_mod in valid_mods[:-1]:
                    mod_string += str(valid_mod) + ', '
                mod_string += 'or ' + str(valid_mods[-1])
                
                tkMessageBox.showinfo('Error!', 'Modification must be ' + mod_string)
                return            
                                    
            if (s3 and execute_modVar != 'intact') or (s4 or s5 or s7):   
                fasta_file = self.configs['options']['fasta_path']
                
                fasta_exists = cfe(fasta_file)
                
                if not fasta_exists:
                    tkMessageBox.showinfo('Error!', '.fasta file not found! Please specify the correct .fasta file path in Step 2')
                    return
            
            if s3 and execute_modVar == 'intact':
                gpquest_protein_filepath = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"PROTEIN":').replace('"', '')
                gpquest_nglycan_filepath = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"NGLYCAN":').replace('"', '')
                gpquest_oglycan_filepath = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', '"OGLYCAN":').replace('"', '')
                
                if not os.path.exists(gpquest_protein_filepath):
                    tkMessageBox.showinfo('Error!', 'GPQuest Protein/Peptide database not found! Please specify the correct Protein/Peptide database in the GPQuest GUI')
                    return
                    
                if not os.path.exists(gpquest_nglycan_filepath):
                    tkMessageBox.showinfo('Error!', 'GPQuest N-linked Glycan database not found! Please specify the correct N-linked Glycan database in the GPQuest GUI')
                    return
                    
                if not os.path.exists(gpquest_oglycan_filepath):
                    tkMessageBox.showinfo('Error!', 'GPQuest O-linked Glycan database not found! Please specify the correct O-linked Glycan database in the GPQuest GUI')
                    return   
                    
                if execute_cleavage_rule not in valid_gpquest_cleavages:
                    cleavage_string = ''
                    if len(valid_gpquest_cleavages) > 1:
                        for cleavage in valid_gpquest_cleavages[:-1]:
                            cleavage_string += str(cleavage) + ', '
                        cleavage_string += 'or ' + str(valid_gpquest_cleavages[-1])
                    else:
                        cleavage_string = str(valid_gpquest_cleavages[0])
                    
                    tkMessageBox.showinfo('Error!', 'Cleavage rule must be ' + cleavage_string + ' for GPQuest processing.')
                    return    
                            
            # currently only MS-GF+ and GPQuest supported on cloud
            if s3:
                if execute_typeVar == 'cloud':
                    if execute_databaseVar != 'msgf':
                        tkMessageBox.showinfo('Error!', 'Currently only MS-GF+ and GPQuest (step 3) are supported in the cloud.')
                        return
            
            if s4:
                if execute_labelVar != '10plex' and execute_labelVar != '11plex' and execute_labelVar != '16plex':
                    if execute_correctionMatrixVar == 'true':
                        tkMessageBox.showinfo('Error!', "Because the 'Reagent label' is not set to 'TMT 10plex', 'TMT 11plex' or 'TMT 16plex', 'Apply correction factors (TMT only)' must be set to 'False' in Step 4")
                        return
                        
            itraq4_channels = ['114', '115', '116', '117']
            itraq8_channels = ['113', '114', '115', '116', '117', '118', '119', '121']
            tmt10_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
            tmt11_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
            tmt16_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
            
            if s7:
                if execute_labelVar == '4plex' and execute_channelVar not in itraq4_channels:
                    tkMessageBox.showinfo('Error!', 'iTRAQ 4plex reference channel must be 114, 115, 116, or 117 in Step 7')
                    return
                elif execute_labelVar == '8plex' and execute_channelVar not in itraq8_channels:
                    tkMessageBox.showinfo('Error!', 'iTRAQ 8plex reference channel must be 113, 114, 115, 116, 117, 118, 119, or 121 in Step 7')
                    return
                elif execute_labelVar == '10plex' and execute_channelVar not in tmt10_channels:
                    tkMessageBox.showinfo('Error!', 'TMT 10plex reference channel must be 126, 127N, 127C, 128N, 128C, 129N, 129C, 130N, 130C, or 131 in Step 7')
                    return
                elif execute_labelVar == '11plex' and execute_channelVar not in tmt11_channels:
                    tkMessageBox.showinfo('Error!', 'TMT 11plex reference channel must be 126, 127N, 127C, 128N, 128C, 129N, 129C, 130N, 130C, 131, or 131C in Step 7')
                    return
                elif execute_labelVar == '16plex' and execute_channelVar not in tmt16_channels:
                    tkMessageBox.showinfo('Error!', 'TMT 16plex reference channel must be 126, 127N, 127C, 128N, 128C, 129N, 129C, 130N, 130C, 131, 131C, 132N, 132C, 133N, 133C, or 134N in Step 7')
                    return
                
            if execute_channelVar == '':
                tkMessageBox.showinfo('Error!', 'Please pick a valid reference channel in Step 7')
                return
            
            #if execute_modVar == 'intact':
            #    label_at_runtime = execute_labelVar
            #    if label_at_runtime == '8plex': #or label_at_runtime == '11plex':
            #        #tkMessageBox.showinfo('Error!', 'GPQuest does not currently support iTRAQ 8plex or TMT 11plex.')
            #        tkMessageBox.showinfo('Error!', 'GPQuest does not currently support iTRAQ 8plex.')
            #        return
            
            # runs quameter on raw files first
            import p0_quameter as p0
            p0.generate_raw_checksums(self.configs)
            p0.idfree_quameter(self.configs)
            
            if sAll:
                if pipe == 'local':
                    import pipeline_local
                    pipeline_local.main(self.configs)
                elif pipe == 'cloud':
                    import pipeline_cloud
                    pipeline_cloud.main(self.configs)
    
            else:
                if s1:
                    import p1_msconvert as p1
                    p1.main(self.configs)
                if s3:
                    if pipe == 'local':
                        if execute_databaseVar == 'msgf':
                            if execute_modVar != 'intact':
                                import p3_msgfplus as p3
                            elif execute_modVar == 'intact':
                                import p3_gpquest as p3
                            
                            p3.main(self.configs)
                        #elif execute_databaseVar == 'myrimatch':
                        #    import p3_myrimatch as p3
                        #    p3.main(self.configs)
                        #elif execute_databaseVar == 'comet':
                        #    import p3_comet as p3
                        #    p3.main(self.configs)
                        #elif execute_databaseVar == 'xtandem':
                        #    import p3_xtandem as p3
                        #    p3.main(self.configs)
                    elif pipe == 'cloud':
                        if execute_databaseVar == 'msgf':
                            if execute_modVar != 'intact':
                                import c3_msgfplus as c3
                            elif execute_modVar == 'intact':
                                import c3_gpquest as c3
                                
                            c3.main(self.configs)
                        #elif execute_databaseVar == 'myrimatch':
                        #    import c3_myrimatch as c3
                        #    c3.main(self.configs)
                        #elif execute_databaseVar == 'comet':
                        #    import c3_comet as c3
                        #    c3.main(self.configs)
                        #elif execute_databaseVar == 'xtandem':
                        #    import c3_xtandem as c3
                        #    c3.main(self.configs)
                if s4:
                    # for running quameter on search results
                    #p0.id_quameter(self.configs)
                    
                    import p4_protein_inference as p4
                    p4.main(self.configs)
                if s5:
                    import p5_decoy_estimation as p5
                    p5.main(self.configs)
                if s6:
                    import p6_peptide_protein_quantitation as p6
                    p6.main(self.configs)
                if s7:
                    import p7_expression_matrix as p7
                    p7.main(self.configs)
                
            print '-' * 150
            print 'Processing is complete!'
            sys.stdout.flush()
            
            print '\nSee pipeline graphical user interface to continue...'
            sys.stdout.flush()

            tkMessageBox.showinfo('Done!', 'Processing is complete!')

            #self.root.focus_force()
            self.root.lift()
        except:
            print '-'
            print 'Error with MS-PyCloud.execute()...'
            e = traceback.format_exc()
            print e
            print '\nPlease run the pipeline again after correcting the parameter which caused the problem.\n' 
            sys.stdout.flush()
            
            try:
                output_path = self.configs['options']['archived_path']
                log_path = output_path + 'logs\\'
                make_path(log_path)
            
                f = open(log_path + 'pipeline_error_log_' + str(time.strftime("%Y%m%d-%H%M%S")) + '.log', 'w')
                f.write('Unexpected error: \n' + str(e))
                f.write('\nAn error occurred while running the pipeline.\n\nPlease run the pipeline again after correcting the parameter which caused the problem.')
                f.close()
            except:
                pass
    
    def exit(self):                
        try:
            for p in self.config_procs:
                p.terminate()
                
            self.root.after_cancel(self.repeat)
            self.root.update_idletasks()
            #self.root.quit()
            self.root.eval('::ttk::CancelRepeat')
            self.root.eval('::ttk::ThemeChanged')
            self.root.destroy()
        except:            
            output_path = self.configs['options']['archived_path']
            log_path = output_path + 'logs\\'
            make_path(log_path)
            
            f = open(log_path + 'pipeline_exit_log_' + str(time.strftime("%Y%m%d-%H%M%S")) + '.log', 'w')
            f.write('Error with MS-PyCloud.exit()...')
            f.write(traceback.format_exc())
            f.write('\nAn error occurred when exiting the pipeline.')
            f.close()
            
            pass    
        
    def shutdown_ttk_repeat(self):
        self.root.after_cancel(self.repeat)
        self.root.update_idletasks()
        self.root.eval('::ttk::CancelRepeat')
        self.root.eval('::ttk::ThemeChanged')
        self.root.destroy()
    
    def exit_add_aws(self):
        try:
            self.add_aws_flag = False
            self.awsFrame.update_idletasks()
            #self.awsFrame.quit()
            self.awsFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_add_aws()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_select_ec2(self):
        try:
            self.select_ec2_flag = False
            self.selectEC2Frame.update_idletasks()
            #self.selectEC2Frame.quit()
            self.selectEC2Frame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_select_ec2()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_select_ebs(self):
        try:
            self.select_ebs_flag = False
            self.selectEBSFrame.update_idletasks()
            #self.selectEBSFrame.quit()
            self.selectEBSFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_select_ebs()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_make_ebs(self):
        try:
            self.make_ebs_flag = False
            self.makeEBSFrame.update_idletasks()
            #self.makeEBSFrame.quit()
            self.makeEBSFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_make_ebs()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
    
    def exit_sample(self):
        try:
            self.sample_flag = False
            self.sample_try_search_results = False
            self.sampleFrame.update_idletasks()
            #self.sampleFrame.quit()
            self.sampleFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_sample()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_guess(self):
        try:
            self.guess_flag = False
            self.guessTopFrame.update_idletasks()
            #self.guessTopFrame.quit()
            self.guessTopFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_guess()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
        
    def exit_msgf(self):
        try:
            self.msgf_flag = False
            self.msgfFrame.update_idletasks()
            #self.msgfFrame.quit()
            self.msgfFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_msgf()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_gpquest_gui(self):
        try:
            self.gpquest_gui_flag = False
            self.gpquestGUIFrame.update_idletasks()
            self.gpquestGUIFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_gpquest_gui()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_myrimatch(self):
        try:
            self.myrimatch_flag = False
            self.myrimatchFrame.update_idletasks()
            #self.myrimatchFrame.quit()
            self.myrimatchFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_myrimatch()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_comet(self):
        try:
            self.comet_flag = False
            self.cometFrame.update_idletasks()
            #self.cometFrame.quit()
            self.cometFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_comet()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def exit_xtandem(self):
        try:
            self.xtandem_flag = False
            self.xtandemFrame.update_idletasks()
            #self.xtandemFrame.quit()
            self.xtandemFrame.destroy()
        except:
            print '-'
            print 'Error with MS-PyCloud.exit_xtandem()...'
            print traceback.format_exc()
            print '-'
            sys.stdout.flush()
            
            pass
            
    def update_display(self):
        try:
            options = steps(self.config_path + 'steps.txt')
            sc_key, sc_keys = sc_key_info(self.pipeline_path + 'configs-starcluster\\starcluster')
            sc_volume, sc_volumes = sc_volume_info(self.pipeline_path + 'configs-starcluster\\starcluster')
            inference_options = steps(self.config_path + 'inference_search_params.txt')
            myrimatch_options = steps(self.config_path + 'myrimatch.cfg')
            comet_options = steps(self.config_path + 'comet.params')
            msgf_options = msgf_search_read(self.config_path + 'msgf_mods.txt')
            ebs_params = steps(self.config_path + 'ebs_params.txt')
            new_raw_path = self.raw_path_text.get('1.0', 'end').strip()
            new_cleavage_rule = self.cleavage_rule_text.get('1.0', 'end').strip()
            new_min_term = self.min_term_text.get('1.0', 'end').strip()
            new_max_mis = self.max_mis_text.get('1.0', 'end').strip()
            new_fdr = self.fdr_text.get('1.0', 'end').strip()
            new_ir = self.ir_text.get('1.0', 'end').strip()
            new_morpheus = self.morpheus_text.get('1.0', 'end').strip()
            new_flr = self.flr_text.get('1.0', 'end').strip()
            new_intensity_threshold = self.intensity_threshold_text.get('1.0', 'end').strip()
            new_min_len = self.min_len_text.get('1.0', 'end').strip()
            new_max_len = self.max_len_text.get('1.0', 'end').strip()
            new_min_psm = self.min_psm_text.get('1.0', 'end').strip()
            new_min_pep = self.min_pep_text.get('1.0', 'end').strip()
            new_fasta_path = self.fasta_path_text.get('1.0', 'end').strip()
            new_ftp_address = self.ftp_address_text.get('1.0', 'end').strip()
            new_ftp_user = self.ftp_user_text.get('1.0', 'end').strip()
            new_ftp_password = self.ftp_password_text.get('1.0', 'end').strip()
            new_ftp_dir = self.ftp_dir_text.get('1.0', 'end').strip()
            new_faa_ext = self.faa_ext_text.get('1.0', 'end').strip()
            new_fasta_name = self.fasta_name_text.get('1.0', 'end').strip()
            new_user_id = self.user_id_text.get('1.0', 'end').strip()
            new_cluster_name = self.cluster_name_text.get('1.0', 'end').strip()
            new_raw_data = glob.glob(new_raw_path + '*.raw')
            new_s3 = self.step3Var.get()
            new_type = self.typeVar.get()   
            new_label = self.labelVar.get().split(' ')[-1].strip()
            new_mod = self.modVar.get().lower()
            
            # update the formatting of self.modVar
            if new_mod == 'global':
                new_mod = 'global'
            elif new_mod == 'speg':
                new_mod = 'glyco'
            elif new_mod == 'intact glyco':
                new_mod = 'intact'
            elif new_mod == 'phospho':
                new_mod = 'phospho'
            
            # raw path
            try:
                config_key = 'raw_data'
                config_val = str(options[config_key]).strip()
                str(config_val)
                    
                if config_val != self.current_raw_path:
                    if self.current_raw_path != '-1':
                        # also updates the output path except when the GUI is initialized
                        self.current_archived_path = config_val
                        
                        self.update_config('steps.txt', 'archived_path', self.current_archived_path)
                        
                    self.current_raw_path = config_val
                    
                    self.raw_path_text.config(state = 'normal')
                    self.raw_path_text.delete('1.0', 'end')
                    self.raw_path_text.insert('insert', self.current_raw_path)
                    self.raw_path_text.config(state = 'disabled')
                    
                    if self.sample_flag:
                        self.raw_path_sample_text.config(state = 'normal')
                        self.raw_path_sample_text.delete('1.0', 'end')
                        self.raw_path_sample_text.insert('insert', self.current_raw_path)
                        self.raw_path_sample_text.config(state = 'disabled')
            except:
                self.update_config('steps.txt', 'raw_data', self.current_raw_path)
                self.update_config('steps.txt', 'archived_path', self.current_archived_path)
            
            # archived path
            try:
                config_key = 'archived_path'
                config_val = str(options[config_key]).strip()
                str(config_val)
                    
                if config_val != self.current_archived_path:
                    self.current_archived_path = config_val
                    
                    self.archived_path_text.config(state = 'normal')
                    self.archived_path_text.delete('1.0', 'end')
                    self.archived_path_text.insert('insert', self.current_archived_path)
                    self.archived_path_text.config(state = 'disabled')
            except:
                self.update_config('steps.txt', 'archived_path', self.current_archived_path)
                
            # fasta path
            if new_fasta_path != self.current_fasta_path:
                try:
                    str(new_fasta_path)
                    self.current_fasta_path = new_fasta_path
                    
                    self.update_config('steps.txt', 'fasta_path', self.current_fasta_path)
                    self.update_config('comet.params', 'database_name', self.current_fasta_path)
                    self.update_config('myrimatch.cfg', 'ProteinDatabase', '"' + self.current_fasta_path + '"')
                except:
                    pass
            else:
                config_key = 'fasta_path'
                comet_key = 'database_name'
                myrimatch_key = 'ProteinDatabase'
                
                try:    
                    config_val = str(options[config_key]).strip()
                    comet_val = str(comet_options[comet_key]).strip()
                    myrimatch_val = str(myrimatch_options[myrimatch_key]).replace('"', '').strip()
                    
                    str(config_val)
                    str(comet_val)
                    str(myrimatch_val)
                        
                    unique_val = None
                    if config_val != comet_val and config_val != myrimatch_val:
                        unique_val = config_val
                    elif comet_val != config_val and comet_val != myrimatch_val:
                        unique_val = comet_val
                    elif myrimatch_val != config_val and myrimatch_val != comet_val:
                        unique_val = myrimatch_val
                    
                    if unique_val != self.current_fasta_path and unique_val != None:
                        self.current_fasta_path = unique_val
                        
                        self.fasta_path_text.config(state = 'normal')
                        self.fasta_path_text.delete('1.0', 'end')
                        self.fasta_path_text.insert('insert', self.current_fasta_path)
                        self.fasta_path_text.config(state = 'disabled')
                        
                        self.update_config('steps.txt', config_key, self.current_fasta_path)
                        self.update_config('comet.params', comet_key, self.current_fasta_path)
                        self.update_config('myrimatch.cfg', myrimatch_key, '"' + self.current_fasta_path + '"')
                except:
                    self.update_config('steps.txt', config_key, self.current_fasta_path)
                    self.update_config('comet.params', comet_key, self.current_fasta_path)
                    self.update_config('myrimatch.cfg', myrimatch_key, '"' + self.current_fasta_path + '"')
            
            # cleavage rule
            spacer = 8
            # must be entire line that ends the section to be edited (leading and trailing white space omitted)
            section_end_key = '],'
            section_end_spacer = 6
            line_end = ''
            
            if new_cleavage_rule != self.current_cleavage_rule:
                try:
                    str(new_cleavage_rule)
                    
                    # gpquest_params.json is updated with all MS-PyCloud valid cleavage rules.  If GPQuest does not support a key
                    # then the user will be warned via a message box from the GUI when attempting to run the pipeline
                    if new_cleavage_rule in valid_pipeline_cleavages:
                        self.current_cleavage_rule = new_cleavage_rule
                    
                        self.update_config('inference_search_params.txt', 'cleavage_rule', self.current_cleavage_rule)
                        self.update_config('myrimatch.cfg', 'CleavageRules', '"' + self.current_cleavage_rule + '"')
                    
                        msgf_cleavage_value = self.determine_msgf_cleavage(self.current_cleavage_rule)
                        self.update_msgf_config('-e', msgf_cleavage_value)
                        
                        comet_cleavage_value = self.determine_comet_cleavage(self.current_cleavage_rule)
                        self.update_config('comet.params', 'search_enzyme_number', comet_cleavage_value) 
                        
                        self.update_gpquest_params_key_list('"PROTEASES":', section_end_key, spacer, section_end_spacer, self.current_cleavage_rule, line_end, quotes = True)       
                except:
                    pass
            else:
                inference_key  = 'cleavage_rule'
                myrimatch_key = 'CleavageRules'
                comet_key = 'search_enzyme_number'
                msgf_key = '-e'
                gpquest_key = '"PROTEASES":'
                
                try:
                    inference_val = str(inference_options[inference_key]).strip()
                    myrimatch_val = str(myrimatch_options[myrimatch_key]).replace('"', '').strip()
                    gpquest_val = str(self.read_gpquest_params_key_list(self.config_path + 'gpquest_params.json', gpquest_key, section_end_key)).replace('"', '')
                    
                    comet_dict_val = str(comet_options[comet_key]).strip()
                    comet_dict = self.comet_cleavage_dict()
                    # fyi - 'Arg-C' is the value for key '5' in the inverse dictionary because it is listed after 'Arg_C' in the dictionary below.  Make a note of this when adding any future parameters to the dictionary
                    inv_comet_dict = {v: k for k, v in comet_dict.iteritems()}
                    comet_val = inv_comet_dict[comet_dict_val]
                    
                    msgf_dict_val = str(msgf_options[msgf_key]).strip()
                    msgf_dict = self.msgf_cleavage_dict()
                    inv_msgf_dict = {v: k for k, v in msgf_dict.iteritems()}
                    msgf_val = inv_msgf_dict[msgf_dict_val]
                    
                    str(inference_val)
                    str(myrimatch_val)
                    str(comet_val)
                    str(msgf_val)
                    str(gpquest_val)
                    
                    unique_val = None
                    if inference_val != myrimatch_val and inference_val != comet_val and inference_val != msgf_val and inference_val != gpquest_val:
                        unique_val = inference_val
                    elif myrimatch_val != inference_val and myrimatch_val != comet_val and myrimatch_val != msgf_val and myrimatch_val != gpquest_val:
                        unique_val = myrimatch_val
                    # skips if comet_val == 'No_enzyme' because this will always be different from other config values
                    elif (comet_val != inference_val and comet_val != myrimatch_val and comet_val != msgf_val and comet_val != gpquest_val) and comet_val != 'No_enzyme':
                            unique_val = comet_val
                    # skips if msgf_val == 'unspecific cleavage' because this will always be different from other config values
                    elif (msgf_val != inference_val and msgf_val != myrimatch_val and msgf_val != comet_val and msgf_val != gpquest_val) and msgf_val != 'unspecific cleavage':
                            unique_val = msgf_val
                    elif gpquest_val != inference_val and gpquest_val != myrimatch_val and gpquest_val != comet_val and gpquest_val != msgf_val:
                        unique_val = gpquest_val  
                        
                    if unique_val != self.current_cleavage_rule and unique_val != None:
                        # gpquest_params.json is updated with all MS-PyCloud valid cleavage rules.  If GPQuest does not support a key
                        # then the user will be warned via a message box from the GUI when attempting to run the pipeline
                        if unique_val in valid_pipeline_cleavages:
                            self.current_cleavage_rule = unique_val
                            
                            self.cleavage_rule_text.delete('1.0', 'end')
                            self.cleavage_rule_text.insert('insert', self.current_cleavage_rule)
                            
                            self.update_config('inference_search_params.txt', inference_key, self.current_cleavage_rule)
                            self.update_config('myrimatch.cfg', myrimatch_key, '"' + self.current_cleavage_rule + '"')
                            
                            msgf_cleavage_value = self.determine_msgf_cleavage(self.current_cleavage_rule)
                            self.update_msgf_config(msgf_key, msgf_cleavage_value)
                            
                            comet_cleavage_value = self.determine_comet_cleavage(self.current_cleavage_rule)
                            self.update_config('comet.params', comet_key, comet_cleavage_value)
                            
                            self.update_gpquest_params_key_list(gpquest_key, section_end_key, spacer, section_end_spacer, self.current_cleavage_rule, line_end, quotes = True)
                        else:
                            raise Exception('')    
                except:
                    # gpquest_params.json is updated with all MS-PyCloud valid cleavage rules.  If GPQuest does not support a key
                    # then the user will be warned via a message box from the GUI when attempting to run the pipeline
                    if self.current_cleavage_rule in valid_pipeline_cleavages:
                        self.update_config('inference_search_params.txt', inference_key, self.current_cleavage_rule)
                        self.update_config('myrimatch.cfg', myrimatch_key, '"' + self.current_cleavage_rule + '"')
                        
                        msgf_cleavage_value = self.determine_msgf_cleavage(self.current_cleavage_rule)
                        self.update_msgf_config(msgf_key, msgf_cleavage_value)        
                        
                        comet_cleavage_value = self.determine_comet_cleavage(self.current_cleavage_rule)
                        self.update_config('comet.params', comet_key, comet_cleavage_value)
                        
                        self.update_gpquest_params_key_list(gpquest_key, section_end_key, spacer, section_end_spacer, self.current_cleavage_rule, line_end, quotes = True)
                            
            # min # cleavage termini
            if new_min_term != self.current_min_term:
                try:
                    int(new_min_term)
                    self.current_min_term = new_min_term
                    
                    self.update_config('inference_search_params.txt', 'min_cleavage_terminus', self.current_min_term)
                    self.update_config('myrimatch.cfg', 'MinTerminiCleavages', self.current_min_term)
                    self.update_config('comet.params', 'num_enzyme_termini', self.current_min_term)
                    self.update_msgf_config('-ntt', self.current_min_term)
                except:
                    pass
            else:
                inference_key  = 'min_cleavage_terminus'
                myrimatch_key = 'MinTerminiCleavages'
                comet_key = 'num_enzyme_termini'
                msgf_key = '-ntt'
                
                try:
                    inference_val = str(inference_options[inference_key]).strip()
                    myrimatch_val = str(myrimatch_options[myrimatch_key]).strip()
                    comet_val = str(comet_options[comet_key]).strip()
                    msgf_val = str(msgf_options[msgf_key]).strip()
                    int(inference_val)
                    int(myrimatch_val)
                    int(comet_val)
                    int(msgf_val)
                    
                    unique_val = None
                    if inference_val != myrimatch_val and inference_val != comet_val and inference_val != msgf_val:
                        unique_val = inference_val
                    elif myrimatch_val != inference_val and myrimatch_val != comet_val and myrimatch_val != msgf_val:
                        unique_val = myrimatch_val
                    elif comet_val != inference_val and comet_val != myrimatch_val and comet_val != msgf_val:
                        unique_val = comet_val
                    elif msgf_val != inference_val and msgf_val != myrimatch_val and msgf_val != comet_val:
                        unique_val = msgf_val
                        
                    if unique_val != self.current_min_term and unique_val != None:
                        self.current_min_term = unique_val
                        
                        self.min_term_text.delete('1.0', 'end')
                        self.min_term_text.insert('insert', self.current_min_term)
                        
                        self.update_config('inference_search_params.txt', inference_key, self.current_min_term)
                        self.update_config('myrimatch.cfg', myrimatch_key, self.current_min_term)
                        self.update_config('comet.params', comet_key, self.current_min_term)
                        self.update_msgf_config(msgf_key, self.current_min_term)
                except:
                    self.update_config('inference_search_params.txt', inference_key, self.current_min_term)
                    self.update_config('myrimatch.cfg', myrimatch_key, self.current_min_term)
                    self.update_config('comet.params', comet_key, self.current_min_term)
                    self.update_msgf_config(msgf_key, self.current_min_term)
                    
            # max miscleavage
            spacer = 6
            line_end = ''
                
            if new_max_mis != self.current_max_mis:
                try:
                    int(new_max_mis)
                    self.current_max_mis = new_max_mis
                    
                    self.update_config('inference_search_params.txt', 'max_miscleavage', self.current_max_mis)
                    self.update_config('myrimatch.cfg', 'MaxMissedCleavages', self.current_max_mis)
                    self.update_config('comet.params', 'allowed_missed_cleavage', self.current_max_mis)
                    self.update_gpquest_params_key('"MISSED_CLEAVAGES":', spacer, self.current_max_mis, line_end, quotes = False)
                except:
                    pass
            else:
                inference_key  = 'max_miscleavage'
                myrimatch_key = 'MaxMissedCleavages'
                comet_key = 'allowed_missed_cleavage'
                gpquest_key = '"MISSED_CLEAVAGES":'
                
                try:
                    inference_val = str(inference_options[inference_key]).strip()
                    myrimatch_val = str(myrimatch_options[myrimatch_key]).strip()
                    comet_val = str(comet_options[comet_key]).strip()
                    gpquest_val = str(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', gpquest_key))
                    int(inference_val)
                    int(myrimatch_val)
                    int(comet_val)
                    int(gpquest_val)
                    
                    unique_val = None
                    if inference_val != myrimatch_val and inference_val != comet_val and inference_val != gpquest_val:
                        unique_val = inference_val
                    elif myrimatch_val != inference_val and myrimatch_val != comet_val and myrimatch_val != gpquest_val:
                        unique_val = myrimatch_val
                    elif comet_val != inference_val and comet_val != myrimatch_val and comet_val != gpquest_val:
                        unique_val = comet_val
                    elif gpquest_val != inference_val and gpquest_val != myrimatch_val and gpquest_val != comet_val:
                        unique_val = gpquest_val
                        
                    if unique_val != self.current_max_mis and unique_val != None:
                        self.current_max_mis = unique_val
                        
                        self.max_mis_text.delete('1.0', 'end')
                        self.max_mis_text.insert('insert', self.current_max_mis)
                        
                        self.update_config('inference_search_params.txt', inference_key, self.current_max_mis)
                        self.update_config('myrimatch.cfg', myrimatch_key, self.current_max_mis)
                        self.update_config('comet.params', comet_key, self.current_max_mis)
                        self.update_gpquest_params_key(gpquest_key, spacer, self.current_max_mis, line_end, quotes = False)
                except:
                    self.update_config('inference_search_params.txt', inference_key, self.current_max_mis)
                    self.update_config('myrimatch.cfg', myrimatch_key, self.current_max_mis)
                    self.update_config('comet.params', comet_key, self.current_max_mis)
                    self.update_gpquest_params_key(gpquest_key, spacer, self.current_max_mis, line_end, quotes = False)
                    
            # min peptide length
            spacer = 6
            line_end = ','
            
            if new_min_len != self.current_min_len:
                try:
                    int(new_min_len)
                    self.current_min_len = new_min_len
                    
                    self.update_config('inference_search_params.txt', 'min_length', self.current_min_len)
                    self.update_config('myrimatch.cfg', 'MinPeptideLength', self.current_min_len)
                    self.update_msgf_config('-minLength', self.current_min_len)
                    self.update_gpquest_params_key('"MIN_PEP_LEN":', spacer, self.current_min_len, line_end, quotes = False)
                except:
                    pass
            else:
                inference_key  = 'min_length'
                myrimatch_key = 'MinPeptideLength'
                msgf_key = '-minLength'
                gpquest_key = '"MIN_PEP_LEN":'
                
                try:
                    inference_val = str(inference_options[inference_key]).strip()
                    myrimatch_val = str(myrimatch_options[myrimatch_key]).strip()
                    msgf_val = str(msgf_options[msgf_key]).strip()
                    gpquest_val = str(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', gpquest_key))
                    int(inference_val)
                    int(myrimatch_val)
                    int(msgf_val)
                    int(gpquest_val)
                    
                    unique_val = None
                    if inference_val != myrimatch_val and inference_val != msgf_val and inference_val != gpquest_val:
                        unique_val = inference_val
                    elif myrimatch_val != inference_val and myrimatch_val != msgf_val and myrimatch_val != gpquest_val:
                        unique_val = myrimatch_val
                    elif msgf_val != inference_val and msgf_val != myrimatch_val and msgf_val != gpquest_val:
                        unique_val = msgf_val
                    elif gpquest_val != inference_val and gpquest_val != myrimatch_val and gpquest_val != msgf_val:
                        unique_val = gpquest_val
                        
                    if unique_val != self.current_min_len and unique_val != None:
                        self.current_min_len = unique_val
                        
                        self.min_len_text.delete('1.0', 'end')
                        self.min_len_text.insert('insert', self.current_min_len)
                        
                        self.update_config('inference_search_params.txt', inference_key, self.current_min_len)
                        self.update_config('myrimatch.cfg', myrimatch_key, self.current_min_len)
                        self.update_msgf_config(msgf_key, self.current_min_len)
                        self.update_gpquest_params_key(gpquest_key, spacer, self.current_min_len, line_end, quotes = False)
                except:
                    self.update_config('inference_search_params.txt', inference_key, self.current_min_len)
                    self.update_config('myrimatch.cfg', myrimatch_key, self.current_min_len)
                    self.update_msgf_config(msgf_key, self.current_min_len)
                    self.update_gpquest_params_key(gpquest_key, spacer, self.current_min_len, line_end, quotes = False)
                    
            # max peptide length
            spacer = 6
            line_end = ','
            
            if new_max_len != self.current_max_len:
                try:
                    int(new_max_len)
                    self.current_max_len = new_max_len
                    
                    self.update_config('inference_search_params.txt', 'max_length', self.current_max_len)
                    self.update_config('myrimatch.cfg', 'MaxPeptideLength', self.current_max_len)
                    self.update_msgf_config('-maxLength', self.current_max_len)
                    self.update_gpquest_params_key('"MAX_PEP_LEN":', spacer, self.current_max_len, line_end, quotes = False)
                    self.update_luciphor2_params_key('MAX_PEP_LEN', self.current_max_len)
                except:
                    pass
            else:
                inference_key  = 'max_length'
                myrimatch_key = 'MaxPeptideLength'
                msgf_key = '-maxLength'
                gpquest_key = '"MAX_PEP_LEN":'
                luciphor2_key = 'MAX_PEP_LEN'
                
                try:
                    inference_val = str(inference_options[inference_key]).strip()
                    myrimatch_val = str(myrimatch_options[myrimatch_key]).strip()
                    msgf_val = str(msgf_options[msgf_key]).strip()
                    gpquest_val = str(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', gpquest_key))
                    luciphor2_val = str(self.read_luciphor2_params_key(self.config_path + 'luciphor2_input.txt', luciphor2_key))
                    int(inference_val)
                    int(myrimatch_val)
                    int(msgf_val)
                    int(gpquest_val)
                    int(luciphor2_val)
                    
                    unique_val = None
                    if inference_val != myrimatch_val and inference_val != msgf_val and inference_val != gpquest_val and inference_val != luciphor2_val:
                        unique_val = inference_val
                    elif myrimatch_val != inference_val and myrimatch_val != msgf_val and myrimatch_val != gpquest_val and myrimatch_val != luciphor2_val:
                        unique_val = myrimatch_val
                    elif msgf_val != inference_val and msgf_val != myrimatch_val and msgf_val != gpquest_val and msgf_val != luciphor2_val:
                        unique_val = msgf_val
                    elif gpquest_val != inference_val and gpquest_val != myrimatch_val and gpquest_val != msgf_val and gpquest_val != luciphor2_val:
                        unique_val = gpquest_val
                    elif luciphor2_val != inference_val and luciphor2_val != myrimatch_val and luciphor2_val != msgf_val and luciphor2_val != gpquest_val:
                        unique_val = luciphor2_val
                        
                    if unique_val != self.current_max_len and unique_val != None:
                        self.current_max_len = unique_val
                        
                        self.max_len_text.delete('1.0', 'end')
                        self.max_len_text.insert('insert', self.current_max_len)
                        
                        self.update_config('inference_search_params.txt', inference_key, self.current_max_len)
                        self.update_config('myrimatch.cfg', myrimatch_key, self.current_max_len)
                        self.update_msgf_config(msgf_key, self.current_max_len)
                        self.update_gpquest_params_key(gpquest_key, spacer, self.current_max_len, line_end, quotes = False)
                        self.update_luciphor2_params_key(luciphor2_key, self.current_max_len)
                except:
                    self.update_config('inference_search_params.txt', inference_key, self.current_max_len)
                    self.update_config('myrimatch.cfg', myrimatch_key, self.current_max_len)
                    self.update_msgf_config(msgf_key, self.current_max_len)
                    self.update_gpquest_params_key(gpquest_key, spacer, self.current_max_len, line_end, quotes = False)
                    self.update_luciphor2_params_key(luciphor2_key, self.current_max_len)
            
            # max charge state
            spacer = 6
            line_end = ','
            
            comet1_key = 'precursor_charge'
            comet2_key = 'max_precursor_charge'
            msgf_key = '-maxCharge'
            # currently the GPQuest key is unique to the '"CHARGE": {' subsection
            gpquest_key = '"max":'
            luciphor2_key = 'MAX_CHARGE_STATE'
            
            try:
                # precursor_charge key in the comet config file contains both the min and max charge precursor charge states
                comet1_val = str(comet_options[comet1_key]).strip().split(' ')[-1].strip()
                comet2_val = str(comet_options[comet2_key]).strip() 
                msgf_val = str(msgf_options[msgf_key]).strip()
                gpquest_val = str(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', gpquest_key))
                luciphor2_val = str(self.read_luciphor2_params_key(self.config_path + 'luciphor2_input.txt', luciphor2_key))
                int(comet1_val)
                int(comet2_val)
                int(msgf_val)
                int(gpquest_val)
                int(luciphor2_val)
                
                unique_val = None
                if comet1_val != comet2_val and comet1_val != msgf_val and comet1_val != gpquest_val and comet1_val != luciphor2_val:
                    unique_val = comet1_val
                elif comet2_val != comet1_val and comet2_val != msgf_val and comet2_val != gpquest_val and comet2_val != luciphor2_val:
                    unique_val = comet2_val
                elif msgf_val != comet1_val and msgf_val != comet2_val and msgf_val != gpquest_val and msgf_val != luciphor2_val:
                    unique_val = msgf_val
                elif gpquest_val != comet1_val and gpquest_val != comet2_val and gpquest_val != msgf_val and gpquest_val != luciphor2_val:
                    unique_val = gpquest_val
                elif luciphor2_val != comet1_val and luciphor2_val != comet2_val and luciphor2_val != msgf_val and luciphor2_val != gpquest_val:
                    unique_val = luciphor2_val
                    
                if unique_val != self.current_max_charge and unique_val != None:
                    self.current_max_charge = unique_val
                    
                    # comment spacer is fixed at 17 because the maximum precursor charge state is a single digit number
                    # comet1_key includes minimum precursor charge, and so only the last value in the config file needs to be changed
                    self.update_comet_params_key(comet1_key, self.current_max_charge, comment_spacer = 17, val_list_spacer = ' ', change_last_val_only = True)
                    # comment spacer is fixed at 15 because the maximum precursor charge state is a single digit number
                    self.update_comet_params_key(comet2_key, self.current_max_charge, comment_spacer = 15, val_list_spacer = '', change_last_val_only = False)
                    self.update_msgf_config(msgf_key, self.current_max_charge)
                    self.update_gpquest_params_key(gpquest_key, spacer, self.current_max_charge, line_end, quotes = False)
                    self.update_luciphor2_params_key(luciphor2_key, self.current_max_charge)
            except:
                # comment spacer is fixed at 17 because the maximum precursor charge state is a single digit number
                # comet1_key includes minimum precursor charge, and so only the last value in the config file needs to be changed
                self.update_comet_params_key(comet1_key, self.current_max_charge, comment_spacer = 17, val_list_spacer = ' ', change_last_val_only = True)
                # comment spacer is fixed at 15 because the maximum precursor charge state is a single digit number
                self.update_comet_params_key(comet2_key, self.current_max_charge, comment_spacer = 15, val_list_spacer = '', change_last_val_only = False)
                self.update_msgf_config(msgf_key, self.current_max_charge)
                self.update_gpquest_params_key(gpquest_key, spacer, self.current_max_charge, line_end, quotes = False)
                self.update_luciphor2_params_key(luciphor2_key, self.current_max_charge)
            
            # search engine
            try:
                config_key = 'search_engine'
                config_val = str(options[config_key]).strip()  
                    
                if config_val == 'msgf':
                    if config_val != self.databaseVar.get():
                        self.databaseVar.set(config_val)
                else:
                    self.update_config('steps.txt', config_key, self.databaseVar.get())
            except:
                self.update_config('steps.txt', config_key, self.databaseVar.get()) 
            
            # fdr
            spacer = 4
            line_end = ''
            
            if new_fdr != self.current_fdr:
                try:
                    float(new_fdr)
                    float(self.current_fdr)
                    
                    if new_fdr != self.current_fdr:
                        self.current_fdr = new_fdr
                        config_formatted_fdr = str(float(self.current_fdr))
                        gpquest_formatted_fdr = str(float(self.current_fdr) / 100.0)
                
                        self.update_config('steps.txt', 'false_discovery_rate', config_formatted_fdr)   
                        self.update_gpquest_params_key('"FDR":', spacer, gpquest_formatted_fdr, line_end, quotes = False)
                except:
                    pass    
            else:
                config_key = 'false_discovery_rate'
                gpquest_key = '"FDR":'
                
                try:
                    config_val = str(options[config_key]).strip()
                    gpquest_val = str(self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', gpquest_key))
                    
                    float(config_val)
                    float(gpquest_val)
                    float(new_fdr)
                    
                    # need to convert to floats for comparison
                    config_mapped_val = str(float(config_val))
                    gpquest_mapped_val = str(float(gpquest_val) * 100.0)
                    new_mapped_fdr = str(float(new_fdr))
                    
                    unique_val = None
                    if config_mapped_val != new_mapped_fdr and config_mapped_val != gpquest_mapped_val:
                        unique_val = config_mapped_val
                    elif gpquest_mapped_val != new_mapped_fdr and gpquest_mapped_val != config_mapped_val:
                        unique_val = gpquest_mapped_val
                        
                    # can use new_mapped_fdr here instead of float(self.current_fdr) because this else statement means that self.current_fdr == new_fdr
                    if unique_val != new_mapped_fdr and unique_val != None:
                        self.current_fdr = unique_val
                        config_formatted_fdr = str(float(self.current_fdr))
                        gpquest_formatted_fdr = str(float(self.current_fdr) / 100.0)
                        
                        self.fdr_text.delete('1.0', 'end')
                        self.fdr_text.insert('insert', self.current_fdr)
                    
                        self.update_config('steps.txt', config_key, config_formatted_fdr)
                        self.update_gpquest_params_key(gpquest_key, spacer, gpquest_formatted_fdr, line_end, quotes = False)
                except:
                    config_formatted_fdr = str(float(self.current_fdr))
                    gpquest_formatted_fdr = str(float(self.current_fdr) / 100.0)
                    
                    self.update_config('steps.txt', config_key, config_formatted_fdr)
                    self.update_gpquest_params_key(gpquest_key, spacer, gpquest_formatted_fdr, line_end, quotes = False)
            
            # IR score
            if new_ir != self.current_ir:
                try:
                    float(new_ir)
                    self.current_ir = new_ir
                    
                    self.update_config('steps.txt', 'ir_score', self.current_ir)
                except:
                    pass
            else:
                config_key = 'ir_score'
                
                try:
                    config_val = str(options[config_key]).strip()
                    float(config_val)
                    
                    if config_val != self.current_ir:
                        self.current_ir = config_val
                        
                        self.ir_text.delete('1.0', 'end')
                        self.ir_text.insert('insert', self.current_ir)
                except:
                    self.update_config('steps.txt', config_key, self.current_ir)
                    
            # Morpheus score
            if new_morpheus != self.current_morpheus:
                try:
                    float(new_morpheus)
                    self.current_morpheus = new_morpheus
                    
                    self.update_config('steps.txt', 'morpheus_score', self.current_morpheus)
                except:
                    pass
            else:
                config_key = 'morpheus_score'
                
                try:
                    config_val = str(options[config_key]).strip()
                    float(config_val)
                    
                    if config_val != self.current_morpheus:
                        self.current_morpheus = config_val
                        
                        self.morpheus_text.delete('1.0', 'end')
                        self.morpheus_text.insert('insert', self.current_morpheus)
                except:
                    self.update_config('steps.txt', config_key, self.current_morpheus)
            
            # false localization rate
            if new_flr != self.current_flr:
                try:
                    float(new_flr)
                    self.current_flr = new_flr
                    
                    self.update_config('steps.txt', 'false_localization_rate', self.current_flr)
                except:
                    pass
            else:
                config_key = 'false_localization_rate'
                
                try:
                    config_val = str(options[config_key]).strip()
                    float(config_val)
                    
                    if config_val != self.current_flr:
                        self.current_flr = config_val
                        
                        self.flr_text.delete('1.0', 'end')
                        self.flr_text.insert('insert', self.current_flr)
                except:
                    self.update_config('steps.txt', config_key, self.current_flr)
                    
            # intensity threshold
            if new_intensity_threshold != self.current_intensity_threshold:
                try:
                    int(new_intensity_threshold)
                    self.current_intensity_threshold = new_intensity_threshold
                
                    self.update_config('steps.txt', 'intensity_threshold', self.current_intensity_threshold)   
                except:
                    pass    
            else:
                config_key = 'intensity_threshold'
                
                try:
                    config_val = str(options[config_key]).strip()
                    int(config_val)
                        
                    if config_val != self.current_intensity_threshold:
                        self.current_intensity_threshold = config_val
                        
                        self.intensity_threshold_text.delete('1.0', 'end')
                        self.intensity_threshold_text.insert('insert', self.current_intensity_threshold)
                except:
                    self.update_config('steps.txt', config_key, self.current_intensity_threshold)
            
            # min psm per peptide
            if new_min_psm != self.current_min_psm:
                try:
                    int(new_min_psm)
                    self.current_min_psm = new_min_psm
                        
                    self.update_config('steps.txt', 'min_psm_per_peptide', self.current_min_psm)
                except:
                    pass
            else:
                config_key = 'min_psm_per_peptide'
                
                try:
                    config_val = str(options[config_key]).strip()
                    int(config_val)
                        
                    if config_val != self.current_min_psm:
                        self.current_min_psm = config_val
                        
                        self.min_psm_text.delete('1.0', 'end')
                        self.min_psm_text.insert('insert', self.current_min_psm)
                except:
                    self.update_config('steps.txt', config_key, self.current_min_psm)   
            
            # min peptide per protein
            if new_min_pep != self.current_min_pep:
                try:
                    int(new_min_pep)
                    self.current_min_pep = new_min_pep
                    
                    self.update_config('steps.txt', 'min_peptide_per_protein', self.current_min_pep)
                except:
                    pass
            else:
                config_key = 'min_peptide_per_protein'
                
                try:
                    config_val = str(options[config_key]).strip()
                    int(config_val)
                        
                    if config_val != self.current_min_pep:
                        self.current_min_pep = config_val
                        
                        self.min_pep_text.delete('1.0', 'end')
                        self.min_pep_text.insert('insert', self.current_min_pep)
                except:
                    self.update_config('steps.txt', config_key, self.current_min_pep) 
            
            # n-terminus methionine cleavage 
            try:
                config_key = 'optional_n_term_met_cleavage'
                config_val = str(options[config_key]).strip()  
                    
                if config_val == 'true' or config_val == 'false':
                    if config_val != self.nMetVar.get():
                        self.nMetVar.set(config_val)
                else:
                    self.update_config('steps.txt', config_key, self.nMetVar.get())
            except:
                self.update_config('steps.txt', config_key, self.nMetVar.get())
                
            # correction_matrix use
            try:
                config_key = 'correction_matrix'
                config_val = str(options[config_key]).strip()
                    
                if config_val == 'true' or config_val == 'false':
                    if config_val != self.correctionMatrixVar.get():
                        self.correctionMatrixVar.set(config_val)
                else:
                    self.update_config('steps.txt', config_key, self.correctionMatrixVar.get())
            except:
                self.update_config('steps.txt', config_key, self.correctionMatrixVar.get())
                
            # modification (global/glyco/intact/phospho)
            try:
                config_key = 'modification'
                config_val = str(options[config_key]).strip()  
                    
                if config_val in valid_mods or config_val == '':
                    if config_val != new_mod:
                        # update the formatting of config_val
                        mod_val = config_val
                        if mod_val == 'global':
                            mod_val = 'Global'
                        elif mod_val == 'glyco':
                            mod_val = 'SPEG'
                        elif mod_val == 'intact':
                            mod_val = 'Intact glyco'
                        elif mod_val == 'phospho':
                            mod_val = 'Phospho'
                        
                        self.modVar.set(mod_val)
                else:
                    self.update_config('steps.txt', config_key, new_mod)
            except:
                self.update_config('steps.txt', config_key, new_mod)
                
            # localize_phosphosites (true/false)
            try:
                config_key = 'localize_phosphosites'
                config_val = str(options[config_key]).strip()
                
                if config_val == 'true' or config_val == 'false':
                    if config_val != self.localizePhosphositesVar.get():
                        self.localizePhosphositesVar.set(config_val)
                else:
                    self.update_config('steps.txt', config_key, self.localizePhosphositesVar.get())
            except:
                self.update_config('steps.txt', config_key, self.localizePhosphositesVar.get())
            
            # normalization channel
            try:
                config_key = 'normalization_channel'
                config_val = str(options[config_key]).strip() 
                    
                if config_val in valid_channels or config_val == '':
                    # update the formatting to match the combobox
                    if config_val in itraq_channels:
                        config_val += ' - iTRAQ'
                    elif config_val in tmt_channels:
                        config_val += ' - TMT'
                    
                    if config_val != self.channelVar.get():
                        self.channelVar.set(config_val)
                else:
                    self.update_config('steps.txt', config_key, str(self.channelVar.get()).split('-')[0].strip())
            except:
                self.update_config('steps.txt', config_key, str(self.channelVar.get()).split('-')[0].strip())
                
            # ftp address
            if new_ftp_address != self.current_ftp_address:
                try:
                    str(new_ftp_address)
                    self.current_ftp_address = new_ftp_address
                    
                    self.update_config('steps.txt', 'ftp_address', self.current_ftp_address)
                except:
                    pass
            else:
                config_key = 'ftp_address'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_ftp_address:
                        self.current_ftp_address = config_val
                        
                        self.ftp_address_text.delete('1.0', 'end')
                        self.ftp_address_text.insert('insert', self.current_ftp_address)
                except:
                    self.update_config('steps.txt', config_key, self.current_ftp_address)
                    
            # ftp user
            if new_ftp_user != self.current_ftp_user:
                try:
                    str(new_ftp_user)
                    self.current_ftp_user = new_ftp_user
                    
                    self.update_config('steps.txt', 'ftp_user', self.current_ftp_user)
                except:
                    pass
            else:
                config_key = 'ftp_user'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_ftp_user:
                        self.current_ftp_user = config_val
                        
                        self.ftp_user_text.delete('1.0', 'end')
                        self.ftp_user_text.insert('insert', self.current_ftp_user)
                except:
                    self.update_config('steps.txt', config_key, self.current_ftp_user)
                
            # ftp password
            if new_ftp_password != self.current_ftp_password:
                try:
                    str(new_ftp_password)
                    self.current_ftp_password = new_ftp_password
                
                    self.update_config('steps.txt', 'ftp_password', self.current_ftp_password)
                except:
                    pass
            else:
                config_key = 'ftp_password'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_ftp_password:
                        self.current_ftp_password = config_val
                        
                        self.ftp_password_text.delete('1.0', 'end')
                        self.ftp_password_text.insert('insert', self.current_ftp_password)
                except:
                    self.update_config('steps.txt', config_key, self.current_ftp_password)
                
            # ftp dir
            if new_ftp_dir != self.current_ftp_dir:
                try:
                    str(new_ftp_dir)
                    self.current_ftp_dir = new_ftp_dir
                
                    self.update_config('steps.txt', 'ftp_dir', self.current_ftp_dir)
                except:
                    pass
            else:
                config_key = 'ftp_dir'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_ftp_dir:
                        self.current_ftp_dir = config_val
                        
                        self.ftp_dir_text.delete('1.0', 'end')
                        self.ftp_dir_text.insert('insert', self.current_ftp_dir)
                except:
                    self.update_config('steps.txt', config_key, self.current_ftp_dir)
                
            # faa ext
            if new_faa_ext != self.current_faa_ext:
                try:
                    str(new_faa_ext)
                    self.current_faa_ext = new_faa_ext
                    
                    self.update_config('steps.txt', 'faa_extension', self.current_faa_ext)                 
                except:
                    pass
            else:
                config_key = 'faa_extension'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_faa_ext:
                        self.current_faa_ext = config_val
                        
                        self.faa_ext_text.delete('1.0', 'end')
                        self.faa_ext_text.insert('insert', self.current_faa_ext)
                except:
                    self.update_config('steps.txt', config_key, self.current_faa_ext)
            
            # fasta name
            if new_fasta_name != self.current_fasta_name:
                try:
                    str(new_fasta_name)
                    self.current_fasta_name = new_fasta_name.strip().replace('.fasta', '')
                    
                    self.update_config('steps.txt', 'fasta_filename', self.current_fasta_name + '.fasta')
                except:
                    pass
            else:
                config_key = 'fasta_filename'
                
                try:
                    config_val = str(options[config_key]).strip().replace('.fasta', '')
                    str(config_val)
                        
                    if config_val != self.current_fasta_name:
                        self.current_fasta_name = config_val
                        
                        self.fasta_name_text.delete('1.0', 'end')
                        self.fasta_name_text.insert('insert', self.current_fasta_name)
                except:    
                    self.current_fasta_name = self.current_fasta_name.strip().replace('.fasta', '')
                                    
                    self.update_config('steps.txt', config_key, self.current_fasta_name + '.fasta')
                    
            # user id
            if new_user_id != self.current_user_id:
                try:
                    str(new_user_id)
                    self.current_user_id = new_user_id
                    
                    self.update_config('steps.txt', 'cluster_user_id', self.current_user_id)
                except:
                    pass
            else:
                config_key = 'cluster_user_id'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_user_id:
                        self.current_user_id = config_val
                        
                        self.user_id_text.delete('1.0', 'end')
                        self.user_id_text.insert('insert', self.current_user_id)
                except:
                    self.update_config('steps.txt', config_key, self.current_user_id)
                    
            # cluster name
            if new_cluster_name != self.current_cluster_name:
                try:
                    str(new_cluster_name)
                    self.current_cluster_name = new_cluster_name
                    
                    self.update_config('steps.txt', 'cluster_name', self.current_cluster_name)
                except:
                    pass
            else:
                config_key = 'cluster_name'
                
                try:
                    config_val = str(options[config_key]).strip()
                    str(config_val)
                        
                    if config_val != self.current_cluster_name:
                        self.current_cluster_name = config_val
                        
                        self.cluster_name_text.delete('1.0', 'end')
                        self.cluster_name_text.insert('insert', self.current_cluster_name)
                except:
                    self.update_config('steps.txt', config_key, self.current_cluster_name)
            
            # ec2 key selection
            if sc_keys == [] or sc_key not in sc_keys:
                self.update_sc_key_config('')
                
                self.key_choice_text.config(state = 'normal')
                self.key_choice_text.delete('1.0', 'end')
                self.key_choice_text.insert('insert', '')
                self.key_choice_text.config(state = 'disabled')
            else:
                try:  
                    if sc_key in sc_keys or sc_key == '':
                        if sc_key != self.current_ec2_key:
                            self.current_ec2_key = sc_key
                            
                            self.key_choice_text.config(state = 'normal')
                            self.key_choice_text.delete('1.0', 'end')
                            self.key_choice_text.insert('insert', self.current_ec2_key)
                            self.key_choice_text.config(state = 'disabled')
                    else:
                        self.update_sc_key_config(self.current_ec2_key)
                except:
                    self.update_sc_key_config(self.current_ec2_key)                                        
            
            # ec2 key options
            if self.select_ec2_flag:
                f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
                lines = f.readlines()
                f.close()
                
                new_keys = []
                key_section_flag = False
                for line in lines:
                    line = line.strip()
                    
                    if line == '## Defining EC2 Keypairs ##':
                        key_section_flag = True
                        continue
                        
                    if line == '## Defining Cluster Templates ##':
                        break
                        
                    if key_section_flag == True:   
                        if line.split(' ')[0].strip() == '[key':
                            key_name = line.split('[key')[-1].split(']')[0].strip()
                            new_keys.append(key_name)
                        elif line != '':
                            if line[0] == '#':
                                if line.split('#')[1].strip().split(' ')[0].strip() == '[key':
                                    key_name = line.split('[key')[-1].split(']')[0].strip()
                                    new_keys.append(key_name)
                            
                # update listbox contents
                if new_keys != self.current_keys:
                    self.current_keys = new_keys
                    
                    self.key_choice.delete(0, 'end')
                    for k in self.current_keys:
                        self.key_choice.insert(-1, k)   
                    
                    scroll_pos = self.key_choice.yview()
                    self.key_choice.yview('moveto', scroll_pos[0])
            
            # volume selection
            if sc_volumes == [] or sc_volume not in sc_volumes:
                volume_choice_val = str(ebs_params['volume_choice']).strip()
                
                if sc_volume != '' or volume_choice_val != '':
                    self.update_sc_volume_config('')
                    self.update_config('ebs_params.txt', 'volume_choice', '')
                    
                    self.volume_choice_text.config(state = 'normal')
                    self.volume_choice_text.delete('1.0', 'end')
                    self.volume_choice_text.insert('insert', '')
                    self.volume_choice_text.config(state = 'disabled')
            else:
                try:
                    if sc_volume in sc_volumes or sc_volume == '':
                        if sc_volume != self.current_volume_choice:
                            self.current_volume_choice = sc_volume
                            
                            self.volume_choice_text.config(state = 'normal')
                            self.volume_choice_text.delete('1.0', 'end')
                            self.volume_choice_text.insert('insert', self.current_volume_choice)
                            self.volume_choice_text.config(state = 'disabled')
                            
                            self.update_config('ebs_params.txt', 'volume_choice', self.current_volume_choice)
                    else:
                        self.update_sc_volume_config(self.current_volume_choice)
                        self.update_config('ebs_params.txt', 'volume_choice', self.current_volume_choice)
                except:
                    self.update_sc_volume_config(self.current_volume_choice)
                    self.update_config('ebs_params.txt', 'volume_choice', self.current_volume_choice)
            
            # volume options
            if self.select_ebs_flag:
                f = open(self.pipeline_path + 'configs-starcluster\\starcluster', 'r')
                lines = f.readlines()
                f.close()
                
                new_volumes = []
                volume_section_flag = False
                for line in lines:
                    line = line.strip()
                    
                    if line == '# Sections starting with "volume" define your EBS volumes':
                        volume_section_flag = True
                        continue
                        
                    if line == '## Configuring Security Group Permissions ##':
                        break
                        
                    if volume_section_flag == True: 
                        if line.split(' ')[0].strip() == '[volume':
                            volume_name = line.split('[volume')[-1].split(']')[0].strip()
                            new_volumes.append(volume_name)
                        elif line != '':
                            if line[0] == '#':
                                if line.split('#')[1].strip().split(' ')[0].strip() == '[volume':
                                    volume_name = line.split('[volume')[-1].split(']')[0].strip()
                                    new_volumes.append(volume_name)
                            
                # update listbox contents
                if new_volumes != self.current_volumes:
                    self.current_volumes = new_volumes
                    
                    self.volume_choice.delete(0, 'end')
                    for vol in self.current_volumes:
                        self.volume_choice.insert(-1, vol)   
                    
                    scroll_pos = self.volume_choice.yview()
                    self.volume_choice.yview('moveto', scroll_pos[0])
            
            # gpquest gui parameters
            if self.gpquest_gui_flag:
                new_protein_database = self.protein_database_text.get('1.0', 'end').strip()
                new_nglycan_database = self.nglycan_database_text.get('1.0', 'end').strip()
                new_oglycan_database = self.oglycan_database_text.get('1.0', 'end').strip()
                new_ms1_ppm = self.ms1_ppm_text.get('1.0', 'end').strip()
                new_ms2_ppm = self.ms2_ppm_text.get('1.0', 'end').strip()
                        
                key = '"PROTEIN":'
                spacer = 6
                line_end = ','
                if new_protein_database != self.current_protein_database:
                    try:
                        str(new_protein_database)
                        self.current_protein_database = new_protein_database
                    
                        val = self.current_protein_database
                            
                        self.update_gpquest_params_key(key, spacer, val, line_end, quotes = True)
                    except:
                        pass
                else:
                    try:
                        config_val = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')
                        str(config_val)
                            
                        if config_val != self.current_protein_database:
                            self.current_protein_database = config_val
                            
                            self.protein_database_text.config(state = 'normal')
                            self.protein_database_text.delete('1.0', 'end')
                            self.protein_database_text.insert('insert', self.current_protein_database)
                            self.protein_database_text.config(state = 'disabled')
                    except:
                        self.update_gpquest_params_key(key, spacer, self.current_protein_database, line_end, quotes = True)
                        
                key = '"NGLYCAN":'
                spacer = 6
                line_end = ','
                if new_nglycan_database != self.current_nglycan_database:
                    try:
                        str(new_nglycan_database)
                        self.current_nglycan_database = new_nglycan_database
                    
                        val = self.current_nglycan_database
                            
                        self.update_gpquest_params_key(key, spacer, val, line_end, quotes = True)
                    except:
                        pass
                else:
                    try:
                        config_val = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')
                        str(config_val)
                            
                        if config_val != self.current_nglycan_database:
                            self.current_nglycan_database = config_val
                            
                            self.nglycan_database_text.config(state = 'normal')
                            self.nglycan_database_text.delete('1.0', 'end')
                            self.nglycan_database_text.insert('insert', self.current_nglycan_database)
                            self.nglycan_database_text.config(state = 'disabled')
                    except:
                        self.update_gpquest_params_key(key, spacer, self.current_nglycan_database, line_end, quotes = True)
                        
                key = '"OGLYCAN":'
                spacer = 6
                line_end = ''
                if new_oglycan_database != self.current_oglycan_database:
                    try:
                        str(new_oglycan_database)
                        self.current_oglycan_database = new_oglycan_database
                    
                        val = self.current_oglycan_database
                            
                        self.update_gpquest_params_key(key, spacer, val, line_end, quotes = True)
                    except:
                        pass
                else:
                    try:
                        config_val = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')
                        str(config_val)
                            
                        if config_val != self.current_oglycan_database:
                            self.current_oglycan_database = config_val
                            
                            self.oglycan_database_text.config(state = 'normal')
                            self.oglycan_database_text.delete('1.0', 'end')
                            self.oglycan_database_text.insert('insert', self.current_oglycan_database)
                            self.oglycan_database_text.config(state = 'disabled')
                    except:
                        self.update_gpquest_params_key(key, spacer, self.current_oglycan_database, line_end, quotes = True)
            
                key = '"MS1(PPM)":'
                spacer = 6
                line_end = ','
                if new_ms1_ppm != self.current_ms1_ppm:
                    try:
                        str(new_ms1_ppm)
                        self.current_ms1_ppm = new_ms1_ppm
                    
                        val = self.current_ms1_ppm
                            
                        self.update_gpquest_params_key(key, spacer, val, line_end, quotes = False)
                    except:
                        pass
                else:
                    try:
                        config_val = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')
                        str(config_val)
                            
                        if config_val != self.current_ms1_ppm:
                            self.current_ms1_ppm = config_val
                            
                            self.ms1_ppm_text.delete('1.0', 'end')
                            self.ms1_ppm_text.insert('insert', self.current_ms1_ppm)
                    except:
                        self.update_gpquest_params_key(key, spacer, self.current_ms1_ppm, line_end, quotes = False)
                
                key = '"MS2(PPM)":'
                spacer = 6
                line_end = ','
                if new_ms2_ppm != self.current_ms2_ppm:
                    try:
                        str(new_ms2_ppm)
                        self.current_ms2_ppm = new_ms2_ppm
                    
                        val = self.current_ms2_ppm
                            
                        self.update_gpquest_params_key(key, spacer, val, line_end, quotes = False)
                    except:
                        pass
                else:
                    try:
                        config_val = self.read_gpquest_params_key(self.config_path + 'gpquest_params.json', key).replace('"', '')
                        str(config_val)
                            
                        if config_val != self.current_ms2_ppm:
                            self.current_ms2_ppm = config_val
                            
                            self.ms2_ppm_text.delete('1.0', 'end')
                            self.ms2_ppm_text.insert('insert', self.current_ms2_ppm)
                    except:
                        self.update_gpquest_params_key(key, spacer, self.current_ms2_ppm, line_end, quotes = False)
            
            # volume creation
            if self.make_ebs_flag:
                new_name = self.volume_name_text.get('1.0', 'end').strip()
                new_size = self.volume_size_text.get('1.0', 'end').strip()
                new_zone = self.volume_zone_text.get('1.0', 'end').strip()
                new_mount = self.volume_mount_path_text.get('1.0', 'end').strip()
                
                if new_name != self.current_volume_name:
                    try:
                        str(new_name)
                        self.current_volume_name = new_name
                        
                        self.update_config('ebs_params.txt', 'volume_name', self.current_volume_name)
                    except:
                        pass
                else:
                    config_key = 'volume_name'
                    
                    try:
                        config_val = str(ebs_params[config_key]).strip()
                        str(config_val)
                            
                        if config_val != self.current_volume_name:
                            self.current_volume_name = config_val
                            
                            self.volume_name_text.delete('1.0', 'end')
                            self.volume_name_text.insert('insert', self.current_volume_name)
                    except:
                        self.update_config('ebs_params.txt', config_key, self.current_volume_name)
                        
                if new_size != self.current_volume_size:
                    try:
                        int(new_size)
                        self.current_volume_size = new_size
                        
                        self.update_config('ebs_params.txt', 'volume_size', self.current_volume_size)
                    except:
                        pass
                else:
                    config_key = 'volume_size'
                    
                    try:
                        config_val = str(ebs_params[config_key]).strip()
                        int(config_val)
                            
                        if config_val != self.current_volume_size:
                            self.current_volume_size = config_val
                            
                            self.volume_size_text.delete('1.0', 'end')
                            self.volume_size_text.insert('insert', self.current_volume_size)
                    except:
                        self.update_config('ebs_params.txt', config_key, self.current_volume_size)
                
                if new_zone != self.current_volume_zone:
                    try:
                        str(new_zone)
                        self.current_volume_zone = new_zone
                        
                        self.update_config('ebs_params.txt', 'volume_zone', self.current_volume_zone)
                    except:
                        pass
                else:
                    config_key = 'volume_zone'
                    
                    try:
                        config_val = str(ebs_params[config_key]).strip()
                        str(config_val)
                            
                        if config_val != self.current_volume_zone:
                            self.current_volume_zone = config_val
                            
                            self.volume_zone_text.delete('1.0', 'end')
                            self.volume_zone_text.insert('insert', self.current_volume_zone)
                    except:
                        self.update_config('ebs_params.txt', config_key, self.current_volume_zone)
                        
                if new_mount != self.current_volume_mount:
                    try:
                        str(new_mount)
                        self.current_volume_mount = new_mount
                        
                        self.update_config('ebs_params.txt', 'mount_path', self.current_volume_mount)
                    except:
                        pass
                else:
                    config_key = 'mount_path'
                    
                    try:
                        config_val = str(ebs_params[config_key]).strip()
                        str(config_val)
                            
                        if config_val != self.current_volume_mount:
                            self.current_volume_mount = config_val
                            
                            self.volume_mount_path_text.config(state = 'normal')
                            self.volume_mount_path_text.delete('1.0', 'end')
                            self.volume_mount_path_text.insert('insert', self.current_volume_mount)
                            self.volume_mount_path_text.config(state = 'disabled')
                    except:
                        self.update_config('ebs_params.txt', config_key, self.current_volume_mount)
            
            for index, raw in enumerate(new_raw_data):
                new_raw_data[index] = os.path.basename(raw).replace('.raw', '')
            
            new_raw_data.sort(key = natural_keys)
            
            # sample names gui updating
            if self.sample_flag == True:
                if new_raw_path != self.current_raw_path:
                    self.sample_try_search_results = True
                    
                    self.current_raw_path = new_raw_path
                    
                    self.current_raw_data = glob.glob(self.current_raw_path + '*.raw')
                    self.current_raw_data.sort(key = natural_keys)
                    
                    for index, raw in enumerate(self.current_raw_data):
                        self.current_raw_data[index] = os.path.basename(raw).replace('.raw', '')
                    
                    self.sample_options.delete('1.0', 'end')
                    
                    if self.current_raw_data != []:
                        if len(self.current_raw_data) > 1:
                            first_sample_new_line = '\n'
                        else:
                            first_sample_new_line = ''
                            
                        self.sample_options.insert('insert', self.current_raw_data[0] + first_sample_new_line)
                        
                        if len(self.current_raw_data) > 1:
                            for raw in self.current_raw_data[1:-1]:
                                raw = os.path.basename(raw)    
                                self.sample_options.insert('insert', '\n' + raw + '\n')
                                
                            self.sample_options.insert('insert', '\n' + self.current_raw_data[-1])
                        
                    #self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample)  
                elif new_raw_data != self.current_raw_data:
                    self.sample_try_search_results = True
                    
                    self.current_raw_data = list(new_raw_data)
                    self.current_raw_data.sort(key = natural_keys)
                    
                    for index, raw in enumerate(self.current_raw_data):
                        self.current_raw_data[index] = os.path.basename(raw).replace('.raw', '')
                    
                    scroll_pos = self.sample_options.yview()
                    
                    self.sample_options.delete('1.0', 'end')
                    
                    if self.current_raw_data != []:
                        if len(self.current_raw_data) > 1:
                            first_sample_new_line = '\n'
                        else:
                            first_sample_new_line = ''
                            
                        self.sample_options.insert('insert', self.current_raw_data[0] + first_sample_new_line)
                        
                        if len(self.current_raw_data) > 1:
                            for raw in self.current_raw_data[1:-1]:
                                raw = os.path.basename(raw)    
                                self.sample_options.insert('insert', '\n' + raw + '\n')
                            
                            self.sample_options.insert('insert', '\n' + self.current_raw_data[-1])
                    
                    self.sample_options.yview('moveto', scroll_pos[0])
                    
                    #self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample)
                    
                if self.sample_options.get('1.0', 'end').strip() == '' and self.sample_try_search_results == True:
                    # only try this once per change in raw data path or raw data files
                    self.sample_try_search_results = False
                    
                    mzml_files = glob.glob(self.current_raw_path + 'step1-mzml\\*.mzml')
                    mzml_files.sort(key = natural_keys)
                    
                    for index, mzml in enumerate(mzml_files):
                        mzml_files[index] = os.path.basename(mzml).replace('.mzML', '')
                    
                    self.sample_options.delete('1.0', 'end')
                    
                    if mzml_files != []: 
                        self.sample_options.insert('insert', mzml_files[0] + '\n')
                        
                        if len(mzml_files) > 1:
                            for mzml in mzml_files[1:-1]:
                                mzml = os.path.basename(mzml)    
                                self.sample_options.insert('insert', '\n' + mzml + '\n')
                            
                            self.sample_options.insert('insert', '\n' + mzml_files[-1])
                    else:
                        search_path_files = glob.glob(self.current_raw_path + 'step3-search_results\\*.*')
                        search_path_files.sort(key = natural_keys)
                        
                        valid_extensions = ['mzid', 'pepxml', 'xml', 'csv']
                        search_files = []
                        
                        for f in search_path_files:
                            f_extension = f.split('.')[-1].lower()
                            
                            if f_extension in valid_extensions:
                                search_files.append(f)
                        
                        for index, mzid in enumerate(search_files):
                            search_files[index] = os.path.basename(mzid).replace('.' + mzid.split('.')[-1], '')
                            
                        # removes duplicate names since multiple file extensions are possible for the same filename
                        search_files = list(set(search_files))
                        search_files.sort(key = natural_keys)
                            
                        self.sample_options.delete('1.0', 'end')
                            
                        if search_files != []:
                            self.sample_options.insert('insert', search_files[0] + '\n')
                            
                            if len(search_files) > 1:
                                for mzid in search_files[1:-1]:
                                    mzid = os.path.basename(mzid)    
                                    self.sample_options.insert('insert', '\n' + mzid + '\n')
                                    
                                self.sample_options.insert('insert', '\n' + search_files[-1])
            
                    #self.size_window(self.sampleFrame, self.centered_width_sample, self.centered_height_sample, self.x_sample, self.y_sample)
                                
            # gui for updating msgf modifications and parameters - will complete at a later date
            if self.msgf_flag == True:
                new_msgf_available_text_position = self.msgf_available_mods.index('insert')
                new_msgf_available_mods = self.msgf_available_mods.get('1.0', 'end').strip().split('\n')
                new_msgf_selected_mods = self.msgf_selected_mods.get('1.0', 'end').strip().split('\n')
                
                if new_msgf_available_text_position != self.current_msgf_available_text_position:
                    # highlight line the cursor is on
                    self.msgf_available_mods.tag_remove('sel', '1.0', 'end')
                    self.msgf_available_mods.tag_add('sel', 'insert linestart', 'insert lineend+1c')
                    
                    self.msgf_available_mods.mark_set('insert', 'insert lineend')
                    new_msgf_available_text_position = self.msgf_available_mods.index('insert')
                    self.current_msgf_available_text_position = new_msgf_available_text_position
                    
                new_msgf_selected_text_position = self.msgf_selected_mods.index('insert')
                
                if new_msgf_selected_text_position != self.current_msgf_selected_text_position:
                    # highlight line the cursor is on
                    self.msgf_selected_mods.tag_remove('sel', '1.0', 'end')
                    self.msgf_selected_mods.tag_add('sel', 'insert linestart', 'insert lineend+1c')
                    
                    self.msgf_selected_mods.mark_set('insert', 'insert lineend')
                    new_msgf_selected_text_position = self.msgf_selected_mods.index('insert')
                    self.current_msgf_selected_text_position = new_msgf_selected_text_position
                    
                #if new_msgf_available_mods != self.current_msgf_available_mods or new_msgf_selected_mods != self.current_msgf_selected_mods:
                #    self.size_window(self.msgfFrame, self.centered_width_msgf, self.centered_height_msgf, self.x_msgf, self.y_msgf)
            
            if new_s3 != self.current_s3:
                self.current_s3 = new_s3
                
                if self.current_s3:
                    self.step3_msgf.config(state = 'normal')
                    
                    # not currently supported
                    if self.current_type != 'cloud':
                        self.step3_myrimatch.config(state = 'normal')
                        self.step3_comet.config(state = 'normal')
                        self.step3_xtandem.config(state = 'normal')
                    elif self.current_type == 'cloud':
                        self.step3_myrimatch.config(state = 'disabled')
                        self.step3_comet.config(state = 'disabled')
                        self.step3_xtandem.config(state = 'disabled')
                
                else:
                    self.step3_msgf.config(state = 'disabled')
                    self.step3_myrimatch.config(state = 'disabled')
                    self.step3_comet.config(state = 'disabled')
                    self.step3_xtandem.config(state = 'disabled')
            
            if new_label != self.current_label:
                self.current_label = new_label
                
                if self.current_label == '10plex':
                    #self.channel113.config(state = 'disabled')
                    #self.channel114.config(state = 'disabled')
                    #self.channel115.config(state = 'disabled')
                    #self.channel116.config(state = 'disabled')
                    #self.channel117.config(state = 'disabled')
                    #self.channel118.config(state = 'disabled')
                    #self.channel119.config(state = 'disabled')
                    #self.channel121.config(state = 'disabled')
                    #self.channel126.config(state = 'normal')
                    #self.channel127N.config(state = 'normal')
                    #self.channel127C.config(state = 'normal')
                    #self.channel128N.config(state = 'normal')
                    #self.channel128C.config(state = 'normal')
                    #self.channel129N.config(state = 'normal')
                    #self.channel129C.config(state = 'normal')
                    #self.channel130N.config(state = 'normal')
                    #self.channel130C.config(state = 'normal')
                    #self.channel131.config(state = 'normal')
                    #self.channel131C.config(state = 'disabled')
                    
                    self.step7.config(state = 'normal')
                    
                    self.apply_correction_true.config(state = 'normal')
                elif self.current_label == '11plex':
                    #self.channel113.config(state = 'disabled')
                    #self.channel114.config(state = 'disabled')
                    #self.channel115.config(state = 'disabled')
                    #self.channel116.config(state = 'disabled')
                    #self.channel117.config(state = 'disabled')
                    #self.channel118.config(state = 'disabled')
                    #self.channel119.config(state = 'disabled')
                    #self.channel121.config(state = 'disabled')
                    #self.channel126.config(state = 'normal')
                    #self.channel127N.config(state = 'normal')
                    #self.channel127C.config(state = 'normal')
                    #self.channel128N.config(state = 'normal')
                    #self.channel128C.config(state = 'normal')
                    #self.channel129N.config(state = 'normal')
                    #self.channel129C.config(state = 'normal')
                    #self.channel130N.config(state = 'normal')
                    #self.channel130C.config(state = 'normal')
                    #self.channel131.config(state = 'normal')
                    #self.channel131C.config(state = 'normal')
                    
                    self.step7.config(state = 'normal')
                    
                    self.apply_correction_true.config(state = 'normal')
                elif self.current_label == '16plex':
                    self.step7.config(state = 'normal')
                    
                    self.apply_correction_true.config(state = 'normal')
                elif self.current_label == '4plex':
                    #self.channel113.config(state = 'disabled')
                    #self.channel114.config(state = 'normal')
                    #self.channel115.config(state = 'normal')
                    #self.channel116.config(state = 'normal')
                    #self.channel117.config(state = 'normal')
                    #self.channel118.config(state = 'disabled')
                    #self.channel119.config(state = 'disabled')
                    #self.channel121.config(state = 'disabled')
                    #self.channel126.config(state = 'disabled')
                    #self.channel127N.config(state = 'disabled')
                    #self.channel127C.config(state = 'disabled')
                    #self.channel128N.config(state = 'disabled')
                    #self.channel128C.config(state = 'disabled')
                    #self.channel129N.config(state = 'disabled')
                    #self.channel129C.config(state = 'disabled')
                    #self.channel130N.config(state = 'disabled')
                    #self.channel130C.config(state = 'disabled')
                    #self.channel131.config(state = 'disabled')
                    #self.channel131C.config(state = 'disabled')
                    
                    self.step7.config(state = 'normal')
                    
                    self.apply_correction_true.config(state = 'disabled')
                elif self.current_label == '8plex':
                    #self.channel113.config(state = 'normal')
                    #self.channel114.config(state = 'normal')
                    #self.channel115.config(state = 'normal')
                    #self.channel116.config(state = 'normal')
                    #self.channel117.config(state = 'normal')
                    #self.channel118.config(state = 'normal')
                    #self.channel119.config(state = 'normal')
                    #self.channel121.config(state = 'normal')
                    #self.channel126.config(state = 'disabled')
                    #self.channel127N.config(state = 'disabled')
                    #self.channel127C.config(state = 'disabled')
                    #self.channel128N.config(state = 'disabled')
                    #self.channel128C.config(state = 'disabled')
                    #self.channel129N.config(state = 'disabled')
                    #self.channel129C.config(state = 'disabled')
                    #self.channel130N.config(state = 'disabled')
                    #self.channel130C.config(state = 'disabled')
                    #self.channel131.config(state = 'disabled')
                    #self.channel131C.config(state = 'disabled')
                    
                    self.step7.config(state = 'normal')
                    
                    self.apply_correction_true.config(state = 'disabled')
                elif self.current_label == 'free':
                    #self.channel113.config(state = 'disabled')
                    #self.channel114.config(state = 'disabled')
                    #self.channel115.config(state = 'disabled')
                    #self.channel116.config(state = 'disabled')
                    #self.channel117.config(state = 'disabled')
                    #self.channel118.config(state = 'disabled')
                    #self.channel119.config(state = 'disabled')
                    #self.channel121.config(state = 'disabled')
                    #self.channel126.config(state = 'disabled')
                    #self.channel127N.config(state = 'disabled')
                    #self.channel127C.config(state = 'disabled')
                    #self.channel128N.config(state = 'disabled')
                    #self.channel128C.config(state = 'disabled')
                    #self.channel129N.config(state = 'disabled')
                    #self.channel129C.config(state = 'disabled')
                    #self.channel130N.config(state = 'disabled')
                    #self.channel130C.config(state = 'disabled')
                    #self.channel131.config(state = 'disabled')
                    #self.channel131C.config(state = 'disabled')
                    
                    self.step7.config(state = 'disabled')
                    self.step7Var.set(False) 
                    
                    self.apply_correction_true.config(state = 'disabled')  
                
                try:
                    config_key = 'label_type'
                    gpquest_key = '"LABEL":'
                    
                    config_val = str(options[config_key]).strip()
                    
                    # get gpquest_val
                    config_map = {}
                    config_map['TMT16plex'] = '16plex'
                    config_map['TMT10plex'] = '10plex'
                    config_map['iTRAQ4plex'] = '4plex'
                    config_map['None'] = 'free'
                    # GPQuest cannot currently process iTRAQ8plex or TMT11plex, and the GUI provides a warning message in this case
                    config_map['TMT11plex'] = '11plex'
                    config_map['iTRAQ8plex'] = '8plex'
                    
                    infile = open(self.config_path + 'gpquest_params.json', 'r')
                    infile_lines = infile.readlines()
                    infile.close()
                    
                    for line in infile_lines:
                        current_line = line.strip()
                        
                        # comma is to make sure modifying the right "LABEL" section since "LABEL" appears twice in the config file
                        if current_line[:len(gpquest_key)] == gpquest_key and current_line[-1] == ',':
                            gpquest_val = current_line.split(gpquest_key)[-1].strip().split(',')[0].strip().replace('"', '')
                            gpquest_mapped_val = config_map[gpquest_val]
                            break
                            
                    str(config_val)
                    str(gpquest_mapped_val)
                    
                    if config_val != gpquest_mapped_val or config_val != self.current_label or gpquest_mapped_val != self.current_label:
                        self.write_config_label("updatecombobox")
                except:
                    self.write_config_label("updatecombobox")                 
            else:
                # reagent label (4plex/8plex/10plex/11plex)
                try:
                    config_key = 'label_type'
                    gpquest_key = '"LABEL":'
                    
                    config_val = str(options[config_key]).strip()
                    
                    # get gpquest_val
                    config_map = {}
                    config_map['TMT16plex'] = '16plex'
                    config_map['TMT10plex'] = '10plex'
                    config_map['iTRAQ4plex'] = '4plex'
                    config_map['None'] = 'free'
                    # GPQuest cannot currently process iTRAQ8plex or TMT11plex, and the GUI provides a warning message in this case
                    config_map['TMT11plex'] = '11plex'
                    config_map['iTRAQ8plex'] = '8plex'
                    
                    infile = open(self.config_path + 'gpquest_params.json', 'r')
                    infile_lines = infile.readlines()
                    infile.close()
                    
                    for line in infile_lines:
                        current_line = line.strip()
                        
                        # comma is to make sure modifying the right "LABEL" section since "LABEL" appears twice in the config file
                        if current_line[:len(gpquest_key)] == gpquest_key and current_line[-1] == ',':
                            gpquest_val = current_line.split(gpquest_key)[-1].strip().split(',')[0].strip().replace('"', '')
                            gpquest_mapped_val = config_map[gpquest_val]
                            break
                            
                    str(config_val)
                    str(gpquest_mapped_val)
                    
                    unique_val = None
                    if config_val != new_label and config_val != gpquest_mapped_val:
                        unique_val = config_val
                    elif gpquest_mapped_val != new_label and gpquest_mapped_val != config_val:
                        unique_val = gpquest_mapped_val
                    
                    if unique_val != None:
                        if unique_val in valid_labels or unique_val == '':
                            # update the formatting for labelVar
                            label_val = unique_val
                            if unique_val == '10plex' or unique_val == '11plex' or unique_val == '16plex':
                                label_val = 'TMT ' + unique_val
                            elif unique_val == '4plex' or unique_val == '8plex':
                                label_val = 'iTRAQ ' + unique_val
                            elif unique_val == 'free' or unique_val == 'None':
                                label_val = 'Label free'
                            
                            self.current_label = unique_val
                            self.labelVar.set(label_val)
                            self.write_config_label("updatecombobox")
                        else:
                            raise Exception('')
                except:
                    self.write_config_label("updatecombobox")
                
            if new_type != self.current_type:
                self.current_type = new_type
                
                if self.current_type == 'cloud':
                    #self.add_aws_creds.config(state = 'disabled')
                    
                    # not currently supported
                    self.step3_myrimatch.config(state = 'disabled')
                    self.step3_comet.config(state = 'disabled')
                    self.step3_xtandem.config(state = 'disabled')
                else:
                    #self.add_aws_creds.config(state = 'disabled')
                    
                    # not currently supported
                    if self.current_s3:
                        self.step3_myrimatch.config(state = 'normal')
                        self.step3_comet.config(state = 'normal')
                        self.step3_xtandem.config(state = 'normal')
                        
            if new_mod != self.current_mod:
                self.current_mod = new_mod
                
                if self.current_mod == 'intact':
                    self.step3_msgf.config(text = 'GPQuest')
                    #self.label_8plex.config(state = 'disabled')
                    #self.label_11plex.config(state = 'disabled')
                    #self.step4.config(state = 'disabled')
                    #self.step5.config(state = 'disabled')
                    #self.step6.config(state = 'disabled')
                    #self.step7.config(state = 'disabled')
                    #self.step4Var.set(False)
                    #self.step5Var.set(False)
                    #self.step6Var.set(False)
                    #self.step7Var.set(False)
                    self.ir_text.config(bg = '#ffffff')
                    self.morpheus_text.config(bg = '#ffffff')
                else:
                    self.step3_msgf.config(text = 'MS-GF+  ')
                    #self.label_8plex.config(state = 'normal')
                    #self.label_11plex.config(state = 'normal')
                    #self.step4.config(state = 'normal')
                    #self.step5.config(state = 'normal')
                    #self.step6.config(state = 'normal')
                    
                    #if self.current_label != 'free':
                    #    self.step7.config(state = 'normal')
                    
                    self.ir_text.config(bg = '#d3d3d3')
                    self.morpheus_text.config(bg = '#d3d3d3')
                    
                if self.current_mod != 'phospho':
                    self.localizePhosphosites.config(state = 'disabled')
                    #self.localizePhosphositesVar.set('false') 
                    #self.flr_text.config(state = 'disabled')
                    self.flr_text.config(bg = '#d3d3d3')
                else:
                    self.localizePhosphosites.config(state = 'normal')
                    #self.flr_text.config(state = 'normal')
                    self.flr_text.config(bg = '#ffffff')
        except IOError:
            # ignores the occasional message that permission is denied to write to the config files because it is a timing issue with this updated display function - the file gets updated regardless
            pass  
        
        self.repeat = self.root.after(100, self.update_display)
        
    @staticmethod
    def all_steps_check(step_values):
        for step in step_values:
            if step == False:
                return False
        
        return True
    
    @staticmethod
    def determine_msgf_cleavage(cleavage_word):
        msgf_cleavages = {'unspecific cleavage': '0',
                            'Trypsin': '1',
                            'Chymotrypsin': '2',
                            'Lys-C': '3',
                            'Lys-N': '4',
                            'glutamyl endopeptidase': '5',
                            'Glu-C': '5',
                            'glutamyl endopeptidase (Glu-C)': '5',
                            'Arg-C': '6',
                            'Asp-N': '7',
                            'alphaLP': '8',
                            'no cleavage': '9'}
                            
        if cleavage_word in msgf_cleavages:
            return msgf_cleavages[cleavage_word]
        else:
            # returns 0 for cases where MS-GF+ doesn't specify the enzyme that we used
            return 0
        
    @staticmethod
    def msgf_cleavage_dict():
        msgf_cleavages = {'unspecific cleavage': '0',
                            'Trypsin': '1',
                            'Chymotrypsin': '2',
                            'Lys-C': '3',
                            'Lys-N': '4',
                            'glutamyl endopeptidase': '5',
                            'Glu-C': '5',
                            'glutamyl endopeptidase (Glu-C)': '5',
                            'Arg-C': '6',
                            'Asp-N': '7',
                            'alphaLP': '8',
                            'no cleavage': '9'}
                            
        return msgf_cleavages
    
    @staticmethod
    def determine_comet_cleavage(cleavage_word):
        comet_cleavages = {'No_enzyme': '0',
                            'Trypsin': '1',
                            'Trypsin/P': '2',
                            'Lys_C': '3',
                            'Lys_N': '4',
                            'Arg_C': '5',
                            'Arg-C': '5',
                            'Asp_N': '6',
                            'CNBr': '7',
                            'Glu_C': '8',
                            'PepsinA': '9',
                            'Chymotrypsin': '10'}
                            
        if cleavage_word in comet_cleavages:
            return comet_cleavages[cleavage_word] 
        else:
            # returns 0 for cases where Comet doesn't specify the enzyme that we used
            return 0 
            
    @staticmethod
    def comet_cleavage_dict():
        comet_cleavages = {'No_enzyme': '0',
                            'Trypsin': '1',
                            'Trypsin/P': '2',
                            'Lys_C': '3',
                            'Lys_N': '4',
                            'Arg_C': '5',
                            'Arg-C': '5',
                            'Asp_N': '6',
                            'CNBr': '7',
                            'Glu_C': '8',
                            'PepsinA': '9',
                            'Chymotrypsin': '10'}
        
        return comet_cleavages                            
        
# ******************************************************************************

def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]
    
# ******************************************************************************

if __name__ == '__main__':
    app = Processing_Pipeline()