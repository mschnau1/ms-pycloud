import os
import re
import sys
import glob
import time
import math
import shutil
import numpy
import traceback
from hotpot.pathing import make_path
from hotpot.configuring import read_steps_config as steps
from hotpot.read_write import read_lines as rl
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'

# number of header rows - defaults to 1 since this is hard coded into pipeline output
header = 1

# mod endings of filenames
mod_endings = ['intensity.' + x for x in ['sites', 'peptides', 'proteins', 'glycosites', 'glycopeptides', 'glycoproteins', 'phosphosites', 'phosphopeptides', 'phosphoproteins', 'nglycoformsites', 'nglycoformpeptides', 'nglycoformproteins', 'oglycoformsites', 'oglycoformpeptides', 'oglycoformproteins']]
ratio_endings = ['ratio.' + x for x in ['sites', 'peptides', 'proteins', 'glycosites', 'glycopeptides', 'glycoproteins', 'phosphosites', 'phosphopeptides', 'phosphoproteins', 'nglycoformsites', 'nglycoformpeptides', 'nglycoformproteins', 'oglycoformsites', 'oglycoformpeptides', 'oglycoformproteins']]

def main(configs, filename_mod):
    print '\nStep 7c: Merging intensity expression matrices together...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    sample_filenames = sorted(configs['sample filenames'])
    header_data = configs['header_expression_matrix']
    
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2] 
    matrix_path = output_folder_path + 'step7-expression_matrix\\sets\\'
    matrix_path_virtual = output_folder_path + 'step7-expression_matrix\\virtual_reference\\sets\\'
    output_path = output_folder_path + 'step7-expression_matrix\\'
    output_path_virtual = output_folder_path + 'step7-expression_matrix\\virtual_reference\\'
    qc_path = output_folder_path + 'quality_control\\'
    norm_channel = str(options['normalization_channel'])
    label_type = options['label_type']
    mod = str(options['modification']).lower()
    localize_phosphosites = str(options['localize_phosphosites']).lower()
    # assumes an equal sign after the gene name identifier
    gene_name_identifier = str(options['gene_name_identifier']) + '='
    
    itraq4_channels = ['114', '115', '116', '117']
    itraq8_channels = ['113', '114', '115', '116', '117', '118', '119', '121']
    tmt10_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
    tmt11_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
    tmt16_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
    
    if label_type == '4plex':
        header_section = 'iTRAQ4plex'
        channels = itraq4_channels
        num_channels = 4
    elif label_type == '8plex':
        header_section = 'iTRAQ8plex'
        channels = itraq8_channels
        num_channels = 8
    elif label_type == '10plex':
        header_section = 'TMT10plex'
        channels = tmt10_channels
        num_channels = 10
    elif label_type == '11plex':
        header_section = 'TMT11plex'
        channels = tmt11_channels
        num_channels = 11
    elif label_type == '16plex':
        header_section = 'TMT16plex'
        channels = tmt16_channels
        num_channels = 16
    
    if mod == 'global':
        protein_end = 'intensity.' + filename_mod + 'proteins'
        peptide_end = 'intensity.' + filename_mod + 'peptides'
        site_end = 'intensity.' + filename_mod + 'sites'
    elif mod == 'glyco':
        protein_end = 'intensity.' + filename_mod + 'proteins'
        peptide_end = 'intensity.' + filename_mod + 'peptides'
        site_end = 'intensity.' + filename_mod + 'sites'
    elif mod == 'phospho':
        protein_end = 'intensity.' + filename_mod + 'proteins'
        peptide_end = 'intensity.' + filename_mod + 'peptides'
        site_end = 'intensity.' + filename_mod + 'sites'
    elif mod == 'intact':
        protein_end = 'intensity.' + filename_mod + 'proteins'
        peptide_end = 'intensity.' + filename_mod + 'peptides'
        site_end = 'intensity.' + filename_mod + 'sites'
    
    # creates the output path folders
    make_path(qc_path)
    
    # handle single shot data
    matrices_all = glob.glob(matrix_path + '*.tsv')
    matrices_all_virtual = glob.glob(matrix_path_virtual + '*.tsv')
    matrices_all.sort(key = natural_keys)
    matrices_all_virtual.sort(key = natural_keys)
    if sample_filenames == list():
        for matrix_f in matrices_all:
            matrix_f_basename = os.path.basename(matrix_f).replace('.tsv', '')
            
            for intensity_end in mod_endings:
                matrix_f_basename = matrix_f_basename.replace('.' + intensity_end, '')
            
            for ratio_end in ratio_endings:
                matrix_f_basename = matrix_f_basename.replace('.' + ratio_end, '')
            
            if matrix_f_basename not in sample_filenames:
                sample_filenames.append(matrix_f_basename)
                
        # removes non-unique names in case a set file was left over from previous analysis
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)
        
    label_matrices(matrices_all, protein_end, peptide_end, site_end, sample_filenames, output_path, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier)
    
    # generate abundance matrices
    impute_missing = False
    #generate_abundance_matrices_using_mean(output_path, norm_channel, channels, num_channels, impute_missing)
    generate_abundance_matrices(output_path, norm_channel, channels, num_channels, impute_missing)
    
    # combine virtual reference matrices
    print '\nStep 7c continued: Merging virtual reference intensity expression matrices together...'
    print '-' * 150
    sys.stdout.flush()
    
    label_matrices_with_virtual_reference(matrices_all_virtual, protein_end, peptide_end, site_end, sample_filenames, output_path_virtual, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier)
    
    # generate abundance matrices
    impute_missing = False
    #generate_abundance_matrices_using_mean(output_path_virtual, norm_channel, channels, num_channels, impute_missing)
    generate_abundance_matrices(output_path_virtual, norm_channel, channels, num_channels, impute_missing)
    
    # the plots do not need to be generated when only the nglycoform matrices have been created
    if mod != 'intact' or (mod == 'intact' and filename_mod == 'oglycoform'):
        print '\nGenerating quality control distribution plots...'
        print '-'
        sys.stdout.flush()
        
        # generate quality control distribution plots for all matrices
        plt.rcParams.update({'figure.max_open_warning': 0})
        output_file = 'plots-' + 'expression_matrix_distributions-' + data_set_name + '.pdf'
        generate_expression_matrix_qc_distribution_plots(output_path, qc_path, output_file, sample_filenames, num_channels)
        
        # generate quality control distribution plots for all virtual reference matrices
        output_file = 'plots-' + 'expression_matrix_distributions-virtual_reference-' + data_set_name + '.pdf'
        generate_expression_matrix_qc_distribution_plots(output_path_virtual, qc_path, output_file, sample_filenames, num_channels)
        
        print 'Success...'
        sys.stdout.flush()
        
    # the FDR statistics only need to updated once the N-glyco and O-glyco matrices are generated
    if mod == 'intact' and filename_mod == 'oglycoform':  
        print '\nGenerating glycopeptide-level FDR statistics...'
        print '-'
        sys.stdout.flush()
        
        glyco_mod = 'nglycoform'
        file_suffix = '-' + glyco_mod
        fdr_name = 'N-glycopeptide'
        n_glyco_fdr = update_glyco_fdr_statistics(output_path, qc_path, glyco_mod, file_suffix, fdr_name)
        
        glyco_mod = 'oglycoform'
        file_suffix = '-' + glyco_mod
        fdr_name = 'O-glycopeptide'
        o_glyco_fdr = update_glyco_fdr_statistics(output_path, qc_path, glyco_mod, file_suffix, fdr_name)
        
        f = open(qc_path + 'stats-fdr_log-' + data_set_name + '.log', 'r')
        fdr_lines = f.readlines()
        f.close()
        
        # only keep the first 3 lines so we don't keep adding in more glycopeptide FDR statistics
        fdr_output_lines = list(fdr_lines[:3])
        
        for fdr_row in n_glyco_fdr:
            fdr_output_lines.append(fdr_row)
            
        for fdr_row in o_glyco_fdr:
            fdr_output_lines.append(fdr_row)
        
        f = open(qc_path + 'stats-fdr_log-' + data_set_name + '.log', 'w')
        for fdr_line in fdr_output_lines:
            f.write(fdr_line)
        f.close()
        
        print 'Success...'
        sys.stdout.flush()
    
def update_glyco_fdr_statistics(output_path, qc_path, glyco_mod, file_suffix, fdr_name):
    f = open(output_path + glyco_mod + '-peptide_matrix-intensities.tsv', 'r')
    matrix_lines = f.readlines()
    f.close()
    
    num_true_glycopeptides = len(matrix_lines) - header
    
    # calculate psm, peptide, and protein level fdr for the total protein inference results
    fdr_output = []
    
    peptide_fdr = -1
    
    try:
        decoy_path = output_path.replace('step7-expression_matrix\\', 'step5-decoy\\')    
        
        if os.path.isfile(decoy_path + 'reversed_decoy_log' + file_suffix + '.log'):
            f_pep = open(decoy_path + 'reversed_decoy_log' + file_suffix + '.log', 'r')
            decoy_lines = f_pep.readlines()
            f_pep.close()
        else:
            decoy_lines = []
            fdr_output.append('reversed_decoy_log' + file_suffix + '.log file is missing from the decoy folder.  Please rerun Step 5. \n\n') 
                    
        for line in decoy_lines:
            line = line.strip()
            
            if ':' in line:
                line = line.split(':')
                line_key = str(line[0].strip())
                line_val = str(line[1].strip())
            
                if line_val[-1] == ',':
                    line_val = float(line_val[:-1])
                else:
                    line_val = float(line_val)
                
                if line_key == '"final_decoy_peptide_num"':
                    peptide_fdr = (line_val / float(num_true_glycopeptides)) * 100.0
                        
            if peptide_fdr != -1:
                break
    except:
        print '-'
        print 'Error with p7c_combining_expression_matrices_intensities.update_glyco_fdr_statistics()...'
        e = traceback.format_exc()
        print e
        print '-'
        sys.stdout.flush()
            
        print '\nAn error occurred while calculating the glycopeptide fdr.'
        sys.stdout.flush()
        
        fdr_output.append('Unexpected error: \n' + str(e) + '\n\n')
    
    fdr_output.append(fdr_name + ' fdr = ' + str(peptide_fdr) + ' %\n')  
    
    return fdr_output        

def generate_abundance_matrices_using_mean(folder_path, reference_channel, channels, num_channels, impute_missing = False):
    # uses mean for determining the reference intensity (Ref_i)
    print '\nGenerating MS2 abundance matrices (mean Ref_i, impute missing reference channel intensities = ' + str(impute_missing).lower() + ')...'
    print '-'
    sys.stdout.flush()
    
    # index column for each row in the expression matrices - should always be the first column
    protein_col = 0
    
    missing_flag = 'None'
    log2_file_endings = ['-log2_ratios', '-log2_ratios-MD_norm', '-log2_ratios-MD_norm_MAD_scaling']
    
    # get reference intensities (Ref_i) for each protein
    intensity_matrix_files = sorted(glob.glob(folder_path + '*-intensities.tsv'))
    for intensity_matrix_file in intensity_matrix_files:
        intensity_basename = os.path.basename(intensity_matrix_file)
        
        if 'phosphopeptide' in intensity_basename:
            first_intensity_col = 3
        elif 'glycoform-peptide' in intensity_basename:
            first_intensity_col = 2
        elif 'glycopeptide' in intensity_basename:
            first_intensity_col = 2
        elif 'peptide' in intensity_basename:
            first_intensity_col = 1
        elif 'phosphosite' in intensity_basename:
            first_intensity_col = 4
        elif 'glycoform-site' in intensity_basename:
            first_intensity_col = 2
        elif 'glycosite' in intensity_basename:
            first_intensity_col = 2
        elif 'site' in intensity_basename:
            first_intensity_col = 1
        elif 'phosphogene' in intensity_basename:
            first_intensity_col = 1
        elif 'gene' in intensity_basename:
            first_intensity_col = 1
        elif 'protein' in intensity_basename:
            first_intensity_col = 2
        else:
            raise Exception('intensity_begin_col could not be determined for the ' + intensity_basename + ' matrix')
        
        reference_index = channels.index(reference_channel)
        
        f = open(intensity_matrix_file, 'r')
        intensity_lines = f.readlines()
        f.close()
        
        # determine the minimum intensity for imputing missing reference channel intensities from the whole intensity matrix
        minimum_intensity = -1
        if impute_missing:
            for line in intensity_lines[header:]:
                line = line.strip().split('\t')
                intensities = line[first_intensity_col:]
                
                for intensity in intensities:
                    if intensity != missing_flag:
                        intensity_val = float(intensity)
                        if minimum_intensity == - 1:
                            minimum_intensity = intensity_val
                        elif intensity_val < minimum_intensity:
                            minimum_intensity = intensity_val
                            
            if minimum_intensity == -1:
                raise Exception('a global minimum intensity to use for imputing missing reference channel intensities could not be found')    
        
        reference_intensity_map = {}
        for line in intensity_lines[header:]:
            line = line.strip().split('\t')
            protein = line[protein_col].strip()
            intensities = line[first_intensity_col:]
            
            if protein in reference_intensity_map:
                raise Exception(protein + ' is in reference_intensity_map')
                
            reference_intensity_map[protein] = ''
            
            reference_intensities = []
            for index, intensity in enumerate(intensities):
                adjusted_index = index - reference_index
                if adjusted_index % num_channels == 0:
                    if intensity != missing_flag:
                        reference_intensities.append(float(intensity))
                    elif impute_missing:
                        reference_intensities.append(minimum_intensity)
                    
            mean_intensity = missing_flag
            mean_intensity_log2 = missing_flag
            if reference_intensities != []:
                mean_intensity = numpy.mean(reference_intensities)
                if mean_intensity > 1e-10:
                    mean_intensity_log2 = math.log(mean_intensity, 2)
                else:
                    mean_intensity = missing_flag
                    mean_intensity_log2 = missing_flag
                    
            reference_intensity_map[protein] = mean_intensity_log2
        
        # convert log2 ratios to abundances
        for log2_file_ending in log2_file_endings:  
            log2_matrix_file = folder_path + intensity_basename.replace('-intensities', log2_file_ending)
            log2_basename = os.path.basename(log2_matrix_file)
            
            abundance_matrix_file = folder_path + log2_basename.replace('-log2_ratios', '-abundances')
                      
            f = open(log2_matrix_file, 'r')
            log2_lines = f.readlines()
            f.close()
            
            abundance_data = []
            for line in log2_lines[header:]:
                line = line.strip().split('\t')
                output_line = list(line)
                protein = line[protein_col].strip()
                intensities = line[first_intensity_col:]
                
                if protein not in reference_intensity_map:
                    raise Exception(protein + ' is not in reference_intensity_map')
                    
                reference_intensity = reference_intensity_map[protein]
                
                for index, intensity in enumerate(intensities):
                    if intensity != missing_flag:
                        if reference_intensity != missing_flag:
                            output_line[index + first_intensity_col] = str(float(intensity) + reference_intensity)
                        else:
                            output_line[index + first_intensity_col] = missing_flag
                
                output_line.insert(first_intensity_col, str(reference_intensity))
                
                abundance_data.append(output_line)
                
            f = open(abundance_matrix_file, 'w')
            header_line = log2_lines[0].strip().split('\t')
            header_line.insert(first_intensity_col, 'Intensity.Reference')
            header_line = '\t'.join(header_line).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for row in abundance_data:
                output_line = '\t'.join(row).strip()
                
                f.write(str(output_line).strip() + '\n')
            
            f.close()
    
    print 'Success...'
    sys.stdout.flush()

def generate_abundance_matrices(folder_path, reference_channel, channels, num_channels, impute_missing = False):
    # uses median for determining the reference intensity (Ref_i)
    print '\nGenerating MS2 abundance matrices (median Ref_i, impute missing reference channel intensities = ' + str(impute_missing).lower() + ')...'
    print '-'
    sys.stdout.flush()
    
    # index column for each row in the expression matrices - should always be the first column
    protein_col = 0
    
    missing_flag = 'None'
    log2_file_endings = ['-log2_ratios', '-log2_ratios-MD_norm', '-log2_ratios-MD_norm_MAD_scaling']
    
    # get reference intensities (Ref_i) for each protein
    intensity_matrix_files = sorted(glob.glob(folder_path + '*-intensities.tsv'))
    for intensity_matrix_file in intensity_matrix_files:
        intensity_basename = os.path.basename(intensity_matrix_file)
        
        if 'phosphopeptide' in intensity_basename:
            first_intensity_col = 3
        elif 'glycoform-peptide' in intensity_basename:
            first_intensity_col = 2
        elif 'glycopeptide' in intensity_basename:
            first_intensity_col = 2
        elif 'peptide' in intensity_basename:
            first_intensity_col = 1
        elif 'phosphosite' in intensity_basename:
            first_intensity_col = 4
        elif 'glycoform-site' in intensity_basename:
            first_intensity_col = 2
        elif 'glycosite' in intensity_basename:
            first_intensity_col = 2
        elif 'site' in intensity_basename:
            first_intensity_col = 1
        elif 'phosphogene' in intensity_basename:
            first_intensity_col = 1
        elif 'gene' in intensity_basename:
            first_intensity_col = 1
        elif 'protein' in intensity_basename:
            first_intensity_col = 2
        else:
            raise Exception('intensity_begin_col could not be determined for the ' + intensity_basename + ' matrix')
        
        reference_index = channels.index(reference_channel)
        
        f = open(intensity_matrix_file, 'r')
        intensity_lines = f.readlines()
        f.close()
        
        # determine the minimum intensity for imputing missing reference channel intensities from the whole intensity matrix
        minimum_intensity = -1
        if impute_missing:
            for line in intensity_lines[header:]:
                line = line.strip().split('\t')
                intensities = line[first_intensity_col:]
                
                for intensity in intensities:
                    if intensity != missing_flag:
                        intensity_val = float(intensity)
                        if minimum_intensity == - 1:
                            minimum_intensity = intensity_val
                        elif intensity_val < minimum_intensity:
                            minimum_intensity = intensity_val
                            
            if minimum_intensity == -1:
                raise Exception('a global minimum intensity to use for imputing missing reference channel intensities could not be found')    
        
        reference_intensity_map = {}
        for line in intensity_lines[header:]:
            line = line.strip().split('\t')
            protein = line[protein_col].strip()
            intensities = line[first_intensity_col:]
            
            if protein in reference_intensity_map:
                raise Exception(protein + ' is in reference_intensity_map')
                
            reference_intensity_map[protein] = ''
            
            reference_intensities = []
            for index, intensity in enumerate(intensities):
                adjusted_index = index - reference_index
                if adjusted_index % num_channels == 0:
                    if intensity != missing_flag:
                        reference_intensities.append(float(intensity))
                    elif impute_missing:
                        reference_intensities.append(minimum_intensity)
                    
            median_intensity = missing_flag
            median_intensity_log2 = missing_flag
            if reference_intensities != []:
                median_intensity = numpy.median(reference_intensities)
                if median_intensity > 1e-10:
                    median_intensity_log2 = math.log(median_intensity, 2)
                else:
                    median_intensity = missing_flag
                    median_intensity_log2 = missing_flag
                    
            reference_intensity_map[protein] = median_intensity_log2
        
        # convert log2 ratios to abundances
        for log2_file_ending in log2_file_endings:  
            log2_matrix_file = folder_path + intensity_basename.replace('-intensities', log2_file_ending)
            log2_basename = os.path.basename(log2_matrix_file)
            
            abundance_matrix_file = folder_path + log2_basename.replace('-log2_ratios', '-abundances')
                      
            f = open(log2_matrix_file, 'r')
            log2_lines = f.readlines()
            f.close()
            
            abundance_data = []
            for line in log2_lines[header:]:
                line = line.strip().split('\t')
                output_line = list(line)
                protein = line[protein_col].strip()
                intensities = line[first_intensity_col:]
                
                if protein not in reference_intensity_map:
                    raise Exception(protein + ' is not in reference_intensity_map')
                    
                reference_intensity = reference_intensity_map[protein]
                
                for index, intensity in enumerate(intensities):
                    if intensity != missing_flag:
                        if reference_intensity != missing_flag:
                            output_line[index + first_intensity_col] = str(float(intensity) + reference_intensity)
                        else:
                            output_line[index + first_intensity_col] = missing_flag
                
                output_line.insert(first_intensity_col, str(reference_intensity))
                
                abundance_data.append(output_line)
                
            f = open(abundance_matrix_file, 'w')
            header_line = log2_lines[0].strip().split('\t')
            header_line.insert(first_intensity_col, 'Intensity.Reference')
            header_line = '\t'.join(header_line).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for row in abundance_data:
                output_line = '\t'.join(row).strip()
                
                f.write(str(output_line).strip() + '\n')
            
            f.close()
    
    print 'Success...'
    sys.stdout.flush()

def generate_expression_matrix_qc_distribution_plots(matrix_path, output_path, output_file, sample_filenames, num_channels): 
    # currently handles colors for up to 24 sets, but the colors are reused in cases of more sets
    color_names_original = ['palegreen', 'lightcoral', 'skyblue', 'peachpuff', 'darkorchid', 'chocolate', 'teal', 'violet', 'limegreen', 'tomato', 'wheat', 'plum', 'steelblue', 'mediumturquoise', 'coral', 'mediumseagreen', 'royalblue', 'navajowhite', 'blueviolet', 'sandybrown', 'greenyellow', 'peru', 'darkcyan', 'magenta']
    num_colors = len(color_names_original)
    
    # the individual sets are sorted this way when combining the matrices so the order of the axes labels will be correct
    sample_filenames_copy = list(sample_filenames)
    sample_filenames_copy.sort(key = natural_keys)
    
    matrix_files = glob.glob(matrix_path + '*.tsv')
    matrix_files.sort(key = natural_keys)
    for matrix_file in matrix_files:
        basename = os.path.basename(matrix_file).replace('.tsv', '')
        plot_title = basename
        
        # use 'linear' scale for abundance and log2 ratio matrices, and 'log' scale for intensity matrices
        log_linear = ''
        ylabel = ''
        intensity_begin_col = -1
        if 'abundances' in basename:
            log_linear = 'linear'
            ylabel = 'abundance'
            
            # abundance first intensity column is shifted +1 column due to the addition of the reference intensity column
            if 'phosphopeptide' in basename:
                intensity_begin_col = 4
            elif 'glycoform-peptide' in basename:
                intensity_begin_col = 3
            elif 'glycopeptide' in basename:
                intensity_begin_col = 3
            elif 'peptide' in basename:
                intensity_begin_col = 2
            elif 'phosphosite' in basename:
                intensity_begin_col = 5
            elif 'glycoform-site' in basename:
                intensity_begin_col = 3
            elif 'glycosite' in basename:
                intensity_begin_col = 3
            elif 'site' in basename:
                intensity_begin_col = 2
            elif 'phosphogene' in basename:
                intensity_begin_col = 2
            elif 'gene' in basename:
                intensity_begin_col = 2
            elif 'protein' in basename:
                intensity_begin_col = 3
            else:
                raise Exception('intensity_begin_col could not be determined for the ' + basename + ' matrix') 
        elif 'log2_ratios' in basename:
            log_linear = 'linear'
            ylabel = 'log2 ratio'
            
            if 'phosphopeptide' in basename:
                intensity_begin_col = 3
            elif 'glycoform-peptide' in basename:
                intensity_begin_col = 2
            elif 'glycopeptide' in basename:
                intensity_begin_col = 2
            elif 'peptide' in basename:
                intensity_begin_col = 1
            elif 'phosphosite' in basename:
                intensity_begin_col = 4
            elif 'glycoform-site' in basename:
                intensity_begin_col = 2
            elif 'glycosite' in basename:
                intensity_begin_col = 2
            elif 'site' in basename:
                intensity_begin_col = 1
            elif 'phosphogene' in basename:
                intensity_begin_col = 1
            elif 'gene' in basename:
                intensity_begin_col = 1
            elif 'protein' in basename:
                intensity_begin_col = 2
            else:
                raise Exception('intensity_begin_col could not be determined for the ' + basename + ' matrix') 
        elif 'intensities' in basename:
            log_linear = 'log'
            ylabel = 'intensity'
            
            if 'phosphopeptide' in basename:
                intensity_begin_col = 3
            elif 'glycoform-peptide' in basename:
                intensity_begin_col = 2
            elif 'glycopeptide' in basename:
                intensity_begin_col = 2
            elif 'peptide' in basename:
                intensity_begin_col = 1
            elif 'phosphosite' in basename:
                intensity_begin_col = 4
            elif 'glycoform-site' in basename:
                intensity_begin_col = 2
            elif 'glycosite' in basename:
                intensity_begin_col = 2
            elif 'site' in basename:
                intensity_begin_col = 1
            elif 'phosphogene' in basename:
                intensity_begin_col = 1
            elif 'gene' in basename:
                intensity_begin_col = 1
            elif 'protein' in basename:
                intensity_begin_col = 2
            else:
                raise Exception('intensity_begin_col could not be determined for the ' + basename + ' matrix') 
        else:
            raise Exception(basename + ' is not an abundance/intensity/log2 ratio matrix')
        
        if intensity_begin_col == -1:
            raise Exception('intensity_begin_col could not be determined for the ' + basename + ' matrix') 
        
        f = open(matrix_file, 'r')
        lines = f.readlines()
        f.close()
        
        header_data = lines[0].strip().split('\t')[intensity_begin_col:]
        num_intensity_cols = len(header_data)
        num_sets = int(math.ceil(float(num_intensity_cols) / float(num_channels)))
        colors_multiple_factor = int(math.ceil(float(num_sets) / float(num_colors)))
        
        color_names = color_names_original * colors_multiple_factor
        
        header_data = '\t'.join(header_data).strip()
        
        output_data = [[] for x in range(num_intensity_cols)]
        max_intensity = -1
        for line in lines[header:]:
            line = line.strip().split('\t')
            intensities = line[intensity_begin_col:]
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                
                if intensity != 'None':
                    intensity = float(intensity)
                    
                    output_data[i].append(intensity)
                    
                    if intensity > max_intensity:
                        max_intensity = intensity
        
        # minimum channel median
        min_median = 2 * max_intensity
        min_percentile_25 = 2 * max_intensity
        max_percentile_75 = -1
        for channel_intensities in output_data:
            channel_median = numpy.median(channel_intensities)
            channel_percentile_25 = numpy.percentile(channel_intensities, 25)
            channel_percentile_75 = numpy.percentile(channel_intensities, 75)
            
            if channel_median < min_median:
                min_median = channel_median
                
            if channel_percentile_25 < min_percentile_25:
                min_percentile_25 = channel_percentile_25
                
            if channel_percentile_75 > max_percentile_75:
                max_percentile_75 = channel_percentile_75
        
        # threshold values to determine if the y-axis minimum plot range needs to be adjusted
        #min_median_threshold = 0.1 * float(min_median)
        min_percentile_25_threshold = 0.333 * float(min_percentile_25)
        max_percentile_75_threshold = 3.0 * float(max_percentile_75)
        
        #labels = ['Set ' + str(((x - 1) / num_channels) + 1) for x in range(1, num_intensity_cols + 1, num_channels)]
        #labels = sample_filenames_copy
        # do not label xticks for now
        labels = []
        boxprops = dict(linestyle = '-', linewidth = 0.5)
        medianprops = dict(linestyle = '-.', linewidth = 1.5, color = 'k') 
        
        rotation_deg = 0
        if len(labels) == 1:
            rotation_deg = 45
        else:
            rotation_deg = 45
                
        figure = plt.figure(figsize=(11, 8.5), dpi=72)  
        ax = plt.gca()
        ax.yaxis.grid(True)
        ax.xaxis.grid(True, linestyle = '--')
        #ax.set_facecolor([0.98, 0.98, 0.98])
        bplot = plt.boxplot(output_data, showcaps = True, showfliers = False, notch = False, boxprops = boxprops, medianprops = medianprops, patch_artist = True)
        plt.title(plot_title)
        plt.xlabel('sample')
        plt.ylabel(ylabel)     
        plt.xticks(numpy.arange(1, num_intensity_cols + 1, num_channels), labels, rotation = rotation_deg, fontsize = '10')
        plt.xlim(0, num_intensity_cols + 1)
        plt.yscale(log_linear)
        
        caps = bplot['caps']
        
        min_cap = 2 * max_intensity
        max_cap = -1
        for i in range(0, len(caps), 2):
            capbottom = caps[i].get_ydata()[0]
            captop = caps[i+1].get_ydata()[0]
            
            if capbottom < min_cap:
                min_cap = capbottom
                
            if captop > max_cap:
                max_cap = captop
        
        ymin = -1
        ymax = -1
        if log_linear == 'log':
            # use whisker rounding to nearest integer multiple of power of 10 for ymin and ymax
            roundMin = 10**math.floor(math.log10(min_cap))
            roundMax = 10**math.floor(math.log10(max_cap))
            ymin = int(math.floor(min_cap / roundMin)) * roundMin
            ymax = int(math.ceil(max_cap / roundMax)) * roundMax
            
            # adjust the y-axis minimum and maximum if the caps are too long
            if ymin < min_percentile_25_threshold:
                roundMin = 10**math.floor(math.log10(min_percentile_25_threshold))
                ymin = int(math.floor(min_percentile_25_threshold / roundMin)) * roundMin
                
            if ymax > max_percentile_75_threshold:
                roundMax = 10**math.floor(math.log10(max_percentile_75_threshold))
                ymax = int(math.ceil(max_percentile_75_threshold / roundMax)) * roundMax
        elif log_linear == 'linear':
            # use whisker rounding to nearest integer multiple of 1 for ymin and ymax
            ymin = int(math.floor(float(min_cap) / 1.0)) * 1.0
            ymax = int(math.ceil(float(max_cap) / 1.0)) * 1.0
        else:
            raise Exception("log_linear must be either 'log' or 'linear'")
        
        plt.ylim(ymin, ymax)
        
        # fill with colors
        colors = []
        for color_name in color_names:
            for q in range(num_channels):
                colors.append(color_name)
        
        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)
    
        plt.tight_layout()
        #plt.show()
        #plt.savefig(output_path + output_file)
        #plt.close('all')
        
    pdf = matplotlib.backends.backend_pdf.PdfPages(output_path + output_file)
    for fig in xrange(1, figure.number + 1):
        pdf.savefig(fig)
    pdf.close()            
    plt.close('all')

def label_matrices_with_virtual_reference(matrices_all, protein_end, peptide_end, site_end, sample_filenames, output_path, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier): 
    filename_dash = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        filename_dash = '-'
        
    protein_matrices = []
    peptide_matrices = []
    site_matrices = []
    for matrix in matrices_all:
        matrix_basename = os.path.basename(matrix).replace('.tsv', '')
        name_end = matrix_basename.split('.')
        name_end = '.'.join(name_end[-2:])
        
        if protein_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + protein_end)[0]
            
            if matrix_sample_name in sample_filenames:
                protein_matrices.append(matrix)
                
        if peptide_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + peptide_end)[0]
            
            if matrix_sample_name in sample_filenames:
                peptide_matrices.append(matrix)
        
        if site_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + site_end)[0]
            
            if matrix_sample_name in sample_filenames:
                site_matrices.append(matrix)
    
    if protein_matrices != []:
        # protein matrix
        print 'Combining ' + filename_mod + filename_dash + 'protein expression matrices into a final ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'protein_matrix-intensities.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = protein_matrices, protein_flag = True, peptide_flag = False, site_flag = False, file_end = protein_end, mod = mod, localize_phosphosites = localize_phosphosites)
        normalize_combined_matrix(output_file, output_filepath = output_path + filename_mod + filename_dash + 'protein_matrix-intensities_normalized.tsv', protein_flag = True, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
        # generate gene-level matrices
        # column with the protein names in the protein matrix
        protein_col = 1
        first_intensity_col = 2
        
        gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
        norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
        generate_gene_level_matrices(output_file, gene_output_file, gene_name_identifier, header, protein_col, first_intensity_col)
        if os.path.exists(gene_output_file):
            normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
    if peptide_matrices != []:
        # peptides matrix
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
    
        output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-intensities.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = peptide_matrices, protein_flag = False, peptide_flag = True, site_flag = False, file_end = peptide_end, mod = mod, localize_phosphosites = localize_phosphosites)
        normalize_combined_matrix(output_file, output_filepath = output_path + filename_mod + filename_dash + 'peptide_matrix-intensities_normalized.tsv', protein_flag = False, peptide_flag = True, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
        if mod == 'phospho' and localize_phosphosites == 'true':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_intensity_col = 3
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
            norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
            generate_gene_level_phospho_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col)
            if os.path.exists(gene_output_file):
                normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)

        if mod == 'glyco':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_intensity_col = 2
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
            norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
            generate_gene_level_speg_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col)
            if os.path.exists(gene_output_file):
                normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
    if site_matrices != []:
        # sites matrix
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'site_matrix-intensities.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = site_matrices, protein_flag = False, peptide_flag = False, site_flag = True, file_end = site_end, mod = mod, localize_phosphosites = localize_phosphosites)
        normalize_combined_matrix(output_file, output_filepath = output_path + filename_mod + filename_dash + 'site_matrix-intensities_normalized.tsv', protein_flag = False, peptide_flag = False, site_flag = True, mod = mod, localize_phosphosites = localize_phosphosites)
    
        # old method
        #if mod == 'phospho' and localize_phosphosites == 'true':
        #    # generate gene-level matrices
        #    # data_file columns
        #    phosphosite_index_col = 0
        #    gene_col = 1
        #    first_intensity_col = 4
        #    
        #    gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
        #    norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
        #    generate_gene_level_phospho_matrices_from_phosphosites(output_file, gene_output_file, header, phosphosite_index_col, gene_col, first_intensity_col)
        #    if os.path.exists(gene_output_file):
        #        normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
    expression_sets_filename = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        expression_sets_filename = filename_mod + filename_dash + 'expression_matrix_sets-intensities.log'
    else:
        expression_sets_filename = 'expression_matrix_sets-intensities.log'
        
    f = open(output_path + expression_sets_filename, 'w')
    
    f.write('Protein matrix set files included in the combined protein intensity matrices -\n')
    for protein_matrix in protein_matrices:
        f.write('\t' + str(os.path.basename(protein_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Peptide matrix set files included in the combined peptide intensity matrices -\n')
    for peptide_matrix in peptide_matrices:
        f.write('\t' + str(os.path.basename(peptide_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Site matrix set files included in the combined site intensity matrices -\n')
    for site_matrix in site_matrices:
        f.write('\t' + str(os.path.basename(site_matrix).replace('.tsv', '')) + '\n')
    
    f.close()
    
    # quality control statistics for the number of sets that were included in the final expression matrices
    #f = open(qc_path + 'stats-number_log_expression_matrix-' + data_set_name + '.tsv', 'w')
    #stats_line = '# sets quantified'
    #
    #f.write(str(stats_line).strip() + '\n')
    #
    ## peptide matrices are generated for all different types of data
    #stats_line = str(len(peptide_matrices))
    #
    #f.write(str(stats_line).strip() + '\n')
    #
    #f.close()

def label_matrices(matrices_all, protein_end, peptide_end, site_end, sample_filenames, output_path, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier): 
    filename_dash = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        filename_dash = '-'
        
    protein_matrices = []
    peptide_matrices = []
    site_matrices = []
    for matrix in matrices_all:
        matrix_basename = os.path.basename(matrix).replace('.tsv', '')
        name_end = matrix_basename.split('.')
        name_end = '.'.join(name_end[-2:])
        
        if protein_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + protein_end)[0]
            
            if matrix_sample_name in sample_filenames:
                protein_matrices.append(matrix)
                
        if peptide_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + peptide_end)[0]
            
            if matrix_sample_name in sample_filenames:
                peptide_matrices.append(matrix)
        
        if site_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + site_end)[0]
            
            if matrix_sample_name in sample_filenames:
                site_matrices.append(matrix)
    
    if protein_matrices != []:
        # protein matrix
        print 'Combining ' + filename_mod + filename_dash + 'protein expression matrices into a final ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'protein_matrix-intensities.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = protein_matrices, protein_flag = True, peptide_flag = False, site_flag = False, file_end = protein_end, mod = mod, localize_phosphosites = localize_phosphosites)
        normalize_combined_matrix(output_file, output_filepath = output_path + filename_mod + filename_dash + 'protein_matrix-intensities_normalized.tsv', protein_flag = True, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
        # generate gene-level matrices
        # column with the protein names in the protein matrix
        protein_col = 1
        first_intensity_col = 2
        
        gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
        norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
        generate_gene_level_matrices(output_file, gene_output_file, gene_name_identifier, header, protein_col, first_intensity_col)
        if os.path.exists(gene_output_file):
            normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
    if peptide_matrices != []:
        # peptides matrix
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
    
        output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-intensities.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = peptide_matrices, protein_flag = False, peptide_flag = True, site_flag = False, file_end = peptide_end, mod = mod, localize_phosphosites = localize_phosphosites)
        normalize_combined_matrix(output_file, output_filepath = output_path + filename_mod + filename_dash + 'peptide_matrix-intensities_normalized.tsv', protein_flag = False, peptide_flag = True, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
        if mod == 'phospho' and localize_phosphosites == 'true':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_intensity_col = 3
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
            norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
            generate_gene_level_phospho_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col)
            if os.path.exists(gene_output_file):
                normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)

        if mod == 'glyco':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_intensity_col = 2
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
            norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
            generate_gene_level_speg_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col)
            if os.path.exists(gene_output_file):
                normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
    if site_matrices != []:
        # sites matrix
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'site_matrix-intensities.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = site_matrices, protein_flag = False, peptide_flag = False, site_flag = True, file_end = site_end, mod = mod, localize_phosphosites = localize_phosphosites)
        normalize_combined_matrix(output_file, output_filepath = output_path + filename_mod + filename_dash + 'site_matrix-intensities_normalized.tsv', protein_flag = False, peptide_flag = False, site_flag = True, mod = mod, localize_phosphosites = localize_phosphosites)
    
        # old method
        #if mod == 'phospho' and localize_phosphosites == 'true':
        #    # generate gene-level matrices
        #    # data_file columns
        #    phosphosite_index_col = 0
        #    gene_col = 1
        #    first_intensity_col = 4
        #    
        #    gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities.tsv'
        #    norm_gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-intensities_normalized.tsv'
        #    generate_gene_level_phospho_matrices_from_phosphosites(output_file, gene_output_file, header, phosphosite_index_col, gene_col, first_intensity_col)
        #    if os.path.exists(gene_output_file):
        #        normalize_combined_matrix(gene_output_file, output_filepath = norm_gene_output_file, protein_flag = False, peptide_flag = False, site_flag = False, mod = mod, localize_phosphosites = localize_phosphosites)
    
    expression_sets_filename = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        expression_sets_filename = filename_mod + filename_dash + 'expression_matrix_sets-intensities.log'
    else:
        expression_sets_filename = 'expression_matrix_sets-intensities.log'
        
    f = open(output_path + expression_sets_filename, 'w')
    
    f.write('Protein matrix set files included in the combined protein intensity matrices -\n')
    for protein_matrix in protein_matrices:
        f.write('\t' + str(os.path.basename(protein_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Peptide matrix set files included in the combined peptide intensity matrices -\n')
    for peptide_matrix in peptide_matrices:
        f.write('\t' + str(os.path.basename(peptide_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Site matrix set files included in the combined site intensity matrices -\n')
    for site_matrix in site_matrices:
        f.write('\t' + str(os.path.basename(site_matrix).replace('.tsv', '')) + '\n')
    
    f.close()
    
    # quality control statistics for the number of sets that were included in the final expression matrices
    f = open(qc_path + 'stats-number_log_expression_matrix-' + data_set_name + '.tsv', 'w')
    stats_line = '# sets quantified'
    
    f.write(str(stats_line).strip() + '\n')
    
    # peptide matrices are generated for all different types of data
    stats_line = str(len(peptide_matrices))
    
    f.write(str(stats_line).strip() + '\n')
    
    f.close()

def generate_gene_level_speg_matrices(data_file, gene_output_file, header, gene_col, first_intensity_col):
    # generate the log2 gene matrix
    print '\nRolling up the glycopeptide matrix to glycogene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            gene = line[gene_col].strip()
            intensities = line[first_intensity_col:]
            
            gene_index = ''
            if gene != '-':
                gene_index = gene
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            if not gene_name_found_flag:
                if gene_index != '':
                    gene_name_found_flag = True
                    
            if gene_index != '':
                if gene_index not in gene_map:
                    gene_map[gene_index] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[gene_index][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.sum(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene.Index'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush()
        else:
            print 'Glyco gene indices could not be generated from the gene names...'
            sys.stdout.flush()

def generate_gene_level_phospho_matrices(data_file, gene_output_file, header, gene_col, first_intensity_col):
    # generate the log2 gene matrix
    print '\nRolling up the phosphopeptide matrix to phosphogene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            gene = line[gene_col].strip()
            intensities = line[first_intensity_col:]
            
            gene_index = ''
            if gene != '-':
                gene_index = gene
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            if not gene_name_found_flag:
                if gene_index != '':
                    gene_name_found_flag = True
                    
            if gene_index != '':
                if gene_index not in gene_map:
                    gene_map[gene_index] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[gene_index][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.sum(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene.Index'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush()
        else:
            print 'Phospho gene indices could not be generated from the gene names...'
            sys.stdout.flush()

# old method
def generate_gene_level_phospho_matrices_from_phosphosites(data_file, gene_output_file, header, phosphosite_index_col, gene_col, first_intensity_col):
    # generate the log2 gene matrix
    print '\nRolling up the phosphosite matrix to phosphogene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            phosphosite_index = line[phosphosite_index_col].strip()
            gene = line[gene_col].strip()
            intensities = line[first_intensity_col:]
            
            site_position = phosphosite_index.split('_')[-1].strip()
            gene_index = ''
            if site_position != '0' and gene != '-':
                gene_index = gene + '_' + site_position
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            if not gene_name_found_flag:
                if gene_index != '':
                    gene_name_found_flag = True
                    
            if gene_index != '':
                if gene_index not in gene_map:
                    gene_map[gene_index] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[gene_index][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.sum(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene.Index'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush()
        else:
            print 'Phospho gene indices could not be combined from the gene names and site positions...'
            sys.stdout.flush()
            
def generate_gene_level_matrices(data_file, gene_output_file, gene_name_identifier, header, protein_col, first_intensity_col):
    # generate the log2 gene matrix
    print '\nRolling up the protein matrix to gene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            proteins = line[protein_col].strip().split(';')
            intensities = line[first_intensity_col:]
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            current_genes = []
            for protein in proteins:
                if gene_name_identifier in protein:
                    gene = protein.split(gene_name_identifier)[1].strip().split(' ')[0].strip()
                    if gene != '' and gene not in current_genes:
                        current_genes.append(gene)
                
            if not gene_name_found_flag:
                if current_genes != []:
                    gene_name_found_flag = True
            
            current_genes = sorted(current_genes)       
            current_genes = ';'.join(current_genes)
              
            if current_genes != '':      
                if current_genes not in gene_map:
                    gene_map[current_genes] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[current_genes][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.sum(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
                    
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush() 
        else:
            print 'Gene names not found in the protein names...'
            sys.stdout.flush()

# normalization factors in each channel = max(summed intensity in each channel) / (summed intensity in that channel)  
def normalize_combined_matrix(matrix_filepath, output_filepath, protein_flag, peptide_flag, site_flag, mod, localize_phosphosites):
    # if combining protein matrices then the starting column with the ratio data is column 2, otherwise it is column 1 (e.g. glycosites, glycopeptides)
    if protein_flag:
        data_column_min = 2
    else:
        if mod == 'phospho' and localize_phosphosites == 'true':
            # the phosphosite matrices have an additional column compared to the phosphopeptide matrices
            if site_flag:
                data_column_min = 4
            elif peptide_flag:
                data_column_min = 3
            else:
                # handles gene-level data
                data_column_min = 1
        elif mod == 'intact':
            data_column_min = 2
        elif mod == 'glyco':
            if peptide_flag or site_flag:
                data_column_min = 2
            else:
                # handles gene-level data
                data_column_min = 1
        else:
            # this handles gene-level data as well
            data_column_min = 1
    
    f = open(matrix_filepath, 'r')
    lines = f.readlines()
    f.close()
    
    header_lines = 1
    total_data_columns = len(lines[header_lines].strip().split('\t')[data_column_min:])
    
    norm_factors = [0] * total_data_columns
    
    for line in lines[header_lines:]:
        data = line.strip().split('\t')[data_column_min:]
        
        for i, col in enumerate(data):
            if col != 'None':
                intensity = float(col)
                
                norm_factors[i] += intensity
            
    max_intensity = numpy.max(norm_factors)
    
    for i, norm in enumerate(norm_factors):
        if norm > 0:
            norm_factors[i] = float(max_intensity) / float(norm)
        else:
            norm_factors[i] = None
        
    output_data = [str(lines[header_lines - 1]).strip()]
        
    for line in lines[header_lines:]:
        new_line = ''
        
        data = line.strip().split('\t')
        
        new_line = ''
        for x in range(data_column_min):
            new_line += str(data[x]) + '\t'
        
        for i, col in enumerate(data[data_column_min:]):
            if col != 'None' and norm_factors[i] != None:
                norm_col = float(col) * float(norm_factors[i])
            else:
                norm_col = 'None'
                
            new_line += str(norm_col) + '\t'
            
        output_data.append(new_line.strip())    
    
    outfile = open(output_filepath, 'w')
    for row in output_data:
        outfile.write(str(row).strip() + '\n')

    outfile.close()

def combine_expression_matrices(header_section, header_data, num_channels, output_file, matrices, protein_flag, peptide_flag, site_flag, file_end, mod, localize_phosphosites):
    # if combining protein matrices then the starting column with the ratio data is column 2, otherwise it is column 1 (e.g. glycosites, glycopeptides)
    if protein_flag:
        first_ratio_col = 2
    else:
        if mod == 'phospho' and localize_phosphosites == 'true':
            # the phosphosite matrices have an additional column compared to the phosphopeptide matrices
            if site_flag:
                first_ratio_col = 4
            else:
                first_ratio_col = 3
        elif mod == 'intact':
            first_ratio_col = 2
        elif mod == 'glyco':
            if peptide_flag or site_flag:
                first_ratio_col = 2
            else:
                # hadles gene-level data
                first_ratio_col = 1
        else:
            first_ratio_col = 1
        
    header_sections = ['iTRAQ4plex', 'iTRAQ8plex', 'TMT10plex', 'TMT11plex', 'TMT16plex']
    
    header_dict = {}
    header_flag = False
    specified_names = []
    # get sample names to be used as the header for the final matrices
    for line in header_data:
        line = line.strip()
        split_line = line.split('\t')
        
        if line == header_section:
            header_flag = True
            continue
            
        if header_flag == True and (line == '' or line in header_sections):
            header_flag = False
            break
        
        if header_flag == True:
            # skip header row
            if split_line[0].strip() == 'Filename':
                continue
            else:
                filename = split_line[0].strip().replace('.txt', '').replace('.tsv', '')
                
                header_dict[filename] = []
                for name in split_line[1:]:
                    header_dict[filename].append(name) 
                    
                    # make sure that the sample names are unique
                    if name in specified_names:
                       raise Exception('sample names in header_expression_matrix.tsv were not unique')
                       
                    specified_names.append(name)   
         
    combined_matrix = {}
    combined_matrix['header'] = []
    
    matrix_count = -1
    files_len = len(matrices)
    
    percent_interval = math.floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0

    percent_checked = 0.0
    num_files_checked = 0

    print 'Reading ' + file_end + ' expression matrices progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for matrix in matrices:
        num_files_checked += 1
        
        filename = os.path.basename(matrix).replace('.tsv', '')
        for mod_ending in mod_endings:
            filename = filename.replace('.' + mod_ending, '')
    
        matrix_count += 1
        
        f = open(matrix, 'r')
        lines = f.readlines()
        f.close()
        
        header_line = lines[0].strip().split('\t')
        
        if matrix_count == 0:
            combined_matrix['header'].append(header_line[0])
            
            if protein_flag:
                combined_matrix['header'].append(header_line[1])
            
            if mod == 'phospho' and localize_phosphosites == 'true':
                combined_matrix['header'].append(header_line[1])
                combined_matrix['header'].append(header_line[2])
                if site_flag:
                    combined_matrix['header'].append(header_line[3])
                    
            if mod == 'intact':
                combined_matrix['header'].append(header_line[1])
                
            if mod == 'glyco' and (peptide_flag or site_flag):
                combined_matrix['header'].append(header_line[1])
            
            if filename in header_dict:
                for header_value in header_dict[filename]:
                    combined_matrix['header'].append(header_value)
            else:
                print filename + ' was missing from the header_expression_matrix.tsv file. Using default header.'
                sys.stdout.flush()
                
                for header_value in header_line[first_ratio_col:]:
                    combined_matrix['header'].append(header_value + ' (' + filename + ')')
        else:
            if filename in header_dict:
                for header_value in header_dict[filename]:
                    combined_matrix['header'].append(header_value)
            else:
                print filename + ' was missing from the header_expression_matrix.tsv file. Using default header.'
                sys.stdout.flush()
                
                for header_value in header_line[first_ratio_col:]:
                    combined_matrix['header'].append(header_value + ' (' + filename + ')')
    
        for line in lines[header:]:
            line = line.strip()
            split_line = line.split('\t')
            
            for i, intensity_val in enumerate(split_line[first_ratio_col:]):
                if intensity_val != 'None':
                    intensity_val = float(intensity_val)
                    
                if intensity_val == 0:
                    split_line[i + first_ratio_col] = 'None'

            protein = split_line[0]
            if protein not in combined_matrix:
                # puts in protein names column
                combined_matrix[protein] = []
                
                if protein_flag:
                    combined_matrix[protein].append(split_line[1])
                    
                if mod == 'phospho' and localize_phosphosites == 'true':
                    combined_matrix[protein].append(split_line[1])
                    combined_matrix[protein].append(split_line[2])
                    if site_flag:
                        combined_matrix[protein].append(split_line[3])
                        
                if mod == 'intact':
                    combined_matrix[protein].append(split_line[1])
                    
                if mod == 'glyco' and (peptide_flag or site_flag):
                    combined_matrix[protein].append(split_line[1])
                
                # fills in 0 values for previous expression matrices
                for i in range(num_channels * matrix_count):
                    combined_matrix[protein].append('None')
                    
                for ratio in split_line[first_ratio_col:]:
                    combined_matrix[protein].append(ratio)
            else:
                for ratio in split_line[first_ratio_col:]:
                    combined_matrix[protein].append(ratio)
                    
        
        # total number of entries that each dictionary key should currently have in its list    
        num_ratios = (matrix_count + 1) * num_channels + (first_ratio_col - 1)
        
        for key in combined_matrix:
            current_num_ratios = len(combined_matrix[key])
            ratio_diff = num_ratios - current_num_ratios
                
            if ratio_diff > 0:
                if ratio_diff % num_channels != 0:
                    print 'Error! Data is corrupted.  Ratio columns were not appended correctly.'
                else:
                    for i in range(ratio_diff):
                        combined_matrix[key].append('None')
                        
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = math.floor(current_percent)
        
            print 'Reading ' + file_end + ' expression matrices progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()
            
    if percent_checked < 100.0:
        print 'Reading ' + file_end + ' expression matrices progress... 100 %'
        sys.stdout.flush()
    
    f = open(output_file, 'w')
    
    output_line = ''
    for header_col in combined_matrix['header']:
        output_line += str(header_col) + '\t'
        
    f.write(output_line.strip() + '\n')
    
    for key in combined_matrix:
        if key != 'header':
            output_line = str(key)
            
            for value in combined_matrix[key]:
                output_line += '\t' + str(value)
                
            f.write(output_line + '\n')
        
    f.close()  

def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]
    
if __name__ == '__main__':
    # header_expression_matrix data
    f = open(config_path + 'header_expression_matrix.tsv', 'r')
    header_expression_matrix_data = f.readlines()
    f.close()
    
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['header_expression_matrix'] = header_expression_matrix_data
    configs['backup'] = True
    
    main(configs)