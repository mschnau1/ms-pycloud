import os
import sys
import glob
import time
import shutil
import subprocess
import tkMessageBox
from Tkinter import *
from hotpot.pathing import make_path
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'

def main(configs):
    print '\nStep 3 continued:\tConverting pepXML database search results to mzID format...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    root = Tk().withdraw()
    
    options = configs['options']
    exes = configs['exes']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    pepxml_path = data_root + 'pepxml\\'
    mzid_path = data_root + 'mzid\\'
    search_results_path = data_root + 'step3-search_results\\'
    idconvert_file = exes['idconvert']
        
    # creates the output path folders
    make_path(mzid_path)
    
    pepxml_files = glob.glob(pepxml_path + '*.pepxml')
    
    if pepxml_files == []:
        tkMessageBox.showinfo('Error!', 'Please move the pepxml files to: \n' + pepxml_path)
    else:
        tkMessageBox.showinfo('Note!', 'Result mzid files will be placed in: \n' + mzid_path + '\n-\nTo use the mzid files for Steps 4 - 7, please put only the mzid files in: \n' + search_results_path)
        
        for pepxml_file in pepxml_files:
            filename = str(pepxml_file).split('\\')[-1]
            filename = filename[:-7] + '.mzid'
            if not os.path.exists(mzid_path + filename):
                subprocess.call(idconvert_file + ' ' + pepxml_file + ' -o ' + mzid_path)
      
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['backup'] = True
    
    main(configs)