A sample fasta file and gpquest databases have been provided in the "sample_databases" folder.  They can be selected
using the MS-PyCloud GUI.

fasta : swissprot_human_20180529.fasta

gpquest databases :
	protein : human_npeps.xlsx
	nglycan : nglycans.xlsx
	oglycan : oglycans.xlsx