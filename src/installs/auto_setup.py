import os
import sys
import glob
import subprocess
import tkMessageBox
from Tkinter import *

parent_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(parent_path)
import i0d_install_python as i0d
import i0b_find_executables as i0b

pipeline_path = parent_path + '\\'
install_path = pipeline_path + 'installs\\'
programs_path = install_path + 'windows\\'
# determines the path to store the executable paths list based on where the steps config file is
exe_path = pipeline_path + 'configs\\executables.txt'

# names of the executable files
file_7z = '7z.exe'
file_msconvert = 'msconvert.exe'
file_idconvert = 'idconvert.exe'
file_java = 'java.exe'
file_putty = 'putty.exe'
file_python = 'python.exe'
file_python3 = 'python3.exe'
file_pip = 'pip.exe'
file_pip3 = 'pip3.exe'
name_dict = {file_7z : '7zip', file_msconvert : 'msconvert', file_idconvert: 'idconvert', file_java : 'java', file_putty : 'putty', file_python : 'python', file_python3 : 'python3', file_pip : 'pip', file_pip3 : 'pip3'}

install_path_dict = {}
install_path_dict['7z920-x64.msi'] = 'C:\\Program Files\\7-Zip\\' + file_7z
#install_path_dict['jre-7u79-windows-x64.exe'] = 'C:\\Program Files\\Java\\jre7\\bin\\' + file_java
# java jre from amazon corretto 8 is included with pycloud 
install_path_dict['jre-7u79-windows-x64.exe'] = pipeline_path + 'java\\jre8\\bin\\' + file_java
install_path_dict['putty-0.64-installer.exe'] = 'C:\\Program Files (x86)\\PuTTY\\' + file_putty
install_path_dict['pwiz-setup-3.0.8642-x86_64.msi'] = 'C:\\Program Files\\ProteoWizard\\ProteoWizard 3.0.8642\\' + file_msconvert

# silent exe installs without user prompts
exe_args = {}
exe_args['putty-0.64-installer.exe'] = '/silent'
exe_args['jre-7u79-windows-x64.exe'] = '/s'
exe_args['visualcppbuildtools_full.exe'] = '/passive'

install_dict = {}
install_dict['7z920-x64.msi'] = '7-Zip'
install_dict['jre-7u79-windows-x64.exe'] = 'Java'
install_dict['putty-0.64-installer.exe'] = 'PuTTY'
install_dict['pwiz-setup-3.0.8642-x86_64.msi'] = 'ProteoWizard'
install_dict['VCForPython27.msi'] = 'Visual C++ Compiler for Python27'
install_dict['visualcppbuildtools_full.exe'] = 'Visual C++ Compiler for Python3'

batch_files = ['MS-PyCloud.bat', 'batch-manual_terminate_cluster.bat', 'batch-manual_cloud_retrieval.bat', 'batch-manual_terminate_PuTTY.bat', 'batch-manual_terminate_volumecreator.bat']
batch_files = [pipeline_path + entry for entry in batch_files]

def main():
    try:
        python_path = i0d.main()
        write_executables_config(python_path)
        exes = read_executables_config()
            
        #root = Tk()
        #root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        #root.update_idletasks()
        #tkMessageBox.showinfo('Alert!', 'Java, 7-Zip, ProteoWizard, and PuTTY will be installed.  You may skip the installations for software that is already installed on your computer.')
        #root.destroy()
        
        exes_to_find = []
        
        msi_files = glob.glob(programs_path + '*.msi')
        exe_files = glob.glob(programs_path + '*.exe')
        
        for msi in msi_files:
            msi_basename = os.path.basename(msi)
            
            try:
                os.popen('msiexec /passive /i ' + msi)
                
                # Visual C++ compiler does not have a specific path to install to
                if msi_basename != 'VCForPython27.msi':
                    installed_path = install_path_dict[msi_basename]
                    install_basename = os.path.basename(installed_path)
                    if not os.path.exists(installed_path):
                        exes_to_find.append(install_basename)
                    else:
                        exes[install_basename] = installed_path
            except Exception as e:
                root = Tk()
                root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
                root.update_idletasks()
                tkMessageBox.showinfo('Error!', 'One of the install files was not able to be run by Python.  Please double click the following .msi file to manually install it:\n\n' + str(msi))
                root.destroy()
                
        for exe in exe_files:
            exe_basename = os.path.basename(exe)
            
            if exe_args[exe_basename] != '':
                exe_args[exe_basename] = ' ' + exe_args[exe_basename]
            
            try:
                os.popen(exe + exe_args[exe_basename])   
                
                # Visual C++ compiler does not have a specific path to install to
                if exe_basename != 'visualcppbuildtools_full.exe':
                    installed_path = install_path_dict[exe_basename]
                    install_basename = os.path.basename(installed_path)
                    if not os.path.exists(installed_path):
                        exes_to_find.append(install_basename)
                    else:
                        exes[install_basename] = installed_path
            except Exception as e:
                root = Tk()
                root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
                root.update_idletasks()
                tkMessageBox.showinfo('Error!', 'One of the install files was not able to be run by Python.  Please double click the following .exe file to manually install it:\n\n' + str(exe))
                root.destroy()
    
        # python2 packages
        print '\nInstalling necessary python2 packages...'
        print '-' * 150
        sys.stdout.flush()
    
        #subprocess.call(exes[file_python] + ' ' + programs_path + 'get-pip.py')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir --upgrade pip==20.3.3')
        #subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir tendo')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir numpy==1.16.2')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pyparsing==2.4.7')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir backports.functools-lru-cache==1.6.4')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pytz==2021.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir six==1.15.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir python-dateutil==2.8.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir setuptools==28.8.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir kiwisolver==1.1.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir cycler==0.10.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir matplotlib==2.2.4')       
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir lxml==4.3.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir mpmath==0.19')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir sympy==1.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir psutil==5.6.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir Pillow==5.4.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pywin32==228')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pypiwin32==223')
        #subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir setuptools==28.8.0')
        #subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pycrypto')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pyteomics==4.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir scipy==1.2.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir scikit-learn==0.20.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pandas==0.24.2')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir xlrd==1.2.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pycparser==2.20')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir cffi==1.14.5')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pynacl==1.4.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir bcrypt==3.1.7')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir enum34==1.1.10')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir ipaddress==1.0.23')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir cryptography==3.3.2')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir paramiko==2.7.2')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir boto==2.49.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir workerpool==0.9.4')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir MarkupSafe==1.1.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir Jinja2==2.11.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir decorator==4.4.2')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir iptools==0.7.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir optcomplete==1.2-devel')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pycrypto==2.6.1')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir scp==0.13.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir iso8601==0.1.14')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir starcluster==0.95.6')
        #subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir tqdm')
        #subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir aws-shell')
        # PyInstaller is necessary for rebuilding PyCloud's .exe install file
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir altgraph==0.17')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pywin32-ctypes==0.2.0')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir future==0.18.2')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir pefile==2019.4.18')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir dis3==0.1.3')
        subprocess.call(exes[file_python] + ' ' + exes[file_pip] + ' install --no-cache-dir PyInstaller==3.6')
        
        # python3 packages
        print '\nInstalling necessary python3 packages...'
        print '-' * 150
        sys.stdout.flush()
        
        #subprocess.call(exes[file_python3] + ' ' + programs_path + 'get-pip.py')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir --upgrade pip==20.3.3')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir tendo')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir numpy==1.19.4')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pyparsing==2.4.7')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir Pillow==8.0.1')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir six==1.15.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir python-dateutil==2.8.1')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir kiwisolver==1.3.1')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir cycler==0.10.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir matplotlib==3.3.3')       
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir lxml==4.6.2')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir mpmath==0.19')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir sympy==1.7')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir psutil==5.7.3')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir Pillow==8.0.1')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pywin32==300')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pypiwin32==223')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir setuptools==47.1.0')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pycrypto')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pyteomics==4.4.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir scipy==1.6.2')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir joblib==0.17.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir threadpoolctl==2.1.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir scikit-learn==0.23.2')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pytz==2021.1')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pandas==1.1.5')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir xlrd==1.2.0')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir starcluster==0.95.6')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir tqdm')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir aws-shell')
        #subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir joblib==0.17.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir tqdm==4.54.1')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir pyopenms==2.6.0')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir hjson==3.0.2')
        subprocess.call(exes[file_python3] + ' ' + exes[file_pip3] + ' install --no-cache-dir glypy==0.12.6')
        
        # find the rest of the executables
        if exes_to_find != []:
            programs = i0b.main(exes_to_find)
            for program in programs:
                exes[program] = programs[program]
            
        write_executables_config(exes)
        
        # update the 4 batch files (MS-PyCloud.bat, batch-manual_terminate_cluster.bat, batch-manual_cloud_retrieval.bat, batch-manual_terminate_PuTTY.bat) with the full python executable path
        for batch_file in batch_files:
            f = open(batch_file, 'r')
            lines = f.readlines()
            f.close()
            
            outfile = open(batch_file, 'w')
            
            for line in lines:
                split_line = line.split(' ')
                
                output_line = ''
                if '.py' in split_line[-1]:
                    output_line = exes[file_python] + ' ' + ' '.join(split_line[1:])
                else:
                    output_line = line
                
                outfile.write(str(output_line))
            
            outfile.close()
        
        print '\nDone!'
        
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Success!', 'Installation was completed successfully!')
        root.destroy()
    except Exception as e:
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Error!', 'Installation was not completed due to the following error:\n\n' + str(e) + '\n\nPlease try to rerun this install file.')
        root.destroy() 
    
def write_executables_config(paths):
    executable_files = open(exe_path, 'w')
    
    # writes the executable function paths into the txt file
    for key in sorted(paths):
        executable_files.write(name_dict[key] + ' = ' + paths[key] + '\n\n')
    
    executable_files.close()
    
def read_executables_config():
    f = open(exe_path, 'r')
    exe_lines = f.readlines()
    f.close()
    
    executables = {}
    for line in exe_lines:
        line = line.strip()
        
        if line != '' and '=' in line:
            line = line.split('=')
            key = str(line[0]).strip()
            value = str(line[1]).strip()
            
            # flip the name_dict keys and values to get the corrected key
            key = name_dict.keys()[name_dict.values().index(key)]
            
            executables[key] = value
            
    return executables

if __name__ == '__main__':
    main()