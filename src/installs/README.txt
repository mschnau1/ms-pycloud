Easy way:
Double-click the program_installs.exe file.

Hard way:
MS-PyCloud is written using Python, version 2.7.  A python install script (program_installs.py) has been included for installing all of the dependencies, and can be 
executed by double clicking the program_installs.bat batch file once Python has been installed.

N.B. A copy of the Python 2.7.14 has been provided and can be installed by the user if Python is not already installed on their machine.  Please double click the 
Python installer (python-2.7.14_64-bit-systems.msi) to install Python before executing the program_installs.py file.  Make sure to select the 
option "Add python.exe to Path" (this is *NOT* selected by default), so that the batch install file (program_installs.bat) can complete the MS-PyCloud installation.

N.B.2 The auto_setup.exe file is only used for the automated installation from the self-extracting executable.  If attempting to install manually, then please 
use the program_installs.exe file.