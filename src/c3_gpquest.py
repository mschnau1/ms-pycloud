"""
Description:
    Uses GPQuest to search and convert the mzml files to csv files.
    
Notes: 
    - gpquest_params.json configuration file is used.
"""

import os
import sys
import glob
import math
import time
import shutil
import subprocess
import c0b_upload_data_to_ebs as c0b
import c0c_start_cluster as c0c
import c0e_terminate_cluster as c0e
import p3w_convert_gpquest_csv_to_tsv as p3w
import p3b_mzml_only_charge_injection_time as p3b
#import hotpot.gpquest_create_protein_decoy_database as gpquest_decoy
from Tkinter import *
from hotpot.pathing import make_path
from hotpot.configuring import read_new_ebs_config
from hotpot.read_write import read_uncommented_lines as rul
from hotpot.read_write import append_step_sh_script as aps
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_key_location as sc_key_location

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
tmp_path = pipeline_path + 'tmp\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
scripts_path = pipeline_path + 'shell_scripts\\'

def main(configs): 
    print '\nStep 3:\tUsing GPQuest to search the mzML files...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    fasta_file = config_path + 'gpquest_params.json'
    gpquest_log_filename = 'GPQuest3.log'
    param_json_filename = 'param.json'
    java_file = exes['python3']
    msgf_jar_file = pipeline_path + 'GPQuest21.py'
    cloud_log_path = output_folder_path + 'logs\\'
    
    # creates the output path folders
    make_path(mzid_path) 
    
    # creates the cloud_log path folder 
    make_path(cloud_log_path)
    
    # create the GPQuest decoy Protein database if necessary
    database_file = read_gpquest_params_key(fasta_file, '"PROTEIN":').replace('"', '')
    database_split = database_file.split('.')
    decoy_database_file = database_split[0] + '_decoys.csv'
    target_decoy_database_file = database_split[0] + '_target_decoys.txt'
    
    if not os.path.exists(decoy_database_file) or not os.path.exists(target_decoy_database_file):
        print('\nGPQuest is creating the decoy databases...')
        print('-' * 150)
        sys.stdout.flush()
        
        #gpquest_decoy.main(input_files = None, output_dir = None, param_file = fasta_file)
        subprocess.call(java_file + ' ' + msgf_jar_file + ' ' + 'None' + ' ' + mzid_path + ' ' + fasta_file)
    
    try:
        ebs_params = configs['ebs_params']
        sc_key, sc_keys = sc_key_info(starcluster_config_path)
        sc_key_path = sc_key_location(starcluster_config_path, sc_key)
        
        # sets variables to configuration values
        raw_end  =  data_root.split('\\')[-2]
        fasta_filename = os.path.basename(fasta_file)
        cluster_name = str(options['cluster_name']).strip()
        pem_key = sc_key_path
        ppk_key = pem_key.replace('.pem', '.ppk')
        python2_path = os.path.dirname(exes['python']) + '\\'
        starcluster_file = python2_path + 'Scripts\\starcluster.exe'
        putty_file = exes['putty']
        putty_path = os.path.dirname(putty_file) + '\\'
        volume_choice = ebs_params['volume_choice']
        mount_path = get_volume_mount_path(volume_choice)
                
        # grabs all mzml filenames
        mzml_files_all = glob.glob(mzml_path + '*.mzML')
                
        # only specifies mzML files for database searching on the cloud that have not already been searched locally
        mzml_files = []
        for mzml_file in mzml_files_all:
            mzml_basename = os.path.basename(mzml_file)
            csv_name = mzml_basename.replace('.mzML', '.csv')
            sptxt_name = mzml_basename.replace('.mzML', '.sptxt')
            iso_name = mzml_basename.replace('.mzML', '_iso.txt')
            
            if not os.path.exists(mzid_path + csv_name) or not os.path.exists(mzid_path + sptxt_name) or not os.path.exists(mzid_path + iso_name):
                mzml_files.append(mzml_file)
        
        if len(mzml_files) != 0:
            # maximum Amazon AWS instance size is currently 100 nodes
            if len(mzml_files) <= 100:       
                num_nodes = len(mzml_files)
            else:
                num_nodes = 100
                
            mzml_files_by_node = map(None, *[iter(mzml_files)] * int(math.ceil(float(len(mzml_files)) / num_nodes)))
            node_num = len(mzml_files_by_node)
            
            update_sc_cluster_size_config(node_num)
            
            # delete any leftover scripts that were not deleted last time due to an exception or premature exit
            sh_files = glob.glob(scripts_path + 'c3_*.*')
            for sh_file in sh_files:
                os.remove(sh_file)
            
            # start cluster and upload data
            c0b.main(configs)
            c0c.main(configs)
            
            configs['node ips'] = rul(config_path + 'node_ips.txt')
            node_ips = configs['node ips']
            
            cloud_pipeline_path = mount_path + 'gpquest/'
            cloud_anaconda_install_path = '$HOME/anaconda3/'
            cloud_data_path = mount_path + 'data/'
            cloud_msgf_jar_file = cloud_pipeline_path + 'software/GPQuest21.py'
            cloud_fasta_path = '/gpquest_files/param/'
            cloud_fasta_file = cloud_fasta_path + fasta_filename
            cloud_mzml_path = cloud_data_path + raw_end + '/mzml/'
            cloud_mzid_path = cloud_data_path + raw_end + '/search_results/'
            # GPQuest 2.1 now uses Anaconda3 instead of Anaconda2
            cloud_java_file = 'sudo $HOME/anaconda3/bin/python'
            
            if not os.path.exists(ppk_key):
                ppk_command = putty_path + 'puttygen ' + pem_key
                
                print ppk_command
                sys.stdout.flush()
                subprocess.call(ppk_command)  
            
            i = 0
            for mzmls in mzml_files_by_node:
                if isinstance(mzmls, tuple):
                    current_mzmls = []
                    
                    for mzml in mzmls:
                        if mzml != None:
                            current_mzmls.append(mzml)
                else:
                    current_mzmls = [mzmls]
                
                mzml_files_by_node[i] = current_mzmls
                i = i + 1
            
            # file header for c3_master.sh
            f = open(scripts_path + 'c3_master_search.sh', 'w')
            f.write('#/bin/bash\n\n')
            f.close()
            
            f = open(scripts_path + 'c3_master_build.sh', 'w')
            f.write('#/bin/bash\n\n')
            f.close()
            
            msgf_build_command = 'sudo bash ' + cloud_pipeline_path + 'anaconda/Miniconda3-py37_4.9.2-Linux-x86_64.sh -b -p ' + cloud_anaconda_install_path + '\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir --upgrade pip==20.3.3\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir numpy==1.19.4\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyparsing==2.4.7\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir Pillow==8.0.1\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir six==1.15.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir python-dateutil==2.8.1\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir kiwisolver==1.3.1\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir cycler==0.10.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir matplotlib==3.3.3\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir lxml==4.6.2\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir mpmath==0.19\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir sympy==1.7\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir psutil==5.7.3\n\n'
            #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir Pillow==8.0.1\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pywin32==300\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pypiwin32==223\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir setuptools==47.1.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyteomics==4.4.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir scipy==1.6.2\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir joblib==0.17.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir threadpoolctl==2.1.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir scikit-learn==0.23.2\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pytz==2021.1\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pandas==1.1.5\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir xlrd==1.2.0\n\n'
            #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir joblib==0.17.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir tqdm==4.54.1\n\n'
            #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir scikit-learn==0.23.2\n\n'
            #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyteomics==4.4.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyopenms==2.6.0\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir hjson==3.0.2\n\n'
            msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir glypy==0.12.6\n\n'
            # pandas and xlrd are already installed in Anaconda
            #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pandas\n\n'
            #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir xlrd'
            aps(filename = scripts_path + 'c3_master_build.sh', command = msgf_build_command)
            
            for mzml in mzml_files_by_node[0]:
                if mzml != None:
                    mzml_filename = os.path.basename(mzml)
                    
                    msgf_search_command = cloud_java_file + ' ' + cloud_msgf_jar_file + ' ' + cloud_mzml_path + mzml_filename + ' ' + cloud_mzid_path + ' ' + cloud_fasta_file
                    aps(filename = scripts_path + 'c3_master_search.sh', command = msgf_search_command)
                    
            for i in range(1, len(mzml_files_by_node)):
                node_name = '00' + str(i)
                node_name = 'node' + node_name[-3:]
                
                f = open(scripts_path + 'c3_' + node_name + '_build.sh', 'w')
                f.write('#/bin/bash\n\n')
                f.close()
                
                msgf_build_command = 'sudo bash ' + cloud_pipeline_path + 'anaconda/Miniconda3-py37_4.9.2-Linux-x86_64.sh -b -p ' + cloud_anaconda_install_path + '\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir --upgrade pip==20.3.3\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir numpy==1.19.4\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyparsing==2.4.7\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir Pillow==8.0.1\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir six==1.15.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir python-dateutil==2.8.1\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir kiwisolver==1.3.1\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir cycler==0.10.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir matplotlib==3.3.3\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir lxml==4.6.2\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir mpmath==0.19\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir sympy==1.7\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir psutil==5.7.3\n\n'
                #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir Pillow==8.0.1\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pywin32==300\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pypiwin32==223\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir setuptools==47.1.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyteomics==4.4.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir scipy==1.6.2\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir joblib==0.17.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir threadpoolctl==2.1.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir scikit-learn==0.23.2\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pytz==2021.1\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pandas==1.1.5\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir xlrd==1.2.0\n\n'
                #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir joblib==0.17.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir tqdm==4.54.1\n\n'
                #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir scikit-learn==0.23.2\n\n'
                #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyteomics==4.4.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pyopenms==2.6.0\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir hjson==3.0.2\n\n'
                msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir glypy==0.12.6\n\n'
                # pandas and xlrd are already installed in Anaconda
                #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir pandas\n\n'
                #msgf_build_command += 'sudo ' + cloud_anaconda_install_path + 'bin/pip install --no-cache-dir xlrd'
                aps(filename = scripts_path + 'c3_' + node_name + '_build.sh', command = msgf_build_command)
                
                # file header for c3_[nodes].sh files
                f = open(scripts_path + 'c3_' + node_name + '_search.sh', 'w')
                f.write('#/bin/bash\n\n')
                f.close()
                
                for mzml in mzml_files_by_node[i]:
                    if mzml != None:
                        mzml_filename = os.path.basename(mzml)
                        
                        msgf_search_command = cloud_java_file + ' ' + cloud_msgf_jar_file + ' ' + cloud_mzml_path + mzml_filename + ' ' + cloud_mzid_path + ' ' + cloud_fasta_file
                        aps(filename = scripts_path + 'c3_' + node_name + '_search.sh', command = msgf_search_command)
            
            # only runs the master_build file due to error with anaconda on the cloud if trying to install on the nodes
            #script_names = glob.glob(scripts_path + 'c3_*_build.sh')
            script_names = glob.glob(scripts_path + 'c3_master_build.sh')
            
            procs = []
            
            for ip, script_name in zip(node_ips, script_names):
                plink_c3_command = putty_path + 'plink ' + ip + ' -i ' + ppk_key + ' -m ' + script_name 
                print plink_c3_command
                sys.stdout.flush()
                proc = subprocess.Popen(plink_c3_command, shell = False, stdin = None, stdout = None, stderr = None, close_fds = True)
                procs.append(proc)
                
            for p in procs:
                print proc.communicate("y\n")
                proc.wait()
                proc.terminate()
                
            print ''
            sys.stdout.flush()
            
            script_names = glob.glob(scripts_path + 'c3_*_search.sh')
            
            procs = []
            for ip, script_name in zip(node_ips, script_names):
                plink_c3_command = putty_path + 'plink ' + ip + ' -i ' + ppk_key + ' -m ' + script_name
                print plink_c3_command
                sys.stdout.flush()
                proc = subprocess.Popen(plink_c3_command, shell = False, stdin = None, stdout = None, stderr = None, close_fds = True)
                procs.append(proc)
                
            for p in procs:
                print p.communicate("y\n")
                p.wait()
                p.terminate()
            
            # terminate cluster
            c0e.main(configs)
            
            # starts the starcluster instance for retrieving data - will be terminated once data is downloaded
            start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name + ' --cluster-size=1 --master-instance-type=t2.micro'
            print start_command
            sys.stdout.flush()
            subprocess.call(start_command)
            
            print '\nRetrieving ' + str(len(mzml_files)) + ' csv/sptxt files from the cloud...'
            print '-' * 150
            sys.stdout.flush()
            
            files_len = len(mzml_files)
            
            percent_interval = math.floor(100.0 / files_len)
            if percent_interval < 5.0:
                percent_interval = 5.0
            
            percent_checked = 0.0
            num_files_checked = 0
            
            if not os.path.exists(mzid_path + gpquest_log_filename):
                get_gpquest_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + gpquest_log_filename + ' ' + mzid_path
                #print get_gpquest_command
                #sys.stdout.flush()
                subprocess.call(get_gpquest_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
            if not os.path.exists(mzid_path + param_json_filename):
                get_param_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + param_json_filename + ' ' + mzid_path
                #print get_param_command
                #sys.stdout.flush()
                subprocess.call(get_param_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
            
            print 'Cloud csv/sptxt download progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
            sys.stdout.flush()
            for mzml_file in mzml_files:
                mzml_basename = os.path.basename(mzml_file)
                csv_filename = mzml_basename.replace('.mzML', '.csv')
                sptxt_filename = mzml_basename.replace('.mzML', '.sptxt')
                iso_filename = mzml_basename.replace('.mzML', '_iso.txt')
                
                # get csv results from the cluster
                get_csv_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + csv_filename + ' ' + mzid_path
                #print get_csv_command
                #sys.stdout.flush()
                subprocess.call(get_csv_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                # get sptxt results from the cluster
                get_sptxt_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + sptxt_filename + ' ' + mzid_path
                #print get_sptxt_command
                #sys.stdout.flush()
                subprocess.call(get_sptxt_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                # get iso results from the cluster
                get_iso_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + iso_filename + ' ' + mzid_path
                #print get_iso_command
                #sys.stdout.flush()
                subprocess.call(get_iso_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = math.floor(current_percent)
                
                    print 'Cloud csv/sptxt download progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                    
            if percent_checked < 100.0:
                print 'Cloud csv/sptxt download progress... 100 %'
                sys.stdout.flush()
            
            print ''
            sys.stdout.flush() 
            
            # terminates cluster that was used to download the csv files    
            terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --force ' + cluster_name
            print terminate_command
            sys.stdout.flush()
            proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
            print proc.communicate("y\n")
            proc.wait()
            proc.terminate()
            
            # delete c3 scripts
            sh_files = glob.glob(scripts_path + 'c3_*.*')
            for sh_file in sh_files:
                os.remove(sh_file)
            
            write_config_params = []
            write_config_params.append('mzml_path = ' + mzml_path)
            write_config_params.append('search_engine = ' + 'GPQuest')
            write_config_outfile = mzid_path + 'step3.config'
            
            write_mspy(write_config_params, write_config_outfile)
            write_tp(config_path + 'gpquest_params.json', write_config_outfile, 'gpquest_params.json settings')
            
        f = open(cloud_log_path + 'cloud_log_' + str(time.strftime("%Y%m%d-%H%M%S")) + '.log', 'w')
        f.write('No errors reported.\n\nIf expected result files are missing, there was likely an error with the initialization of StarCluster - please run the pipeline again if this is the case.')
        f.close()
        
        # convert csv results to tsv
        p3w.main(configs) 
        
        sys.stdout.flush()
        p3b.main(configs)
    except Exception as e:
        # terminate cluster
        c0e.main(configs)
        
        # delete the .sh scripts in case of exception before deletion
        sh_files = glob.glob(scripts_path + 'c3_*.*')
        for sh_file in sh_files:
            os.remove(sh_file)
            
        write_config_params = []
        write_config_params.append('mzml_path = ' + mzml_path)
        write_config_params.append('search_engine = ' + 'GPQuest')
        write_config_outfile = mzid_path + 'step3.config'
        
        write_mspy(write_config_params, write_config_outfile)
        write_tp(config_path + 'gpquest_params.json', write_config_outfile, 'gpquest_params.json settings')
        
        print 'An error occurred while processing data on the cloud and the cluster has been terminated. Please run the pipeline again.'
        print str(e)
        sys.stdout.flush()
        
        f = open(cloud_log_path + 'cloud_error_log_' + str(time.strftime("%Y%m%d-%H%M%S")) + '.log', 'w')
        f.write('Unexpected error: \n' + str(e))
        f.write('\nAn error occurred while processing data on the cloud and the cluster has been terminated.\n\nPlease run the pipeline again.')
        f.close()
        
        #root = Tk()
        #root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        #root.update_idletasks()
        #tkMessageBox.showinfo('Error!', 'An error occurred while processing data on the cloud and the cluster has been terminated. Please try again.')
        #root.destroy()       

# ******************************************************************************

def read_gpquest_params_key(filepath, key):
    # key must be a unique beginning to a line.  If this is not the case, then do NOT use this function.
    infile = open(filepath, 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    val = ''
    for line in infile_lines:
        line = line.strip()
        
        if line[:len(key)] == key:
            val = line.split(key)[1].strip()
            if val[-1] == ',':
                val = val.split(',')[0].strip()
                    
    if val != '':
        return val
    else:
        raise Exception(str(key) + ' was not found in ' + str(filepath) + '.')

def update_sc_cluster_size_config(cluster_size):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    starcluster_output_data = []
    
    cluster_section_flag = False
    for line in lines:
        line = str(line).strip()
        
        if line == '[cluster mspycloud]':
            cluster_section_flag = True
            
        if line == '## Defining Additional Cluster Templates ##':
            cluster_section_flag = False
        
        if cluster_section_flag == True:
            split_line = line.split('=')    
            
            key = split_line[0].strip().lower()
            if key == '':
                new_line = line
            elif key == 'cluster_size':
                new_line = 'CLUSTER_SIZE = ' + str(cluster_size)
            elif key[0] == '#':
                if key.split('#')[1].strip() == 'cluster_size':
                    new_line = 'CLUSTER_SIZE = ' + str(cluster_size)
                else:
                    new_line = line
            else:
                new_line = line
        else:
            new_line = line
            
        starcluster_output_data.append(new_line + '\n')
                
    f = open(starcluster_config_path, 'w')
    for starcluster_output_line in starcluster_output_data:
        f.write(starcluster_output_line)
    f.close()
    
def get_volume_mount_path(volume_choice):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    volume_section_flag = False
    
    for line in lines:
        line = line.strip()
        
        # specified volume must NOT be commented out
        if line == '[volume ' + volume_choice + ']':
            volume_section_flag = True
            
        if volume_section_flag == True:
            if line[:10] == 'MOUNT_PATH':
                line = line.split('=')[-1].strip()
                mount_path = line
                
                return mount_path
    
# ******************************************************************************
               
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['ebs_params'] = read_new_ebs_config(config_path + 'ebs_params.txt')
    configs['node ips'] = rul(config_path + 'node_ips.txt')
    configs['backup'] = True
    
    main(configs)