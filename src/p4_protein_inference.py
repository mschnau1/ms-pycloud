"""
Description:
    Create a peptide to protein map based on all of the available iTRAQ samples. 
    
Notes:
    - min_psm_per_peptide and min_peptide_per_protein are user specified in the steps.txt file.
"""

import os
import re
import sys
import json
import glob
import time
import shutil
import numpy as np
import matplotlib.pyplot as plt
import hotpot.mzid as mzid
import hotpot.pepfdr as pepfdr
import hotpot.data_matrix as data_matrix
import hotpot.n_glycopeptide as glycopeptide
import hotpot.phosphopeptide as phosphopeptide
import hotpot.isobaric_correction_factors as corr_factors
from math import floor
from copy import deepcopy
from hotpot.pathing import make_path
from hotpot.labeling_efficiency import get_label_masses
from hotpot.labeling_efficiency import generate_labeling_efficiency_file
from hotpot.labeling_efficiency import combine_labeling_efficiency_files
from hotpot.spectrum import MS2
from hotpot.spectrum import Spectrum
from hotpot.protein_identification import *
from hotpot.read_write import read_lines as rl
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps
from hotpot.read_write import read_config_param as rcp
from hotpot.md5_checksums import step4_psm_quant_md5_checksums as psm_md5
from hotpot.md5_checksums import step4_search_results_md5_checksums as search_md5
from hotpot.md5_checksums import step4_search_results_gpquest_md5_checksums as search_md5_gpquest
from hotpot.md5_checksums import config_file_md5_checksum as config_md5
from matplotlib.backends.backend_pdf import PdfPages

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'

def main(configs): 
    print '\nStep 4:\tCreate a peptide to protein map based on specified sets...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    plt.ion()
    
    options = configs['options']
    inference_options = configs['inference_search']
    sample_filenames = configs['sample filenames']
    sample_filenames.sort(key = natural_keys)
    correction_matrix_lines = configs['correction_factors']
    mod = str(options['modification']).lower()
    
    # preserve original sample_filenames list  
    original_sample_filenames = list(sample_filenames)
    
    if mod == 'global' or mod == 'glyco' or mod == 'phospho':
        psm_quantitation(options, list(sample_filenames), correction_matrix_lines)
    elif mod == 'intact':
        # handles conversion of GPQuest results to psm quantitation format
        # uses Morpheus score instead of -log10(MS-GF+ SpectralEValue)
        # GPQuest results are already filtered at specified PSM-level FDR
        psm_quantitation_gpquest(options, list(sample_filenames), correction_matrix_lines)
    else:
        raise Exception('mod must be either "global", "glyco", "intact", or "phospho"')
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    qc_path = output_folder_path + 'quality_control\\'
    data_set_name = data_root.split('\\')[-2]
    mzid_path = data_root + 'step3-search_results\\'
    quantitation_path = output_folder_path + 'step4a-psm_quantitation\\'
    peptide_protein_path = output_folder_path + 'step4b-protein_inference\\'
    fasta_file = options['fasta_path']
    min_psm_per_peptide = int(options['min_psm_per_peptide'])
    min_peptide_per_protein = int(options['min_peptide_per_protein'])
    optional_n_term_met_cleavage = str(options['optional_n_term_met_cleavage']).lower()
    label_type = options['label_type']
    cleavage_rule = inference_options['cleavage_rule']
    max_miscleavage = int(inference_options['max_miscleavage'])
    min_length = int(inference_options['min_length'])
    max_length = int(inference_options['max_length'])
    min_cleavage_terminus = int(inference_options['min_cleavage_terminus'])
    digestion_type = str(inference_options['cleavage_rule']).strip().lower()
    
    itraq4_channels = ['114', '115', '116', '117']
    itraq8_channels = ['113', '114', '115', '116', '117', '118', '119', '121']
    tmt10_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
    tmt11_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
    tmt16_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
    
    if label_type == '4plex':
        channels = itraq4_channels
        num_channels = 4
    elif label_type == '8plex':
        channels = itraq8_channels
        num_channels = 8
    elif label_type == '10plex':
        channels = tmt10_channels
        num_channels = 10
    elif label_type == '11plex':
        channels = tmt11_channels
        num_channels = 11
    elif label_type == '16plex':
        channels = tmt16_channels
        num_channels = 16
    
    if optional_n_term_met_cleavage == 'true':
        optional_n_term_met_cleavage = True
    else:
        optional_n_term_met_cleavage = False
    
    output_path = peptide_protein_path
    # determines if data was archived by the user and will need to be rerun
    output_path_exists = os.path.exists(output_path)
    
    # creates the output path folders
    make_path(output_path)
    make_path(qc_path)
    
    # handle single shot data
    data_files = glob.glob(quantitation_path + '*.txt')
    if original_sample_filenames == list():
        for data_f in data_files:
            data_f_basename = os.path.basename(data_f).replace('.txt', '')
            
            if data_f_basename not in sample_filenames:
                sample_filenames.append(data_f_basename)
                
        # removes non-unique names in case a set file was left over from previous analysis
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)
    # adds in each fraction to the sample_filenames list for this step to get data for each fraction
    #else:
    #    for data_f in data_files:
    #        data_f_basename = os.path.basename(data_f).replace('.txt', '')
    #        
    #        if data_f_basename not in sample_filenames:
    #            sample_filenames.append(data_f_basename)    
    
    sample_filenames.sort(key = natural_keys)
    
    # determines if the code in this function needs to be rerun or not by comparing md5 checksums of the psm quantitation files to the md5 checksums of those files the last time protein inference was run
    run_code = False
    run_samples = set()
    move_md5 = False
    
    # number of header lines in md5 checksum files
    md5_header = 1
    psm_md5_filename = 'step4_psm_md5_checksums.tsv'
    
    files_should_exist = [output_path + 'peptide_protein.txt', output_path + 'peptide_protein_log.log', output_path + data_set_name + '_protein_inference_idfree_quameter.tsv', output_path + 'step4_inference.config']
    
    if not os.path.exists(quantitation_path + psm_md5_filename) or output_path_exists == False:
        run_code = True
        
        psm_md5(quantitation_path, quantitation_path, sample_filenames)
        config_md5(config_path + 'sample_filenames.txt', output_path)
        
        for sample_filename in sample_filenames:
            run_samples.add(sample_filename)
    else:
        try:
            for filepath in files_should_exist:
                if not os.path.exists(filepath):
                    run_code = True
                
            for sample_filename in sample_filenames:
                if not os.path.exists(output_path + sample_filename + '.txt') or not os.path.exists(output_path + sample_filename + '_log.log'):
                    run_code = True
                    run_samples.add(sample_filename)
                
            if not os.path.exists(output_path + 'sample_filenames_md5_checksum.tsv'):
                run_code = True
                
                config_md5(config_path + 'sample_filenames.txt', output_path)
            else:
                f = open(output_path + 'sample_filenames_md5_checksum.tsv', 'r')
                previous_md5_lines = f.readlines()
                f.close()
                
                config_md5(config_path + 'sample_filenames.txt', output_path)
                
                f = open(output_path + 'sample_filenames_md5_checksum.tsv', 'r')
                current_md5_lines = f.readlines()
                f.close()
                
                previous_md5 = {}
                for line in previous_md5_lines[md5_header:]:
                    line = line.strip().split('\t')
                    filename = line[0].strip()
                    checksum = line[1].strip()
                    
                    previous_md5[filename] = checksum
                    
                current_md5 = {}
                for line in current_md5_lines[md5_header:]:
                    line = line.strip().split('\t')
                    filename = line[0].strip()
                    checksum = line[1].strip()
                    
                    current_md5[filename] = checksum
                
                for key in current_md5:
                    if key not in previous_md5:
                        run_code = True        
                    elif previous_md5[key] != current_md5[key]:
                        run_code = True
                            
                for key in previous_md5:
                    if key not in current_md5:
                        run_code = True
                    elif current_md5[key] != previous_md5[key]:
                        run_code = True
            
            f = open(quantitation_path + psm_md5_filename, 'r')
            previous_md5_lines = f.readlines()
            f.close()
            
            psm_md5(quantitation_path, output_path, sample_filenames)
            
            f = open(output_path + psm_md5_filename, 'r')
            current_md5_lines = f.readlines()
            f.close()
            
            previous_md5 = {}
            for line in previous_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                previous_md5[filename] = checksum
                
            current_md5 = {}
            for line in current_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                current_md5[filename] = checksum
            
            for key in current_md5:
                run_sample = False
                
                if key not in previous_md5:
                    run_sample = True        
                elif previous_md5[key] != current_md5[key]:
                    run_sample = True
                
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(output_path + sample_filename + '.txt') or not os.path.exists(output_path + sample_filename + '_log.log'):
                            run_code = True
                            run_samples.add(sample_filename)
                        break
                        
            for key in previous_md5:
                run_sample = False
                
                if key not in current_md5:
                    run_sample = True
                elif current_md5[key] != previous_md5[key]:
                    run_sample = True
                    
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(output_path + sample_filename + '.txt') or not os.path.exists(output_path + sample_filename + '_log.log'):
                            run_code = True
                            run_samples.add(sample_filename)
                        break
                        
            move_md5 = True
        except:
            if os.path.exists(output_path + psm_md5_filename):
                os.remove(output_path + psm_md5_filename)
            
            run_code = True
        
            psm_md5(quantitation_path, quantitation_path, sample_filenames)
            config_md5(config_path + 'sample_filenames.txt', output_path)
            
            for sample_filename in sample_filenames:
                run_samples.add(sample_filename)    
    
    if run_code:
        # make copy of the sample_filenames.txt config file so that there is a record of what files were used in generating the peptide_protein.txt result
        if original_sample_filenames != list(): 
            shutil.copyfile(config_path + 'sample_filenames.txt', qc_path + 'records-peptide_protein_sets-' + data_set_name + '.log')
        else:
            sets_f = open(qc_path + 'records-peptide_protein_sets-' + data_set_name + '.log', 'w')
            
            for sample_filename in sample_filenames[: -1]:
                sets_f.write(str(sample_filename) + '\n\n')
                
            sets_f.write(str(sample_filenames[-1]))
            sets_f.close()
        
        peptide_psm_count_map = {}
    
        set_count = 0
        run_set_count = 0
        for f in data_files:
            run_current_file = False
            current_filename = str(os.path.basename(f).replace('.txt', ''))
            
            # runs the set protein inferences that need to be run
            if current_filename in run_samples:
                run_current_file = True
                current_peptide_psm_count_map = {}
            
            if current_filename in sample_filenames:
                set_count += 1
                
                print 'Reading ' + f + ' (set ' + str(set_count) + ' of ' + str(len(sample_filenames)) + ')'
                sys.stdout.flush()
                
                dm = data_matrix.DataMatrix.data_matrix_from_tsv(f, header = False)
                print len(dm.column_to_list('Column_1'))
                # adds to the psm count for a given peptide if a match is found
                for peptide in dm.column_to_list('Column_1'):
                    if original_sample_filenames != list():
                        if current_filename in original_sample_filenames:
                            if peptide in peptide_psm_count_map:
                                peptide_psm_count_map[peptide] += 1
                            else:
                                peptide_psm_count_map[peptide] = 1
                    elif current_filename in sample_filenames:
                        if peptide in peptide_psm_count_map:
                            peptide_psm_count_map[peptide] += 1
                        else:
                            peptide_psm_count_map[peptide] = 1
                    
                    if run_current_file == True:
                        if peptide in current_peptide_psm_count_map:
                            current_peptide_psm_count_map[peptide] += 1
                        else:
                            current_peptide_psm_count_map[peptide] = 1
                            
                print ''
                sys.stdout.flush()
                
            if run_current_file == True:
                run_set_count += 1
                
                print 'Inferring proteins from set ' + f + ' (set ' + str(run_set_count) + ' of ' + str(len(run_samples)) + ')'
                sys.stdout.flush()
            
                protein_inference(current_peptide_psm_count_map, output_path, 
                                fasta_file,
                                min_psm_per_peptide = min_psm_per_peptide,
                                min_peptide_per_protein = min_peptide_per_protein,
                                cleavage_rule = cleavage_rule,
                                max_miscleavage = max_miscleavage,
                                min_length = min_length, 
                                max_length = max_length,
                                min_cleavage_terminus = min_cleavage_terminus,
                                optional_n_term_met_cleavage = optional_n_term_met_cleavage,
                                mod = mod,
                                output_filename = current_filename)
                                
                print ''
                sys.stdout.flush()
        
        print(len(peptide_psm_count_map))
        print(sum(peptide_psm_count_map.values()))
        
        print ''
        print('Inferring proteins from all set psm quantitation files together...')
        sys.stdout.flush() 
        
        protein_inference(peptide_psm_count_map, output_path, 
                        fasta_file,
                        min_psm_per_peptide = min_psm_per_peptide,
                        min_peptide_per_protein = min_peptide_per_protein,
                        cleavage_rule = cleavage_rule,
                        max_miscleavage = max_miscleavage,
                        min_length = min_length, 
                        max_length = max_length,
                        min_cleavage_terminus = min_cleavage_terminus,
                        optional_n_term_met_cleavage = optional_n_term_met_cleavage,
                        mod = mod,
                        output_filename = 'peptide_protein')
                        
        print '\nGenerating completed quality control results (please wait)...'
        print '-' * 150
        sys.stdout.flush()
        
        # compile protein inference log files from each fraction and all together into one tsv file with the idfree quameter results
        log_files = glob.glob(peptide_protein_path + '*_log.log')
        
        log_results = {}
        for log_file in log_files:
            fraction_filename = os.path.basename(log_file).replace('_log.log', '')
            log_results[fraction_filename] = {}
            
            f = open(log_file, 'r')
            lines = f.readlines()
            f.close()
            
            for line in lines:
                line = line.strip()
                if ':' in line:
                    line = line.split(':')
                    line_key = line[0].strip()
                    line_val = line[1].strip()
                    
                    if line_val[-1] == ',':
                        line_val = line_val[:-1]
                    
                    if line_key == '"final_psm_num"':
                        log_results[fraction_filename]['"final_psm_num"'] = line_val
                    elif line_key == '"final_peptide_num"':
                        log_results[fraction_filename]['"final_peptide_num"'] = line_val
                    elif line_key == '"final_protein_group_num"':
                        log_results[fraction_filename]['"final_protein_group_num"'] = line_val
                    elif line_key == '"num_psm_before_filtering"':
                        log_results[fraction_filename]['"num_psm_before_filtering"'] = line_val
                    elif line_key == '"num_peptide_before_filtering"':
                        log_results[fraction_filename]['"num_peptide_before_filtering"'] = line_val
        
        idfree_file = mzid_path + data_set_name + '_search_idfree_quameter_results.tsv'
        
        if os.path.isfile(idfree_file):
            f = open(idfree_file, 'r')
            idfree_lines = f.readlines()
            f.close()
        else:
            # handles missing quameter results file
            idfree_lines = ['Filename']
            
        # will be used to check which protein inference fraction files are to be used in later section of code
        quameter_fraction_names = []
        
        output_data = []
        for line in idfree_lines:
            #valid_line = False
            
            # always set valid_line to True because all raw_filenames in search_results quameter file will be checked against the sample_filenames configuration file
            valid_line = True
            
            if line == idfree_lines[0]: 
                #valid_line = True
                line = line.strip().split('\t')   
                line.insert(1, '"num_peptide_before_filtering"')
                line.insert(1, '"num_psm_before_filtering"')
                line.insert(1, '"final_protein_group_num"')
                line.insert(1, '"final_peptide_num"')
                line.insert(1, '"final_psm_num"')
            else:
                line = line.strip().split('\t')
                raw_filename = line[0].strip().replace('.raw', '').replace('"', '')
                quameter_fraction_names.append(raw_filename)
                
                if raw_filename in log_results:
                    #valid_line = True
                    current_log = log_results[raw_filename]
                    line.insert(1, current_log['"num_peptide_before_filtering"'])
                    line.insert(1, current_log['"num_psm_before_filtering"'])
                    line.insert(1, current_log['"final_protein_group_num"'])
                    line.insert(1, current_log['"final_peptide_num"'])
                    line.insert(1, current_log['"final_psm_num"'])
                else:
                    #valid_line = False
                    line.insert(1, -1)
                    line.insert(1, -1)
                    line.insert(1, -1)
                    line.insert(1, -1)
                    line.insert(1, -1)
                
            if valid_line == True:
                output_line = ''
                for value in line:
                    output_line += str(value) + '\t'
                    
                output_data.append(output_line.strip() + '\n')
        
        # add in a blank line to the tsv file
        if original_sample_filenames != list():
            output_data.append('\n')
        
        # add in the all together protein inference results from each set to the tsv file
        for sample_filename in original_sample_filenames:
            if sample_filename in log_results:
                sample_log = log_results[sample_filename]
                
                sample_list = [sample_filename + '.txt']
                sample_list.insert(1, sample_log['"num_peptide_before_filtering"'])
                sample_list.insert(1, sample_log['"num_psm_before_filtering"'])
                sample_list.insert(1, sample_log['"final_protein_group_num"'])
                sample_list.insert(1, sample_log['"final_peptide_num"'])
                sample_list.insert(1, sample_log['"final_psm_num"'])
        
                output_line = ''
                for value in sample_list:
                    output_line += value + '\t'
                
                output_data.append(output_line.strip() + '\n')
    
        # add in a blank line to the tsv file
        output_data.append('\n')
        
        # add in the all together protein inference results to the tsv file
        peptide_protein_log = log_results['peptide_protein']
        
        peptide_protein_list = ['peptide_protein.txt']
        peptide_protein_list.insert(1, peptide_protein_log['"num_peptide_before_filtering"'])
        peptide_protein_list.insert(1, peptide_protein_log['"num_psm_before_filtering"'])
        peptide_protein_list.insert(1, peptide_protein_log['"final_protein_group_num"'])
        peptide_protein_list.insert(1, peptide_protein_log['"final_peptide_num"'])
        peptide_protein_list.insert(1, peptide_protein_log['"final_psm_num"'])
        
        output_line = ''
        for value in peptide_protein_list:
            output_line += value + '\t'
        
        output_data.append(output_line.strip() + '\n')
        
        # add in a blank line to the tsv file
        output_data.append('\n')
        
        # determine the number of peptides out of all peptides that are unique to 1 fraction, 2 fractions, 3 fractions, and 4+ fractions and add results to the tsv file
        # get peptide num per fraction and identification rate for each fraction from set data
        if original_sample_filenames != list():
            qc_sample_filenames = list(original_sample_filenames)
            fraction_only_flag = False
        else:
            qc_sample_filenames = list(sample_filenames)
            fraction_only_flag = True
            
        psm_path_files = glob.glob(quantitation_path + '*.txt')
        prot_path_files = glob.glob(peptide_protein_path + '*.txt')
            
        psm_files = []
        for psm_file in psm_path_files:
            psm_basename = os.path.basename(psm_file).replace('.txt', '')
                
            if psm_basename in qc_sample_filenames:
                psm_files.append(psm_file)
                        
        prot_files = []
        for prot_file in prot_path_files:
            prot_basename = os.path.basename(prot_file).replace('.txt', '')
                
            if prot_basename in qc_sample_filenames:
                prot_files.append(prot_file)
                        
        prot_inf_peps = {}
        for prot_file in prot_files:
            setname = os.path.basename(prot_file).replace('.txt', '')
            
            f = open(prot_file, 'r')
            lines = f.readlines()
            f.close() 
            
            prot_inf_peps[setname] = []
            for line in lines:
                line = line.strip().split('\t')
                pep = str(line[0]).strip()
                
                prot_inf_peps[setname].append(pep)
        
        # count the number of psms from each fraction in the set psm file that were used to map the proteins for that set's protein inference
        fraction_psm_count = {} 
        fraction_peptide_count = {}
        fraction_peptides = {}
        fraction_protein_count = {}
        fraction_proteins = {}
        pep_fraction_map = {}
        miscleavage_map = {}
        psm_intensity_data = {}
        
        files_len = len(psm_files)
        
        percent_interval = floor(100.0 / files_len)
        if percent_interval < 5.0:
            percent_interval = 5.0
    
        percent_checked = 0.0
        num_files_checked = 0
    
        print 'Reading psm_quantitation files, for quality control statistics, progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
        sys.stdout.flush()
        for psm_file in psm_files:
            num_files_checked += 1
            
            setname = os.path.basename(psm_file).replace('.txt', '')
            
            f = open(psm_file, 'r')
            lines = f.readlines()
            f.close()
            
            num_miscleavages = {}
            unique_peptides = {}  
            if not fraction_only_flag:
                if setname not in unique_peptides and setname not in num_miscleavages:
                    num_miscleavages[setname] = 0
                    unique_peptides[setname] = []
                else:
                    raise Exception('Miscleavage rate already calculated for ' + str(setname) + '.  Sample filenames are not unique!') 
                
            for line in lines:
                line = line.strip().split('\t')
                pep = str(line[0]).strip()
                fraction_name = str(line[-1]).strip().split('.')[0].strip()
                # protein value is read in below
                prot = ''
                
                # the protein column is different for intact glycopeptide data
                if mod == 'intact':
                    prot = str(line[-6]).strip()
                else:
                    prot = str(line[-4]).strip()
                
                if fraction_name not in fraction_psm_count:
                    fraction_psm_count[fraction_name] = 0
                
                if fraction_name not in fraction_peptide_count:
                    fraction_peptides[fraction_name] = []
                    fraction_peptide_count[fraction_name] = 0
                    
                if fraction_name not in fraction_protein_count:
                    fraction_proteins[fraction_name] = []
                    fraction_protein_count[fraction_name] = 0
    
                if pep in prot_inf_peps[setname]:
                    # counts how many psm's used for protein inference came from each fraction/single shot file
                    fraction_psm_count[fraction_name] += 1
                        
                    # counts how many peptides from psm's used for protein inference came from each fraction/single shot file
                    if pep not in fraction_peptides[fraction_name]:
                        fraction_peptides[fraction_name].append(pep)
                        fraction_peptide_count[fraction_name] += 1
                        
                    # counts how many proteins from psm's used for protein inference came from each fraction/single shot file
                    if prot not in fraction_proteins[fraction_name]:
                        fraction_proteins[fraction_name].append(prot)
                        fraction_protein_count[fraction_name] += 1
                        
                    # keeps track of how many fractions the peptide is in
                    if pep not in pep_fraction_map:
                        pep_fraction_map[pep] = set()
                        pep_fraction_map[pep].add(fraction_name)
                    else:
                        pep_fraction_map[pep].add(fraction_name)
                        
                # get miscleavage rate data
                if fraction_name not in unique_peptides:
                    unique_peptides[fraction_name] = []
                
                if fraction_name not in num_miscleavages:
                    num_miscleavages[fraction_name] = 0
                
                # miscleavage updating
                if pep not in unique_peptides[fraction_name]:
                    unique_peptides[fraction_name].append(pep)
                    
                    set_flag = False
                    if not fraction_only_flag and pep not in unique_peptides[setname]:
                        unique_peptides[setname].append(pep)
                        set_flag = True
                        
                    # determine if the peptide was miscleaved
                    num_cleavage_sites = 0
                    
                    amino_position = -1
                    for amino in pep[:-1]:
                        amino_position += 1
                        
                        if amino == 'T':
                            if digestion_type == 'alphalp':
                                num_cleavage_sites += 1
                        elif amino == 'A':
                            if digestion_type == 'alphalp':
                                num_cleavage_sites += 1
                        elif amino == 'S':
                            if digestion_type == 'alphalp':
                                num_cleavage_sites += 1
                        elif amino == 'V':
                            if digestion_type == 'alphalp':
                                num_cleavage_sites += 1
                        elif amino == 'R':
                            if digestion_type == 'arg-c':
                                num_cleavage_sites += 1
                            elif digestion_type == 'trypsin/p':
                                num_cleavage_sites += 1
                            elif digestion_type == 'trypsin':
                                succeeding_amino = amino_position + 1
                                if pep[succeeding_amino] != 'P':
                                    num_cleavage_sites += 1
                        elif amino == 'K':
                            if digestion_type == 'trypsin/p':
                                num_cleavage_sites += 1
                            elif digestion_type == 'trypsin':
                                succeeding_amino = amino_position + 1
                                if pep[succeeding_amino] != 'P':
                                    num_cleavage_sites += 1
                    
                    if digestion_type == 'trypsin' or digestion_type == 'trypsin/p':    
                        if pep[-1] == 'K' or pep[-1] == 'R':
                            num_cleavage_sites += 1
                    elif digestion_type == 'arg-c':
                        if pep[-1] == 'R':
                            num_cleavage_sites += 1
                    elif digestion_type == 'alphalp':
                        if pep[-1] == 'T' or pep[-1] == 'A' or pep[-1] == 'S' or pep[-1] == 'V':
                            num_cleavage_sites += 1
                    
                    if num_cleavage_sites > 1:
                        num_miscleavages[fraction_name] += 1
                        
                        if not fraction_only_flag and set_flag:
                            num_miscleavages[setname] += 1
                                
                        #print pep 
                
                # read psm quantitation results to get median intensity across all psms in each channel
                if label_type != 'free':
                    if fraction_name not in psm_intensity_data:
                        # summed intensities or median intensity for each channel - uncomment/comment code to pick
                        psm_intensity_data[fraction_name] = [0 for x in range(num_channels)]
                        #psm_intensity_data[fraction_name] = [list() for x in range(num_channels)]
                        
                    if setname not in psm_intensity_data and not fraction_only_flag:
                        # summed intensities or median intensity for each channel - uncomment/comment code to pick
                        psm_intensity_data[setname] = [0 for x in range(num_channels)]
                        #psm_intensity_data[setname] = [list() for x in range(num_channels)]
                        
                    intensities = line[2: 2 + num_channels]
                    for index, intensity in enumerate(intensities):
                        psm_intensity_data[fraction_name][index] += float(intensity)
                        # handles singleshot cases where the user selects the whole filename as the setname
                        if not fraction_only_flag and setname != fraction_name:
                            psm_intensity_data[setname][index] += float(intensity)      
                        
                        #psm_intensity_data[fraction_name][index].append(float(intensity))
                        #if not fraction_only_flag and setname != fraction_name:
                        #    psm_intensity_data[setname][index].append(float(intensity))
                            
            # update miscleavage_map results
            for f_name in unique_peptides:
                #num_peptides = len(psm_lines)
                num_peptides = len(unique_peptides[f_name])
                
                if f_name not in miscleavage_map:
                    if digestion_type == 'trypsin' or digestion_type == 'trypsin/p' or digestion_type == 'arg-c' or digestion_type == 'alphalp':
                        miscleavage_map[f_name] = (float(num_miscleavages[f_name]) / float(num_peptides)) * 100.0
                    else:
                        miscleavage_map[f_name] = 'NA'
                else:
                    raise Exception('Miscleavage rate already calculated for ' + str(f_name) + '.  Sample filenames are not unique!') 
            
            # update the psm_intensity_data_results
            #for index, intensity_list in enumerate(psm_intensity_data[fraction_name]):
            #    psm_intensity_data[fraction_name][index] = np.median(sorted(psm_intensity_data[fraction_name][index])) 
            #if not fraction_only_flag:
            #    for index, intensity_list in enumerate(psm_intensity_data[setname]):
            #        psm_intensity_data[setname][index] = np.median(sorted(psm_intensity_data[setname][index])) 
            
            current_percent = ((100.0 * num_files_checked) / files_len)
            if current_percent >= (percent_checked + percent_interval):
                percent_checked = floor(current_percent)
            
                print 'Reading psm_quantitation files, for quality control statistics, progress... ' + str(percent_checked) + ' %'
                sys.stdout.flush() 
        
        if percent_checked < 100.0:
            print 'Reading psm_quantitation files, for quality control statistics, progress... 100 %'
            sys.stdout.flush()                         
        
        peptide_uniqueness = {}
        peptide_uniqueness['1'] = 0
        peptide_uniqueness['2'] = 0
        peptide_uniqueness['3'] = 0
        peptide_uniqueness['4+'] = 0
        peptide_uniqueness_keys = sorted(peptide_uniqueness)
        
        if pep_fraction_map != dict():
            for key in pep_fraction_map:
                if original_sample_filenames != list():
                    num_fractions = int(len(pep_fraction_map[key]))
                    
                    unique_key = ''
                    if num_fractions >= 4:
                        unique_key = '4+'
                    else:
                        unique_key = str(num_fractions)
                        
                    peptide_uniqueness[unique_key] += 1
                # handles singleshot data
                else:
                    peptide_uniqueness['1'] += 1
        else:
            peptide_uniqueness['1'] = -1
            peptide_uniqueness['2'] = -1
            peptide_uniqueness['3'] = -1
            peptide_uniqueness['4+'] = -1
            
        for key in peptide_uniqueness_keys:
            fraction_str = 'fractions'
            if key != '4+':
                if int(key) == 1:
                    fraction_str = 'fraction'
                
            output_line = 'Number of peptides in ' + key + ' ' + fraction_str + ' \t \t' + str(peptide_uniqueness[key])
            
            output_data.append(output_line.strip() + '\n')       
        
        # outputs updated idfree quameter file after protein inference        
        outfile = open(peptide_protein_path + data_set_name + '_protein_inference_idfree_quameter.tsv', 'w')
        for output_line in output_data:
            outfile.write(output_line)
        outfile.close()
        
        # begin qc file management
        qc_file = qc_path + 'qc-quameter-' + data_set_name + '.tsv'
        
        qc_file_flag = True
        if os.path.isfile(qc_file):
            f = open(qc_file, 'r')
            qc_lines = f.readlines()
            f.close()
        else:
            qc_file_flag = False
            
            # handles missing qc file
            qc_lines = ['Filename']           
        
        # read protein inference quameter file for injection time data
        inf_quam_infile = open(mzid_path + data_set_name + '_search_idfree_quameter_results.tsv', 'r')
        inf_quam_lines = inf_quam_infile.readlines()
        inf_quam_infile.close()
        
        inf_injection_header = []
        inf_header_row = inf_quam_lines[0].strip().split('\t')
        median_injection_col = -1
        mean_injection_col = -1
        std_injection_col = -1
        for inf_col in inf_header_row:
            if 'injection time # (mzML)' in inf_col:
                inf_injection_header.append(str(inf_col))  
            elif 'Median injection time' in inf_col:
                if median_injection_col == -1:
                    median_injection_col = inf_header_row.index(inf_col)
                else:
                    raise Exception('Problem with column headers in the search_results folder quameter file.')
            elif 'Mean injection time' in inf_col:
                if mean_injection_col == -1:
                    mean_injection_col = inf_header_row.index(inf_col)
                else:
                    raise Exception('Problem with column headers in the search_results folder quameter file.')
            elif 'Std. dev. injection time' in inf_col:
                if std_injection_col == -1:
                    std_injection_col = inf_header_row.index(inf_col)  
                else:
                    raise Exception('Problem with column headers in the search_results folder quameter file.')     
                    
        inf_injection_begin = inf_header_row.index(inf_injection_header[0])
        inf_injection_end = inf_header_row.index(inf_injection_header[-1])
        
        # read protein inference quameter file for injection time data
        inf_injection_times = {}
        median_injection_times = {}
        mean_injection_times = {}
        std_injection_times = {}
        for inf_line in inf_quam_lines[1:]:
            inf_line = inf_line.strip().split('\t')
            inf_filename = inf_line[0].strip().replace('.raw', '').replace('"', '')
            
            inf_injection_times[inf_filename] = []
            for inf_col in inf_line[inf_injection_begin: inf_injection_end + 1]:
                inf_injection_times[inf_filename].append(float(inf_col))
                
            median_injection_times[inf_filename] = inf_line[median_injection_col]
            mean_injection_times[inf_filename] = inf_line[mean_injection_col]
            std_injection_times[inf_filename] = inf_line[std_injection_col]
                    
        output_data = []
        qc_header = ''
        for line in qc_lines:
            #valid_line = False
            
            # always set valid_line to True because all raw_filenames in search_results quameter file will be checked against the sample_filenames configuration file
            valid_line = True
            
            if line == qc_lines[0]: 
                #valid_line = True
                line = line.strip().split('\t')           
                
                # keep these two things together - necessary for future calculation
                qc_header = list(line) 
                if qc_file_flag:
                    line.insert(1, 'Identification rate (%, final_psm_num / MS2-Count)')
                line.insert(1, 'Miscleavage rate (%, peptide level)')
                
                for inf_injection_time in inf_injection_header[::-1]:
                    line.insert(1, inf_injection_time)
                    
                line.insert(1, 'Std. dev. injection time')
                line.insert(1, 'Mean injection time')
                line.insert(1, 'Median injection time')
                
                if label_type != 'free':
                    for channel in channels[::-1]:
                        line.insert(1, str(channel) + ' summed intensity (all PSM)')
                
                line.insert(1, '"num_peptide_before_filtering"')
                line.insert(1, '"num_psm_before_filtering"')
                line.insert(1, '"final_protein_group_num"')
                line.insert(1, '"final_peptide_num"')
                line.insert(1, '"final_psm_num"')
            else:
                line = line.strip().split('\t')
                raw_filename = line[0].strip().replace('.raw', '').replace('"', '')
                
                identification_rate = ''
                try:
                    identification_rate = str((float(fraction_psm_count[raw_filename]) / float(line[qc_header.index('MS2-Count')])) * 100.0)
                except:
                    identification_rate = '-1'
                
                line.insert(1, identification_rate)
                
                miscleavage_rate = ''
                try:
                    miscleavage_rate = str(miscleavage_map[raw_filename])
                except:
                    miscleavage_rate = '-1'
                
                line.insert(1, miscleavage_rate)
                
                for inf_injection_time in inf_injection_times[raw_filename][::-1]:
                    line.insert(1, str(inf_injection_time))
                    
                line.insert(1, str(std_injection_times[raw_filename]))
                line.insert(1, str(mean_injection_times[raw_filename]))
                line.insert(1, str(median_injection_times[raw_filename]))
                    
                if label_type != 'free':
                    try:
                        for psm_intensity in psm_intensity_data[raw_filename][::-1]:
                            line.insert(1, str(psm_intensity))  
                    except:
                        for channel in channels[::-1]:
                            line.insert(1, '-1')
                
                if raw_filename in log_results:
                    #valid_line = True  
                    
                    current_log = log_results[raw_filename]
                    line.insert(1, current_log['"num_peptide_before_filtering"'])
                    line.insert(1, current_log['"num_psm_before_filtering"'])
                    line.insert(1, current_log['"final_protein_group_num"'])
                    line.insert(1, current_log['"final_peptide_num"'])
                    line.insert(1, current_log['"final_psm_num"'])
                else:
                    #valid_line = False  
                    
                    # placeholder values for protein inference data
                    line.insert(1, -1)
                    line.insert(1, -1)
                    
                    fraction_protein = ''
                    try:
                        fraction_protein = str(fraction_protein_count[raw_filename])
                    except:
                        fraction_protein = '-1'
                        
                    fraction_peptide = ''
                    try:
                        fraction_peptide = str(fraction_peptide_count[raw_filename])
                    except:
                        fraction_peptide = '-1'
                        
                    fraction_psm = ''
                    try:
                        fraction_psm = str(fraction_psm_count[raw_filename])
                    except:
                        fraction_psm = '-1'
                    
                    # use the data from the set psm_quantitation file to fill in for missing protein inference data by fraction
                    line.insert(1, fraction_protein)
                    line.insert(1, fraction_peptide)
                    line.insert(1, fraction_psm)
                
            if valid_line == True:
                output_line = ''
                for value in line:
                    output_line += str(value) + '\t'
                    
                output_data.append(output_line.strip() + '\n')
        
        # add in a blank line to the tsv file
        if original_sample_filenames != list() and qc_file_flag:
            output_data.append('\n')
        
        with PdfPages(qc_path + 'plots-injection_time_histograms-' + data_set_name + '.pdf') as pdf:
            # generates the injection time histograms if the user left the sample_filenames.txt config file blank
            if original_sample_filenames == list():
                for sample_filename in sample_filenames:
                    if sample_filename in log_results:
                        injection_time_set_data = []
                        # does not check for 'any' (*) characters before the sample_filename because sample_filenames is filled with the exact filenames when original_sample_filenames is an empty list
                        injection_time_files = glob.glob(mzid_path + sample_filename + '*.injection.times.tsv')
                        for injection_time_f in injection_time_files:
                            injection_f = open(injection_time_f, 'r')
                            injection_time_lines = injection_f.readlines()
                            injection_f.close()
                            
                            for injection_time_line in injection_time_lines:
                                injection_val = float(injection_time_line.strip())
                                
                                injection_time_set_data.append(injection_val)
                        
                        if injection_time_set_data != []:                        
                            # generate histogram figure
                            bin_max = max(injection_time_set_data)
            
                            bin_time = 0.0
                            bin_edges = []
                            xtick_labels = [bin_time]
                            while bin_time <= bin_max - 10.0:
                                begin_time = bin_time
                                end_time = begin_time + 10.0
                                #xtick_labels.append(float((float(end_time) - float(begin_time)) / 2.0) + float(begin_time))
                                xtick_labels.append(end_time)
                                
                                bin_edges.append(begin_time)
                                bin_edges.append(end_time)
                                
                                bin_time += 10.0
                                
                            # last bin before bin_max
                            bin_edges.append(bin_time)
                            bin_edges.append(bin_max)
                            #xtick_labels.append(float((float(bin_max) - float(bin_time)) / 2.0) + float(bin_time))
                            # a bin for the maximum bin time only
                            bin_edges.append(bin_max)
                            bin_edges.append(bin_max + 10)
                            xtick_labels.append(bin_max)
                            
                            figure = plt.figure(figsize=(11, 8.5), dpi=72)
                            ax = figure.gca()
                            ax.set_xticks(xtick_labels) 
                            plt.grid(linestyle = '--')      
                            plt.hist(injection_time_set_data, bins = bin_edges, rwidth = 0.96, color = 'green', alpha = 0.75)
                            plt.title('Injection times histogram - ' + sample_filename)
                            plt.xlabel('Injection time (ms)')
                            plt.ylabel('Spectra (#)')  
                            plt.xticks(rotation = 'vertical') 
                            
                            pdf.savefig(figure)           
                            plt.close()   
            
            # add in the all together protein inference results from each set to the tsv file
            for sample_filename in original_sample_filenames:
                if sample_filename in log_results:             
                    miscleavage_rate = miscleavage_map[sample_filename]
                    
                    sample_log = log_results[sample_filename]
                    
                    sample_list = [sample_filename + '.txt']
                    
                    sample_list.insert(1, str(miscleavage_rate))
                    
                    # placeholder values between protein inference data and miscleavage rate for the set
                    for x in range(inf_injection_begin, inf_injection_end + 1):
                        sample_list.insert(1, -1)
                    
                    injection_time_set_data = []
                    # checks for 'any' (*) characters before the sample_filename because the user could have omitted characters in the set names from the beginning of the filenames
                    injection_time_files = glob.glob(mzid_path + '*' + sample_filename + '*.injection.times.tsv')
                    for injection_time_f in injection_time_files:
                        injection_f = open(injection_time_f, 'r')
                        injection_time_lines = injection_f.readlines()
                        injection_f.close()
                        
                        for injection_time_line in injection_time_lines:
                            injection_val = float(injection_time_line.strip())
                            
                            injection_time_set_data.append(injection_val)
                    
                    if injection_time_set_data != []:
                        sample_list.insert(1, np.std(injection_time_set_data))
                        sample_list.insert(1, np.mean(injection_time_set_data))
                        sample_list.insert(1, np.median(injection_time_set_data))
                        
                        # generate histogram figure
                        bin_max = max(injection_time_set_data)
        
                        bin_time = 0.0
                        bin_edges = []
                        xtick_labels = [bin_time]
                        while bin_time <= bin_max - 10.0:
                            begin_time = bin_time
                            end_time = begin_time + 10.0
                            #xtick_labels.append(float((float(end_time) - float(begin_time)) / 2.0) + float(begin_time))
                            xtick_labels.append(end_time)
                            
                            bin_edges.append(begin_time)
                            bin_edges.append(end_time)
                            
                            bin_time += 10.0
                            
                        # last bin before bin_max
                        bin_edges.append(bin_time)
                        bin_edges.append(bin_max)
                        #xtick_labels.append(float((float(bin_max) - float(bin_time)) / 2.0) + float(bin_time))
                        # a bin for the maximum bin time only
                        bin_edges.append(bin_max)
                        bin_edges.append(bin_max + 10)
                        xtick_labels.append(bin_max)
                        
                        figure = plt.figure(figsize=(11, 8.5), dpi=72)
                        ax = figure.gca()
                        ax.set_xticks(xtick_labels) 
                        plt.grid(linestyle = '--')      
                        plt.hist(injection_time_set_data, bins = bin_edges, rwidth = 0.96, color = 'green', alpha = 0.75)
                        plt.title('Injection times histogram - ' + sample_filename)
                        plt.xlabel('Injection time (ms)')
                        plt.ylabel('Spectra (#)')
                        plt.xticks(rotation = 'vertical')
                        
                        pdf.savefig(figure)           
                        plt.close() 
                    else:   
                        # placeholder values median, mean, and std. dev. injection times if no fraction data is available
                        sample_list.insert(1, -1)
                        sample_list.insert(1, -1)
                        sample_list.insert(1, -1)
                    
                    if label_type != 'free':
                        for psm_intensity in psm_intensity_data[sample_filename][::-1]:
                            sample_list.insert(1, str(psm_intensity))
                    
                    sample_list.insert(1, sample_log['"num_peptide_before_filtering"'])
                    sample_list.insert(1, sample_log['"num_psm_before_filtering"'])
                    sample_list.insert(1, sample_log['"final_protein_group_num"'])
                    sample_list.insert(1, sample_log['"final_peptide_num"'])
                    sample_list.insert(1, sample_log['"final_psm_num"'])
            
                    output_line = ''
                    for value in sample_list:
                        output_line += str(value) + '\t'
                    
                    output_data.append(output_line.strip() + '\n')
        
        # add in a blank line to the tsv file
        output_data.append('\n')
        
        # add in the all together protein inference results to the tsv file
        peptide_protein_log = log_results['peptide_protein']
        
        peptide_protein_list = ['peptide_protein.txt']
        peptide_protein_list.insert(1, peptide_protein_log['"num_peptide_before_filtering"'])
        peptide_protein_list.insert(1, peptide_protein_log['"num_psm_before_filtering"'])
        peptide_protein_list.insert(1, peptide_protein_log['"final_protein_group_num"'])
        peptide_protein_list.insert(1, peptide_protein_log['"final_peptide_num"'])
        peptide_protein_list.insert(1, peptide_protein_log['"final_psm_num"'])
        
        output_line = ''
        for value in peptide_protein_list:
            output_line += value + '\t'
        
        output_data.append(output_line.strip() + '\n')
        
        # add in a blank line to the tsv file
        output_data.append('\n')  
        
        for key in peptide_uniqueness_keys:
            fraction_str = 'fractions'
            if key != '4+':
                if int(key) == 1:
                    fraction_str = 'fraction'
                
            output_line = 'Number of peptides in ' + key + ' ' + fraction_str + ' \t \t' + str(peptide_uniqueness[key])
            
            output_data.append(output_line.strip() + '\n')   
                
        # open qc complete file for writing
        outfile = open(qc_path + 'qc-complete-' + data_set_name + '.tsv', 'w')
        for output_line in output_data:
            outfile.write(output_line)
        outfile.close()
        # end qc file management
    
    plt.ioff()
    
    if move_md5 == True:
        # move new psm_md5_checksums.tsv from protein_inference folder to psm_quantitation folder
        os.remove(quantitation_path + psm_md5_filename)
        shutil.move(output_path + psm_md5_filename, quantitation_path + psm_md5_filename)
            
    write_config_params = []
    write_config_params.append('psm_quantitation_path = ' + quantitation_path)
    write_config_params.append('fasta_path = ' + fasta_file)
    write_config_params.append('min_psm_per_peptide = ' + str(min_psm_per_peptide))
    write_config_params.append('min_peptide_per_protein = ' + str(min_peptide_per_protein))
    write_config_params.append('optional_n_term_met_cleavage = ' + str(optional_n_term_met_cleavage))
    write_config_params.append('cleavage_rule = ' + cleavage_rule)
    write_config_params.append('max_miscleavage = ' + str(max_miscleavage))
    write_config_params.append('min_cleavage_terminus = ' + str(min_cleavage_terminus))
    write_config_params.append('min_length = ' + str(min_length))
    write_config_params.append('max_length = ' + str(max_length))
    write_config_params.append('modification = ' + mod)
    write_config_outfile = output_path + 'step4_inference.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'sample_filenames.txt', write_config_outfile, 'sample_filenames.txt settings')

# ******************************************************************************

def psm_quantitation_gpquest(options, sample_filenames, correction_matrix_lines):
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2]
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    quantitation_path = output_folder_path + 'step4a-psm_quantitation\\'
    qc_path = output_folder_path + 'quality_control\\'
    fdr = float(options['false_discovery_rate']) / 100.0
    ir_score_threshold = float(options['ir_score'])
    morpheus_score_threshold = float(options['morpheus_score'])
    label_type = options['label_type']
    correction = str(options['correction_matrix']).lower()
    
    output_path = quantitation_path
    # determines if data was archived by the user and will need to be rerun
    output_path_exists = os.path.exists(output_path)
    
    # creates the output path folders
    make_path(output_path)
    make_path(qc_path)
    
    # GPQuest search result data columns to be extracted
    ms2_col = 1
    charge_col = 3
    ox_peaks_col = 10
    ox_intensity_col = 11 
    ox_ir_col = 12
    ox_ions_col = 13
    intensities_col = 17
    protein_col = 19
    pep_col = 21
    mod_pep_col = 22
    ir_score_col = 46
    morpheus_score_col = 47
    hyper_score_col = 48
    n_glycan_col = 54
    n_glycans_col = 40
    o_glycan_col = 41
    o_glycans_col = 42
    fdr_col = 59
    # number of header rows in GPQuest search result files
    gpquest_header = 1
    
    itraq4_channels = ['114', '115', '116', '117']
    itraq8_channels = ['113', '114', '115', '116', '117', '118', '119', '121']
    tmt10_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
    tmt11_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
    tmt16_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
    
    if correction == 'true':
        correction = True
    else:
        correction = False
    
    # retrieve tmt correction matrix if necessary
    if label_type == '4plex':
        channels = itraq4_channels
        
        if correction:
            correction_matrix = corr_factors.itraq4plex_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.itraq4plex_identity_matrix()
    elif label_type == '8plex':
        channels = itraq8_channels
        
        if correction:
            correction_matrix = corr_factors.itraq8plex_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.itraq8plex_identity_matrix()
    elif label_type == '10plex':
        channels = tmt10_channels
        
        if correction:
            correction_matrix = corr_factors.tmt10_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.tmt10_identity_matrix()
    elif label_type == '11plex':
        channels = tmt11_channels
        
        if correction:
            correction_matrix = corr_factors.tmt11_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.tmt11_identity_matrix()
    elif label_type == '16plex':
        channels = tmt16_channels
        
        if correction:
            correction_matrix = corr_factors.tmt16_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.tmt16_identity_matrix()
    elif label_type == 'free':
        channels = []
    else:
        raise Exception('label_type must be either "free", "10plex", "11plex", "16plex", "4plex", or "8plex"')
    
    valid_extensions = ['gpquest.tsv']

    mzid_file_ext = ''    
    mzid_path_files = glob.glob(mzid_path + '*.*')
    for mzid_f in mzid_path_files:
        file_ext = str('.'.join(mzid_f.split('.')[1:]))
        
        if file_ext.lower() in valid_extensions:    
            if mzid_file_ext == '':
                mzid_file_ext = file_ext
                break
                
    mzid_files = glob.glob(mzid_path + '*.' + mzid_file_ext)
    if sample_filenames == list():
        for mzid_f in mzid_files:
            mzid_f_basename = os.path.basename(mzid_f).replace('.' + mzid_file_ext, '')
            
            if mzid_f_basename not in sample_filenames:
                sample_filenames.append(mzid_f_basename)
                
        # removes non-unique names in case a set file was left over from previous analysis - should not be necessary for this step because search results do not currently have sets together, but should not cause problems
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)
    # adds in each fraction to the sample_filenames list for this step to get data for each fraction
    #else:
    #    for mzid_f in mzid_files:
    #        mzid_f_basename = os.path.basename(mzid_f).replace('.' + mzid_file_ext, '')
    #        
    #        if mzid_f_basename not in sample_filenames:
    #            sample_filenames.append(mzid_f_basename)
    
    sample_filenames.sort(key = natural_keys)
    
    # determines if the code in this function needs to be rerun or not by comparing md5 checksums of the search result files to the md5 checksums of those files the last time psm quantitation was run
    updated_sample_filenames = set()
    
    move_md5 = False
    # number of header lines in md5 checksum files
    md5_header = 1
    search_md5_filename = 'step4_search_md5_checksums.tsv'
    
    files_should_exist = [qc_path + 'stats-number_log_psm-' + data_set_name + '.tsv', output_path + 'step4_psm.config']
    
    if not os.path.exists(mzid_path + search_md5_filename) or output_path_exists == False:
        search_md5_gpquest(mzid_path, mzid_path, sample_filenames)
        
        for sample_filename in sample_filenames:
            updated_sample_filenames.add(sample_filename)
    else:
        try:
            for filepath in files_should_exist:
                if not os.path.exists(filepath):
                    for sample_filename in sample_filenames:
                        updated_sample_filenames.add(sample_filename)
            
            for sample_filename in sample_filenames:
                if not os.path.exists(output_path + sample_filename + '.txt'):# or not os.path.exists(output_path + sample_filename + '_threshold_scores.tsv'):
                    updated_sample_filenames.add(sample_filename)
            
            f = open(mzid_path + search_md5_filename, 'r')
            previous_md5_lines = f.readlines()
            f.close()
            
            search_md5_gpquest(mzid_path, quantitation_path, sample_filenames)
                
            f = open(quantitation_path + search_md5_filename, 'r')
            current_md5_lines = f.readlines()
            f.close()
            
            previous_md5 = {}
            for line in previous_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                previous_md5[filename] = checksum
                
            current_md5 = {}
            for line in current_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                current_md5[filename] = checksum
            
            for key in current_md5:
                run_sample = False
                
                if key not in previous_md5:
                    run_sample = True        
                elif previous_md5[key] != current_md5[key]:
                    run_sample = True
                        
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(quantitation_path + sample_filename + '.txt'):
                            updated_sample_filenames.add(sample_filename)
                        break
                        
            for key in previous_md5:
                run_sample = False
                
                if key not in current_md5:
                    run_sample = True
                elif current_md5[key] != previous_md5[key]:
                    run_sample = True
                    
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(quantitation_path + sample_filename + '.txt'):
                            updated_sample_filenames.add(sample_filename)
                        break
       
            move_md5 = True
        except:
            if os.path.exists(quantitation_path + search_md5_filename):
                os.remove(quantitation_path + search_md5_filename)
                
            search_md5_gpquest(mzid_path, mzid_path, sample_filenames)
            
            for sample_filename in sample_filenames:
                updated_sample_filenames.add(sample_filename) 
    
    updated_sample_filenames = list(updated_sample_filenames)
    updated_sample_filenames.sort(key = natural_keys)
    
    set_count = 0
    # generate psm quantitation results
    for itraq_name in updated_sample_filenames:
        set_count += 1
    
        # test whether the sample filename corresponds to a fraction name exactly                
        mzid_fraction_test = mzid_path + itraq_name + '.' + mzid_file_ext
        mzid_fraction_flag = False
        if os.path.exists(mzid_fraction_test):
            mzid_fraction_flag = True
                  
        if mzid_fraction_flag:
            mzid_files = glob.glob(mzid_path + itraq_name + '.' + mzid_file_ext)
        else:
            mzid_files = glob.glob(mzid_path + '*' + itraq_name + '*.' + mzid_file_ext)
        
        output_file = output_path + itraq_name + '.txt'
        
        print 'Generating psm quantitation results for set ' + str(itraq_name) + ' (set ' + str(set_count) + ' of ' + str(len(updated_sample_filenames)) + ')'
        print ''
        sys.stdout.flush()
   
        # counts the number of files that were processed for each set
        num_log = str(len(mzid_files))
    
        files_len = len(set(mzid_files))
    
        percent_interval = floor(100.0 / files_len)
        if percent_interval < 5.0:
            percent_interval = 5.0
        
        percent_checked = 0.0
        num_files_checked = 0
        
        print 'Reading search_results and mzML files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
        sys.stdout.flush()
    
        data = []
        for gpquest_f in mzid_files:
            num_files_checked += 1
            
            f = open(gpquest_f, 'r')
            lines = f.readlines()
            f.close()
            
            basename = os.path.basename(gpquest_f).replace('.' + mzid_file_ext, '').strip()
            mzml_file = basename + '.mzML'
            
            scans_per_file = []
            for line in lines[gpquest_header:]:
                line = line.strip().split('\t')
                ms2 = int(line[ms2_col].strip())
                    
                scans_per_file.append(ms2)
            
            if label_type == 'free':
                precursor_data = Spectrum.get_mz_intensity_by_scans_and_precursor_data(mzml_path + mzml_file, scans_per_file)
            
            for line in lines[gpquest_header:]:
                line = line.strip().split('\t')
                ms2 = int(line[ms2_col].strip())
                charge = line[charge_col].strip()  
                ox_peaks = line[ox_peaks_col].strip()
                ox_intensity = line[ox_intensity_col].strip()
                ox_ir = line[ox_ir_col].strip()
                ox_ions = line[ox_ions_col].strip()
                intensities = line[intensities_col].strip().split(';')
                protein = line[protein_col].strip()
                pep = line[pep_col].strip()
                mod_pep = line[mod_pep_col].strip()
                ir_score = float(line[ir_score_col].strip())
                morpheus_score = float(line[morpheus_score_col].strip())
                hyper_score = float(line[hyper_score_col].strip())
                n_glycan = line[n_glycan_col].strip().split(':')[0].strip()
                n_glycans = line[n_glycans_col].strip()
                o_glycan = line[o_glycan_col].strip().split(':')[0].strip()
                o_glycans = line[o_glycans_col].strip()
                gpquest_fdr = float(line[fdr_col].strip())
                
                # filter by the FDR cutoff, the IR score threshold, and the Morpheus score threshold
                if gpquest_fdr <= fdr and ir_score >= ir_score_threshold and morpheus_score >= morpheus_score_threshold:
                    # keep the same formatting as MS-GF+ psm quantitation files
                    if protein[-1] == ';':
                        protein = protein[:-1]
                    
                    filename = basename + '.' + str(ms2) + '.' + str(ms2) + '.' + charge
                    
                    output_row = []
                    output_row.append(pep)
                    output_row.append(mod_pep)
                    
                    # to validate precursor intensity (assumed true if non-label free data)
                    precursor_flag = True
                    if label_type != 'free':
                        intensity_map = {}
                        for intensity_data in intensities:
                            intensity_data = intensity_data.strip().split('/')
                            channel = intensity_data[0].strip()
                            intensity = float(intensity_data[1].strip())
                            
                            if channel in intensity_map:
                                raise Exception(channel + ' is already in intensity_map')
                            
                            intensity_map[channel] = intensity
                        
                        tag_intensities = []
                        for channel in channels:
                            tag_intensities.append(intensity_map[channel])
                            
                        corrected_tag_intensities = list(np.dot(correction_matrix, tag_intensities))
                    
                        for intensity_index, corrected_intensity in enumerate(corrected_tag_intensities):
                            if corrected_intensity < 0:
                                # resets negative values to 0 since negative intensities do not make sense in the context of mass spectrometry proteomics
                                corrected_tag_intensities[intensity_index] = 0
                    
                        output_row.extend(corrected_tag_intensities)
                    else:
                        # get MS1 precursor intensity
                        precursor_intensity = str(precursor_data[1][ms2]['peak intensity'])
                        
                        # checks if the precursor intensity was missing - stored as 'BAD' instead of float
                        if precursor_intensity != 'BAD':
                            precursor_intensity = float(precursor_intensity)
                            output_row.append(precursor_intensity)
                        else:
                            precursor_flag = False
                    
                    output_row.append(ox_peaks)
                    output_row.append(ox_intensity)
                    output_row.append(ox_ir)
                    output_row.append(ox_ions)
                    output_row.append(n_glycan)
                    output_row.append(n_glycans)
                    output_row.append(o_glycan) 
                    output_row.append(o_glycans)                   
                    output_row.append(protein)
                    output_row.append(ms2)
                    output_row.append(ir_score)
                    output_row.append(morpheus_score)
                    output_row.append(hyper_score)
                    output_row.append(filename)
                    
                    if precursor_flag:
                        if protein.lower() != 'decoy':
                            data.append(output_row)
        
            current_percent = ((100.0 * num_files_checked) / files_len)
            if current_percent >= (percent_checked + percent_interval):
                percent_checked = floor(current_percent)
            
                print 'Reading search_results and mzML files progress... ' + str(percent_checked) + ' %'
                sys.stdout.flush()      
    
        if percent_checked < 100.0:
            print 'Reading search_results and mzML files progress... 100 %'
            sys.stdout.flush()
        
        print '-'
        sys.stdout.flush()
        
        if data != []:
            write_itraq_to_file(data, output_file)
        
        # begin number_log file creation
        # create log file with the number of mzid files grouped together for each set psm_quantitation file
        # report how many fractions were used for set psm quantitation files so that the user can verify pipeline worked correctly
        f = open(output_path + str(itraq_name) + '_number_log.tsv', 'w')
        f.write('Set name\t# fraction files\n')
        f.write(str(itraq_name) + '\t' + num_log)
        f.close()
        # end number_log file creation

    # combine the number_log files into one final output file
    combine_number_log_files(output_path, qc_path, data_set_name, list(sample_filenames))
    
    # delete combined labeling_efficiency qc file if it exists because labeling efficiency is not run for intact glycopeptide searches (NOTE: individual labeling efficiency files still exist for later use if needed)
    if os.path.exists(qc_path + 'labeling_efficiency-' + data_set_name + '.tsv'):
        os.remove(qc_path + 'labeling_efficiency-' + data_set_name + '.tsv')
    
    if move_md5 == True:
        # move new search_md5_checksums.tsv from psm_quantitation folder to the search_results folder
        os.remove(mzid_path + search_md5_filename)
        shutil.move(quantitation_path + search_md5_filename, mzid_path + search_md5_filename)
    
    # make a copy of the correction_factors.tsv config file if the correction factors were used
    if correction:
        shutil.copyfile(config_path + 'correction_factors.tsv', output_path + 'step4_correction_factors_config.tsv')
    
    write_config_params = []
    write_config_params.append('mzml_path = ' + mzml_path)
    write_config_params.append('search_results_path = ' + mzid_path)
    write_config_params.append('false_discovery_rate = ' + str(fdr * 100.0) + '%')
    write_config_params.append('ir_score = ' + str(ir_score_threshold))
    write_config_params.append('morpheus_score = ' + str(morpheus_score_threshold))
    write_config_params.append('correction_matrix = ' + str(correction).lower())
    write_config_outfile = output_path + 'step4_psm.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'sample_filenames.txt', write_config_outfile, 'sample_filenames.txt settings')

def psm_quantitation(options, sample_filenames, correction_matrix_lines):      
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2]
    fasta_file = options['fasta_path']
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    quantitation_path = output_folder_path + 'step4a-psm_quantitation\\'
    qc_path = output_folder_path + 'quality_control\\'
    fdr = float(options['false_discovery_rate']) / 100.0
    label_type = options['label_type']
    mod = str(options['modification']).lower()
    correction = str(options['correction_matrix']).lower()
    mz_diff_tmt = float(options['mz_diff_tmt'])
    mz_diff_itraq = float(options['mz_diff_itraq'])
    label_masses = get_label_masses(label_type, mzid_path + 'step3.config')
    
    itraq4 = [114.1, 115.1, 116.1, 117.1]
    itraq8 = [113.1, 114.1, 115.1, 116.1, 117.1, 118.1, 119.1, 121.1]
    tmt10 = [126.127726, 127.124761, 127.131081, 128.128116, 128.134436, 129.131471, 129.137790, 130.134825, 130.141145, 131.138180]
    tmt11 = [126.127726, 127.124761, 127.131081, 128.128116, 128.134436, 129.131471, 129.137790, 130.134825, 130.141145, 131.138180, 131.144499]
    tmt16 = [126.127726, 127.124761, 127.131081, 128.128116, 128.134436, 129.131471, 129.137790, 130.134825, 130.141145, 131.138180, 131.144499, 132.141535, 132.147855, 133.144890, 133.151210, 134.148245]
    free_masses = []
    search_engine = steps(mzid_path + 'step3.config')['search_engine'].lower()
    valid_extensions = ['mzid', 'pepxml', 'pep.xml']
    
    if correction == 'true':
        correction = True
    else:
        correction = False
    
    fasta_map = get_fasta_protein_seq_map(fasta_file)
    
    mzid_file_ext = ''    
    mzid_path_files = glob.glob(mzid_path + '*.*')
    for mzid_f in mzid_path_files:
        file_ext = str('.'.join(mzid_f.split('.')[1:]))
        
        if file_ext.lower() in valid_extensions:    
            if mzid_file_ext == '':
                mzid_file_ext = file_ext
                break
                
    mzid_files = glob.glob(mzid_path + '*.' + mzid_file_ext)
    if sample_filenames == list():
        for mzid_f in mzid_files:
            mzid_f_basename = os.path.basename(mzid_f).replace('.' + mzid_file_ext, '')
            
            if mzid_f_basename not in sample_filenames:
                sample_filenames.append(mzid_f_basename)
                
        # removes non-unique names in case a set file was left over from previous analysis - should not be necessary for this step because search results do not currently have sets together, but should not cause problems
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)
    # adds in each fraction to the sample_filenames list for this step to get data for each fraction
    #else:
    #    for mzid_f in mzid_files:
    #        mzid_f_basename = os.path.basename(mzid_f).replace('.' + mzid_file_ext, '')
    #        
    #        if mzid_f_basename not in sample_filenames:
    #            sample_filenames.append(mzid_f_basename)
    
    sample_filenames.sort(key = natural_keys)
    
    output_path = quantitation_path
    # determines if data was archived by the user and will need to be rerun
    output_path_exists = os.path.exists(output_path)
    
    # creates the output path folders
    make_path(output_path)
    make_path(qc_path)
    
    # determines if the code in this function needs to be rerun or not by comparing md5 checksums of the search result files to the md5 checksums of those files the last time psm quantitation was run
    updated_sample_filenames = set()
    
    move_md5 = False
    # number of header lines in md5 checksum files
    md5_header = 1
    search_md5_filename = 'step4_search_md5_checksums.tsv'
    
    files_should_exist = [qc_path + 'stats-number_log_psm-' + data_set_name + '.tsv', qc_path + 'stats-psm_minimum_threshold_scores-' + data_set_name + '.tsv', output_path + 'step4_psm.config']
    
    if not os.path.exists(mzid_path + search_md5_filename) or output_path_exists == False:
        search_md5(mzid_path, mzid_path, sample_filenames)
        
        for sample_filename in sample_filenames:
            updated_sample_filenames.add(sample_filename)
    else:
        try:
            for filepath in files_should_exist:
                if not os.path.exists(filepath):
                    for sample_filename in sample_filenames:
                        updated_sample_filenames.add(sample_filename)
            
            for sample_filename in sample_filenames:
                if not os.path.exists(output_path + sample_filename + '.txt') or not os.path.exists(output_path + sample_filename + '_threshold_scores.tsv'):
                    updated_sample_filenames.add(sample_filename)
            
            f = open(mzid_path + search_md5_filename, 'r')
            previous_md5_lines = f.readlines()
            f.close()
            
            search_md5(mzid_path, quantitation_path, sample_filenames)
                
            f = open(quantitation_path + search_md5_filename, 'r')
            current_md5_lines = f.readlines()
            f.close()
            
            previous_md5 = {}
            for line in previous_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                previous_md5[filename] = checksum
                
            current_md5 = {}
            for line in current_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                current_md5[filename] = checksum
            
            for key in current_md5:
                run_sample = False
                
                if key not in previous_md5:
                    run_sample = True        
                elif previous_md5[key] != current_md5[key]:
                    run_sample = True
                        
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(quantitation_path + sample_filename + '.txt'):
                            updated_sample_filenames.add(sample_filename)
                        break
                        
            for key in previous_md5:
                run_sample = False
                
                if key not in current_md5:
                    run_sample = True
                elif current_md5[key] != previous_md5[key]:
                    run_sample = True
                    
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(quantitation_path + sample_filename + '.txt'):
                            updated_sample_filenames.add(sample_filename)
                        break
       
            move_md5 = True
        except:
            if os.path.exists(quantitation_path + search_md5_filename):
                os.remove(quantitation_path + search_md5_filename)
                
            search_md5(mzid_path, mzid_path, sample_filenames)
            
            for sample_filename in sample_filenames:
                updated_sample_filenames.add(sample_filename) 
    
    updated_sample_filenames = list(updated_sample_filenames)
    updated_sample_filenames.sort(key = natural_keys)
    
    # mz difference tolerance is in units of Daltons (+/-)
    if label_type == '4plex':
        tag_masses = itraq4
        mz_diff_tolerance = mz_diff_itraq
    elif label_type == '8plex':
        tag_masses = itraq8
        mz_diff_tolerance = mz_diff_itraq
    elif label_type == '10plex':
        tag_masses = tmt10
        mz_diff_tolerance = mz_diff_tmt
    elif label_type == '11plex':
        tag_masses = tmt11
        mz_diff_tolerance = mz_diff_tmt
    elif label_type == '16plex':
        tag_masses = tmt16
        mz_diff_tolerance = mz_diff_tmt
    elif label_type == 'free':
        # placeholder values - not used
        tag_masses = free_masses
        mz_diff_tolerance = 0.1
    
    score_eq = ''
    set_count = 0
    # generate psm quantitation results
    for itraq_name in updated_sample_filenames:
        set_count += 1
    
        # test whether the sample filename corresponds to a fraction name exactly                
        mzid_fraction_test = mzid_path + itraq_name + '.' + mzid_file_ext
        mzid_fraction_flag = False
        if os.path.exists(mzid_fraction_test):
            mzid_fraction_flag = True
                  
        if mzid_fraction_flag:
            mzid_files = glob.glob(mzid_path + itraq_name + '.' + mzid_file_ext)
        else:
            mzid_files = glob.glob(mzid_path + '*' + itraq_name + '*.' + mzid_file_ext)
        
        output_file = output_path + itraq_name + '.txt'
        
        print 'Generating psm quantitation results for set ' + str(itraq_name) + ' (set ' + str(set_count) + ' of ' + str(len(updated_sample_filenames)) + ')'
        print ''
        sys.stdout.flush()
        
        data_results = itraq(mzid_files, mzml_path, quantitation_path,
                    itraq_name = itraq_name, 
                    search_engine = search_engine,
                    correction_matrix_lines = correction_matrix_lines,
                    fdr = fdr, 
                    itraq_tag_mass = tag_masses,
                    mz_diff_tolerance = mz_diff_tolerance,
                    label_type = label_type,
                    mod = mod,
                    correction = correction)
                                
        data = data_results[0]
        threshold_dict = data_results[1]
        score_eq = data_results[2]
   
        # counts the number of files that were processed for each set
        num_log = str(len(mzid_files))
   
        if data != []:
            write_itraq_to_file(data, output_file)
            
        # begin number_log file creation
        # create log file with the number of mzid files grouped together for each set psm_quantitation file
        # report how many fractions were used for set psm quantitation files so that the user can verify pipeline worked correctly
        f = open(output_path + str(itraq_name) + '_number_log.tsv', 'w')
        f.write('Set name\t# fraction files\n')
        f.write(str(itraq_name) + '\t' + num_log)
        f.close()
        # end number_log file creation
        
        # begin labeling_efficiency file creation
        if label_type != 'free':
            generate_labeling_efficiency_file(output_folder_path, output_file, fasta_map, label_type, label_masses)
        # end labeling_efficiency file_creation
   
        # begin threshold_scores file creation
        # create a psm threshold score log for the current set         
        # generate header line
        min_charge = 999
        max_charge = -1
        threshold_keys = sorted(threshold_dict)
        
        header_line = 'Filename' + '\t'
        
        for key in threshold_keys:
            charge_keys = sorted(threshold_dict[key])
            
            for charge in charge_keys:
                charge = int(charge)
                
                if charge < min_charge:
                    min_charge = charge
                    
                if charge > max_charge:
                    max_charge = charge
                    
        for charge in range(min_charge, max_charge + 1):
            header_line += '+' + str(charge) + ' charge threshold score' + '\t'
            
        header_line += 'Scoring parameter' + '\t'
        
        f = open(output_path + str(itraq_name) + '_threshold_scores.tsv', 'w')
        
        f.write(header_line.strip() + '\n')
        
        for key in threshold_keys:
            charge_keys = sorted(threshold_dict[key])
            output_line = str(key) + '\t'
            
            for charge in range(min_charge, max_charge + 1):
                if charge in charge_keys:
                    output_line += str(threshold_dict[key][charge]) + '\t'
                else:
                    # placeholder value for no threshold score
                    output_line += 'None' + '\t'
                    
            output_line += str(score_eq) + '\t'
                
            f.write(output_line.strip() + '\n')
        
        f.close()
        # end threshold_scores file creation
        
        # the scoring equation will be same for all results and so only need to retrieve once
        if score_eq == '':
            score_eq = data_results[2]

    # combine the number_log files into one final output file
    combine_number_log_files(output_path, qc_path, data_set_name, list(sample_filenames))
    
    # begin labeling_efficiency files combining
    # output the labeling efficiency information to the qc_path
    if label_type != 'free':
        combine_labeling_efficiency_files(data_root, output_folder_path, sample_filenames, label_type)
    else:
        # delete combined labeling_efficiency qc file if it exists because label free data has no labeling efficiency (NOTE: individual labeling efficiency files still exist for later use if needed)
        if os.path.exists(qc_path + 'labeling_efficiency-' + data_set_name + '.tsv'):
            os.remove(qc_path + 'labeling_efficiency-' + data_set_name + '.tsv')
    # end labeling_efficiency files combining
    
    # combine the the threshold_scores files into one final output file
    combine_threshold_scores_files(output_path, qc_path, data_set_name, list(sample_filenames))
    
    if move_md5 == True:
        # move new search_md5_checksums.tsv from psm_quantitation folder to the search_results folder
        os.remove(mzid_path + search_md5_filename)
        shutil.move(quantitation_path + search_md5_filename, mzid_path + search_md5_filename)
    
    # make a copy of the correction_factors.tsv config file if the correction factors were used
    if correction:
        shutil.copyfile(config_path + 'correction_factors.tsv', output_path + 'step4_correction_factors_config.tsv')
    
    write_config_params = []
    write_config_params.append('mzml_path = ' + mzml_path)
    write_config_params.append('search_results_path = ' + mzid_path)
    write_config_params.append('fasta_path = ' + fasta_file)
    write_config_params.append('false_discovery_rate = ' + str(fdr * 100.0) + '%')
    write_config_params.append('mz_diff_tolerance = ' + str(mz_diff_tolerance))
    write_config_params.append('correction_matrix = ' + str(correction).lower())
    write_config_outfile = output_path + 'step4_psm.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'sample_filenames.txt', write_config_outfile, 'sample_filenames.txt settings')

def get_fasta_protein_seq_map(fasta_file):
    # create map of proteins to peptide sequences from the fasta file in order to get site positions in the protein sequence
    f = open(fasta_file, 'r')
    fasta_lines = f.readlines()
    f.close()
        
    fasta_map = {}
    fasta_protein = ''
    fasta_seq = ''
    for line in fasta_lines:
        line = line.strip()
        
        if line[0] == '>':
            if fasta_protein != '' and fasta_seq != '':
                fasta_map[fasta_protein] = fasta_seq
                
            fasta_protein = line.split('>')[1].strip().split(' ')[0].strip()
            fasta_seq = ''
        else:
            fasta_seq += line
            
    if fasta_protein != '' and fasta_seq != '':
        fasta_map[fasta_protein] = fasta_seq
        
    return fasta_map

def combine_number_log_files(output_path, qc_path, data_set_name, sample_filenames):
    number_files_all = glob.glob(output_path + '*_number_log.tsv')
    number_files = []
    
    for number_file in number_files_all:
        number_basename = os.path.basename(number_file).split('_number_log.tsv')[0]
        
        if number_basename in sample_filenames:
            number_files.append(number_file)
          
    number_files.sort(key = natural_keys)
                      
    output_header = ''
    output_data = []
    for number_f in number_files:
        f = open(number_f, 'r')
        lines = f.readlines()
        f.close()
        
        if output_header == '':
            output_header = lines[0]
            
        output_data.append(lines[1])
        
    f = open(qc_path + 'stats-number_log_psm-' + data_set_name + '.tsv', 'w')
    f.write(str(output_header).strip() + '\n')
    
    for row in output_data:
        f.write(str(row).strip() + '\n')
    f.close()   

def combine_threshold_scores_files(output_path, qc_path, data_set_name, sample_filenames):
    threshold_files_all = glob.glob(output_path + '*_threshold_scores.tsv')
    threshold_files = []

    # make sure the combined psm threshold score log file is not included
    for threshold_file in threshold_files_all:
        threshold_basename = os.path.basename(threshold_file).split('_threshold_scores.tsv')[0]
        
        if threshold_basename in sample_filenames:
            threshold_files.append(threshold_file)
            
    threshold_files.sort(key = natural_keys)
    
    threshold_scores_output_data = []
    
    min_charge = 999
    max_charge = -1
    
    threshold_data = {}
    for threshold_file in threshold_files:  
        g = open(threshold_file, 'r')
        threshold_lines = g.readlines()
        g.close()
        
        header_line = threshold_lines[0].strip().split('\t')
        charge_strs = header_line[1:-1]
        
        charges = set()
        for charge_str in charge_strs:
            charge = int(charge_str.strip().split(' ')[0].split('+')[-1])
            charges.add(charge)
            
            if charge < min_charge:
                min_charge = charge
            
            if charge > max_charge:
                max_charge = charge
                
        charges = sorted(charges)
        
        for line in threshold_lines[1:]:
            split_line = line.strip().split('\t')
            filename = split_line[0].strip()
            
            if filename not in threshold_data:
                threshold_data[filename] = {}
            
            scores = split_line[1:-1]
            
            for score, charge in zip(scores, charges):
                threshold_data[filename][charge] = score
                
            threshold_data[filename]['score eq'] = split_line[-1]
    
    header_line = 'Filename' + '\t'
                
    for charge in range(min_charge, max_charge + 1):
        header_line += '+' + str(charge) + ' charge threshold score' + '\t'
        
    header_line += 'Scoring parameter' + '\t'
    
    threshold_scores_output_data.append(header_line.strip() + '\n') 
    
    threshold_keys = sorted(threshold_data)
    threshold_keys.sort(key = natural_keys)
    
    for key in threshold_keys:
        file_keys = sorted(threshold_data[key])
        charge_keys = []
        
        for file_key in file_keys:
            if file_key != 'score eq':
                charge_keys.append(file_key)
        
        output_line = str(key) + '\t'
        
        for charge in range(min_charge, max_charge + 1):
            if charge in charge_keys:
                output_line += str(threshold_data[key][charge]) + '\t'
            else:
                # placeholder value for no threshold score
                output_line += 'None' + '\t'
    
        score_eq = threshold_data[key]['score eq']
        output_line += str(score_eq) + '\t'
        
        threshold_scores_output_data.append(output_line.strip() + '\n')            
    
    # create combined psm threshold score log for all sets specified in the sample_filenames.txt config file    
    f = open(qc_path + 'stats-psm_minimum_threshold_scores-' + data_set_name + '.tsv', 'w')
    for threshold_scores_output_line in threshold_scores_output_data:
        f.write(threshold_scores_output_line)
    f.close()

def get_scans_from_psms(psms):
    scan_list = []
    for end_scan, peptide, modified_peptide,\
            score, proteins, is_decoy, spectrum in psms:
        scan_list.append(end_scan)
    return scan_list

def get_mzml_file_names_from_psms(psms):
    mzml_file_name_list = []
    for end_scan, peptide, modified_peptide, \
            score, proteins, is_decoy, spectrum in psms:
        mzml_file_name_list.append(''.join(spectrum.split('.')[:-3]) + '.mzML')
    return mzml_file_name_list
  

def itraq(identification_files, mzml_path, quantitation_path,
          itraq_name,
          search_engine,
          correction_matrix_lines, 
          fdr = 0.01, 
          score_used_for_ranking = 'mvh',
          decoy_prefix = 'rev_', 
          decoy_suffix = ':reversed', 
          is_score_larger_better = True,
          itraq_tag_mass = [114.1, 115.1, 116.1, 117.1],
          mz_diff_tolerance = 0.06,
          label_type = '',
          mod = 'global',
          correction = True):
    
    # retrieve tmt correction matrix if necessary
    if label_type == '4plex':
        if correction:
            correction_matrix = corr_factors.itraq4plex_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.itraq4plex_identity_matrix()
    elif label_type == '8plex':
        if correction:
            correction_matrix = corr_factors.itraq8plex_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.itraq8plex_identity_matrix()
    elif label_type == '10plex':
        if correction:
            correction_matrix = corr_factors.tmt10_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.tmt10_identity_matrix()
    elif label_type == '11plex':
        if correction:
            correction_matrix = corr_factors.tmt11_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.tmt11_identity_matrix()
    elif label_type == '16plex':
        if correction:
            correction_matrix = corr_factors.tmt16_correction_matrix(correction_matrix_lines, quantitation_path)
        else:
            correction_matrix = corr_factors.tmt16_identity_matrix()
    elif label_type != 'free':
        raise Exception('label_type parameter not set to "free", "10plex", "11plex", "16plex", "4plex", or "8plex"')  
    
    psms = []
    threshold_dict = {}
    score_eq = ''
    if search_engine == 'myrimatch':
        # if myrimatch was the search engine, sets default score used for ranking to 'xcorr'
        score_used_for_ranking = 'XCorr'
        decoy_prefix = rcp(filepath = config_path + 'myrimatch.cfg', param_name = 'DecoyPrefix')
        psms_results = pepfdr.psms_at_fdr(identification_files, fdr, 
                                  score_used_for_ranking,
                                  decoy_prefix, 
                                  decoy_suffix, 
                                  is_score_larger_better,
                                  mod)
        psms = psms_results[0]
        threshold_dict[itraq_name] = psms_results[1]
        score_eq = psms_results[2]
    elif search_engine == 'comet':
        # if comet was the search engine, sets default score used for ranking to 'xcorr'
        score_used_for_ranking = 'XCorr'
        decoy_prefix = rcp(filepath = config_path + 'comet.params', param_name = 'decoy_prefix')
        psms_results = pepfdr.psms_at_fdr(identification_files, fdr, 
                                  score_used_for_ranking,
                                  decoy_prefix, 
                                  decoy_suffix, 
                                  is_score_larger_better,
                                  mod)
        psms = psms_results[0]
        threshold_dict[itraq_name] = psms_results[1]
        score_eq = psms_results[2]
    elif search_engine == 'msgf':
        files_len = len(identification_files)
    
        percent_interval = floor(100.0 / files_len)
        if percent_interval < 5.0:
            percent_interval = 5.0
    
        percent_checked = 0.0
        num_files_checked = 0
    
        print 'Reading search_results files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
        sys.stdout.flush()
        for f in identification_files:
            num_files_checked += 1
            
            mzident = mzid.Mzid(f)
            psms_results = mzident.psms_at_fdr(fdr, is_score_larger_better = is_score_larger_better, mod = mod) 
            psms.extend(psms_results[0])
            threshold_dict[os.path.basename(f)] = psms_results[1]
            score_eq = psms_results[2]
            
            current_percent = ((100.0 * num_files_checked) / files_len)
            if current_percent >= (percent_checked + percent_interval):
                percent_checked = floor(current_percent)
            
                print 'Reading search_results files progress... ' + str(percent_checked) + ' %'
                sys.stdout.flush()
        
        if percent_checked < 100.0:
            print 'Reading search_results files progress... 100 %'
            sys.stdout.flush()
            
        print ''
        sys.stdout.flush() 
    else:
        raise Exception('search engine should be msgf, comet, or myrimatch') 

    scan_list = get_scans_from_psms(psms)
    mzml_file_names = get_mzml_file_names_from_psms(psms)
    data = []

    files_len = len(set(mzml_file_names))
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0
    
    percent_checked = 0.0
    num_files_checked = 0
    
    print 'Reading mzML files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for mzml_file in set(mzml_file_names):
        num_files_checked += 1
        
        scans_per_file = []
        psms_per_file = []
        i = 0
        for f in mzml_file_names:
            if f == mzml_file:
                scans_per_file.append(scan_list[i])
                psms_per_file.append(psms[i])
            i += 1

        ms2_data = MS2.get_all_ms2_spectra_from_mzml(mzml_path + mzml_file)
        
        if label_type == 'free':
            precursor_data = Spectrum.get_mz_intensity_by_scans_and_precursor_data(mzml_path + mzml_file, scans_per_file)

        sorted_psms = sorted(psms_per_file, key=lambda x: x[0])
        sorted_ms2_data = sorted(ms2_data, key=lambda x: x.scan)

        psms_iter = iter(sorted_psms)

        end_scan, peptide, modified_peptide, \
            score, proteins, is_decoy, spectrum = next(psms_iter)

        for ms2 in sorted_ms2_data:

            while ms2.scan == end_scan:
                # to validate precursor intensity (assumed true if non-label free data)
                precursor_flag = True
                
                entry = [peptide, modified_peptide]
                
                if label_type != 'free':
                    # apply tag intensity correction factors
                    # need to figure out when this code will apply correction factors based on user input of correction percentages that are unique to each lot of reagent labels
                    tag_intensities = ms2.get_isobaric_quantitation(tag_mass = itraq_tag_mass,
                                                        mz_diff_tolerance = mz_diff_tolerance)
                    
                    corrected_tag_intensities = list(np.dot(correction_matrix, tag_intensities))
                
                    for intensity_index, corrected_intensity in enumerate(corrected_tag_intensities):
                        if corrected_intensity < 0:
                            # resets negative values to 0 since negative intensities do not make sense in the context of mass spectrometry proteomics
                            corrected_tag_intensities[intensity_index] = 0
                
                    entry.extend(corrected_tag_intensities)
                else:
                    # get MS1 precursor intensity
                    precursor_intensity = str(precursor_data[1][ms2.scan]['peak intensity'])
                    
                    # checks if the precursor intensity was missing - stored as 'BAD' instead of float
                    if precursor_intensity != 'BAD':
                        precursor_intensity = float(precursor_intensity)
                        entry.append(precursor_intensity)
                    else:
                        precursor_flag = False
                        
                entry.extend([';'.join(proteins), ms2.scan, score, spectrum])
                
                if precursor_flag:
                    data.append(entry)
                try:
                    end_scan, peptide, modified_peptide, \
                        score, proteins, is_decoy, spectrum = next(psms_iter)
                except StopIteration:
                    #print('Reach the end of the list.')
                    break
                    
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = floor(current_percent)
        
            print 'Reading mzML files progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()      

    if percent_checked < 100.0:
        print 'Reading mzML files progress... 100 %'
        sys.stdout.flush()
    
    print '-'
    sys.stdout.flush()
    
    return (data, threshold_dict, score_eq) 
  
def write_itraq_to_file(itraq_data, output_file):
    f = open(output_file, 'w')
    for row in itraq_data:
        f.write('\t'.join([str(s) for s in row]) + '\n')
    f.close()

def get_peptide_psm_count_map(itraq_data_files):
    peptide_psm_count_map = {}
    for data_file in itraq_data_files:
        f = open(data_file, 'r')
        for l in f:
            elements = l.strip().split('\t')
            peptide = elements[0]
            if peptide in peptide_psm_count_map:
                peptide_psm_count_map[peptide] += 1
            else:
                peptide_psm_count_map[peptide] = 1
        f.close()
                    
    return peptide_psm_count_map

def protein_inference(peptide_psm_count_map, output_path, 
                      fasta_file,
                      min_psm_per_peptide = 2,
                      min_peptide_per_protein = 2,
                      cleavage_rule = 'Trypsin',
                      max_miscleavage = 1,
                      min_length = 6, max_length = 50,
                      min_cleavage_terminus = 2,
                      optional_n_term_met_cleavage = True,
                      mod = 'global',
                      output_filename = 'filename_error'):

    log_message = {}

    log_message['num_peptide_before_filtering'] = len(peptide_psm_count_map)
    log_message['num_psm_before_filtering'] = sum(peptide_psm_count_map.values())
    
    peptide_psm_count_map = deepcopy(peptide_psm_count_map)
    
    peptide_psm_count_map = filter_by_psms_per_peptide(peptide_psm_count_map, 
                                                       min_psm_per_peptide = min_psm_per_peptide)

    log_message['num_all_peptide_after_filtering'] = len(peptide_psm_count_map)
    log_message['num_all_psm_after_filtering'] = sum(peptide_psm_count_map.values())

    peptide_set = peptide_psm_count_map.keys()
    #peptide_set_original = peptide_psm_count_map.keys()
    #
    #if mod == 'glyco':
    #    # filters out NXS/T motif peptides because of glyco modification
    #    peptide_set = glycopeptide.select_glycopeptide(peptide_set_original)
    #elif mod == 'phospho':
    #    # filtered in psm_quantitation when filtering psms
    #    peptide_set = peptide_set_original
    #elif mod == 'global':
    #    peptide_set = peptide_set_original
    #else:
    #    raise Exception("Modification must be either 'global', 'glyco', or 'phospho'.")

    peptide_assignment = protein_grouping(
        peptide_set,
        fasta_file, 
        min_peptide_per_protein = min_peptide_per_protein,
        cleavage_rule = cleavage_rule,
        max_miscleavage = max_miscleavage,
        min_length = min_length, 
        max_length = max_length,
        min_cleavage_terminus = min_cleavage_terminus,
        optional_n_term_met_cleavage = optional_n_term_met_cleavage,
        log_message = log_message)

    assigned_peptides = set()
    protein_groups = set()
    for s, p, t in peptide_assignment:
        assigned_peptides.add(s)
        protein_groups.add(p)

    num_assigned_psm = 0
    for p in peptide_psm_count_map:
        if p in assigned_peptides:
            num_assigned_psm += peptide_psm_count_map[p]


    log_message['final_peptide_num'] = len(assigned_peptides)
    log_message['final_protein_group_num'] = len(protein_groups)
    log_message['final_psm_num'] = num_assigned_psm

    f = open(output_path + output_filename + '.txt', 'w')
    for s, p, t in peptide_assignment:
        f.write('\t'.join([s, p, t]))
        f.write('\n')
    f.close()

    f = open(output_path + output_filename + '_log.log', 'w')
    f.write(json.dumps(log_message, sort_keys=True, indent=4) + '\n')
    f.close()
    
def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]

# ******************************************************************************
               
if __name__ == '__main__':
    # correction_factors data
    f = open(config_path + 'correction_factors.tsv', 'r')
    correction_factors_data = f.readlines()
    f.close()
    
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['correction_factors'] = correction_factors_data
    configs['backup'] = True
    
    main(configs)