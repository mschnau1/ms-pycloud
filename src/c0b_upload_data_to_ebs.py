import os
import sys
import glob
import subprocess
from math import floor
from datetime import datetime
from hotpot.configuring import read_new_ebs_config
from hotpot.read_write import read_lines as rl
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_key_location as sc_key_location

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
tmp_path = pipeline_path + 'tmp\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
scripts_path = pipeline_path + 'shell_scripts\\'

def main(configs):
    print '\nStep c0b: Uploading data to the EBS...'
    print '-' * 150
    sys.stdout.flush()
    
    options = configs['options']
    exes = configs['exes']
    ebs_params = configs['ebs_params']
    sc_key, sc_keys = sc_key_info(starcluster_config_path)
    sc_key_path = sc_key_location(starcluster_config_path, sc_key)
    
    raw_path = options['raw_data']
    raw_end  =  raw_path.split('\\')[-2]
    mzml_path = raw_path + 'step1-mzml\\'
    mzid_path = raw_path + 'step3-search_results\\'
    fasta_file = str(options['fasta_path'])
    fasta_path = os.path.dirname(fasta_file) + '\\'
    fasta_path_end = fasta_path.split('\\')[-2]
    cluster_name = str(options['cluster_name']).strip()
    pem_key = sc_key_path
    ppk_key = pem_key.replace('.pem', '.ppk')
    user_id = options['cluster_user_id'] 
    python2_path = os.path.dirname(exes['python']) + '\\'
    starcluster_file = python2_path + 'Scripts\\starcluster.exe'
    putty_file = exes['putty']
    putty_path = os.path.dirname(putty_file) + '\\'
    volume_choice = ebs_params['volume_choice']
    volume_id = str(get_volume_id(volume_choice))
    mount_path = get_volume_mount_path(volume_choice)

    if not os.path.exists(ppk_key):
        ppk_command = putty_path + 'puttygen ' + pem_key
        
        print ppk_command
        sys.stdout.flush()
        subprocess.call(ppk_command)  
    
    cloud_fasta_path = mount_path + 'data/fasta/' + fasta_path_end + '/'
    cloud_mzml_path = mount_path + 'data/' + raw_end + '/mzml/'
    
    # starts the starcluster instance for uploading data - will be terminated once data is uploaded
    start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name + ' --cluster-size=1 --master-instance-type=t2.micro'
    print start_command
    sys.stdout.flush()
    subprocess.call(start_command)
    
    print ''
    sys.stdout.flush()
    
    # retrieves the public dns of the master node
    ip_command = starcluster_file + ' -c ' + starcluster_config_path + ' sshmaster ' + cluster_name + ' GET http://169.254.169.254/latest/meta-data/public-hostname'
    print ip_command
    sys.stdout.flush()
    vals = subprocess.Popen(ip_command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    ip, errors = vals.communicate()
    vals.wait()
    vals.terminate()
    
    master_ip = user_id + '@' + str(ip).strip()
    
    write_data_dirs(scripts_path = scripts_path, mount_path = mount_path, fasta_end = fasta_path_end, raw_end = raw_end)
    
    # commands to put items on master node 
    plink_dirs_command = putty_path + 'plink ' + master_ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'create_data_dirs.sh'
    print plink_dirs_command
    sys.stdout.flush()
    proc = subprocess.Popen(plink_dirs_command, stdin = subprocess.PIPE, shell = False)
    print proc.communicate("y\n")
    proc.wait()
    proc.terminate()
    
    print ''
    sys.stdout.flush()
    
    # copy fasta files to the ebs
    if os.path.exists(fasta_file):
        put_fasta_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + fasta_file + ' ' + cloud_fasta_path
        print put_fasta_command
        sys.stdout.flush()
        subprocess.call(put_fasta_command, shell = True)
    
        print ''
        sys.stdout.flush()
    
    mzid_files_all = glob.glob(mzid_path + '*.*')
    mzid_files = set()
    for mzid_f in mzid_files_all:
        file_ext = str('.'.join(mzid_f.split('.')[1:]))
        mzid_files.add(os.path.basename(mzid_f).replace('.' + file_ext, ''))
    
    mzml_files_all = glob.glob(mzml_path + '*.mzML')
    mzml_files = []
    for mzml_f in mzml_files_all:
        mzml_basename = os.path.basename(mzml_f).replace('.mzML', '')
        
        if mzml_basename not in mzid_files:
            mzml_files.append(mzml_f)
    
    mzml_files = sorted(mzml_files)
    
    uploaded_mzmls_file = mzml_path + 'uploaded_mzmls-' + volume_id + '.log'
    if os.path.isfile(uploaded_mzmls_file):
        f = open(uploaded_mzmls_file, 'r')
        uploaded_lines = f.readlines()
        f.close()
    else:
        uploaded_lines = []
        
    uploaded_files = []
    for line in uploaded_lines:
        line = line.strip()
        
        uploaded_files.append(line)
        
        if line != '':
            full_mzml_path = mzml_path + line
            if full_mzml_path in mzml_files:
                mzml_files.remove(full_mzml_path) 
    
    if len(mzml_files) > 0:
        # creates 3 lines with the current date & time to be written in to the uploaded_mzmls file so that it can be easily deciphered when different mzml groups were uploaded,
        # in case starcluster has an error while creating the instance to upload, or has an error while uploading a file
        uploaded_files.append('-')
        current_time = 'date : ' + str(datetime.now().strftime('%Y%m%d_%H%M%S')) + ' (yyyymmdd_hhmmss)' 
        uploaded_files.append(current_time)
        uploaded_files.append('-')
        
        print 'Uploading ' + str(len(mzml_files)) + ' mzML file(s) to the cloud...'
        print '-' * 150
        sys.stdout.flush()
        
        files_len = len(mzml_files)
        
        percent_interval = floor(100.0 / files_len)
        if percent_interval < 5.0:
            percent_interval = 5.0
        
        percent_checked = 0.0
        num_files_checked = 0
        
        print 'Cloud mzML upload progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
        sys.stdout.flush()
        for mzml_f in mzml_files:
            mzml_basename = os.path.basename(mzml_f)
            
            if mzml_basename not in uploaded_files:
                # copy mzml files to the ebs
                put_mzml_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + mzml_f + ' ' + cloud_mzml_path
                #print put_mzml_command
                #sys.stdout.flush()
                subprocess.call(put_mzml_command, shell = True)
                
                print ''
                sys.stdout.flush()
                
                uploaded_files.append(mzml_basename)
            
            num_files_checked += 1
            current_percent = ((100.0 * num_files_checked) / files_len)
            if current_percent >= (percent_checked + percent_interval):
                percent_checked = floor(current_percent)
            
                print 'Cloud mzML upload progress... ' + str(percent_checked) + ' %'
                sys.stdout.flush()
                
        if percent_checked < 100.0:
            print 'Cloud mzML upload progress... 100 %'
            sys.stdout.flush()
            
        print ''
        sys.stdout.flush()
        
    f = open(uploaded_mzmls_file, 'w')
    for uploaded_f in uploaded_files[:-1]:
        f.write(str(uploaded_f).strip() + '\n')
        
    f.write(str(uploaded_files[-1]).strip())
    f.close()
    
    terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
    print terminate_command
    sys.stdout.flush()
    proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
    print proc.communicate("y\n")
    proc.wait()
    proc.terminate()
    
    print '-' * 150
    print 'Data uploaded to EBS!'
    sys.stdout.flush()

def get_volume_id(volume_choice):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    volume_section_flag = False
    
    for line in lines:
        line = line.strip()
        
        # specified volume must NOT be commented out
        if line == '[volume ' + volume_choice + ']':
            volume_section_flag = True
            
        if volume_section_flag == True:
            if line[:9] == 'VOLUME_ID':
                line = line.split('=')[-1].strip()
                volume_id = line
                
                return volume_id
    
def get_volume_mount_path(volume_choice):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    volume_section_flag = False
    
    for line in lines:
        line = line.strip()
        
        # specified volume must NOT be commented out
        if line == '[volume ' + volume_choice + ']':
            volume_section_flag = True
            
        if volume_section_flag == True:
            if line[:10] == 'MOUNT_PATH':
                line = line.split('=')[-1].strip()
                mount_path = line
                
                if mount_path[0] != '/':
                    mount_path = '/' + mount_path
                                
                if mount_path[-1] != '/':
                    mount_path += '/'
                
                return mount_path

def get_mount_paths():
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    mount_paths = []
    
    for line in lines:
        if line[:10] == 'MOUNT_PATH':
            line = line.split('=')
            path = str(line[1].strip())
            
            if path[0] != '/':
                path = '/' + path
            
            if path[-1] != '/':
                path += '/'
            
            mount_paths.append(path)
        
    return mount_paths
    
def write_data_dirs(scripts_path, mount_path, fasta_end, raw_end):
    root_sh = open(scripts_path + 'create_data_dirs.sh', 'w')
    
    root_sh.write('#/bin/bash\n')
    root_sh.write('cd ' + mount_path + '\n')
    root_sh.write('sudo mkdir -p data\n')
    root_sh.write('cd data\n')
    root_sh.write('sudo mkdir -p fasta\n')
    root_sh.write('cd fasta\n')
    root_sh.write('sudo mkdir -p ' + fasta_end + '\n')
    root_sh.write('cd ' + mount_path + '\n')
    root_sh.write('cd data\n')
    root_sh.write('sudo mkdir -p ' + raw_end + '\n')
    root_sh.write('cd ' + raw_end  + '\n')
    root_sh.write('sudo mkdir -p mzml\n')
    root_sh.write('sudo mkdir -p search_results\n')
    # GPQuest processing folders - done in step c0c_start_cluster
    #root_sh.write('sudo mkdir -p database\n')
    #root_sh.write('sudo mkdir -p param\n')
    
    root_sh.close()
    
def read_msgf_config(filepath):
    """
    Description: 
        Reads all the commands to be used with MSGFPlus.
        
    Args:
        - filepath: the full filepath of the msgfplus configuration txt file.
        
    Returns:
        - lines_list: The first element is the heap size for Java.  Second element is the
                      destination for the copy command (if applicable, otherwise blank).
                      Third element is the string concatenation of the rest of the configuration
                      parameters.
    """
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    lines_list = [' -Xmx1024M','','']
    
    for i in range(len(lines)):
        # strips all leading and trailing whitespace
        lines[i] = lines[i].strip()
        lines[i] = str(lines[i])
        
        if lines[i][:4] == '-Xmx':
            # sets the first element of lines_list to the user specified heap size
            lines_list[0] = ' ' + lines[i]
        elif lines[i][-8:] == '.BuildSA':
            # sets the second element to the user specified copy command destination
            lines_list[1] = ' ' + lines[i]
        else:
            # sets the third element to the string concatenation of the rest of the commands
            lines_list[2] = lines_list[2] + ' ' + lines[i]
    
    return lines_list

if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['msgf mods'] = rl(config_path + 'msgf_mods.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['ebs_params'] = read_new_ebs_config(config_path + 'ebs_params.txt')
    
    main(configs)