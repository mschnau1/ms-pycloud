"""
Description:
    Uses MSGFPlus to search the fasta database and convert the mzml files to mzid
    files.
    
Notes: 
    - MSGF search configuration files are used.
"""

import os
import sys
import glob
import math
import time
import shutil
import traceback
import subprocess
import c0b_upload_data_to_ebs as c0b
import c0c_start_cluster as c0c
import c0e_terminate_cluster as c0e
import p3x_convert_mzid_to_tsv as p3x
import p3a_mzml_mzid_charge_injection_time as p3a
import tkMessageBox
from Tkinter import *
from datetime import datetime
from hotpot.pathing import make_path
from hotpot.configuring import read_new_ebs_config
from hotpot.read_write import read_lines as rl
from hotpot.read_write import read_uncommented_lines as rul
from hotpot.read_write import write_tmp_config as wtc
from hotpot.read_write import write_msgf_mods as wmm
from hotpot.read_write import write_msgf_search as wms
from hotpot.read_write import write_msgf_build as wmb
from hotpot.read_write import append_step_sh_script as aps
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import get_starcluster_key_info as sc_key_info
from hotpot.configuring import get_starcluster_key_location as sc_key_location

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
msgf_path = pipeline_path + 'msgfplus\\'
tmp_path = pipeline_path + 'tmp\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'
scripts_path = pipeline_path + 'shell_scripts\\'

def main(configs): 
    print '\nStep 3:\tUsing MSGFPlus to search the fasta file and create mzid files...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    
    # force the search parameter -tda to 1 for decoy search
    edit_msgf_config_decoy_search()
    msgf_mods = rl(config_path + 'msgf_mods.txt')
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    fasta_file = options['fasta_path']
    cloud_log_path = output_folder_path + 'logs\\'
    
    # creates the cloud_log path folder 
    make_path(cloud_log_path)
    
    try:
        exes = configs['exes']
        ebs_params = configs['ebs_params']
        sc_key, sc_keys = sc_key_info(starcluster_config_path)
        sc_key_path = sc_key_location(starcluster_config_path, sc_key)
        
        # sets variables to configuration values
        raw_end  =  data_root.split('\\')[-2]
        fasta_filename = os.path.basename(fasta_file)
        fasta_path = os.path.dirname(fasta_file) + '\\'
        fasta_path_end = fasta_path.split('\\')[-2]
        wmm(tmp_path + 'msgf_mods.txt', msgf_mods) 
        wms(tmp_path + 'msgf_search.txt', msgf_mods)
        wmb(tmp_path + 'msgf_build.txt', msgf_mods) 
        msgf_search_file = tmp_path + 'msgf_search.txt'
        msgf_build_file = tmp_path + 'msgf_build.txt'
        msgf_mod_file = tmp_path + 'msgf_mods.txt'
        cluster_name = str(options['cluster_name']).strip()
        pem_key = sc_key_path
        ppk_key = pem_key.replace('.pem', '.ppk')
        python2_path = os.path.dirname(exes['python']) + '\\'
        starcluster_file = python2_path + 'Scripts\\starcluster.exe'
        putty_file = exes['putty']
        putty_path = os.path.dirname(putty_file) + '\\'
        volume_choice = ebs_params['volume_choice']
        mount_path = get_volume_mount_path(volume_choice)
                
        # grabs all mzml filenames
        mzml_files_all = glob.glob(mzml_path + '*.mzML')
                
        # only specifies mzML files for database searching on the cloud that have not already been searched locally
        mzml_files = []
        for mzml_file in mzml_files_all:
            mzid_name = os.path.basename(mzml_file).replace('.mzML', '.mzid')
            
            if not os.path.exists(mzid_path + mzid_name):
                mzml_files.append(mzml_file)
        
        if len(mzml_files) != 0:
            # maximum Amazon AWS instance size is currently 100 nodes
            if len(mzml_files) <= 100:       
                num_nodes = len(mzml_files)
            else:
                num_nodes = 100
                
            mzml_files_by_node = map(None, *[iter(mzml_files)] * int(math.ceil(float(len(mzml_files)) / num_nodes)))
            node_num = len(mzml_files_by_node)
            
            update_sc_cluster_size_config(node_num)
            
            search_files = glob.glob(mzid_path + '*.*')
            
            result_type = ''
            archive_results = False
            for search_f in search_files:
                file_ext = str('.'.join(search_f.split('.')[1:])).lower()
                
                if file_ext == 'pepxml' or file_ext == 'pep.xml':
                    archive_results = True
                    
                    if file_ext == 'pepxml':
                        result_type = 'myrimatch'
                    elif file_ext == 'pep.xml':
                        result_type = 'comet'
                    
                    break
                    
            if archive_results == True:
                current_time = str(datetime.now().strftime('%Y%m%d_%H%M%S'))
                archived_path = output_folder_path + 'archived\\' + result_type + '\\' + current_time + '\\'
                make_path(archived_path)
                
                print 'Notice! Because you have chosen a different search engine from previously searched data, the previous results will be archived in ' + archived_path
                
                shutil.copytree(mzid_path, archived_path + 'step3-search_results\\')
                shutil.rmtree(mzid_path, ignore_errors=True)
                
                if os.path.exists(output_folder_path + 'configs\\'):
                    shutil.copytree(output_folder_path + 'configs\\', archived_path + 'configs\\')
                    shutil.rmtree(output_folder_path + 'configs\\', ignore_errors=True)
                    shutil.copytree(config_path, output_folder_path + 'configs\\')
                
                if os.path.exists(output_folder_path + 'quality_control\\'):
                    shutil.copytree(output_folder_path + 'quality_control\\', archived_path + 'quality_control\\')
                    shutil.rmtree(output_folder_path + 'quality_control\\', ignore_errors=True)
                    
                if os.path.exists(output_folder_path + 'logs\\'):
                    shutil.copytree(output_folder_path + 'logs\\', archived_path + 'logs\\')
                    shutil.rmtree(output_folder_path + 'logs\\', ignore_errors=True)
            
                if os.path.exists(output_folder_path + 'step4a-psm_quantitation\\'):
                    shutil.copytree(output_folder_path + 'step4a-psm_quantitation\\', archived_path + 'step4a-psm_quantitation\\')
                    shutil.rmtree(output_folder_path + 'step4a-psm_quantitation\\', ignore_errors=True)
                    
                if os.path.exists(output_folder_path + 'step4b-protein_inference\\'):
                    shutil.copytree(output_folder_path + 'step4b-protein_inference\\', archived_path + 'step4b-protein_inference\\')
                    shutil.rmtree(output_folder_path + 'step4b-protein_inference\\', ignore_errors=True)
                
                if os.path.exists(output_folder_path + 'step6-peptide_protein_quantitation\\'):
                    shutil.copytree(output_folder_path + 'step6-peptide_protein_quantitation\\', archived_path + 'step6-peptide_protein_quantitation\\')
                    shutil.rmtree(output_folder_path + 'step6-peptide_protein_quantitation\\', ignore_errors=True)
                
                if os.path.exists(output_folder_path + 'step5-decoy\\'):
                    shutil.copytree(output_folder_path + 'step5-decoy\\', archived_path + 'step5-decoy\\')
                    shutil.rmtree(output_folder_path + 'step5-decoy\\', ignore_errors=True)
                    
                if os.path.exists(output_folder_path + 'step7-expression_matrix\\'):
                    shutil.copytree(output_folder_path + 'step7-expression_matrix\\', archived_path + 'step7-expression_matrix\\')
                    shutil.rmtree(output_folder_path + 'step7-expression_matrix\\', ignore_errors=True)
            
            # delete any leftover scripts that were not deleted last time due to an exception or premature exit
            sh_files = glob.glob(scripts_path + 'c3_*.*')
            for sh_file in sh_files:
                os.remove(sh_file)
            
            # upload data to the ebs and start the computational cluster
            c0b.main(configs)
            c0c.main(configs)
            
            configs['node ips'] = rul(config_path + 'node_ips.txt')
            node_ips = configs['node ips']
            
            cloud_pipeline_path = mount_path + 'mspycloud/'
            cloud_data_path = mount_path + 'data/'
            cloud_msgf_jar_file = cloud_pipeline_path + 'msgfplus/MSGFPlus.jar'
            cloud_msgf_mod_file = cloud_pipeline_path + 'tmp/msgf_mods.txt'
            cloud_fasta_path = cloud_data_path + 'fasta/' + fasta_path_end + '/'
            cloud_fasta_file = cloud_fasta_path + fasta_filename
            cloud_mzml_path = cloud_data_path + raw_end + '/mzml/'
            cloud_mzid_path = cloud_data_path + raw_end + '/search_results/'
            cloud_java_file = 'sudo java'
            
            if not os.path.exists(ppk_key):
                ppk_command = putty_path + 'puttygen ' + pem_key
                
                print ppk_command
                sys.stdout.flush()
                subprocess.call(ppk_command)
            
            # creates the output path folders
            make_path(mzid_path) 
            
            # copy tmp msgf search file to cluster
            put_msgf_search_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + msgf_search_file + ' ' + cloud_pipeline_path + 'tmp/'
            print put_msgf_search_command
            sys.stdout.flush()
            subprocess.call(put_msgf_search_command, shell = True)
            
            # copy tmp msgf search file to cluster
            put_msgf_build_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + msgf_build_file + ' ' + cloud_pipeline_path + 'tmp/'
            print put_msgf_build_command
            sys.stdout.flush()
            subprocess.call(put_msgf_build_command, shell = True)
            
            # copy tmp msgf modification file to cluster
            put_msgf_mod_command = starcluster_file + ' -c ' + starcluster_config_path + ' put ' + cluster_name + ' ' + msgf_mod_file + ' ' + cloud_pipeline_path + 'tmp/'
            print put_msgf_mod_command
            sys.stdout.flush()
            subprocess.call(put_msgf_mod_command, shell = True) 
            
            i = 0
            for mzmls in mzml_files_by_node:
                if isinstance(mzmls, tuple):
                    current_mzmls = []
                    
                    for mzml in mzmls:
                        if mzml != None:
                            current_mzmls.append(mzml)
                else:
                    current_mzmls = [mzmls]
                
                mzml_files_by_node[i] = current_mzmls
                i = i + 1
        
            build_config = read_msgf_config(msgf_build_file)
            search_config = read_msgf_config(msgf_search_file)
            
            # file header for c3_master.sh
            f = open(scripts_path + 'c3_master_search.sh', 'w')
            f.write('#/bin/bash\n\n')
            f.close()
            
            f = open(scripts_path + 'c3_master_build.sh', 'w')
            f.write('#/bin/bash\n\n')
            f.close()
            
            msgf_build_command = cloud_java_file + build_config[0] + ' -cp ' + cloud_msgf_jar_file + build_config[1] + ' -d ' + cloud_fasta_file + build_config[2]
            aps(filename = scripts_path + 'c3_master_build.sh', command = msgf_build_command)
            
            # master node
            for mzml in mzml_files_by_node[0]:
                if mzml != None:
                    mzml_filename = os.path.basename(mzml)
                    
                    # the filename of the potential mzml file to be created
                    mzid_filename = mzml_filename[:-5] + '.mzid'
                    
                    msgf_search_command = cloud_java_file + search_config[0] + ' -jar ' + cloud_msgf_jar_file + ' -s ' + cloud_mzml_path + mzml_filename + ' -d ' + cloud_fasta_file + ' -o ' + cloud_mzid_path + mzid_filename + ' -mod ' + cloud_msgf_mod_file + search_config[2]
                    aps(filename = scripts_path + 'c3_master_search.sh', command = msgf_search_command)
                    
            # batch file with plink search, and removes the node (terminates nodes except the master node)        
            master_plink_command = '"' + putty_path + 'plink" ' + node_ips[0] + ' -i ' + ppk_key + ' -m ' + scripts_path + 'c3_master_search.sh'
            aps(filename = scripts_path + 'c3_master_driver.bat', command = master_plink_command)
            
            # rest of nodes        
            for i, ip in zip(range(1, len(mzml_files_by_node)), node_ips[1:]):
                node_name = '00' + str(i)
                node_name = 'node' + node_name[-3:]
                
                # file header for c3_[nodes].sh files
                f = open(scripts_path + 'c3_' + node_name + '_search.sh', 'w')
                f.write('#/bin/bash\n\n')
                f.close()
                
                for mzml in mzml_files_by_node[i]:
                    if mzml != None:
                        mzml_filename = os.path.basename(mzml)
                        
                        # the filename of the potential mzml file to be created
                        mzid_filename = mzml_filename[:-5] + '.mzid'
                        
                        msgf_search_command = cloud_java_file + search_config[0] + ' -jar ' + cloud_msgf_jar_file + ' -s ' + cloud_mzml_path + mzml_filename + ' -d ' + cloud_fasta_file + ' -o ' + cloud_mzid_path + mzid_filename + ' -mod ' + cloud_msgf_mod_file + search_config[2]
                        aps(filename = scripts_path + 'c3_' + node_name + '_search.sh', command = msgf_search_command)
                        
                # batch file with plink search, and removes the node (terminates the node)         
                node_plink_command = '"' + putty_path + 'plink" ' + ip + ' -i ' + ppk_key + ' -m ' + scripts_path + 'c3_' + node_name + '_search.sh'
                aps(filename = scripts_path + 'c3_' + node_name + '_driver.bat', command = node_plink_command)
                
                #node_remove_command = starcluster_file + ' -c ' + starcluster_config_path + ' removenode --confirm ' + cluster_name + ' ' + node_name
                #aps(filename = scripts_path + 'c3_' + node_name + '_driver.bat', command = node_remove_command)
            
            print '\n'
            print '-' * 150
            print 'Building MS-GF+ reversely categorized fasta database...'
            print '-' * 150
            sys.stdout.flush()
            
            plink_c3_command = putty_path + 'plink ' + node_ips[0] + ' -i ' + ppk_key + ' -m ' + scripts_path + 'c3_master_build.sh'
            print plink_c3_command
            sys.stdout.flush()
            proc = subprocess.Popen(plink_c3_command, shell = False, stdin = None, stdout = None, stderr = None, close_fds = True)
            print proc.communicate("y\n")
            proc.wait()
            proc.terminate()
            
            script_names = glob.glob(scripts_path + 'c3_*_driver.bat')
            
            print ''
            sys.stdout.flush()
            
            procs = []
            for script_name in script_names:
                script_command = script_name
                print script_command
                sys.stdout.flush()
                proc = subprocess.Popen(script_command, shell = False, stdin = None, stdout = None, stderr = None, close_fds = True)
                procs.append(proc)
                
            for p in procs:
                print p.communicate("y\n")
                p.wait()
                p.terminate()
            
            # terminate cluster
            c0e.main(configs)
            
            print ''
            sys.stdout.flush()
            
            # starts the starcluster instance for retrieving data - will be terminated once data is downloaded
            start_command = starcluster_file + ' -c ' + starcluster_config_path + ' start ' + cluster_name + ' --cluster-size=1 --master-instance-type=t2.micro'
            print start_command
            sys.stdout.flush()
            subprocess.call(start_command)
            
            print '\nRetrieving ' + str(len(mzml_files)) + ' mzid files from the cloud...'
            print '-' * 150
            sys.stdout.flush()
            
            files_len = len(mzml_files)
            
            percent_interval = math.floor(100.0 / files_len)
            if percent_interval < 5.0:
                percent_interval = 5.0
            
            percent_checked = 0.0
            num_files_checked = 0
            
            print 'Cloud mzid download progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
            sys.stdout.flush()
            for mzml_file in mzml_files:
                mzid_filename = os.path.basename(mzml_file).replace('.mzML', '.mzid')
                
                # get mzid results from the cluster
                get_mzid_command = starcluster_file + ' -c ' + starcluster_config_path + ' get ' + cluster_name + ' ' + cloud_mzid_path + mzid_filename + ' ' + mzid_path
                #print get_mzid_command
                #sys.stdout.flush()
                subprocess.call(get_mzid_command, shell = True, stdin = None, stdout = None, stderr = None, close_fds = True)
                
                print ''
                sys.stdout.flush()
                
                num_files_checked += 1
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = math.floor(current_percent)
                
                    print 'Cloud mzid download progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                    
            if percent_checked < 100.0:
                print 'Cloud mzid download progress... 100 %'
                sys.stdout.flush()
            
            print ''
            sys.stdout.flush()
            
            # terminates cluster that was used to download the mzid files    
            terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
            print terminate_command
            sys.stdout.flush()
            proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
            print proc.communicate("y\n")
            proc.wait()
            proc.terminate()
            
            # delete c3 scripts
            sh_files = glob.glob(scripts_path + 'c3_*.*')
            for sh_file in sh_files:
                os.remove(sh_file)
            
            write_config_params = []
            write_config_params.append('mzml_path = ' + mzml_path)
            write_config_params.append('fasta_path = ' + fasta_file)
            write_config_params.append('search_engine = ' + 'MSGF')
            write_config_outfile = mzid_path + 'step3.config'
            
            write_mspy(write_config_params, write_config_outfile)
            write_tp(config_path + 'msgf_mods.txt', write_config_outfile, 'msgf_mods.txt settings')
            
            os.remove(tmp_path + 'msgf_mods.txt')
            os.remove(tmp_path + 'msgf_search.txt')
            os.remove(tmp_path + 'msgf_build.txt')
            
        f = open(cloud_log_path + 'cloud_log_' + str(time.strftime("%Y%m%d-%H%M%S")) + '.log', 'w')
        f.write('No errors reported.\n\nIf expected result files are missing, there was likely an error with the initialization of StarCluster - please run the pipeline again if this is the case.')
        f.close()
        
        # convert mzid results to tsv
        p3x.main(configs) 
        
        sys.stdout.flush()
        p3a.main(configs)
    except:
        # terminate cluster
        c0e.main(configs)
        
        # delete the scripts in case of exception before deletion
        sh_files = glob.glob(scripts_path + 'c3_*.*')
        for sh_file in sh_files:
            os.remove(sh_file)
            
        write_config_params = []
        write_config_params.append('mzml_path = ' + mzml_path)
        write_config_params.append('fasta_path = ' + fasta_file)
        write_config_params.append('search_engine = ' + 'MSGF')
        write_config_outfile = mzid_path + 'step3.config'
        
        write_mspy(write_config_params, write_config_outfile)
        write_tp(config_path + 'msgf_mods.txt', write_config_outfile, 'msgf_mods.txt settings')
        
        # delete temporary config files if they exist
        try:
            os.remove(tmp_path + 'msgf_mods.txt')
        except:
            pass
            
        try:
            os.remove(tmp_path + 'msgf_search.txt')
        except:
            pass
            
        try:
            os.remove(tmp_path + 'msgf_build.txt')
        except:
            pass
        
        print '-'
        print 'Error with c3_msgfplus.main()...'
        e = traceback.format_exc()
        print '\nAn error occurred while processing data on the cloud and the cluster has been terminated. Please run the pipeline again.'
        print e
        print '-'
        sys.stdout.flush()
        
        f = open(cloud_log_path + 'cloud_error_log_' + str(time.strftime("%Y%m%d-%H%M%S")) + '.log', 'w')
        f.write('Unexpected error: \n' + str(e))
        f.write('\nAn error occurred while processing data on the cloud and the cluster has been terminated.\n\nPlease run the pipeline again.')
        f.close()
        
        #root = Tk()
        #root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        #root.update_idletasks()
        #tkMessageBox.showinfo('Error!', 'An error occurred while processing data on the cloud and the cluster has been terminated. Please try again.')
        #root.destroy()       

# ******************************************************************************

def edit_msgf_config_decoy_search():
    wtc(tmp_path + 'msgf_mods.txt', config_path + 'msgf_mods.txt')
            
    infile = open(tmp_path + 'msgf_mods.txt', 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    outfile = open(config_path + 'msgf_mods.txt', 'w')
     
    search_section_flag = False   
    for line in infile_lines:
        strip_line = line.strip()
        
        if strip_line == '#-# search parameters':
            search_section_flag = True
            
        if strip_line == '#-# build parameters':
            search_section_flag = False
            
        if line[0:len('-tda')] == '-tda' and search_section_flag:
            outfile.write('-tda 1' + '\n')
        else:
            outfile.write(line)
                    
    outfile.close()
        
    os.remove(tmp_path + 'msgf_mods.txt')

def update_sc_cluster_size_config(cluster_size):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    starcluster_output_data = []
    
    cluster_section_flag = False
    for line in lines:
        line = str(line).strip()
        
        if line == '[cluster mspycloud]':
            cluster_section_flag = True
            
        if line == '## Defining Additional Cluster Templates ##':
            cluster_section_flag = False
        
        if cluster_section_flag == True:
            split_line = line.split('=')    
            
            key = split_line[0].strip().lower()
            if key == '':
                new_line = line
            elif key == 'cluster_size':
                new_line = 'CLUSTER_SIZE = ' + str(cluster_size)
            elif key[0] == '#':
                if key.split('#')[1].strip() == 'cluster_size':
                    new_line = 'CLUSTER_SIZE = ' + str(cluster_size)
                else:
                    new_line = line
            else:
                new_line = line
        else:
            new_line = line
            
        starcluster_output_data.append(new_line + '\n')
                
    f = open(starcluster_config_path, 'w')
    for starcluster_output_line in starcluster_output_data:
        f.write(starcluster_output_line)
    f.close()

def read_msgf_config(filepath):
    """
    Description: 
        Reads all the commands to be used with MSGFPlus.
        
    Args:
        - filepath: the full filepath of the msgfplus configuration txt file.
        
    Returns:
        - lines_list: The first element is the heap size for Java.  Second element is the
                      destination for the copy command (if applicable, otherwise blank).
                      Third element is the string concatenation of the rest of the configuration
                      parameters.
    """
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    lines_list = [' -Xmx1024M','','']
    
    for i in range(len(lines)):
        # strips all leading and trailing whitespace
        lines[i] = lines[i].strip()
        lines[i] = str(lines[i])
        
        if lines[i][:4] == '-Xmx':
            # sets the first element of lines_list to the user specified heap size
            lines_list[0] = ' ' + lines[i]
        elif lines[i][-8:] == '.BuildSA':
            # sets the second element to the user specified copy command destination
            lines_list[1] = ' ' + lines[i]
        else:
            # sets the third element to the string concatenation of the rest of the commands
            lines_list[2] = lines_list[2] + ' ' + lines[i]
    
    return lines_list
    
def get_volume_mount_path(volume_choice):
    f = open(starcluster_config_path, 'r')
    lines = f.readlines()
    f.close()
    
    volume_section_flag = False
    
    for line in lines:
        line = line.strip()
        
        # specified volume must NOT be commented out
        if line == '[volume ' + volume_choice + ']':
            volume_section_flag = True
            
        if volume_section_flag == True:
            if line[:10] == 'MOUNT_PATH':
                line = line.split('=')[-1].strip()
                mount_path = line
                
                return mount_path
    
# ******************************************************************************
               
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['msgf mods'] = rl(config_path + 'msgf_mods.txt')
    configs['ebs_params'] = read_new_ebs_config(config_path + 'ebs_params.txt')
    configs['node ips'] = rul(config_path + 'node_ips.txt')
    configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)