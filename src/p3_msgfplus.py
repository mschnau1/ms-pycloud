"""
Description:
    Uses MSGFPlus to search the fasta database and convert the mzml files to mzid
    files.
    
Notes: 
    - MSGF search and build configuration files are used.
"""

import os
import sys
import glob
import time
import shutil
import subprocess
import tkMessageBox
import p3x_convert_mzid_to_tsv as p3x
import p3a_mzml_mzid_charge_injection_time as p3a
from Tkinter import *
from datetime import datetime
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.read_write import write_tmp_config as wtc
from hotpot.read_write import write_msgf_mods as wmm
from hotpot.read_write import write_msgf_search as wms
from hotpot.read_write import write_msgf_build as wmb
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
msgf_path = pipeline_path + 'msgfplus\\'
tmp_path = pipeline_path + 'tmp\\'

def main(configs): 
    print '\nStep 3:\tUsing MSGFPlus to search the fasta file and create mzid files...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
    
    # force the search parameter -tda to 1 for decoy search
    edit_msgf_config_decoy_search()
    msgf_mods = rl(config_path + 'msgf_mods.txt')
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_path = options['archived_path']
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    fasta_file = options['fasta_path']
    java_file = exes['java']
    msgf_jar_file = msgf_path + 'MSGFPlus.jar'
    wmm(tmp_path + 'msgf_mods.txt', msgf_mods) 
    wms(tmp_path + 'msgf_search.txt', msgf_mods)
    wmb(tmp_path + 'msgf_build.txt', msgf_mods) 
    msgf_search_file = tmp_path + 'msgf_search.txt'
    msgf_mod_file = tmp_path + 'msgf_mods.txt'
    msgf_build_file = tmp_path + 'msgf_build.txt'
    
    if not os.path.exists(fasta_file.replace('.fasta','.revCat.fasta')):
        build_config = read_msgf_config(msgf_build_file)
        
        # indexes the fasta file for faster searching
        subprocess.call(java_file + build_config[0] + ' -cp ' + msgf_jar_file + build_config[1] + \
            ' -d ' + fasta_file + \
            build_config[2]
            )
    
    search_files = glob.glob(mzid_path + '*.*')
    
    result_type = ''
    archive_results = False
    for search_f in search_files:
        file_ext = str('.'.join(search_f.split('.')[1:])).lower()
        
        if file_ext == 'pepxml' or file_ext == 'pep.xml':
            archive_results = True
            
            if file_ext == 'pepxml':
                result_type = 'myrimatch'
            elif file_ext == 'pep.xml':
                result_type = 'comet'
            
            break
            
    if archive_results == True:
        current_time = str(datetime.now().strftime('%Y%m%d_%H%M%S'))
        archived_path = output_path + 'archived\\' + result_type + '\\' + current_time + '\\'
        make_path(archived_path)
        
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Notice!', 'Because you have chosen a different search engine from previously searched data, the previous results will be archived in ' + archived_path)
        root.destroy()
        
        shutil.copytree(mzid_path, archived_path + 'step3-search_results\\')
        shutil.rmtree(mzid_path, ignore_errors=True)
        
        if os.path.exists(output_path + 'configs\\'):
            shutil.copytree(output_path + 'configs\\', archived_path + 'configs\\')
            shutil.rmtree(output_path + 'configs\\', ignore_errors=True)
            shutil.copytree(config_path, output_path + 'configs\\')
        
        if os.path.exists(output_path + 'quality_control\\'):
            shutil.copytree(output_path + 'quality_control\\', archived_path + 'quality_control\\')
            shutil.rmtree(output_path + 'quality_control\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'logs\\'):
            shutil.copytree(output_path + 'logs\\', archived_path + 'logs\\')
            shutil.rmtree(output_path + 'logs\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'step4a-psm_quantitation\\'):
            shutil.copytree(output_path + 'step4a-psm_quantitation\\', archived_path + 'step4a-psm_quantitation\\')
            shutil.rmtree(output_path + 'step4a-psm_quantitation\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'step4b-protein_inference\\'):
            shutil.copytree(output_path + 'step4b-protein_inference\\', archived_path + 'step4b-protein_inference\\')
            shutil.rmtree(output_path + 'step4b-protein_inference\\', ignore_errors=True)
        
        if os.path.exists(output_path + 'step6-peptide_protein_quantitation\\'):
            shutil.copytree(output_path + 'step6-peptide_protein_quantitation\\', archived_path + 'step6-peptide_protein_quantitation\\')
            shutil.rmtree(output_path + 'step6-peptide_protein_quantitation\\', ignore_errors=True)
        
        if os.path.exists(output_path + 'step5-decoy\\'):
            shutil.copytree(output_path + 'step5-decoy\\', archived_path + 'step5-decoy\\')
            shutil.rmtree(output_path + 'step5-decoy\\', ignore_errors=True)
            
        if os.path.exists(output_path + 'step7-expression_matrix\\'):
            shutil.copytree(output_path + 'step7-expression_matrix\\', archived_path + 'step7-expression_matrix\\')
            shutil.rmtree(output_path + 'step7-expression_matrix\\', ignore_errors=True)
            
    # creates the output path folders
    make_path(mzid_path)    
    
    # grabs all mzml filenames
    mzml_files = glob.glob(mzml_path + '*.mzML')

    search_config = read_msgf_config(msgf_search_file)
    
    for mzml_file in mzml_files:
        mzml_filename = os.path.basename(mzml_file)
        
        # the filename of the potential mzml file to be created
        mzid_filename = mzml_filename[:-5] + '.mzid'
        
        # if the particular mzid file does not already exist, then the mzml file is converted to mzid via MSGFPlus
        if not os.path.exists(mzid_path + mzid_filename):
            subprocess.call(java_file + search_config[0] + \
                ' -jar ' + msgf_jar_file + \
                ' -s ' + mzml_path + mzml_filename + \
                ' -d ' + fasta_file + \
                ' -o ' + mzid_path + mzid_filename + \
                ' -mod ' + msgf_mod_file + \
                search_config[2]
                )
                
    # convert mzid results to tsv
    p3x.main(configs)
    
    sys.stdout.flush()
    p3a.main(configs) 
    
    write_config_params = []
    write_config_params.append('mzml_path = ' + mzml_path)
    write_config_params.append('fasta_path = ' + fasta_file)
    write_config_params.append('search_engine = ' + 'MSGF')
    write_config_outfile = mzid_path + 'step3.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'msgf_mods.txt', write_config_outfile, 'msgf_mods.txt settings')
    
    os.remove(tmp_path + 'msgf_mods.txt')
    os.remove(tmp_path + 'msgf_search.txt')
    os.remove(tmp_path + 'msgf_build.txt') 

# ******************************************************************************

def edit_msgf_config_decoy_search():
    wtc(tmp_path + 'msgf_mods.txt', config_path + 'msgf_mods.txt')
            
    infile = open(tmp_path + 'msgf_mods.txt', 'r')
    infile_lines = infile.readlines()
    infile.close()
    
    outfile = open(config_path + 'msgf_mods.txt', 'w')
     
    search_section_flag = False   
    for line in infile_lines:
        strip_line = line.strip()
        
        if strip_line == '#-# search parameters':
            search_section_flag = True
            
        if strip_line == '#-# build parameters':
            search_section_flag = False
            
        if line[0:len('-tda')] == '-tda' and search_section_flag:
            outfile.write('-tda 1' + '\n')
        else:
            outfile.write(line)
                    
    outfile.close()
        
    os.remove(tmp_path + 'msgf_mods.txt')

def read_msgf_config(filepath):
    """
    Description: 
        Reads all the commands to be used with MSGFPlus.
        
    Args:
        - filepath: the full filepath of the msgfplus configuration txt file.
        
    Returns:
        - lines_list: The first element is the heap size for Java.  Second element is the
                      destination for the copy command (if applicable, otherwise blank).
                      Third element is the string concatenation of the rest of the configuration
                      parameters.
    """
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # creates a list to be filled with the configurations
    lines_list = [' -Xmx1024M','','']
    
    for i in range(len(lines)):
        # strips all leading and trailing whitespace
        lines[i] = lines[i].strip()
        lines[i] = str(lines[i])
        
        if lines[i][:4] == '-Xmx':
            # sets the first element of lines_list to the user specified heap size
            lines_list[0] = ' ' + lines[i]
        elif lines[i][-8:] == '.BuildSA':
            # sets the second element to the user specified copy command destination
            lines_list[1] = ' ' + lines[i]
        else:
            # sets the third element to the string concatenation of the rest of the commands
            lines_list[2] = lines_list[2] + ' ' + lines[i]
    
    return lines_list
    
# ******************************************************************************
               
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['msgf mods'] = rl(config_path + 'msgf_mods.txt')
    configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)