"""
Description:
    Writes a tsv file with all of the peptides from the quantitation tsv file and their
    associated protein group matches as found in the peptide_protein tsv file.
    
Notes:
"""

import os
import sys
import copy
import glob
import time
import shutil
import traceback
import subprocess
import hotpot.phosphopeptide as phosphopeptide
import hotpot.n_glycopeptide as glycopeptide
from math import floor
from pyteomics import mass
from hotpot.data_matrix import *
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.read_write import read_luciphor2_config_lines as rluciphor2
from hotpot.read_write import write_luciphor2_config_lines as wluciphor2
from hotpot.configuring import read_steps_config as steps
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.md5_checksums import step6_psm_quant_md5_checksums as psm_md5
from hotpot.md5_checksums import inf_file_md5_checksum as inf_md5

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'
luciphor2_path = pipeline_path + 'luciphor2\\'
tmp_path = pipeline_path + 'tmp\\'

# luciPHOr2 phosphosite localization parameters
c_term_key = 'C-term'

luciphor2_config_keys = []
luciphor2_config_keys.append('SPECTRUM_PATH')
luciphor2_config_keys.append('INPUT_DATA')
luciphor2_config_keys.append('OUTPUT_FILE')

luciphor2_header = []
luciphor2_header.append('srcFile')
luciphor2_header.append('scanNum')
luciphor2_header.append('charge')
luciphor2_header.append('PSMscore')
luciphor2_header.append('peptide')
luciphor2_header.append('modSites')

luciphor_input_header_line = '\t'.join(luciphor2_header)

# number of header rows - defaults to 1 since this is hard coded into pipeline output
header = 1

def main(configs): 
    print '\nStep 6:\tCreate formatted tsv files with the protein matches of identified peptides...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
    sample_filenames = sorted(configs['sample filenames'])
    wluciphor2(tmp_path + 'luciphor2_input.txt', configs['luciphor2']) 
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2] 
    mzid_path = data_root + 'step3-search_results\\'
    quantitation_path = output_folder_path + 'step4a-psm_quantitation\\'
    itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\'
    qc_path = output_folder_path + 'quality_control\\'
    fasta_file = options['fasta_path']
    peptide_protein_file = output_folder_path + 'step4b-protein_inference\\peptide_protein.txt'
    java_file = exes['java']
    luciphor2_jar_file = luciphor2_path + 'luciphor2.jar'
    luciphor2_config_file = tmp_path + 'luciphor2_input.txt' 
    intensity_threshold = int(options['intensity_threshold'])
    label_type = options['label_type']
    mod = str(options['modification']).lower()
    localize_phosphosites = str(options['localize_phosphosites']).lower()
    flr = float(options['false_localization_rate']) / 100.0
    localization_memory = str(options['localization_memory'])
    step3_config = mzid_path + 'step3.config'
    
    if localize_phosphosites == 'true':
        localize_phosphosites = True
    else:
        localize_phosphosites = False
    
    if mod == 'phospho':
        mod_peps = phosphopeptide.get_phospho_mass(step3_config)
    elif mod == 'glyco':
        mod_peps = glycopeptide.get_glyco_mass(step3_config)
        
    if mod == 'intact':
        header_end = ['Oxonium.Peaks', 'Oxonium.Intensity', 'Oxonium.IR', 'Oxonium.Ions', 'N-glycan', 'N-glycans', 'O-glycan', 'O-glycans', 'Proteins', 'Scan', 'IR.Score', 'Morpheus.Score', 'Hyper.Score', 'Spectrum']
        
        itraq4 = ['Sequence', 'Modifications', 'X114', 'X115', 'X116', 'X117'] + header_end
        itraq8 = ['Sequence', 'Modifications', 'X113', 'X114', 'X115', 'X116', 'X117', 'X118', 'X119', 'X121'] + header_end
        tmt10 = ['Sequence', 'Modifications', 'X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131'] + header_end
        tmt11 = ['Sequence', 'Modifications', 'X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'X131C'] + header_end
        tmt16 = ['Sequence', 'Modifications', 'X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'X131C', 'X132N', 'X132C', 'X133N', 'X133C', 'X134N'] + header_end
        free = ['Sequence', 'Modifications', 'XMS1'] + header_end
    else:
        score_str = 'Score'
        
        itraq4 = ['Sequence', 'Modifications', 'X114', 'X115', 'X116', 'X117', 'Proteins', 'Scan', score_str, 'Spectrum']
        itraq8 = ['Sequence', 'Modifications', 'X113', 'X114', 'X115', 'X116', 'X117', 'X118', 'X119', 'X121', 'Proteins', 'Scan', score_str, 'Spectrum']
        tmt10 = ['Sequence', 'Modifications', 'X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'Proteins', 'Scan', score_str, 'Spectrum']
        tmt11 = ['Sequence', 'Modifications', 'X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'X131C', 'Proteins', 'Scan', score_str, 'Spectrum']
        tmt16 = ['Sequence', 'Modifications', 'X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'X131C', 'X132N', 'X132C', 'X133N', 'X133C', 'X134N', 'Proteins', 'Scan', score_str, 'Spectrum']
        free = ['Sequence', 'Modifications', 'XMS1', 'Proteins', 'Scan', score_str, 'Spectrum']
    
    # 0-based index for the first intensity column
    first_intensity_col = 2
    
    if label_type == '4plex':
        column_header_names = itraq4
        num_intensity_cols = 4
    elif label_type == '8plex':
        column_header_names = itraq8
        num_intensity_cols = 8
    elif label_type == '10plex':
        column_header_names = tmt10
        num_intensity_cols = 10
    elif label_type == '11plex':
        column_header_names = tmt11
        num_intensity_cols = 11
    elif label_type == '16plex':
        column_header_names = tmt16
        num_intensity_cols = 16
    elif label_type == 'free':
        column_header_names = free
        num_intensity_cols = 1
    
    data_path = quantitation_path
    output_path = itraq_path
    output_path_exists = os.path.exists(output_path)
        
    # creates the output path folders
    make_path(output_path)
    make_path(qc_path)
    
    if mod == 'intact':
        make_path(output_path + 'n-glycoforms\\')
        make_path(output_path + 'o-glycoforms\\')
    
    # handle single shot data
    data_files_all = glob.glob(data_path + '*.txt')
    if sample_filenames == list():
        for data_f in data_files_all:
            data_f_basename = os.path.basename(data_f).replace('.txt', '')
            
            if data_f_basename not in sample_filenames:
                sample_filenames.append(data_f_basename)
        
        # removes non-unique names in case a set file was left over from previous analysis
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)            
                
    run_samples = set()
    move_md5 = False
    # number of header lines in md5 checksum files
    md5_header = 1
    psm_md5_filename = 'step6_psm_md5_checksums.tsv'
    inf_md5_filename = 'peptide_protein_md5_checksum.tsv'
    
    files_should_exist = [output_path + 'step6.config']
    
    if not os.path.exists(quantitation_path + psm_md5_filename) or output_path_exists == False:
        psm_md5(quantitation_path, quantitation_path, sample_filenames)
        inf_md5(peptide_protein_file, output_path)
        
        for sample_filename in sample_filenames:
            run_samples.add(sample_filename)
    else:
        try:
            for filepath in files_should_exist:
                if not os.path.exists(filepath):
                    for sample_filename in sample_filenames:
                        run_samples.add(sample_filename)
                   
            for sample_filename in sample_filenames:
                if not os.path.exists(output_path + sample_filename + '.txt'):
                    run_samples.add(sample_filename)
            
            if not os.path.exists(output_path + inf_md5_filename):
                inf_md5(peptide_protein_file, output_path)
                
                for sample_filename in sample_filenames:
                    run_samples.add(sample_filename)
            else:
                f = open(output_path + inf_md5_filename, 'r')
                previous_md5_lines = f.readlines()
                f.close()
                
                inf_md5(peptide_protein_file, output_path)
                
                f = open(output_path + inf_md5_filename, 'r')
                current_md5_lines = f.readlines()
                f.close()
                
                previous_md5 = {}
                for line in previous_md5_lines[md5_header:]:
                    line = line.strip().split('\t')
                    filename = line[0].strip()
                    checksum = line[1].strip()
                    
                    previous_md5[filename] = checksum
                    
                current_md5 = {}
                for line in current_md5_lines[md5_header:]:
                    line = line.strip().split('\t')
                    filename = line[0].strip()
                    checksum = line[1].strip()
                    
                    current_md5[filename] = checksum
                
                for key in current_md5:
                    run_all_samples = False
                    
                    if key not in previous_md5:
                        run_all_samples = True        
                    elif previous_md5[key] != current_md5[key]:
                        run_all_samples = True
                        
                    if run_all_samples == True:
                        for sample_filename in sample_filenames:
                            run_samples.add(sample_filename)
                            
                for key in previous_md5:
                    run_all_samples = False
                    
                    if key not in current_md5:
                        run_all_samples = True
                    elif current_md5[key] != previous_md5[key]:
                        run_all_samples = True
                        
                    if run_all_samples == True:
                        for sample_filename in sample_filenames:
                            run_samples.add(sample_filename)
            
            f = open(quantitation_path + psm_md5_filename, 'r')
            previous_md5_lines = f.readlines()
            f.close()
            
            psm_md5(quantitation_path, output_path, sample_filenames)
                
            f = open(output_path + psm_md5_filename, 'r')
            current_md5_lines = f.readlines()
            f.close()
            
            previous_md5 = {}
            for line in previous_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                previous_md5[filename] = checksum
                
            current_md5 = {}
            for line in current_md5_lines[md5_header:]:
                line = line.strip().split('\t')
                filename = line[0].strip()
                checksum = line[1].strip()
                
                current_md5[filename] = checksum
            
            for key in current_md5:
                run_sample = False
                
                if key not in previous_md5:
                    run_sample = True        
                elif previous_md5[key] != current_md5[key]:
                    run_sample = True
                
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(output_path + sample_filename + '.txt'):
                            run_samples.add(sample_filename)
                        break
                        
            for key in previous_md5:
                run_sample = False
                
                if key not in current_md5:
                    run_sample = True
                elif current_md5[key] != previous_md5[key]:
                    run_sample = True
                    
                for sample_filename in sample_filenames:
                    if sample_filename in key:
                        if run_sample == True or not os.path.exists(output_path + sample_filename + '.txt'):
                            run_samples.add(sample_filename)
                        break
       
            move_md5 = True
        except:
            if os.path.exists(output_path + psm_md5_filename):
                os.remove(output_path + psm_md5_filename)
                
            psm_md5(quantitation_path, quantitation_path, sample_filenames)
            inf_md5(peptide_protein_file, output_path)
            
            for sample_filename in sample_filenames:
                run_samples.add(sample_filename) 
    
    data_files_to_be_localized = []
    if run_samples != set():
        # pick only the sample filenames files
        data_files = []
        for f in data_files_all:
            current_filename = str(os.path.basename(f).replace('.txt', ''))
            
            if current_filename in run_samples:
                data_files.append(f)
                data_files_to_be_localized.append(current_filename)
        
        peptide_to_pg_map = {}
        print('Building peptide_to_pg_map ...')
    
        # stores the data from the peptide_protein file into a dictionary with the peptide as the key
        with open(peptide_protein_file) as f:
            for l in f:
                cells = l.rstrip().split('\t')
                p = cells[0]
                s = cells[1]
                m = cells[2]
                if p not in peptide_to_pg_map:
                    peptide_to_pg_map[p] = [s, m]
                else:
                    print('Unexpected. Please check the program.')
    
        files_len = len(data_files)
    
        percent_interval = floor(100.0 / files_len)
        if percent_interval < 5.0:
            percent_interval = 5.0
    
        percent_checked = 0.0
        num_files_checked = 0
    
        print 'Reading psm_quantitation files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
        sys.stdout.flush()
        for data_file in data_files:
            num_files_checked += 1
            
            basename = os.path.basename(data_file)
            
            out_file = output_path + basename
            if mod == 'intact':
                nglycoform_out_file = output_path + 'n-glycoforms\\' + basename
                oglycoform_out_file = output_path + 'o-glycoforms\\' + basename
                
            dm = DataMatrix.data_matrix_from_tsv_with_threshold(data_file, first_intensity_col, num_intensity_cols, intensity_threshold, header = False)
            dm.set_col_names(column_header_names)
            peptides = dm.column_to_list('Sequence')
            mod_peptides = dm.column_to_list('Modifications')
            
            if mod == 'phospho':
                pg = []
                at = []
                for i, p in enumerate(peptides):
                    mod_pep = mod_peptides[i]
                    is_phospho = phosphopeptide.has_motif(mod_pep, mod_peps)
                    
                    if p in peptide_to_pg_map and is_phospho:
                        pg.append(peptide_to_pg_map[p][0])
                        at.append(peptide_to_pg_map[p][1])
                    else:
                        pg.append('Not_Found')
                        at.append('Not_Found')
            elif mod == 'glyco':
                pg = []
                at = []
                for i, p in enumerate(peptides):
                    mod_pep = mod_peptides[i]
                    # verifies that the peptide has NxS/T motif and also contains the at least one deamidated amino acid
                    is_glyco_motif = glycopeptide.has_motif(p)
                    is_glyco = glycopeptide.has_modification(mod_pep, mod_peps)
                    
                    if p in peptide_to_pg_map and is_glyco_motif and is_glyco:
                        pg.append(peptide_to_pg_map[p][0])
                        at.append(peptide_to_pg_map[p][1])
                    else:
                        pg.append('Not_Found')
                        at.append('Not_Found')
            else:
                pg = [(peptide_to_pg_map[p][0] \
                        if p in peptide_to_pg_map else 'Not_Found') \
                        for p in peptides]
                at = [(peptide_to_pg_map[p][1] \
                        if p in peptide_to_pg_map else 'Not_Found') \
                        for p in peptides]
            
            dm.insert_column(pg, 'Protein.Group.Accessions', 2)
            dm.insert_column(at, 'PG.Assignment.Type', 3)
            dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions')
            dm.write_tsv(out_file)
            
            if mod == 'intact': 
                dm.write_nglycoforms_tsv(nglycoform_out_file)
                dm.write_oglycoforms_tsv(oglycoform_out_file)  
                
            current_percent = ((100.0 * num_files_checked) / files_len)
            if current_percent >= (percent_checked + percent_interval):
                percent_checked = floor(current_percent)
            
                print 'Reading psm_quantitation files progress... ' + str(percent_checked) + ' %'
                sys.stdout.flush()
                
        if percent_checked < 100.0:
            print 'Reading psm_quantitation files progress... 100 %'
            sys.stdout.flush()
            
    if mod == 'phospho' and localize_phosphosites:
        target_mod_name = 'phospho'
        
        # determine the amino acid/terminus where the modifications occur
        positions, fixed_mods, var_mods, target_mods = get_modification_positions(step3_config, target_mod_name)
        
        c_term_mod = False
        c_term_mods = set([])
        for position_key in positions:
            mod_masses = positions[position_key]
            
            if c_term_key in position_key:
                if not c_term_mod:
                    c_term_mod = True
                    
                for modification_mass in mod_masses:
                    c_term_mods.add(modification_mass)
                    
        c_term_mods = list(c_term_mods)
        
        # generate the luciphor2 input files for each set
        corrected_fixed_mods, corrected_var_mods, corrected_target_mods = generate_luciphor2_input_files(itraq_path, data_files_to_be_localized, positions, fixed_mods, var_mods, target_mods, c_term_mod, c_term_mods, luciphor2_config_file)
        
        luciphor2_input_path = itraq_path + 'luciphor2_input\\'
        luciphor2_output_path = itraq_path + 'luciphor2_output\\'
        
        # localize phosphosites using luciphor2 for each set
        run_luciphor2(data_root, luciphor2_input_path, luciphor2_output_path, data_files_to_be_localized, java_file, luciphor2_jar_file, luciphor2_config_file, localization_memory)
        
        # quality control statistics for the number of sets that were successfully localized by LuciPHOr2
        localized_sets = glob.glob(luciphor2_output_path + '*.txt')
        
        f = open(qc_path + 'stats-number_log_luciphor2-' + data_set_name + '.tsv', 'w')
        stats_line = '# sets localized'
        
        f.write(str(stats_line).strip() + '\n')
        
        # peptide matrices are generated for all different types of data
        stats_line = str(len(localized_sets))
        
        f.write(str(stats_line).strip() + '\n')
        
        f.close()
        
        # create map of proteins to peptide sequences from the fasta file in order to get site positions in the protein sequence
        f = open(fasta_file, 'r')
        fasta_lines = f.readlines()
        f.close()
        
        fasta_map = {}
        fasta_protein = ''
        fasta_seq = ''
        for line in fasta_lines:
            line = line.strip()
            
            if line[0] == '>':
                if fasta_protein != '' and fasta_seq != '':
                    fasta_map[fasta_protein] = fasta_seq
                    
                fasta_protein = line.split('>')[1].strip().split(' ')[0].strip()
                fasta_seq = ''
            else:
                fasta_seq += line
                
        if fasta_protein != '' and fasta_seq != '':
            fasta_map[fasta_protein] = fasta_seq
        
        filtered_results_output_path = itraq_path + 'phosphosites_localized\\'
        # filter the localized PSMs by global FLR
        filter_localized_results_by_global_flr(itraq_path, luciphor2_output_path, filtered_results_output_path, data_files_to_be_localized, fasta_map, positions, corrected_fixed_mods, corrected_var_mods, corrected_target_mods, c_term_mod, c_term_mods, flr)
        
        if run_samples != set():
            localized_folder = 'phosphosites_localized'
            generate_site_localization_statistics_per_set(output_path, localized_folder, qc_path, data_set_name)
        
    if move_md5 == True:
        # move new psm_md5_checksums.tsv from protein_inference folder to psm_quantitation folder
        os.remove(quantitation_path + psm_md5_filename)
        shutil.move(output_path + psm_md5_filename, quantitation_path + psm_md5_filename)
    
    if mod == 'intact' and run_samples != set():
        # the glycopeptide identifications only need to updated once the N-glyco and O-glyco matrices are generated 
        print '\nGenerating glycopeptide-level identifications by set...'
        print '-'
        sys.stdout.flush()
        
        glyco_folder = 'n-glycoforms'
        n_glyco_identifications = count_glycoform_identifications_per_set(output_path, glyco_folder)
        
        glyco_folder = 'o-glycoforms'
        o_glyco_identifications = count_glycoform_identifications_per_set(output_path, glyco_folder)
        
        f = open(qc_path + 'stats-glycopeptide_identifications-' + data_set_name + '.tsv', 'w')
        header_line = 'Filename'
        header_line += '\t' + 'n-glycopeptide identifications'
        header_line += '\t' + 'o-glycopeptide identifications'
        
        f.write(str(header_line).strip() + '\n')
        
        for glyco_key in sorted(n_glyco_identifications):
            n_glyco_val = n_glyco_identifications[glyco_key]
            o_glyco_val = o_glyco_identifications[glyco_key]
            
            output_line = str(glyco_key)
            output_line += '\t' + str(n_glyco_val)
            output_line += '\t' + str(o_glyco_val)
            
            f.write(str(output_line).strip() + '\n')
            
        f.close()
        
        print 'Success...'
        sys.stdout.flush()
        
        if label_type == 'free':
            # the FDR statistics only need to updated once the N-glyco and O-glyco matrices are generated 
            print '\nGenerating glycopeptide-level FDR statistics...'
            print '-'
            sys.stdout.flush()
            
            glyco_mod = 'nglycoform'
            glyco_folder = 'n-glycoforms'
            file_suffix = '-' + glyco_mod
            fdr_name = 'N-glycopeptide'
            n_glyco_fdr = update_glyco_fdr_statistics(output_path, qc_path, glyco_mod, glyco_folder, file_suffix, fdr_name)
            
            glyco_mod = 'oglycoform'
            glyco_folder = 'o-glycoforms'
            file_suffix = '-' + glyco_mod
            fdr_name = 'O-glycopeptide'
            o_glyco_fdr = update_glyco_fdr_statistics(output_path, qc_path, glyco_mod, glyco_folder, file_suffix, fdr_name)
            
            f = open(qc_path + 'stats-fdr_log-' + data_set_name + '.log', 'r')
            fdr_lines = f.readlines()
            f.close()
            
            # only keep the first 3 lines so we don't keep adding in more glycopeptide FDR statistics
            fdr_output_lines = list(fdr_lines[:3])
            
            for fdr_row in n_glyco_fdr:
                fdr_output_lines.append(fdr_row)
                
            for fdr_row in o_glyco_fdr:
                fdr_output_lines.append(fdr_row)
            
            f = open(qc_path + 'stats-fdr_log-' + data_set_name + '.log', 'w')
            for fdr_line in fdr_output_lines:
                f.write(fdr_line)
            f.close()
            
            print 'Success...'
            sys.stdout.flush()
    
    write_config_params = []
    write_config_params.append('psm_quantitation_path = ' + quantitation_path)
    write_config_params.append('peptide_protein_file = ' + peptide_protein_file)
    write_config_params.append('label_type = ' + label_type)
    write_config_params.append('intensity_threshold = ' + str(intensity_threshold))
    write_config_params.append('false_localization_rate = ' + str(flr * 100.0) + '%')
    write_config_outfile = output_path + 'step6.config'
    
    write_mspy(write_config_params, write_config_outfile)
    write_tp(config_path + 'sample_filenames.txt', write_config_outfile, 'sample_filenames.txt settings')
    write_tp(tmp_path + 'luciphor2_input.txt', write_config_outfile, 'luciphor2_input.txt settings')
    
    os.remove(tmp_path + 'luciphor2_input.txt')

# ******************************************************************************

def generate_site_localization_statistics_per_set(output_path, localized_folder, qc_path, data_set_name):
    # column of the phosphosite index in the matrix file
    phosphosite_index_col = -6
    
    print '\nGenerating quality control site localization statistics by set...'
    print '-'
    sys.stdout.flush()
    
    phospho_files = glob.glob(output_path + localized_folder + '\\*.txt')
    
    output_data = {}
    for phospho_f in phospho_files:
        basename = os.path.basename(phospho_f).replace('.txt', '')
        
        output_data[basename] = []
        
        f = open(phospho_f, 'r')
        matrix_lines = f.readlines()
        f.close()
        
        unlocalized_sites_set = set([])
        localized_sites_set = set([])
        for line in matrix_lines[header:]:
            line = line.strip().split('\t')
            phosphosite_index = line[phosphosite_index_col].strip().replace('"', '')
            site_position = phosphosite_index.split('_')[-1].strip()
            
            if site_position == '0':
                unlocalized_sites_set.add(phosphosite_index)
            else:
                localized_sites_set.add(phosphosite_index)
        
        unlocalized_sites = len(unlocalized_sites_set)
        localized_sites = len(localized_sites_set)
        
        total_sites = localized_sites + unlocalized_sites
        percent_unlocalized_sites = round((float(unlocalized_sites) / float(total_sites)) * 100.0, 2)
        
        output_data[basename].append(str(total_sites))
        output_data[basename].append(str(localized_sites))
        output_data[basename].append(str(unlocalized_sites))
        output_data[basename].append(str(percent_unlocalized_sites))
    
    #print basename
    #print '-'
    #print 'Total sites = ' +  str(total_sites)        
    #print 'Localized sites = ' + str(localized_sites)
    #print 'Unlocalized sites = ' + str(unlocalized_sites)
    #print 'Percent unlocalized sites = ' + str(round((float(unlocalized_sites) / float(total_sites)) * 100.0, 2)) + ' %'
    
    output_file = qc_path + 'stats-site_localization_identifications-' + data_set_name + '.tsv'
    f = open(output_file, 'w')
    
    header_line = 'Filename'
    header_line += '\t' + 'total sites'
    header_line += '\t' + 'localized sites'
    header_line += '\t' + 'unlocalized sites'
    header_line += '\t' + 'percent unlocalized sites'
    
    f.write(str(header_line).strip() + '\n')
    
    for site_key in sorted(output_data):
        output_line = str(site_key)
        
        output_row = output_data[site_key]
        for site_val in output_row:
            output_line += '\t' + str(site_val)
            
        f.write(str(output_line).strip() + '\n')
    
    f.close()
        
    print 'Success...'
    sys.stdout.flush()

def count_glycoform_identifications_per_set(output_path, glyco_folder):
    glycoform_col = 0
    
    glyco_psm_files = glob.glob(output_path + glyco_folder + '\\*.txt')
    
    output_data = {}
    for glyco_psm_f in glyco_psm_files:
        basename = os.path.basename(glyco_psm_f).replace('.txt', '')
        
        f = open(glyco_psm_f, 'r')
        matrix_lines = f.readlines()
        f.close()
        
        glycoforms = set([])
        for line in matrix_lines[header:]:
            line = line.strip().split('\t')
            glycoform = line[glycoform_col].strip()
            
            glycoforms.add(glycoform)
            
        output_data[basename] = len(glycoforms)
        
    return output_data

def update_glyco_fdr_statistics(output_path, qc_path, glyco_mod, glyco_folder, file_suffix, fdr_name):
    glycoform_col = 0
    
    glyco_psm_files = glob.glob(output_path + glyco_folder + '\\*.txt')
    
    glycoforms = set([])
    for glyco_psm_f in glyco_psm_files:
        f = open(glyco_psm_f, 'r')
        matrix_lines = f.readlines()
        f.close()
        
        for line in matrix_lines[header:]:
            line = line.strip().split('\t')
            glycoform = line[glycoform_col].strip()
            
            glycoforms.add(glycoform)
            
    num_true_glycopeptides = len(glycoforms)  
    
    # calculate psm, peptide, and protein level fdr for the total protein inference results
    fdr_output = []
    
    peptide_fdr = -1
    
    try:
        decoy_path = output_path.replace('step6-peptide_protein_quantitation\\', 'step5-decoy\\')    
        
        if os.path.isfile(decoy_path + 'reversed_decoy_log' + file_suffix + '.log'):
            f_pep = open(decoy_path + 'reversed_decoy_log' + file_suffix + '.log', 'r')
            decoy_lines = f_pep.readlines()
            f_pep.close()
        else:
            decoy_lines = []
            fdr_output.append('reversed_decoy_log' + file_suffix + '.log file is missing from the decoy folder.  Please rerun Step 5. \n\n') 
                    
        for line in decoy_lines:
            line = line.strip()
            
            if ':' in line:
                line = line.split(':')
                line_key = str(line[0].strip())
                line_val = str(line[1].strip())
            
                if line_val[-1] == ',':
                    line_val = float(line_val[:-1])
                else:
                    line_val = float(line_val)
                
                if line_key == '"final_decoy_peptide_num"':
                    peptide_fdr = (line_val / float(num_true_glycopeptides)) * 100.0
                        
            if peptide_fdr != -1:
                break
    except:
        print '-'
        print 'Error with p6_peptide_protein_quantitation.update_glyco_fdr_statistics()...'
        e = traceback.format_exc()
        print e
        print '-'
        sys.stdout.flush()
            
        print '\nAn error occurred while calculating the glycopeptide fdr.'
        sys.stdout.flush()
        
        fdr_output.append('Unexpected error: \n' + str(e) + '\n\n')
    
    fdr_output.append(fdr_name + ' fdr = ' + str(peptide_fdr) + ' %\n')  
    
    return fdr_output 

def determine_protein_position_of_peptide(pep, protein_seq):
    first_pep_index = -1
    num_occurrences = 0
    
    if pep in protein_seq:
        first_pep_index = protein_seq.find(pep) + 1
        num_occurrences = protein_seq.count(pep)
    else:
        raise Exception(pep + ' was unexpectedly not found in its assigned protein sequence')
    
    return first_pep_index, num_occurrences
    
def determine_first_and_last_site_amino_acids(pep, target_mod_aminos):
    first_amino_position = -1
    last_amino_position = -1
    for index, aa in enumerate(pep):
        if aa in target_mod_aminos:
            if first_amino_position == -1:
                first_amino_position = index + 1
                
            last_amino_position = index + 1
            
    if first_amino_position == -1 or last_amino_position == -1:
        raise Exception('the target modification amino acids were not in the peptide sequence')
    
    return first_amino_position, last_amino_position

def add_mod_masses(keys_of_interest, mods):
    # returns the mass string to be added to the corrected modified peptide sequence from the luciphor2 output
    mass_str = ''
    for key in mods:
        key_lower = key.lower()
        
        found_key = False
        for key_of_interest in keys_of_interest:
            if key_lower == key_of_interest:
                found_key = True
                break
                
        if found_key:
            for mod_mass in mods[key]:
                mass_str += '[' + str(mod_mass) + ']'
                        
    return mass_str

def correct_mod_pep_seq(mod_sites_map, localized_mod_pep, corrected_fixed_mods, corrected_var_mods, corrected_target_mods):
    # determines how many modifications should be in the final modified peptide sequence
    original_num_mods = 0
    for key in mod_sites_map:
        for mod_mass in mod_sites_map[key]:
            original_num_mods += 1
    
    # keeps record of the amino acid positions where the target mods are localized to
    target_mods_positions = {}
    
    corrected_mod_pep = ''
    num_mods_added = 0
    aa_index = -1
    for aa in localized_mod_pep:
        if aa.isalpha():
            aa_index += 1
            
        aa_index_str = str(aa_index)     
        aa_lower = aa.lower()
        aa_upper = aa.upper()
         
        if aa == '[' or aa == ']':
            if aa in mod_sites_map:
                for mod_mass in mod_sites_map[aa]:
                    corrected_mod_pep += '[' + str(mod_mass) + ']'
                    num_mods_added += 1
        elif aa == aa_upper:
            corrected_mod_pep += aa
            if aa_index_str in mod_sites_map and aa not in corrected_var_mods and aa not in corrected_target_mods:
                for mod_mass in mod_sites_map[aa_index_str]:
                    corrected_mod_pep += '[' + str(mod_mass) + ']'
                    num_mods_added += 1
        elif aa == aa_lower:
            keys_of_interest = [aa]
            corrected_mod_pep += aa_upper
            
            mass_str = add_mod_masses(keys_of_interest, corrected_var_mods)           
            corrected_mod_pep += mass_str
            if mass_str != '':
                num_mods_added += 1
            
            mass_str = add_mod_masses(keys_of_interest, corrected_target_mods)
            corrected_mod_pep += mass_str
            if mass_str != '':
                num_mods_added += 1
                # 0-based index
                target_mods_positions[aa_index] = aa_upper
            
    if original_num_mods != num_mods_added:
        raise Exception(corrected_mod_pep + ' was not correctly formatted from the localized sequence format, ' + localized_mod_pep + '')
    
    return corrected_mod_pep, target_mods_positions

def filter_localized_results_by_global_flr(itraq_path, input_path, output_path, data_files_to_be_localized, fasta_map, positions, corrected_fixed_mods, corrected_var_mods, corrected_target_mods, c_term_mod, c_term_mods, flr):
    print '\nStep 6c: Filtering localized PSMs by global false localization rate...'
    print '-' * 150
    sys.stdout.flush()
    
    # creates the output path folders
    make_path(output_path)
    
    # peptide_protein_quantitation data columns to be extracted
    p_pep_col = 0
    p_mod_pep_col = 1
    p_protein_col = 2
    p_spectrum_col = -1
    
    # luciphor2_output data columns to be extracted
    spectrum_col = 0
    pep1_col = 2
    pep2_col = 3
    pep1_score_col = 8
    pep2_score_col = 9
    global_flr_col = 10
    
    # column to insert the phosphosite and phosphopeptide indices into the localized output file
    phospho_insert_col = -4
    
    header = 1
    
    target_mod_aminos = sorted(corrected_target_mods)
    
    input_files = sorted(glob.glob(input_path + '*.txt'))
    for input_f in input_files:
        basename = os.path.basename(input_f).replace('-localized.txt', '')
        
        output_file = output_path + basename + '.txt'
        if basename in data_files_to_be_localized or not os.path.exists(output_file):
            print 'Filtering ' + basename + ' ...'
            sys.stdout.flush()
            
            f = open(input_f, 'r')
            input_lines = f.readlines()
            f.close()
            
            data_f = itraq_path + basename + '.txt'
            f = open(data_f, 'r')
            data_lines = f.readlines()
            f.close()
                
            output_header = data_lines[0].strip().split('\t')
            output_header.insert(phospho_insert_col, 'Phosphosite.Index')
            output_header.insert(phospho_insert_col, 'Phosphopeptide.Index')
                
            localized_map = {}
            for line in input_lines[header:]:
                line = line.strip().split('\t')
                global_flr = float(line[global_flr_col])
                spectrum = line[spectrum_col].strip()
                
                if global_flr <= flr:
                    pep1_score = float(line[pep1_score_col])
                    pep2_score = float(line[pep2_score_col])
                    
                    mod_pep_col = ''
                    if pep1_score > pep2_score:
                        mod_pep_col = pep1_col
                    elif pep2_score > pep1_score:
                        mod_pep_col = pep2_col
                    else:
                        print '-'
                        print 'Error with p6_peptide_protein_quantitation.filter_localized_results_by_global_flr()...'
                        print traceback.format_exc()
                        print "\n\npep1score is equal to pep2score..."
                        print '-'
                        sys.stdout.flush()
                        
                    mod_pep = line[mod_pep_col]
                    
                    localized_map[spectrum] = mod_pep
                    
            output_data = []
            for line in data_lines[header:]:
                line = line.strip().split('\t')
                spectrum = line[p_spectrum_col].strip()
                
                current_pep = line[p_pep_col].strip()
                current_mod_pep = line[p_mod_pep_col].strip()
                current_protein = line[p_protein_col].strip().split(';')[0].strip()
                output_line = list(line)
                
                # format the modification sites for input into Luciphor2
                mod_sites = determine_mod_sites(current_mod_pep, positions, c_term_mods, include_c_term_mod = c_term_mod)
                mod_sites = mod_sites.split(',')
                mod_sites_map = {}
                for mod_site in mod_sites:
                    mod_site = mod_site.strip().split('=')
                    mod_position = mod_site[0].strip()
                    mod_mass = float(mod_site[1])
                    
                    # reformat the position key to same as luciphor2 output
                    if mod_position == '-100':
                        mod_position = '['
                    elif mod_position == '100':
                        mod_position = ']'
                    
                    if mod_position not in mod_sites_map:
                        mod_sites_map[mod_position] = []
                    
                    if mod_mass not in mod_sites_map[mod_position]:
                        mod_sites_map[mod_position].append(mod_mass)
                
                first_site_pep_index, last_site_pep_index = determine_first_and_last_site_amino_acids(current_pep, target_mod_aminos)
                # determine the position in the protein sequence that the peptide starts
                first_protein_pep_index, num_peptides_in_protein = determine_protein_position_of_peptide(current_pep, fasta_map[current_protein])
                
                # -1 is because both indices are 1-based
                first_site_protein_index = first_site_pep_index + first_protein_pep_index - 1
                last_site_protein_index = last_site_pep_index + first_protein_pep_index - 1
                
                num_possible_sites = 0
                for aa in corrected_target_mods:
                    for mod_mass in corrected_target_mods[aa]:
                        mod_amino = aa + '[' + str(mod_mass) + ']'
                        if mod_amino in current_mod_pep:
                            num_possible_sites += current_mod_pep.count(mod_amino)
                  
                num_localized_sites = 0  
                site_positions = ''            
                if spectrum in localized_map:
                    localized_mod_pep = localized_map[spectrum]
                    # convert the luciphor2 output modified peptide sequence to the format from MS-GF+
                    corrected_mod_pep, target_mods_positions = correct_mod_pep_seq(mod_sites_map, localized_mod_pep, corrected_fixed_mods, corrected_var_mods, corrected_target_mods)
                    output_line[p_mod_pep_col] = corrected_mod_pep
                
                    # determines that the site is in fact not localized if the peptide sequence is not unique in the protein sequence
                    if num_peptides_in_protein == 1:
                        for aa in corrected_target_mods:
                            for mod_mass in corrected_target_mods[aa]:
                                mod_amino = aa + '[' + str(mod_mass) + ']'
                                if mod_amino in corrected_mod_pep:
                                    num_localized_sites += corrected_mod_pep.count(mod_amino)
                        
                        for site_position in sorted(target_mods_positions):
                            protein_site_position = first_protein_pep_index + site_position
                            site_positions += target_mods_positions[site_position] + str(protein_site_position)
                    
                # create phosphosite key
                site_key = current_protein
                site_key += '_' + str(first_site_protein_index)
                site_key += '_' + str(last_site_protein_index)
                site_key += '_' + str(num_possible_sites)
                site_key += '_' + str(num_localized_sites)
                
                if site_positions != '':
                    site_key += '_' + site_positions
                    
                # create phosphopeptide key
                pep_key = current_protein
                pep_key += '_' + str(first_site_protein_index)
                pep_key += '_' + str(last_site_protein_index)
                
                output_line.insert(phospho_insert_col, site_key)
                output_line.insert(phospho_insert_col, pep_key)
                
                output_data.append(output_line)
                    
            f = open(output_file, 'w')
            f.write(str('\t'.join(output_header)).strip() + '\n')
            
            for output_row in output_data:
                output_line = '\t'.join(output_row)
                f.write(str(output_line).strip() + '\n')
            
            f.close()

def correct_masses(mod_dict, found_masses):
    updated_dict = copy.deepcopy(mod_dict)
    for mod_position in updated_dict:
        for mass_index, mod_mass in enumerate(updated_dict[mod_position]):
            if mod_mass not in found_masses:
                for found_mass in found_masses:
                    min_mass = float(min(mod_mass, found_mass))
                    max_mass = float(max(mod_mass, found_mass))
                    
                    # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
                    if ((min_mass / max_mass) * 100.0) >= 99.999:
                        updated_dict[mod_position][mass_index] = found_mass
                        break
    
    return updated_dict

def retrieve_mod_masses_from_input_files(input_files):
    mod_sites_col = -1 
    header = 1
         
    found_mod_masses = set([])
    for input_file in input_files:
        f = open(input_file, 'r')
        input_lines = f.readlines()
        f.close()
        
        for line in input_lines[header:]:
            line = line.strip().split('\t')
            mod_sites = line[mod_sites_col].strip().split(',')
            
            # keep record of the modification masses that were found
            for mod_site_val in mod_sites:
                mod_site_val = mod_site_val.strip()
                
                mod_site_mass = float(mod_site_val.split('=')[1].strip())
                
                found_mod_masses.add(mod_site_mass)
            
    found_mod_masses = list(found_mod_masses)
    
    return found_mod_masses

def update_luciphor2_modifications_config_file(corrected_fixed_mods, corrected_var_mods, corrected_target_mods, luciphor2_config_file):
    # updates the modifications with the modifications specified in the LuciPHOr2 input file
    f = open(luciphor2_config_file, 'r')
    config_lines = f.readlines()
    f.close()
    
    fixed_mods_section = False
    var_mods_section = False
    target_mods_section = False
    
    decoy_mass = set([])
    for mod_position in corrected_target_mods:
        for mod_mass in corrected_target_mods[mod_position]:
            decoy_mass.add(mod_mass)
            
    decoy_mass = list(decoy_mass)
    if len(decoy_mass) > 1:
        print '-'
        print 'Error with p6_peptide_protein_quantitation.update_luciphor2_modifications_config_file()...'
        print traceback.format_exc()
        print "\n\nDecoy mass could not be determined..."
        print '-'
        sys.stdout.flush()
    
    decoy_mass = decoy_mass[0]    
    
    config_output_data = []
    for line in config_lines:
        line_strip = line.strip()

        output_line = ''        
        if fixed_mods_section:
            if line_strip == "## For C-terminal modifications use ']' for the residue character":
                output_line = line_strip + '\n'
                
                for mod_position in sorted(corrected_fixed_mods):
                    mod_position_lower = mod_position.lower()
                    formatted_position = mod_position
                    if mod_position_lower == 'n-term' or mod_position_lower == 'prot-n-term':
                        formatted_position = '['
                    elif mod_position_lower == 'c-term' or mod_position_lower == 'prot-c-term':
                        formatted_position = ']'
                        
                    for mod_mass in corrected_fixed_mods[mod_position]:
                        output_line += 'FIXED_MOD = ' + formatted_position + ' ' + str(mod_mass) + '\n'
                        
                output_line += '\n'  
            else:
                if line_strip[:len('FIXED_MOD')] != 'FIXED_MOD' and line_strip != '':
                    output_line = line
        elif var_mods_section:
            if line_strip == "## For C-terminal modifications use ']' for the residue character":
                output_line = line_strip + '\n'
                
                for mod_position in sorted(corrected_var_mods):
                    mod_position_lower = mod_position.lower()
                    formatted_position = mod_position
                    if mod_position_lower == 'n-term' or mod_position_lower == 'prot-n-term':
                        formatted_position = '['
                    elif mod_position_lower == 'c-term' or mod_position_lower == 'prot-c-term':
                        formatted_position = ']'
                        
                    for mod_mass in corrected_var_mods[mod_position]:
                        output_line += 'VAR_MOD = ' + formatted_position + ' ' + str(mod_mass) + '\n'
                        
                output_line += '\n'  
            else:
                if line_strip[:len('VAR_MOD')] != 'VAR_MOD' and line_strip != '':
                    output_line = line
        elif target_mods_section:
            if line_strip == "## Syntax: TARGET_MOD = <RESIDUE> <MODIFICATION_MASS>":
                output_line = line_strip + '\n'
                
                for mod_position in sorted(corrected_target_mods):
                    mod_position_lower = mod_position.lower()
                    formatted_position = mod_position
                    if mod_position_lower == 'n-term' or mod_position_lower == 'prot-n-term':
                        formatted_position = '['
                    elif mod_position_lower == 'c-term' or mod_position_lower == 'prot-c-term':
                        formatted_position = ']'
                        
                    for mod_mass in corrected_target_mods[mod_position]:
                        output_line += 'TARGET_MOD = ' + formatted_position + ' ' + str(mod_mass) + '\n'
                        
                output_line += '\n'
            else:
                if line_strip[:len('TARGET_MOD')] != 'TARGET_MOD' and line_strip != '':
                    output_line = line
        else:
            if line_strip[:len('DECOY_MASS')] == 'DECOY_MASS':
                comment = ''
                if '#' in line_strip:
                    comment = '## ' + line_strip.split('#')[-1].strip()
                    
                output_line = 'DECOY_MASS = ' + str(decoy_mass) + '  ' + comment + '\n'
            else:
                output_line = line
        
        if line_strip == "## Place here any FIXED modifications that were used in your search":
            fixed_mods_section = True
            var_mods_section = False
            target_mods_section = False
        elif line_strip == "## Place here any VARIABLE modifications that might be encountered that you don't":
            fixed_mods_section = False
            var_mods_section = True
            target_mods_section = False
        elif line_strip == "## List the amino acids to be searched for and their mass modifications":
            fixed_mods_section = False
            var_mods_section = False
            target_mods_section = True
        elif line_strip == "## List the types of neutral losses that you want to consider":
            fixed_mods_section = False
            var_mods_section = False
            target_mods_section = False
            
        if output_line != '' or (output_line == '' and not fixed_mods_section and not var_mods_section and not target_mods_section):
            config_output_data.append(output_line)
        
    f = open(luciphor2_config_file, 'w')
    for output_line in config_output_data:
        f.write(str(output_line))
        
    f.close()

def update_luciphor2_filepaths_config_file(mzml_path, input_file, output_file, luciphor2_config_file):
    f = open(luciphor2_config_file, 'r')
    config_lines = f.readlines()
    f.close()
    
    config_output_data = []
    for line in config_lines:
        line_strip = line.strip()
        
        output_line = line
        for config_key in luciphor2_config_keys:
            if line_strip[:len(config_key)] == config_key:
                comment = ''
                if '#' in line_strip:
                    comment = '## ' + line_strip.split('#')[-1].strip()
                    
                if config_key == 'SPECTRUM_PATH':
                    output_line = 'SPECTRUM_PATH = ' + mzml_path + ' ' + comment + '\n'
                elif config_key == 'INPUT_DATA':
                    output_line = 'INPUT_DATA = ' + input_file + ' ' + comment + '\n'
                elif config_key == 'OUTPUT_FILE':
                    output_line = 'OUTPUT_FILE = ' + output_file + ' ' + comment + '\n'
                    
        config_output_data.append(output_line)
        
    f = open(luciphor2_config_file, 'w')
    for output_line in config_output_data:
        f.write(str(output_line))
        
    f.close()
        
def run_luciphor2(data_root, input_path, output_path, data_files_to_be_localized, java_file, luciphor2_jar_file, luciphor2_config_file, localization_memory): 
    print '\nStep 6b: Using LuciPHOr2 to localize phosphosites...'
    print '-' * 150
    sys.stdout.flush()
    
    mzml_path = data_root + 'step1-mzml\\'
    
    # creates the output path folders
    make_path(output_path)
    
    data_files = sorted(glob.glob(input_path + '*.txt'))
    for data_f in data_files:
        input_filename = os.path.basename(data_f)
        basename = input_filename.replace('-luciphor2.txt', '')
        output_file = output_path + basename + '-localized.txt'
        if basename in data_files_to_be_localized or not os.path.exists(output_file):
            # update configs in the luciphor2 config file
            update_luciphor2_filepaths_config_file(mzml_path, data_f, output_file, luciphor2_config_file)
        
            subprocess.call(java_file + ' -Xmx' + localization_memory + 'M' + \
                ' -jar ' + luciphor2_jar_file + \
                ' ' + luciphor2_config_file
                )

def generate_luciphor2_input_files(data_path, data_files_to_be_localized, positions, fixed_mods, var_mods, target_mods, c_term_mod, c_term_mods, luciphor2_config_file):
    print '\nStep 6a: Generating input files for LuciPHOr2...'
    print '-' * 150
    sys.stdout.flush()
    
    output_path = data_path + 'luciphor2_input\\'
    
    # creates the output path folders
    make_path(output_path)
    
    # peptide_protein_quantitation data columns to be extracted
    pep_col = 0
    mod_pep_col = 1
    scan_col = -3
    score_col = -2
    # charge information comes from filename_col
    filename_col = -1

    header = 1
    
    data_files = sorted(glob.glob(data_path + '*.txt'))
    
    for data_f in data_files:
        basename = os.path.basename(data_f).replace('.txt', '').strip()
        luciphor2_filename = basename + '-luciphor2.txt'
        if basename in data_files_to_be_localized or not os.path.exists(output_path + luciphor2_filename):
            print 'Generating the Luciphor2 formatted input file for ' + basename + ' ...'
            sys.stdout.flush()
            
            f = open(data_f, 'r')
            lines = f.readlines()
            f.close()
            
            output_data = []
            for line in lines[header:]:
                line = line.strip().split('\t')
                pep = line[pep_col].strip()
                mod_pep = line[mod_pep_col].strip()
                scan = line[scan_col].strip()
                score = line[score_col].strip()
                filename_data = line[filename_col].strip().split('.')
                charge = filename_data[-1].strip()
                filename = filename_data[0].strip() + '.mzML'
                
                # format the modification sites for input into Luciphor2
                mod_sites = determine_mod_sites(mod_pep, positions, c_term_mods, include_c_term_mod = c_term_mod)
                
                output_row = []
                output_row.append(filename)
                output_row.append(scan)
                output_row.append(charge)
                output_row.append(score)
                output_row.append(pep)
                output_row.append(mod_sites)
                
                output_data.append(output_row)
                
            f = open(output_path + luciphor2_filename, 'w')
            f.write(str(luciphor_input_header_line).strip() + '\n')
        
            for output_row in output_data:
                output_line = '\t'.join(output_row)
                
                f.write(str(output_line).strip() + '\n')
            
            f.close()
            
    output_files = sorted(glob.glob(output_path + '*-luciphor2.txt'))
    found_mod_masses = retrieve_mod_masses_from_input_files(output_files)
    # correct the fixed_mods, var_mods, and target_mods lists for any masses that were calculated by pyteomics from a chemical composition
    corrected_fixed_mods = correct_masses(fixed_mods, found_mod_masses)
    corrected_var_mods = correct_masses(var_mods, found_mod_masses)
    corrected_target_mods = correct_masses(target_mods, found_mod_masses)
    
    # update the luciphor2 config file with the modifications that were used
    update_luciphor2_modifications_config_file(corrected_fixed_mods, corrected_var_mods, corrected_target_mods, luciphor2_config_file)
    
    return corrected_fixed_mods, corrected_var_mods, corrected_target_mods
        
def get_modification_positions(step3_config, target_mod_name):
    non_amino_positions = ['N-term', 'C-term', 'Prot-N-term', 'Prot-C-term']
    
    positions = {}
    fixed_mods = {}
    var_mods = {}
    target_mods = {}
    
    f = open(step3_config, 'r')
    msgf_lines = f.readlines()
    f.close()
    
    mod_section = False
    for line in msgf_lines:
        line = line.strip()
        
        if mod_section:
            if line == '#-# search parameters':
                break
                
        if not mod_section:
            if line == '#-# modifications':
                mod_section = True
                continue
                    
        if mod_section:
            if line != '':
                if line[0] != '#':
                    line = line.split(',')
                    # remove any comments from the end of the lines
                    line[-1] = line[-1].split('#')[0].strip()
                    modification_name = line[-1].lower()
                    modification_type = line[2]
                    
                    mod_mass_str = line[0]
                    mod_mass = ''
                    try:
                        mod_mass_val = float(mod_mass_str)
                        mod_mass = mod_mass_val
                    except:
                        try:
                            mod_mass_val = float(mass.calculate_mass(formula = mod_mass_str))
                            mod_mass = mod_mass_val
                        except:
                            try:
                                mod_mass_val = float(mass.calculate_mass(composition = mod_mass_str))
                                mod_mass = mod_mass_val
                            except:
                                print '-'
                                print 'Error with p6_peptide_protein_quantitation.get_modification_positions()...'
                                print traceback.format_exc()
                                print "\n\nCould not determine the modification's mass..."
                                print '-'
                                sys.stdout.flush()
                                
                                mod_mass = ''                        
                    
                    mod_position = ''
                    if line[1] == '*':
                        mod_position = line[3]
                    else:
                        mod_position = line[1]
                        
                    if mod_position not in non_amino_positions:
                        mod_positions = list(mod_position)
                    else:
                        mod_positions = [mod_position]
                    
                    if modification_name == target_mod_name:
                        if mod_mass != '':
                            if mod_position not in target_mods:
                                target_mods[mod_position] = set([])
                            
                            target_mods[mod_position].add(mod_mass)
                    elif modification_type == 'fix':
                        if mod_mass != '':
                            if mod_position not in fixed_mods:
                                fixed_mods[mod_position] = set([])
                        
                            fixed_mods[mod_position].add(mod_mass)
                    elif modification_type == 'opt':
                        if mod_mass != '':
                            if mod_position not in var_mods:
                                var_mods[mod_position] = set([])
                                
                            var_mods[mod_position].add(mod_mass)
                    
                    if mod_mass != '':
                        for mod_p in mod_positions:
                            if mod_p not in positions:    
                                positions[mod_p] = []
                                positions[mod_p].append(mod_mass)
                            else:
                                positions[mod_p].append(mod_mass)
    
    for key in fixed_mods:
        fixed_mods[key] = list(fixed_mods[key])
        
    for key in var_mods:
        var_mods[key] = list(var_mods[key])
        
    for key in target_mods:
        target_mods[key] = list(target_mods[key])
        
    return positions, fixed_mods, var_mods, target_mods

def check_if_mod_in_mods_list(current_mod, mods):
    included = False
    for mods_val in mods:
        min_mass = float(min(current_mod, mods_val))
        max_mass = float(max(current_mod, mods_val))
        
        # MS-GF+ and pyteomics calculate masses from chemical forumals ever so slightly differently
        if ((min_mass / max_mass) * 100.0) >= 99.999:
            included = True
            break
            
    return included

def determine_mod_sites(mod_pep, positions, c_term_mods, include_c_term_mod = False):
    mod_sites = ''
    
    # determine position of last amino acid
    last_amino = -2
    for i, aa in enumerate(mod_pep):
        if aa.isalpha():
            last_amino = i
            
    # check to make sure that the modified peptide had at least one amino acid
    if last_amino == -2:
        raise Exception('could not determine the last amino acid position for the following modified peptide sequence, ' + str(mod_pep))
    
    # generate mod_sites string
    amino_num = -1
    current_mod = ''
    current_amino = ''
    c_terminus_flag = False
    for i, aa in enumerate(mod_pep):    
        if aa.isalpha():
            current_amino = aa
            
            if current_mod != '':
                if amino_num == -1:
                    if mod_sites != '':
                        mod_sites += ',' + '-100=' + current_mod
                    else:
                        mod_sites += '-100=' + current_mod
                elif c_terminus_flag:
                    in_c_term_mods = check_if_mod_in_mods_list(float(current_mod), c_term_mods)
                    in_non_c_term_mods = False
                    if current_amino in positions:
                        in_non_c_term_mods = check_if_mod_in_mods_list(float(current_mod), positions[current_amino])
                    
                    if in_c_term_mods and not in_non_c_term_mods:
                        if mod_sites != '':
                            mod_sites += ',' + '100=' + current_mod
                        else:
                            mod_sites += '100=' + current_mod
                    else:
                        if mod_sites != '':
                            mod_sites += ',' + str(amino_num) + '=' + current_mod 
                        else:
                            mod_sites += str(amino_num) + '=' + current_mod 
                else:
                    if mod_sites != '':
                        mod_sites += ',' + str(amino_num) + '=' + current_mod 
                    else:
                        mod_sites += str(amino_num) + '=' + current_mod 
            
            amino_num += 1
            current_mod = ''
        elif aa.isdigit() or aa == '.':
            current_mod += aa
            
        if i == last_amino:
            if include_c_term_mod:
                c_terminus_flag = True
            
    # adds any additional C-terminus modifications
    if current_mod != '':
        if c_terminus_flag:
            in_c_term_mods = check_if_mod_in_mods_list(float(current_mod), c_term_mods)
            in_non_c_term_mods = False
            if current_amino in positions:
                in_non_c_term_mods = check_if_mod_in_mods_list(float(current_mod), positions[current_amino])
            
            if in_c_term_mods and not in_non_c_term_mods:
                if mod_sites != '':
                    mod_sites += ',' + '100=' + current_mod
                else:
                    mod_sites = '100=' + current_mod
            else:
                if mod_sites != '':
                    mod_sites += ',' + str(amino_num) + '=' + current_mod 
                else:
                    mod_sites += str(amino_num) + '=' + current_mod 
        else:
            if mod_sites != '':
                mod_sites += ',' + str(amino_num) + '=' + current_mod 
            else:
                mod_sites += str(amino_num) + '=' + current_mod 
    
    return mod_sites
                                                            
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['luciphor2'] = rluciphor2(config_path + 'luciphor2_input.txt')
    configs['backup'] = True

    main(configs)