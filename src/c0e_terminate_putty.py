import os
import sys
import subprocess
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'

def main(configs):
    print '\nStep c0e: Terminating the PuTTY cluster...'
    print '-' * 150
    sys.stdout.flush()
    
    options = configs['options']
    exes = configs['exes']
    
    cluster_name = 'puttycluster_' + str(options['cluster_name']).strip()
    python2_path = os.path.dirname(exes['python']) + '\\'
    starcluster_file = python2_path + 'Scripts\\starcluster.exe'
    
    terminate_command = starcluster_file + ' -c ' + starcluster_config_path + ' terminate --confirm --force ' + cluster_name
    print terminate_command
    sys.stdout.flush()
    proc = subprocess.Popen(terminate_command, stdin = subprocess.PIPE, shell = True)
    proc.wait()
    proc.terminate()
    
    print ''
    print '-' * 150
    print cluster_name + ' has been terminated!'
    print '-' * 150
    sys.stdout.flush()
    
if __name__ == '__main__':  
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    
    main(configs)