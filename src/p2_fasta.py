"""
Description:
    Downloads updated .faa (or other type) search database files, converts them to a .fasta file,
    and writes the .fasta file
    
Notes:
    - faa_path will be deleted and created anew.
    - compressed_faa_path will be deleted and created anew.
    - fasta_path will be deleted and created anew.
"""

import os
import re
import sys
import glob
import time
import shutil
import ftplib
import tkMessageBox
from Tkinter import *
from hotpot.extracting import unzip
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.configuring import read_steps_config as steps
from hotpot.dating import timestamp_to_datetime as time_to_date

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
tmp_path = pipeline_path + 'tmp\\'

def main(configs): 
    print '\nStep 2:\tUpdating faa search database files and converting to the fasta file...'
    print '-' * 150
    sys.stdout.flush()
    
    # copying the configs is not necessary since this step only downloads a fasta file from ftp
    #if configs['backup']:
    #    # copies the config files over
    #    data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
    #    if not os.path.exists(data_config_copy_path):
    #        shutil.copytree(config_path, data_config_copy_path + 'configs\\')
    #        shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
    #        #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
    #        
    #    configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
        
    # sets variables to configuration values
    data_root = options['raw_data']
    #archived_path = options['archived_path']
    fasta_path = data_root + 'step2-fasta\\'
    faa_path = fasta_path + 'faa\\'
    ftp_address = options['ftp_address']
    ftp_user = options['ftp_user']
    ftp_password = options['ftp_password']
    ftp_dir = options['ftp_dir']
    file_name_check = options['faa_extension']
    fasta_filename = options['fasta_filename']
    unzip_prog = exes['7zip']
    
    # filepath for storing the compressed faa data
    compressed_faa_path = fasta_path + 'compressed_faa\\'
    
    fasta_file = fasta_path + fasta_filename
    
    if os.path.exists(fasta_file) == True:
        fasta_size = os.path.getsize(fasta_file)
    else:
        # does not exist yet
        fasta_size = -1
    
    if (not os.path.exists(fasta_file)) or fasta_size == 0:
        # connect to host, default port
        ftp = ftplib.FTP(ftp_address, ftp_user, ftp_password)
    
        # change to the human protein .faa file directory
        ftp.cwd(ftp_dir)    
        
        # determine the file's zip extension string length and adds 1 to it to account for '.' 
        zip_ext_len = len(file_name_check.split('.')[-1]) + 1
    
        # empty list to be filled with .faa file data and converted to a .fasta file
        fasta_data = []
    
        # creates the output path folders
        make_path(compressed_faa_path)
        make_path(faa_path)
        make_path(fasta_path)
    
        # retrieves the file names in the current directory
        file_names = sorted(ftp.nlst())
    
        file_name_check_len = len(file_name_check)
    
        # variable that will determine and store the modification time of the most recently modified faa file
        most_recent_time = 0
    
        # retrieves only the correct .faa files from ftp
        for f in file_names:
            if (len(f) >= file_name_check_len):
                # finds the correct .faa files and retrieves them to the local output path
                if (f[-1*(file_name_check_len):] == file_name_check):
                    faa_zipped_file = open(compressed_faa_path + f, 'wb')
                    print '\tDownloading faa file: ' + f
                    ftp.retrbinary('RETR ' + f, faa_zipped_file.write)
                    faa_zipped_file.close()
                    
                    unzipped_filename = f[:-1*zip_ext_len]
                    
                    # unzips the compressed .faa files
                    unzip(compressed_faa_path + f, faa_path, unzip_prog)
                    
                    mod_time = os.path.getmtime(faa_path + unzipped_filename)
                    # stores the modification time of the most recently modified faa file
                    if mod_time >= most_recent_time:
                        most_recent_time = mod_time
                    
                    # adds .faa file data to the fasta_data list
                    fasta_data.extend(read_fasta(faa_path + unzipped_filename))
        
        ftp.close()
        
        print '\tWriting fasta file: ' + fasta_file
        # writes the .fasta file
        write_fasta(fasta_data, fasta_file)
        
        fasta_size = os.path.getsize(fasta_file)
        
        if fasta_size == 0:
            shutil.rmtree(compressed_faa_path)
            shutil.rmtree(faa_path)
            shutil.rmtree(fasta_path)
            
            root = Tk()
            root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
            root.update_idletasks()
            tkMessageBox.showinfo('Error!', 'Files were not retrieved from ftp correctly.  Please check ftp parameters and try again.')
            root.destroy()
            
            return None
        else:
            write_config_params = []
            write_config_params.append('fasta_path = ' + fasta_path) 
            write_config_params.append('ftp_address = ' + ftp_address)
            write_config_params.append('ftp_user = ' + ftp_user)
            write_config_params.append('ftp_password = ' + ftp_password)
            write_config_params.append('ftp_dir = ' + ftp_dir)
            write_config_params.append('faa_extension = ' + file_name_check)
            write_config_params.append('fasta_filename = ' + fasta_filename)  
            write_config_outfile = fasta_path + 'step2.config'
        
            write_mspy(write_config_params, write_config_outfile)
            
        #    archived_faa_path = archived_path + 'download_files\\' + time_to_date(most_recent_time) + '\\'
        #    
        #    if not os.path.exists(archived_faa_path):
        #        make_path(archived_faa_path)
        #        faa_files = glob.glob(faa_path + '*.faa*')
        #        for faa in faa_files:
        #            faa_basename = os.path.basename(faa)
        #            shutil.copyfile(faa, archived_faa_path + faa_basename)
        #            os.remove(faa)
        #    else:
        #        faa_files = glob.glob(faa_path + '*.faa*')
        #        for faa in faa_files:
        #            os.remove(faa)
        #                
        #    archived_fasta_path = archived_path + time_to_date(most_recent_time) + '\\'
        #
        #    if not os.path.exists(archived_fasta_path):
        #        shutil.copytree(fasta_path, archived_fasta_path + '\\')
        #        
        #    return archived_fasta_path
        
            return fasta_path
        
# ******************************************************************************

def read_fasta(filename):
    with open(filename) as f:
        lines = f.readlines()

    fasta_data_current_file = []
    info = None
    for l in lines:
        l = l.rstrip()
        if l.startswith('>'):
            if info:
                fasta_data_current_file.append((info, ''.join(seq)))
            info, seq = l[1:], []
        else:
            seq.append(l)
    
    if info:
        fasta_data_current_file.append((info, ''.join(seq)))

    return fasta_data_current_file


def write_fasta(fasta_data, filename, multi_line = True,
                seq_line_width = 80):
    with open(filename, 'w') as f:
        for info, seq in fasta_data:
            f.write('>' + info + '\n')
            if multi_line:
                i = 0
                while (i + 1) * seq_line_width <= len(seq):
                    f.write(seq[(i * seq_line_width):((i + 1) * seq_line_width)] + '\n')
                    i += 1
                if i * seq_line_width < len(seq):
                    f.write(seq[(i * seq_line_width):] + '\n')     

# ******************************************************************************
                                        
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['backup'] = True
    
    main(configs)