"""
Description:
    Creates a tsv of the binary log of the channels 116/114 and 117/115 for each protein
    group.
    
Notes:
"""

import os
import sys
import glob
import time
import shutil
import numpy
import p7a_combining_expression_matrices as p7a
import p7b_expression_matrix_intensities as p7b
from math import floor
from hotpot.data_matrix import *
from hotpot.pathing import make_path
from hotpot.read_write import read_lines as rl
from hotpot.configuring import read_steps_config as steps
from hotpot.read_write import write_third_party_config_params as write_tp
from hotpot.read_write import write_mspycloud_config_params as write_mspy
from hotpot.fasta import fasta_gi_to_protein_name_dict as fasta_protein_names
from hotpot.md5_checksums import step7_protein_quant_md5_checksums as protein_md5
from hotpot.retrieve_qc_statistics_from_set_qc_files import retrieve_qc_stats as qc_stats

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'
header_rows = 1

def main(configs):
    options = configs['options']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    label_type = options['label_type']
    output_folder_path = options['archived_path']
    mod = str(options['modification']).lower()
    localize_phosphosites = str(options['localize_phosphosites']).lower()
    
    if mod == 'global':
        filename_prefix = ''
        protein_matrix_flag = True
        itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\'
        generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites)
    elif mod == 'intact':
        # generate n-linked glycoform matrices
        filename_prefix = 'n-glycoform'
        protein_matrix_flag = False
        itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\n-glycoforms\\'
        generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites)
        
        # generate o-linked glycoform matrices
        filename_prefix = 'o-glycoform'
        protein_matrix_flag = False
        itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\o-glycoforms\\'
        generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites)
    elif mod == 'phospho':
        filename_prefix = ''
        protein_matrix_flag = False
        
        if localize_phosphosites == 'true':
            itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\phosphosites_localized\\'
        else:
            itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\'
            
        generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites)
    elif mod == 'glyco':
        filename_prefix = ''
        protein_matrix_flag = False
        itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\'
        generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites)
        
    # compile the qc statistics that can be copied into the xlsx qc file
    if label_type != 'free':
        qc_stats(configs) 

def generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites): 
    options = configs['options']
    sample_filenames = sorted(configs['sample filenames'])
    
    # sets variables to configuration values
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    fasta_file = options['fasta_path']
    peptide_protein_file = output_folder_path + 'step4b-protein_inference\\peptide_protein.txt'
    #itraq_path = output_folder_path + 'step6-peptide_protein_quantitation\\'
    data_matrix_path = output_folder_path + 'step7-expression_matrix\\'
    norm_channel = str(options['normalization_channel'])
    label_type = options['label_type']
    mod = str(options['modification']).lower()
    contams_lines = configs['contams_lines']
    contams = get_fasta_contams(contams_lines)
    # assumes an equal sign after the gene name identifier
    gene_name_identifier = str(options['gene_name_identifier']) + '='
    
    peptide_protein_map = get_peptide_protein_map(peptide_protein_file)
    
    if label_type != 'free':
        print '\nStep 7:\tCreate tsv files for the log2 ratio expression matrices...'
        print '-' * 150
        sys.stdout.flush()
        
        if configs['backup']:
            # copies the config files over
            data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
            if not os.path.exists(data_config_copy_path):
                shutil.copytree(config_path, data_config_copy_path + 'configs\\')
                shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
                #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
                
            configs['backup'] = False
        
        filename_mod = ''
        if mod != 'global':
            if mod != 'intact':
                filename_mod = mod
            else:
                # converts 'n-glycoform' to 'nglycoform' and 'o-glycoform' to 'oglycoform' for the filename endings
                filename_mod = filename_prefix.replace('-', '')
                
        filename_dash = ''
        if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
            filename_dash = '-'
            
        if protein_matrix_flag:
            mod_endings = ['.ratio.' + filename_mod + x + '.tsv' for x in ['proteins', 'peptides', 'sites']]
        else:
            mod_endings = ['.ratio.' + filename_mod + x + '.tsv' for x in ['peptides', 'sites']]
        
        itraq4_channels = ['114', '115', '116', '117']
        itraq8_channels = ['113', '114', '115', '116', '117', '118', '119', '121']
        tmt10_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131']
        tmt11_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C']
        tmt16_channels = ['126', '127N', '127C', '128N', '128C', '129N', '129C', '130N', '130C', '131', '131C', '132N', '132C', '133N', '133C', '134N']
        itraq4 = ['X114', 'X115', 'X116', 'X117']
        itraq8 = ['X113', 'X114', 'X115', 'X116', 'X117', 'X118', 'X119', 'X121']
        tmt10 = ['X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131']
        tmt11 = ['X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'X131C']
        tmt16 = ['X126', 'X127N', 'X127C', 'X128N', 'X128C', 'X129N', 'X129C', 'X130N', 'X130C', 'X131', 'X131C', 'X132N', 'X132C', 'X133N', 'X133C', 'X134N']
        
        if label_type == '4plex':
            channels = itraq4_channels
            column_header_names = itraq4
        elif label_type == '8plex':
            channels = itraq8_channels
            column_header_names = itraq8
        elif label_type == '10plex':
            channels = tmt10_channels
            column_header_names = tmt10
        elif label_type == '11plex':
            channels = tmt11_channels
            column_header_names = tmt11
        elif label_type == '16plex':
            channels = tmt16_channels
            column_header_names = tmt16
        
        norm_index = channels.index(norm_channel)
        norm_ratios = 'X' + norm_channel
        
        itraq4_ratios = ['log2ratio_X114_' + norm_ratios, 'log2ratio_X115_' + norm_ratios, 'log2ratio_X116_' + norm_ratios, 'log2ratio_X117_' + norm_ratios]
        itraq8_ratios = ['log2ratio_X113_' + norm_ratios, 'log2ratio_X114_' + norm_ratios, 'log2ratio_X115_' + norm_ratios, 'log2ratio_X116_' + norm_ratios, 'log2ratio_X117_' + norm_ratios, 'log2ratio_X118_' + norm_ratios, 'log2ratio_X119_' + norm_ratios, 'log2ratio_X121_' + norm_ratios]
        tmt10_ratios = ['log2ratio_X126_' + norm_ratios, 'log2ratio_X127N_' + norm_ratios, 'log2ratio_X127C_' + norm_ratios, 'log2ratio_X128N_' + norm_ratios, 'log2ratio_X128C_' + norm_ratios, 'log2ratio_X129N_' + norm_ratios, 'log2ratio_X129C_' + norm_ratios, 'log2ratio_X130N_' + norm_ratios, 'log2ratio_X130C_' + norm_ratios, 'log2ratio_X131_' + norm_ratios]
        tmt11_ratios = ['log2ratio_X126_' + norm_ratios, 'log2ratio_X127N_' + norm_ratios, 'log2ratio_X127C_' + norm_ratios, 'log2ratio_X128N_' + norm_ratios, 'log2ratio_X128C_' + norm_ratios, 'log2ratio_X129N_' + norm_ratios, 'log2ratio_X129C_' + norm_ratios, 'log2ratio_X130N_' + norm_ratios, 'log2ratio_X130C_' + norm_ratios, 'log2ratio_X131_' + norm_ratios, 'log2ratio_X131C_' + norm_ratios]
        tmt16_ratios = ['log2ratio_X126_' + norm_ratios, 'log2ratio_X127N_' + norm_ratios, 'log2ratio_X127C_' + norm_ratios, 'log2ratio_X128N_' + norm_ratios, 'log2ratio_X128C_' + norm_ratios, 'log2ratio_X129N_' + norm_ratios, 'log2ratio_X129C_' + norm_ratios, 'log2ratio_X130N_' + norm_ratios, 'log2ratio_X130C_' + norm_ratios, 'log2ratio_X131_' + norm_ratios, 'log2ratio_X131C_' + norm_ratios, 'log2ratio_X132N_' + norm_ratios, 'log2ratio_X132C_' + norm_ratios, 'log2ratio_X133N_' + norm_ratios, 'log2ratio_X133C_' + norm_ratios, 'log2ratio_X134N_' + norm_ratios]
        
        if label_type == '4plex':
            ratio_header_names = itraq4_ratios
        elif label_type == '8plex':
            ratio_header_names = itraq8_ratios
        elif label_type == '10plex':
            ratio_header_names = tmt10_ratios
        elif label_type == '11plex':
            ratio_header_names = tmt11_ratios
        elif label_type == '16plex':
            ratio_header_names = tmt16_ratios
        
        data_path = itraq_path
        output_path = data_matrix_path + 'sets\\'
        output_path_virtual = data_matrix_path + 'virtual_reference\\sets\\'
        output_path_exists = os.path.exists(output_path)
        
        # creates the output path folders
        make_path(data_matrix_path)
        make_path(output_path)
        make_path(output_path_virtual)
        
        # handle single shot data
        data_files_all = sorted(glob.glob(data_path + '*.txt'))
        if sample_filenames == list():
            for data_f in data_files_all:
                data_f_basename = os.path.basename(data_f).replace('.txt', '')
                
                if data_f_basename not in sample_filenames:
                    sample_filenames.append(data_f_basename)
                    
            # removes non-unique names in case a set file was left over from previous analysis
            #remove_sample_names = set()
            #for sample_filename_1 in sample_filenames:
            #    for sample_filename_2 in sample_filenames:
            #        if sample_filename_1 != sample_filename_2:
            #            # would return -1 if sample_filename_1 was not found in sample_filename_2
            #            if sample_filename_2.find(sample_filename_1) != -1:
            #                remove_sample_names.add(sample_filename_1)
            #                break
            #
            #for remove_name in remove_sample_names:
            #    sample_filenames.remove(remove_name)
        
        run_samples = set()
        move_md5 = False
        # number of header lines in md5 checksum files
        md5_header = 1
        protein_md5_filename = 'step7_protein_md5_checksums.tsv'
        
        files_should_exist = [data_matrix_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios.tsv', data_matrix_path + filename_mod + filename_dash + 'site_matrix-log2_ratios.tsv', data_matrix_path + 'step7.config']
        if protein_matrix_flag:
            files_should_exist.append(data_matrix_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios.tsv')
        
        if not os.path.exists(itraq_path + protein_md5_filename) or output_path_exists == False:
            protein_md5(itraq_path, itraq_path, sample_filenames)
            
            for sample_filename in sample_filenames:
                run_samples.add(sample_filename)
        else:
            try:
                for filepath in files_should_exist:
                    if not os.path.exists(filepath):
                        for sample_filename in sample_filenames:
                            run_samples.add(sample_filename)
                        break
                
                for sample_filename in sample_filenames:
                    for mod_ending in mod_endings:
                        if not os.path.exists(output_path + sample_filename + mod_ending):
                            run_samples.add(sample_filename)
                
                f = open(itraq_path + protein_md5_filename, 'r')
                previous_md5_lines = f.readlines()
                f.close()
                
                protein_md5(itraq_path, output_path, sample_filenames)
                    
                f = open(output_path + protein_md5_filename, 'r')
                current_md5_lines = f.readlines()
                f.close()
                
                previous_md5 = {}
                for line in previous_md5_lines[md5_header:]:
                    line = line.strip().split('\t')
                    filename = line[0].strip()
                    checksum = line[1].strip()
                    
                    previous_md5[filename] = checksum
                    
                current_md5 = {}
                for line in current_md5_lines[md5_header:]:
                    line = line.strip().split('\t')
                    filename = line[0].strip()
                    checksum = line[1].strip()
                    
                    current_md5[filename] = checksum
                
                for key in current_md5:
                    run_sample = False
                    
                    if key not in previous_md5:
                        run_sample = True        
                    elif previous_md5[key] != current_md5[key]:
                        run_sample = True
                    
                    for sample_filename in sample_filenames:
                        if sample_filename in key:
                            if run_sample == True:
                                run_samples.add(sample_filename)
                            else:
                                for mod_ending in mod_endings:
                                    if not os.path.exists(output_path + sample_filename + mod_ending):
                                        run_samples.add(sample_filename)
                            break
                            
                for key in previous_md5:
                    run_sample = False
                    
                    if key not in current_md5:
                        run_sample = True
                    elif current_md5[key] != previous_md5[key]:
                        run_sample = True
                        
                    for sample_filename in sample_filenames:
                        if sample_filename in key:
                            if run_sample == True:
                                run_samples.add(sample_filename)
                            else:
                                for mod_ending in mod_endings:
                                    if not os.path.exists(output_path + sample_filename + mod_ending):
                                        run_samples.add(sample_filename)
                            break
        
                move_md5 = True
            except:
                if os.path.exists(output_path + protein_md5_filename):
                    os.remove(output_path + protein_md5_filename)
                    
                protein_md5(itraq_path, itraq_path, sample_filenames)
                
                for sample_filename in sample_filenames:
                    run_samples.add(sample_filename) 
        
        if run_samples != set():
            # dictionary of the protein names in the fasta file with the gi accession numbers as the keys
            fasta_protein_name_dict = fasta_protein_names(fasta_file)
            fasta_protein_seq_map = get_fasta_protein_seq_map(fasta_file)
            
            # pick only the sample filenames files that need to be run
            data_files = []
            for f in data_files_all:
                current_filename = str(os.path.basename(f).replace('.txt', ''))
                
                if current_filename in run_samples:
                    data_files.append(f)
        
            files_len = len(data_files)
    
            percent_interval = floor(100.0 / files_len)
            if percent_interval < 5.0:
                percent_interval = 5.0
        
            percent_checked = 0.0
            num_files_checked = 0
        
            print 'Reading peptide_protein_quantitation files progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
            sys.stdout.flush()
            for data_file in data_files:
                num_files_checked += 1
                
                f = open(data_file, 'r')
                data_lines = f.readlines()
                f.close()
                                
                num_lines = len(data_lines)
                                
                # generate the output matrices
                if num_lines > header_rows:
                    label_matrices(output_path, data_file, contams, norm_ratios, norm_index, label_type, filename_mod, peptide_protein_map, fasta_protein_name_dict, fasta_protein_seq_map, column_header_names, ratio_header_names, mod, protein_matrix_flag, localize_phosphosites, gene_name_identifier)
                    # generate matrices using the virtual reference
                    label_matrices_with_virtual_reference(output_path_virtual, data_file, contams, norm_ratios, norm_index, label_type, filename_mod, peptide_protein_map, fasta_protein_name_dict, fasta_protein_seq_map, column_header_names, ratio_header_names, mod, protein_matrix_flag, localize_phosphosites, gene_name_identifier)
                
                current_percent = ((100.0 * num_files_checked) / files_len)
                if current_percent >= (percent_checked + percent_interval):
                    percent_checked = floor(current_percent)
                
                    print 'Reading peptide_protein_quantitation files progress... ' + str(percent_checked) + ' %'
                    sys.stdout.flush()
                    
            if percent_checked < 100.0:
                print 'Reading peptide_protein_quantitation files progress... 100 %'
                sys.stdout.flush()
                    
            # generate combined expression matrices output
            # optional - provide header.txt file in raw data folder
            if data_files != []:
                p7a.main(configs, filename_mod)                
        
        if move_md5 == True:
            # move new search_md5_checksums.tsv from psm_quantitation folder to the search_results folder
            os.remove(itraq_path + protein_md5_filename)
            shutil.move(output_path + protein_md5_filename, itraq_path + protein_md5_filename)
            
        # generate the intensity expression matrices
        p7b.generate_expression_matrices(configs, itraq_path, filename_prefix, protein_matrix_flag, localize_phosphosites)
        
        write_config_params = []
        write_config_params.append('peptide_protein_quantitation_path = ' + itraq_path)
        write_config_params.append('normalization_channel = ' + norm_channel)
        write_config_params.append('label_type = ' + label_type)
        write_config_outfile = data_matrix_path + 'step7.config'
        
        write_mspy(write_config_params, write_config_outfile)   
        write_tp(config_path + 'sample_filenames.txt', write_config_outfile, 'sample_filenames.txt settings') 
    
# ******************************************************************************

def label_matrices_with_virtual_reference(output_path, data_file, contams, norm_ratios, norm_index, label_type, filename_mod, peptide_protein_map, fasta_protein_name_dict, fasta_protein_seq_map, column_header_names, ratio_header_names, mod, protein_matrix_flag, localize_phosphosites, gene_name_identifier): 
    phosphosite_map = {}
    phosphopeptide_map = {}
    if mod == 'phospho' and localize_phosphosites == 'true':
        phosphosite_map, phosphopeptide_map = get_phosphosite_and_phosphopeptide_maps(data_file, fasta_protein_seq_map)
        
    # 2 is the Protein.Group.Accessions column number
    protein_group_col = 2
    
    # protein matrices
    if protein_matrix_flag:
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
        
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Protein.Group.Accessions']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Sequence', numpy.median, ratio_header_names) \
            .transform_column(lambda x: x[0], 'Sequence') \
            .transform_column(lambda x: x[0], 'Protein.Group.Accessions') \
            .group_by('Protein.Group.Accessions', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_file = output_path + os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'proteins.tsv'
        dm.write_tsv(output_file)
        
        add_protein_names(output_file, fasta_protein_name_dict)
    
    # peptide matrices
    if (mod != 'phospho' or (mod == 'phospho' and localize_phosphosites == 'false')) and mod != 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
        
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Sequence']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Sequence', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'peptides.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        # add gene names to SPEG matrices
        if mod == 'glyco':
            add_gene_names_speg(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycopeptide', gene_name_identifier)
    elif mod == 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
        
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Sequence']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Sequence', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'peptides.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_gene_names_glyco(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycopeptide', gene_name_identifier)
    else:
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
        
        select_columns_first = ['Phosphopeptide.Index', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Phosphopeptide.Index']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Phosphopeptide.Index', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'peptides.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_peptide_sequence_phospho(output_file, phosphopeptide_map, 'phosphopeptide')
        add_gene_names_phospho(output_file, fasta_protein_name_dict, 'phosphopeptide', gene_name_identifier)
    
    # site matrices
    if (mod != 'phospho' or (mod == 'phospho' and localize_phosphosites == 'false')) and mod != 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
            
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
            
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Modifications']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Modifications', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'sites.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        # add gene names to SPEG matrices
        if mod == 'glyco':
            add_gene_names_speg(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycosite', gene_name_identifier)
    elif mod == 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
            
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
            
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Modifications']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Modifications', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'sites.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_gene_names_glyco(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycosite', gene_name_identifier)
    else:
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
            
        for column_header_name in column_header_names:
            dm.log2_ratio_with_virtual_reference([column_header_name], norm_ratios, column_header_names)
            
        select_columns_first = ['Phosphosite.Index', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Phosphosite.Index']
        select_columns_second.extend(ratio_header_names)
        
        # use this filter to remove unlocalized phosphosites
        # .filter_rows(lambda x: x.split('_')[-1].strip() != '0', 'Phosphosite.Index') \
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Phosphosite.Index', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'sites.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_peptide_sequence_phospho(output_file, phosphosite_map, 'phosphosite')
        add_gene_names_phospho(output_file, fasta_protein_name_dict, 'phosphosite', gene_name_identifier) 

def label_matrices(output_path, data_file, contams, norm_ratios, norm_index, label_type, filename_mod, peptide_protein_map, fasta_protein_name_dict, fasta_protein_seq_map, column_header_names, ratio_header_names, mod, protein_matrix_flag, localize_phosphosites, gene_name_identifier): 
    phosphosite_map = {}
    phosphopeptide_map = {}
    if mod == 'phospho' and localize_phosphosites == 'true':
        phosphosite_map, phosphopeptide_map = get_phosphosite_and_phosphopeptide_maps(data_file, fasta_protein_seq_map)
        
    # 2 is the Protein.Group.Accessions column number
    protein_group_col = 2
    
    # protein matrices
    if protein_matrix_flag:
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
        
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Protein.Group.Accessions']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Sequence', numpy.median, ratio_header_names) \
            .transform_column(lambda x: x[0], 'Sequence') \
            .transform_column(lambda x: x[0], 'Protein.Group.Accessions') \
            .group_by('Protein.Group.Accessions', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_file = output_path + os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'proteins.tsv'
        dm.write_tsv(output_file)
        
        add_protein_names(output_file, fasta_protein_name_dict)
    
    # peptide matrices
    if (mod != 'phospho' or (mod == 'phospho' and localize_phosphosites == 'false')) and mod != 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
        
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Sequence']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Sequence', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'peptides.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        # add gene names to SPEG matrices
        if mod == 'glyco':
            add_gene_names_speg(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycopeptide', gene_name_identifier)
    elif mod == 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
        
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Sequence']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Sequence', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'peptides.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_gene_names_glyco(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycopeptide', gene_name_identifier)
    else:
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
        
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
        
        select_columns_first = ['Phosphopeptide.Index', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Phosphopeptide.Index']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Phosphopeptide.Index', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'peptides.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_peptide_sequence_phospho(output_file, phosphopeptide_map, 'phosphopeptide')
        add_gene_names_phospho(output_file, fasta_protein_name_dict, 'phosphopeptide', gene_name_identifier)
    
    # site matrices
    if (mod != 'phospho' or (mod == 'phospho' and localize_phosphosites == 'false')) and mod != 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
            
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
            
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Modifications']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Modifications', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'sites.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        # add gene names to SPEG matrices
        if mod == 'glyco':
            add_gene_names_speg(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycosite', gene_name_identifier)
    elif mod == 'intact':
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
            
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
            
        select_columns_first = ['Sequence', 'Modifications', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Modifications']
        select_columns_second.extend(ratio_header_names)
        
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Modifications', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'sites.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_gene_names_glyco(output_file, fasta_protein_name_dict, peptide_protein_map, 'glycosite', gene_name_identifier)
    else:
        dm = DataMatrix.data_matrix_from_tsv_remove_contams(data_file, protein_group_col, contams, header = True)
        dm.quantitative_columns_to_float(column_header_names)
        itraq_medians = dm.get_statistics_omit_zeroes(column_header_names, 
                                        numpy.median)
                                        
        norm_factors = get_norm_factors(itraq_medians, norm_index, label_type)
        
        dm.normalize_quantitative_columns(column_header_names, norm_factors)
            
        for column_header_name in column_header_names:
            dm.log2_ratio([column_header_name], norm_ratios)
            
        select_columns_first = ['Phosphosite.Index', 'Protein.Group.Accessions']
        select_columns_first.extend(ratio_header_names)
        select_columns_second = ['Phosphosite.Index']
        select_columns_second.extend(ratio_header_names)
        
        # use this filter to remove unlocalized phosphosites
        # .filter_rows(lambda x: x.split('_')[-1].strip() != '0', 'Phosphosite.Index') \
        dm.filter_rows(lambda x: x != 'Not_Found', 'Protein.Group.Accessions') \
            .select_columns(select_columns_first) \
            .group_by('Phosphosite.Index', numpy.median, ratio_header_names) \
            .select_columns(select_columns_second) 
        output_name = os.path.basename(data_file).replace('.txt', '') + '.ratio.' + filename_mod + 'sites.tsv'
        output_file = output_path + output_name
        dm.write_tsv(output_file)
        
        add_peptide_sequence_phospho(output_file, phosphosite_map, 'phosphosite')
        add_gene_names_phospho(output_file, fasta_protein_name_dict, 'phosphosite', gene_name_identifier)    

def add_peptide_sequence_phospho(filepath, phospho_map, matrix_type):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # phosphosite matrix columns
    mer_seq_col = 0
    mod_pep_col = 1
    
    updated_lines = []

    for line in lines:
        # add in name header 
        if line == lines[0]:
            line = line.strip().split('\t')
            
            if matrix_type == 'phosphosite':
                line.insert(1, 'Flanking.Sequence.Phosphosites.6mer')
                line.insert(1, 'Modifications')
            elif matrix_type == 'phosphopeptide':
                line.insert(1, 'Sequence')
                
            updated_lines.append(line)
            
            continue
        
        line = line.strip().split('\t')
        gi_list = str(line[0]).strip().split(';')
        
        if matrix_type == 'phosphopeptide':
            seqs = ''
            for gi in gi_list:
                try:
                    seq = str(phospho_map[gi]).strip()
                    
                    if seqs == '':
                        seqs += seq
                    else:
                        seqs += ';' + seq
                except:
                    #print 'gi accession, ' + str(gi) + ', is not a key in the phospho map.'
                    #sys.stdout.flush()
                    
                    continue
            
            if seqs == '':
                #print 'peptide sequences for the following gi accession group, ' + str(gi_list) + ', were not found.'
                #sys.stdout.flush()
                
                seqs = 'None'
                
            line.insert(1, seqs)
            updated_lines.append(line)
        elif matrix_type == 'phosphosite':
            seqs = ''
            mer_seqs = ''
            for gi in gi_list:
                try:
                    map_data = phospho_map[gi]
                    seq = str(map_data[mod_pep_col].strip())
                    mer_seq = str(map_data[mer_seq_col].strip())
                    
                    if seqs == '':
                        seqs += seq
                    else:
                        seqs += ';' + seq
                        
                    if mer_seqs == '':
                        mer_seqs += mer_seq
                    else:
                        mer_seqs += ';' + mer_seq
                except:
                    #print 'gi accession, ' + str(gi) + ', is not a key in the phospho map.'
                    #sys.stdout.flush()
                    
                    continue
            
            if seqs == '':
                #print 'peptide sequences for the following gi accession group, ' + str(gi_list) + ', were not found.'
                #sys.stdout.flush()
                
                seqs = 'None'
            
            if mer_seqs == '':
                #print 'peptide sequences for the following gi accession group, ' + str(gi_list) + ', were not found.'
                #sys.stdout.flush()
                
                mer_seqs = 'None'
            
            line.insert(1, mer_seqs)    
            line.insert(1, seqs)
            updated_lines.append(line)
        
    # rewrite output file
    f = open(filepath, 'w')
    
    for line in updated_lines:
        output_line = ''
        
        for col in line:
            output_line += str(col) + '\t'
            
        f.write(output_line.strip() + '\n')
    
    f.close()
    
def get_phosphosite_and_phosphopeptide_maps(data_file, fasta_map):
    f = open(data_file, 'r')
    lines = f.readlines()
    f.close()
    
    pep_col = 0
    mod_pep_col = 1
    phosphosite_index_col = -6
    phosphopeptide_index_col = -5
    phosphopeptides_num_underscores = -2
    
    # number of amino acids on either end of phosphosites to retreive from the protein peptide sequence
    mer = 6

    header = 1

    phosphosite_map = {}
    phosphopeptide_map = {}
    for line in lines[header:]:
        line = line.strip().split('\t')
        pep = line[pep_col].strip()
        mod_pep = line[mod_pep_col].strip()
        phosphosite = line[phosphosite_index_col].strip()
        phosphopeptide = line[phosphopeptide_index_col].strip()
        protein = '_'.join(phosphopeptide.split('_')[ : phosphopeptides_num_underscores])
        protein_seq = fasta_map[protein]
        protein_seq_len = len(protein_seq)
        
        #phosphosite_split = phosphosite.split('_')
        #if phosphosite_split[-1].strip() == '0':
        #    # the indices refer to how many underscores come after the protein ID
        #    # -1 is to convert from 1-based indices to 0-based indices
        #    first_site_protein_position = int(phosphosite_split[-4]) - 1
        #    last_site_protein_position = int(phosphosite_split[-3]) - 1
        #else:
        #    # the indices refer to how many underscores come after the protein ID
        #    # -1 is to convert from 1-based indices to 0-based indices
        #    first_site_protein_position = int(phosphosite_split[-5]) - 1
        #    last_site_protein_position = int(phosphosite_split[-4]) - 1
        
        seq = ''
        phosphosites = phosphosite.split('_')[-1].strip()
        if phosphosites == '0':
            seq = pep
        else:
            # retrieve the phosphosite protein positions from the phosphosite index
            phosphosite_positions = []
            phosphosite_position = ''
            for char in phosphosites:
                if char.isalpha():
                    if phosphosite_position != '':
                        phosphosite_positions.append(int(phosphosite_position))
                        
                    phosphosite_position = ''
                elif char.isdigit():
                    phosphosite_position += char
                    
            if phosphosite_position != '':
                phosphosite_positions.append(int(phosphosite_position))
                
            # -1 is to convert from 1-based indices to 0-based indices
            first_site_protein_position = min(phosphosite_positions) - 1
            last_site_protein_position = max(phosphosite_positions) - 1   
            
            # adjust the first amino acid position by mer
            if first_site_protein_position <= mer:
                first_site_protein_position = 0
            else:
                first_site_protein_position -= mer
                
            # adjust the last amino acid position by mer
            if last_site_protein_position >= (protein_seq_len - mer - 1):
                last_site_protein_position = protein_seq_len
            else:
                last_site_protein_position += mer + 1
                
            seq_6mer = protein_seq[first_site_protein_position : last_site_protein_position]
            
            # reformats the 6-mer sequence to change phosphosites to lower case
            for aa_index, aa in enumerate(seq_6mer):
                adjusted_index = aa_index + first_site_protein_position + 1
                if adjusted_index in phosphosite_positions:
                    seq += aa.lower()
                else:
                    seq += aa
        
        if phosphosite not in phosphosite_map:
            phosphosite_map[phosphosite] = [seq, mod_pep, pep]
        elif len(pep) < len(phosphosite_map[phosphosite][-1]):
            phosphosite_map[phosphosite] = [seq, mod_pep, pep]
            
        if phosphopeptide not in phosphopeptide_map:
            phosphopeptide_map[phosphopeptide] = pep
        elif len(pep) < len(phosphopeptide_map[phosphopeptide]):
            phosphopeptide_map[phosphopeptide] = pep
            
    return phosphosite_map, phosphopeptide_map

def get_fasta_protein_seq_map(fasta_file):
    # create map of proteins to peptide sequences from the fasta file in order to get site positions in the protein sequence
    f = open(fasta_file, 'r')
    fasta_lines = f.readlines()
    f.close()
        
    fasta_map = {}
    fasta_protein = ''
    fasta_seq = ''
    for line in fasta_lines:
        line = line.strip()
        
        if line[0] == '>':
            if fasta_protein != '' and fasta_seq != '':
                fasta_map[fasta_protein] = fasta_seq
                
            fasta_protein = line.split('>')[1].strip().split(' ')[0].strip()
            fasta_seq = ''
        else:
            fasta_seq += line
            
    if fasta_protein != '' and fasta_seq != '':
        fasta_map[fasta_protein] = fasta_seq
        
    return fasta_map

def get_peptide_protein_map(peptide_protein_file):
    # get peptide to protein map from the protein inference peptide_protein.txt file
    f = open(peptide_protein_file, 'r')
    lines = f.readlines()
    f.close()
    
    pep_col = 0
    protein_col = 1
    
    peptide_protein_map = {}
    for line in lines:
        line = line.strip().split('\t')
        pep = line[pep_col].strip()
        protein = line[protein_col].strip()
        
        if pep  in peptide_protein_map:
            raise Exception(pep + ' is already in peptide_protein_map')
            
        peptide_protein_map[pep] = protein
    
    return peptide_protein_map

def add_gene_names_speg(filepath, fasta_protein_name_dict, peptide_protein_map, matrix_type, gene_name_identifier):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    glycopeptide_col = 0
    
    updated_lines = []

    for line in lines:
        # add in gene name header 
        if line == lines[0]:
            line = line.strip().split('\t')
            
            line.insert(1, 'Gene')
            updated_lines.append(line)
            
            continue
        
        line = line.strip().split('\t')
        mod_pep = line[glycopeptide_col].strip()
        
        seq = ''
        if matrix_type == 'glycopeptide':
            seq = mod_pep
        elif matrix_type == 'glycosite':
            # removes modifications from the modified peptide sequences of the glycosite files
            valid_aa_section = True
            for char in mod_pep:
                if valid_aa_section:
                    if char == '[':
                        valid_aa_section = False
                        continue
                    else:
                        if char == char.upper():
                            # update the peptide sequence
                            seq += char
                elif not valid_aa_section:
                    if char == ']':
                        valid_aa_section = True
                        continue
        
        gi_list = peptide_protein_map[seq].split(';')
        
        gene_names = ''
        genes_found = set([])
        for gi in gi_list:
            try:
                gene_name = str(fasta_protein_name_dict[gi]).split(gene_name_identifier)[1].strip().split(' ')[0].strip()
                
                found_flag = False
                if gene_name != '':
                    if gene_name in genes_found:
                        found_flag = True
                        
                    genes_found.add(gene_name)
                
                if not found_flag:
                    if gene_names == '':
                        gene_names += gene_name
                    else:
                        gene_names += ';' + gene_name
            except:
                #print 'gi accession, ' + str(gi) + ', is not a key in the fasta gi to protein name dictionary.'
                #sys.stdout.flush()
                
                continue
        
        if gene_names == '':
            #print 'gene names for the following gi accession group, ' + str(gi_list) + ', were not found.'
            #sys.stdout.flush()
            
            gene_names = '-'
        
        line.insert(1, gene_names)
        updated_lines.append(line)
        
    # rewrite output file
    f = open(filepath, 'w')
    
    for line in updated_lines:
        output_line = ''
        
        for col in line:
            output_line += str(col) + '\t'
            
        f.write(output_line.strip() + '\n')
    
    f.close()

def add_gene_names_glyco(filepath, fasta_protein_name_dict, peptide_protein_map, matrix_type, gene_name_identifier):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    glycopeptide_col = 0
    
    updated_lines = []

    for line in lines:
        # add in gene name header 
        if line == lines[0]:
            line = line.strip().split('\t')
            
            line.insert(1, 'Gene')
            updated_lines.append(line)
            
            continue
        
        line = line.strip().split('\t')
        mod_pep = line[glycopeptide_col].strip().split('-')[0].strip()
        
        seq = ''
        if matrix_type == 'glycopeptide':
            seq = mod_pep
        elif matrix_type == 'glycosite':
            # removes modifications from the modified peptide sequences of the glycosite files
            valid_aa_section = True
            for char in mod_pep:
                if valid_aa_section:
                    if char == '[':
                        valid_aa_section = False
                        continue
                    else:
                        if char == char.upper():
                            # update the peptide sequence
                            seq += char
                elif not valid_aa_section:
                    if char == ']':
                        valid_aa_section = True
                        continue
        
        gi_list = peptide_protein_map[seq].split(';')
        
        gene_names = ''
        genes_found = set([])
        for gi in gi_list:
            try:
                gene_name = str(fasta_protein_name_dict[gi]).split(gene_name_identifier)[1].strip().split(' ')[0].strip()
                
                found_flag = False
                if gene_name != '':
                    if gene_name in genes_found:
                        found_flag = True
                        
                    genes_found.add(gene_name)
                
                if not found_flag:
                    if gene_names == '':
                        gene_names += gene_name
                    else:
                        gene_names += ';' + gene_name
            except:
                #print 'gi accession, ' + str(gi) + ', is not a key in the fasta gi to protein name dictionary.'
                #sys.stdout.flush()
                
                continue
        
        if gene_names == '':
            #print 'gene names for the following gi accession group, ' + str(gi_list) + ', were not found.'
            #sys.stdout.flush()
            
            gene_names = '-'
        
        line.insert(1, gene_names)
        updated_lines.append(line)
        
    # rewrite output file
    f = open(filepath, 'w')
    
    for line in updated_lines:
        output_line = ''
        
        for col in line:
            output_line += str(col) + '\t'
            
        f.write(output_line.strip() + '\n')
    
    f.close()

def add_gene_names_phospho(filepath, fasta_protein_name_dict, matrix_type, gene_name_identifier):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    phosphosites_num_underscores_no_localization = -4
    phosphosites_num_underscores_localization = -5
    phosphopeptides_num_underscores = -2
    
    updated_lines = []

    for line in lines:
        # add in gene name header 
        if line == lines[0]:
            line = line.strip().split('\t')
            
            line.insert(1, 'Gene')
            updated_lines.append(line)
            
            continue
        
        line = line.strip().split('\t')
        gi_list = str(line[0]).strip().split(';')
        for gi_index, gi in enumerate(gi_list):
            gi_split = gi.strip().split('_')
            
            if matrix_type == 'phosphosite':
                if gi_split[-1] == '0':
                    gi_list[gi_index] = '_'.join(gi_split[ : phosphosites_num_underscores_no_localization])
                else:
                    gi_list[gi_index] = '_'.join(gi_split[ : phosphosites_num_underscores_localization])
            elif matrix_type == 'phosphopeptide':
                gi_list[gi_index] = '_'.join(gi_split[ : phosphopeptides_num_underscores])
        
        gene_names = ''
        for gi in gi_list:
            try:
                gene_name = str(fasta_protein_name_dict[gi]).split(gene_name_identifier)[1].strip().split(' ')[0].strip()
                
                if gene_names == '':
                    gene_names += gene_name
                else:
                    gene_names += ';' + gene_name
            except:
                #print 'gi accession, ' + str(gi) + ', is not a key in the fasta gi to protein name dictionary.'
                #sys.stdout.flush()
                
                continue
        
        if gene_names == '':
            #print 'gene names for the following gi accession group, ' + str(gi_list) + ', were not found.'
            #sys.stdout.flush()
            
            gene_names = '-'
            
        line.insert(1, gene_names)
        updated_lines.append(line)
        
    # rewrite output file
    f = open(filepath, 'w')
    
    for line in updated_lines:
        output_line = ''
        
        for col in line:
            output_line += str(col) + '\t'
            
        f.write(output_line.strip() + '\n')
    
    f.close()

def add_protein_names(filepath, fasta_protein_name_dict):
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    updated_lines = []

    for line in lines:
        # add in protein name header 
        if line == lines[0]:
            line = line.strip().split('\t')
            
            line.insert(1, 'Protein.Group.Names')
            updated_lines.append(line)
            
            continue
        
        line = line.strip().split('\t')
        gi_list = str(line[0]).strip().split(';')
        
        protein_names = ''
        for gi in gi_list:
            try:
                protein_name = str(fasta_protein_name_dict[gi])
                
                if protein_names == '':
                    protein_names += protein_name
                else:
                    protein_names += ';' + protein_name
            except:
                #print 'gi accession, ' + str(gi) + ', is not a key in the fasta gi to protein name dictionary.'
                #sys.stdout.flush()
                
                continue
        
        if protein_names == '':
            #print 'protein names for the following gi accession group, ' + str(gi_list) + ', were not found.'
            #sys.stdout.flush()
            
            protein_names = 'None'
            
        line.insert(1, protein_names)
        updated_lines.append(line)
        
    # rewrite output file
    f = open(filepath, 'w')
    
    for line in updated_lines:
        output_line = ''
        
        for col in line:
            output_line += str(col) + '\t'
            
        f.write(output_line.strip() + '\n')
    
    f.close()

def get_fasta_contams(contams_lines):
    contams_data = set()
    for line in contams_lines:
        line = line.strip()
        
        if line[0] == '>':
            protein_id = line.split('>')[1].strip().split(' ')[0].strip()
            contams_data.add(protein_id)
            
    return contams_data

def get_norm_factors(itraq_medians, norm_index, label_type):
    if label_type == '4plex':
        norm_factors = [itraq_medians[norm_index] / itraq_medians[x] if itraq_medians[x] != None and itraq_medians[norm_index] != None else None for x in range(4)]
    elif label_type == '8plex':
        norm_factors = [itraq_medians[norm_index] / itraq_medians[x] if itraq_medians[x] != None and itraq_medians[norm_index] != None else None for x in range(8)]
    elif label_type == '10plex':
        norm_factors = [itraq_medians[norm_index] / itraq_medians[x] if itraq_medians[x] != None and itraq_medians[norm_index] != None else None for x in range(10)]
    elif label_type == '11plex':
        norm_factors = [itraq_medians[norm_index] / itraq_medians[x] if itraq_medians[x] != None and itraq_medians[norm_index] != None else None for x in range(11)]
    elif label_type == '16plex': 
        norm_factors = [itraq_medians[norm_index] / itraq_medians[x] if itraq_medians[x] != None and itraq_medians[norm_index] != None else None for x in range(16)]                   
    
    return norm_factors

# ******************************************************************************
               
if __name__ == '__main__':
    # header_expression_matrix data
    f = open(config_path + 'header_expression_matrix.tsv', 'r')
    header_expression_matrix_data = f.readlines()
    f.close()
    
    # contaminants.fasta data
    f = open(config_path + 'contaminants.fasta', 'r')
    contams_lines = f.readlines()
    f.close()
    
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['header_expression_matrix'] = header_expression_matrix_data
    configs['contams_lines'] = contams_lines
    configs['backup'] = True
    
    #main(configs)