import os
import sys
import glob
import time
import shutil
import subprocess
import tkMessageBox
from Tkinter import *
from hotpot.pathing import make_path
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'

def main(configs):
    print '\nStep 3 continued:\tConverting mzID database search results to pepXML format...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    root = Tk().withdraw()
    
    options = configs['options']
    exes = configs['exes']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    pepxml_path = data_root + 'pepxml\\'
    search_results_path = data_root + 'step3-search_results\\'
    mzid_path = search_results_path
    idconvert_file = exes['msconvert'].replace('msconvert.exe', 'idconvert.exe')
        
    # creates the output path folders
    make_path(pepxml_path)
    
    mzid_files = glob.glob(mzid_path + '*.mzid')
    
    if mzid_files == []:
        tkMessageBox.showinfo('Error!', 'Please move the mzid files to: \n' + mzid_path)
    else:
        tkMessageBox.showinfo('Note!', 'Result pepxml files will be placed in: \n' + pepxml_path + '\n-\nTo use the pepxml files for Steps 4 - 7, please put only the pepxml files in: \n' + search_results_path)
        
        for mzid_file in mzid_files:
            filename = str(mzid_file).split('\\')[-1]
            filename = filename[:-5] + '.pepxml'
            if not os.path.exists(pepxml_path + filename):
                subprocess.call(idconvert_file + ' ' + mzid_file + ' --pepXML -o ' + pepxml_path)
      
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['backup'] = True
    
    main(configs)