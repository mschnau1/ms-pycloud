import os
import sys
import glob
import shutil
import numpy as np
import time as pytime
#from pylab import frange
from pyteomics import mzml
from hotpot.spectrum import Spectrum
from multiprocessing import Pool
from multiprocessing import cpu_count
from hotpot.read_write import read_lines as rl
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'

def main(configs):
    print '\nStep 3 continued: Reading charge states and injection times from mzML files only...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(pytime.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    sample_filenames = sorted(configs['sample filenames'])
    
    data_root = options['raw_data']
    data_set_name = data_root.split('\\')[-2]
    mzml_path = data_root + 'step1-mzml\\'
    mzid_path = data_root + 'step3-search_results\\'
    num_thread = int(cpu_count()) - 1
    
    # sets the step for the injection time grouping
    injection_time_step = 10
    
    if num_thread == 0:
        num_thread = 1
    
    # handle single shot data
    mzml_files = glob.glob(mzml_path + '*.mzML')
    if sample_filenames == list():
        for mzml_f in mzml_files:
            mzml_f_basename = os.path.basename(mzml_f).replace('.mzML', '')
            
            if mzml_f_basename not in sample_filenames:
                sample_filenames.append(mzml_f_basename)
    
    data_roots = [data_root] * len(mzml_files)
    
    pool = Pool(processes = num_thread)
    pool.map(charge_injection_time_in_parallel, zip(mzml_files, data_roots))   
    
    # combine search quameter tsv results into one tsv file
    tsv_files = sorted(glob.glob(mzid_path + '*.qual.mspycloud.tsv'))
    
    if tsv_files != []:
        longest_charge_list = []
        longest_injection_list = []
        max_injection_time = -1
        longest_header = ''
        for tsv_file in tsv_files:
            current_filename = str(os.path.basename(tsv_file).replace('.qual.mspycloud.tsv', ''))
            
            for sample_filename in sample_filenames:
                if sample_filename in current_filename:                                       
                    f = open(tsv_file, 'r')
                    lines = f.readlines()
                    f.close()
                    
                    header = lines[0]
                    header_list = header.strip().split('\t')
                    
                    charge_list = []
                    injection_list = []
                    #injection_ratio_list = []
                    for col in header_list:
                        if 'charge #' in col:
                            charge_list.append(col)
                        elif 'injection time # (mzML)' in col:
                            injection_list.append(col)
                    
                    for charge_col in charge_list:
                        if charge_col not in longest_charge_list:
                            longest_charge_list.append(charge_col)
                        
                    for injection_col in injection_list:
                        # checks to see if the injection time header name is the final injection time value
                        if '-' not in injection_col:
                            injection_time = float(injection_col.split('injection time # (mzML)')[0].strip())
                        
                            if injection_time > max_injection_time:
                                max_injection_time = injection_time
                    break
                
        for time in np.arange(0, max_injection_time + injection_time_step, injection_time_step):
            if time < (max_injection_time - injection_time_step):
                time_key = str(time) + '-' + str(time + injection_time_step) + ' injection time # (mzML)'
                if time_key not in longest_injection_list:
                    longest_injection_list.append(time_key)
            elif time < max_injection_time:
                time_key = str(time) + '-' + str(max_injection_time) + ' injection time # (mzML)'
                if time_key not in longest_injection_list:
                    longest_injection_list.append(time_key)
                    
                time_key = str(max_injection_time) + ' injection time # (mzML)'
                if time_key not in longest_injection_list:
                    longest_injection_list.append(time_key)
        
        for i, charge_col in enumerate(longest_charge_list):
            longest_charge_list[i] = str(charge_col).strip()
            
        for i, injection_col in enumerate(longest_injection_list):
            longest_injection_list[i] = str(injection_col).strip()
        
        longest_header_part = sorted(sorted(longest_charge_list), key = len) + longest_injection_list
        
        f = open(tsv_files[0], 'r')
        lines = f.readlines()
        f.close()
        
        longest_header_list = []
        longest_header_part_flag = False
        
        header = lines[0]
        header_list = header.strip().split('\t')
        
        for col in header_list:
            col = str(col).strip()
            
            if col not in longest_header_part:
                longest_header_list.append(col)
            elif not longest_header_part_flag:
                longest_header_list += longest_header_part  
                longest_header_part_flag = True
                
        if not longest_header_part_flag:
            longest_header_list += longest_header_part  
            longest_header_part_flag = True
        
        longest_header = '\t'.join(longest_header_list).strip()
        
        output_data = [longest_header]
        for tsv_file in tsv_files:
            current_filename = str(os.path.basename(tsv_file).replace('.qual.mspycloud.tsv', ''))
            
            for sample_filename in sample_filenames:
                if sample_filename in current_filename:
                    f = open(tsv_file, 'r')
                    lines = f.readlines()
                    f.close()
                    
                    header_line = lines[0]
                    header_list = header_line.strip().split('\t')
                    current_val_line = lines[1]
                    val_list_initial = current_val_line.strip().split('\t')
                    
                    vals_dict = {}
                    for i, col in enumerate(header_list):
                        col = str(col).strip()
                        
                        vals_dict[col] = val_list_initial[i]
                    
                    val_list = [0] * len(longest_header_list)   
                    for val_header in vals_dict:
                        # necessary check because injection time bins are not necessarily the same for each file
                        if val_header in longest_header_list:
                            header_index = longest_header_list.index(val_header)
                            val_list[header_index] = vals_dict[val_header]
                        else:
                            if 'injection time # (mzML)' in val_header:
                                header_key = 'injection time # (mzML)'
                                
                                if '-' not in val_header:
                                    val_time = float(val_header.split(header_key)[0].strip())
                                else:
                                    val_time = float(val_header.split(header_key)[0].strip().split('-')[-1].strip())
                                
                                for y, header_name in enumerate(longest_header_list):
                                    if header_key in header_name and '-' in header_name:
                                        header_name = header_name.split(header_key)[0].strip()
                                        times = header_name.split('-')
                                        
                                        beg_time = float(times[0].strip())
                                        end_time = float(times[-1].strip())
                                        
                                        if val_time >= beg_time and val_time < end_time:
                                            val_list[y] = int(val_list[y]) + int(vals_dict[val_header])
                                            val_list[y] = str(val_list[y])
                                            break
                            else:
                                print 'Unexpected issue: Column missing from header was not an injection time bin column!'               
                    
                    val_line = ''
                    for val in val_list:
                        val = str(val).strip()
                        val_line += val + '\t'
                    val_line += '\n'
                    
                    output_data.append(val_line)
                    break
            
        f = open(mzid_path + data_set_name + '_search_idfree_quameter_results.tsv', 'w')
        for row in output_data:
            f.write(row.strip() + '\n')
        
        f.close()       

# ******************************************************************************
                         
def charge_injection_time_in_parallel((mzml_file, data_root)):
    """
    Description: Makes multiple calls to quameter
    
    Args:
        - raw_file: one of the raw data files
        - qual_tsv_file: qual_tsv_file output path
    """
    mzid_path = data_root + 'step3-search_results\\'
    
    # charge and injection time data that will be added to the quameter results file
    data = {}
    # sets the step for the injection time grouping
    injection_time_step = 10
    # number of decimal places to round injection time values
    injection_time_precision = 9
    
    raw_filename = os.path.basename(mzml_file).replace('.mzML', '')
    
    # if the particular charge and injection time quameter result file does not already exist, then it is created
    if not os.path.exists(mzid_path + raw_filename + '.qual.mspycloud.tsv') or not os.path.exists(mzid_path + raw_filename + '.injection.times.tsv'):
        with mzml.read(mzml_file) as reader:
            print 'Reading ' + mzml_file
            sys.stdout.flush()
            
            data[raw_filename] = {}
            data[raw_filename]['charges'] = {}
            data[raw_filename]['charges']['unassigned'] = 0
            data[raw_filename]['charges']['assigned'] = 0
            data[raw_filename]['injection times'] = {}
            data[raw_filename]['injection times']['assigned'] = {}
            data[raw_filename]['injection times']['assigned']['count'] = 0
            data[raw_filename]['injection times']['assigned']['all'] = []
            data[raw_filename]['injection times']['unassigned'] = {}
            data[raw_filename]['injection times']['unassigned']['count'] = 0
            
            for entry in reader:
                spec = Spectrum(entry)
                ms_level = int(spec.ms_level)
                
                if ms_level == 2: 
                    try:
                        injection_time = np.round(float(spec.scan_list['scan'][0]['ion injection time']), injection_time_precision)  
                        
                        data[raw_filename]['injection times']['assigned']['all'].append(injection_time)
                        data[raw_filename]['injection times']['assigned']['count'] += 1
                    except ValueError:
                        data[raw_filename]['injection times']['unassigned']['count'] += 1
                        #print 'Invalid injection time, not a floating point number! - scan (' + raw_filename + ') = ' + str(scan)        
                    
                    if spec.precursor_list != None:
                        precursor_list = spec.precursor_list['precursor'][0]
                        selected_ion = precursor_list['selectedIonList']['selectedIon'][0]
                        
                        if 'charge state' in selected_ion:
                            charge = int(selected_ion['charge state'])
                            
                            if charge in data[raw_filename]['charges']:
                                data[raw_filename]['charges'][charge] += 1
                                data[raw_filename]['charges']['assigned'] += 1
                            else:
                                data[raw_filename]['charges'][charge] = 1
                                data[raw_filename]['charges']['assigned'] += 1
                        else:
                            data[raw_filename]['charges']['unassigned'] += 1
                    else:
                        data[raw_filename]['charges']['unassigned'] += 1
    
        # picks out the maximum injection time for splitting up the injection times into 10 second intervals
        max_injection_time = max(data[raw_filename]['injection times']['assigned']['all'])
            
        all_charges = data[raw_filename]['charges'].keys()
        all_charges.remove('unassigned')
        all_charges.remove('assigned')
        
        min_charge = min(all_charges)
        max_charge = max(all_charges)
        
        time_keys = []
        charge_keys = []
    
        for time in np.arange(0, max_injection_time + injection_time_step, injection_time_step):
            if time < (max_injection_time - injection_time_step):
                time_key = str(time) + '-' + str(time + injection_time_step)
                time_keys.append(time_key)
                
                data[raw_filename]['injection times'][time_key] = 0
            elif time < max_injection_time:
                time_key = str(time) + '-' + str(max_injection_time)
                time_keys.append(time_key)
                time_keys.append(str(max_injection_time))
                    
                data[raw_filename]['injection times'][time_key] = 0
                data[raw_filename]['injection times'][str(max_injection_time)] = 0          
        
        injection_times = data[raw_filename]['injection times']['assigned']['all']
        for injection_time in injection_times:
            for time_key in time_keys:
                if '-' in time_key:
                    time_key_selection = time_key.strip().split('-')
                    beg_time = float(time_key_selection[0].strip())
                    end_time = float(time_key_selection[-1].strip())
                    
                    if injection_time >= beg_time and injection_time < end_time:
                        data[raw_filename]['injection times'][time_key] += 1
                        break
                else:
                    time_key_selection = float(time_key.strip())
                    if injection_time == time_key_selection:
                        data[raw_filename]['injection times'][time_key] += 1
                        break
                                
        for key in range(min_charge, max_charge + 1):
            charge_keys.append(key)
            
            if key in data[raw_filename]['charges']:
                #print 'Charge ' + str(key) + ' = ' + str(data[raw_filename]['charges'][key]) 
                continue
            else:
                data[raw_filename]['charges'][key] = 0
                #print 'Charge ' + str(key) + ' = ' + str(data[raw_filename]['charges'][key])
        
        #print 'Assigned charges = ' + str(data[raw_filename]['charges']['assigned'])
        #print 'Unassigned charges = ' + str(data[raw_filename]['charges']['unassigned'])
        
        # prints injection time results
        for key in time_keys:
            #print 'Injection time ' + str(key) + ' = ' + str(data[raw_filename]['injection times'][key])
            continue
        
        reverse_time_keys = list(time_keys)
        reverse_time_keys.reverse()
        reverse_charge_keys = list(charge_keys)
        reverse_charge_keys.reverse()
        
        idfree_file = data_root + raw_filename + '.qual.tsv'
        
        if os.path.isfile(idfree_file):
            f = open(idfree_file, 'r')
            idfree_lines = f.readlines()
            f.close()
        else:
            # handles missing quameter results file
            idfree_lines = ['Filename', raw_filename + '.raw']
        
        outfile_data = []
        
        for line in idfree_lines:  
            if line == idfree_lines[0]:
                line = line.strip().split('\t')   
                
                for time in reverse_time_keys:
                    line.insert(1, str(time) + ' injection time # (mzML)')
                
                for charge in reverse_charge_keys:
                    line.insert(1, '+' + str(charge) + ' charge # (mzML)')
                
                line.insert(1, 'invalid injection times # (mzML)')
                line.insert(1, 'valid injection times # (mzML)')
                line.insert(1, 'invalid charges # (mzML)')
                line.insert(1, 'valid charges # (mzML)')    
                
                line.insert(1, 'Std. dev. injection time')
                line.insert(1, 'Mean injection time')
                line.insert(1, 'Median injection time')  
            else:
                line = line.strip().split('\t')
                current_raw_filename = line[0].strip().replace('.raw', '').replace('"', '')
                
                current_data = data[current_raw_filename]
                
                for time in reverse_time_keys:
                    line.insert(1, current_data['injection times'][time])
                
                for charge in reverse_charge_keys:
                    line.insert(1, current_data['charges'][charge])
                    
                line.insert(1, current_data['injection times']['unassigned']['count'])
                line.insert(1, current_data['injection times']['assigned']['count'])
                line.insert(1, current_data['charges']['unassigned'])
                line.insert(1, current_data['charges']['assigned'])
                
                line.insert(1, np.std(current_data['injection times']['assigned']['all']))
                line.insert(1, np.mean(current_data['injection times']['assigned']['all']))
                line.insert(1, np.median(current_data['injection times']['assigned']['all']))
                
                f = open(mzid_path + raw_filename + '.injection.times.tsv', 'w')
                for injection_t in current_data['injection times']['assigned']['all']:
                    f.write(str(injection_t).strip() + '\n')
                f.close() 
                
            output_line = ''
            for value in line:
                output_line += str(value) + '\t'
                
            outfile_data.append(output_line.strip() + '\n')
        
        outfile = open(mzid_path + raw_filename + '.qual.mspycloud.tsv', 'w')
        for outfile_line in outfile_data:
            outfile.write(outfile_line)
        outfile.close()
        
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['backup'] = True
    
    main(configs)