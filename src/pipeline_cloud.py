"""
Description:
    Data processing pipeline for CPTAC project.  This file will run all of the pipeline
    steps for a given set of newly acquired raw (.raw) data.  

Notes: 
    - Some steps will create new filepaths and if those filepaths already exist
    then they will deleted and created anew.  The header in each step's python function will mention
    which, if any, filepaths will be created anew.
    - If desired, each step can be run separately by either commenting out the other 
    steps' main calls, or opening a particular step's python module and running it.
"""
import os
import time
import shutil
from hotpot.configuring import read_new_ebs_config
from hotpot.read_write import read_lines as rl
from hotpot.read_write import read_uncommented_lines as rul
from hotpot.configuring import read_steps_config as steps

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
starcluster_config_path = pipeline_path + 'configs-starcluster\\starcluster'

def main(configs):
    import p0_quameter as step0
    import p1_msconvert as step1
    import p4_protein_inference as step4
    import p5_decoy_estimation as step5
    import p6_peptide_protein_quantitation as step6
    import p7_expression_matrix as step7
    
    step3_database = str(configs['options']['search_engine'])
    mod = str(configs['options']['modification']).strip().lower()
    label_type = str(configs['options']['label_type']).strip()
    
    if step3_database == 'msgf':
        if mod != 'intact':
            import c3_msgfplus as step3
        elif mod == 'intact':
            import c3_gpquest as step3
    #elif step3_database == 'myrimatch':
    #    import c3_myrimatch as step3
    #elif step3_database == 'comet':
    #    import c3_comet as step3
    #elif step3_database == 'xtandem':
    #    import c3_xtandem as step3
    else:
        raise Exception('Currently only MS-GF+ is supported (to select MS-GF+ use "msgf").')
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\')
            
        configs['backup'] = False
    
    step0.generate_raw_checksums(configs)
    step0.idfree_quameter(configs)
    step1.main(configs) 
    step3.main(configs)
    # for running quameter on search results
    #step0.id_quameter(configs)
    step4.main(configs)
    step5.main(configs)
    step6.main(configs)
    if label_type != 'free':
        step7.main(configs)
                          
if __name__ == '__main__':
    # header_expression_matrix data
    f = open(config_path + 'header_expression_matrix.tsv', 'r')
    header_expression_matrix_data = f.readlines()
    f.close()
            
    # correction_factors data
    f = open(config_path + 'correction_factors.tsv', 'r')
    correction_factors_data = f.readlines()
    f.close()
    
    # contaminants.fasta data
    f = open(config_path + 'contaminants.fasta', 'r')
    contams_lines = f.readlines()
    f.close()
    
    configs = {}
    
    configs['options'] = steps(config_path + 'steps.txt')
    configs['inference_search'] = steps(config_path + 'inference_search_params.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['myrimatch'] = steps(config_path + 'myrimatch.cfg')
    configs['comet'] = steps(config_path + 'comet.params')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['msconvert'] = rl(config_path + 'msconvert.txt')
    configs['msgf mods'] = rl(config_path + 'msgf_mods.txt')
    configs['ebs_params'] = read_new_ebs_config(config_path + 'ebs_params.txt')
    configs['node ips'] = rul(config_path + 'node_ips.txt')
    configs['header_expression_matrix'] = header_expression_matrix_data
    configs['correction_factors'] = correction_factors_data
    configs['contams_lines'] = contams_lines
    configs['backup'] = True
    
    main(configs)