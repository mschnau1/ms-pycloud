import os
import sys
import glob
import time
import shutil
import subprocess
from multiprocessing import Pool
from multiprocessing import cpu_count
from hotpot.configuring import read_steps_config as steps
from hotpot.configuring import read_msgf_mods_search_config as rmms

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
install_path = pipeline_path + 'installs\\'
documentation_path = pipeline_path + 'documentation\\'
msgf_path = pipeline_path + 'msgfplus\\'
msgf_params = rmms(config_path + 'msgf_mods.txt')

def main(configs):
    print '\nStep 3 continued: Converting mzID database search results to tsv format...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    exes = configs['exes']
    
    # sets variables to configuration values
    data_root = options['raw_data']
    mzid_path = data_root + 'step3-search_results\\'
    java_file = exes['java']
    #num_thread = int(cpu_count()) - 1
    
    # limited due to memory constraints on computers and this step is quick regardless
    num_thread = 2
    
    if num_thread == 0:
        num_thread = 1
    
    heap_string = msgf_params['-Xmx']
    heap_value = int(int(heap_string[:-1]) / num_thread)
    heap_size = '-Xmx' + str(heap_value) + heap_string[-1]
    
    mzid_files = glob.glob(mzid_path + '*.mzid')
    mzid_paths = [mzid_path] * len(mzid_files)
    java_files = [java_file] * len(mzid_files)
    heap_sizes = [heap_size] * len(mzid_files)
    
    pool = Pool(processes = num_thread)
    pool.map(mzid_to_tsv_in_parallel, zip(mzid_files, mzid_paths, java_files, heap_sizes))
            
def mzid_to_tsv_in_parallel((mzid_file, mzid_path, java_file, heap_size)):
    mzid_split = str(mzid_file).strip().split('\\')
    filename = mzid_split[-1].replace('.mzid', '.tsv')
    
    if not os.path.exists(mzid_path + filename):
        command = java_file + ' ' + heap_size + ' -cp ' + msgf_path + 'MSGFPlus.jar edu.ucsd.msjava.ui.MzIDToTsv -i ' + mzid_file + ' -o ' + mzid_path + filename + ' -showDecoy 1'
        print 'Converting... ' + str(os.path.basename(mzid_file))
        sys.stdout.flush()
        subprocess.call(command)   
    
if __name__ == '__main__':
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['exes'] = steps(config_path + 'executables.txt')
    configs['backup'] = True
    
    main(configs)