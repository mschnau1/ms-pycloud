# import standard libraries
import os, re, sys
import json
import numpy as np
import pandas as pd
import time
import random
import logging
import datetime
from sklearn.linear_model import LogisticRegression
import itertools as it
import math

# import multiprocessing
from multiprocessing import freeze_support
from multiprocessing.pool import Pool
from multiprocessing import cpu_count
import multiprocessing
import traceback

# pyteomics
from pyteomics import mgf, mzml
from tqdm import tqdm

from hotpot.gpquest_utils import read_param

# dir_path = os.path.dirname(os.path.realpath(__file__))

# print(log_file_path)


def timenow():
    return str(datetime.datetime.now())


def log_info(text):
    logging.info(
        "{0}: {1}".format(
            timenow(),
            text
        )
    )


def log_debug(text, method=""):
    logging.debug(
        "{0}: {1},{2}".format(
            timenow(),
            text,
            method
        )
    )


# read glycan list
def glycans_from_xlsx(xlsx_path, sheet_name):
    df = pd.ExcelFile(xlsx_path).parse(sheet_name)
    glycans = []
    columns = list(df.columns.values)
    for index, row in df.iterrows():
        glycans.append("".join(["{0}{1:d}".format(i, int(row[i])) for i in columns]))

    # unique glycans
    log_info("Preparation! Read in Glycans {} from {}".format(
        len(glycans), xlsx_path
    ))
    orig_count = len(glycans)
    glycans = list(set(glycans))
    log_info("Preparation! Remove {} Replicates! Unique N-linked Glycans {}".format(
        orig_count - len(glycans), len(glycans)
    ))
    log_info("Preparation! Monosaccharides : {}".format(",".join(columns)))
    return glycans


def add_glycan_mods(glycans, param):
    fix_gmods = []
    bin_gmods = []
    opt_gmods = []
    for mod in param["SEARCH"]["MODIFICATIONS"]["glycan_mods"]:
        x = re.split(",", mod)
        if x[2] == "fix":
            fix_gmods.append(x)
        elif x[2] == "binary":
            bin_gmods.append(x)
        elif x[2] == "opt":
            opt_gmods.append(x)

    for mod in fix_gmods:
        tg = mod[1]
        ng = mod[5]
        for i in range(len(glycans)):
            glycans[i] = re.sub(tg, ng, glycans[i])

    new_glycans = []
    if len(bin_gmods) == 0:
        new_glycans = glycans
    else:
        for mod in bin_gmods:
            tg = mod[1]
            ng = mod[5]
            for i in range(len(glycans)):
                g = glycans[i]
                g2 = re.sub(tg, ng, g)
                new_glycans.append(g)
                new_glycans.append(g2)

    for mod in opt_gmods:
        tg = mod[1]
        ng = mod[5]
        newgs = []
        for i in range(len(new_glycans)):
            g = new_glycans[i]
            x = re.finditer("[A-Za-z][\d]+", g)
            x = [g[j.start():j.end()] for j in x]
            temp_residues = []
            j = 0
            for j in range(len(x)):
                k = x[j][0]
                v = int(x[j][1:])
                if k == tg:
                    for k in range(v + 1):
                        z = "{0}{1}{2}{3}".format(tg, v - k, ng, k)
                        temp_residues.append(z)
                    break
            for t in temp_residues:
                newx = x[:]
                newx[j] = t
                newg = "".join(newx)
                newgs.append(newg)
        new_glycans = newgs[:]

    return new_glycans


def build_glycan_database(glycans, param):
    # calculate mass and build glycan database
    mass = param["MASS"]
    for i in range(len(glycans)):
        g = glycans[i]
        m = []
        x = re.finditer("[A-Za-z][\d]+", g)
        x = [g[j.start():j.end()] for j in x]
        for j in x:
            k = j[0]
            v = int(j[1:])
            m.append(v * mass["GLYCAN"][k])
        comp_mass = np.sum(np.array(m, dtype=float))
        glycans[i] = [g, comp_mass]
    return glycans


# peptide
def peptides_from_xlsx(pdb, ext=".xlsx"):
    df = None
    if ext == ".xlsx":
        df = pd.ExcelFile(pdb).parse("Sheet1")
    elif ext == ".csv":
        df = pd.read_csv(pdb, sep=',', header=0)
    else:
        return []
    peptides = {}
    n = 0
    for index, row in df.iterrows():
        n += 1
        if n % 100000 == 0:
            sys.stdout.write("{0}...".format(n))
            if n % 1000000 == 0:
                print("")
        sequence = str(row["Sequence"]).upper()
        pid = str(row["Accession"])
        try:
            pn = str(row["Protein Name"])
        except:
            pn = "UNKNOWN"
        try:
            start = row["Start"]
        except KeyError:
            start = -1
        try:
            peptides[sequence].append([pid, pn, start])
        except KeyError:
            peptides[sequence] = [[pid, pn, start]]
    return peptides


# TODO: digest fasta file
def peptides_from_fasta(pdb):
    print("digestdb")
    return []


def add_mods(sequences, param):
    pep_mods = param["SEARCH"]["MODIFICATIONS"]["mods"]
    fix_pep_mods = []
    bin_pep_mods = []
    opt_pep_mods = []

    # split modificaiton types
    for pm in pep_mods:
        x = re.split(",", pm)
        if x[1] in ["C-term", "N-term"]:
            continue
        if x[2] == "fix":
            fix_pep_mods.append(x)
        elif x[2] == "binary":
            bin_pep_mods.append(x)
        elif x[2] == "opt":
            opt_pep_mods.append(x)

    # add fix modifications to all the sequences
    for mod in fix_pep_mods:
        taa = mod[1]
        naa = mod[5]
        for i in range(len(sequences)):
            sequences[i] = re.sub(taa, naa, sequences[i])

    # add binary modifications to all the sequences, size enlarged
    for mod in bin_pep_mods:
        temp_sequences = []
        taa = mod[1]
        naa = mod[5]
        for i in range(len(sequences)):
            seq = sequences[i]
            seq2 = re.sub(taa, naa, seq)
            temp_sequences.append(seq)
            if seq2 != seq:
                temp_sequences.append(seq2)
        sequences = temp_sequences

    if len(opt_pep_mods) == 0:
        return sequences

    # add variable modfications to all the sequences
    new_sequences = []
    opt_mod_map = {}
    for mod in opt_pep_mods:
        opt_mod_map[mod[1]] = mod
    max_opt_mod = int(param["SEARCH"]["MODIFICATIONS"]["MAX_OPT_MOD"])
    for i in range(len(sequences)):
        seq = list(sequences[i])
        new_sequences.append("".join(seq))
        opt_mod_sites = []
        for j in range(0, len(seq)):
            if seq[j] in opt_mod_map:
                mod = opt_mod_map[seq[j]]
                if mod[3] == "any" or mod[3] == j:
                    opt_mod_sites.append(j)
        site_combinations = []
        for j in range(1, max_opt_mod + 1):
            site_combinations += it.combinations(opt_mod_sites, j)
        for sc in site_combinations:
            seq2 = seq[:]
            for s in sc:
                mod = opt_mod_map[seq[s]]
                seq2[s] = mod[5]
            new_sequences.append("".join(seq2))
    return new_sequences


# make decoys
def make_decoys(peptides):
    x = "".join(peptides)
    x = list(x)
    random.shuffle(x)
    sequence = "".join(x)
    decoys = []
    start = 0
    for p in peptides:
        end = start + len(p)
        decoys.append(sequence[start:end])
        start = end
    return decoys


def build_ion_index(peps, param):
    local_ion_index = {}
    factor = int(param["DATABASE"]["ION_INDEX"]["FACTOR"])
    for i in range(len(peps)):
        peptide = Peptide(peps[i], param)
        # print(peps[i])
        # if peps[i] == "EEQYNSTYR":
        #     print(peps[i] + "**************************")
        #     print(peptide)
        fmz, fn = peptide.get_fragments(["b", "y"], param)
        # if peps[i] == "EEQYNSTYR":
        #     print("fmz",fmz)
        #     print("fn",fn)

        for mz in fmz:
            int_mz = int(mz * factor)
            try:
                local_ion_index[int_mz].append(i)
            except KeyError:
                local_ion_index[int_mz] = [i]
    return local_ion_index


def peaks_match2(x, y, param):
    # print(x,y)
    """
    :param x: numpy array, ascend
    :param y: numpy array, ascend
    :param tol: float
    :return: matches, dictionary, {x_i:[y_i1,y_i2]}
    """
    start = 0
    end = len(y)
    matches = {}
    for i in range(len(x)):
        # print(start,end)
        update = None
        for j in range(start, end):
            # print(yj,xi,x[i+1])
            if (not update) and y[j] > x[i]:
                start = j
                update = True
            if i + 1 < len(x) and y[j] >= x[i + 1]:
                break
            if abs(x[i] - y[j]) <= np.max([param["_ms2_tol_factor"] * x[i], param["_ms2_tol_min"]]):
                try:
                    matches[i].append(j)
                except KeyError:
                    matches[i] = [j]
            else:
                if y[j] > x[i]:
                    break
    return matches


class Candidate:
    def __init__(self, peptide):
        self.x = {
            'Peptide': peptide,
            'Status': "NORMAL"
            # 'Glycan': None,
            # 'Mod': None,
            # 'Potential Glycans': [],
            # 'Theo Glycans': []
        }

    def __getitem__(self, item):
        try:
            return self.x[item]
        except KeyError:
            return None

    def __setitem__(self, key, value):
        self.x[key] = value


class Matching:
    def __init__(self):
        self.x = {
            'Peptide': {
                'PeaksNumber': 0,
                'Intensity': 0,
                'Ions': [],
                'IRatio': 0
            },
            'GP': {
                'PeaksNumber': 0,
                'Intensity': 0,
                'Ions': [],
                'IRatio': 0
            },
            'Mod': {
                'Mass': 0,
                'Mods': [],
            }
        }

    def __getitem__(self, item):
        try:
            return self.x[item]
        except KeyError:
            if item == "Scores":
                self.get_scores()
                return self.x["Scores"]
            else:
                return None

    def __setitem__(self, key, value):
        self.x[key] = value

    def get_scores(self):
        scores = {
            "Total IRatio": self['Peptide']['IRatio'] + self['GP']['IRatio'],
            "PeaksNumber": self['Peptide']['PeaksNumber'] + self['GP']['PeaksNumber'],
            "Hyper Score": self['Peptide']["hyperscore"]
        }
        scores["Morpheus"] = scores["PeaksNumber"] + scores["Total IRatio"]
        self.x['Scores'] = scores


class GPQuest3Score:
    def __init__(self, **kwargs):
        candidate = kwargs.get("candidate", Candidate(None))
        matching = kwargs.get("matching", Matching())
        self.x = {
            'Candidate': candidate,
            'Ion Index': {
                'Number': 0
            },
            'Matching': matching,
        }

    def __getitem__(self, item):
        try:
            return self.x[item]
        except KeyError:
            return None


def check_glycans(glycans, delta_mass, adducts, param):
    adducts_mass = adducts["mass"]
    adducts_name = adducts["name"]
    found_glycans = []
    for i in range(len(adducts_mass)):
        gmass = delta_mass - adducts_mass[i]
        for k in range(0, 5):  # isotope
            x = glycans.get(int(gmass - k * param["_mH"]))
            if x:
                mass_list = np.array([j[1] for j in x], dtype=float)
                diff_list = np.abs(mass_list - gmass + k * param["_mH"])
                mass_tol_list = mass_list * param["_ms1_tol_factor2"]
                y = np.where(diff_list < mass_tol_list)
                if y[0].size > 0:
                    found_glycans += [x[j] + [adducts_name[i], k] for j in y[0]]
    # print(found_glycans)
    return found_glycans


class Spectrum:
    def __init__(self, raw, param, file_format=".mzML"):
        self.x = {}
        # for k in raw:
        #     self.x[k] = raw[k]
        # self.x["m/z array"] = raw["m/z array"]
        # self.x["intensity array"] = raw["intensity array"]
        self.x['mz'] = np.copy(raw['m/z array'])
        self.x['i'] = np.copy(raw['intensity array'])
        self.x['mask'] = np.ones(self['mz'].shape, dtype=bool)
        self.x['ann'] = {}
        self.x['TIC'] = np.sum(self.x["i"])
        self.x['RTIC'] = self.x['TIC']
        self.x["Number of Peaks"] = len(raw["m/z array"])
        self.x['ms1scan'] = 'NA'
        self.x['BasePeakIntensity'] = 0
        self.x['BasePeakMz'] = 0
        if file_format == ".mgf":
            # mH = float(mass["MIC"]["H+"])
            title = self.parse_mgf_title(raw["params"]['title'])
            self.x["scan"] = title["scan"]
            self.x["ms level"] = 2
            self.x["precursor"] = {
                "mz": raw["params"]["pepmass"][0],
                "intensity": raw["params"]["pepmass"][1],
                "charge": raw["params"]["charge"][0],
                'mass': (self['precursor']['mz'] - param["_mH"]) * self['precursor']['charge']
            }
            self.x["rt"] = raw["params"]["rtinseconds"]

        elif file_format == ".mzML":
            self.x["scan"] = raw["index"] + 1
            self.x["ms level"] = raw["ms level"]
            if self.x["ms level"] == 2:
                try:
                    precursor = raw["precursorList"]['precursor'][0]['selectedIonList']['selectedIon'][0]

                    self.x["precursor"] = {}
                    # self.x['precursor']["mz"] = float(precursor["selected ion m/z"])
                    self.x["precursor"]["mz"] = float(precursor["selected ion m/z"])
                    self.x["precursor"]["isolation window target mz"] = \
                        raw['precursorList']['precursor'][0]['isolationWindow']['isolation window target m/z']
                    try:
                        self.x['precursor']["charge"] = int(precursor["charge state"])
                    except KeyError:
                        self.x['precursor']['charge'] = 0
                    self.x['precursor']['mass'] = self.calculate_precursor_mass()
                    try:
                        self.x["precursor"]["intensity"] = float(precursor["peak intensity"])
                    except KeyError:
                        self.x["precursor"]["intensity"] = 0
                except Exception as e:
                    log_debug('WARNING! Precursor {0}, Exception: {1}'.format(self.x["scan"], str(e)))
                    self.x = {}
                self.x["rt"] = raw["scanList"]["scan"][0]["scan start time"]
                # self.x["collision energy"] = raw["precursorList"]['precursor'][0]['activation']['collision energy']

    def __getitem__(self, item):
        try:
            return self.x[item]
        except KeyError:
            if item == "precursor mass":
                self.x["precursor"]["mass"] = self.calculate_precursor_mass()
                return self.x["precursor"]["mass"]
            return None

    def __setitem__(self, key, value):
        self.x[key] = value

    def calculate_precursor_mass(self):
        # mH = float(mass["MIC"]["H+"])
        return float(self.x["precursor"]["mz"]) * int(self.x["precursor"]["charge"])

    @staticmethod
    def make_column_names():
        columns = ['scan', 'mz', 'charge', 'intensity', 'est_charge', "est_mz", "mass_shift", "isotopes"]
        return columns

    def make_report(self):
        data = [self['scan']]
        data.append(self['precursor']['mz'])
        data.append(self['precursor']['charge'])
        data.append(self['precursor']['intensity'])
        data.append(self['precursor']['estimated charge'])
        data.append(self['precursor']['estimated monoisotopic mz'])
        data.append(self['precursor']['mass shift'])
        data.append(",".join(["{0:.2f}".format(i) for i in self['precursor']['isotopes']]))
        return data

    def oxo_match(self, names=[]):
        matched_number = 0
        matched_i = 0
        matched_names = []
        for i in names:
            oxo_mz, oxo_i = self.x['oxo'][i]
            if oxo_i > 0:
                matched_number += 1
            matched_i += oxo_i
            matched_names.append("{0},{1:.4f}".format(i, oxo_i))
        result = {
            "matched number": matched_number,
            "matched intensity": matched_i,
            "matched names": ";".join(matched_names)
        }
        return result
        # return [matched_number,matched_i,",".join(matched_names)]

    def reporters_match(self, names=[]):
        matched_number = 0
        matched_names = []
        for i in self['reporters']:
            if self['reporters'][i] > 0:
                matched_number += 1
        # print(self['reproters'])
        matched_names = ";".join(["{0},{1}".format(
            i, self['reporters'][i]
        ) for i in names])
        result = {
            "matched number": matched_number,
            "matched names": matched_names
        }
        return result
        # return [matched_number,matched_names]

    def peaks_match(self, ions, names, param):
        # print(names)
        x = np.argsort(ions)
        # print(x)
        xions = ions[x]
        xnames = [names[i] for i in x]

        matches = peaks_match2(self['mz_arr'], xions, param)
        matched_names = [xnames[i] for i in set(list(it.chain.from_iterable(matches.values())))]
        matched_names_number = len(matched_names)
        matched_peaks_number = len(matches.keys())

        targets = sorted(matches.keys())
        sum_intensity = np.sum(self['i'][targets])

        m = sorted(matches.keys())
        matched_mz_list = ["{0:.4f}".format(i) for i in self['mz'][np.array(m, dtype=int)]]
        matched_i_list = ["{0:.0f}".format(i) for i in self['i'][np.array(m, dtype=int)]]
        matches_frag_list = ["/".join([xnames[j] for j in matches[i]]) for i in m]
        mlist = ["{0}:{1}:{2}".format(matched_mz_list[i], matched_i_list[i],
                                      matches_frag_list[i]) for i in range(len(matched_mz_list))]

        result = {
            "PeaksNumber": matched_peaks_number,
            "Intensity": sum_intensity,
            "IonsNumber": matched_names_number,
            # "Ions": matched_names,
            "Matches": ";".join(mlist),
            "_matches": matches
        }
        return result

    def predict_glycan_type(self, param):
        predictor = param['GLYCAN_TYPE_PREDICTOR']
        try:
            oxo_i = np.array([self['oxo'][i] for i in param["_predictor_ions"]])
            oxo_i /= oxo_i[0]
            # print(oxo_i)
            prob = predictor.predict_proba([oxo_i])
            if prob[0][0] < 0.5:
                self.x['glycan type'] = "N"
                self.x['glycan prob'] = float(prob[0][1])
            else:
                self.x['glycan type'] = "O"
                self.x['glycan prob'] = float(prob[0][0])
        except:
            self.x['glycan type'] = 'NA'
            self.x['glycan prob'] = 'NA%'
        return True

    def parse_mgf_title(self, title):
        x = re.split("\s", title)
        y = re.split("\.", x[0])
        t = {
            "scan": y[1],
            "filename": y[0]
        }
        return t

    # def remove_ions(self, ions):
    #     matches = peaks_match(self['mz'], ions)
    #     targets = sorted(matches.keys())
    #     mask = np.ones(self['mz'].shape, dtype=bool)
    #     mask[targets] = False
    #     self["mz"] = self["mz"][mask]
    #     self["i"] = self["i"][mask]

    def get_oxo_peaksnumber(self):
        if not self['oxo']:
            return 0
        peaks_number = 0
        for x in self['oxo']:
            if self['oxo'][x] > 0:
                peaks_number += 1
        return peaks_number

    def get_oxo_intensity(self):
        if not self['oxo']:
            return 0
        else:
            return np.sum([self['oxo'][i] for i in self['oxo']])

    def remove_oxo_ions(self, param):
        topn = int(param["PREPROCESS"]["OXONIUM_IONS"]["TOPN"])
        ess = param["PREPROCESS"]["OXONIUM_IONS"]["ESS_IONS"]
        nss = param["PREPROCESS"]["OXONIUM_IONS"]["NON_ESS_IONS"]
        # tol = int(param["SEARCH"]["MASS_TOLERANCE"]["MS2(PPM)"])
        mz_arr = self['mz'][self['mask']]
        i_arr = self['i'][self['mask']]
        ess_matches = peaks_match2(mz_arr, param["_sorted_ess_mz_list"], param)
        ess_targets = sorted(ess_matches.keys())

        if len(ess_targets) < len(ess):
            logging.debug("SCAN: {0}, REMOVE OXO: {1}".format(self['scan'], len(ess_targets)))
            return False
        y = self['i'].argsort()[-topn:]
        threshold = self['i'][y[0]]
        # y.sort()

        for i in ess_targets:
            # if i not in y:
            if i_arr[i] < threshold:
                logging.debug("SCAN: {0}, REMOVE OXO: {1} not in top {2}".format(self['scan'], i, topn))
                return False

        nss_matches = peaks_match2(mz_arr, param["_sorted_nss_mz_list"], param)
        nss_targets = sorted(nss_matches.keys())

        self.x['oxo'] = {}
        for i in ess:
            self.x['oxo'][i] = 0
        for i in nss:
            self.x['oxo'][i] = 0

        iso_ess_mz_list = list(np.array(param["_sorted_ess_mz_list"], dtype=float) + 1)
        iso_nss_mz_list = list(np.array(param["_sorted_nss_mz_list"], dtype=float) + 1)
        iso_ess_matches = peaks_match2(mz_arr, iso_ess_mz_list, param)
        iso_nss_matches = peaks_match2(mz_arr, iso_nss_mz_list, param)

        z = np.where(self['mask'] == True)
        y = np.array(ess_targets + nss_targets + list(iso_ess_matches.keys()) + list(iso_nss_matches.keys()),
                     dtype=int)
        # y = np.array(y,dtype=int)
        zy = z[0][y]
        self['mask'][zy] = False

        loc_dict = {}
        for i in range(int(y.shape[0])):
            loc_dict[y[i]] = zy[i]

        for p in ess_matches:
            name = ess[param["_ess_mz_order"][ess_matches[p][0]]]
            self.x['oxo'][name] += i_arr[p]
            self['ann'][loc_dict[p]] = name

        for p in iso_ess_matches:
            name = ess[param["_ess_mz_order"][iso_ess_matches[p][0]]]
            self.x['oxo'][name] += i_arr[p]
            self['ann'][loc_dict[p]] = name + "[M+1]"

        for p in nss_matches:
            name = nss[param["_nss_mz_order"][nss_matches[p][0]]]
            self.x['oxo'][name] += i_arr[p]
            self['ann'][loc_dict[p]] = name

        for p in iso_nss_matches:
            name = nss[param["_nss_mz_order"][iso_nss_matches[p][0]]]
            self.x['oxo'][name] += i_arr[p]
            self['ann'][loc_dict[p]] = name + "[M+1]"

        self['oxo']['ratio'] = np.sum(np.array(list(self['oxo'].values()), dtype=float)) / np.sum(i_arr)

        # mask = np.ones(self['mz'].shape, dtype=bool)
        # mask[ess_targets] = False
        # mask[nss_targets] = False
        # mask[iso_ess_matches.keys()] = False
        # mask[iso_nss_matches.keys()] = False
        # self["mz"] = self["mz"][mask]
        # self["i"] = self["i"][mask]
        self["RTIC_RM_OXO"] = np.sum(self["i"][self["mask"]])
        return True

    def get_reporters_peaksnumber(self):
        if not self['reporters']:
            return 0
        peaks_number = 0
        for x in self['reporters']:
            i = self['reporters'][x]
            if i > 0:
                peaks_number += 1
        return peaks_number

    def get_reporters_intensity(self):
        if not self['reporters']:
            return 0
        intensity = 0
        for x in self['reporters']:
            i = self['reporters'][x]
            intensity += i
        return intensity

    def get_reporters_str(self):
        # print(self.x)
        # print(self.x['reporters'])
        x = ["{0}/{1:.4f}".format(r, self['reporters'][r]) for r in sorted(self.x['reporters'].keys())]
        return ";".join(x)

    def remove_reporter_ions(self, ions, channels, param):
        try:
            matches = peaks_match2(self['mz'], ions, param)
            targets = sorted(matches.keys())

            self.x['reporters'] = {}
            for i in channels:
                self.x['reporters'][i] = 0
            for t in targets:
                i = self['i'][t]
                name = channels[matches[t][0]]
                self.x['reporters'][name] += i
                self['ann'][t] = name

            # mask = np.ones(self['mz'].shape, dtype=bool)
            self["mask"][targets] = False
            # self["mz"] = self["mz"][mask]
            # self["i"] = self["i"][mask]
            self["RTIC"] = np.sum(self['i'][self['mask']])
            # if int(self['scan']) == 7540:
            #     mz_arr = np.array(self['mz'][self['mask']])
            #     i_arr = np.array(self['i'][self['mask']])
            #     with open('C:/Users/yhu39/Documents/temp/7540_rm_reporters.mgf', 'w') as f:
            #         for i, j in zip(mz_arr, i_arr):
            #             f.write('{0}\t{1}\n'.format(i, j))
            #         f.close()
            return True
        except:
            return False

    def remove_zeros(self):
        x = np.where(self['i'] > 0)
        self['mz'] = self['mz'][x]
        self['i'] = self['i'][x]
        self['mask'] = self['mask'][x]
        peaks_number = len(self['mz'])
        if peaks_number < 10:
            log_info("{0},{1}".format(
                "scan: {0}".format(self['scan']),
                "func: remove_zeros, too few peaks: {0}".format(peaks_number)
            ))
            return False
        else:
            return True

    def top_peaks(self, n=100):
        # mz = np.array([1, 4, 3, 5, 2, 7])
        # i = np.array([100, 200, 300, 400, 500, 600])
        try:
            y = self['i'][self['mask']].argsort()[-n:]
            y.sort()
            z = np.where(self['mask'] == True)
            y = z[0][y]
            self["mask"] = np.zeros(self['mz'].shape, dtype=bool)
            self["mask"][y] = True
            # self['mz'] = self['mz'][y]
            # self['i'] = self['i'][y]
            # if int(self['scan']) == 7540:
            #     mz_arr = np.array(self['mz'][self['mask']])
            #     i_arr = np.array(self['i'][self['mask']])
            #     with open('C:/Users/yhu39/Documents/temp/7540_top_peaks.mgf', 'w') as f:
            #         for i, j in zip(mz_arr, i_arr):
            #             f.write('{0}\t{1}\n'.format(i, j))
            #         f.close()
            return True
        except:
            log_info("WARNING!,{0}, function: top_peaks, Error: {0}".format(
                self['scan'],
                sys.exc_info()[0]))
            return False

    def match(self, pepobj, param, **kwargs):
        # tol = param["SEARCH"]["MASS_TOLERANCE"]["MS2(PPM)"]
        if self["precursor"]["mass"] < pepobj["Monoisotopic Mass"]:
            return None
        matching = Matching()
        ann = {}

        # matching intact glycopeptide (Pep, Pep+HexNac,...)
        # generate glycopep mass list

        gp_ions, gp_names = pepobj.make_intact_glycopep_ions(param)

        # matching['GP'] = self.peaks_match(ions=gp_ions, names=gp_names,param=param)
        # sort glycopep mass list and name list, ascending sort
        asc_gp_order = np.argsort(gp_ions)
        asc_gp_ions = gp_ions[asc_gp_order]
        asc_gp_names = [gp_names[i] for i in list(asc_gp_order)]

        # match mz_arr and ions list
        gp_matches = peaks_match2(self['mz_arr'], asc_gp_ions, param)
        gp_matched_names = [asc_gp_names[i] for i in set(list(it.chain.from_iterable(gp_matches.values())))]
        gp_matched_names_number = len(gp_matched_names)
        gp_matched_peaks_number = len(gp_matches.keys())

        gp_targets = np.array(sorted(gp_matches.keys()), dtype=int)
        gp_sum_intensity = np.sum(self['i_arr'][gp_targets])

        # m = sorted(gp_matches.keys())
        gp_matched_mz_list = ["{0:.4f}".format(i) for i in self['mz_arr'][gp_targets]]
        gp_matched_i_list = ["{0:.0f}".format(i) for i in self['i_arr'][gp_targets]]
        # gp_matches_frag_list = ["/".join([asc_gp_names[j] for j in gp_matches[i]]) for i in gp_targets]
        gp_matches_frag_list = []
        for i in gp_targets:
            ann[i] = "/".join([asc_gp_names[j] for j in gp_matches[i]])
            gp_matches_frag_list.append(ann[i])
        gp_mlist = ["{0}:{1}:{2}".format(gp_matched_mz_list[i],
                                         gp_matched_i_list[i],
                                         gp_matches_frag_list[i]) for i in range(len(gp_matched_mz_list))]

        # keep search result of GP ions
        matching['GP'] = {
            "PeaksNumber": gp_matched_peaks_number,
            "Intensity": gp_sum_intensity,
            "IonsNumber": gp_matched_names_number,
            "Matches": ";".join(gp_mlist),
        }

        # search isotope peaks of gp ions
        iso_gp_ions = np.array(gp_ions, dtype=float)
        iso_gp_ions[0:5] += 1
        iso_gp_ions[5:9] += 0.5
        iso_gp_ions[9:] += 0.333333
        # iso_gp_matches = self.peaks_match(ions=iso_gp_ions, names=gp_names, param=param)

        # sort glycopep mass list and name list, ascending sort
        asc_iso_gp_order = np.argsort(iso_gp_ions)
        asc_iso_gp_ions = iso_gp_ions[asc_iso_gp_order]
        asc_iso_gp_names = [gp_names[i] + "[M+1]" for i in list(asc_iso_gp_order)]

        iso_gp_matches = peaks_match2(self['mz_arr'], asc_iso_gp_ions, param)
        iso_gp_targets = np.array(list(iso_gp_matches.keys()), dtype=int)

        for i in iso_gp_targets:
            ann[i] = "/".join([asc_iso_gp_names[j] for j in iso_gp_matches[i]])

        matching['GP']["Intensity"] += np.sum(self['i_arr'][iso_gp_targets])
        # matching['GP']["PeaksNumber"] += iso_gp_matches["PeaksNumber"]
        gpr = matching['GP']["Intensity"] / np.sum(self['i_arr'])
        matching['GP']['IRatio'] = gpr

        mask = np.ones(self['mz_arr'].shape, dtype=bool)
        mask[gp_targets] = False
        mask[iso_gp_targets] = False
        temp_mz = self["mz_arr"][mask]
        temp_i = self["i_arr"][mask]

        nglycans = kwargs.get("nglycans", None)
        oglycans = kwargs.get("oglycans", None)

        # search peptide fragment peaks
        # compare with b/y. c/z ions
        pepfrag_ions, pepfrag_names = pepobj.get_fragments(["b", "y"], param)
        pepfrag_ions = np.array(pepfrag_ions, dtype=float)

        asc_pepfrag_order = np.argsort(pepfrag_ions)
        # srt = sorted, asc = ascending
        asc_pepfrag_ions = pepfrag_ions[asc_pepfrag_order]
        asc_pepfrag_names = [pepfrag_names[i] for i in list(asc_pepfrag_order)]

        # tol = int(param["SEARCH"]["MASS_TOLERANCE"]["MS2(PPM)"])
        pepfrag_matches = peaks_match2(temp_mz, asc_pepfrag_ions, param)

        # filter all spectra without peptide fragments matchings
        if len(pepfrag_matches.keys()) == 0:
            return None
        pepfrag_matched_names = [asc_pepfrag_names[i] for i in
                                 set(list(it.chain.from_iterable(pepfrag_matches.values())))]
        pepfrag_matched_names_number = len(pepfrag_matched_names)
        pepfrag_matched_peaks_number = len(pepfrag_matches.keys())

        nb = 0
        ny = 0
        ib = 0
        iy = 0
        for i in pepfrag_matches:
            pep_frag_name = asc_pepfrag_names[pepfrag_matches[i][0]]
            if pep_frag_name[0] == "b":
                nb += 1
                ib += temp_i[i]
            elif pep_frag_name[0] == "y":
                ny += 1
                iy += temp_i[i]

        # hyperscore = np.log(math.factorial(nb) * math.factorial(ny) * math.factorial(matching['GP']["PeaksNumber"]) * (
        #     ib + iy + matching['GP']["Intensity"]))

        pepfrag_targets = np.array(sorted(list(pepfrag_matches.keys())), dtype=int)
        sum_intensity = np.sum(temp_i[pepfrag_targets])
        iratio = sum_intensity / np.sum(self['i_arr'])

        # print(pepobj['Sequence'])
        # print(nb)
        # print(ny)
        # print(matching['GP']["PeaksNumber"])
        # print(iratio)

        hyperscore = np.log(math.factorial(nb + ny + matching['GP']["PeaksNumber"]) * (iratio + gpr))

        # m = sorted(pepfrag_matches.keys())
        pepfrag_matched_mz_list = ["{0:.4f}".format(i) for i in temp_mz[np.array(pepfrag_targets, dtype=int)]]
        pepfrag_matched_i_list = ["{0:.0f}".format(i) for i in temp_i[np.array(pepfrag_targets, dtype=int)]]
        # pepfrag_matches_frag_list = ["/".join([asc_pepfrag_names[j] for j in pepfrag_matches[i]]) for i in
        #                              pepfrag_targets]
        pepfrag_matches_frag_list = []
        z = np.where(mask == True)
        z = z[0]
        for i in pepfrag_targets:
            x = z[i]
            ann[x] = "/".join([asc_pepfrag_names[j] for j in pepfrag_matches[i]])
            pepfrag_matches_frag_list.append(ann[x])

        pepfrag_mlist = ["{0}:{1}:{2}".format(pepfrag_matched_mz_list[i],
                                              pepfrag_matched_i_list[i],
                                              pepfrag_matches_frag_list[i]) for i in
                         range(len(pepfrag_matched_mz_list))]

        matching["Peptide"] = {
            "PeaksNumber": pepfrag_matched_peaks_number,
            "Intensity": sum_intensity,
            "IonsNumber": pepfrag_matched_names_number,
            # "Ions": matched_names,
            "Matches": ";".join(pepfrag_mlist),
            "IRatio": iratio,
            'Nb': nb,
            "Ny": ny,
            'Ib': ib,
            'Iy': iy,
            'hyperscore': hyperscore
        }

        matching["Annotation"] = ann
        # print(self['precursor']['mass'])
        # print(pepobj["Monoisotopic Mass"])
        # print(mod_mass)

        delta_mass = self['precursor']['mass'] - pepobj["Monoisotopic Mass"]
        matching['Mod']['Mass'] = delta_mass
        precursor_charge = self["precursor"]["charge"]
        try:
            adducts = param["SEARCH"]["MODIFICATIONS"]["ADDCUT"][precursor_charge]
        except KeyError:
            adducts = {
                "mass": [1.00728 * precursor_charge],
                "name": ["{0}H".format(precursor_charge)]
            }
        # TODO: add 'space' to param
        # print(self['scan'],mod_mass)
        # mods = param["SPACE"]['MOD_INDEX'][mod_mass]

        if nglycans:
            found_nglycans = check_glycans(nglycans, delta_mass, adducts, param)
            if found_nglycans:
                matching["Mod"]["NGLYCAN MASS"] = found_nglycans[0][1]
                matching['Mod']['NGLYCAN'] = "{0}:{1}".format(found_nglycans[0][0], found_nglycans[0][2])
                matching['Mod']["Shift"] = found_nglycans[0][3]
                matching['Mod']['NGLYCANS'] = "/".join(
                    ["{0}:{1:.4f}:{2}:{3}".format(g[0], g[1], g[2], g[3]) for g in found_nglycans])
            else:
                matching["Mod"]["NGLYCAN MASS"] = 0
                matching['Mod']["Shift"] = 0
                matching['Mod']['NGLYCAN'] = "NA"
                matching['Mod']['NGLYCANS'] = "NA"

        if oglycans:
            found_oglycans = check_glycans(oglycans, delta_mass, adducts, param)
            if found_oglycans:
                matching["Mod"]["OGLYCAN MASS"] = found_oglycans[0][1]
                matching['Mod']['OGLYCAN'] = "{0}:{1}".format(found_oglycans[0][0], found_oglycans[0][2])
                matching['Mod']["Shift"] = found_oglycans[0][3]
                matching['Mod']['OGLYCANS'] = "/".join(
                    ["{0}:{1:.4f}:{2}:{3}".format(g[0], g[1], g[2], g[3]) for g in found_oglycans])
            else:
                matching["Mod"]["OGLYCAN MASS"] = 0
                matching['Mod']["Shift"] = 0
                matching["Mod"]["OGLYCAN"] = "NA"
                matching["Mod"]["OGLYCANS"] = "NA"

        # mods = []
        # matching['Mod']['Mods'] = [param["SPACE"]['MODS'][m] for m in mods]

        return matching


def top_peaks(spectrum, param):
    n = int(param["PREPROCESS"]["PEAK_SELECTION"]["TOPN"])
    return spectrum.top_peaks(n)


def remove_zeros(spectrum, param=None):
    return spectrum.remove_zeros()


def oxonium_ions_filtration(spectrum, param):
    return spectrum.remove_oxo_ions(param)


def glycan_type_prediction(spectrum, param):
    return spectrum.predict_glycan_type(param)


def remove_reporter_ions(spectrum, param):
    label = param['SEARCH']['LABEL']
    mass = param["MASS"]
    if label == "None":
        spectrum['reporters'] = {}
        return True
    else:
        return spectrum.remove_reporter_ions(ions=np.array(mass["LABEL"][label]["ions"], dtype=float),
                                             channels=mass["LABEL"][label]["channels"],
                                             param=param)


proc_dict = {
    "TopPeaks": top_peaks,
    "OxoFilter": oxonium_ions_filtration,
    "GlycanPredictor": glycan_type_prediction,
    "RemoveReporterIons": remove_reporter_ions,
    "RemoveZeros": remove_zeros
}


def make_isotopes(mz, charges, neg_mz_window, pos_mz_window, param):
    isotopes = {}
    for charge in charges:
        temp_mz_list = []
        unit = float(param["_mH"]) / charge
        for i in range(neg_mz_window, pos_mz_window + 1):
            new_mz = mz + i * unit
            temp_mz_list.append("{0:.4f}".format(new_mz))
        isotopes[charge] = temp_mz_list
    return isotopes


def merge_isotopes_ms1scans(ms1scans, mz, min_charge, max_charge, neg_mz_window, pos_mz_window, param):
    charges = range(max_charge, min_charge - 1, -1)
    isotopes = make_isotopes(mz, charges, neg_mz_window=neg_mz_window, pos_mz_window=pos_mz_window, param=param)
    mz_list = []
    for i in isotopes.values():
        mz_list += i
    mz_list = list(set(mz_list))
    mz_list = np.array(mz_list, dtype=float)
    mz_list.sort()
    intensity_list = np.zeros(len(mz_list))
    for spec in ms1scans:
        matches = peaks_match2(mz_list, spec['mz'], param)
        for i in matches:
            m = matches[i]
            mi = [spec['i'][x] for x in m]
            intensity_list[i] += np.sum(np.array(mi, dtype=float))
    isotopes = {
        "index": isotopes,
        "peaks": {}
    }
    for i in range(len(mz_list)):
        isotopes['peaks']["{0:.4f}".format(mz_list[i])] = intensity_list[i]
    # with open("E:\\data\\raw\\test\\isotopes.txt","w") as f:
    #     for i in isotopes['peaks']:
    #         f.write("{0:.4f}\t{1:.4f}\n".format(float(i),isotopes['peaks'][i]))
    #     f.close()
    return isotopes


def precursor_charge_recalculation_yhu(spectrum, ms1scans, param):
    pmz = spectrum['precursor']['isolation window target mz']

    min_charge = 2
    max_charge = 6
    neg_mz_window = -3
    pos_mz_window = 3
    isotopes = merge_isotopes_ms1scans(ms1scans, pmz, min_charge=min_charge, max_charge=max_charge,
                                       neg_mz_window=neg_mz_window, pos_mz_window=pos_mz_window, param=param)
    # print("isotopes")
    # print(isotopes)
    base_line = isotopes['peaks']['{0:.4f}'.format(pmz)] * 0.2
    intensites = []
    for charge in isotopes['index']:
        mz_list = isotopes['index'][charge]
        bins = [isotopes["peaks"][mz] for mz in mz_list]
        # print(charge,bins)
        mi = np.sum(np.array(bins, dtype=float))
        mn = 0
        for b in bins:
            if b > base_line:
                mn += 1
        intensites.append([charge, mi, mn])
    # print(intensites)
    intensites = sorted(intensites, key=lambda x: (x[2], x[1]), reverse=True)
    # print(intensites)
    precursor_charge = intensites[0][0]
    # print("charge:{}".format(charge))
    max_i = intensites[0][1]
    max_n = intensites[0][2]
    if max_n == 1:
        spectrum['precursor']['estimated charge'] = 0
        return
    bins = None
    isotopes = merge_isotopes_ms1scans(ms1scans, pmz, min_charge=min_charge, max_charge=max_charge,
                                       neg_mz_window=neg_mz_window, pos_mz_window=pos_mz_window, param=param)
    mz_list = isotopes['index'][precursor_charge]
    bins = [isotopes["peaks"][mz] for mz in mz_list]
    # print(precursor_charge)
    # print(bins)
    left_bins = np.array(bins[0:4])
    right_bins = np.array(bins[3:])
    num_left = np.size(np.where(left_bins > 0))
    num_right = np.size(np.where(right_bins > 0))
    if num_left <= 1 and num_right <= 1:
        spectrum['precursor']['estimated charge'] = 0
        return
    else:
        if precursor_charge == 4 or precursor_charge == 6:
            mz_list = isotopes['index'][precursor_charge]
            bins = np.array([isotopes["peaks"][mz] for mz in mz_list], dtype=float)
            mp = np.size(np.where(bins > base_line))
            mi = np.sum(bins)
            mz_list2 = isotopes['index'][precursor_charge / 2]
            bins2 = np.array([isotopes["peaks"][mz] for mz in mz_list2], dtype=float)
            mp2 = np.size(np.where(bins2 > base_line))
            mi2 = np.sum(bins2)
            if mp2 > mp and mi2 > mi and mp < int(len(mz_list) / 2):
                precursor_charge /= 2
    spectrum['precursor']['estimated charge'] = precursor_charge
    # print(precursor_charge)


def precursor_monoisotopic_mz_recalculation(spectrum, ms1scans, param, mode="original"):
    mz = spectrum['precursor']['mz']
    charge = spectrum['precursor']['charge']
    if charge == 0:
        spectrum['precursor']['estimated monoisotopic mz'] = 0
        spectrum['precursor']['mass shift'] = 0
        spectrum['precursor']['isotopes'] = []
        return
    if mode == "estimated":
        mz = spectrum['precursor']['isolation window target mz']
        charge = spectrum["precursor"]["estimated charge"]
    isotopes = merge_isotopes_ms1scans(ms1scans,
                                       mz,
                                       min_charge=charge,
                                       max_charge=charge,
                                       neg_mz_window=-3,
                                       pos_mz_window=3, param=param)
    mz_list = isotopes['index'][charge]
    bins = [isotopes["peaks"][mz] for mz in mz_list]
    bins = np.array(bins, dtype=float)
    max_intensity = np.max(bins)
    # print(max_intensity)
    base_line = 0.1 * max_intensity
    x = np.where(bins > base_line)
    # print(x)
    if np.size(x) == 0:
        spectrum['precursor']['estimated monoisotopic mz'] = 0
        spectrum['precursor']['mass shift'] = 0
        spectrum['precursor']['isotopes'] = bins
    else:
        # offset = np.min(x)
        offset = 3
        for k in range(3, -1, -1):
            if bins[k] < base_line:
                break
            else:
                offset = k
        # if offset > 3:
        #     offset = 3
        mass_shift = offset - 3
        spectrum['precursor']['estimated monoisotopic mz'] = mz_list[offset]
        spectrum['precursor']['mass shift'] = mass_shift
        spectrum['precursor']['isotopes'] = bins


def preprocess_msfile(ms_file, param):
    print("START Preprocessing...")
    log_info("Preprocessing Start!")
    ms2_spectra = []
    spectra = None
    bn, ext = os.path.splitext(ms_file)
    if ext in [".mzml", ".mzML"]:
        spectra = mzml.read(ms_file)
    elif ext in [".MGF", ".mgf"]:
        spectra = mgf.read(ms_file)

    i = 0

    ms1_spectra = []
    ms1_spectra_index = {}

    try:
        scans = param["SEARCH"]["SCANS"]
    except KeyError:
        scans = []
    scans_dict = {}
    for s in scans:
        scans_dict[s] = None
    scan_range = param["SEARCH"]["SCANS"]
    scan_range_size = len(scan_range)
    max_scan = 0
    if scan_range_size > 0:
        max_scan = int(np.max(np.array(scan_range, dtype=int)))
    for spectrum in spectra:
        i += 1
        if i % 10000 == 0:
            log_info('Preprocess: {0}'.format(i))
            print('Preprocess: {0}'.format(i))
        # if int(spectrum['index']) > 8372:
        #     break
        scan = spectrum['index'] + 1
        if scan_range_size > 0:
            if scan > max_scan:
                break
            if scan not in scans_dict:
                continue

        spectrum = Spectrum(spectrum, param, file_format=ext)
        if not spectrum:
            continue
        if spectrum['ms level'] == 1:
            ms1_spectra.append(spectrum)
            ms1_spectra_index[spectrum['scan']] = len(ms1_spectra) - 1
        elif spectrum["ms level"] == 2:
            if len(scans) != 0 and (spectrum['scan'] not in scans_dict.keys()):
                continue
            if ext in [".mzml", ".mzML"]:
                try:
                    spectrum['ms1scan'] = ms1_spectra[-1]['scan']
                except Exception as e:
                    log_debug("WARNING! Can't find MS1 Scan {}".format(spectrum['scan'], e))
                    spectrum['ms1scan'] = "NA"
            else:
                spectrum['ms1scan'] = 'NA'
            succeed = True
            # for proc_name in param['preprocess']:
            # methods = ["RemoveZeros", "RemoveReporterIons", "TopPeaks", "OxoFilter", "GlycanPredictor"]
            methods = param["WORKFLOW"]["PREPROCESS"]
            for proc_name in methods:
                proc = proc_dict[proc_name]
                succeed = proc(spectrum, param)
                if not succeed:
                    log_debug("SCAN: {0}, FAIL: {1}".format(spectrum['scan'], proc_name))
                    break
            if succeed:
                ms2_spectra.append(spectrum)
                # todo delete
                # if int(spectrum['scan']) == 7540:
                #     mz_arr = np.array(spectrum['mz'][spectrum['mask']])
                #     i_arr = np.array(spectrum['i'][spectrum['mask']])
                #     with open('C:/Users/yhu39/Documents/temp/7540_1.mgf', 'w') as f:
                #         for i, j in zip(mz_arr, i_arr):
                #             f.write('{0}\t{1}\n'.format(i, j))
                #         f.close()
                #     break
    print("Start MSCorrection @ {}".format(str(datetime.datetime.now())))

    if "MSCORRECTION" in param["WORKFLOW"]:
        n = 0
        for spectrum in ms2_spectra:
            n += 1
            if n % 1000 == 0:
                print("CORRECT {}...".format(n))
            if 'charge' not in spectrum['precursor'] or spectrum['precursor']['charge'] == 0:
                orig_scan = spectrum['ms1scan']
                # print(orig_scan)
                orig_pos = ms1_spectra_index[orig_scan]
                # print(orig_pos)
                start_pos = orig_pos - param["WORKFLOW"]["MSCORRECTION"]["SCAN_WINDOW"]
                # print(start_pos)
                end_pos = orig_pos + param['WORKFLOW']['MSCORRECTION']["SCAN_WINDOW"]
                # print(end_pos)
                ms1scans = ms1_spectra[start_pos:end_pos]
                # if spectrum["precursor"]["charge"] == 0:
                precursor_charge_recalculation_yhu(spectrum, ms1scans, param)
                spectrum["precursor"]["charge"] = spectrum["precursor"]["estimated charge"]

                precursor_monoisotopic_mz_recalculation(spectrum, ms1scans, param)
                spectrum["precursor"]["mz"] = spectrum["precursor"]["estimated monoisotopic mz"]

                spectrum["precursor"]["mass"] = spectrum.calculate_precursor_mass()
    print("Preprocess Done @ {}".format(str(datetime.datetime.now())))
    return ms2_spectra


class Peptide:
    def __init__(self, peptide, param, **kwargs):
        # param = kwargs.get("param", None)
        seq, mods = self.parse_peptide(peptide, param)
        self.x = {
            "Sequence": "".join(seq),
            "Mods": mods,
            "Status": kwargs.get("status", "NORMAL"),
            "Proteins": kwargs.get("proteins", []),
            "Fragments": None,
            "Monoisotopic Mass": 0,
            "length": len(seq)
        }
        self.calc_monoisotopic_mass(param)
        # print(seq)
        # print(self["Monoisotopic Mass"])
        prot = kwargs.get("prot")
        if prot:
            self["Proteins"].append(prot)

    def __getitem__(self, item):
        if item in self.x:
            # if item == "Fragments":
            #     if not self.x["Fragments"]:
            #         self.make_fragments()
            #         return self.x["Fragments"]
            return self.x[item]
        else:
            return None

    def __setitem__(self, key, value):
        self.x[key] = value

    @staticmethod
    def parse_peptide(peptide, param):
        label = param["SEARCH"]['LABEL']
        tokens = list(peptide)
        seq = []
        mods = {}
        for i in range(len(tokens)):
            aa = tokens[i]
            if aa in param["_mods_map"]:
                mods[i + 1] = param["_mods_map"][aa][1]
                seq.append(param["_mods_map"][aa][0])
            else:
                seq.append(aa)
        if label in ["TMT10plex", "iTRAQ4plex", "iTRAQ8plex", "TMT11plex", "TMT16plex"]:
            mods['n'] = label
            for i in range(len(seq)):
                if seq[i] == 'K':
                    mods[i + 1] = label

        return seq, mods

    @staticmethod
    def parse_peptide2(peptide, param):
        # peptide = "n[42]PEN[+N5H3S0G0]STIDEKc[+50]"
        # peptide = "ABC[CD]CDn[14]"
        label = param["SEARCH"]["LABEL"]
        m = re.finditer("[A-Znc]\[.+?\]|[A-Z]", peptide)
        tokens = [peptide[i.start():i.end()] for i in m]
        seq = []
        mods = {}
        for i in range(len(tokens)):
            m = re.search("([A-Znc])\[(.+)\]", tokens[i])
            if m:
                aa = m.group(1)
                mod = m.group(2)
                if aa in ["n", "c"]:
                    mods[aa] = mod
                else:
                    seq.append(aa)
                    mods[i + 1] = mod
            else:
                aa = tokens[i]
                seq.append(aa)
                if aa == "C" and (i + 1) not in mods.keys():
                    mods[i + 1] = "Carbamidomethyl"
        if label in ["TMT10plex", "iTRAQ4plex", "iTRAQ8plex", "TMT11plex", "TMT16plex"]:
            mods['n'] = label
            for i in range(len(seq)):
                if seq[i] == 'K':
                    mods[i + 1] = label

        return seq, mods

    def get_full_name(self):
        name = list(self["Sequence"])
        nterm = ""
        cterm = ""
        for i in self["Mods"]:
            if i == "n":
                nterm = "n[{}]".format(self["Mods"][i])
            elif i == "c":
                cterm = "c[{}]".format(self["Mods"][i])
            else:
                name[i - 1] = "{0:s}[{1:s}]".format(name[i - 1], self["Mods"][i])
        return nterm + "".join(name) + cterm

    def make_intact_glycopep_ions(self, param):
        # mH = float(mass["MIC"]["H+"])
        pepmass = self['Monoisotopic Mass']
        # intact_glylcopep_ions = mass.const_intact_pep_charges * (pepmass + mass.const_intact_pep_glycans) + mH
        charges = np.array(param["MASS"]["GPF"]["charge"])
        gmass = np.array(param["MASS"]["GPF"]["mass"])
        intact_glylcopep_ions = charges * (pepmass + gmass) + param["_mH"]
        return intact_glylcopep_ions, param["MASS"]["GPF"]["names"]

    def get_fragments(self, types, param, charge=2):
        mass = param["MASS"]
        # fragments = {"b": [[],[]]}
        if self["Fragments"]:
            return self.x["Fragments"]
        fragments = {}
        names = []
        ions = []
        peplen = len(self.x["Sequence"])
        residues_mass = [mass["AA"][i] for i in self.x["Sequence"]]
        for k in self["Mods"]:
            v = self["Mods"][k]
            if k == "n":
                residues_mass[0] += mass["MOD"][v]
            elif k == "c":
                residues_mass[-1] += mass["MOD"][v]
            else:
                residues_mass[k - 1] += mass["MOD"][v]

        # for i in range(peplen):
        #     residues_mass[i] += self.get_token_monoisotopic_mass(i)
        # types = list("bycz")
        for t in types:
            if t == "b":
                fragments[t] = [np.array([np.sum(residues_mass[0:i]) for i in range(1, peplen + 1)]) + param["_b_mz"]]
            elif t == "y":
                fragments[t] = [
                    np.array([np.sum(residues_mass[peplen - i:]) for i in range(1, peplen + 1)]) + param["_y_mz"]]
            elif t == "c":
                fragments[t] = [np.array([np.sum(residues_mass[0:i]) for i in range(1, peplen + 1)]) + param["_c_mz"]]
            elif t == "z":
                fragments[t] = [
                    np.array([np.sum(residues_mass[peplen - i:]) for i in range(1, peplen + 1)]) + param["_z_mz"]]
        for t in fragments:
            for i in range(2, charge + 1):
                x = fragments[t][0]
                x = (x + (i - 1) * param["_mH"]) / i
                fragments[t].append(x)

        for t in sorted(types):
            for i in range(len(fragments[t])):
                ions += list(fragments[t][i])
                if i == 0:
                    names += ["{}{}".format(t, j + 1) for j in range(len(fragments[t][i]))]
                else:
                    c = i + 1
                    names += ["{}{}+{}".format(t, j + 1, c) for j in range(len(fragments[t][i]))]
        self.x["Fragments"] = [ions, names]
        return ions, names

    # def make_fragments(self, charge=3, force=False):  #
    #     # if len(self.x["Fragments"]) > 0 and (not force):
    #     #     return self["Fragments"]
    #     # if len(self['Fragments']) > 0 and (not force):
    #     #     return self['Fragments']
    #     # mH = float(mass["MIC"]["H+"])
    #     names = []
    #     ions = []
    #     peptide_length = len(self.x["Sequence"])
    #     frag_type_count = len(list("by"))
    #     x = 0
    #     ions = np.zeros((peptide_length - x) * (charge - 1) * frag_type_count, dtype=float)
    #     precursor_mass = self.x["Monoisotopic Mass"]
    #     ions[0] = self.get_nterm_monoisotopic_mass() + self.get_token_monoisotopic_mass(0)
    #     # b+1 ions
    #     for i in range(1, peptide_length - x):
    #         ions[i] = ions[i - 1] + self.get_token_monoisotopic_mass(i)
    #     rb = ions[0:(peptide_length - x)][::-1]
    #     # print(self.x["Fragments"][0:(peptide_length - x)])
    #     # print(rb)
    #     # print(precursor_mass)
    #     names += ["b{0}".format(i) for i in range(1, peptide_length - x + 1)]
    #     # y+1 ions
    #     ions[peptide_length - x:2 * (peptide_length - x)] = precursor_mass - rb + 2 * mH
    #     if x == 0:
    #         start = peptide_length - x
    #         end = 2 * (peptide_length - x)
    #         ions[start:end - 1] = ions[start + 1: end]
    #         ions[2 * (peptide_length - x) - 1] = self.x["Monoisotopic Mass"] + mH
    #     names += ["y{0}".format(i) for i in range(1, peptide_length - x + 1)]
    #     # b+c, y+c ions
    #     for c in range(2, charge):
    #         b_start = (c - 1) * 2 * (peptide_length - x)
    #         ions[b_start:b_start + (peptide_length - x)] = (ions[0:peptide_length - x] + (
    #             c - 1) * mH) / c
    #         names += ["b{0}^{1}".format(i, c) for i in range(1, peptide_length - x + 1)]
    #         y_start = b_start + (peptide_length - x)
    #         ions[y_start:y_start + (peptide_length - x)] = (ions[
    #                                                         (peptide_length - x):2 * (
    #                                                             peptide_length - x)] + (
    #                                                             c - 1) * mH) / c
    #         names += ["y{0}^{1}".format(i, c) for i in range(1, peptide_length - x + 1)]
    #     self.x["Fragments"] = [ions, names]

    def get_nterm_monoisotopic_mass(self, param):
        mass = param["MASS"]
        nterm_mass = float(mass["MIC"]['nterm'])
        if "n" in self["Mods"].keys():
            mod = self.x["Mods"]["n"]
            nterm_mass += float(mass["MOD"][mod])
        return nterm_mass

    def get_cterm_monoisotopic_mass(self, param):
        mass = param["MASS"]
        cterm_mass = float(mass["MIC"]["cterm"])
        if "c" in self["Mods"].keys():
            mod = self.x["Mods"]["c"]
            cterm_mass += float(mass["MOD"][mod])
        return cterm_mass

    def get_token_monoisotopic_mass(self, i, param):
        mass = param["MASS"]
        aa = self["Sequence"][i]
        token_mass = float(mass['AA'][aa])
        if i + 1 in self["Mods"]:
            mod = self["Mods"][i + 1]
            token_mass += float(mass["MOD"][mod])
        return token_mass

    def calc_monoisotopic_mass(self, param):
        mass = param["MASS"]
        try:
            # pepmass = self.get_nterm_monoisotopic_mass()
            # for i in range(len(self.sequence)):
            #     pepmass += self.get_aa_monoisotopic_mass(i)
            # pepmass += self.get_cterm_monoisotopic_mass()
            # return pepmass
            m = 0
            for i in list(self["Sequence"]):
                m += float(mass["AA"][i])
                # if i == "C":
                #     m += mass.mod_monoisotopic_mass['Carbamidomethyl']
            m += float(mass["MIC"]['nterm'])
            m += float(mass["MIC"]['cterm'])
            for i in self["Mods"].keys():
                mod = self["Mods"][i]
                m += float(mass["MOD"][mod])
            self["Monoisotopic Mass"] = m
            return m
        except KeyError as e:
            m = re.search("'([^']*)'", e.message)
            print("ERROR! Peptide Calculate Mass: {0},{1}".format(e.message, self["Sequence"]))
            if m:
                key = m.group(1)
                sys.stderr.write('Error!Fail to find mass of amino acid: {0:s}\n'.format(key))
            sys.exit(1)


class LogExceptions(object):
    def __init__(self, callable):
        self.__callable = callable

    def __call__(self, *args, **kwargs):
        try:
            result = self.__callable(*args, **kwargs)

        except Exception as e:
            # Here we add some debugging help. If multiprocessing's
            # debugging is on, it will arrange to log the traceback
            error(traceback.format_exc())
            # Re-raise the original exception so the Pool worker can
            # clean up
            raise

        # It was fine, give a normal answer
        return result


class LoggingPool(Pool):
    def apply_async(self, func, args=(), kwds={}, callback=None):
        return Pool.apply_async(self, LogExceptions(func), args, kwds, callback)


# Shortcut to multiprocessing's logger
def error(msg, *args):
    return multiprocessing.get_logger().error(msg, *args)


def compare(d):
    g = 0
    for i in d.keys():
        g += 1
    return g


class GPQuest3Result:
    def __init__(self, spectrum, score, param):
        mass = param["MASS"]
        # print(score['Matching']['Mod']['Mods'])
        self.x = {
            'MS1': spectrum['ms1scan'],
            'MS2': spectrum['scan'],
            'RT': "{0:.5f}".format(spectrum['rt']),
            'Precursor Charge': spectrum['precursor']['charge'],
            'Precursor MZ': spectrum['precursor']['mz'],
            'Precursor Mass': "{0:.4f}".format(spectrum.calculate_precursor_mass()),
            'Mass Shift': 0,
            'Adjusted Isotope': 0,
            'TIC': spectrum['TIC'],
            'RTIC': spectrum['RTIC'],
            'Oxonium Peaks': spectrum.get_oxo_peaksnumber(),
            'Oxonium Intensity': spectrum.get_oxo_intensity(),
            # 'Oxonium IR': spectrum.get_oxo_intensity() / spectrum['TIC'],
            'Oxonium IR': spectrum['oxo']['ratio'],
            'Oxonium Ions': ";".join(
                ["{0:s}/{1:.2f}/{2:.4f}".format(i, mass["OXONIUM_IONS"][i], spectrum['oxo'][i]) for i in
                 param["_oxoall"]]),
            'Reporter Peaks': spectrum.get_reporters_peaksnumber(),
            'Reporter Intensity': spectrum.get_reporters_intensity(),
            'Reporter IR': spectrum.get_reporters_intensity() / spectrum['TIC'],
            'Reporters': spectrum.get_reporters_str(),
            'PID': score['Candidate']['Peptide']['PID'],
            'Protein Accession': score['Candidate']['Peptide']['Protein Accession'],
            'Protein Name': score['Candidate']['Peptide']['Protein Name'],
            'Sequence': score['Candidate']['Peptide']['Sequence'],
            'Sequence Mass': score['Candidate']['Peptide']['Monoisotopic Mass'],
            'Peptide': score['Candidate']['Peptide'].get_full_name(),
            'Ion Index Number': score['Ion Index']['Number'],
            'Sequence Peaks': score['Matching']['Peptide']['PeaksNumber'],
            'Sequence Intensity': score['Matching']['Peptide']['Intensity'],
            'Sequence IR': "{0:.2f}".format(score['Matching']['Peptide']['IRatio']),
            # 'Sequence Fragments': ",".join(score['Matching']['Peptide']['Ions']),
            "Sequence Fragments": score['Matching']['Peptide']['Matches'],
            'GP Peaks': score['Matching']['GP']['PeaksNumber'],
            'GP Intensity': score['Matching']['GP']['Intensity'],
            'GP IR': "{0:.2f}".format(score['Matching']['GP']['IRatio']),
            'GP Ions': score['Matching']['GP']["Matches"],
            'Predicted Glycan Type': spectrum['glycan type'],
            'Predicted Glycan Prob': spectrum['glycan prob'],
            # 'Glycan Type': score['Candidate']['Glycan']['Type'],
            # 'Glycan Mass': score['Candidate']['Glycan']['Mass'],
            # 'Glycan': score['Candidate']['Glycan']['Name'],
            # 'Other Glycans': score['Candidate']['Potential Glycans'],
            # 'Theo Glycans': score['Candidate']['Theo Glycans'],
            'Delta Mass': score['Matching']['Mod']['Mass'],
            'Mod Mass': score["Matching"]["Mod"]["NGLYCAN MASS"],
            "Mod Mass Shift": score['Matching']['Mod']["Shift"],
            'Mod': "NA",
            "NGLYCAN": score["Matching"]["Mod"]["NGLYCAN"],
            "NGLYCANS": score["Matching"]["Mod"]["NGLYCANS"],
            "OGLYCAN": score["Matching"]["Mod"]["OGLYCAN"],
            "OGLYCANS": score["Matching"]["Mod"]["OGLYCANS"],
            'Mod Prob': "NA",
            'Intact GP': "NA",
            'Status': score['Candidate']['Status'],
            'IR Score': score["Matching"]["Scores"]['Total IRatio'],
            'Morpheus Score': score["Matching"]["Scores"]['Morpheus'],
            'Hyper Score': score["Matching"]["Peptide"]["hyperscore"],
        }
        if len(score['Matching']['Mod']['Mods']) > 0:
            self['Mod Mass'] = score['Matching']['Mod']['Mods'][0][2]
            self['Mod Mass Shift'] = self['Mod Mass'] - self["Delta Mass"]
            self['Mod'] = score['Matching']['Mod']['Mods'][0][0]
            self['Mod Prob'] = "NA"
        if self['Mod'] != "NA":
            self['Intact GP'] = "{}+{}".format(
                self['Sequence'], self['Mod']
            )
        z = np.where(spectrum['mask'] == True)
        self.x["Annotation"] = {}
        match_ann = score['Matching']["Annotation"]

        for i in spectrum["ann"]:
            self.x["Annotation"][i] = spectrum["ann"][i]

        for i in match_ann:
            self.x["Annotation"][z[0][i]] = match_ann[i]

        self.x['mz'] = np.copy(spectrum['mz'])
        self.x['i'] = np.copy(spectrum['i'])

    def __getitem__(self, item):
        try:
            return self.x[item]
        except KeyError:
            return None

    def __setitem__(self, key, value):
        self.x[key] = value

    @staticmethod
    def get_keys():
        return [
            'MS1',
            'MS2',
            'RT',
            'Precursor Charge',
            'Precursor MZ',
            'Precursor Mass',
            'Mass Shift',
            'Adjusted Isotope',
            'TIC',
            'RTIC',
            'Oxonium Peaks',
            'Oxonium Intensity',
            'Oxonium IR',
            'Oxonium Ions',
            'Reporter Peaks',
            'Reporter Intensity',
            'Reporter IR',
            'Reporters',
            'PID',
            'Protein Accession',
            'Protein Name',
            'Sequence',
            'Peptide',
            'Sequence Mass',
            'Ion Index Number',
            'Sequence Peaks',
            'Sequence Intensity',
            'Sequence IR',
            'Sequence Fragments',
            'GP Peaks',
            'GP Intensity',
            'GP IR',
            'GP Ions',
            'Predicted Glycan Type',
            'Predicted Glycan Prob',
            # 'Glycan Type',
            # 'Glycan Mass',
            # 'Glycan',
            # 'Other Glycans',
            # 'Theo Glycans',
            'Delta Mass',
            'Mod Mass',
            "Mod Mass Shift",
            'Mod',
            "NGLYCAN",
            "NGLYCANS",
            "OGLYCAN",
            "OGLYCANS",
            'Mod Prob',
            'Intact GP',
            'Status',
            'IR Score',
            'Morpheus Score',
            "Hyper Score"
        ]


def search(spectra, param, ion_index, peptides, peps, nglycans, oglycans):
    rd = []
    best = 0
    found = 0
    factor = int(param["DATABASE"]["ION_INDEX"]["FACTOR"])
    min_ions = int(param["DATABASE"]["ION_INDEX"]["MIN_IONS"])

    # print(len(spectra))
    for i in tqdm(range(len(spectra))):
        spectrum = spectra[i]
        # if int(spectrum['scan']) == 7540:
        #     print('OXO...........................')
        #     print(spectrum['oxo'])
        #     print('Reporters............................')
        #     print(spectrum['reporters'])
        #     # sys.exit(1)
        # elif int(spectrum['scan']) > 7540:
        #     sys.exit((1))
        # else:
        #     continue

        spectrum["mz_arr"] = np.array(spectrum['mz'][spectrum['mask']])
        spectrum["i_arr"] = np.array(spectrum['i'][spectrum['mask']])

        # print(len(spectrum['mz_arr']))
        # print(len(spectrum['i_arr']))
        # with open('C:/Users/yhu39/Documents/temp/7528.mgf','w') as f:
        #     for i,j in zip (spectrum['mz_arr'],spectrum['i_arr']):
        #         f.write('{0}\t{1}\n'.format(i,j))
        #     f.close()
        # print(xnames)
        # for i in range(len(spectrum['mz'])):
        #     print("MS2,{},{}".format(spectrum['mz'][i],spectrum['i'][i]))
        # print(spectrum['scan'])
        # if i % 1000 == 0:
        #     print("Processed {}".format(i))

        # find candidates
        candidates = {}
        fac_mz_arr = np.array(np.array(spectrum["mz_arr"]) * factor, dtype=int)
        # x = np.array(fac_mz_arr, dtype=int)
        for fac_mz in fac_mz_arr:
            # print("fmz,{0}".format(i))
            try:
                pepids = ion_index[int(fac_mz)]
                # if i in [7183,14357]:
                #     print('pepids',i,[peps[k] for k in pepids])
            except KeyError:
                continue
            for pid in pepids:
                try:
                    candidates[pid] += 1
                except KeyError:
                    candidates[pid] = 1
        selection = []
        # print(["{},{}".format(peps[pid], candidates[pid]) for pid in candidates])
        # f = open("I:\\test.log", "w")
        # for pid in candidates:
        #     f.write("candidate,{},{}".format(peps[pid], candidates[pid]))
        #     f.write("\n")
        # f.close()
        for pid in candidates:
            if candidates[pid] >= min_ions:
                selection.append([pid, candidates[pid]])

        # print(selection)

        if len(selection) == 0:
            continue

        # log_info("{},{}".format(spectrum['scan'],
        #                         ";".join(["{}:{}".format(c[0], c[1]) for c in selection])))

        matched = False
        r = []
        spectrum["i_arr"] = np.sqrt(spectrum["i_arr"])
        spectrum['RTIC_SQRT_I'] = np.sum(spectrum["i_arr"])
        # print(spectrum['scan'])
        # if int(spectrum['scan']) == 24009:
        #     print(selection)
        # sys.exit(1)
        for pid, ion_index_number in selection:
            peptide = peps[pid]
            # print(peptide)
            # if peptide == "EEQFNSTFR":
            #     print("OK")
            peptide = Peptide(peptide, param)
            m = spectrum.match(peptide, param, nglycans=nglycans, oglycans=oglycans)
            # if int(spectrum['scan']) == 24009:
            #     print(peptide.__dict__)
            #     if m:
            #         print(m.__dict__)
            #     else:
            #         print(m)
            # sys.exit(1)
            if not m:
                continue
            # if m['GP']['PeaksNumber'] == 0:
            if m['Peptide']['PeaksNumber'] < np.min([min_ions, 0.5 * peptide["length"]]):
                continue
            if 1 < peptide["length"] <= 5:
                if m['GP']['PeaksNumber'] < 3:
                    continue
            elif 6 <= peptide["length"] <= 10:
                if m['GP']['PeaksNumber'] < 2:
                    continue
            elif 11 <= peptide["length"] <= 15:
                if m['GP']['PeaksNumber'] < 1:
                    continue
            if m['Peptide']['IRatio'] + m['GP']['IRatio'] < 0.1:
                continue

            peptide['PID'] = pid
            #         print('pid',pid)
            c = Candidate(peptide=peptide)
            if pid > len(peps) / 2 - 1:
                c['status'] = "DECOY"

            # 20200531
            if m['Mod']['NGLYCAN'] == 'NA':
                continue
            r.append(GPQuest3Score(candidate=c, matching=m))
            # if int(spectrum['scan']) in [19328,24009,36344]:
            #     print('ADD {0},{1}'.format(pid,c['status']))
            #     print(peptide.__dict__)
        if len(r) == 0:
            continue

        sr = sorted(r, key=lambda x: x['Matching']["Scores"]["Hyper Score"], reverse=True)
        # if int(spectrum['scan']) in [19328,24009,36344]:
        #     print(">>>>>>>".format(spectrum['scan']))
        #     for i in sr:
        #         print(i['Matching'].__dict__)
        #         print(i['Candidate']['Peptide'].__dict__)
        #     print("@@@@")
        #     print(sr[0]['Matching'].__dict__)
        #     print(sr[0]['Candidate']['Peptide'].__dict__)
        # sys.exit(1)
        # TODO recover
        # param["SEARCH"]["KEEP"] = "ALL"
        if param["SEARCH"]["KEEP"] == "ALL":
            pass
        elif param["SEARCH"]["KEEP"] == "NGLYCANS":
            if sr[0]["Matching"]['Mod']['NGLYCAN'] == "NA":
                continue
        elif param["SEARCH"]["KEEP"] == "OGLYCANS":
            if sr[0]["Matching"]['Mod']['OGLYCAN'] == "NA":
                continue
        elif param["SEARCH"]["KEEP"] == "ALL_GLYCANS":
            if sr[0]["Matching"]['Mod']['NGLYCAN'] == "NA" and sr[0]["Matching"]['Mod']['OGLYCAN'] == "NA":
                continue
        # for i in range(len(sr)):
        for j in range(1):
            pid = sr[j]['Candidate']['Peptide']['PID']
            if pid > len(peps) / 2 - 1:
                sr[j]['Candidate']['Peptide']['Protein Accession'] = "DECOY"
                sr[j]['Candidate']['Peptide']['Protein Name'] = "DECOY"
            else:
                sr[j]['Candidate']['Peptide']['Protein Accession'] = ""
                sr[j]['Candidate']['Peptide']['Protein Name'] = ""
                seq = peps[pid].upper()
                proteins = peptides[seq]
                # print(proteins)
                for protac, protname, start in proteins:
                    # prot = param["SPACE"]["PROTEINS_MAP"][protac]
                    prot = protac
                    if not prot:
                        sr[j]['Candidate']['Peptide']['Protein Accession'] += "NA;"
                        sr[j]['Candidate']['Peptide']['Protein Name'] += "NA;"
                        continue
                    # protname = prot["name"]
                    # protname = protac
                    try:
                        sr[j]['Candidate']['Peptide']['Protein Accession'] += protac + ";"
                    except:
                        # print(protac)
                        # print(protname)
                        sr[j]['Candidate']['Peptide']['Protein Accession'] += str(protac) + ";"
                    sr[j]['Candidate']['Peptide']['Protein Name'] += protname + ";"

            result = GPQuest3Result(spectrum, sr[j], param)
            # if int(spectrum['scan']) == 24009:
            #     print(result.__dict__)
            #     sys.exit(1)
            # rd.append([result[c] for c in result.get_keys()])
            rd.append(result)

        found += 1
    print("found", found)
    print("spectra", len(spectra))
    return rd


def save_to_csv(filename, lines):
    df = pd.DataFrame(lines)
    df.to_csv(filename, index=False)


def main(input_files=None, output_dir=None, param_file=None):
    freeze_support()
    log_file_path = output_dir + os.sep + "GPQuest3.log"
    logging.basicConfig(filename=log_file_path, level=logging.DEBUG)
    param = read_param(param_file, input=input_files, output=output_dir)
    # N-glycan
    nglycan_path = param["DATABASE"]["PATH"]["NGLYCAN"]
    new_nglycan_path = os.path.splitext(nglycan_path)[0] + "_temp.csv"
    try:
        nglycan_sheet_name = param["DATABASE"]["PATH"]["NGLYCAN_SHEET_NAME"]
    except KeyError:
        nglycan_sheet_name = "Sheet1"

    nglycans = glycans_from_xlsx(nglycan_path, nglycan_sheet_name)
    nglycans = add_glycan_mods(nglycans, param)
    nglycans = build_glycan_database(nglycans, param)

    new_nglycan_df = pd.DataFrame(nglycans, columns=["Composition", "Residue Mass"])
    new_nglycan_df.to_csv(new_nglycan_path, index=False)
    log_info("Preparation: Save unique nglycans: {0} in Path: {1}!".format(
        len(nglycans), new_nglycan_path))

    # O-glycan
    oglycan_path = param["DATABASE"]["PATH"]["OGLYCAN"]
    new_oglycan_path = os.path.splitext(oglycan_path)[0] + "_temp.csv"
    try:
        oglycan_sheet_name = param["DATABASE"]["PATH"]["OGLYCAN_SHEET_NAME"]
    except KeyError:
        oglycan_sheet_name = "Sheet1"
    oglycans = glycans_from_xlsx(oglycan_path, oglycan_sheet_name)
    oglycans = add_glycan_mods(oglycans, param)
    oglycans = build_glycan_database(oglycans, param)

    new_oglycan_df = pd.DataFrame(oglycans, columns=["Composition", "Residue Mass"])
    new_oglycan_df.to_csv(new_oglycan_path, index=False)
    log_info("Preparation: Save unique oglycans: {0} in Path: {1}!".format(
        len(oglycans), new_oglycan_path))

    peptides = []
    pdb = param["DATABASE"]["PATH"]["PROTEIN"]
    fn, ext = os.path.splitext(pdb)
    if ext == ".xlsx":
        print("Read Peptides from {}".format(pdb))
        peptides = peptides_from_xlsx(pdb)
    elif ext == ".csv":
        print("Read Peptides from {}".format(pdb))
        peptides = peptides_from_xlsx(pdb, ext=".csv")
    elif ext == ".fasta":
        peptides = peptides_from_fasta(pdb)

    # make decoys
    targets = sorted(peptides.keys())
    targets = add_mods(targets, param)
    # save_to_csv("I:\\targets.csv", targets)
    # print("Save to CSV: I:\\targets.xlsx")
    if ext == ".xlsx":
        decoy_file_path = re.sub("\.xlsx", "_decoys.csv", pdb)
    elif ext == ".csv" or ext == ".fasta":
        decoy_file_path = re.sub("\.csv", "_decoys.csv", pdb)
    decoys = []
    if os.path.isfile(decoy_file_path):
        decoys_df = pd.read_csv(decoy_file_path)
        decoys = list(decoys_df.iloc[:, 0])
    else:
        decoys = make_decoys(targets)
        save_to_csv(decoy_file_path, decoys)
        print("Save DECOYs of {} to {}".format(pdb, decoy_file_path))

    new_decoy_df = pd.DataFrame(decoys)
    new_decoy_df['status'] = 'DECOY'
    new_target_df = pd.DataFrame(targets)
    new_target_df['status'] = 'NORMAL'
    new_target_decoy_df = pd.concat([new_target_df, new_decoy_df])

    target_decoy_file_path = re.sub('_decoys.csv', '_target_decoys.txt', decoy_file_path)
    if not os.path.exists(target_decoy_file_path):
        new_target_decoy_df.to_csv(target_decoy_file_path, index=False, sep="\t")

    # skips the database searches if any of the main() function parameters are missing
    # this is used to create the decoy databases if needed and not search any input files
    if input_files != None and output_dir != None and param_file != None:
        peps = targets + decoys
    
        n = int(8)
        log_info("Create {0:d} Workers in Pool".format(n))
    
        multiprocessing.log_to_stderr()
        logger = multiprocessing.get_logger()
        logger.setLevel(logging.ERROR)
    
        ion_index = build_ion_index(peps, param)
        print("Ion Index {}".format(len(ion_index.keys())))
    
        nglycan_dict = {}
        for g in nglycans:
            m = int(g[1])
            try:
                nglycan_dict[m].append(g)
            except KeyError:
                nglycan_dict[m] = [g]
    
        oglycan_dict = {}
        for g in oglycans:
            m = int(g[1])
            try:
                oglycan_dict[m].append(g)
            except KeyError:
                oglycan_dict[m] = [g]
    
        def get_iso_peaks(path, out_dir, force=False):
            from hotpot.gpquest_utils import mzml2spectra, calc_isotope_peaks
            
            folder, fn = os.path.split(path)
            bn, ext = os.path.splitext(fn)
            out_path = out_dir + os.sep + bn + "_iso.txt"
            if (not os.path.exists(out_path)) or force:
                df = mzml2spectra(path, n_cores=4, to_str=False)
                df3 = calc_isotope_peaks(df, n_cores=4)
                df3['mz_array'] = [','.join(['{:.4f}'.format(k) for k in i]) for i in tqdm(df3['mz_array'])]
                df3['iso_mz_array'] = [','.join(['{:.4f}'.format(k) for k in i]) for i in tqdm(df3['iso_mz_array'])]
                df3['iso_i_array'] = [','.join(['{:.4f}'.format(k) for k in i]) for i in tqdm(df3['iso_i_array'])]
                df3['intensity_array'] = [','.join(['{:.4f}'.format(k) for k in i]) for i in tqdm(df3['intensity_array'])]
                df3.to_csv(out_path, sep="\t", index=False)
            return out_path
    
        force_renew_iso = bool(param['SEARCH']['FORCE_RENEW'])
        for i in param['INPUT']['FILENAMES']:
            print("Searching {}".format(i))
            bn, ext = os.path.splitext(i)
            filename = os.path.split(bn)[-1]
            spectra_iso_path = get_iso_peaks(i, out_dir=output_dir, force=force_renew_iso)
    
            # decide cores number
            cores = cpu_count() - 2
            if cores < 2:
                cores = 2
    
            spectra = preprocess_msfile(i, param)
    
            results = search(spectra, param, ion_index, peptides, peps, nglycan_dict, oglycan_dict)
    
            rs = [[r[c] for c in r.get_keys()] for r in results]
            result_dataframe = pd.DataFrame(rs, columns=GPQuest3Result.get_keys())
            result_dataframe = result_dataframe.sort_values(by=['Morpheus Score'], ascending=[False])
            ndecoy = 0
            total = 0
            fdrs = []
            for index, row in result_dataframe.iterrows():
                total += 1
                if row['Protein Accession'] == "DECOY":
                    ndecoy += 1
                fdrs.append("{0:.4f}".format(float(ndecoy) / (total - ndecoy)))
            # 20200531
            for fi in range(len(fdrs))[::-1]:
                fj = fi + 1
                if fj < len(fdrs):
                    if fdrs[fj] < fdrs[fi]:
                        fdrs[fi] = fdrs[fj]
            result_dataframe["FDR"] = fdrs
            result_file = param["OUTPUT"]["PATH"] + os.sep + "{}.csv".format(filename)
            # result_dataframe.to_csv(result_file, index=False)
            
            from hotpot.gpquest_utils import refine_result
            
            result_dataframe = refine_result(result_dataframe,
                                            spectra_iso_df=pd.read_csv(spectra_iso_path, sep="\t"),
                                            min_isotope_score=0.9,
                                            min_glycan_priority=0,
                                            min_monoisotope_ratio=0.05
                                            )
            result_dataframe.to_csv(result_file, index=False)
            print("DONE!Save result to {0}!".format(result_file))
            if param["SEARCH"]["SAVE_SPLIB"] == "YES":
                splib_file = param["OUTPUT"]["PATH"] + os.sep + "{}.sptxt".format(filename)
                with open(splib_file, "w") as f:
                    for r in results:
                        f.write("SCAN:" + str(r['MS2']) + "\n")
                        mz_arr = r['mz']
                        i_arr = r['i']
                        pkl_len = r['mz'].shape[0]
                        ann = r['Annotation']
                        for p in range(pkl_len):
                            f.write("{0:.4f}\t{1:.2f}\t{2:s}\n".format(mz_arr[p], i_arr[p], ann.get(p, "")))
                        f.write("\n")
                f.close()
    
        log_info("DONE!")
        # logging.shutdown()
    
    print("DONE!")

if __name__ == "__main__":
    freeze_support()
    ## input_files = [
    ##     "C:\\Users\\husin\\Desktop\\ganglong\\Ganglong_CHO_Medium_Global_05262017.mzML"
    ## ]
    ## output_dir = "C:\\Users\\husin\\Desktop\\ganglong\\out"
    ## param_file = "C:\\Users\\husin\\Desktop\\ganglong\\glycositesatlas_iTRAQ_GPQuest3Test_Ganglong.json"
    ## wd_mzml = "G:\\CPTAC2\\TCGA\\data\\intact_glycopeptides\\20170324\\mzml"
    #wd_mzml = "E:\\TCGA\\data"
    #dirs = os.listdir(wd_mzml)
    #input_files = []
    #for d in dirs:
    #    if re.search("^JH_TCGA.*\.mzML", d):
    #        input_files.append(wd_mzml + os.sep + d)
    ## input_files = [
    ## "E:\\data\\Weiming\\3GProtein\\mzml\\09292015_WY_FETUIN_OLINK.mzML",
    ## "E:\\data\\Weiming\\3GProtein\\mzml\\09292015_WY_GLYCOPH_OLINK.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\01_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\01_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\01_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\02_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\02_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\02_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\03_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\03_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\03_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\04_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\04_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\04_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\05_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\05_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\05_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\06_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\06_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\06_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\07_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R01.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\07_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R02.mzML",
    ## "J:\\NCI60\\60_cell_lines\\IntactGlycopeptide\\mzml\\07_NCI60_IntGlyco_JHUZ_20170603_LUMOS_R03.mzML"
    ## ]
    ## output_dir = "J:\\NCI60\\60_cell_lines\\YHU\\GPQuest\\out2017080901"
    ## param_file = "J:\\NCI60\\60_cell_lines\\YHU\\GPQuest\\param\\SEPG_tmt10_GPQuest3S.json"
    #output_dir = wd_mzml + os.sep + "out"
    #param_file = "E:\\TCGA\\search\\GPQuest34_DataIntactgp20170324_Done20170820\\param" + os.sep + "SEPG_itraq4_GPQuest3S.json"
    
    input_files = [sys.argv[1]]
    output_dir = sys.argv[2]
    param_file = sys.argv[3]
    
    # handles string "None" values
    # used for handling decoy database generation
    if input_files == ['None']:
        input_files = None
    
    if output_dir == 'None':
        output_dir = None
        
    if param_file == 'None':
        param_file = None
    
    if input_files != None and output_dir != None and param_file != None:
        print('\nGPQuest is running...')
        print('-' * 150)
        sys.stdout.flush()
    
    main(input_files=input_files, output_dir=output_dir, param_file=param_file)
