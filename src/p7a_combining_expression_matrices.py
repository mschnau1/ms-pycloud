import os
import re
import sys
import glob
import time
import shutil
from math import floor
from hotpot.data_matrix import *
from hotpot.configuring import read_steps_config as steps
from hotpot.read_write import read_lines as rl

pipeline_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
config_path = pipeline_path + 'configs\\'
documentation_path = pipeline_path + 'documentation\\'

# number of header rows - defaults to 1 since this is hard coded into pipeline output
header = 1

# mod endings of filenames
mod_endings = ['ratio.' + x for x in  ['sites', 'peptides', 'proteins', 'glycosites', 'glycopeptides', 'glycoproteins', 'phosphosites', 'phosphopeptides', 'phosphoproteins', 'nglycoformsites', 'nglycoformpeptides', 'nglycoformproteins', 'oglycoformsites', 'oglycoformpeptides', 'oglycoformproteins']]
intensity_endings = ['intensity.' + x for x in ['sites', 'peptides', 'proteins', 'glycosites', 'glycopeptides', 'glycoproteins', 'phosphosites', 'phosphopeptides', 'phosphoproteins', 'nglycoformsites', 'nglycoformpeptides', 'nglycoformproteins', 'oglycoformsites', 'oglycoformpeptides', 'oglycoformproteins']]

def main(configs, filename_mod):
    print '\nStep 7a: Merging log2 ratio expression matrices together...'
    print '-' * 150
    sys.stdout.flush()
    
    if configs['backup']:
        # copies the config files over
        data_config_copy_path = configs['options']['archived_path'] + 'configs\\' + str(time.strftime("%Y%m%d-%H%M%S")) + '\\'
        if not os.path.exists(data_config_copy_path):
            shutil.copytree(config_path, data_config_copy_path + 'configs\\')
            shutil.copytree(pipeline_path + 'configs-pycloud_version\\', data_config_copy_path + 'configs-pycloud_version\\')
            #shutil.copytree(pipeline_path + 'configs-starcluster\\', data_config_copy_path + 'configs-starcluster\\') 
            
        configs['backup'] = False
    
    options = configs['options']
    sample_filenames = sorted(configs['sample filenames'])
    header_data = configs['header_expression_matrix']
    
    data_root = options['raw_data']
    output_folder_path = options['archived_path']
    data_set_name = data_root.split('\\')[-2]
    matrix_path = output_folder_path + 'step7-expression_matrix\\sets\\'
    matrix_path_virtual = output_folder_path + 'step7-expression_matrix\\virtual_reference\\sets\\'
    output_path = output_folder_path + 'step7-expression_matrix\\'
    output_path_virtual = output_folder_path + 'step7-expression_matrix\\virtual_reference\\'
    qc_path = output_folder_path + 'quality_control\\'
    label_type = options['label_type']
    mod = str(options['modification']).lower()
    localize_phosphosites = str(options['localize_phosphosites']).lower()
    # assumes an equal sign after the gene name identifier
    gene_name_identifier = str(options['gene_name_identifier']) + '='
    
    if label_type == '4plex':
        header_section = 'iTRAQ4plex'
        num_channels = 4
    elif label_type == '8plex':
        header_section = 'iTRAQ8plex'
        num_channels = 8
    elif label_type == '10plex':
        header_section = 'TMT10plex'
        num_channels = 10
    elif label_type == '11plex':
        header_section = 'TMT11plex'
        num_channels = 11
    elif label_type == '16plex':
        header_section = 'TMT16plex'
        num_channels = 16
    
    if mod == 'global':
        protein_end = 'ratio.' + filename_mod + 'proteins'
        peptide_end = 'ratio.' + filename_mod + 'peptides'
        site_end = 'ratio.' + filename_mod + 'sites'
    elif mod == 'glyco':
        protein_end = 'ratio.' + filename_mod + 'proteins'
        peptide_end = 'ratio.' + filename_mod + 'peptides'
        site_end = 'ratio.' + filename_mod + 'sites'
    elif mod == 'phospho':
        protein_end = 'ratio.' + filename_mod + 'proteins'
        peptide_end = 'ratio.' + filename_mod + 'peptides'
        site_end = 'ratio.' + filename_mod + 'sites'
    elif mod == 'intact':
        protein_end = 'ratio.' + filename_mod + 'proteins'
        peptide_end = 'ratio.' + filename_mod + 'peptides'
        site_end = 'ratio.' + filename_mod + 'sites'
    
    # handle single shot data
    matrices_all = glob.glob(matrix_path + '*.tsv')
    matrices_all_virtual = glob.glob(matrix_path_virtual + '*.tsv')
    matrices_all.sort(key = natural_keys)
    matrices_all_virtual.sort(key = natural_keys)
    if sample_filenames == list():
        for matrix_f in matrices_all:
            matrix_f_basename = os.path.basename(matrix_f).replace('.tsv', '').replace('.' + protein_end, '').replace('.' + peptide_end, '').replace('.' + site_end, '')
            
            for ratio_end in mod_endings:
                matrix_f_basename = matrix_f_basename.replace('.' + ratio_end, '')
            
            for intensity_end in intensity_endings:
                matrix_f_basename = matrix_f_basename.replace('.' + intensity_end, '')
            
            if matrix_f_basename not in sample_filenames:
                sample_filenames.append(matrix_f_basename)
                
        # removes non-unique names in case a set file was left over from previous analysis
        #remove_sample_names = set()
        #for sample_filename_1 in sample_filenames:
        #    for sample_filename_2 in sample_filenames:
        #        if sample_filename_1 != sample_filename_2:
        #            # would return -1 if sample_filename_1 was not found in sample_filename_2
        #            if sample_filename_2.find(sample_filename_1) != -1:
        #                remove_sample_names.add(sample_filename_1)
        #                break
        #
        #for remove_name in remove_sample_names:
        #    sample_filenames.remove(remove_name)
        
    label_matrices(matrices_all, protein_end, peptide_end, site_end, sample_filenames, output_path, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier)
    
    # combine virtual reference matrices
    print '\nStep 7a continued: Merging virtual reference log2 ratio expression matrices together...'
    print '-' * 150
    sys.stdout.flush()
    
    label_matrices_with_virtual_reference(matrices_all_virtual, protein_end, peptide_end, site_end, sample_filenames, output_path_virtual, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier)
    
def label_matrices_with_virtual_reference(matrices_all, protein_end, peptide_end, site_end, sample_filenames, output_path, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier): 
    filename_dash = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        filename_dash = '-'
    
    protein_matrices = []
    peptide_matrices = []
    site_matrices = []
    for matrix in matrices_all:
        matrix_basename = os.path.basename(matrix).replace('.tsv', '')
        name_end = matrix_basename.split('.')
        name_end = '.'.join(name_end[-2:])
        
        if protein_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + protein_end)[0]
            
            if matrix_sample_name in sample_filenames:
                protein_matrices.append(matrix)
                
        if peptide_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + peptide_end)[0]
            
            if matrix_sample_name in sample_filenames:
                peptide_matrices.append(matrix)
        
        if site_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + site_end)[0]
            
            if matrix_sample_name in sample_filenames:
                site_matrices.append(matrix)
    
    if protein_matrices != []:
        # protein matrix
        print 'Combining ' + filename_mod + filename_dash + 'protein expression matrices into a final ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = protein_matrices, protein_flag = True, peptide_flag = False, site_flag = False, file_end = protein_end, mod = mod, localize_phosphosites = localize_phosphosites)
        
        # generate the log2 protein matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
        first_intensity_col = 2
        
        print '\nCombining ' + filename_mod + filename_dash + 'protein expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        f = open(output_file, 'r')
        original_lines = f.readlines()
        f.close()
        
        # retrieves the column header from the protein_matrix-log2_ratios.tsv file
        column_header_names = original_lines[0].strip().split('\t')[first_intensity_col:]
        
        dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
        dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
        
        log2_medians = dm.get_medians_omit_none_values(column_header_names)
            
        dm.md_normalize_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios-MD_norm.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        print '\nCombining ' + filename_mod + filename_dash + 'protein expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        dm.mad_scale_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        # generate gene-level matrices
        # column with the protein names in the protein matrix
        protein_col = 1
        first_gene_intensity_col = 1
        
        gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
        generate_gene_level_matrices(output_file, gene_output_file, gene_name_identifier, header, protein_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)
    
    if peptide_matrices != []:
        # peptides matrix
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
    
        output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = peptide_matrices, protein_flag = False, peptide_flag = True, site_flag = False, file_end = peptide_end, mod = mod, localize_phosphosites = localize_phosphosites)
    
        # generate the log2 peptide matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
        if mod == 'phospho' and localize_phosphosites == 'true':
            first_intensity_col = 3
        elif mod == 'intact' or mod == 'glyco':
            first_intensity_col = 2
        else:
            first_intensity_col = 1
        
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
        
        f = open(output_file, 'r')
        original_lines = f.readlines()
        f.close()
        
        # retrieves the column header from the peptide_matrix-log2_ratios.tsv file
        column_header_names = original_lines[0].strip().split('\t')[first_intensity_col:]
        
        dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
        dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
        
        log2_medians = dm.get_medians_omit_none_values(column_header_names)
            
        dm.md_normalize_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios-MD_norm.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
        
        dm.mad_scale_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        # generate gene-level phospho matrices
        if mod == 'phospho' and localize_phosphosites == 'true':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_gene_intensity_col = 1
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
            generate_gene_level_phospho_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)
    
        # generate gene-level speg matrices
        if mod == 'glyco':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_gene_intensity_col = 1
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
            generate_gene_level_speg_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)
    
    if site_matrices != []:
        # sites matrix
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'site_matrix-log2_ratios.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = site_matrices, protein_flag = False, peptide_flag = False, site_flag = True, file_end = site_end, mod = mod, localize_phosphosites = localize_phosphosites)

        # generate the log2 site matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
        if mod == 'phospho' and localize_phosphosites == 'true':
            first_intensity_col = 4
        elif mod == 'intact' or mod == 'glyco':
            first_intensity_col = 2
        else:
            first_intensity_col = 1
        
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        f = open(output_file, 'r')
        original_lines = f.readlines()
        f.close()
        
        # retrieves the column header from the site_matrix-log2_ratios.tsv file
        column_header_names = original_lines[0].strip().split('\t')[first_intensity_col:]
        
        dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
        dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
        
        log2_medians = dm.get_medians_omit_none_values(column_header_names)
            
        dm.md_normalize_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'site_matrix-log2_ratios-MD_norm.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        dm.mad_scale_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'site_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        # generate site localization statistics and gene-level phospho matrices
        #if mod == 'phospho' and localize_phosphosites == 'true':
        #    generate_site_localization_statistics(output_path, qc_path, data_set_name)
        #    
        #    # old method
        #    ## generate gene-level matrices
        #    ## data_file columns
        #    #phosphosite_index_col = 0
        #    #gene_col = 1
        #    #first_gene_intensity_col = 1
        #    #
        #    #gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
        #    #generate_gene_level_phospho_matrices_from_phosphosites(output_file, gene_output_file, header, phosphosite_index_col, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)

    expression_sets_filename = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        expression_sets_filename = filename_mod + filename_dash + 'expression_matrix_sets-log2_ratios.log'
    else:
        expression_sets_filename = 'expression_matrix_sets-log2_ratios.log'
        
    f = open(output_path + expression_sets_filename, 'w')
    
    f.write('Protein matrix set files included in the combined protein log2 ratio matrix -\n')
    for protein_matrix in protein_matrices:
        f.write('\t' + str(os.path.basename(protein_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Peptide matrix set files included in the combined peptide log2 ratio matrix -\n')
    for peptide_matrix in peptide_matrices:
        f.write('\t' + str(os.path.basename(peptide_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Site matrix set files included in the combined site log2 ratio matrix -\n')
    for site_matrix in site_matrices:
        f.write('\t' + str(os.path.basename(site_matrix).replace('.tsv', '')) + '\n')
    
    f.close()

def label_matrices(matrices_all, protein_end, peptide_end, site_end, sample_filenames, output_path, qc_path, data_set_name, header_section, num_channels, filename_mod, header_data, mod, localize_phosphosites, gene_name_identifier): 
    filename_dash = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        filename_dash = '-'
    
    protein_matrices = []
    peptide_matrices = []
    site_matrices = []
    for matrix in matrices_all:
        matrix_basename = os.path.basename(matrix).replace('.tsv', '')
        name_end = matrix_basename.split('.')
        name_end = '.'.join(name_end[-2:])
        
        if protein_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + protein_end)[0]
            
            if matrix_sample_name in sample_filenames:
                protein_matrices.append(matrix)
                
        if peptide_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + peptide_end)[0]
            
            if matrix_sample_name in sample_filenames:
                peptide_matrices.append(matrix)
        
        if site_end == name_end:
            matrix_sample_name = matrix_basename.split('.' + site_end)[0]
            
            if matrix_sample_name in sample_filenames:
                site_matrices.append(matrix)
    
    if protein_matrices != []:
        # protein matrix
        print 'Combining ' + filename_mod + filename_dash + 'protein expression matrices into a final ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = protein_matrices, protein_flag = True, peptide_flag = False, site_flag = False, file_end = protein_end, mod = mod, localize_phosphosites = localize_phosphosites)
        
        # generate the log2 protein matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
        first_intensity_col = 2
        
        print '\nCombining ' + filename_mod + filename_dash + 'protein expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        f = open(output_file, 'r')
        original_lines = f.readlines()
        f.close()
        
        # retrieves the column header from the protein_matrix-log2_ratios.tsv file
        column_header_names = original_lines[0].strip().split('\t')[first_intensity_col:]
        
        dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
        dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
        
        log2_medians = dm.get_medians_omit_none_values(column_header_names)
            
        dm.md_normalize_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios-MD_norm.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        print '\nCombining ' + filename_mod + filename_dash + 'protein expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'protein matrix...'
        print '-'
        sys.stdout.flush()
        
        dm.mad_scale_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'protein_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        # generate gene-level matrices
        # column with the protein names in the protein matrix
        protein_col = 1
        first_gene_intensity_col = 1
        
        gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
        generate_gene_level_matrices(output_file, gene_output_file, gene_name_identifier, header, protein_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)
    
    if peptide_matrices != []:
        # peptides matrix
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
    
        output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = peptide_matrices, protein_flag = False, peptide_flag = True, site_flag = False, file_end = peptide_end, mod = mod, localize_phosphosites = localize_phosphosites)
    
        # generate the log2 peptide matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
        if mod == 'phospho' and localize_phosphosites == 'true':
            first_intensity_col = 3
        elif mod == 'intact' or mod == 'glyco':
            first_intensity_col = 2
        else:
            first_intensity_col = 1
        
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
        
        f = open(output_file, 'r')
        original_lines = f.readlines()
        f.close()
        
        # retrieves the column header from the peptide_matrix-log2_ratios.tsv file
        column_header_names = original_lines[0].strip().split('\t')[first_intensity_col:]
        
        dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
        dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
        
        log2_medians = dm.get_medians_omit_none_values(column_header_names)
            
        dm.md_normalize_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios-MD_norm.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        print '\nCombining ' + filename_mod + filename_dash + 'peptide expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'peptide matrix...'
        print '-'
        sys.stdout.flush()
        
        dm.mad_scale_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'peptide_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        # generate gene-level phospho matrices
        if mod == 'phospho' and localize_phosphosites == 'true':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_gene_intensity_col = 1
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
            generate_gene_level_phospho_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)
    
        # generate gene-level speg matrices
        if mod == 'glyco':
            # generate gene-level matrices
            # data_file columns
            gene_col = 1
            first_gene_intensity_col = 1
            
            gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
            generate_gene_level_speg_matrices(output_file, gene_output_file, header, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)
    
    if site_matrices != []:
        # sites matrix
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        output_file = output_path + filename_mod + filename_dash + 'site_matrix-log2_ratios.tsv'
        combine_expression_matrices(header_section = header_section, header_data = header_data, num_channels = num_channels, output_file = output_file, matrices = site_matrices, protein_flag = False, peptide_flag = False, site_flag = True, file_end = site_end, mod = mod, localize_phosphosites = localize_phosphosites)

        # generate the log2 site matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
        if mod == 'phospho' and localize_phosphosites == 'true':
            first_intensity_col = 4
        elif mod == 'intact' or mod == 'glyco':
            first_intensity_col = 2
        else:
            first_intensity_col = 1
        
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        f = open(output_file, 'r')
        original_lines = f.readlines()
        f.close()
        
        # retrieves the column header from the site_matrix-log2_ratios.tsv file
        column_header_names = original_lines[0].strip().split('\t')[first_intensity_col:]
        
        dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
        dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
        
        log2_medians = dm.get_medians_omit_none_values(column_header_names)
            
        dm.md_normalize_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'site_matrix-log2_ratios-MD_norm.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        print '\nCombining ' + filename_mod + filename_dash + 'site expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'site matrix...'
        print '-'
        sys.stdout.flush()
        
        dm.mad_scale_columns(column_header_names, log2_medians)
        
        norm_output_file = output_path + filename_mod + filename_dash + 'site_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
        dm.write_tsv(norm_output_file)
        
        print 'Success...'
        sys.stdout.flush()
        
        # generate site localization statistics and gene-level phospho matrices
        if mod == 'phospho' and localize_phosphosites == 'true':
            generate_site_localization_statistics(output_path, qc_path, data_set_name)
            
            # old method
            ## generate gene-level matrices
            ## data_file columns
            #phosphosite_index_col = 0
            #gene_col = 1
            #first_gene_intensity_col = 1
            #
            #gene_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios.tsv'
            #generate_gene_level_phospho_matrices_from_phosphosites(output_file, gene_output_file, header, phosphosite_index_col, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash)

    expression_sets_filename = ''
    if filename_mod == 'nglycoform' or filename_mod == 'oglycoform':
        expression_sets_filename = filename_mod + filename_dash + 'expression_matrix_sets-log2_ratios.log'
    else:
        expression_sets_filename = 'expression_matrix_sets-log2_ratios.log'
        
    f = open(output_path + expression_sets_filename, 'w')
    
    f.write('Protein matrix set files included in the combined protein log2 ratio matrix -\n')
    for protein_matrix in protein_matrices:
        f.write('\t' + str(os.path.basename(protein_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Peptide matrix set files included in the combined peptide log2 ratio matrix -\n')
    for peptide_matrix in peptide_matrices:
        f.write('\t' + str(os.path.basename(peptide_matrix).replace('.tsv', '')) + '\n')
        
    f.write('\n')
    
    f.write('Site matrix set files included in the combined site log2 ratio matrix -\n')
    for site_matrix in site_matrices:
        f.write('\t' + str(os.path.basename(site_matrix).replace('.tsv', '')) + '\n')
    
    f.close()

def generate_gene_level_speg_matrices(data_file, gene_output_file, header, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash):
    # generate the log2 gene matrix
    print '\nRolling up the glycopeptide matrix to glycogene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            gene = line[gene_col].strip()
            intensities = line[first_intensity_col:]
            
            gene_index = ''
            if gene != '-':
                gene_index = gene
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            if not gene_name_found_flag:
                if gene_index != '':
                    gene_name_found_flag = True
                    
            if gene_index != '':
                if gene_index not in gene_map:
                    gene_map[gene_index] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[gene_index][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.median(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene.Index'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush() 
            
            # generate the MD norm and MD norm + MAD scaling gene-level matrices
            generate_median_norm_and_mad_scaling_gene_matrices(gene_output_file, first_gene_intensity_col, filename_mod, filename_dash)
        else:
            print 'Glyco gene indices could not be generated from the gene names...'
            sys.stdout.flush()

def generate_gene_level_phospho_matrices(data_file, gene_output_file, header, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash):
    # generate the log2 gene matrix
    print '\nRolling up the phosphopeptide matrix to phosphogene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            gene = line[gene_col].strip()
            intensities = line[first_intensity_col:]
            
            gene_index = ''
            if gene != '-':
                gene_index = gene
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            if not gene_name_found_flag:
                if gene_index != '':
                    gene_name_found_flag = True
                    
            if gene_index != '':
                if gene_index not in gene_map:
                    gene_map[gene_index] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[gene_index][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.median(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene.Index'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush() 
            
            # generate the MD norm and MD norm + MAD scaling gene-level matrices
            generate_median_norm_and_mad_scaling_gene_matrices(gene_output_file, first_gene_intensity_col, filename_mod, filename_dash)
        else:
            print 'Phospho gene indices could not be generated from the gene names...'
            sys.stdout.flush()
            
# old method
def generate_gene_level_phospho_matrices_from_phosphosites(data_file, gene_output_file, header, phosphosite_index_col, gene_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash):
    # generate the log2 gene matrix
    print '\nRolling up the phosphosite matrix to phosphogene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            phosphosite_index = line[phosphosite_index_col].strip()
            gene = line[gene_col].strip()
            intensities = line[first_intensity_col:]
            
            site_position = phosphosite_index.split('_')[-1].strip()
            gene_index = ''
            if site_position != '0' and gene != '-':
                gene_index = gene + '_' + site_position
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            if not gene_name_found_flag:
                if gene_index != '':
                    gene_name_found_flag = True
                    
            if gene_index != '':
                if gene_index not in gene_map:
                    gene_map[gene_index] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[gene_index][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.median(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene.Index'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush() 
            
            # generate the MD norm and MD norm + MAD scaling gene-level matrices
            generate_median_norm_and_mad_scaling_gene_matrices(gene_output_file, first_gene_intensity_col, filename_mod, filename_dash)
        else:
            print 'Phospho gene indices could not be combined from the gene names and site positions...'
            sys.stdout.flush()

def generate_site_localization_statistics(output_path, qc_path, data_set_name):
    # input file to read
    matrix_file = output_path + 'phosphosite_matrix-log2_ratios.tsv'
    
    # column of the phosphosite index in the matrix file
    phosphosite_index_col = 0
    
    # number of header rows
    header = 1
    
    if os.path.exists(matrix_file):
        print '\nGenerating quality control site localization statistics...'
        print '-'
        sys.stdout.flush()
        
        output_file = qc_path + 'stats-site_localization-' + data_set_name + '.log'
        
        basename = os.path.basename(matrix_file).replace('.tsv', '')
        
        f = open(matrix_file, 'r')
        matrix_lines = f.readlines()
        f.close()
        
        unlocalized_sites = 0
        localized_sites = 0
        for line in matrix_lines[header:]:
            line = line.strip().split('\t')
            phosphosite_index = line[phosphosite_index_col].strip().replace('"', '')
            site_position = phosphosite_index.split('_')[-1].strip()
            
            if site_position == '0':
                unlocalized_sites += 1
            else:
                localized_sites += 1
        
        total_sites = localized_sites + unlocalized_sites
        #print basename
        #print '-'
        #print 'Total sites = ' +  str(total_sites)        
        #print 'Localized sites = ' + str(localized_sites)
        #print 'Unlocalized sites = ' + str(unlocalized_sites)
        #print 'Percent unlocalized sites = ' + str(round((float(unlocalized_sites) / float(total_sites)) * 100.0, 2)) + ' %'
        
        f = open(output_file, 'w')
        
        f.write(str(basename) + '\n')
        f.write('-' + '\n')
        f.write('Total sites = ' +  str(total_sites) + '\n')
        f.write('Localized sites = ' + str(localized_sites) + '\n')
        f.write('Unlocalized sites = ' + str(unlocalized_sites) + '\n')
        f.write('Percent unlocalized sites = ' + str(round((float(unlocalized_sites) / float(total_sites)) * 100.0, 2)) + ' %')
        
        f.close()
        
    print 'Success...'
    sys.stdout.flush()

def generate_median_norm_and_mad_scaling_gene_matrices(output_file, first_gene_intensity_col, filename_mod, filename_dash):
    # generate the log2 gene matrices with median (MD) normalization and MD + median absolute deviation (MAD) scaling
    print '\nCombining ' + filename_mod + filename_dash + 'gene expression matrices into a final median normalized (MD) ' + filename_mod + filename_dash + 'gene matrix...'
    print '-'
    sys.stdout.flush()
    
    output_path = os.path.dirname(output_file) + '\\'
    
    f = open(output_file, 'r')
    original_lines = f.readlines()
    f.close()
    
    # retrieves the column header from the gene_matrix-log2_ratios.tsv file
    column_header_names = original_lines[0].strip().split('\t')[first_gene_intensity_col:]
    
    dm = DataMatrix.data_matrix_from_tsv(output_file, header = True)
    dm.quantitative_columns_to_float(column_header_names, missing_value_imputed_with = None)
    
    log2_medians = dm.get_medians_omit_none_values(column_header_names)
        
    dm.md_normalize_columns(column_header_names, log2_medians)
    
    norm_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios-MD_norm.tsv'
    dm.write_tsv(norm_output_file)
    
    print 'Success...'
    sys.stdout.flush()
    
    print '\nCombining ' + filename_mod + filename_dash + 'gene expression matrices into a final median normalized (MD) + median absolute deviation (MAD) scaled ' + filename_mod + filename_dash + 'gene matrix...'
    print '-'
    sys.stdout.flush()
    
    dm.mad_scale_columns(column_header_names, log2_medians)
    
    norm_output_file = output_path + filename_mod + filename_dash + 'gene_matrix-log2_ratios-MD_norm_MAD_scaling.tsv'
    dm.write_tsv(norm_output_file)
    
    print 'Success...'
    sys.stdout.flush() 

def generate_gene_level_matrices(data_file, gene_output_file, gene_name_identifier, header, protein_col, first_intensity_col, first_gene_intensity_col, filename_mod, filename_dash):
    # generate the log2 gene matrix
    print '\nRolling up the protein matrix to gene-level...'
    print '-'
    sys.stdout.flush()
    
    if os.path.exists(data_file):
        f = open(data_file, 'r')
        lines = f.readlines()
        f.close()
        
        original_header_intensity_cols = lines[0].strip().split('\t')[first_intensity_col:]
        
        gene_map = {}
        gene_name_found_flag = False
        for line in lines[header:]:
            line = line.strip().split('\t')
            proteins = line[protein_col].strip().split(';')
            intensities = line[first_intensity_col:]
            
            for i, intensity in enumerate(intensities):
                intensity = intensity.strip()
                # missing value flag in MS-PyCloud expression matrices is 'None'
                if intensity != 'None':
                    intensities[i] = float(intensity)
            
            current_genes = []
            for protein in proteins:
                if gene_name_identifier in protein:
                    gene = protein.split(gene_name_identifier)[1].strip().split(' ')[0].strip()
                    if gene != '' and gene not in current_genes:
                        current_genes.append(gene)
                
            if not gene_name_found_flag:
                if current_genes != []:
                    gene_name_found_flag = True
                    
            current_genes = sorted(current_genes)       
            current_genes = ';'.join(current_genes)
                    
            if current_genes != '':
                if current_genes not in gene_map:
                    gene_map[current_genes] = [[] for x in range(len(intensities))]
                
                for i, intensity in enumerate(intensities):
                    if intensity != 'None':
                        gene_map[current_genes][i].append(intensity)
                        
        for gene in gene_map:
            for i, intensity_list in enumerate(gene_map[gene]):
                if intensity_list != []:
                    gene_map[gene][i] = numpy.median(intensity_list)
                else:
                    gene_map[gene][i] = 'None'
        
        if gene_name_found_flag:
            f = open(gene_output_file, 'w')
            header_line = 'Gene'
            for header_col in original_header_intensity_cols:
                header_line += '\t' + str(header_col).strip()
            
            f.write(str(header_line).strip() + '\n')
            
            for gene in sorted(gene_map):
                output_row = str(gene)
                for intensity in gene_map[gene]:
                    output_row += '\t' + str(intensity)
                    
                f.write(str(output_row).strip() + '\n')
                    
            f.close()
            
            print 'Success...'
            sys.stdout.flush() 
            
            # generate the MD norm and MD norm + MAD scaling gene-level matrices
            generate_median_norm_and_mad_scaling_gene_matrices(gene_output_file, first_gene_intensity_col, filename_mod, filename_dash)
        else:
            print 'Gene names not found in the protein names...'
            sys.stdout.flush()

def combine_expression_matrices(header_section, header_data, num_channels, output_file, matrices, protein_flag, peptide_flag, site_flag, file_end, mod, localize_phosphosites):
    # if combining protein matrices then the starting column with the ratio data is column 2, otherwise it is column 1 (e.g. glycosites, glycopeptides)
    if protein_flag:
        first_ratio_col = 2
    else:
        if mod == 'phospho' and localize_phosphosites == 'true':
            # the phosphosite matrices have an additional column compared to the phosphopeptide matrices
            if site_flag:
                first_ratio_col = 4
            else:
                first_ratio_col = 3
        elif mod == 'intact':
            first_ratio_col = 2
        elif mod == 'glyco':
            if peptide_flag or site_flag:
                first_ratio_col = 2
            else:
                # handles gene-level data
                first_ratio_col = 1
        else:
            first_ratio_col = 1
        
    header_sections = ['iTRAQ4plex', 'iTRAQ8plex', 'TMT10plex', 'TMT11plex', 'TMT16plex']
    
    header_dict = {}
    header_flag = False
    specified_names = []
    # get sample names to be used as the header for the final matrices
    for line in header_data:
        line = line.strip()
        split_line = line.split('\t')
        
        if line == header_section:
            header_flag = True
            continue
            
        if header_flag == True and (line == '' or line in header_sections):
            header_flag = False
            break
        
        if header_flag == True:
            # skip header row
            if split_line[0].strip() == 'Filename':
                continue
            else:
                filename = split_line[0].strip().replace('.txt', '').replace('.tsv', '')
                
                header_dict[filename] = []
                for name in split_line[1:]:
                    header_dict[filename].append(name)
                    
                    # make sure that the sample names are unique
                    if name in specified_names:
                       raise Exception('sample names in header_expression_matrix.tsv were not unique')
                       
                    specified_names.append(name) 
         
    combined_matrix = {}
    combined_matrix['header'] = []
    
    matrix_count = -1
    files_len = len(matrices)
    
    percent_interval = floor(100.0 / files_len)
    if percent_interval < 5.0:
        percent_interval = 5.0

    percent_checked = 0.0
    num_files_checked = 0

    print 'Reading ' + file_end + ' expression matrices progress... ' + str(percent_checked) + ' % (update interval >= ' + str(percent_interval) + ' %)'
    sys.stdout.flush()
    for matrix in matrices:
        num_files_checked += 1
        
        filename = os.path.basename(matrix).replace('.tsv', '')
        for mod_ending in mod_endings:
            filename = filename.replace('.' + mod_ending, '')
    
        matrix_count += 1
        
        f = open(matrix, 'r')
        lines = f.readlines()
        f.close()
        
        header_line = lines[0].strip().split('\t')
        
        if matrix_count == 0:
            combined_matrix['header'].append(header_line[0])
            
            if protein_flag:
                combined_matrix['header'].append(header_line[1])
                
            # add in extra localization header columns
            if mod == 'phospho' and localize_phosphosites == 'true':
                combined_matrix['header'].append(header_line[1])
                combined_matrix['header'].append(header_line[2])
                if site_flag:
                    combined_matrix['header'].append(header_line[3])
                    
            if mod == 'intact':
                combined_matrix['header'].append(header_line[1])
                
            if mod == 'glyco' and (peptide_flag or site_flag):
                combined_matrix['header'].append(header_line[1])
            
            if filename in header_dict:
                for header_value in header_dict[filename]:
                    combined_matrix['header'].append(header_value)
            else:
                print filename + ' was missing from the header_expression_matrix.tsv file. Using default header.'
                sys.stdout.flush()
                
                for header_value in header_line[first_ratio_col:]:
                    combined_matrix['header'].append(header_value + ' (' + filename + ')')
        else:
            if filename in header_dict:
                for header_value in header_dict[filename]:
                    combined_matrix['header'].append(header_value)
            else:
                print filename + ' was missing from the header_expression_matrix.tsv file. Using default header.'
                sys.stdout.flush()
                
                for header_value in header_line[first_ratio_col:]:
                    combined_matrix['header'].append(header_value + ' (' + filename + ')')
    
        for line in lines[header:]:
            line = line.strip()
            split_line = line.split('\t')
            
            protein = split_line[0]
            if protein not in combined_matrix:
                # puts in protein names column
                combined_matrix[protein] = []
                
                if protein_flag:
                    combined_matrix[protein].append(split_line[1])
                    
                # add in extra localization data columns
                if mod == 'phospho' and localize_phosphosites == 'true':
                    combined_matrix[protein].append(split_line[1])
                    combined_matrix[protein].append(split_line[2])
                    if site_flag:
                        combined_matrix[protein].append(split_line[3])
                
                if mod == 'intact':
                    combined_matrix[protein].append(split_line[1])
                    
                if mod == 'glyco' and (peptide_flag or site_flag):
                    combined_matrix[protein].append(split_line[1])
                
                # fills in 0 values for previous expression matrices
                for i in range(num_channels * matrix_count):
                    combined_matrix[protein].append('None')
                    
                for ratio in split_line[first_ratio_col:]:
                    combined_matrix[protein].append(ratio)
            else:
                for ratio in split_line[first_ratio_col:]:
                    combined_matrix[protein].append(ratio)
                    
        
        # total number of entries that each dictionary key should currently have in its list    
        num_ratios = (matrix_count + 1) * num_channels + (first_ratio_col - 1)
        
        for key in combined_matrix:
            current_num_ratios = len(combined_matrix[key])
            ratio_diff = num_ratios - current_num_ratios
                
            if ratio_diff > 0:
                if ratio_diff % num_channels != 0:
                    print 'Error! Data is corrupted.  Ratio columns were not appended correctly.'
                else:
                    for i in range(ratio_diff):
                        combined_matrix[key].append('None')
                
        current_percent = ((100.0 * num_files_checked) / files_len)
        if current_percent >= (percent_checked + percent_interval):
            percent_checked = floor(current_percent)
        
            print 'Reading ' + file_end + ' expression matrices progress... ' + str(percent_checked) + ' %'
            sys.stdout.flush()
            
    if percent_checked < 100.0:
        print 'Reading ' + file_end + ' expression matrices progress... 100 %'
        sys.stdout.flush()
                        
    f = open(output_file, 'w')
    
    output_line = ''
    for header_col in combined_matrix['header']:
        output_line += str(header_col) + '\t'
        
    f.write(output_line.strip() + '\n')
    
    for key in combined_matrix:
        if key != 'header':
            output_line = str(key)
            
            for value in combined_matrix[key]:
                output_line += '\t' + str(value)
                
            f.write(output_line + '\n')
        
    f.close()    

def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]
    
if __name__ == '__main__':
    # header_expression_matrix data
    f = open(config_path + 'header_expression_matrix.tsv', 'r')
    header_expression_matrix_data = f.readlines()
    f.close()
            
    configs = {}
    configs['options'] = steps(config_path + 'steps.txt')
    configs['sample filenames'] = rl(config_path + 'sample_filenames.txt')
    configs['header_expression_matrix'] = header_expression_matrix_data
    configs['backup'] = True
    
    main(configs)