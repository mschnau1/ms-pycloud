"""
Description:
    Finds the executable files needed to run the rest CPTAC processing pipeline.
    
Notes:
"""

import os
import sys
import tkMessageBox
from Tkinter import *

parent_path = os.path.dirname(os.path.abspath(__file__)) + '\\'
install_path = parent_path + 'installs\\'

python_exe = install_path + 'python-2.7.14_64-bit-systems.msi'
python3_exe = install_path + 'python-3.7.9-amd64.exe'
python_path = 'C:\\Python27_MS-PyCloud\\'
python3_path = 'C:\\Python37_MS-PyCloud\\'

def main():
    print '\nStep 0:\tInstalling the executables that will be used to run the pipeline...'
    print '-' * 150
    sys.stdout.flush()
    
    # install python2
    try:
        os.popen('msiexec /passive /i ' + str(python_exe) + ' TARGETDIR=' + str(python_path))
    except Exception as e:
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Error!', 'Python 2.7.14 was not able to be installed.  Please double click the following .msi file to manually install it:\n\n' + str(python_exe))
        root.destroy()
    
    # install python3
    try:
        os.popen(str(python3_exe) + ' TargetDir=' + str(python3_path) + ' /passive')
    except Exception as e:
        root = Tk()
        root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
        root.update_idletasks()
        tkMessageBox.showinfo('Error!', 'Python 3.7.9 was not able to be installed.  Please double click the following .exe file to manually install it:\n\n' + str(python3_exe))
        root.destroy()
    
    paths = {}
    
    # python2 and python3
    if os.path.exists(python_path) and os.path.exists(python3_path):
        installed_path = python_path 
        installed3_path = python3_path
    else:
        import i0a_find_python as i0a
        installed_paths = i0a.main() 
        installed_path = os.path.dirname(os.path.dirname(installed_paths['python2714.chm'])) + '\\'
        installed3_path = os.path.dirname(os.path.dirname(installed_paths['python379.chm'])) + '\\'
        
    paths['python.exe'] = installed_path + 'python.exe'
    paths['python3.exe'] = installed3_path + 'python.exe'
    paths['pip.exe'] = installed_path + 'Scripts\\pip.exe'  
    paths['pip3.exe'] = installed3_path + 'Scripts\\pip.exe'  
    
    return paths

if __name__ == '__main__':
    main()