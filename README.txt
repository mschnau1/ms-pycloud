1) installation instructions:
	- to download MS-PyCloud:
		- navigate to https://bitbucket.org/mschnau1/ms-pycloud/src/main/
		- click "ms-pycloud_v*-setup.exe"
		- click "View raw" button (this will download the installer package)
	
	- once downloaded, navigate to your Downloads folder and 
	  double-click "ms-pycloud_v*-setup.exe"
	
2) MS-PyCloud will be installed and a shortcut will be placed on your Desktop:
	- to run MS-PyCloud either double-click the Desktop shortcut, or navigate to 
	  the installation folder and double-click "MS-PyCloud.bat"